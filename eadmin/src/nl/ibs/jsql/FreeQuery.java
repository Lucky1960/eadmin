package nl.ibs.jsql;



import java.util.ArrayList;
import java.util.Collection;
import java.util.ListIterator;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

import nl.ibs.jsql.sql.*;


/**
 * @author c_hc01
 */
public interface FreeQuery extends SuperQuery, SharedQueryExecutionMethods {

	/**
	 * Sets the translatable Query string. <br/>
	 * If there is aleady a query set, this will be replaced by the query supplied as an argument.
	 *
	 * @param filter a String defining a valid query.
	 */
	public void setQuery(String query);
	
	
	/**
	 * Utility method, sets the translatable Query string after first replacing posible occurences in the Query string 
	 * of the keys suplied in the map by the corresponding values in the map. <br/>
	 * If there is aleady a query set, this will be replaced by the resulting query.
	 * 
	 *
	 * @param filter a String defining a valid query.
	 * @param replacements a Map with strings (keys) and the values with wicht they schould be replaced in the query string .
	 */
	public void setQuery(String query, Map replacements);

		
	/**
	 * Returns the query result set as a 2 dimensional object array.
	 *
	 * @return the query result set as a 2 dimensional object array
	 */
	public Object[][] getResultArray() throws Exception;

	/**
	 * Returns the query result as a object array of specified type. Uses the columnames or aliasses to set
	 * the object attributes. You can uses aliases in your SELECT clause to map column names to object attrributes.
	 * Collums returned should map tot attributes of the correct type in the class provided or there should be no attributes avalilable
	 * with the same name ignoring case. Best performace will be realised if there is an exact match (name and type) between,
	 * columns returned and the attributes on the object.
	 * If this is not feasable use getResultCollection(Class clss, String[] variables) instead.
	 * 
	 * @param the (non abstract) class that should be returned. Class must have an empty constructor. 
	 * @return the query result as a object array
	 */
	public Collection getResultCollection(Class clss) throws Exception;

	/**
	 * Returns the query result as a Collection of objects of the specified type.
	 * 
	 * @param the (non abstract) class that should be returned. Class must have an empty constructor. 
	 * @param the varable names that have to be set by the implementation in the proper order.
	 * Variable types must be compatible with types returned by the query (optimal performance) or returned types
	 * should be convertable to varable types (i.e. Integer to String or int to BigDecimal). If the query returns values that
	 * can or should be ignored, don't skip them but supply null values in there place in the array with variable names i.e. new String[]{"name", "age", null, null, "city"}
	 * @return the query result as a Collection of specified type.
	 */
	public Collection getResultCollection(Class clss, String[] variables) throws Exception;

	/**
	 * Returns a subset of the full resultset as a 2 dimensional object array.
	 * The subcollection begins at the specified <code>beginIndex</code> and
	 * extends to the index <code>endIndex - 1</code>.<br/>
	 * If both beginIndex and endIndex are 0 then full result is returned.<br/>
	 * If endIndex exceeds subset size then everything from beginIndex and thereafter is returned.<br/>
	 * Thus if endIndex > 0 the maximum size of the ArrayList is <code>endIndex-beginIndex</code>.
	 * <p>
	 * Examples:
	 * <blockquote><pre>
	 * Thus if the full collection is represented by A,B,C,D,E,F then
	 * getResultArray(1, 3) returns a result representing B,C
	 * getResultArray(0, 5) returns a result representing A,B,C,D,E
	 * getResultArray(5, 10) returns a result representing F
	 * getResultArray(0,0) returns a result representing A,B,C,D,E,F
	 * </pre></blockquote>
	 *
	 * @param      beginIndex   the beginning index, inclusive.
	 * @param      endIndex     the ending index, exclusive.
	 * @return     2 dimensional object array representing the specified subset.
	 * @exception  IllegalArgumentException  if the
	 *             <code>beginIndex</code> is negative,or
	 *             <code>endIndex</code> is negative,or
	 *             <code>beginIndex</code> is larger than
	 *             <code>endIndex</code>.
	 */
	public Object[][] getResultArray(int beginIndex, int endIndex) throws Exception;

	/**
	 * Returns a subset of the full resultset as as a object array of specified type. Uses the columnames or aliasses to set
	 * the object attributes. You can uses aliases in your SELECT clause to map column names to object attrributes.
	 * Collums returned should map tot attributes of the correct type in the class provided or there should be no attributes avalilable
	 * with the same name ignoring case. Best performace will be realised if there is an exact match (name and type) between,
	 * columns returned and the attributes on the object.<br/>
	 * If this is not feasable use getResultCollection(Class clss, String[] variables, int beginIndex, int endIndex) instead.<br/>
	 *
	 * The subcollection begins at the specified <code>beginIndex</code> and
	 * extends to the index <code>endIndex - 1</code>.<br/>
	 * If both beginIndex and endIndex are 0 then full result is returned.<br/>
	 * If endIndex exceeds subset size then everything from beginIndex and thereafter is returned.<br/>
	 * Thus if endIndex > 0 the maximum size of the ArrayList is <code>endIndex-beginIndex</code>.
	 * <p>
	 * Examples:
	 * <blockquote><pre>
	 * Thus if the full collection is represented by A,B,C,D,E,F then
	 * getResultCollection(clss,1, 3) returns a result representing B,C
	 * getResultCollection(clss,0, 5) returns a result representing A,B,C,D,E
	 * getResultCollection(clss,5, 10) returns a result representing F
	 * getResultCollection(clss,0,0) returns a result representing A,B,C,D,E,F
	 * </pre></blockquote>
	 *
	 * @param the (non abstract) class that should be returned. Class must have an empty constructor. 
	 * @param      beginIndex   the beginning index, inclusive.
	 * @param      endIndex     the ending index, exclusive.
	 * @return     the object array of specified type representing the specified subset.
	 * @exception  IllegalArgumentException  if the
	 *             <code>beginIndex</code> is negative,or
	 *             <code>endIndex</code> is negative,or
	 *             <code>beginIndex</code> is larger than
	 *             <code>endIndex</code>.
	 */
	public Collection getResultCollection(Class clss, int beginIndex, int endIndex) throws Exception;

	/**
	 * Returns a subset of the full resultset as a Collection of objects of the specified type.
	 * The subcollection begins at the specified <code>beginIndex</code> and
	 * extends to the index <code>endIndex - 1</code>.<br/>
	 * If both beginIndex and endIndex are 0 then full result is returned.<br/>
	 * If endIndex exceeds subset size then everything from beginIndex and thereafter is returned.<br/>
	 * Thus if endIndex > 0 the maximum size of the ArrayList is <code>endIndex-beginIndex</code>.
	 * <p>
	 * Examples:
	 * <blockquote><pre>
	 * Thus if the full collection is represented by A,B,C,D,E,F then
	 * getResultCollection(clss, variables, 1, 3) returns a result representing B,C
	 * getResultCollection(clss, variables, 0, 5) returns a result representing A,B,C,D,E
	 * getResultCollection(clss, variables, 5, 10) returns a result representing F
	 * getResultCollection(clss, variables, 0,0) returns a result representing A,B,C,D,E,F
	 * </pre></blockquote>
	 *
	 * @param the (non abstract) class that should be returned. Class must have an empty constructor. 
	 * @param the varable names that have to be set by the implementation in the proper order.
	 *    Variable types must be compatible with types returned by the query (optimal performance) or returned types
	 *    should be convertable to varable types (i.e. Integer to String or int to BigDecimal). If the query returns values that
	 *    can or should be ignored, don't skip them but supply null values in there place in the array with variable names i.e. new String[]{"name", "age", null, null, "city"}
	 * @param      beginIndex   the beginning index, inclusive.
	 * @param      endIndex     the ending index, exclusive.
	 * @return     the Collection of objects of the specified type representing the specified subset.
	 * @exception  IllegalArgumentException  if the
	 *             <code>beginIndex</code> is negative,or
	 *             <code>endIndex</code> is negative,or
	 *             <code>beginIndex</code> is larger than
	 *             <code>endIndex</code>.
	 */
	public Collection getResultCollection(Class clss, String[] variables, int beginIndex, int endIndex) throws Exception;	

	/**
	 * This method returns a collections of the fillable specified, Fillable is a interface.
	 * @param filled Fillable that needs to be filled.
	 * @param beginIndex startindex
	 * @param endIndex end index for the list.
	 * @return a Collection of fillables.
	 * @throws Exception
	 */	
	public Collection getResultCollection(Fillable filled, int beginIndex, int endIndex) throws Exception;

}

