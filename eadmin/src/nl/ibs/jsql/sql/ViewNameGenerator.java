package nl.ibs.jsql.sql;


/**
 * @author c_md07
 *
 */
public final class ViewNameGenerator {
	private long name = -1;

	/** This is the default instance used for this singleton. */
	private static ViewNameGenerator defaultInstance;

	/**
	 * Constructor of the object. <br>
	 *
	 * This constructor should remain private
	 */
	private ViewNameGenerator() {
		super();
	}

	/**
	 * Gets the unique instance of this class. <br>
	 *
	 * @return the unique instance of this class
	 */
	public final synchronized static ViewNameGenerator getInstance() {
		if (ViewNameGenerator.defaultInstance == null) {
			ViewNameGenerator.defaultInstance = new ViewNameGenerator();
		}
		return ViewNameGenerator.defaultInstance;
	}

	public String getNewViewName() {
		name++;
		return "vw" + String.valueOf(name);
	}

}

