package nl.ibs.jsql.sql;



import java.util.*;
import java.sql.*;
import nl.ibs.jeelog.*;



/**
 *
 * @author  c_hc01
 */
public class ForeignKeyDefinition {
    private TreeMap foreignKeyColumnPairs  = new TreeMap();
    
	
    public static final short UPDATE_RULE_KEY_NO_ACTION = DatabaseMetaData.importedKeyNoAction; //do not allow update of primary key if it has been imported 
	public static final short UPDATE_RULE_KEY_CASCADE = DatabaseMetaData.importedKeyCascade; //change imported key to agree with primary key update 
	public static final short UPDATE_RULE_KEY_SET_NULL = DatabaseMetaData.importedKeySetNull; //change imported key to NULL if its primary key has been updated 
	public static final short UPDATE_RULE_KEY_SET_DEFAULT = DatabaseMetaData.importedKeySetDefault; //change imported key to default values if its primary key has been updated 
	public static final short UPDATE_RULE_KEY_RESTRICT = DatabaseMetaData.importedKeyRestrict; //same as importedKeyNoAction (for ODBC 2.x compatibility)
    
	public static final short DELETE_RULE_KEY_NO_ACTION = DatabaseMetaData.importedKeyNoAction; // do not allow delete of primary key if it has been imported 
	public static final short DELETE_RULE_KEY_CASCADE = DatabaseMetaData.importedKeyCascade; // delete rows that import a deleted key 
	public static final short DELETE_RULE_KEY_SET_NULL = DatabaseMetaData.importedKeySetNull; // change imported key to NULL if its primary key has been deleted 
	public static final short DELETE_RULE_KEY_RESTRICT = DatabaseMetaData.importedKeyRestrict; // same as importedKeyNoAction (for ODBC 2.x compatibility) 
	public static final short DELETE_RULE_KEY_SET_DEFAULT = DatabaseMetaData.importedKeySetDefault; // change imported key to default if its primary key has been deleted 

	public static final short DEFERRABLITY_KEY_INITIALLY_DEFERRED = DatabaseMetaData.importedKeyInitiallyDeferred; // see SQL92 for definition 
	public static final short DEFERRABLITY_KEY_INITIALLY_IMMEDIATE = DatabaseMetaData.importedKeyInitiallyImmediate; // see SQL92 for definition 
	public static final short DEFERRABLITY_KEY_NOT_DEFERABLE = DatabaseMetaData.importedKeyNotDeferrable; 

    

	private String PKTableCatalog;     // =>(may be null)
	private String PKTableSchema;      // =>(may be null)
	private String PKTableName;
    //private String PKColumnName;
	private String FKTableCatalog;     // =>(may be null)
	private String FKTableSchema;      // =>(may be null) public void createIndexDefinitionClass(){
	private String FKTableName;        
    //private String FKColumnName;        
    //private short keySeq;             // =>Sequence number within a foreign key 
	private short updateRule;
	private short deleteRule;
	private String FKName;            // Foreign key name (may be null) 
	private String PKName;            // Primary key name (may be null) 
	private short deferrability;       

    public static HashMap getForeignKeyDefinitions(String schemaPattern, String tablePattern, DatabaseMetaData meta) {
        HashMap map = new HashMap();
        
        ResultSet rs = null;
        String _schemaPattern = (schemaPattern == null || schemaPattern.equals("")) ? null : schemaPattern;
        try{
            rs = meta.getImportedKeys(null , _schemaPattern , tablePattern);
            while(rs.next()){
            	String name = rs.getString(12);
                if (name == null || name.trim().equals("")){
                    Log.error("Found foreign key without name, this is not permitted and may cause problems");
                    continue;    
                }
                ForeignKeyDefinition definition = (ForeignKeyDefinition)map.get(name);
                if (definition == null){
                    definition = new ForeignKeyDefinition(rs,meta);
                    map.put(name , definition);
                }else{
                    definition.update(rs,meta);
                }
            }
        }catch(Exception e){
            Log.error(e.getMessage());
            e.printStackTrace();
        }finally{
        	if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {	
				}
        }
        return map;
    }
    
    private ForeignKeyDefinition(ResultSet rs, DatabaseMetaData meta){
        try{
            ResultSetMetaData rsmd = rs.getMetaData();
            int z = rsmd.getColumnCount();

            if(z < 1)
              return;
            PKTableCatalog = rs.getString(1);       // (may be null)
            if(z < 2)
              return;
            PKTableSchema = rs.getString(2);        // (may be null)
            if(z < 3)
              return;
            PKTableName = rs.getString(3);
            //if(z < 4)
            //  return;
            //PKColumnName = rs.getString(4);
            if(z < 5)
              return;
            FKTableCatalog = rs.getString(5);       // (may be null)
            if(z < 6)
              return;
            FKTableSchema = rs.getString(6);        // (may be null)
            if(z < 7)
              return;
            FKTableName = rs.getString(7);
            //if(z < 8)
            //  return;
            //FKColumnName = rs.getString(8);
            //if(z < 9)
            //  return;
            //keySeq = rs.getShort(9);               
            if(z < 10)
              return;
            updateRule = rs.getShort(10);         
            if(z < 11)
              return;
            deleteRule = rs.getShort(11);           
            if(z < 12)
               return;
            FKName = rs.getString(12);
            if(z < 13)
               return;
            PKName = rs.getString(13);
            if(z < 14)
               return;
            deferrability = rs.getShort(14);

            update (rs, meta);
             
        }catch(Exception e){
            Log.error(e.getMessage());
            e.printStackTrace(); 
        }
    }

    public void update(ResultSet rs, DatabaseMetaData meta){
        try{
            ResultSetMetaData rsmd = rs.getMetaData();
            if( rsmd.getColumnCount() < 9)
              return;
            ForeignKeyColumnPair pair = new ForeignKeyColumnPair();
            pair.PKColumnName = rs.getString(4);
            pair.FKColumnName = rs.getString(8);
            
            short _keySeq = rs.getShort(9); 

            foreignKeyColumnPairs.put(new Integer(_keySeq-1),pair);
             
        }catch(Exception e){
            Log.error(e.getMessage());
            e.printStackTrace(); 
        }
    }
    
	/**
	 * Returns wether the evaluation of foreign key constraints can be deferred until commit.<br/>
	 * Possible return values:
	 * <ul>
	 * <li>DEFERRABLITY_KEY_INITIALLY_DEFERRED - see SQL92 for definition</li>
	 * <li>DEFERRABLITY_KEY_INITIALLY_IMMEDIATE - see SQL92 for definition</li>
	 * <li>DEFERRABLITY_KEY_NOT_DEFERABLE</li>
	 * </ul> 
	 * @return deferibility
	 */
	public short getDeferrability() {
		return deferrability;
	}

	/**
	 * @return
	 */
	public String getFKName() {
		return FKName;
	}

	/** Foreign key table catalog
	 * @return FKTableCatalog or null;
	 */
	public String getFKTableCatalog() {
		return FKTableCatalog;
	}

	/** ForeignKey Table Name
	 * @return FKTableName;
	 */
	public String getFKTableName() {
		return FKTableName;
	}

   /** Foreign key table schema
	 * @return FKTableSchema or null;
	 */
	public String getFKTableSchema() {
		return FKTableSchema;
	}

	/** ForeignKey column pairs 
	 * @return foreignKey column pairs in sequence of the foreign key
	 */
	public ForeignKeyColumnPair[] getForeignKeyColumnPairs() {
		ForeignKeyColumnPair[] fkcps = new ForeignKeyColumnPair[foreignKeyColumnPairs.size()];
		Iterator it = foreignKeyColumnPairs.values().iterator();
		for (int i = 0; i < fkcps.length && it.hasNext(); i++)
			fkcps[i] = (ForeignKeyColumnPair) it.next();
		return fkcps;
	}

	/** Primary key name 
	 * @return PKName
	 */
	public String getPKName() {
		return PKName;
	}

	/** Primairy key table catalog
	 * @return PKTableCatalog or null;
	 */
	public String getPKTableCatalog() {
		return PKTableCatalog;
	}

	/** Primairy key table name
	 * @return
	 */
	public String getPKTableName() {
		return PKTableName;
	}

	/** Primairy key table schema
	  * @return FKTableSchema or null;
	  */
	public String getPKTableSchema() {
		return PKTableSchema;
	}

	/**
	 * Returns what happens to a foreign key when the primary key is updated:<br/>
	 * <ul>
	 * <li>UPDATE_RULE_KEY_NO_ACTION - do not allow update of primary key if it has been imported</li> 
	 * <li>UPDATE_RULE_KEY_CASCADE - change imported key to agree with primary key update</li>  
	 * <li>UPDATE_RULE_KEY_SET_NULL - change imported key to NULL if its primary key has been updated</li>  
	 * <li>UPDATE_RULE_KEY_SET_DEFAULT - change imported key to default values if its primary key has been updated</li>  
	 * <li>UPDATE_RULE_KEY_RESTRICT - same as importedKeyNoAction (for ODBC 2.x compatibility)</li> 
     * </ul>
	 * @return update rule
	 */
	public short getUpdateRule() {
		return updateRule;
	}

	/**
	 * Returns what happens to the foreign key when primary is deleted:<br/>
	 * <ul>
	 * <li>DELETE_RULE_KEY_NO_ACTION - do not allow delete of primary key if it has been imported</li> 
	 * <li>DELETE_RULE_KEY_CASCADE - delete rows that import a deleted key</li> 
	 * <li>DELETE_RULE_KEY_SET_NULL - change imported key to NULL if its primary key has been deleted</li> 
	 * <li>DELETE_RULE_KEY_RESTRICT - same as importedKeyNoAction (for ODBC 2.x compatibility)</li> 
	 * <li>DELETE_RULE_KEY_SET_DEFAULT - change imported key to default if its primary key has been deleted</li>
	 * </ul>
	 * @return delete rule
	 */
	public short getDeleteRule() {
		return deleteRule;
	}

}

