package nl.ibs.jsql.sql;



import java.util.*;
import java.sql.*;
import nl.ibs.jeelog.*;

/**
 *
 * @author  c_hc01
 */
public class IndexDefinition {
    private TreeMap indexColumns = new TreeMap();

    public static final short TYPE_TABLE_STATISTICS_AND_INDEX_DESCRIPTIONS = DatabaseMetaData.tableIndexStatistic; // this identifies table statistics that are returned in conjuction with a table's index descriptions 
    public static final short TYPE_CLUSTERED_INDEX = DatabaseMetaData.tableIndexClustered; // this is a clustered index 
    public static final short TYPE_HASHED_INDEX = DatabaseMetaData.tableIndexHashed; // this is a hashed index 
    public static final short TYPE_OTHER_INDEX = DatabaseMetaData.tableIndexOther; // this is some other style of index 

    

    private String tableCatalog;       // Table catalog (may be null)
    private String tableSchema;        // Table schema (may be null)
    private String tableName;          // Table name
    private boolean nonUnique;         // Can index values be non-unique. false when TYPE is tableIndexStatistic 
    private String indexQualifier;     // Index catalog (may be null); null when TYPE is tableIndexStatistic 
    private String indexName;          // Index name; null when TYPE is tableIndexStatistic 
    private short  indexType;          // Index type: 
    //public short  ordinalPosition;  // Column sequence number within index; zero when TYPE is tableIndexStatistic 
    //public String columnName;       // Column name; null when TYPE is tableIndexStatistic 
    //private String sortSequence;       // column sort sequence, "A" => ascending, "D" => descending, may be null if sort sequence is not supported; null when TYPE is tableIndexStatistic 
    private int cardinality;           // When TYPE is tableIndexStatistic, then this is the number of rows in the table; otherwise, it is the number of unique values in the index. 
    private int pages;                 // When TYPE is tableIndexStatisic then this is the number of pages used for the table, otherwise it is the number of pages used for the current index. 
    private String filterCondition;    // Filter condition, if any. (may be null) 

    
    public static HashMap getIndexDefinitions(String schemaPattern, String tablePattern, DatabaseMetaData meta) {
        HashMap map = new HashMap();
        String _schemaPattern = (schemaPattern == null || schemaPattern.equals("")) ? null : schemaPattern;
        ResultSet rs = null;
        try{
            rs = meta.getIndexInfo(null , _schemaPattern , tablePattern , false, false);
            while(rs.next()){
                String name = rs.getString(6);
                if (name == null || name.trim().equals("")){
                    if (rs.getShort(7) != TYPE_TABLE_STATISTICS_AND_INDEX_DESCRIPTIONS)
                        Log.warn("Found index without name omn table "+ rs.getString(3) +". Index will not be deleted");
                    continue;    
                }
                IndexDefinition definition = (IndexDefinition)map.get(name);
                if (definition == null){
                    definition = new IndexDefinition(rs,meta);
                    map.put(name , definition);
                }else{
                    definition.update(rs,meta);
                }
            }
        }catch(Exception e){
            Log.error(e.getMessage());
            e.printStackTrace();
        }finally{
        	if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {	
				}
        }
        return map;
    }
    
    private IndexDefinition(ResultSet rs, DatabaseMetaData meta){
        try{
            ResultSetMetaData rsmd = rs.getMetaData();
            int z = rsmd.getColumnCount();

            if(z < 1)
              return;
            tableCatalog = rs.getString(1);        // Table catalog (may be null)
            if(z < 2)
              return;
            tableSchema = rs.getString(2);         // Table schema (may be null)
            if(z < 3)
              return;
            tableName = rs.getString(3);           // Table name
            if(z < 4)
              return;
            nonUnique = rs.getBoolean(4);          // Can index values be non-unique. false when TYPE is tableIndexStatistic 
            if(z < 5)
              return;
            indexQualifier = rs.getString(5);      // Index catalog (may be null); null when TYPE is tableIndexStatistic 
            if(z < 6)
              return;
            indexName = rs.getString(6);           // Index name; null when TYPE is tableIndexStatistic 
            if(z < 7)
              return;
            indexType = rs.getShort(7);            // Index type:
            //if(z < 8)
            //  return; 
            //ordinalPosition = rs.getShort(8);      // Column sequence number within index; zero when TYPE is tableIndexStatistic 
            //if(z < 9)
            //  return ;
            //columnName = rs.getString(9);          // Column name; null when TYPE is tableIndexStatistic 
            //if(z < 10)
            //  return;
            //sortSequence = rs.getString(10);       // column sort sequence, "A" => ascending, "D" => descending, may be null if sort sequence is not supported; null when TYPE is tableIndexStatistic
            update (rs, meta);
            if(z < 11)
              return;
            cardinality = rs.getInt(11);           // When TYPE is tableIndexStatistic, then this is the number of rows in the table; otherwise, it is the number of unique values in the index. 
            if(z < 12)
              return;
            pages = rs.getInt(12);                 // When TYPE is tableIndexStatisic then this is the number of pages used for the table, otherwise it is the number of pages used for the current index. 
            if(z < 13)
              return;
            filterCondition = rs.getString(13);    // Filter condition, if any. (may be null) 
            

        }catch(Exception e){
            Log.error(e.getMessage());
            e.printStackTrace(); 
        }

    }

    public void update(ResultSet rs, DatabaseMetaData meta){
        try{
            ResultSetMetaData rsmd = rs.getMetaData();
            if( rsmd.getColumnCount() < 10)
              return;
            IndexColumn ic = new IndexColumn();
            ic.columnName = rs.getString(9);
            ic.sortSequence = rs.getString(10);
            
            short seq = rs.getShort(8); 
            indexColumns.put(new Integer(seq-1),ic);
             
        }catch(Exception e){
            Log.error(e.getMessage());
            e.printStackTrace(); 
        }
    }

    	   
	/** When TYPE is TYPE_TABLE_STATISTICS_AND_INDEX_DESCRIPTIONS, then this is the number of rows in the table; otherwise, it is the number of unique values in the index.
	 * 
	 * @return cardinality
	 */
	public int getCardinality() {
		return cardinality;
	}

	/** Filter condition, if any (may be null).
	 * @return filter condition or null
	 */
	public String getFilterCondition() {
		return filterCondition;
	}
	
	/** Index columns in defined sequence.
	 * 
	 * @return index columns
	 */
	public IndexColumn[] getIndexColumns(){
		IndexColumn[] ics = new IndexColumn[indexColumns.size()];
		Iterator it = indexColumns.values().iterator();
		for (int i = 0; i < ics.length && it.hasNext(); i++) {
			ics[i] = (IndexColumn)it.next();
		}	
		return ics;
	}


	/** Index name; null when TYPE is TYPE_TABLE_STATISTICS_AND_INDEX_DESCRIPTIONS
	 * @return index name 
	 */
	public String getIndexName() {
		return indexName;
	}

	/** Index catalog (may be null); null when TYPE is TYPE_TABLE_STATISTICS_AND_INDEX_DESCRIPTIONS.
	 * @return index qualifier
	 */
	public String getIndexQualifier() {
		return indexQualifier;
	}

	/** Index type.<br/>
	 * Possible values:
	 * <ul>
	 *  <li>TYPE_TABLE_STATISTICS_AND_INDEX_DESCRIPTIONS - this identifies table statistics that are returned in conjuction with a table's index descriptions</li> 
	 *  <li>TYPE_CLUSTERED_INDEX - this is a clustered index</li> 
	 *  <li>TYPE_HASHED_INDEX - this is a hashed index</li> 
	 *  <li>TYPE_OTHER_INDEX - this is some other style of index</li> 
	 * </ul>
	 * @return index type
	 */
	public short getIndexType() {
		return indexType;
	}

	/**Can index values be non-unique. Returns false when TYPE is tableIndexStatistic.
	 * @return non-unique
	 */
	public boolean isNonUnique() {
		return nonUnique;
	}

	/**When TYPE is TYPE_TABLE_STATISTICS_AND_INDEX_DESCRIPTIONS then this is the number of pages used for the table, otherwise it is the number of pages used for the current index. 
	 * @return pages
	 */
	public int getPages() {
		return pages;
	}

	/** Table catalog (may be null).
	 * @return table catalog or null
	 */
	public String getTableCatalog() {
		return tableCatalog;
	}

	/** Table name.
	 * @return table name.
	 */
	public String getTableName() {
		return tableName;
	}

	/** Table schema (may be null).
	 * @return table schema or null.
	 */
	public String getTableSchema() {
		return tableSchema;
	}
	

}

