package nl.ibs.jsql.sql;



public interface ExtendedPersistenceMetaData extends PersistenceMetaData{
	
	public String getClassNameConstraint(ConnectionProvider provider);
	
}

