package nl.ibs.jsql.sql;


import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.ListIterator;

import nl.ibs.jsql.BusinessObject;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;


/**
 *
 * @author  c_hc01
 */

abstract public class BusinessObjectManager implements Serializable{
   public static final short RETURN_FIRST_OBJECT=0;
   public static final short RETURN_COLLECTION=-1;
   public static final short RETURN_LISTITERATOR=-2;
   public static final short RETURN_DOCUMENT=-3;
   public static final short RETURN_DOCUMENT_FRAGMENT=-4;
   public static final short RETURN_DOCUMENT_FRAGMENT_ARRAYLIST=-5;
   
   private static Hashtable getInstanceMethods = new Hashtable();
	
   
  /* public static final short RETURN_PK_COLLECTION=-6;
   public static final short RETURN_PK=-7;*/
  
   
   public Object processResultSet(ResultSet resultSet, short instruction) throws Exception{
      switch(instruction){
        case RETURN_FIRST_OBJECT:
            if(resultSet.next())
                return getBusinessObject(resultSet);
            else
                return null;
         case RETURN_COLLECTION:
            return getCollection(resultSet);
         case RETURN_LISTITERATOR:
            return getListIterator(resultSet);
         case RETURN_DOCUMENT:
            return getDocument(resultSet);
         case RETURN_DOCUMENT_FRAGMENT:
            return getDocumentFragment(resultSet);
         case RETURN_DOCUMENT_FRAGMENT_ARRAYLIST:
            return getDocumentFragmentArrayList(resultSet);    
    /*   case RETURN_PK_COLLECTION:
            return getPKCollection(resultSet);  
         case RETURN_PK: 
            return getPK(resultSet);    */
         default:
           throw new Exception("Unknown instruction");         
      }
   }

   public String getClassName(ResultSet rs) throws Exception{
      ConnectionProvider provider = getConnectionProvider();
      PersistenceMetaData pmd = getPersistenceMetaData();
      return  rs.getString(pmd.getFieldName("className",provider));
   }

   abstract public ConnectionProvider getConnectionProvider() throws Exception;
   abstract public PersistenceMetaData getPersistenceMetaData ();

   abstract protected Object getBusinessObject(ResultSet rs)throws Exception;

   abstract protected Collection getCollection(ResultSet rs) throws Exception;
   abstract protected ListIterator getListIterator(ResultSet rs) throws Exception;

   abstract protected Document getDocument(ResultSet resultSet) throws Exception;
   abstract protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception;
   abstract protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception;  
   
   //abstract protected Object getPKCollection(ResultSet rs) throws Exception;
   //abstract protected Object getPK(ResultSet rs) throws Exception;
//Marco changed
   public Object getBusinessObjectForView(ResultSet rs)throws Exception {
   		return getBusinessObject(rs);
   }

   public  Collection getCollectionForView(ResultSet rs) throws Exception {
   	return getCollection(rs);
   }
   
   public ListIterator getListIteratorForView(ResultSet rs) throws Exception {
   	return getListIterator(rs);
   }

   
	public Document getDocumentForView(ResultSet resultSet) throws Exception {
		return getDocument(resultSet);
		
	}
	public DocumentFragment getDocumentFragmentForView(ResultSet resultSet) throws Exception {
		return getDocumentFragment(resultSet);		
	}
	
	public ArrayList getDocumentFragmentArrayListForView(ResultSet resultSet) throws Exception {
		  return getDocumentFragmentArrayList(resultSet);
	}
//Marco end

	public static BusinessObjectManager getBusinessObjectManager(Class businessObjectClass) throws Exception {
		Method getInstanceMethod = (Method)getInstanceMethods.get(businessObjectClass);
		if (getInstanceMethod == null) {
			if (businessObjectClass == null)
				throw new IllegalArgumentException("class supplied as parameter is null!");
			/* Next check never valid for Vegas in combination with XDE   	
			if (!BusinessObject.class.isAssignableFrom(businessObjectClass))
				throw new IllegalArgumentException("class supplied as parameter does not implement BusinessObject!");*/
			String name = getNameBusinessObjectManagerClass(businessObjectClass);
			Class bomClass = businessObjectClass.getClassLoader().loadClass(name);
			getInstanceMethod = bomClass.getDeclaredMethod("getInstance", (Class[]) null);
			getInstanceMethods.put(businessObjectClass,getInstanceMethod);
		}
		return (BusinessObjectManager) getInstanceMethod.invoke(null, (Object[]) null);
	}

	private static String getNameBusinessObjectManagerClass(Class _class) throws Exception {
		String prefix = "impl.";
		String postFix = "Manager_Impl";
		String fqClassName = _class.getName(); // fully Qualified className

		String packageName = "";
		String className = "";
		int i = fqClassName.lastIndexOf('.');
		if (i < 0)
			className = fqClassName;
		else {
			packageName = fqClassName.substring(0, i + 1);
			className = fqClassName.substring(i + 1);
		}
		if (!packageName.endsWith(prefix))
			packageName += prefix; // append impl. to package name 
		if (className.endsWith("_Impl"))
			className = className.substring(0, className.lastIndexOf("_Impl")); // remove _Impl from classname

		return packageName + className + postFix;
	}


}


