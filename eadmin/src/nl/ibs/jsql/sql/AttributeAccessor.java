package nl.ibs.jsql.sql;


import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class provides methods that allow you to change attribute values in a class even if they are declared as private!
 * ATTENTION Use these methods with extreme care!!!! 
 * Abuse can cause database/ object corruption or some other unwanted
 * sideeffects
 * This class iwas originally a copy of nl.ibs.util.AttributeChanger
 * @author Huub Cleutjens
 * @version 0.2
 */

public class AttributeAccessor implements Serializable {
	private static final long serialVersionUID = 1L;
	private Class clss;
	private transient HashMap fields = null;
	private transient HashMap classes = null;
	/**
	* Constructs an Attribute accessor that can change attributes just for the class in this constructor.
	* Getting and setting attributes using this constructor is faster than using the default constructor.
	* @param aClass the class for which the internal attribute cache is build.  
	* */

	public AttributeAccessor(Class aClass) {
		this.clss = aClass;
	}

	/**
	 * Constructs an Attribute accessor that can change attributes on ANY Object that you pass to the methods.
	 * Getting and setting attributes using this constructor is slower that the specialized constructor.
	  */

	public AttributeAccessor() {
		
	}

	/**
	 * gets the attribute value using reflection and introspection.
	 * @param object the object on which the attribute has to be retrieved.
	 * @param attributeName the name of the attribute whose value you want to get 
	 * @param value will contain the returned attribute value (instead of returning the value as return value (it sound confusing, i know!). 
	 * @throws Exception thrown if attribute was not found or if a security problem arose.
	 */
	public int getIntAttribute(Object object, String attributeName) throws Exception {
		return getField(object.getClass(), attributeName).getInt(object);
	}

	/**
	* gets the attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be retrieved.
	* @param attributeName the name of the attribute whose value you want to get 
	* @return the requested attribute value. 
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public Object getObjectAttribute(Object object, String attributeName) throws Exception {
		return getField(object.getClass(), attributeName).get(object);
	}
	/**
	* gets the attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be retrieved.
	* @param attributeName the name of the attribute whose value you want to get 
	* @return the requested attribute value. 
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public boolean getBooleanAttribute(Object object, String attributeName) throws Exception {
		return getField(object.getClass(), attributeName).getBoolean(object);
	}
	/**
	* gets the attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be retrieved.
	* @param attributeName the name of the attribute whose value you want to get 
	* @return the requested attribute value. 
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public byte getByteAttribute(Object object, String attributeName) throws Exception {
		return getField(object.getClass(), attributeName).getByte(object);
	}
	/**
	* gets the attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be retrieved.
	* @param attributeName the name of the attribute whose value you want to get 
	* @return the requested attribute value. 
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public char getCharAttribute(Object object, String attributeName) throws Exception {
		return getField(object.getClass(), attributeName).getChar(object);
	}
	/**
	* gets the attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be retrieved.
	* @param attributeName the name of the attribute whose value you want to get 
	* @return the requested attribute value. 
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public double getDoubleAttribute(Object object, String attributeName) throws Exception {
		return getField(object.getClass(), attributeName).getDouble(object);
	}
	/**
	* gets the attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be retrieved.
	* @param attributeName the name of the attribute whose value you want to get 
	* @return the requested attribute value. 
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public float getFloatAttribute(Object object, String attributeName) throws Exception {
		return getField(object.getClass(), attributeName).getFloat(object);
	}
	/**
	* gets the attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be retrieved.
	* @param attributeName the name of the attribute whose value you want to get 
	* @return the requested attribute value. 
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public long getLongAttribute(Object object, String attributeName) throws Exception {
		return getField(object.getClass(), attributeName).getLong(object);
	}
	/**
	* gets the attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be retrieved.
	* @param attributeName the name of the attribute whose value you want to get 
	* @return the requested attribute value. 
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public short getShortAttribute(Object object, String attributeName) throws Exception {
		return getField(object.getClass(), attributeName).getShort(object);
	}

	/**
	* sets an attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void setAttribute(Object object, String attributeName, int value) throws Exception {
		getField(object.getClass(), attributeName).setInt(object, value);
	}

	/**
	*  sets an attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void setAttribute(Object object, String attributeName, Object value) throws Exception {
		getField(object.getClass(), attributeName).set(object, value);
	}

	/**
	* sets an attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void setAttribute(Object object, String attributeName, boolean value) throws Exception {
		getField(object.getClass(), attributeName).setBoolean(object, value);
	}
	/**
	* sets an attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void setAttribute(Object object, String attributeName, byte value) throws Exception {
		getField(object.getClass(), attributeName).setByte(object, value);
	}
	/**
	* sets an attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void setAttribute(Object object, String attributeName, char value) throws Exception {
		getField(object.getClass(), attributeName).setChar(object, value);
	}
	/**
	* sets an attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void setAttribute(Object object, String attributeName, double value) throws Exception {
		getField(object.getClass(), attributeName).setDouble(object, value);
	}
	/**
	* sets an attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void setAttribute(Object object, String attributeName, float value) throws Exception {
		getField(object.getClass(), attributeName).setFloat(object, value);
	}
	/**
	* sets an attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void setAttribute(Object object, String attributeName, long value) throws Exception {
		getField(object.getClass(), attributeName).setLong(object, value);
	}
	/**
	* sets an attribute value using reflection and introspection.
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void setAttribute(Object object, String attributeName, short value) throws Exception {
		getField(object.getClass(), attributeName).setShort(object, value);
	}
	
	
	public HashMap getFields() {
		if (clss == null)
			return null;
		if (fields == null)
			fields = collectFields(clss, new HashMap());
		return fields;
	}

	private Field getField(Class theClass, String attributeName) throws Exception {
		if (clss != null)
			return getFieldFromCache(attributeName);
		return getFieldFromObject(theClass, attributeName);
	}

	/**
	 * This method gets the field from the class that corresponds with the passed attribute name using reflection.
	 * This field may reside in the inheritance structure of the class.
	 * IF the field was not found or a security problem was detected, an Exception is thrown.
	 * @param theClass the class that contains the field.
	 * @param attributeName the attribute name of the field.
	 * @return Field the field you were looking for.
	 * @throws Exception thrown if no field was found with specified name or security problems arose during lookup.
	 */
	private Field getFieldFromObject(Class theClass, String attributeName) throws Exception {
		Field field = (Field) getFieldMap(theClass).get(attributeName);
		if (field == null)
			throw new Exception("Atribute " + attributeName + " not found in class " + theClass + "!");
		return field;
	}

	/**
	 * This method gets the map with fields from the passes class.
	 * The fields in the map include fields in the inheritance structure of the class.
	 * @param theClass the class that contains the field.
	 * @return Map the map with fields you were looking for.
	 */
	private Map getFieldMap(Class theClass) {
		HashMap fields=null;
		if (classes == null)
			classes = new HashMap();
		else
			fields = (HashMap) classes.get(theClass);
		if (fields == null) {
			fields = collectFields(theClass, new HashMap());
			classes.put(theClass, fields);
		}
		return fields;
	}

	/**
	* This method gets the field from the internal cached class fields that corresponds with the passed attribute name using reflection.
	* IF the field was not found an Exception is thrown.
	* @param attributeName the attribute name of the field.
	* @return Field the field you were looking for.
	* @throws Exception thrown if no field was found with specified name.
	*/
	private Field getFieldFromCache(String attributeName) throws Exception {
		//System.out.println("inspecting "+theClass.toString());
		Field field = clss==null?null:(Field) getFields().get(attributeName);
		if (field == null)
			throw new Exception("Atribute " + attributeName + " not found in cache!");
		return field;
	}

	/**
	* This method builds a hashtable that contains all attribute Filed definitions of the passes class.
	* If a class inheritance structure contains duplicate attribute names, just the first is inculde in the hastable! 
	* @param theClass the class for which the Fields hashtable is must be build
	* @return a Hashtable containing all Field definitions. each Filed is uniquely identified by its attribute name.
	* @throws Exception thrown if security problems arose during lookup.
	*/
	private HashMap collectFields(Class theClass, HashMap fields) {
		if (theClass == null)
			return fields;
		//System.out.println("inspecting " + theClass.toString());

		Field[] fArray = theClass.getDeclaredFields();
		for (int i = 0; i < fArray.length; i++) {
			Field f = fArray[i];
			f.setAccessible(true);
			if (!fields.containsKey(f.getName()))
				fields.put(f.getName(), f);
		}
		return collectFields(theClass.getSuperclass(), fields);
	}

	/**
	* set an attribute value in a forced manner using reflection and introspection.
	* First searches for a field on an object with the same name with same case. If not
	* found try's to find a field with the same name ignoring case. If not found either
	* returns or throws exception depending on the value of  attribute ignoreFieldsNotFound.
	* If field found method wil set field with value trying to convert to proper type if needed.
	* 
	* @param object the object on which the attribute has to be set.
	* @param attributeName the name of the attribute you want to change 
	* @param value the new value for the attribute
	* @param ignoreFieldsNotFound if false wil throw an exception if attribute not found on object.
	* @throws Exception thrown if attribute was not found or if a security problem arose.
	*/
	public void forceAttribute(Object object, String attributeName, Object value, boolean ignoreFieldsNotFound) throws Exception {
		Class theClass = object.getClass();
		Map fieldMap = clss != null ? getFields() : getFieldMap(theClass);
		Field field = (Field) fieldMap.get(attributeName);
		if (field == null) {
			Iterator names = fieldMap.keySet().iterator();
			while (names.hasNext()) {
				String name = (String) names.next();
				if (attributeName.equalsIgnoreCase(name)) {
					field = (Field) fieldMap.get(name);
					break;
				}
			}
		}

		if (field == null) {
			if (ignoreFieldsNotFound)
				return;
			else
				throw new Exception("Could not find field " + attributeName + " on " + theClass.getName());
		}

		set(object, value, field);

	}

	private static void set(Object object, Object value, Field field) throws IllegalAccessException, Exception {
		if (value == null) {
			field.set(object, value);
			return;
		}
		Class fieldType = field.getType();
		Class valueType = value.getClass();

		if (fieldType.isAssignableFrom(valueType)) {
			field.set(object, value);
		} else if ((fieldType == boolean.class && value.getClass() == Boolean.class)
				|| (fieldType == int.class && valueType == Integer.class)
				|| (fieldType == short.class && value.getClass() == Short.class)
				|| (fieldType == long.class && value.getClass() == Long.class)
				|| (fieldType == double.class && value.getClass() == Double.class)
				|| (fieldType == float.class && value.getClass() == Float.class)) {
			field.set(object, value);
		} else {
			if (String.class.isAssignableFrom(fieldType)) {
				field.set(object, String.valueOf(value));
			} else if (valueType == String.class) {
				String st = (String) value;
				if (st != null && (st=st.trim()).endsWith("-"))
					st = "-" + st.substring(0,st.length()-1);
				if (fieldType == Integer.class || fieldType == int.class)
					field.set(object, new Integer(st==null||(st=st.trim()).length()==0?"0":st));
				else if (fieldType == Short.class || fieldType == short.class)
					field.set(object, new Short(st==null||(st=st.trim()).length()==0?"0":st));
				else if (fieldType == Long.class || fieldType == long.class)
					field.set(object, new Long(st==null||(st=st.trim()).length()==0?"0":st));
				else if (fieldType == Double.class || fieldType == double.class)
					field.set(object, new Double(st==null||(st=st.trim()).length()==0?"0":st));
				else if (fieldType == Float.class || fieldType == float.class)
					field.set(object, new Float(st==null||(st=st.trim()).length()==0?"0":st));
				else if (fieldType == BigInteger.class)
					field.set(object, new BigInteger(st==null||(st=st.trim()).length()==0?"0":st));
				else if (fieldType == BigDecimal.class)
					field.set(object, new BigDecimal(st==null||(st=st.trim()).length()==0?"0":st));
				else
					throw new Exception("Could not convert from " + valueType.getName() + " to " + fieldType.getName());
			} else if (BigInteger.class.isAssignableFrom(valueType)) {
				BigInteger bi = (BigInteger) value;
				if (fieldType == Integer.class || fieldType == int.class)
					field.set(object, new Integer(bi.intValue()));
				else if (fieldType == Short.class || fieldType == short.class)
					field.set(object, new Short(bi.shortValue()));
				else if (fieldType == Long.class || fieldType == long.class)
					field.set(object, new Long(bi.longValue()));
				else if (fieldType == Double.class || fieldType == double.class)
					field.set(object, new Double(bi.doubleValue()));
				else if (fieldType == Float.class || fieldType == float.class)
					field.set(object, new Float(bi.floatValue()));
				else
					throw new Exception("Could not convert from " + valueType.getName() + " to " + fieldType.getName());
			} else if (BigDecimal.class.isAssignableFrom(valueType)) {
				BigDecimal bd = (BigDecimal) value;
				if (fieldType == Integer.class || fieldType == int.class)
					field.set(object, new Integer(bd.intValue()));
				else if (fieldType == Short.class || fieldType == short.class)
					field.set(object, new Short(bd.shortValue()));
				else if (fieldType == Long.class || fieldType == long.class)
					field.set(object, new Long(bd.longValue()));
				else if (fieldType == Double.class || fieldType == double.class)
					field.set(object, new Double(bd.doubleValue()));
				else if (fieldType == Float.class || fieldType == float.class)
					field.set(object, new Float(bd.floatValue()));
				else
					throw new Exception("Could not convert from " + valueType.getName() + " to " + fieldType.getName());
			} else if (Integer.class.isAssignableFrom(valueType)) {
				Integer i = (Integer) value;
				if (fieldType == BigDecimal.class)
					field.set(object, new BigDecimal(i.intValue()));
				else if (fieldType == BigInteger.class)
					field.set(object, new BigInteger(String.valueOf(i.intValue())));
				else if (fieldType == Short.class || fieldType == short.class)
					field.set(object, new Short(i.shortValue()));
				else if (fieldType == Long.class || fieldType == long.class)
					field.set(object, new Long(i.longValue()));
				else if (fieldType == Double.class || fieldType == double.class)
					field.set(object, new Double(i.doubleValue()));
				else if (fieldType == Float.class || fieldType == float.class)
					field.set(object, new Float(i.floatValue()));
				else
					throw new Exception("Could not convert from " + valueType.getName() + " to " + fieldType.getName());
			} else if (Short.class.isAssignableFrom(valueType)) {
				Short s = (Short) value;
				if (fieldType == BigDecimal.class)
					field.set(object, new BigDecimal(s.shortValue()));
				else if (fieldType == BigInteger.class)
					field.set(object, new BigInteger(String.valueOf(s.shortValue())));
				else if (fieldType == Integer.class || fieldType == int.class)
					field.set(object, new Integer(s.intValue()));
				else if (fieldType == Long.class || fieldType == long.class)
					field.set(object, new Long(s.longValue()));
				else if (fieldType == Double.class || fieldType == double.class)
					field.set(object, new Double(s.doubleValue()));
				else if (fieldType == Float.class || fieldType == float.class)
					field.set(object, new Float(s.floatValue()));
				else
					throw new Exception("Could not convert from " + valueType.getName() + " to " + fieldType.getName());
			} else if (Long.class.isAssignableFrom(valueType)) {
				Long l = (Long) value;
				if (fieldType == BigDecimal.class)
					field.set(object, new BigDecimal(l.longValue()));
				else if (fieldType == BigInteger.class)
					field.set(object, new BigInteger(String.valueOf(l.longValue())));
				else if (fieldType == Short.class || fieldType == short.class)
					field.set(object, new Short(l.shortValue()));
				else if (fieldType == Integer.class || fieldType == int.class)
					field.set(object, new Integer(l.intValue()));
				else if (fieldType == Double.class || fieldType == double.class)
					field.set(object, new Double(l.doubleValue()));
				else if (fieldType == Float.class || fieldType == float.class)
					field.set(object, new Float(l.floatValue()));
				else
					throw new Exception("Could not convert from " + valueType.getName() + " to " + fieldType.getName());
			} else if (Double.class.isAssignableFrom(valueType)) {
				Double d = (Double) value;
				if (fieldType == BigDecimal.class)
					field.set(object, new BigDecimal(d.doubleValue()));
				else if (fieldType == BigInteger.class)
					field.set(object, new BigInteger(String.valueOf(d.doubleValue())));
				else if (fieldType == Short.class || fieldType == short.class)
					field.set(object, new Short(d.shortValue()));
				else if (fieldType == Long.class || fieldType == long.class)
					field.set(object, new Long(d.longValue()));
				else if (fieldType == Integer.class || fieldType == int.class)
					field.set(object, new Integer(d.intValue()));
				else if (fieldType == Float.class || fieldType == float.class)
					field.set(object, new Float(d.floatValue()));
				else
					throw new Exception("Could not convert from " + valueType.getName() + " to " + fieldType.getName());
			} else if (Float.class.isAssignableFrom(valueType)) {
				Float f = (Float) value;
				if (fieldType == BigDecimal.class)
					field.set(object, new BigDecimal(f.floatValue()));
				else if (fieldType == BigInteger.class)
					field.set(object, new BigInteger(String.valueOf(f.floatValue())));
				else if (fieldType == Short.class || fieldType == short.class)
					field.set(object, new Short(f.shortValue()));
				else if (fieldType == Long.class || fieldType == long.class)
					field.set(object, new Long(f.longValue()));
				else if (fieldType == Double.class || fieldType == double.class)
					field.set(object, new Double(f.doubleValue()));
				else if (fieldType == Integer.class || fieldType == int.class)
					field.set(object, new Integer(f.intValue()));
				else
					throw new Exception("Could not convert from " + valueType.getName() + " to " + fieldType.getName());
			} else {
				throw new Exception("Could not convert from " + valueType.getName() + " to " + fieldType.getName());
			}
		}
	}
	
}


