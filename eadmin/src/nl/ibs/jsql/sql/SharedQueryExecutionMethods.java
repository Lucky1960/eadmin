package nl.ibs.jsql.sql;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ListIterator;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;


public interface SharedQueryExecutionMethods extends Serializable {

	/**
	 * Returns first Object or null if no BusinessObject satisfied filter conditions.
	 *
	 * @return BussinesObject (null if no Businessobject satisfied filter conditions).
	 */
	public Object getFirstObject() throws Exception;

	/**
	 * Returns Collection of BusinessObjects.
	 *
	 * @return Colleciton of BusinessObjects (empty if no Businessobject satisfy filter conditions).
	 */
	public ListIterator getListIterator() throws Exception;

	/**
	 * Returns ListIterator of BusinessObjects.
	 *
	 * @return ListIterator of BusinessObjects (empty if no Businessobject satisfy filter conditions).
	 */
	public Collection getCollection() throws Exception;
   
   
	/**
	 * Returns XML representation of BusinessObject collection as Document.
	 *
	 * @return Document XML representation of BusinessObject collection.
	 */
	public Document getDocument() throws Exception;
   
	/**
	 * Returns XML representation of BusinessObject collection as a DocumentFragment.
	 *
	 * @return DocumentFragment XML representation of BusinessObject collection.
	 */
	public DocumentFragment getDocumentFragment() throws Exception;
   
	/**
	 * Returns XML representation of BusinessObject collection as an ArrayList of DocumentFragments.
	 *
	 * @return Document xml representation of BusinessObject collection as an ArrayList of DocumentFragments, where every DocumentFragment represents one
	 * BusinessObject.
	 */
	public ArrayList getDocumentFragmentArrayList() throws Exception;
   
	/**
	 * Returns a subset of the full BusinessObject resultset as a ListIterator.
	 * The subset begins at the specified <code>beginIndex</code> and
	 * extends to the index <code>endIndex - 1</code>.<br/>
	 * If both beginIndex and endIndex are 0 then full result is returned.<br/>
	 * If endIndex exceeds resultset size then everything from beginIndex and thereafter is returned.<br/>
	 * Thus if endIndex > 0 the maximum size of the ArrayList is <code>endIndex-beginIndex</code>.
	 * <p>
	 * Examples:
	 * <blockquote><pre>
	 * Thus if the full result collection is represented by A,B,C,D,E,F then
	 * getListIterator(1, 3) returns a result representing B,C
	 * getListIterator(0, 5) returns a result representing A,B,C,D,E
	 * getListIterator(5, 10) returns a result representing F
	 * getListIterator(0, 0) returns a result representing A,B,C,D,E,F
	 * </pre></blockquote>
	 *
	 * @param      beginIndex   the beginning index, inclusive.
	 * @param      endIndex     the ending index, exclusive.
	 * @return     the ListIterator representing the specified subset.
	 * @exception  IllegalArgumentException if the
	 *             <code>beginIndex</code> is negative,or
	 *             <code>endIndex</code> is negative,or
	 *             <code>beginIndex</code> is larger than
	 *             <code>endIndex</code>.
	 */
	public ListIterator getListIterator(int beginIndex, int endIndex) throws Exception;

	/**
	 * Returns a subset of the full BusinessObject resultset as a Collection.
	 * The subset begins at the specified <code>beginIndex</code> and
	 * extends to the index <code>endIndex - 1</code>.<br/>
	 * If both beginIndex and endIndex are 0 then full result is returned.<br/>
	 * If endIndex exceeds resultset size then everything from beginIndex and thereafter is returned.<br/>
	 * Thus if endIndex > 0 the maximum size of the ArrayList is <code>endIndex-beginIndex</code>.
	 * <p>
	 * Examples:
	 * <blockquote><pre>
	 * Thus if the full result collection is represented by A,B,C,D,E,F then
	 * getCollection(1, 3) returns a result representing B,C
	 * getCollection(0, 5) returns a result representing A,B,C,D,E
	 * getCollection(5, 10) returns a result representing F
	 * getCollection(0,0) returns a result representing A,B,C,D,E,F
	 * </pre></blockquote>
	 *
	 * @param      beginIndex   the beginning index, inclusive.
	 * @param      endIndex     the ending index, exclusive.
	 * @return     the Collection representing the specified subset.
	 * @exception  IllegalArgumentException if the
	 *             <code>beginIndex</code> is negative,or
	 *             <code>endIndex</code> is negative,or
	 *             <code>beginIndex</code> is larger than
	 *             <code>endIndex</code>.
	 */
	public Collection getCollection(int beginIndex, int endIndex) throws Exception;
   
   
	/**
	 * Returns a subset of the full BusinessObject resultset as a Document.
	 * The subcollection begins at the specified <code>beginIndex</code> and
	 * extends to the index <code>endIndex - 1</code>.<br/>
	 * If both beginIndex and endIndex are 0 then full result is returned.<br/>
	 * If endIndex exceeds subset size then everything from beginIndex and thereafter is returned.<br/>
	 * Thus if endIndex > 0 the maximum size of the ArrayList is <code>endIndex-beginIndex</code>.
	 * <p>
	 * Examples:
	 * <blockquote><pre>
	 * Thus if the full result collection is represented by A,B,C,D,E,F then
	 * getDocument(1, 3) returns a result representing B,C
	 * getDocument(0, 5) returns a result representing A,B,C,D,E
	 * getDocument(5, 10) returns a result representing F
	 * getDocument(0,0) returns a result representing A,B,C,D,E,F
	 * </pre></blockquote>
	 *
	 * @param      beginIndex   the beginning index, inclusive.
	 * @param      endIndex     the ending index, exclusive.
	 * @return     the Document representing the specified subset.
	 * @exception  IllegalArgumentException if the
	 *             <code>beginIndex</code> is negative,or
	 *             <code>endIndex</code> is negative,or
	 *             <code>beginIndex</code> is larger than
	 *             <code>endIndex</code>.
	 */
	public Document getDocument(int beginIndex, int endIndex) throws Exception;
   
	/**
	 * Returns a subset of the full BusinessObject resultset as a DocumentFragment.
	 * The subcollection begins at the specified <code>beginIndex</code> and
	 * extends to the index <code>endIndex - 1</code>.<br/>
	 * If both beginIndex and endIndex are 0 then full result is returned.<br/>
	 * If endIndex exceeds subset size then everything from beginIndex and thereafter is returned.<br/>
	 * Thus if endIndex > 0 the maximum size of the ArrayList is <code>endIndex-beginIndex</code>.
	 * <p>
	 * Examples:
	 * <blockquote><pre>
	 * Thus if the full result collection is represented by A,B,C,D,E,F then
	 * getDocumentFragment(1, 3) returns a result representing B,C
	 * getDocumentFragment(0, 5) returns a result representing A,B,C,D,E
	 * getDocumentFragment(5, 10) returns a result representing F
	 * getDocumentFragment(0,0) returns a result representing A,B,C,D,E,F
	 * </pre></blockquote>
	 *
	 * @param      beginIndex   the beginning index, inclusive.
	 * @param      endIndex     the ending index, exclusive.
	 * @return     the DocumentFragment representation of the specified subcollection.
	 * @exception  IllegalArgumentException  if the
	 *             <code>beginIndex</code> is negative,or
	 *             <code>endIndex</code> is negative,or
	 *             <code>beginIndex</code> is larger than
	 *             <code>endIndex</code>.
	 */
	public DocumentFragment getDocumentFragment(int beginIndex, int endIndex) throws Exception;
   
	/**
	 * Returns a subset of the full BusinessObject resultset as a ArrayList of DocumentFragments.
	 * The subcollection begins at the specified <code>beginIndex</code> and
	 * extends to the index <code>endIndex - 1</code>.<br/>
	 * If both beginIndex and endIndex are 0 then full result is returned.<br/>
	 * If endIndex exceeds subset size then everything from beginIndex and thereafter is returned.<br/>
	 * Thus if endIndex > 0 the maximum size of the ArrayList is <code>endIndex-beginIndex</code>.
	 * <p>
	 * Examples:
	 * <blockquote><pre>
	 * Thus if the full collection is represented by A,B,C,D,E,F then
	 * getDocumentFragmentArrayList(1, 3) returns a result representing B,C
	 * getDocumentFragmentArrayList(0, 5) returns a result representing A,B,C,D,E
	 * getDocumentFragmentArrayList(5, 10) returns a result representing F
	 * getDocumentFragmentArrayList(0,0) returns a result representing A,B,C,D,E,F
	 * </pre></blockquote>
	 *
	 * @param      beginIndex   the beginning index, inclusive.
	 * @param      endIndex     the ending index, exclusive.
	 * @return     the ArrayList containing DocumentFragments representing the specified subset.
	 * @exception  IllegalArgumentException  if the
	 *             <code>beginIndex</code> is negative,or
	 *             <code>endIndex</code> is negative,or
	 *             <code>beginIndex</code> is larger than
	 *             <code>endIndex</code>.
	 */
	public ArrayList getDocumentFragmentArrayList(int beginIndex, int endIndex) throws Exception;
	

}

