package nl.ibs.jsql.sql;


import java.sql.*;
import java.io.*;
import org.w3c.dom.*;
import java.util.*;
import nl.ibs.jeelog.*;
import nl.ibs.jsql.*;


/**
 * Deze class should enable us to determine realtime table and table fieldname. But this functionality
 * still has to be added. 
 *
 * @author  c_hc01
 */

public final class ORMapping {

    

    private static HashMap mappings = new HashMap();

    static final ORMapping getInstance(DBData data){
        return getInstance(data.getName());
    }
        
    static final ORMapping getInstance(String name){
      Object mapping = mappings.get(name);
      if(mapping == null){
        mapping = new ORMapping(name); 
        mappings.put(name, mapping);
      }
      return (ORMapping)mapping;
    }
    
    protected ORMapping(String name){
      //Node mapping = null;
    } 

    public final String getTableName(String objectName, String defaultTableName){
        return defaultTableName; // Temperarely dummy implementation!
    }

    public final String getTableFieldName(String variableName , String objectName, String defaultTableFieldName){
        return defaultTableFieldName; // Temperarely dummy implementation!
    }

}

