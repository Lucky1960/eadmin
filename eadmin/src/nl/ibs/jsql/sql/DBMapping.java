package nl.ibs.jsql.sql;


import java.io.Serializable;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import nl.ibs.jeelog.*;

import nl.ibs.jsql.*;


/**
 *
 * @author  c_hc01
 */

public class DBMapping implements Serializable {

    

    private static Document defaultMappings;
    private static Document customMappings;
    private static Map      mappings = new Hashtable();

    private static String[] javaTypes = {"String", "boolean", "Boolean", "short", "Short" , "int", "Integer" , "long", "Long", "double", "Double" , "float", "Float" , "BigInteger", "BigDecimal","Date", "Time", "Timestamp"}; 

    private static HashMap defaultJDBCMappings = new HashMap();
    static { 
        defaultJDBCMappings.put("String", new Integer(Types.VARCHAR));
        defaultJDBCMappings.put("boolean", new Integer(Types.CHAR));
        defaultJDBCMappings.put("Boolean", new Integer(Types.CHAR));
        defaultJDBCMappings.put("short", new Integer(Types.INTEGER));
        defaultJDBCMappings.put("Short", new Integer(Types.INTEGER));
        defaultJDBCMappings.put("int", new Integer(Types.INTEGER));
        defaultJDBCMappings.put("Integer", new Integer(Types.INTEGER));
        defaultJDBCMappings.put("long", new Integer(Types.BIGINT));
        defaultJDBCMappings.put("Long", new Integer(Types.BIGINT));
        defaultJDBCMappings.put("BigInteger", new Integer(Types.BIGINT));
        defaultJDBCMappings.put("double", new Integer(Types.DECIMAL));
        defaultJDBCMappings.put("Double", new Integer(Types.DECIMAL));
        defaultJDBCMappings.put("Float", new Integer(Types.DECIMAL));
        defaultJDBCMappings.put("float", new Integer(Types.DECIMAL));
        defaultJDBCMappings.put("BigDecimal", new Integer(Types.DECIMAL));
        defaultJDBCMappings.put("Date", new Integer(Types.DATE));
        defaultJDBCMappings.put("Time", new Integer(Types.TIME));
        defaultJDBCMappings.put("Timestamp", new Integer(Types.TIMESTAMP));
        defaultJDBCMappings.put("byte[]", new Integer(Types.LONGVARBINARY));
    }

    private static HashMap defaultSQLMappings = new HashMap();
    static { 
        defaultSQLMappings.put("String", "VARCHAR(254)");
        defaultSQLMappings.put("boolean", "CHAR(5)");
        defaultSQLMappings.put("boolean", "CHAR(5)");
        defaultSQLMappings.put("short", "INTEGER(5)");
        defaultSQLMappings.put("Short", "INTEGER(5)");
        defaultSQLMappings.put("int", "INTEGER(9)");
        defaultSQLMappings.put("Integer", "INTEGER(9)");
        defaultSQLMappings.put("long", "INTEGER(19)");
        defaultSQLMappings.put("Long", "INTEGER(19)");
        defaultSQLMappings.put("BigInteger", "INTEGER(23)");
        defaultSQLMappings.put("double", "DECIMAL(15)");
        defaultSQLMappings.put("Double", "DECIMAL(15)");
        defaultSQLMappings.put("float", "DECIMAL(23)");
        defaultSQLMappings.put("Float", "DECIMAL(23)");
        defaultSQLMappings.put("BigDecimal", "DECIMAL(23,0)");
        defaultSQLMappings.put("Date", "DATE");
        defaultSQLMappings.put("Time", "TIME");
        defaultSQLMappings.put("Timestamp", "TIMESTAMP");
        defaultSQLMappings.put("byte[]", "BLOB");
    }

    
    static {
        File defaultmappings =  null ;
        
        try{
        	 Log.info("Search defaultjsql.xml in root classpath");
        	 defaultmappings = new File(DBMapping.class.getClassLoader().getResource("defaultjsql.xml"));
             if (defaultmappings != null && defaultmappings.exists())
             	Log.info("Found defaultjsql.xml in root classpath");
             else
            	 Log.info("No defaultjsql.xml found in root classpath"); 
        }catch (Exception e ){
             Log.info("No defaultjsql.xml found in root classpath!");
        }
        
        if (defaultmappings == null || !defaultmappings.exists()){
            String importPackage =  "nl.ibs.jsql";
            try{
            	Log.info("Search for defaultjsql.xml in package " + importPackage);
            	defaultmappings = new File(DBMapping.class.getClassLoader().getResource(importPackage.replace('.','/') + "/defaultjsql.xml"));
                if (defaultmappings != null && defaultmappings.exists())
                	Log.info("Found defaultjsql.xml in package " + importPackage);
                else
                	Log.info("No defaultjsql.xml found in package " + importPackage);
            }catch (Exception e ){
                Log.warn("Neither found defaultjsql.xml in package " + importPackage+"!");
            }
            
        }

        File custommappings = new File(DBConfig.getCustomMappings());

        if (defaultmappings != null && defaultmappings.exists()){
            try{
            	Log.info("Loading DefaultMappings (\"" + defaultmappings.getPath() + "\")");
                defaultMappings = DBConfig.getDocument(defaultmappings);
                Log.info("DefaultMappings (\"" + defaultmappings.getPath() + "\") loaded");
            }catch(Exception e){
                Log.error(e.getMessage());
                e.printStackTrace();
            }
        }
        
        if (custommappings != null && custommappings.exists()){
            try{
            	Log.info("Loading CustomMappings (\"" + custommappings.getPath() + "\")");
            	customMappings = DBConfig.getDocument(custommappings);
                Log.info("CustomMappings (\"" + custommappings.getPath() + "\") loaded!");
            }catch(Exception e){
                Log.error(e.getMessage());
                e.printStackTrace();
            }
        }
        
        if (defaultMappings == null && customMappings == null){
            Log.error("No mappings found, will use hardcoded default mappings wich are not driver specific!, This might casue serious problems!");
        }
    }
        
    

    private Map jdbcMappings        = new HashMap();
    private Map sqlMappings         = new HashMap();
    private Map instructionMappings = new HashMap();
    private ArrayList instructionList = new ArrayList();
    private boolean commentOnTableSupport = false;
    private boolean commentOnColumnSupport = false;
    private boolean cascadeDeletionSupport = false;
    private boolean cascadeUpdateSupport = false;
    private boolean addConstraintForeignKeyColumns = false;
    private String constraintForeignKeyColumns = "";
    
    static DBMapping getInstance(DBData data){
        return getInstance(data.getDriver());
    } 
    
    static DBMapping getInstance(String driver){
      Object mapping = mappings.get(driver);
      if(mapping == null){
        mapping = new DBMapping(driver); 
        mappings.put(driver, mapping);
      }
      return (DBMapping)mapping;
    }
    
    protected DBMapping(String driver){
      Node mapping = null;
        if (customMappings != null)
          mapping = getTypeMappingForDriver(customMappings, driver);
        if (mapping == null && defaultMappings != null)
          mapping = getTypeMappingForDriver(defaultMappings, driver);
        if (mapping == null && customMappings != null)
          mapping = getTypeMappingForDriver(customMappings, "default");
        if (mapping == null && defaultMappings != null)
          mapping = getTypeMappingForDriver(defaultMappings, "default");    
            
          map(mapping);
            
    } 

	public String convertSQLStatement(String statement) {
		String sql = statement.trim();
		for (int i = 0, z = 0; i < instructionList.size(); i++) {
			String instruction = (String) instructionList.get(i);
			if (instruction.charAt(0) == '^') {
				z = firstOccurenceOfCharacter(instruction, "? ");
				if (sql.startsWith(instruction.substring(1, z < 0 ? instruction.length() : z))) {
					String replacement = (String) instructionMappings.get(instruction);
					if (z < 0) {
						sql = replacement + sql.substring(instruction.length() - 1);
					} else {
						sql = convert(0, sql, instruction.substring(1), replacement, false);
					}
				}
			} else {
				z = firstOccurenceOfCharacter(instruction, "? ");
				int from = sql.indexOf(instruction.substring(0, z < 0 ? instruction.length() : z));
				if (from > -1) {
					String replacement = (String) instructionMappings.get(instruction);
					sql = convert(from, sql, instruction, replacement, true);
				}
			}
		}
		return sql;
	}


	private static String convert(int from, String sql, String instruction, String replacement, boolean repeat) {
		StringTokenizer st = new StringTokenizer(instruction, "? ", true);
		String[] tokens = new String[st.countTokens()];
		int x = 0;
		while (st.hasMoreElements())
			tokens[x++] = st.nextToken();
		return convert(from, sql, tokens, replacement, repeat);
	}

    //TODO HC Unfortunately this method is difficult to comprehend and needs some serieus rethinking, 
    //but as so often time is scarce and I have to move on. 
	private static String convert(int _from, String sql, String[] tokens, String replacement, boolean repeat) {
		int x = 0, from = _from, until = 0;
		HashMap variables = new HashMap();
		boolean valid = true;
		for (x = 0; x < tokens.length && valid; x++) {
			if (tokens[x].equals("?") && x + 1 < tokens.length && tokens[x + 1].equals(" ") == false) {
				until = (x + 2 < tokens.length) ? sql.indexOf(" ", from + 1) : sql.length();
				if (until < 0) {
					from = sql.length();
				} else {
					variables.put(tokens[x++] + tokens[x], sql.substring(from, until));
					from = until;
				}
			} else {
				int end = from + tokens[x].length();
				String fragment = null;
				if (end <= sql.length() && (fragment = sql.substring(from, end)).equalsIgnoreCase(tokens[x])) {
					from = from + tokens[x].length();
					until=end;
				} else {
					valid = false;
					//we are lenient with spaces:
					int counter = 0;
					if (x > 0 && tokens[x - 1].equals(" ")) {
						while (!valid
							&& fragment != null
							&& fragment.length() > 0
							&& fragment.charAt(0) == ' '
							&& (end + (++counter)) < sql.length())
							if ((fragment = sql.substring(from + counter, end + counter)).equalsIgnoreCase(tokens[x])) {
								valid = true;
								from = end + counter;
							}
					}
				}
			}
		}
		if (valid == true) {
			String insert = replacement;
			if (variables.size() > 0) {
				Iterator it = variables.keySet().iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					String value = (String) variables.get(key);
					int i = 0;
					while (i < insert.length() && (i = insert.indexOf(key, i)) >= 0)
						insert = insert.substring(0, i) + value + insert.substring(i + key.length());
				}
			}
			sql = sql.substring(0, _from) + insert + sql.substring(until);
		}

		int i;
		if (repeat && (i = _from + replacement.length()) < sql.length()) {
			from = sql.indexOf(tokens[0], i);
			if (from > 0)
				sql = convert(from, sql, tokens, replacement, repeat);
		}
		return sql;
	}
	
	private static int firstOccurenceOfCharacter(String string, String characters) {
		for (int i = 0; i < string.length(); i++) {
			for (int j = 0; j < characters.length(); j++) {
				if (string.charAt(i) == characters.charAt(j))
					return i;
			}
		}
		return -1;
	}

	protected void addInstructionMapping(String key, String value) {
		if (key == null || key.trim().length() == 0 || (key.trim().length()==1 && key.charAt(0) == '^'))
			return;
		instructionMappings.put(key.trim(), value);
		instructionList.add(key.trim());
	}

    public int getJDBCTypeFor(String javaType){
        return  ((Integer)jdbcMappings.get(javaType)).intValue();
    }

    public boolean instructionCommentOnTableSupported(){
        return  commentOnTableSupport;
    }

    public boolean instructionCommentOnColumnSupported(){
        return  commentOnColumnSupport;
    }
    
    public boolean cascadeUpdateSupport(){
        return  cascadeUpdateSupport;
    }

    public boolean cascadeDeletionSupport(){
        return  cascadeDeletionSupport;
    }

    public boolean addConstraintForeignKeyColumns(){
        return !constraintForeignKeyColumns.equals("");
    }

    public String getConstraintForeignKeyColumns(){
        return constraintForeignKeyColumns;
    }

    public String getSQLDefinition(String javaType){
        return  getSQLDefinition(javaType, 0 , 0);
    }
    
    public String getSQLDefinition(String javaType, int length){
        return getSQLDefinition(javaType, length , 0);
    }
    
    public String getSQLDefinition(String javaType, int length, int scale){
        
        String definition = null;
        
        try{
            
            definition = (String)sqlMappings.get(javaType.trim());
                
                if (length != 0 || scale != 0){
                    String stripped_definition = null;
                    String definition_extension = "";
                        String sLength = null, sScale = null;
                        if (definition.indexOf('(') > -1 ){
                            stripped_definition = definition.substring(0,definition.indexOf('('));
                            String ls = definition.substring(definition.indexOf('(')+1, definition.indexOf(')'));
                            definition_extension = definition.substring(definition.indexOf(')')+1);
                            if (ls.indexOf(',') == -1){
                                sLength = ls.trim();
                            }else{
                                sLength = ls.substring(0,ls.indexOf(',')).trim();
                                sScale = ls.substring(ls.indexOf(',')+1).trim();
                            }
                        }
                        if (length != 0) sLength = Integer.toString(length);
                        if (scale != 0) sScale = Integer.toString(scale);
                        definition = stripped_definition;
                        if (sLength != null){
                            definition = definition + "(" + sLength;
                            if (sScale != null) definition = definition + "," + sScale;
    						definition += ")";
                        }
                        definition += definition_extension;
                }
                
        }catch (Exception e){
            Log.error(e.getMessage());
            e.printStackTrace();
        }
        return definition;
    }
    
    private void map(Node node){
        if (node != null){ 
          NodeList nl = node.getChildNodes();
          for (int x = 0 ; x < nl.getLength() ; x ++){
            if (nl.item(x).getNodeType() == Node.ELEMENT_NODE){
            Element mapping = (Element)nl.item(x);      
              if (mapping.getNodeName().equals("type-mapping")){
                  String javaType = mapping.getAttribute("javatype");
                  Log.info("(Re)map javaType " + javaType + " to JDBCType " + mapping.getAttribute("jdbctype")  + " and SQLType " + mapping.getAttribute("sqltype"));
                  String sqlType = mapping.getAttribute("sqltype");
                  if (sqlType != null && !sqlType.equals("")){
                      sqlMappings.put(javaType, sqlType);
                  }
                  String jdbcType = mapping.getAttribute("jdbctype");
                  if (jdbcType != null && !jdbcType.equals("")){
                      int type = translateToJDBCType(jdbcType);
                      jdbcMappings.put(javaType, new Integer(type));
                  }
              }else if (mapping.getNodeName().equals("instruction-mapping")){
                  String from = mapping.getAttribute("instruction");
                  String to   = mapping.getAttribute("replacement");
                  Log.info("(Re)map instruction " + from + " with " + to);
                  addInstructionMapping(from , to);
              }else if (mapping.getNodeName().equals("non-standard-sql-instructions-support")){
                  commentOnTableSupport = mapping.getAttribute("comment-on-table").equals("true");
                  commentOnColumnSupport = mapping.getAttribute("comment-on-column").equals("true");
                  cascadeDeletionSupport = mapping.getAttribute("cascade-deletion").equals("true");  
                  cascadeUpdateSupport = mapping.getAttribute("cascade-update").equals("true");
                  constraintForeignKeyColumns = mapping.getAttribute("constraint-on-foreignkey-columns");  
              }
            }  
          }
        }

        // Complete mappings with defaults
        for (int x = 0 ; x < javaTypes.length ; x++){
            if (!jdbcMappings.containsKey(javaTypes[x]))
                jdbcMappings.put( javaTypes[x] , defaultJDBCMappings.get(javaTypes[x]) ); 
            if (!sqlMappings.containsKey(javaTypes[x]))
                sqlMappings.put( javaTypes[x]  , defaultSQLMappings.get(javaTypes[x]) ); 
        } 
    }
    
    private static int translateToJDBCType(String type){
        if (type.equals("ARRAY")){
            return java.sql.Types.ARRAY;
        } else  if (type.equals("BIGINT")){
            return java.sql.Types.BIGINT;
        } else  if (type.equals("BINARY")){
            return java.sql.Types.BINARY;
        } else  if (type.equals("BIT")){
            return java.sql.Types.BIT;
        } else  if (type.equals("BLOB")){
            return java.sql.Types.BLOB;
        } else  if (type.equals("BOOLEAN")){
            return java.sql.Types.BOOLEAN;
        } else  if (type.equals("CHAR")){
            return java.sql.Types.CHAR;
        } else  if (type.equals("CLOB")){
            return java.sql.Types.CLOB;
        } else  if (type.equals("DATE")){
            return java.sql.Types.DATE;
        } else  if (type.equals("DECIMAL")){
            return java.sql.Types.DECIMAL;
        } else  if (type.equals("DISTINCT")){
            return java.sql.Types.DISTINCT;
        } else  if (type.equals("DOUBLE")){
            return java.sql.Types.DOUBLE;
        } else  if (type.equals("FLOAT")){
            return java.sql.Types.FLOAT;
        } else  if (type.equals("INTEGER")){
            return java.sql.Types.INTEGER;
        } else  if (type.equals("JAVA_OBJECT")){
            return java.sql.Types.JAVA_OBJECT;
        } else  if (type.equals("LONGVARBINARY")){
            return java.sql.Types.LONGVARBINARY;
        } else  if (type.equals("LONGVARCHAR")){
            return java.sql.Types.LONGVARCHAR;
        } else  if (type.equals("NULL")){
            return java.sql.Types.NULL;
        } else  if (type.equals("NUMERIC")){
            return java.sql.Types.NUMERIC;
        } else  if (type.equals("OTHER")){
            return java.sql.Types.OTHER;
        } else  if (type.equals("REAL")){
            return java.sql.Types.REAL;
        } else  if (type.equals("REF")){
            return java.sql.Types.REF;
        } else  if (type.equals("SMALLINT")){
            return java.sql.Types.SMALLINT;
        } else  if (type.equals("STRUCT")){
            return java.sql.Types.STRUCT;
        } else  if (type.equals("TIME")){
            return java.sql.Types.TIME;
        } else  if (type.equals("TIMESTAMP")){
            return java.sql.Types.TIMESTAMP;
        } else  if (type.equals("VARBINARY")){
            return java.sql.Types.VARBINARY;
        } else  if (type.equals("VARCHAR")){
            return java.sql.Types.VARCHAR;
        }
        Log.error("Problem: JDBCType " + type + " is not a valid type! Correct your mapping file!");
        return Integer.MIN_VALUE;
    }
    
    private static Node getTypeMappingForDriver(Document doc, String driver){
        Node node = null;
        NodeList nl = doc.getDocumentElement().getElementsByTagName("driver-mapping");
        for (int x = 0 ; x < nl.getLength() ; x ++){
            if (((Element)nl.item(x)).getAttribute("driver").equals(driver)){
                node = nl.item(x);
                break;
            }
        }
        return node;
    }
    
    final static class File extends java.io.File{

		public File(URL resource) throws Exception {
			super(URLDecoder.decode(resource.getFile(),"UTF-8"));
		}

		public File(String path) {
			super(path);
		}
    }
    
 /*   
     public static void main(String[] args) {
     
     System.out.println("Fields: " + BusinessObject.class.getDeclaredFields().length);    
         
     System.out.println("String: " + getSQLDefinition("String"));
     System.out.println("String 5: " + getSQLDefinition("String",5));
     System.out.println("boolean " + getSQLDefinition("boolean"));
     System.out.println("int " + getSQLDefinition("int"));
     System.out.println("int 5" + getSQLDefinition("int"));
     System.out.println("long " + getSQLDefinition("long"));
     System.out.println("long 14" + getSQLDefinition("long ", 14));
     System.out.println("BigInteger " + getSQLDefinition("BigInteger"));
     System.out.println("BigDecimal 15 2 " + getSQLDefinition("BigDecimal", 15 , 2)); 
     }
  */  
}

