package nl.ibs.jsql.sql;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.ReflectPermission;
import java.util.HashMap;
import java.util.Hashtable;


/**
 * This class provides a way to instanciate objects using their private Empty constructor.
 * ATTENTION Use this class with extreme care!!!! 
 * Abuse can cause database/ object corruption or some other unwanted
 * sideeffects
 * @author Jan van de Klok
 * @version 0.1
 */


public class ObjectInstanciator {
 
 
/**
 * cretaes an onject instance of the specified class using its (private) default Constructor.
 * @param theClass the from which youn want an instance
 * @return an instance of the requested class or null. Attention: There's no garantuee that this instance will be in a valid state!!!!
 *          A Null value will be returned if the class has no empty constructor
 * @throws Exception
 */
 public static Object createInstance(Class theClass) throws Exception {
   Constructor[]  c = theClass.getDeclaredConstructors();
   for (int i=0; i< c.length; i++) {
     if (c[i].getParameterTypes().length==0) {
       c[i].setAccessible(true); // make sure we can use private constructors
       return c[i].newInstance(new Object[] {}); // create an instance using the default (private) constructor
     }	
   }	 
   throw new Exception("Instanciation of object "+ theClass.getName() + " failed!");	
 }	
}

