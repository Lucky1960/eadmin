package nl.ibs.jsql.sql;


import nl.ibs.jsql.impl.*;

import nl.ibs.jsql.*;

import java.math.*;
import java.sql.*;
import java.util.*;
import nl.ibs.jeelog.*;

public class DBObjectIDGenerator implements PersistenceMetaData{

    

    private static Hashtable ids = new Hashtable();
    private static Set javaVariableNames = new HashSet();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static Set javaPKVariableNames = new HashSet();
    private static final String AUTO_ID = "AutoID";
    private static final String LOWEST_ID = "LowestID";
    //private static final long INITIAL_ID = Long.MIN_VALUE;
    private static final long INITIAL_ID = 1l;
    private static final String BUSINESSOBJECT_NAME = "BusinessObjectName";
   
    
    
    private static PersistenceMetaData inst = new DBObjectIDGenerator();
    {
        fieldNames.put(BUSINESSOBJECT_NAME,"BONAME");
        fieldNames.put(LOWEST_ID,"HIGHESTID");

        fieldDescriptions.put(BUSINESSOBJECT_NAME,"Key to BusinessObject");
        fieldDescriptions.put(LOWEST_ID,"LowestID that is not yet claimed!");

        javaToBONameMapping.put("name", BUSINESSOBJECT_NAME);
        javaToBONameMapping.put("objectId", LOWEST_ID);
        javaVariableNames.add("name");
        javaVariableNames.add("objectId");
        javaPKVariableNames.add("objectId");
      
    }
    
    
    public static PersistenceMetaData getInstance(){
        return inst;
    }
    
    public PersistenceMetaData getPersistingPMD(){
    	return inst;
    }
    
    public String getBusinessObjectName(){
        return "DBObjectIDGenerator";
    }
    
    public String getTableName(ConnectionProvider provider){
        return provider.getORMapping().getTableName(AUTO_ID , "AUTOID");
    }
    
    public String getTableName(String objectName, ConnectionProvider provider){
        return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }
    
    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "CD" , (String)fieldNames.get(name));
    }
    
    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
    	return provider.getORMapping().getTableFieldName(fieldName, objectName , fieldName.toUpperCase() );
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }
    
    public String getTableDescription(){
        return "Table contains the lowest free ID for BuinessObjects";
    }
    
    public String getTableDefinition(ConnectionProvider provider){
        return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" +
        getFieldName(BUSINESSOBJECT_NAME, provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 254, 0) + " NOT NULL	," +
        getFieldName(LOWEST_ID, provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 19, 0) + " DEFAULT " + INITIAL_ID + " NOT NULL )");
    }
    
    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    
    public boolean isBackwardReference(String javaVariableNameOfRelation){
    	return false;
    }
    
    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
    	return new HashMap();
    }
    
    
    public String[][] getColumnDefinitions(ConnectionProvider provider){
        String[][] columnDefinitions = new String[2][4];
        columnDefinitions[0]=new String[]{getFieldName(BUSINESSOBJECT_NAME, provider), provider.getDBMapping().getSQLDefinition("String", 254, 0) , "''"  , " NOT NULL", "Key to BusinessObject"};
        columnDefinitions[1]=new String[]{getFieldName(LOWEST_ID, provider), provider.getDBMapping().getSQLDefinition("long", 19, 0) , String.valueOf(INITIAL_ID)  , " NOT NULL", "LowestID that is not yet claimed!"};
        return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
        return new HashMap();
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
        Map map = new HashMap();
        //Remove, but then moved back, provider prefix in next line because it is not correct for MYSQL, though it might be necessary on AS400
        map.put("JSQL_PK_DBOBJECTIDGENERATOR", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_DBOBJECTIDGENERATOR PRIMARY KEY("+getFieldName(BUSINESSOBJECT_NAME, provider)+")");
       	return map;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
        return null;
    }

    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
        return null;
    }
    
    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
    	return null;
    }
    
    public String getRelationType(String javaVariableNameOfRelation) {
		return null;
	}

	public String getRelationMultiplicity(String javaVariableNameOfRelation) {
		return null;
	}

	public String getAssociationTableName(String javaVariableNameOfRelation, ConnectionProvider provider) {
		return null;
	}

    public boolean containsJavaVariable(String javaFieldName){
        return javaToBONameMapping.containsKey(javaFieldName);
    }

    public boolean definesTable(){
        return true;
    }

    public boolean isPersistable(Object object){
        return false;
    }
    
    static public long getNewID(String name, ConnectionProvider provider) throws Exception {
        synchronized(ids){
            ObjectID id = (ObjectID)ids.get(name);
            if (id != null && id.lowID == 0)
                id.lowID++;
            if (id == null || id.lowID >= id.highID){
                id = getNewObjectID(name, id, provider);
                ids.put(name, id);
            }
            return id.lowID++;  
        }
    }
    
    static private ObjectID getNewObjectID(String name, ObjectID prev, ConnectionProvider provider) throws Exception{
        final String pskeyQuery = "autoIDUpdtQryStmt";
        final String pskeyUpdate ="autoIDUpdtWrtStmt";
        final String pskeyInsert ="autoIDUpdtInsStmt";
        ObjectID id = prev;
        short attempsLeft = 15;

		while (attempsLeft-- > 0) {
        
	        DBData dbData = provider.getDBData();
	        DBConnectionPool pool = DBConnectionPool.getInstance(dbData);
			DBConnection con = pool.getConnection();
			ResultSet rs = null;
	        try{
	     
	            PreparedStatement readStatement = con.getPreparedStatement(pskeyQuery);
	            if (readStatement == null)
	                readStatement =   con.getPreparedStatement(pskeyQuery,
	                " SELECT " + getInstance().getFieldName(LOWEST_ID,provider)  +
	                " FROM " + provider.getPrefix() + getInstance().getTableName(provider)  +
	                " WHERE "  + getInstance().getFieldName(BUSINESSOBJECT_NAME,provider) + "=?");
	            readStatement.setString(1, name);
	            readStatement.execute();
	            rs = readStatement.getResultSet();
	            int updated;
	            
	            if (rs.next()){
	                long prevLowest  = rs.getLong(1);
	                id = createOrUpdateObjectID(name, id , prevLowest);
	                
	                PreparedStatement updateStatement = con.getPreparedStatement(pskeyUpdate);
	                if (updateStatement == null)
	                    updateStatement = con.getPreparedStatement(pskeyUpdate,
	                    " UPDATE " + provider.getPrefix() + getInstance().getTableName(provider)  +
	                    " SET " + getInstance().getFieldName(LOWEST_ID,provider)  +  "=?" +
	                    " WHERE "  + getInstance().getFieldName(BUSINESSOBJECT_NAME,provider) + "=?" +
	                    " AND "  + getInstance().getFieldName(LOWEST_ID,provider) + "=?");
	                updateStatement.setLong(1, id.highID);
	                updateStatement.setString(2, id.name);
	                updateStatement.setLong(3, prevLowest);
                    updated = updateStatement.executeUpdate();
	               
	            }else{
	
	                id = createOrUpdateObjectID(name, id , INITIAL_ID );
	                PreparedStatement insertStatement = con.getPreparedStatement(pskeyInsert);
	                if (insertStatement == null)
	                    insertStatement = con.getPreparedStatement(pskeyInsert,
	                    " INSERT INTO " + provider.getPrefix() + getInstance().getTableName(provider) +
	                    " VALUES (?,?)");
	                insertStatement.setString(1, id.name);
	                insertStatement.setLong(2, id.highID);
	                updated = insertStatement.executeUpdate();
	
	            }
	            
	            if (updated > 0)
					attempsLeft = 0;
		
	        } catch (Exception e){
	            Log.error(e.getMessage());
	            e.printStackTrace();
	            throw e;    
	        }finally{
	        	if (rs != null)
					try {
						rs.close();
					} catch (Exception e) {	
					}
	            pool.returnConnection(con);
	        }
	        
	   }
	   
	   return id;
	        
       
    }
    
    public static ObjectID createOrUpdateObjectID(String name, ObjectID id, long low){
        if (id == null)
            id = new ObjectID(name);
        
        
        long timeSpan = System.currentTimeMillis() - id.time;

        if ( timeSpan > 10000 )  
            id.span /= 10;
        else if ( timeSpan < 1000 )    
            id.span *= 10;
       

        if (id.span < 1)
            id.span = 1;
        else if (id.span > 100) 
            id.span = 100;

        id.lowID = low;
        id.highID = id.lowID + id.span;
        id.time = System.currentTimeMillis();
        return id;
    }
    
    static class ObjectID{
        String name ="";
        long lowID = 0;
        long highID = 0;
        long span = 1;
        long time = 0;
        
        public ObjectID(String _name){
            name = _name;
        }
    }
    
}

