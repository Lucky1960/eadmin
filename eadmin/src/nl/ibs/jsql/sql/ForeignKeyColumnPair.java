package nl.ibs.jsql.sql;



import java.util.*;
import java.sql.*;


/**
 *
 * @author  c_hc01
 */
public class ForeignKeyColumnPair {
       
    protected String PKColumnName;
    protected String FKColumnName; 
	
	public String getFKColumnName() {
		return FKColumnName;
	}

	public String getPKColumnName() {
		return PKColumnName;
	}

}

