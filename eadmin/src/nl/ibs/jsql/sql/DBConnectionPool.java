package nl.ibs.jsql.sql;



import java.util.Stack;
import java.sql.*;
import nl.ibs.jeelog.*;
import java.util.*;
import nl.ibs.jsql.*;


/**
 * Data Base Connnection Pool
 * @author  c_hc01
 * @version 1.1
 */
public class DBConnectionPool extends ConnectionPool{
	private static final long serialVersionUID = 1L;
    
    
	private static Hashtable applicationPools = new Hashtable();
	
    private Stack stack                = null;
    private Set activeConnections      = null;
    private String applicationName		= null;
    private DBData dbData              = null;
    private String seperator           = null;
    private String prefix              = "";
    private String searchStringEscape  = null;
    private ORMapping orMapping        = null;
    private DBMapping dbMapping        = null;
	private int SQLStateType           = -1;
    private String messageIllegalState = null;
    private static HashMap managers = new HashMap();
    
    public static DBConnectionPool getInstance(String application){
        return getInstance(DBData.getDefaultDBData(application));
	}

    public static boolean instanceExistsFor(String application, DBData dbd) {
    	Hashtable pools = (Hashtable)applicationPools.get(application);
    	return pools!=null&&pools.containsKey(dbd);
    }
    
	public String getApplicationName() {
		return applicationName;
	}
	
	public static DBConnectionPool getInstance(DBData dbd) {
		Hashtable pools = (Hashtable)applicationPools.get(dbd.getApplication());
		if (pools == null){
			pools = new Hashtable();
			applicationPools.put(dbd.getApplication(),pools);
		}
		DBConnectionPool pool = (DBConnectionPool) pools.get(dbd);
		if (pool == null) {
			pool = new DBConnectionPool(dbd.getApplication(),dbd);
			synchronized(pool) {
				try {
					pools.put(dbd, pool);
					DBManager manager = getManager(dbd.getApplication(), pool);
					DBTransaction tx = DBPersistenceManager.getTransaction();
					DBPersistenceManager.setTransaction(null);
					manager.init(pool);
					DBPersistenceManager.setTransaction(tx);
				} catch (Exception e) {
					java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();					
					e.printStackTrace( new java.io.PrintStream(baos));
					pool.messageIllegalState = "Can not approach database: " + e.getMessage() + "\n" + baos.toString();
				}
			}
		}
		if (pool.messageIllegalState != null)
			throw new RuntimeException(pool.messageIllegalState);
		return pool;
	}
	
	private static DBManager getManager(String applicationName, DBConnectionPool pool) throws Exception {
		DBManager manager= (DBManager)managers.get(pool);
		if (manager == null){
		 	manager = DBManager.getDBManager(applicationName);
		 	managers.put(pool,manager);
		} 	
		return manager; 	
	}
	
    /** Creation a DBConnectionPool.
     * <P>
     * @param DBData dbd
     **/
    private DBConnectionPool(String applicationName, DBData dbd){
    	this.applicationName = applicationName;
    	this.dbData = dbd;
    	this.stack  = new Stack();
    	this.activeConnections  = new HashSet();
    	String schema = dbData.getSchema();
		if (schema == null || schema.equals("")){
			prefix="";
		}else{
			seperator = getCatalogSeparator();
			if (seperator == null || seperator.equals(""))
				seperator=".";
			prefix = schema + seperator;
		}
		 
		Thread t = new Thread(new Runnable(){
			public void run() {
				while (true) {
					disconnectInactiveInvalidConnections();
					try {
					Thread.sleep(300000);
					} catch (Exception ex) {
					}
				}
			}
		});
		t.setDaemon(true);
		t.start();
    }
    

    public String getName(){
        return dbData.getName();
    }
    
    public String getPrefix() {
        return prefix;
    }

    public ORMapping getORMapping(){
        if (orMapping == null)
            orMapping = ORMapping.getInstance(getName());
        return orMapping;    
    }

    public DBMapping getDBMapping(){
        if (dbMapping == null)
            dbMapping = DBMapping.getInstance(getDriver());
        return dbMapping;    
    }

    public DatabaseMetaData getDatabaseMetaData() throws java.sql.SQLException{
		DatabaseMetaData databaseMetaData = null;
            DBConnection con = getConnection();
            databaseMetaData = con.getConnection().getMetaData();
            returnConnection(con);
        return databaseMetaData;
    }
    
    public int executeUpdate(String sql) throws java.sql.SQLException {
		if (messageIllegalState != null)
			throw new RuntimeException(messageIllegalState);
        DBConnection con = getConnection();
        int rslt = con.executeUpdate(sql);
        returnConnection(con);
        return rslt;
    }

    /** Returns DBConnection
     *  <P>
     *  @return DBConnection
     */
    public DBConnection getConnection() {
		if (messageIllegalState != null)
			throw new RuntimeException(messageIllegalState);
        DBConnection connection = null;
        while (!stack.isEmpty()){
            DBConnection con = (DBConnection)stack.pop();
            if (con != null && con.isConnected()){
            connection = con;
            break;
            }
        }
        if (connection == null) 
            connection = createNewDBConnection();
        activeConnections.add(connection);      
        return connection;
    }
    
    /** Return DBConnection to Pool
     *  <P>
     *  @param DBConnection con
     */
    public void returnConnection(DBConnection con){
        if (con != null && con.isConnected() && activeConnections.contains(con) ){
        	con.reset();
        	con.setPooledAt(System.currentTimeMillis());
            stack.push(con);
            activeConnections.remove(con);
        }    
    }
    
    /** Created new DBConnection
     *  <P>
     *  @return DBConnection
     */
    private DBConnection createNewDBConnection(){
            DBConnection con = new DBConnection(getDriver(), getURL(), dbData.getUser(), dbData.getPassword(), this.getDBMapping());
            con.connect();
            return con;
    }
    
    public  String getCatalogSeparator(){
        if (seperator == null){
            try {
                seperator = getDatabaseMetaData().getCatalogSeparator();
            } catch (Exception e) {
                Log.error(e.getMessage());
                e.printStackTrace();
            }
        }
        return seperator;
    }
    
    public  String getSearchStringEscape(){
		if (searchStringEscape == null){
			try {
				searchStringEscape = getDatabaseMetaData().getSearchStringEscape();
			} catch (Exception e) {
				Log.error(e.getMessage());
				e.printStackTrace(); 
			}
			if (searchStringEscape == null || searchStringEscape.trim().equals(""))
				searchStringEscape = "\\";
		}
		return searchStringEscape;
	}

	public String escapeSearchString(String like){
		if (like.indexOf('%') < 0 && like.indexOf('_') < 0 )
			return like;
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < like.length(); i++) {
			char c = like.charAt(i);
			if (c == '%' || c == '_')
				buffer.append(getSearchStringEscape());
			buffer.append(c);		 
		}
		return buffer.toString();
	}
	
	public String escapeSingleQuotes(String string){
		if (string.indexOf('\'') < 0 )
			return string;
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == '\'')
				buffer.append('\'');
			buffer.append(string.charAt(i));		 
		}
		return buffer.toString();
	}
    
    public String getDriver(){
        return dbData.getDriver(); 
    }
    
    public String getURL(){
    	return dbData.getURL();
    }
    
    public String getSchema(){
    	return dbData.getSchema();
    }

    public boolean getAutoCreateTables(){
        return dbData.getAutoCreateTables();
    }

    public boolean getAutoAddColumns(){
        return dbData.getAutoAddColumns();
    }

    public boolean getAutoModifyColumns(){
        return dbData.getAutoModifyColumns();
    }

    public boolean getAutoDropColumns(){
        return dbData.getAutoDropColumns();
    }
    
    public boolean sameDataBase(ConnectionPool pool){
        return (this == pool || getDBId() == pool.getDBId()); 
    }

	public DBData getDBData(){
		return dbData;
	}
        
	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	protected void finalize() throws Throwable {
		disconnectAll();
		super.finalize();
	}

	public void disconnectInactiveConnections() {
		while (!stack.isEmpty()){
			DBConnection con = (DBConnection)stack.pop();
			if (con != null && con.isConnected()){
				con.disconnect();
			}
		}
	}
	
	public void disconnectAll() {
 	    while (!stack.isEmpty()){
			DBConnection con = (DBConnection)stack.pop();
			if (con != null && con.isConnected()){
				con.disconnect();
			}
		}
		Iterator iterator = activeConnections.iterator();
		while (iterator.hasNext()) {
			DBConnection con = (DBConnection)iterator.next();
			if (con != null && con.isConnected()){
				con.disconnect();
			}
		}
		activeConnections.clear();
		Log.info("Closed connections!");
  	}
 
 	public static void disconnectAllPools() {
 		for (Iterator pools = applicationPools.values().iterator(); pools.hasNext();) {
 			for (Iterator iter = ((Hashtable) pools.next()).values().iterator(); iter.hasNext();) {
 				((DBConnectionPool) iter.next()).disconnectAll();
 			}
 		}
	}
		
	public void disconnectInactiveInvalidConnections() {
 		for (int i = 0; i < stack.size(); i++) {
			DBConnection temp = null;
			boolean remove = false;
			try {
				temp = (DBConnection) stack.get(i);
				if (System.currentTimeMillis()-temp.getPooledAt()>300000)
					remove = true;
				else{	
					//temp.executeQuery("SELECT COUNT(*) FROM " + prefix + "JSQLVRSN", true);
					temp.getConnection().getMetaData().getCatalogs();
				}
			} catch (Exception e) {
				if (temp == null || !temp.isConnected()) {
					remove = true;
				}
			}
			if (remove){
				try {
					if (temp != null && temp.isConnected())
						temp.disconnect();
					stack.remove(temp);
				} catch (Exception ex) {
				}
			}
		}
  	}
  	
  	public static void reset(DBData dbd) {
		Hashtable pools = (Hashtable)applicationPools.get(dbd.getApplication());
		if (pools == null)
 			return;
 		DBConnectionPool pool = (DBConnectionPool) pools.get(dbd);
		if (pool != null) {
			pool.disconnectAll();
			pools.remove(dbd);
		}
	}
  		  		
    /* Next methods are only supported for jdk 1.4 and higher
    public  int getSQLStateType(){
        if (SQLStateType == -1){
            try {
                SQLStateType = getDatabaseMetaData().getSQLStateType();
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            } 
        }
        return SQLStateType;
    }
    
    public boolean isSQLStateTypeSQL99(){
        return (getSQLStateType() == DatabaseMetaData.sqlStateSQL99); 
    }
    
    public boolean isSQLStateTypeXOpen(){
        return (getSQLStateType() == DatabaseMetaData.sqlStateXOpen); 
    }
    */

}

