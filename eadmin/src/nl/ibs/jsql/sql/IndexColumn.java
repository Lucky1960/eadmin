package nl.ibs.jsql.sql;



import java.util.*;
import java.sql.*;


/**
 *
 * @author  c_hc01
 */
public class IndexColumn {

	protected String columnName;
	protected String sortSequence;

	/** Column name
	 * @return column name
	 */
	public String getColumnName() {
		return columnName;
	}

	/** Sort sequence. "A" => ascending, "D" => descending, may be null if sort sequence is not supported.
	 * @return sort sequence
	 */
	public String getSortSequence() {
		return sortSequence;
	}

}

