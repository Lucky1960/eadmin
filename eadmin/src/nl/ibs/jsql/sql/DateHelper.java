package nl.ibs.jsql.sql;


import java.sql.*;
import java.io.*;
import org.w3c.dom.*;
import java.util.*;
import nl.ibs.jeelog.*;

/**
 *
 * @author  c_hc01
 */

public class DateHelper {

    

    private static java.util.Calendar calendar = java.util.Calendar.getInstance();
    public static java.sql.Date zeroDate;
    public static java.sql.Time zeroTime;
    public static java.sql.Timestamp zeroTimestamp;
     
    static{
        calendar.set(0001,1,1,0,0,0);
        long time = calendar.getTime().getTime();
        zeroDate = new java.sql.Date(time);
        zeroTimestamp = new java.sql.Timestamp(time);
    }
    
}

