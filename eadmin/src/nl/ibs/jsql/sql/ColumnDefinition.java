package nl.ibs.jsql.sql;



import java.util.*;
import java.sql.*;
import nl.ibs.jeelog.*;

/**
 *
 * @author  c_hc01
 */
public class ColumnDefinition {
    
    

    public String tableCatalog;     // =>(may be null)
    public String tableSchema;      // =>(may be null)
    public String tableName;
    public String columnName; 
    public int sqlType = -1;            // => SQL type from java.sql.Types
    public String typeName;         // => Data source dependent type name, for a UDT the type name is fully qualified
    public int columnSize = -1;          // => For char or date types this is the maximum number of characters, for numeric or decimal types this is precision.
    //BUFFER_LENGTH => unused
    public int decimalDigits = -1 ;       // => the number of fractional digits
    public int numberPreciousRadix = -1; // => Radix (typically either 10 or 2)
    public int nullable; //=> is NULL allowed
    //columnNoNulls - might not allow NULL values
    //columnNullable - definitely allows NULL values
    //columnNullableUnknown - nullability unknown
    public String remarks;          // => comment describing column (may be null)
    public String columnDefinition = ""; // => default value (may be null)
    //SQL_DATA_TYPE int => unused
    //SQL_DATETIME_SUB int => unused
    public int charOctetLength=-1;     // => for char types the maximum number of bytes in the column
    public int ordinalPosition=-1;     // => index of column in table (starting at 1)
    public String isNullable;       // => "NO" means column definitely does not allow NULL values; "YES" means the column might allow NULL values. An empty string means nobody knows.
    public String scopeCatlog;      // => catalog of table that is the scope of a reference attribute (null if DATA_TYPE isn't REF)
    public String scopeSchema;      // => String => schema of table that is the scope of a reference attribute (null if the DATA_TYPE isn't REF)
    public String scopeTable;       // => table name that this the scope of a reference attribure (null if the DATA_TYPE isn't REF)
    public short sourceDataType=-1;     // => source type of a distinct type or user-generated Ref type, SQL type from java.sql.Types (null if DATA_TYPE isn't DISTINCT or user-generated REF)
   
    public static HashMap getColumnDefinitions(String schemaPattern, String tablePattern, DatabaseMetaData meta) {
        return getColumnDefinitions(schemaPattern, tablePattern, "_%", meta);
    }
    
    public static HashMap getColumnDefinitions(String schemaPattern, String tablePattern, String columnPattern ,DatabaseMetaData meta) {
        HashMap map = new HashMap();
        
        ResultSet rs = null;
        String _schemaPattern = (schemaPattern == null || schemaPattern.equals("")) ? null : schemaPattern;
        try{
            rs = meta.getColumns(null , _schemaPattern , tablePattern , columnPattern);
            while(rs.next()){
                ColumnDefinition definition = new ColumnDefinition(rs,meta);
                map.put(definition.columnName.toUpperCase() , definition);
            }
        }catch(Exception e){
            Log.error(e.getMessage());
            e.printStackTrace();
        }finally {
        	if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {	
				}
        }
        return map;
    }
    
    private ColumnDefinition(ResultSet rs, DatabaseMetaData meta){
        try{
            ResultSetMetaData rsmd = rs.getMetaData();
            int z = rsmd.getColumnCount();

            if(z >= 1)tableCatalog = rs.getString(1);       // (may be null)
            else{return;} 
            if(z >= 2)tableSchema = rs.getString(2);        // =>(may be null)
            else{return;} 
            if(z >= 3)tableName = rs.getString(3);
            else{return;} 
            if(z >= 4)columnName = rs.getString(4);
            else{return;} 
            if(z >= 5)sqlType = rs.getInt(5);               // => SQL type from java.sql.Types
            else{return;} 
            if(z >= 6)typeName = rs.getString(6);          // => Data source dependent type name, for a UDT the type name is fully qualified
            else{return;} 
            if(z >= 7)columnSize = rs.getInt(7);           // => For char or date types this is the maximum number of characters, for numeric or decimal types this is precision.
            else{return;}
            //BUFFER_LENGTH => unused 
            if(z >= 9)decimalDigits = rs.getInt(9);         // => the number of fractional digits
            else{return;} 
            if(z >= 10)numberPreciousRadix = rs.getInt(10);  // => Radix (typically either 10 or 2)
            else{return;} 
            if(z >= 11){nullable = rs.getInt(11);  //=> is NULL allowed
                        //columnNoNulls - might not allow NULL values
                        //columnNullable - definitely allows NULL values
                        //columnNullableUnknown - nullability unknown
            }else{return;} 
            if(z >= 12)remarks = rs.getString(12);          // => comment describing column (may be null)
            else{return;} 
            if(z >= 13){columnDefinition = rs.getString(13);  // => default value (may be null)
            //SQL_DATA_TYPE int => unused
            //SQL_DATETIME_SUB int => unused
            }else{return;} 
            if(z >= 16)charOctetLength = rs.getInt(16);      // => for char types the maximum number of bytes in the column
            else{return;} 
            if(z >= 17)ordinalPosition = rs.getInt(17);    // => index of column in table (starting at 1)
            else{return;} 
            if(z >= 18)isNullable = rs.getString(18);       // => "NO" means column definitely does not allow NULL values; "YES" means the column might allow NULL values. An empty string means nobody knows.
            else{return;} 
            if(z >= 19)scopeCatlog = rs.getString(19);      // => catalog of table that is the scope of a reference attribute (null if DATA_TYPE isn't REF)
            else{return;} 
            if(z >= 20)scopeSchema = rs.getString(20);      // => String => schema of table that is the scope of a reference attribute (null if the DATA_TYPE isn't REF)
            else{return;} 
            if(z >= 21)scopeTable = rs.getString(21);       // => table name that this the scope of a reference attribure (null if the DATA_TYPE isn't REF)
            else{return;} 
            if(z >= 22)sourceDataType = rs.getShort(22);     // => source type of a distinct type or user-generated Ref type, SQL type from java.sql.Types (null if DATA_TYPE isn't DISTINCT or user-generated REF)
            
        }catch(Exception e){
            Log.error(e.getMessage());
            e.printStackTrace(); 
        }
    }
    
}

