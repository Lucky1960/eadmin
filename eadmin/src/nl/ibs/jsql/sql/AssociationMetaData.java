package nl.ibs.jsql.sql;


import java.io.Serializable;
import java.util.Map;

import nl.ibs.jsql.*;


public class AssociationMetaData implements Serializable {
	private String tableName;
	private String[][] columnDefinitions;
	private String tableDefinition;
	private Map constraints;
	private Map indexes;
	
	public AssociationMetaData(String _tableName, String[][] _columnDefinitions, String _tableDefinition, Map _constraints, Map _indexes) {
		tableName = _tableName;
		columnDefinitions = _columnDefinitions;
		tableDefinition = _tableDefinition;
		constraints = _constraints;
		indexes = _indexes;
	}
	
	public String getTableName(){
		return tableName;
	}
	
	public String[][] getColumnDefinitions(){
		return columnDefinitions;
	}
	
    public String getTableDefinition(){
    	return tableDefinition;
    }
   
	public Map getIndexDefinitions(){
		return indexes;
	}
	public Map getConstraintDefinitions(){
		return constraints;
	}
	
}


