package nl.ibs.jsql.sql;



import java.util.*;
import java.sql.*;


/**
 *
 * @author  c_hc01
 */
public class PrimaryKeyColumn {

	protected String columnName;
	protected String sortSequence;

	/** Column name
	 * @return column name
	 */
	public String getColumnName() {
		return columnName;
	}

}

