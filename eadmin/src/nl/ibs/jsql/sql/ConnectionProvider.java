package nl.ibs.jsql.sql;



import nl.ibs.jsql.*;



public interface ConnectionProvider{

     public String getPrefix();
     public String getSearchStringEscape();
     public DBConnection getConnection() throws Exception;
     public void returnConnection(DBConnection connection) throws Exception;
     public DBMapping getDBMapping();
     public ORMapping getORMapping();
     public DBData getDBData();
	 public int getDBId();     
     
}

