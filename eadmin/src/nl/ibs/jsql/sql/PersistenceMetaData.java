package nl.ibs.jsql.sql;


import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import nl.ibs.jsql.*;


/**
 *
 * @author  c_hc01
 */

public interface PersistenceMetaData extends Serializable {

	public static final String ONE_TO_ONE 	= "One to one";
    public static final String ONE_TO_MANY 	= "One to many";
    public static final String MANY_TO_ONE 	= "Many to one";
    public static final String MANY_TO_MANY = "Many to many";
    
    public static final String REFERING = "Refering";
	public static final String OWNING = "Owning";
	
	public static final String TO_ASSOCIATIVE_TABLE = "TAT";
	public static final String FROM_ASSOCIATIVE_TABLE ="FAT";

    public String getBusinessObjectName();
    public String getTableName(ConnectionProvider provider);
    public String getTableName(String objectName, ConnectionProvider provider);
    public String getTableDescription();
    public String getFieldName(String fieldName, ConnectionProvider provider);
    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider);
    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider);
    public String getFieldDescription(String fieldName);
    public Set getJavaVariableNames();
    public Set getJavaPKFieldNames();
    
    
    public String getRelationType(String javaVariableNameOfRelation);
    public String getRelationMultiplicity(String javaVariableNameOfRelation);
    public boolean isBackwardReference(String javaVariableNameOfRelation); 
    
    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider);

    public String getTableDefinition(ConnectionProvider provider);
    public Map getAssociativeTableDefinitions(ConnectionProvider provider);
    public String[][] getColumnDefinitions(ConnectionProvider provider);
	public Map getIndexDefinitions(ConnectionProvider provider);
	public Map getConstraintDefinitions(ConnectionProvider provider);
	
	public PersistenceMetaData getPersistingPMD();
    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName);
    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider);
    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type);
    
    public boolean containsJavaVariable(String javaFieldName);
    public boolean definesTable();
    
    public boolean isPersistable(Object object);

}

