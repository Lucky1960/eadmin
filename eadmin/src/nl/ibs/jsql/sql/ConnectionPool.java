package nl.ibs.jsql.sql;



import nl.ibs.jsql.*;

import java.io.Serializable;
import nl.ibs.jeelog.*;

public abstract class ConnectionPool implements ConnectionProvider, Serializable {

     

     public abstract String getPrefix();
     public abstract String getDriver();
     public abstract DBConnection getConnection() throws Exception;
     public abstract void returnConnection(DBConnection connection) throws Exception;
     public abstract DBMapping getDBMapping();
     public abstract ORMapping getORMapping();
     public abstract boolean sameDataBase(ConnectionPool pool);
	 public abstract DBData getDBData();
	 
	 public int getDBId(){
	 	return  getDBData().getDBId();
	 }
	  
     private java.util.ArrayList initiatedManagers = new java.util.ArrayList();

     public Object getManagerInstance(Class manager){
       for (int x = 0 ; x < initiatedManagers.size() ; x++){
           Object object = initiatedManagers.get(x);
           if (object != null && object.getClass() == manager)
               return object;
       }
       return null;
    }
   
    public void addManagerInstance(Object manager){
       initiatedManagers.add(manager);
    }
}

