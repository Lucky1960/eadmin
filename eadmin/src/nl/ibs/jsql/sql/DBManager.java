package nl.ibs.jsql.sql;



import java.sql.*;
import java.util.*;
import nl.ibs.jeelog.*;
import nl.ibs.jsql.impl.*;
import nl.ibs.jsql.*;




      //Custom code
//{CODE-INSERT-BEGIN:1926037870}
//{CODE-INSERT-END:1926037870}
  




public abstract class DBManager{

    


	private final static String META_DATA_TABLE="JSQLMTDT";
	private final static String BO_PACKAGE_COLUMN="BO_PACKAGE";
	private final static String BO_NAME_COLUMN="BO_NAME";
	private final static String BO_ATTRIBUTE_COLUMN="BO_ATTRIBUTE";
	private final static String TABLE_COLUMN="TABLE_NAME";
	private final static String COLUMN_COLUMN="COLUMN_NAME";
	private final static String SQL_DEFINITION_COLUMN="SQL_DEFINITION";
	private final static String DEFAULT_VALUE_COLUMN="DEFAULT_VALUE";
	private final static String EXTENTION_COLUMN="EXTENTION";
	private final static String DESCRIPTION_COLUMN ="DESCRIPTION";
	
	private final static String VERSION_DATA_TABLE = "JSQLVRSN";
	private final static String VERSION_COLUMN = "VERSION_NUMBER";
	private final static String TYPE_COLUMN = "VERSION_TYPE";
	
	private final static String CURRENT_JSQL_VERSION = "1.8.001";
	private final static String JSQL_VERSION = "JSQL_VERSION";
	private final static String APPLICATION_VERSION = "APPLICATION_VERSION";
	
	private final static String CHGJOB = "CALL QSYS.QCMDEXC('CHGJOB INQMSGRPY(*SYSRPYL)',0000000026.00000)";
	
	/*private final static String[] tableType = new String[] { "TABLE" };
	private static ClassLoader classLoader = DBManager.class.getClassLoader();
	private static String _package = "nl.ibs.jsql";*/
	
    private static boolean debug = Log.debug();
    
	private final static HashSet<String> loadedDrivers = new HashSet<String>(2);
	private final static HashMap<String, Connection> baseConnections = new HashMap<String,Connection>(2);
	private final static HashMap<String,String> applicationDBManagers = new HashMap<String,String>(4);
    
    private DBConnectionPool pool;
	private DatabaseMetaData meta;
	private String prefix;
	private HashMap tableDefinitions;
	private HashMap tableNameLists;
	private String db_jsql_version;
	private String db_application_version;
	private boolean readyToDropColumns = false,readyToModifyColumns = false, initialized = true, preConverted = true, postConverted = true, finalized = true;
	


      //Custom code
//{CODE-INSERT-BEGIN:-77861451}
//{CODE-INSERT-END:-77861451}
 




	public static DBManager getDBManager(String applicationName) throws Exception {
		String subClassName = (String)applicationDBManagers.get(applicationName);
		if (subClassName == null){
			subClassName = DBConfig.getDBManager(applicationName);
			if (subClassName == null)
				throw new Exception("No DBManager provided for application: " + applicationName);
			applicationDBManagers.put(applicationName,subClassName);
		}
		return getInstance(subClassName);
	}

	private static DBManager getInstance (String subclass) throws Exception{
		if (subclass == null || subclass.trim().length() == 0  || subclass.trim().equals(DBManager.class.getName()) )
			return new CustomDBManagerImpl();
		Class clss = DBManager.class.getClassLoader().loadClass(subclass);
		return (DBManager)clss.getConstructor((Class[])null).newInstance((Object[])null); 	
	}
	
    public abstract PersistenceMetaData[] getPersistenceMetaDataArray();
   
	public void init(DBConnectionPool pool) throws RuntimeException {
		try {
		    createMYSQLDataBaseIfNotExists(pool);
		    pool.getDBMapping(); //Force loading of mapping
			this.pool = pool;
			prefix = pool.getPrefix();
			updateMetaData();
			db_jsql_version = getDBJSQLVersion();
			db_application_version = getDBApplicationVersion();
			logMetaData();
			initializeDataBase();
		} catch (Throwable e) {
			String message = "initialization failed" +  (e.getMessage()==null?"!":": " + e.getMessage());
			Log.error(message + " See previous messages!");
			e.printStackTrace();
			throw new RuntimeException(message);
		}
	}
	
	public static void createMYSQLDataBaseIfNotExists(DBConnectionPool pool) throws Exception {
		
		String driver = pool.getDriver();
		if(driver.indexOf("mysql")>0){
			Connection conn=null;		
			if (loadedDrivers.contains(driver)==false){
				Class.forName(driver).newInstance();
				loadedDrivers.add(driver);
			}
			DBData dbData=pool.getDBData();
			String url = dbData.getURL();
			String urlWithoutDataBase=url.substring(0,url.lastIndexOf('/'));
			String schema = url.substring(url.lastIndexOf('/')+1);
			if (schema.indexOf('?')>0)
				schema=schema.substring(0,'?');
			String baseConnectionsKey=url+dbData.getUser();
			conn = baseConnections.remove(baseConnectionsKey);
			if (conn != null){
				try {
					if (conn.isClosed())
						conn=null;
					if (conn.isValid(2)==false){
						conn.close();
						conn=null;
					}
				} catch (Exception e){
					conn=null;
				}
			}
			if (conn == null){
				conn = DriverManager.getConnection(urlWithoutDataBase,dbData.getUser(),dbData.getPassword());
			}	
			Statement statement1 = conn.createStatement();
			ResultSet rs = statement1.executeQuery("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '"+schema+"' ");
			if (rs.next()==false){
				Statement statement2 = conn.createStatement();
				statement2.executeUpdate("CREATE DATABASE IF NOT EXISTS "+schema+";");
				statement2.close();
			}
			statement1.close();
			baseConnections.put(baseConnectionsKey,conn);
		}
	}


	public void initializeDataBase() throws Exception {
        
      //Custom code
//{CODE-INSERT-BEGIN:-1295017684}
//{CODE-INSERT-END:-1295017684}
 


		
		try {
		   
			
			if (skipConversion(pool))
				return;
				
			internalInitializeBeforeConversion();
				
			if (schemaExists() && getCurrentJSQLVersion() == null){
				removeForeignKeys();
				removePrimaryKeys();
				removeIndexes();		
			}
			
			initialise(pool);
			if (initialized)
				updateMetaData();

			if (checkSchema())
				updateMetaData();

			removeRedundantConstraintsAndIndexes();
			
			preConvert(pool);
			if (preConverted)
				updateMetaData();

			updateTables();
			

			postConvert(pool);
			if (postConverted)
				updateMetaData();

			updateConstraintsAndIndexes();
			
			removeRedundantColumns();

			persistMetaData();

			finalize(pool);
			
		} catch (Exception e) {
			Log.error(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		
        
      //Custom code
//{CODE-INSERT-BEGIN:2002643279}
//{CODE-INSERT-END:2002643279}
 


        
    }
    
	/**Overwrite this method to conditionally skip database conversion.<br/>
	 * Return false to convert the database, return true to skip conversion 
	 * silently (i.e. because proper version level) otherwise throw exception to
	 * end conversion verbose.</b>
	 * The default implementation returns false 
	 * 
	 * @param pool
	 * @return
	 */
	protected boolean skipConversion(DBConnectionPool pool) throws Exception{
		return false;
	}
	
	/** Overwrite this method to do your own initialisations.Invoked
	 *  after skip(pool) is invoked, but before modification of database.
	 *  
	 * @param connection
	 */
	protected void initialise(DBConnectionPool pool) throws Exception{
		initialized = false;
	}
	
	
	/** Overwrite this method to do your own preprocessing before the creation of
	 *  new tables and columns and the modification of existing tables and columns.
	 *  Invoked after the invocation of the methods skip(pool) and initialise(pool), and 
	 *  after all (remaining) redundant primary keys, indexes and foreign key constraints
	 *  are removed, but before the modifications of existing database tables. 
	 *  
	 * @param connection
	 */
	protected void preConvert(DBConnectionPool pool) throws Exception{
		preConverted = false;
	}

	/** Overwrite this method to do your own processing after the database tables and columns 
	 *  have been changed, but before redundant columns are dropped and before the primary keys,
	 *  indexes and foreign key constraints are updated.
	 * 
	 * @param connection
	 */
	protected void postConvert(DBConnectionPool pool) throws Exception{
		postConverted = false;	
	}

	/** Overwrite this method to do your own cleaning up and or intialisation of the dtabase. Invoked
	 *  after the database conversion is completed.
	 *  Since the conversion is completed here you can savely retrieve, update or create BusinessObjects in this method.
	 * 
	 * @param connection
	 */
	protected void finalize(DBConnectionPool pool) throws Exception {
		finalized = false;
	}
	
	/** Overwite this method to set the new Database Version, this method is called at the end of the conversion. 
	 * @return new version;
	 */
	protected String getNewDataBaseVersion(){
		return getCurrentDataBaseVersion();
	}
	
	
		/**
	 * Remove all indexes on all tables
	 * 
	 * @param connection
	 * @throws Exception
	 */
	protected void removeIndexes() throws Exception {
		removeIndexes((Class[]) null);
	}

	/**
	 * Remove the indexes on the tables in which the provided business objects are stored  
	 *  
	 * @param connection
	 * @param businessObjectClass
	 * @throws Exception
	 */
	protected void removeIndexes(Class[] businessObjectClasses) throws Exception {
		Iterator tdIterator = getTableDefinitions().values().iterator();
		List tableNames = businessObjectClasses == null?null:getTableNameList(businessObjectClasses,true);
		DBConnection connection = pool.getConnection();
		boolean changed = false; 
		try {
			while (tdIterator.hasNext()) {
				TableDefinition td = (TableDefinition) tdIterator.next();
				if (tableNames != null && tableNames.contains(td.getTableName().trim().toUpperCase()) == false)
					continue;
				changed = removeIndexes(connection, changed, td);
			}
		} finally {
			pool.returnConnection(connection);
			if (changed)
				updateMetaData();
		}
	}

	final protected boolean removeIndexes(DBConnection connection, boolean changed, TableDefinition td) throws Exception {
		Log.info("Start remove all named indexes on " + td.getTableName());
		Iterator idIterator = td.getIndexDefinitions().values().iterator();
		while (idIterator.hasNext()) {
			IndexDefinition id = (IndexDefinition) idIterator.next();
			String indexName = id.getIndexName();
			if (indexName != null && !indexName.startsWith("JSQL_FK") && !indexName.startsWith("JSQL_PK")) {
				String fullTableName = getQualifiedName(id.getTableSchema(), id.getTableName());
				String fullIndexName = getQualifiedName(id.getTableSchema(), indexName);
				if (executeUpdate("ALTER TABLE " + fullTableName + " DROP INDEX " + fullIndexName, connection))
					Log.info("Removed index " + indexName + " from table " + fullTableName + ".");
				else
					throw new Error("Failed to remove index " + indexName + " from table " + fullTableName + ".");
				changed = true;	
			}
		}
		return changed;
	}

	/**
	 * Remove all foreign keys on all tables
	 * 
	 * @param connection
	 * @throws Exception
	 */
	protected void removeForeignKeys() throws Exception {
		removeForeignKeys((Class[]) null);
	}

	/**
	 * Remove the foreign keys constraints on the tables in which the provided business objects are stored  
	 *  
	 * @param connection
	 * @param businessObjectClass
	 * @throws Exception
	 */
	protected void removeForeignKeys(Class[] businessObjectClasses) throws Exception {
		Iterator tdIterator = getTableDefinitions().values().iterator();
		List tableNames = businessObjectClasses == null?null:getTableNameList(businessObjectClasses,true);
		DBConnection connection = pool.getConnection();
		boolean changed = false; 
		try {
			while (tdIterator.hasNext()) {
				TableDefinition td = (TableDefinition) tdIterator.next();
				if (tableNames != null &&tableNames.contains(td.getTableName().trim().toUpperCase()) == false)
					continue;
				changed = removeForeignKeys(connection, changed, td);
			}
		} finally {
			pool.returnConnection(connection);
			if (changed)
				updateMetaData();
		}
	}
	
	final protected void internalInitializeBeforeConversion() throws Exception{
		if (schemaExists() && isAS400Driver()) {
			DBConnection dbcon = pool.getConnection();
			Connection con = dbcon.getConnection();
			
			//String url = DBConfig.getURL();
			String url = pool.getURL();
			//Example url: 'db_url=jdbc:as400:FRED;errors=full;block size=512;extended dynamic=true;package=SQLPKG;package library=FACJC'
			int b1 = url.indexOf("package=");
			int b2 = url.indexOf("package library=");
			int b3 = url.indexOf("extended dynamic=true");
			if (b1 > -1 && b2 > -1 && b3 > -1) {

				int e = url.indexOf(";", b1);
				if (e == -1) e = url.length();
				if (e > b1 + 14) e = b1 + 14 ;// IBM neemt alleen de eerste zes posities over; overige posities worden gebruikt voor volgnummer.
				String name = url.substring(b1 + 8, e);
				
				e = url.indexOf(";", b2);
				if (e == -1) e = url.length();
				String lib = url.substring(b2 + 16, e);
				
				StringBuffer sb = new StringBuffer();
				sb.append("CALL QSYS.QCMDEXC('");
				String cmd = "DLTSQLPKG SQLPKG(" + lib + "/" + name + "*)";
				sb.append(cmd);
				sb.append("',");
				sb.append(getLength(cmd));
				sb.append(".00000)");
				String result = sb.toString();
				Log.info(result);
				Statement stmt = con.createStatement();
				try {
					stmt.executeUpdate(result);
				} catch (Exception ex) {
					Log.error(ex.getMessage());
				}
				stmt.close();
			}
			
			pool.returnConnection(dbcon);
			//pool.disconnectAll();
		}
	}
	
	private final static String getLength(String value) {
		String result = Integer.toString(value.length());
		while (result.length() < 10) {
			result = "0" + result;
		}
		return result;
	}
	
	private boolean removeForeignKeys(DBConnection connection, boolean changed, TableDefinition td) throws Exception, Error {
		Log.info("Start removing all named foreign keys on " + td.getTableName());
		Iterator fkdIterator = td.getForeignKeyDefinitions().values().iterator();
		while (fkdIterator.hasNext()) {
			ForeignKeyDefinition fkd = (ForeignKeyDefinition) fkdIterator.next();
			String fkName = fkd.getFKName();
			if (fkName != null /*&& fkName.startsWith("JSQL_FK")*/
				) {
				String fullTableName = getQualifiedName(fkd.getFKTableSchema(), fkd.getFKTableName());
				String fullForeignKeyName = getQualifiedName(fkd.getFKTableSchema(), fkName);
				if (executeUpdate("ALTER TABLE " + fullTableName + " DROP FOREIGN KEY " + fullForeignKeyName,
					connection))
					Log.info("Removed foreign key " + fullForeignKeyName + " from table " + fullTableName + ".");
				else
					throw new Error(
						"Failed to remove foreign key " + fullForeignKeyName + " from table " + fullTableName + ".");
				changed = true;	
			}
		}
		return changed;
	}

	/**
	 * Remove all primary keys on all tables
	 * 
	 * @param connection
	 * @throws Exception
	 */
	protected void removePrimaryKeys() throws Exception {
		removePrimaryKeys((Class[]) null);
	}

	/**
	 * Remove the primary on the tables in which the provided business objects are stored  
	 *  
	 * @param connection
	 * @param businessObjectClass
	 * @throws Exception
	 */
	protected void removePrimaryKeys(Class[] businessObjectClasses) throws Exception {
		Iterator tdIterator = getTableDefinitions().values().iterator();
		List tableNames = businessObjectClasses == null?null:getTableNameList(businessObjectClasses,true);
		DBConnection connection = pool.getConnection();
		boolean changed = false; 
		try {
			while (tdIterator.hasNext()) {
				TableDefinition td = (TableDefinition) tdIterator.next();
				if (tableNames != null && tableNames.contains(td.getTableName().trim().toUpperCase()) == false)
					continue;
				changed = removePrimaryKey(connection, changed, td);
			}
		} finally {
			if (changed)
				updateMetaData();
			pool.returnConnection(connection);
		}
	}

	private boolean removePrimaryKey(DBConnection connection, boolean changed, TableDefinition td) throws Exception, Error {
		PrimaryKeyDefinition pkd = td.getPrimaryKeyDefinition();
		if (pkd != null) {
			String fullTableName = getQualifiedName(pkd.getTableSchema(), pkd.getTableName());
			if (executeUpdate("ALTER TABLE " + fullTableName + " DROP PRIMARY KEY", connection))
				Log.info("Removed primary key from table " + fullTableName + ".");
			else
				throw new Error("Failed to remove primary key from table " + fullTableName + ".");
			changed = true;	
		}
		return changed;
	}

	public List getTableNameList(Class[] businessObjectClasses, boolean includeAssociationTables)throws Exception {
	    if (tableNameLists == null)
	    	tableNameLists	= new HashMap();
		List list = (List)tableNameLists.get(businessObjectClasses);
		if (list != null)
			return list;
		list = new ArrayList();
		PersistenceMetaData[] pmd = getPersistenceMetaDataArray(businessObjectClasses);
		for (int i = 0; i < pmd.length; i++) {
			String tableName = pmd[i].getTableName(pool);
			if (tableName != null && !list.contains(tableName.trim().toUpperCase()))
				list.add(tableName.trim().toUpperCase());
			if (includeAssociationTables) {
				Iterator amds = pmd[i].getAssociativeTableDefinitions(pool).values().iterator();
				while (amds.hasNext()) {
					tableName = ((AssociationMetaData) amds.next()).getTableName();
					if (tableName != null && !list.contains(tableName.trim().toUpperCase()))
						list.add(tableName.trim().toUpperCase());
				}
			}
		}
		tableNameLists.put(businessObjectClasses,list);
		return list;
	}

	private PersistenceMetaData[] getPersistenceMetaDataArray(Class[] businessObjectClasses) throws Exception {
		List list = new ArrayList();
		PersistenceMetaData pmd;
		if (businessObjectClasses != null) {
			for (int i = 0; i < businessObjectClasses.length; i++) {
				BusinessObjectManager manager = BusinessObjectManager.getBusinessObjectManager(businessObjectClasses[i]);
				if (manager != null && !list.contains(pmd = manager.getPersistenceMetaData()))
					list.add(pmd);
			}
		}
		return (PersistenceMetaData[]) list.toArray(new PersistenceMetaData[list.size()]);
	}
	
	
	private String getQualifiedName(String schemaName, String tableName) throws Exception {
		if (schemaName == null || schemaName.trim().length() == 0)
			return tableName;
		else
			return schemaName.trim() + meta.getCatalogSeparator() + tableName;
	}
	
	final protected void removeRedundantConstraintsAndIndexes() throws Exception{
	    DBConnection con = null;
	    try{
		   con = pool.getConnection();
		   removeRedundantForeignKeys(con);
		   removeRedundantPrimaryKeys(con);
		   removeRedundantIndexes(con);
	    }finally{
		   pool.returnConnection(con);
	    }
	}
	

	private void removeRedundantIndexes(DBConnection connection) throws Exception {
		Iterator tdIterator = getTableDefinitions().values().iterator();
		boolean changed = false; 
		try {
			while (tdIterator.hasNext()) {
				TableDefinition td = (TableDefinition) tdIterator.next();
				Log.info("Start check for reduntant indexes on " + td.getTableName());
				Iterator idIterator = td.getIndexDefinitions().values().iterator();
				while (idIterator.hasNext()) {
					IndexDefinition id = (IndexDefinition) idIterator.next();
					String indexName = id.getIndexName();
					short code =0;
					if (indexName != null &&
						indexName.startsWith("JSQL_") &&
						!indexName.startsWith("JSQL_FK") && 
						!indexName.startsWith("JSQL_PK") &&
						(code = isRedundant(id))>0) {
						String fullTableName = getQualifiedName(id.getTableSchema(), id.getTableName());
						String fullIndexName = getQualifiedName(id.getTableSchema(), indexName);
						if (executeUpdate("ALTER TABLE " + fullTableName + " DROP INDEX " + fullIndexName, connection))
							Log.info("Removed "+(code==1?"redundant":"changed") +" index " + indexName + " from table " + fullTableName + ".");
						else
							throw new Error("Failed to remove "+(code==1?"redundant":"changed") +" index " + indexName + " from table " + fullTableName + ".");
						changed = true;
					}
				}
			}
		} finally {
			if (changed)
				updateMetaData();
		}
	}

	/**
	 * Test if index is redundant, returns 0 if not redundant, 1 if redundant because dropped, 2 if reduntant because changed.
	 * @param indexDefinition
	 * @return 0 not redundant, 1 reduntant because dropped, 2 reduntant because changed.  
	 * @throws Exception
	 */
	private short isRedundant(IndexDefinition id) throws Exception {
		String tableName = id.getTableName();
		String indexName = id.getIndexName().trim().toUpperCase();
		PersistenceMetaData[] pmds = getPersistenceMetaDataArray();
		for (int i = 0; i < pmds.length; i++) {
		    if (pmds[i].definesTable() == false)
				continue;
			if (pmds[i].definesTable() && tableName.equalsIgnoreCase(pmds[i].getTableName(pool))) {
				String indexConstraint = (String) pmds[i].getIndexDefinitions(pool).get(indexName);
				if (indexConstraint != null)
					return (short) (isSameIndex(id, indexConstraint) ? 0 : 2);
			}
			Iterator amds = pmds[i].getAssociativeTableDefinitions(pool).values().iterator();
			while (amds.hasNext()) {
				AssociationMetaData amd = (AssociationMetaData) amds.next();
				if (tableName.equalsIgnoreCase(amd.getTableName())) {
					String indexConstraint = (String) amd.getIndexDefinitions().get(indexName);
					if (indexConstraint != null)
						return (short) (isSameIndex(id, indexConstraint) ? 0 : 2);
				}
			}
		}
		return 1;
	}

	private void removeRedundantForeignKeys(DBConnection connection) throws Exception {
		Iterator tdIterator = getTableDefinitions().values().iterator();
		boolean changed = false;
		try {
			while (tdIterator.hasNext()) {
				TableDefinition td = (TableDefinition) tdIterator.next();
				Log.info("Start check for reduntant foreign keys on " + td.getTableName());
				Iterator fkdIterator = td.getForeignKeyDefinitions().values().iterator();
				while (fkdIterator.hasNext()) {
					ForeignKeyDefinition fkd = (ForeignKeyDefinition) fkdIterator.next();
					String fkName = fkd.getFKName();
					short code =0;
					if (fkName != null && fkName.startsWith("JSQL_FK") && (code = isRedundant(fkd))>0) {
						String fullTableName = getQualifiedName(fkd.getFKTableSchema(), fkd.getFKTableName());
						String fullForeignKeyName = getQualifiedName(fkd.getFKTableSchema(), fkName);
						if (executeUpdate("ALTER TABLE " + fullTableName + " DROP FOREIGN KEY " + fullForeignKeyName,connection))
							Log.info(
								"Removed "+(code==1?"redundant":"changed") +" foreign key constraint "
									+ fullForeignKeyName
									+ " from table "
									+ fullTableName
									+ ".");
						else
							throw new Error(
								"Failed to remove "+(code==1?"redundant":"changed") +" foreign key constraint "
									+ fullForeignKeyName
									+ " from table "
									+ fullTableName
									+ ".");
						changed = true;
					}
				}
			}
		} finally {
			if (changed)
				updateMetaData();
		}

	}

	/**
	 * Test if foreign key is redundant, returns 0 if not redundant, 1 if redundant because dropped, 2 if reduntant because changed.
	 * @param foreignKeyDefinition
	 * @return 0 not redundant, 1 reduntant because dropped, 2 reduntant because changed.  
	 * @throws Exception
	 */
	private short isRedundant(ForeignKeyDefinition foreignKeyDefinition) throws Exception {
		String tableName = foreignKeyDefinition.getFKTableName();
		String fkName = foreignKeyDefinition.getFKName().trim().toUpperCase();
		PersistenceMetaData[] pmds = getPersistenceMetaDataArray();
		for (int i = 0; i < pmds.length; i++) {
			if (!pmds[i].definesTable())
				continue;
			if (tableName.equalsIgnoreCase(pmds[i].getTableName(pool))) {
				String fkConstraint = (String) pmds[i].getConstraintDefinitions(pool).get(fkName);
				if (fkConstraint != null)
					return (short) (isSameForeignKey(foreignKeyDefinition, fkConstraint) ? 0 : 2);
			}
			Iterator amds = pmds[i].getAssociativeTableDefinitions(pool).values().iterator();
			while (amds.hasNext()) {
				AssociationMetaData asm = (AssociationMetaData) amds.next();
				if (tableName.equalsIgnoreCase(asm.getTableName())) {
					String fkConstraint = (String) asm.getConstraintDefinitions().get(fkName);
					if (fkConstraint != null)
						return (short) (isSameForeignKey(foreignKeyDefinition, fkConstraint) ? 0 : 2);
				}
			}
		}
		return 1;
	}

	private void removeRedundantPrimaryKeys(DBConnection connection) throws Exception {
		Iterator tdIterator = getTableDefinitions().values().iterator();
		boolean changed = false;
		try {
			while (tdIterator.hasNext()) {
				TableDefinition td = (TableDefinition) tdIterator.next();
				Log.info("Start check for redundant primary key on " + td.getTableName());
				PrimaryKeyDefinition pkd = td.getPrimaryKeyDefinition();
				String pkName;
				short code =0;
				if (pkd != null && (pkName = pkd.getName()) != null && pkName.startsWith("JSQL_PK") && (code = isRedundant(pkd))>0) {
					String fullTableName = getQualifiedName(pkd.getTableSchema(), pkd.getTableName());
					if (executeUpdate("ALTER TABLE " + fullTableName + " DROP PRIMARY KEY", connection))
						Log.info("Removed "+(code==1?"redundant":"changed") +" primary key constraint from table " + fullTableName + ".");
					else
						throw new Error("Failed to remove "+(code==1?"redundant":"changed") +" primary key constraint from table " + fullTableName + ".");
					changed = true;
				}
			}
		} finally {
			if (changed)
				updateMetaData();
		}
	}

	/**
	 * Test if primary key is redundant, returns 0 if not redundant, 1 if redundant because dropped, 2 if reduntant because changed.
	 * @return 0 not redundant, 1 reduntant because dropped, 2 reduntant because changed.  
	 * @throws Exception
	 */
	private short isRedundant(PrimaryKeyDefinition primaryKeyDefinition) throws Exception {
		String tableName = primaryKeyDefinition.getTableName();
		String pkName = primaryKeyDefinition.getName().trim().toUpperCase();
		PersistenceMetaData[] pmds = getPersistenceMetaDataArray();
		for (int i = 0; i < pmds.length; i++) {
		    if (pmds[i].definesTable() == false)
				continue;		
			if (tableName.equalsIgnoreCase(pmds[i].getTableName(pool))) {
				String pkConstraint = (String) pmds[i].getConstraintDefinitions(pool).get(pkName);
				if (pkConstraint != null)
					return (short) (isSamePrimaryKey(primaryKeyDefinition, pkConstraint) ? 0 : 2);
			}
			Iterator amds = pmds[i].getAssociativeTableDefinitions(pool).values().iterator();
			while (amds.hasNext()) {
				AssociationMetaData amd = (AssociationMetaData) amds.next();
				if (tableName.equalsIgnoreCase(amd.getTableName())) {
					String pkConstraint = (String) amd.getConstraintDefinitions().get(pkName);
					if (pkConstraint != null)
						return (short) (isSamePrimaryKey(primaryKeyDefinition, pkConstraint) ? 0 : 2);
				}
			}
		}
		return 1;
	}
	
	final protected void updateMetaData() throws Exception{
		meta = pool.getDatabaseMetaData();
		tableDefinitions = null;
	}
	
	protected HashMap getTableDefinitions() throws Exception{
		if (tableDefinitions == null){
			tableDefinitions = TableDefinition.getTableDefinitions(pool);
		}
		return tableDefinitions;
	}
	
	final protected void updateConstraintsAndIndexes() throws Exception{
		DBConnection con = null;
		try{
		   con = pool.getConnection();
		   updateIndexes(con);
		   updatePrimaryKeys(con);
		   updateForeignKeys(con);
		}finally{
		   pool.returnConnection(con);
		}
	}
	
	private void updatePrimaryKeys(DBConnection connection) throws Exception {
		PersistenceMetaData[] persistenceMetaData = getPersistenceMetaDataArray();
		for (int i = 0; i < persistenceMetaData.length; i++) {
			PersistenceMetaData pmd = persistenceMetaData[i];
			if (pmd.definesTable() == false)
				continue;			
			String tableName = pmd.getTableName(pool);
			try {
				Log.info("Start checking and updating primary keys on " + tableName);
				updatePrimaryKey(pmd, connection);
			} catch (Exception e) {
				Log.error(
					"Problem adding primary key on table "
						+ tableName
						+ " using "
						+ pmd.getClass().getName()
						+ "! "
						+ (e.getMessage() == null ? "" : "Cause: " + e.getMessage()));
				e.printStackTrace();
				throw e;
			}
		}
	}

	private void updateForeignKeys(DBConnection connection) throws Exception {
		PersistenceMetaData[] persistenceMetaData = getPersistenceMetaDataArray();
		for (int i = 0; i < persistenceMetaData.length; i++) {
			PersistenceMetaData pmd = persistenceMetaData[i];
			if (pmd.definesTable() == false)
				continue;			
			String tableName = pmd.getTableName(pool);
			try {
				Log.info("Start checking and updating foreign keys on " + tableName);
				updateForeignKeys(pmd, pool, connection);
			} catch (Exception e) {
				Log.error(
					"Problem adding foreign key on table "
						+ tableName
						+ " using "
						+ pmd.getClass().getName()
						+ "! "
						+ (e.getMessage() == null ? "" : "Cause: " + e.getMessage()));
				e.printStackTrace();
				throw e;
			}
		}
	}

	private void updateIndexes(DBConnection connection) throws Exception {
		PersistenceMetaData[] persistenceMetaData = getPersistenceMetaDataArray();
		for (int i = 0; i < persistenceMetaData.length; i++) {
			PersistenceMetaData pmd = persistenceMetaData[i];
			if (pmd.definesTable() == false)
				continue;
			String tableName = pmd.getTableName(pool);
			try {
				Log.info("Start checking and updating indexes on " + tableName);
				updateIndexes(pmd, pool, connection);
			} catch (Exception e) {
				Log.error(
					"Problem adding indexes on table "
						+ tableName
						+ " using "
						+ pmd.getClass().getName()
						+ "! "
						+ (e.getMessage() == null ? "" : "Cause: " + e.getMessage()));
				e.printStackTrace();
				throw e;
			}
		}
	}

	public void printPrimaryKeys(DBConnection connection) throws Exception {
			Iterator iterator = getTableDefinitions().values().iterator();
			while (iterator.hasNext()) {
				TableDefinition definition = (TableDefinition) iterator.next();
				PrimaryKeyDefinition primaryKeyDefinition = definition.getPrimaryKeyDefinition();
				if (primaryKeyDefinition != null)
					System.out.println(primaryKeyDefinition.getName());
			}
	}

    
	public void printForeignKeys(DBConnection connection) throws Exception {
		Iterator iterator = getTableDefinitions().values().iterator();
		while (iterator.hasNext()) {
			TableDefinition definition = (TableDefinition) iterator.next();
			Iterator foreignKeyConstraints = definition.getForeignKeyDefinitions().values().iterator();
			while (foreignKeyConstraints.hasNext()) {
				ForeignKeyDefinition foreignKeyDefinition = (ForeignKeyDefinition) foreignKeyConstraints.next();
				System.out.println(foreignKeyDefinition.getFKName());
			}
		}
	}


	public void printIndexes(DBConnection connection) throws Exception {
		Iterator iterator = getTableDefinitions().values().iterator();
		while (iterator.hasNext()) {
			TableDefinition definition = (TableDefinition) iterator.next();
			Iterator indexes = definition.getIndexDefinitions().values().iterator();
			while (indexes.hasNext()) {
				IndexDefinition indexDefinition = (IndexDefinition) indexes.next();
				System.out.println(indexDefinition.getIndexName());
			}
		}
		
    }

 
    final protected void updateTables() throws Exception {
		PersistenceMetaData[] persistenceMetaData = getPersistenceMetaDataArray();
		for (int i = 0; i < persistenceMetaData.length; i++) {
			PersistenceMetaData pmd = persistenceMetaData[i];
			if (pmd.definesTable() == false)
				continue;
			String tableName = pmd.getTableName(pool);
			DBConnection connection = null;
			try {
				connection = pool.getConnection();

      //Custom code
//{CODE-INSERT-BEGIN:-1240461706}
//{CODE-INSERT-END:-1240461706}
 

 
				TableDefinition tableDefinition = (TableDefinition) getTableDefinitions().get(tableName.toUpperCase());
				if (tableDefinition == null) {
					if (pool.getAutoCreateTables() == false) {
						Log.info(
							"Table "
								+ tableName
								+ " not found!, Change configuration to 'auto create table' to 'true' in order to auto create the table!");
					} else {
						createTable(pmd, connection);
						createCommentOnTable(pmd, connection);
						createCommentOnColumns(pmd, connection);
					}
				} else {
					updateColumns(pmd.getTableName(pool),pmd.getColumnDefinitions(pool), connection, tableDefinition);
				}
				updateAssociationTables(pmd,connection);
				Log.info("Completed checking table " + tableName);

      //Custom code
//{CODE-INSERT-BEGIN:790511165}
//{CODE-INSERT-END:790511165}
 


			} catch (Exception e) {
				Log.warn("Problem creating table " + tableName + " using " + pmd.getClass().getName());
				Log.error(e.getMessage());
				e.printStackTrace();
				throw e;
			} finally {
				pool.returnConnection(connection);	
			}
		}
		updateMetaData();
    }
   
	private void updateAssociationTables(PersistenceMetaData pmd, DBConnection connection) throws Exception {
		// Check and create associative tables
		Map associativeTables = pmd.getAssociativeTableDefinitions(pool);
		if (associativeTables == null)
			return ;
		Iterator it = associativeTables.values().iterator();
		while (it.hasNext()) {
			AssociationMetaData amd = (AssociationMetaData) it.next();
			TableDefinition tableDefinition = (TableDefinition) getTableDefinitions().get(amd.getTableName().toUpperCase());
			if (tableDefinition == null) {
				if (pool.getAutoCreateTables() == false) {
					Log.info(
						"Association table "
							+ amd.getTableName()
							+ " not found!, Change configuration to 'auto create table' to 'true' in order to auto create the table!");
				} else {
					Log.info(
						"Trying to create association table " + amd.getTableName() + " without Foreign Key and Index Constraints.");
					if (executeUpdate(amd.getTableDefinition(), connection)) {
						Log.info(
							"Associative support table " + amd.getTableName() + " created! [" + amd.getTableDefinition() + "]");	
					} else {
						throw new Exception("Could not create association table " + amd.getTableName() + ". See log for details.");
					}
				}
			} else {
				updateColumns(amd.getTableName(), amd.getColumnDefinitions(), connection, tableDefinition);
				Log.info("Completed checking association table " + amd.getTableName());
			}
		}
	}
     
   /** Checks if schema exists if not creates schema
    * @param meta
    * @param pool
    * @param connection
    * @throws Exception
    * @return true if and lonly if schema did not yet exists and a was succesfully created.
    */
	final protected boolean checkSchema() {
      	String schema = pool.getSchema();
      	DBConnection connection = null;
      	try{
			connection = pool.getConnection();    
      		if (schema != null && !schema.trim().equals("")){
         		Log.info("Start checking schema " + schema);
         		if (!schemaExists(schema, connection)){
            		try{
               			executeUpdate("CREATE SCHEMA " + schema ,connection);
               			return true;
            		}catch(Exception e){
               			Log.warn("Could not create schema " + schema + ": " + e.getMessage()+ " \n If schema does not yet exist you have to create manually!");
            		}
         		}
      		}
      	}finally{
      		pool.returnConnection(connection);
      	}
      	return false;
   	}

   private void createTable(PersistenceMetaData pmd, DBConnection connection) throws Exception {
	  String tableName = pmd.getTableName(pool);

      String createInstruction = pmd.getTableDefinition(pool);
      
      Log.info("Trying to create table " + tableName + " without Foreign Key and Index Constraints");
      if (executeUpdate(createInstruction , connection)){
         Log.info("Table " + tableName + " created! [" + createInstruction + "]");
      }else{
         Log.error("Could not create table " + tableName + "! See prevous messages!");
      }
      
   }
   
	private void updatePrimaryKey(PersistenceMetaData pmd, DBConnection connection) throws Exception {
		if (pmd.definesTable() == false)
			return;
		String tableName = pmd.getTableName(pool);
		Map constraints = pmd.getConstraintDefinitions(pool);
		updatePrimaryKey(tableName, constraints, connection);

		Iterator iter = pmd.getAssociativeTableDefinitions(pool).values().iterator();
		while (iter.hasNext()) {
			AssociationMetaData amd = (AssociationMetaData) iter.next();
			updatePrimaryKey(amd.getTableName(), amd.getConstraintDefinitions(), connection);
		}

	}

	private void updatePrimaryKey(String tableName, Map constraints, DBConnection connection) throws Exception,Error {

		Iterator names = constraints.keySet().iterator();
		while (names.hasNext()) {
			String name = (String) names.next();
			if (name == null || name.startsWith("JSQL_PK_") == false)
				continue;

			String pkConstraint = (String) constraints.get(name);
			pkConstraint = pkConstraint.replaceFirst(prefix, ""); //LVL
			String fullTableName = prefix + tableName;

			PrimaryKeyDefinition primaryKeyDefinition = getPrimaryDefinition(tableName, name);
			if (primaryKeyDefinition != null) {
				if (isSamePrimaryKey(primaryKeyDefinition, pkConstraint))
					continue;
				else {
					if (executeUpdate("ALTER TABLE " + fullTableName + " DROP PRIMARY KEY", connection))
						Log.info("Removed primary key constraint from table " + fullTableName + ".");
					else
						throw new Error("Failed to remove primary key constraint from table " + fullTableName + ".");
				}
			}
			String fullConstraintName = prefix + name;
			String statement = "ALTER TABLE " + fullTableName + " ADD " + pkConstraint;
			if (executeUpdate(statement, connection))
				Log.info(
					"Added primary key constraint "
						+ fullConstraintName
						+ " on table "
						+ fullTableName
						+ ". ["
						+ pkConstraint
						+ "]");
			else
				throw new Error(
					"Failed to add constraint " + fullConstraintName + " on table " + fullTableName + ". [" + pkConstraint + "]");
		}
	}

	public PrimaryKeyDefinition getPrimaryDefinition(String tableName, String primaryKeyName) throws Exception{
		TableDefinition definition = getTableDefinition(tableName);
		if (definition == null)
			return null;
		else	
			return definition.getPrimaryKeyDefinition();
	}

	public boolean isSamePrimaryKey(PrimaryKeyDefinition pkDefinition, String pkConstraint) {
		int x=0,y=0;	
		if (   pkConstraint == null
		    || pkDefinition == null
		    || (x = pkConstraint.indexOf("PRIMARY "))<0
			|| x+6 >= pkConstraint.length() 
			|| (x = pkConstraint.indexOf(" KEY",x+6))<0
			|| x+4 >= pkConstraint.length()
			|| (x = pkConstraint.indexOf("(",x+4))<0
			|| x+1 >= pkConstraint.length()
			|| (y = pkConstraint.indexOf(")",x+1))<0)
				return false;
		StringTokenizer c1 = new StringTokenizer(pkConstraint.substring(x+1,y),", ");
		PrimaryKeyColumn[] c2 = pkDefinition.getPrimaryKeyColumns();
		if (c1.countTokens() != c2.length)
			return false;
		int z = 0;	
		while(c1.hasMoreTokens()){
			if (c1.nextToken().equalsIgnoreCase(c2[z++].getColumnName())==false)
				return false;
		}
		return true;				
	}

	private void updateForeignKeys(PersistenceMetaData pmd, ConnectionProvider provider, DBConnection connection) throws Exception {
		if (pmd.definesTable() == false)
			return;
		String tableName = pmd.getTableName(provider);
		Map constraints = pmd.getConstraintDefinitions(provider);
		updateForeignKeys(tableName, constraints, connection);
		
		Iterator iter = pmd.getAssociativeTableDefinitions(provider).values().iterator();
		while (iter.hasNext()) {
			AssociationMetaData amd = (AssociationMetaData) iter.next();
			updateForeignKeys(amd.getTableName(), amd.getConstraintDefinitions(), connection);
		}
	}

	private void updateForeignKeys(String tableName, Map constraints, DBConnection connection) throws Exception, Error {
		Iterator names = constraints.keySet().iterator();
		while (names.hasNext()) {
			String name = (String) names.next();
			if (name == null || name.startsWith("JSQL_FK_") == false)
				continue;
			String fkConstraint = (String) constraints.get(name);
			fkConstraint = fkConstraint.replaceFirst(prefix, ""); //LVL
			
			String fullTableName = prefix + tableName;
			String fullConstraintName = prefix + name;
		
			ForeignKeyDefinition foreignKeyDefinition = getForeignKeyDefinition(tableName, name);
			if (foreignKeyDefinition != null) {
				if (isSameForeignKey(foreignKeyDefinition, fkConstraint)){
					continue;
				}else {
					if (executeUpdate("ALTER TABLE " + fullTableName + " DROP FOREIGN KEY " + fullConstraintName, connection))
						Log.info("Removed foreign key constraint " + fullConstraintName + " from table " + fullTableName + ".");
					else
						throw new Error("Failed to remove foreign key constraint " + fullConstraintName + " from table " + fullTableName + ".");
				}
			}
			String statement = "ALTER TABLE " + fullTableName + " ADD " + fkConstraint;
			if (executeUpdate(statement, connection))
				Log.info("Added foreign key constraint " + fullConstraintName + " on table " + fullTableName);
			else
				throw new Error("Failed to add constraint " + fullConstraintName + " on table " + fullTableName + ". [" + fkConstraint + "]");
				
		}
	}

	public ForeignKeyDefinition getForeignKeyDefinition(String tableName, String foreignKeyName) throws Exception{
		TableDefinition definition = getTableDefinition(tableName);
		if (definition == null)
			return null;
		Iterator foreignKeyConstraints = definition.getForeignKeyDefinitions().values().iterator();
		while (foreignKeyConstraints.hasNext()) {
			ForeignKeyDefinition foreignKeyDefinition = (ForeignKeyDefinition) foreignKeyConstraints.next();
			if (foreignKeyDefinition.getFKName().equalsIgnoreCase(foreignKeyName))
				return foreignKeyDefinition;
		}
		return null;
	}

	public boolean isSameForeignKey(ForeignKeyDefinition fkDefinition, String fkConstraint) {
		int p = 0,  x1 = 0, y1 = 0,x2 = 0, y2 = 0, z1 = 0 , z2 = 0, length = fkConstraint.length();
		if (fkConstraint == null
			|| fkDefinition == null
			|| (x1 = fkConstraint.indexOf("FOREIGN ")) < 0
			|| (p = x1 + 6) >= length
			|| (x1 = fkConstraint.indexOf(" KEY", p)) < 0
			|| (p = x1 + 4) >= length
			|| (x1 = fkConstraint.indexOf("(", p)) < 0
			|| (p = x1 + 1) >= length
			|| (y1 = fkConstraint.indexOf(")", p)) < 0
			|| (p = y1 + 1) >= length
			|| (z1 = fkConstraint.indexOf("REFERENCES ", p)) < 0
			|| (p = z1 = z1 + 11) >= length
			|| (z2 = x2 = fkConstraint.indexOf("(", p)) < 0
		    || (p = x2 + 1) >= length
		    || (y2 = fkConstraint.indexOf(")", p)) < 0)
			return false;
		StringTokenizer t1 = new StringTokenizer(fkConstraint.substring(x1 + 1, y1), ", ");
		StringTokenizer t2 = new StringTokenizer(fkConstraint.substring(x2 + 1, y2), ", ");
		String fullTableName = fkConstraint.substring(z1 , z2).trim();
		String tableName = fullTableName.indexOf('.') < 0?fullTableName:fullTableName.substring(fullTableName.indexOf('.')+1).trim();
		ForeignKeyColumnPair[] fkcp = fkDefinition.getForeignKeyColumnPairs();
		if (t1.countTokens() != fkcp.length || t2.countTokens() != fkcp.length)
			return false;
		if (!tableName.equalsIgnoreCase(fkDefinition.getPKTableName()))
			return false;	
		int z = 0;
		while (t1.hasMoreTokens() && t2.hasMoreTokens()) {
			if (t1.nextToken().equalsIgnoreCase(fkcp[z].getFKColumnName()) == false)
				return false;
			if (t2.nextToken().equalsIgnoreCase(fkcp[z++].getPKColumnName()) == false)
				return false;	
		}
		return true;
	}
	
	private void updateIndexes(PersistenceMetaData pmd, ConnectionProvider provider, DBConnection connection) throws Exception {
		if (pmd.definesTable() == false)
			return;
		Map indexes = pmd.getIndexDefinitions(provider);
		String tableName = pmd.getTableName(provider);
		updateIndexes(tableName, indexes, connection);
		
		Iterator iter = pmd.getAssociativeTableDefinitions(provider).values().iterator();
		while (iter.hasNext()) {
			AssociationMetaData amd = (AssociationMetaData) iter.next();
			updateIndexes(amd.getTableName(), amd.getIndexDefinitions(), connection);
		}
		
	}

	private void updateIndexes(String tableName, Map indexes, DBConnection connection) throws Exception, Error {
		String fullTableName = prefix + tableName;
		Iterator names = indexes.keySet().iterator();
		while (names.hasNext()) {
			String name = (String) names.next();
			String index = (String) indexes.get(name);
			index = index.replaceFirst(prefix, ""); //LVL
			IndexDefinition indexDefinition = getIndexDefinition(tableName, name);
			if (indexDefinition != null) {
				if (isSameIndex(indexDefinition, index))
					continue;
				else {
					if (executeUpdate("ALTER TABLE " + fullTableName + " DROP INDEX " + prefix+name, connection))
						Log.info("Removed index " + name + " from table " + fullTableName + ".");
					else
						throw new Error("Failed to remove index " + name + " from table " + fullTableName + ".");
				}
			}
		
			String statement = "CREATE " + index;
			if (executeUpdate(statement, connection))
				Log.info("Added index " + name + " on table " + tableName);
			else
				throw new Error("Failed to add index " + name + " on table " + tableName + ". [" + index + "]");
		}
	}

	public IndexDefinition getIndexDefinition(String tableName, String indexName) throws Exception {
		TableDefinition definition = getTableDefinition(tableName);
		if (definition == null)
			return null;
		Iterator indexes = definition.getIndexDefinitions().values().iterator();
		while (indexes.hasNext()) {
			IndexDefinition indexDefinition = (IndexDefinition) indexes.next();
			if (indexDefinition.getIndexName().equalsIgnoreCase(indexName))
				return indexDefinition;
		}
		return null;
	}

	public boolean isSameIndex(IndexDefinition indexDefinition, String indexConstraint) {
		int x = 0, y = 0;
		if (indexDefinition == null
			|| indexConstraint == null
			|| (x = indexConstraint.indexOf("(")) < 0
			|| x + 1 >= indexConstraint.length()
			|| (y = indexConstraint.indexOf(")", x + 1)) < 0)
			return false;
		StringTokenizer c1 = new StringTokenizer(indexConstraint.substring(x + 1, y), ",");
		IndexColumn[] c2 = indexDefinition.getIndexColumns();
		if (c1.countTokens() != c2.length)
			return false;
		int z = 0;
		while (c1.hasMoreTokens()) {
			String def = c1.nextToken().trim();
			IndexColumn indexColumn = c2[z++];
			String sortSequence = "A"; 
			if (def.endsWith(" DESC")){
				sortSequence = "D";
				def = def.substring(0,def.indexOf(" DESC")).trim();		
			}else if (def.endsWith(" ASC")){
				def = def.substring(0,def.indexOf(" ASC")).trim();		
			}
			if (def.equalsIgnoreCase(indexColumn.getColumnName()) == false)
				return false;
			if (sortSequence.equalsIgnoreCase(indexColumn.getSortSequence()) == false)
				return false; 	
		}
		return true;
	}

	private void createCommentOnTable(PersistenceMetaData pmd, DBConnection connection) throws Exception {
		if (!pmd.definesTable() || pool.getDBMapping().instructionCommentOnTableSupported() == false)
			return;
		String tableName = pmd.getTableName(pool);
		String tableDescription = pmd.getTableDescription();
		if (executeUpdate("COMMENT ON TABLE " + prefix + tableName + " IS '" + tableDescription + "'", connection))
			Log.info("Description added to table " + tableName);
		else
			Log.warn("Failed to add description to table " + tableName);
	}
     
    private void createCommentOnColumns(PersistenceMetaData pmd, DBConnection connection) throws Exception {
	  if (!pmd.definesTable() || pool.getDBMapping().instructionCommentOnColumnSupported() == false)
         return;
      String tableName = pmd.getTableName(pool);
      String[][] columns = pmd.getColumnDefinitions(pool);
      int zz = 0;
      for(int y = 0 ; y < columns.length; y++){
         if (executeUpdate("COMMENT ON COLUMN " + prefix + tableName + "." + columns[y][0] + " IS  '" +columns[y][4] + "'", connection))
            zz++;
         else
            break;
      }
      Log.info(zz + " column descriptions added to table " + tableName);
    }
   
    private void updateColumns(String tableName, String[][] columns, DBConnection connection, TableDefinition tableDefinition) throws Exception {
      Log.info("Start checking " + String.valueOf(columns.length) + " columns of table " + tableName);
      
      HashMap cd = tableDefinition.getColumnDefinitions();
      for(int y = 0 ; y < columns.length; y++){
         ColumnDefinition columnDefinition = (ColumnDefinition)cd.get(columns[y][0].toUpperCase());
         if(columnDefinition == null || !equals(columnDefinition,columns[y])){
            String add_or_modify;
            if (columnDefinition == null){
               if (!pool.getAutoAddColumns()){
                  Log.info("Column "  + columns[y][0] + " from table " + tableName + " not found!, Change configuration to 'auto add columns' to let the system add the column!");
                  continue;
               }
               add_or_modify = "ADD";
               Log.info("Adding new column " + columns[y][0] + " to table " + tableName);
            }else{
               if (!pool.getAutoModifyColumns()){
                  Log.warn("Column "  + columns[y][0] + " from table " + tableName + " does not equal definition! This might cause problems, Change configuration to 'auto modify columns' mode to let the system try to modify the column accordingly!");
                  continue;
               } else if (!readyToModifyColumns && isAS400Driver()){
					connection.executeUpdate(CHGJOB);
					readyToDropColumns = true;
			   }
			   readyToModifyColumns = true;
			   		
               add_or_modify = "MODIFY";
               ColumnDefinition d = columnDefinition;
               String found = d.columnName  + " " + d.typeName    + "("+ d.columnSize + "," + d.decimalDigits + ")" + (d.columnDefinition == null ?"":(" DEFAULT " + d.columnDefinition)) + (d.isNullable.equals("YES")?"":" NOT NULL");
               String required = columns[y][0] + " " + columns[y][1] + " "+(columns[y][2]==null?"":(" DEFAULT " + columns[y][2])) + " " + columns[y][3];
               Log.info("Modifying column " + columns[y][0] + " from table " + tableName + "[ required = \"" + required +  "\", found = \"" + found + "\"]");
            }
 			boolean done = false, initialized = false;
            
			if (add_or_modify.equals("MODIFY") && isNullable(columns[y]) == false && columns[y][2] != null){
				Log.info("Initialise column " + columns[y][0] + ": " + columns[y][2]);
				if (executeUpdate("UPDATE " + pool.getPrefix() + tableName + " SET " + columns[y][0] + " = " + columns[y][2] + " WHERE " + columns[y][0] + " IS  NULL "  , connection))
					Log.info("Column initialized");
				else
					Log.warn("Failed to initialise column " + tableName + "."+ columns[y][0] + ": "  + columns[y][2] + "! See previous messages.");
			}
            
            if (columns[y][3] != null && columns[y][2] != null)
				done = initialized = executeUpdate("ALTER TABLE " + pool.getPrefix() + tableName + " "+add_or_modify+" COLUMN " + columns[y][0] + " " + columns[y][1] + " DEFAULT " + columns[y][2] + " " + columns[y][3], connection);
            if  (!done && columns[y][2] != null)
				done = initialized = executeUpdate("ALTER TABLE " + pool.getPrefix() + tableName + " "+add_or_modify+" COLUMN " + columns[y][0] + " " + columns[y][1] + " DEFAULT " + columns[y][2], connection);
            if  (!done && columns[y][3] != null)
				done = executeUpdate("ALTER TABLE " + pool.getPrefix() + tableName + " "+add_or_modify+" COLUMN " + columns[y][0] + " " + columns[y][1] + " " + columns[y][3], connection);
            if  (!done)
				done = executeUpdate("ALTER TABLE " + pool.getPrefix() + tableName + " "+add_or_modify+" COLUMN " + columns[y][0] + " " + columns[y][1] , connection);
            if  (done){
               if (columnDefinition == null)
                  Log.info("Added column " + columns[y][0] + " to table " + tableName + "!");
               else
                  Log.info("Modified column " + columns[y][0] + " from table " + tableName + "!");
               
               if (!initialized && add_or_modify.equals("ADD") && columns[y][2] != null){
                  	Log.info("Initialise new column " + columns[y][0] + ": " + columns[y][2]);
                  	if (executeUpdate("UPDATE " + pool.getPrefix() + tableName + " SET " + columns[y][0] + " = " + columns[y][2], connection))
                     	Log.info("Column initialized");
                  	else
                     	Log.warn("Failed to initialise new column " + tableName + "."+ columns[y][0] + ": "  + columns[y][2] + "! See previous messages.");
               }
               
               if (pool.getDBMapping().instructionCommentOnColumnSupported()){
                  Log.info("Add description " + columns[y][4] + " to new column " + columns[y][0]);
                  if (executeUpdate("COMMENT ON COLUMN " + pool.getPrefix() + tableName + "." + columns[y][0] + " IS  '" + columns[y][4] + "'", connection))
                     Log.info("Column description added");
                  else
                     Log.warn("Failed to add column description!"+ columns[y][4] + " to new column " + tableName + "."+ columns[y][0] + "! See previous messages.");
               }
            }else{
			   if (add_or_modify.equals("ADD"))	
               		Log.error("Could not add column " + columns[y][0] + " to table " + tableName + "! See previous messages.");
               else
			   		Log.error("Could not modify column " + columns[y][0] + " from table " + tableName + "! See previous messages.");	
            }
         }  
      }
    }
    
    private boolean isAS400Driver(){
    	return pool.getDriver().startsWith("com.ibm.as400.access.AS400JDBCDriver") || pool.getDriver().startsWith("com.ibm.db2.jdbc.app.DB2Driver");
    }
    
	final protected void removeRedundantColumns() throws Exception{
			DBConnection con = null;
			try{
			   con = pool.getConnection();
			   removeRedundantColumns(con);
			}finally{
			   pool.returnConnection(con);
			}
	}  
        
	public void removeRedundantColumns(DBConnection connection) throws Exception {
		PersistenceMetaData[] persistenceMetaData = getPersistenceMetaDataArray();
		for (int i = 0; i < persistenceMetaData.length; i++) {
			PersistenceMetaData pmd = persistenceMetaData[i];
			if (pmd.definesTable() == false)
				continue;
			String tableName = pmd.getTableName(pool);
			try {
				TableDefinition tableDefinition = (TableDefinition) getTableDefinitions().get(tableName.toUpperCase());
				if (tableDefinition != null) 
					removeRedundantColumns(pmd.getTableName(pool),pmd.getColumnDefinitions(pool) ,connection, tableDefinition);
				Iterator amds = pmd.getAssociativeTableDefinitions(pool).values().iterator();
				while (amds.hasNext()) {
					AssociationMetaData amd = (AssociationMetaData) amds.next();
					tableName = amd.getTableName();
					tableDefinition = (TableDefinition) getTableDefinitions().get(tableName.toUpperCase());
					if (tableDefinition != null) 
						removeRedundantColumns(amd.getTableName(),amd.getColumnDefinitions() ,connection, tableDefinition);
				}
			} catch (Exception e) {
				Log.warn("Problem encountered removing redundant columns on table table " + tableName + " using " + pmd.getClass().getName());
				Log.error(e.getMessage());
				e.printStackTrace();
				throw e;
			}
		}
	}
     
	private void removeRedundantColumns(String tableName, String[][] columns,DBConnection connection,TableDefinition tableDefinition)throws Exception {
		Log.info("Start removing redundant columns of table " + tableName);

		HashMap cd = tableDefinition.getColumnDefinitions();
		Iterator it = cd.keySet().iterator();
		while (it.hasNext()) {
			String columnName = (String) it.next();
			if (!containsColumnDefinition(columns, columnName)) {
				if (!pool.getAutoDropColumns()) {
					Log.info("Column "+columnName+" from table "+tableName+" no langer part of the model! Change configuration to 'auto drop columns' to let the system try to drop the column!");
					continue;
				}else if (!readyToModifyColumns && isAS400Driver()){
					connection.executeUpdate(CHGJOB);
					readyToModifyColumns = true; 
			    }
				readyToDropColumns = true;
				Log.info("Dropping column "+columnName+" from table "+tableName);
				if (executeUpdate("ALTER TABLE "+prefix+tableName+" DROP COLUMN "+columnName, connection)){
					Log.info("Column "+columnName+" from table "+tableName+" dropped!");
				}else
					Log.info("Could not drop column "+columnName+" from table "+tableName+"!");
			}
		}
	}
    
    /**
     * Get table definition or null if table does not exists
     * 
     * @param tableName
     * @return tabledefinition or null if table does not exists;
     */	
	public TableDefinition getTableDefinition(String tableName) throws Exception{
		Iterator iterator;
		if (tableDefinitions == null){
			iterator = TableDefinition.getTableDefinitions(tableName, new String[]{"TABLE"},pool).values().iterator();
		}else{
			iterator = getTableDefinitions().values().iterator();
		}
		while (iterator.hasNext()) {
			TableDefinition definition = (TableDefinition) iterator.next();
			if (equalsIgnoreCase(tableName,definition.getTableName()))
				return definition;
		}
		return null;
	}
		
	public boolean equalsIgnoreCase(String a, String b){
		a = a==null||a.trim().length()==0?null:a.trim();
		b = b==null||b.trim().length()==0?null:b.trim();
		if (a == null)
			if (b == null)
				return true;
			else
				return false;
		else
			return a.equalsIgnoreCase(b);			
	}

    
    private static boolean executeUpdate(String query, DBConnection con){
        try{
            con.executeUpdate(query);
            return true;
        }catch (java.sql.SQLException e){
            Log.error(e.getMessage());
            e.printStackTrace();    
        }    
        return false;
    }
    
    public boolean schemaExists(){
    	String schema = pool.getSchema();
		if (schema == null || schema.trim().length() == 0) {
			return true;
		}
      	DBConnection connection = pool.getConnection();    
       	boolean exists = schemaExists(schema, connection);
   		pool.returnConnection(connection);
   		return exists;
    }


    private boolean schemaExists(String schema, DBConnection con) {
		String version = null;
		ResultSet rs = null;
		try {
			rs = con.executeQuery("SELECT " + VERSION_COLUMN + " FROM " + prefix + VERSION_DATA_TABLE + " WHERE " + TYPE_COLUMN + " = '"+ APPLICATION_VERSION +"'",true);
			if (rs.next())
				version = rs.getString(1);
		} catch (SQLException e) {
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {	
				}
	    	
		}
		boolean exists = version != null;
        if (Log.debug())
            Log.debug("Schema " +  schema + (exists ? " exists " : " does not exists "));
        return exists;
    }
    
     
    
      //Custom code
//{CODE-INSERT-BEGIN:429161700}
//{CODE-INSERT-END:429161700}
 



    /*
    static void print(ResultSet rs)  throws java.sql.SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        while(!rs.isAfterLast() && rs.next()){
            System.out.println("*******                   *******");
            for(int x = 0 ; x < columnCount; x ++){
                System.out.print(metaData.getColumnName(x+1));
                System.out.print(" = ");
                System.out.println(rs.getString(x+1));
            } 
        }
    }*/
    
    private static boolean containsColumnDefinition( String [][] columns , String columnName){
        for (int x = 0 ; x < columns.length ; x++){
            //System.out.print(columns[x][0] + " " + columnName);
            if (columns[x][0].equalsIgnoreCase(columnName))
                return true;
            }
        return false;
    }
    
    private boolean equals(ColumnDefinition cd1, String[] cd2) throws Exception{
        if (isNullable(cd1) != isNullable(cd2)){
            Log.info("Column " + cd2[0] + " in database is " + (isNullable(cd1)?"":"not ") + "nullable but should " + (isNullable(cd1)?"":"not ") + "be nullable according to definition!");
            return false;
        }
        if (!translateToDefaultTypeName(sqlType(cd1)).equalsIgnoreCase(translateToDefaultTypeName(sqlType(cd2)))){
			String type1 =  translateToDefaultTypeName(sqlType(cd1)) ;
            String type2 =  translateToDefaultTypeName(sqlType(cd2)) ;
            Log.info("Column " + cd2[0] + " in database is of type " + type1 + " but should be type " + type2 + " according to definition!");
            if (type1 == null ||
                type2 == null ||
               (!type1.equalsIgnoreCase("char") && !type1.equalsIgnoreCase("varchar")) ||
               (type1.equalsIgnoreCase("char") && !type2.equalsIgnoreCase("varchar")) ||
               (type1.equalsIgnoreCase("varchar") && !type2.equalsIgnoreCase("char"))
               ) 
            	return false;
        }
        if (hasLength(cd2) && (length(cd1) != length(cd2))){
			Log.info("Column " + cd2[0] + " in database has length " + length(cd1) + " but should have length " + length(cd2) + " according to definition!");
            return false;
        }
        if (hasDecimals(cd2) && (decimals(cd1) != decimals(cd2))){
            Log.info("Column " + cd2[0] + " in database has " + decimals(cd1) + " decimals but should have " + decimals(cd2) + " decimals according to definition!");
            return false;
        }    
           
        if (!removeEnclosingBrackets(defaultValue(cd1)).equals(removeEnclosingBrackets(defaultValue(cd2)))){
        	//Because some database drivers don't give back the default value we check against stored db meta data 
			if (columnDefintionInAccordanceStoredMetaData(cd1.tableName,cd2))
				return true;
            Log.info("Column " + cd2[0] + " in database has defaultvalue " + defaultValue(cd1) + " but should defaultvalue " + defaultValue(cd2) + " according to definition!");
            return false;
        }
        return true;
       
    }
    
    private static boolean isNullable(ColumnDefinition cd){
        return cd.isNullable.trim().equals("YES");
    }
    private static boolean isNullable(String[] cd){
        return (cd[3].trim().indexOf("NOT NULL") == -1);
    }
    
    private static String sqlType(ColumnDefinition cd){
        return cd.typeName.trim();
    }
    private static String sqlType(String[] cd){
        int i =  cd[1].indexOf('(');
        if (i < 0 )
            return cd[1].trim();
        else
            return cd[1].substring(0, i).trim();
    }
    
    private static int length(ColumnDefinition cd){
        return cd.columnSize;
    }

    private static boolean hasLength(String[] cd){
        return (cd[1].indexOf('(') > -1);
    }

    private static int length(String[] cd){
        if (!(cd[1].indexOf('(') > -1))
            return -1;
        else if (cd[1].indexOf(',') > -1)
            return Integer.parseInt(cd[1].substring(cd[1].indexOf('(') +1, cd[1].indexOf(',')).trim());
        else
            return Integer.parseInt(cd[1].substring(cd[1].indexOf('(') + 1, cd[1].indexOf(')')).trim());
    }
    
    private static boolean hasDecimals(String[] cd){
        return (cd[1].indexOf(',') > -1);
    }
    private static int decimals(ColumnDefinition cd){
        return cd.decimalDigits;
    }
    private static int decimals(String[] cd){
        return Integer.parseInt(cd[1].substring(cd[1].indexOf(',') + 1 , cd[1].indexOf(')')).trim());
    }
    
    private static String defaultValue(ColumnDefinition cd){
        if (cd.columnDefinition == null) 
            return "";
        return cd.columnDefinition;
        
    }

    private static String defaultValue(String[] cd){
        return cd[2]==null?"":cd[2].trim();
    }

    private static String removeEnclosingBrackets(String string){
        if (string == null)
            return "";
        string = string.trim();
        if (string.startsWith("'") && string.endsWith("'"))
            return string.substring(1, string.length()-1);
        if (string.startsWith("\"") && string.endsWith("\""))
            return string.substring(1, string.length()-1);
        return string;
    }

    private static String translateToDefaultTypeName(String type){
        type = type.trim();
        if (type.equalsIgnoreCase("int"))
            return "INTEGER";
        
        return type;    
    }
    
    
    final protected void persistMetaData() throws Exception {
		DBConnection connection = null;
		try{
   			connection = pool.getConnection();
			storeMetaData(connection);
			setDBJSQLVersion(connection);
			setDBApplicationVersion(getNewDataBaseVersion(), connection);
		}finally{
		   pool.returnConnection(connection);
		}	
	}
  	
	private void storeMetaData(DBConnection connection) throws Exception {
		TableDefinition td = getTableDefinition(META_DATA_TABLE);
		
		if (td != null && td.getColumnDefinitions().size() < 9){
			connection.executeUpdate("DROP TABLE " + prefix + META_DATA_TABLE);
			td = null;
		}
			
		if (td == null){
			connection.executeUpdate("CREATE TABLE " + getPrefix() + META_DATA_TABLE + " (" +
			BO_PACKAGE_COLUMN + " " + pool.getDBMapping().getSQLDefinition("String", 250, 0) + " NOT NULL	," +
			BO_NAME_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 125, 0) + " NOT NULL	," +
			BO_ATTRIBUTE_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 125, 0) + " NOT NULL	," +
			TABLE_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 128, 0) + " NOT NULL ," +
			COLUMN_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 30, 0) + " NOT NULL ," +
			SQL_DEFINITION_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 100, 0) + " NOT NULL ," +
			DEFAULT_VALUE_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 250, 0) + " ," +
			EXTENTION_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 100, 0) + " ," +
			DESCRIPTION_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 250, 0) + " , "+
			"PRIMARY KEY("+BO_PACKAGE_COLUMN+", "+BO_NAME_COLUMN+", "+BO_ATTRIBUTE_COLUMN+"))");
			updateMetaData();		
		}else{
			connection.executeUpdate("DELETE FROM " + prefix + META_DATA_TABLE + " WHERE 0=0 ");
		}
		
		PersistenceMetaData[] persistenceMetaData = getPersistenceMetaDataArray();
		for (int i = 0; i < persistenceMetaData.length; i++) {
			PersistenceMetaData pmd = persistenceMetaData[i];
			String tableName = pmd.getTableName(pool);
			String boName = pmd.getBusinessObjectName();
			int xyz = pmd.getClass().getName().lastIndexOf(".impl.");
			String _package = xyz<0?"":pmd.getClass().getName().substring(0,xyz);
			String[][] columnDefinitions = pmd.getPersistingPMD().getColumnDefinitions(pool);
			Iterator it = pmd.getJavaVariableNames().iterator();
			while(it.hasNext()){
				String javaVariableName = (String)it.next();
				String columnName = pmd.getFieldNameForJavaField(javaVariableName,pool);
				if (columnName == null)
					continue;//javaVariable refers not to business object attribute
				String[] columnDefinition = getColumnDefinition(columnName,columnDefinitions);
				insertDBMetaDataRecord(connection,_package, boName, javaVariableName,tableName,columnDefinition);	
			}
			
			Map amds = pmd.getAssociativeTableDefinitions(pool);
			it = amds.keySet().iterator();
			while(it.hasNext()){
				boName = (String)it.next();
				AssociationMetaData amd = (AssociationMetaData)amds.get(boName);
				tableName = amd.getTableName();
				String[][] columnDefinition = amd.getColumnDefinitions();
				for (int j = 0; j < columnDefinition.length; j++) {
					insertDBMetaDataRecord(connection, "["+_package+"]", boName, columnDefinition[j][0].toLowerCase(), tableName,columnDefinition[j]);	
				}
			}
		}
	}


	private void insertDBMetaDataRecord(
		DBConnection connection,
		String _package,
		String boName,
		String attributeName,
		String tableName,
		String[] columnDefinition)
		throws SQLException {
		
		String columnName = null, sqlDefinition = null, defaultValue = null, instuctionExtention = null, columnDescription ="";
		if(columnDefinition != null){
			columnName = columnDefinition[0];
			sqlDefinition = columnDefinition[1];
			defaultValue =	columnDefinition[2];
			instuctionExtention  =  columnDefinition[3];
			columnDescription =columnDefinition[4];		
		}
			connection.executeUpdate("INSERT INTO " + 
			prefix + META_DATA_TABLE + 
			" VALUES ("+
			"'"+escape(_package)+"'" 
			+ "," +
			"'"+escape(boName)+"'" 
			+ "," +
			"'"+escape(attributeName)+"'" 
			+ "," +
			"'"+escape(tableName)+"'" 
			+ "," +
			"'"+escape(columnName)+"'" 
			+ "," +
			"'"+escape(sqlDefinition)+"'" 
			+ "," +
			(defaultValue == null?"NULL":"'"+escape(defaultValue)+"'") 
			+ "," +
			(instuctionExtention == null?"NULL":"'"+escape(instuctionExtention)+"'") 
			+ "," +
			"'"+escape(columnDescription)+"'"+
			")");	
	}
	
	private String escape(String value){
		if (value == null || value.indexOf("'")<0)
			return value;
		StringBuffer b = new StringBuffer();	
		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			if (c == '\'')
				b.append(c);
			b.append(c);
		}
		return b.toString();		
	}
	
	private static String[] getColumnDefinition(String columnName, String[][] columnDefintions){
		for (int i = 0; i < columnDefintions.length; i++) {
			if (columnDefintions[i][0].equalsIgnoreCase(columnName))
				return columnDefintions[i];
		}
		return null;
	}
	
	private boolean columnDefintionInAccordanceStoredMetaData(String tableName, String[] columns) throws Exception {
	
			if (!schemaExists() || columns == null || columns.length < 4)
				return false;
			TableDefinition td = getTableDefinition(META_DATA_TABLE);
			if (td == null)
				return false;
		    DBConnection con = pool.getConnection();	
			String selectClause = SQL_DEFINITION_COLUMN+ " , " + DEFAULT_VALUE_COLUMN + " , " + EXTENTION_COLUMN +", " + DESCRIPTION_COLUMN;
			String table =  prefix + META_DATA_TABLE;
			String whereClause = TABLE_COLUMN + " = '"+ tableName +"' AND " + COLUMN_COLUMN +"= '" +columns[0] +"'";
		    ResultSet rs = null;
			try{
				rs = con.executeQuery("SELECT " + selectClause + " FROM " + table + " WHERE " + whereClause);
				if (rs.next()){
					if (!equalsIgnoreCase(rs.getString(1),columns[1]))
					 return false;
					if (!equalsIgnoreCase(rs.getString(2),columns[2]))
					 return false;
					if (!equalsIgnoreCase(rs.getString(3),columns[3]))
					 return false;
					if (!equalsIgnoreCase(rs.getString(4),columns[4]))
					 return false;
					return true;
				}
			}finally {
				if (rs != null)
					rs.close();
				if (con != null)
					pool.returnConnection(con);	
			}	
			return false;
	}	
	
 
    
	/**
	 * Returns the application version level of the current database, returns null if the database does not exists or no version has been set.
	 * @return db applaction level version or null when no db found.
	 */	
	public String getDBApplicationVersion()throws Exception {
		return getDBVersion(APPLICATION_VERSION);
	}

	/**
	 * Returns the jsql version with wich the current database has been created, returns null if the database does not exists or no has been version set.
	 * @return jsql version of the current database or null when no db found.
	 */
	public String getDBJSQLVersion() throws Exception {
		return getDBVersion(JSQL_VERSION);
	}
	
	protected String getDBVersion(String type) throws Exception {
		    String version = null;
			if (!schemaExists())
				return version;
			TableDefinition td = getTableDefinition(VERSION_DATA_TABLE);
			if (td == null)
				return version;
			DBConnection con = pool.getConnection();
			ResultSet rs = null;
			try{	
				rs = con.executeQuery("SELECT " + VERSION_COLUMN + " FROM " + prefix + VERSION_DATA_TABLE + " WHERE " + TYPE_COLUMN + " = '"+ type +"'");
				if (rs.next())
					version = rs.getString(1);
			}finally{
				if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {	
				}
	    	
			}		
			pool.returnConnection(con);
			return version;	
	}

	/**
	 * Set the new application version level of the database.
	 */	
	private void setDBApplicationVersion(String version, DBConnection connection)throws Exception {
		setDBVersion(APPLICATION_VERSION, version, connection);
	}

	/**
	 * Set the new jsql version level of the database.
	 */
	private void setDBJSQLVersion(DBConnection connection) throws Exception {
		setDBVersion(JSQL_VERSION, CURRENT_JSQL_VERSION, connection);
	}
	
	private void setDBVersion(String type, String version, DBConnection con) throws Exception {
			TableDefinition td = getTableDefinition(VERSION_DATA_TABLE);
			
		    if (td == null){
				con.executeUpdate("CREATE TABLE " + getPrefix() + VERSION_DATA_TABLE + " (" +
        		TYPE_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 25, 0) + " NOT NULL	," +
        		VERSION_COLUMN	 + " " + pool.getDBMapping().getSQLDefinition("String", 25, 0) + " NOT NULL )");
        		updateMetaData();		
			}
			
			con.executeUpdate("DELETE FROM " + prefix + VERSION_DATA_TABLE + " WHERE "+TYPE_COLUMN+"='"+type+"'");
		    con.executeUpdate("INSERT INTO " + prefix + VERSION_DATA_TABLE + " VALUES ('"+type+"','"+version+"')");		

	}
	
	/**
	 * Get the database prefix needed for qualifying
	 * tables, indexes and constraints amongst others
	 * @return prefix or ""
	 */
	public String getPrefix(){
		return prefix;	
	}
	
	/**
	 * Current database version. 
	 * To set the database after the conversion implment the method getNewDBVersion
	 * @return current database version
	 */
	protected String getCurrentDataBaseVersion(){
			return db_application_version==null?null:db_application_version.trim();	
	}
	
	/**
	 * Current database version. 
	 * To set the database after the conversion implment the method getNewDBVersion
	 * @return current database version
	 */
	final protected String getCurrentJSQLVersion(){
			return db_jsql_version==null?null:db_jsql_version.trim();
	}
	

	/**
	 * Return the prefixed table name in wich the BusinessObject is stored
	 * 
	 * @param BusinessObject class
	 * @return prefixed table name
	 * @throws Exception
	 */
	public String getTableName(Class bo) throws Exception{
		String classPackage = bo.getPackage()== null?"":bo.getPackage().getName();
		String className = bo.getName().substring((classPackage.length()==0?0:classPackage.length()+1));
		return getTableName(classPackage, className);	
	}

	/**
	 * Return the prefixed table name in wich the BusinessObject is stored
	 * 
	 * @param String packageName BusinessObject
	 * @param String className BusinessObject
	 * @return prefixed table name
	 * @throws Exception
	 */
	public String getTableName(String classPackage, String className) throws Exception{
		classPackage = classPackage==null?"":classPackage;
		PersistenceMetaData pmd = getPersistentMetaDataByBOName(classPackage,className);
		if (pmd != null)
			return getPrefix()+pmd.getTableName(pool);
		//Not found try stored database meta data because bo object may have been removed from model
		TableDefinition td = getTableDefinition(META_DATA_TABLE);
	    if (td != null && td.getColumnDefinitions().size() >= 9){		
			String selectClause = TABLE_COLUMN;
		 	String table =  getPrefix() + META_DATA_TABLE;
			String whereClause = BO_PACKAGE_COLUMN + " = '"+ classPackage  +"' AND " + BO_NAME_COLUMN +"= '" +className+"'";
			DBConnection con = pool.getConnection();
			ResultSet rs = null;
			try{
				rs = con.executeQuery("SELECT DISTINCT " + selectClause + " FROM " + table + " WHERE " + whereClause);
				if (rs.next())
					return getPrefix() + rs.getString(1);		   
			}finally {
				if (rs != null)
					try {
						rs.close();
					} catch (Exception e) {	
					}
				pool.returnConnection(con);	
			}
	   	}	
	   	throw new Exception ("No mapping to database table found for " + className + " in " + classPackage+ "!");
	}
	
	/**
	 * Return the column name in wich the BusinessObject attribute is stored
	 * 
	 * @param BusinessObject class
	 * @param classAttribute (java field name)
	 * @return column name
	 * @throws Exception
	 */
	public String getColumnName(Class bo, String classAttribute) throws Exception{
		String classPackage = bo.getPackage()== null?"":bo.getPackage().getName();
		String className = bo.getName().substring((classPackage.length()==0?0:classPackage.length()+1));
		return getColumnName(classPackage, className, classAttribute);	
	}


	/**
	 * Return the column name in wich the BusinessObject attribute is stored
	 * 
	 * @param String packageName BusinessObject
	 * @param String className BusinessObject
	 * @param String classAttribute (java field name)
	 * @return column name
	 * @throws Exception
	 */
	public String getColumnName(String classPackage, String className, String classAttribute) throws Exception{
		classPackage = classPackage==null?"":classPackage;
		PersistenceMetaData pmd = getPersistentMetaDataByBOName(classPackage,className);
		if (pmd != null && pmd.containsJavaVariable(classAttribute)){
			return pmd.getFieldNameForJavaField(classAttribute,pool);
		}
		// Not found try stored meta data because field may have been removed from model	
		TableDefinition td = getTableDefinition(META_DATA_TABLE);
		if (td != null && td.getColumnDefinitions().size() >= 9){		
			String selectClause = COLUMN_COLUMN;
			String table =  getPrefix() + META_DATA_TABLE;
			String whereClause = BO_PACKAGE_COLUMN + " = '"+ classPackage  +"' AND " + BO_NAME_COLUMN +"= '" +className+"' AND " + BO_ATTRIBUTE_COLUMN +"= '" +classAttribute+"'";
			DBConnection con = pool.getConnection();
			ResultSet rs = null;
			try{
				rs = con.executeQuery("SELECT DISTINCT " + selectClause + " FROM " + table + " WHERE " + whereClause);
				if (rs.next())
					return rs.getString(1);		   
			}finally {
				if (rs != null)
					try {
						rs.close();
					} catch (Exception e) {	
					}
				pool.returnConnection(con);	
			}
		}	
		throw new Exception ("No database mapping found for attribute " + className + "."+classAttribute+ " in " + classPackage+ "."+classAttribute+"!");
	}
	
	private PersistenceMetaData getPersistentMetaDataByBOName(String _package, String name) throws Exception{
		PersistenceMetaData[] pmds = getPersistenceMetaDataArray();
				for (int i = 0; i < pmds.length; i++) {
					if (pmds[i].getBusinessObjectName().equals(name) && (_package == null || pmds[i].getClass().getName().startsWith(_package)))
						return pmds[i];
				}
		return null;
	}

	
	
    protected void logMetaData() throws java.sql.SQLException {

        try{
            Log.info("Database : " +  meta.getDatabaseProductName() + ", version : " +  meta.getDatabaseProductVersion());
            Log.info("Driver : "   +  meta.getDriverName() + ", version: " +  meta.getDriverVersion() + " [major version="+meta.getDriverMajorVersion()+",minor version="+meta.getDriverMinorVersion()+"]");
            if (meta.supportsANSI92FullSQL())
                Log.info("Database provides full ANSI SQL 92 support.");
            else if (meta.supportsANSI92IntermediateSQL())
                Log.info("Database provides intermediate ANSI SQL 92 support.");
            else if (meta.supportsANSI92EntryLevelSQL())
                Log.info("Database provides entry level ANSI SQL 92 support.");
            else
                Log.info("Database not fully provides entry level ANSI SQL 92 support.");
            Log.info("Database "+(meta.supportsCoreSQLGrammar()?"supports":"does not support") + " the core SQL grammar.");
            Log.info("Database "+(meta.supportsExtendedSQLGrammar()?"supports":"does not support") + " the extended SQL grammar.");
            Log.info("Database uses in addition to SQL 92 the following SQL keywords: " +  meta.getSQLKeywords());
            Log.info("Database "+(meta.supportsAlterTableWithAddColumn()?"supports":"does not support") + " the AlterTable With Add Column instruction.");
            Log.info("Database "+(meta.supportsAlterTableWithDropColumn()?"supports":"does not support") + " the AlterTable With Drop Column instruction.");
            Log.info("Database "+(meta.supportsFullOuterJoins()?"fully supports":"does not fully supports") + " Outher Joins.");
            if (!meta.supportsFullOuterJoins()){
                Log.info("Database "+(meta.supportsLimitedOuterJoins()?"has limmited support for ":"does not support") + " Outher Joins.");
            }
            Log.info("Database "+(meta.supportsTransactions()?"supports":"does not support") + " Transactions.");
            if (meta.supportsTransactions()){
                if (meta.getDefaultTransactionIsolation() == java.sql.Connection.TRANSACTION_NONE)
                    Log.info("Default transaction isolationlevel = \"NONE\"");
                if (meta.getDefaultTransactionIsolation() == java.sql.Connection.TRANSACTION_READ_COMMITTED)
                    Log.info("Default transaction isolationlevel = \"READ COMMITED\"");
                if (meta.getDefaultTransactionIsolation() == java.sql.Connection.TRANSACTION_READ_UNCOMMITTED)
                    Log.info("Default transaction isolationlevel = \"READ UNCOMMITED\"");
                if (meta.getDefaultTransactionIsolation() == java.sql.Connection.TRANSACTION_REPEATABLE_READ)
                    Log.info("Default transaction isolationlevel = \"REPEATABLE READ\"");
                if (meta.getDefaultTransactionIsolation() == java.sql.Connection.TRANSACTION_SERIALIZABLE)
                    Log.info("DB uses as default transaction isolationlevel: \"SERIALIZABLE\"");
                Log.info("Database "+(meta.supportsTransactionIsolationLevel(java.sql.Connection.TRANSACTION_READ_COMMITTED)?"supports":"does not support") + " transaction isolation level \"READ COMMITTED\".");
                Log.info("Database "+(meta.supportsTransactionIsolationLevel(java.sql.Connection.TRANSACTION_READ_UNCOMMITTED)?"supports":"does not support") + " transaction isolation level \"READ UNCOMMITTED\".");
                Log.info("Database "+(meta.supportsTransactionIsolationLevel(java.sql.Connection.TRANSACTION_REPEATABLE_READ)?"supports":"does not support") + " transaction isolation level \"REPEATABLE READ\".");
                Log.info("Database "+(meta.supportsTransactionIsolationLevel(java.sql.Connection.TRANSACTION_SERIALIZABLE)?"supports":"does not support") + " transaction isolation level \"SERIALIZABLE\".");
            }

            Log.info("Database "+(meta.supportsUnion()?"supports":"does not support") + " Union.");
            Log.info("Database "+(meta.supportsUnionAll()?"supports":"does not support") + " Union All.");
            Log.info("Database "+(meta.supportsBatchUpdates()?"supports":"does not support") + " Batch Updates.");
            if(meta.getMaxSchemaNameLength() > 0)
                Log.info("Max length schema names: " +  meta.getMaxSchemaNameLength());
            if(meta.getMaxTableNameLength() > 0)
                Log.info("Max length table names: " +  meta.getMaxTableNameLength());
            if(meta.getMaxColumnNameLength() > 0)
                Log.info("Max length column names: " +  meta.getMaxColumnNameLength());
            if(meta.getMaxStatementLength() > 0)
                Log.info("Max length statements: " +  meta.getMaxStatementLength());
            if(meta.getMaxStatements() > 0)
                Log.info("Max number of statements: " +  meta.getMaxStatements());
            if(meta.getMaxConnections() > 0)
                Log.info("Max number of connections: " +  meta.getMaxConnections());
            if(meta.getMaxColumnsInTable() > 0)
                Log.info("Max number of columns in table: " +  meta.getMaxColumnsInTable());
            if(meta.getMaxColumnsInIndex() > 0)
                Log.info("Max number of columns in index: " +  meta.getMaxColumnsInIndex());
            if(meta.getMaxColumnsInSelect() > 0)
                Log.info("Max number of columns in select: " +  meta.getMaxColumnsInSelect());
            if(meta.getMaxColumnsInOrderBy() > 0)
                Log.info("Max number of columns in order by: " +  meta.getMaxColumnsInOrderBy());
            if(meta.getMaxColumnsInGroupBy() > 0)
                Log.info("Max number of columns in group by: " +  meta.getMaxColumnsInGroupBy());

            if (meta.usesLocalFiles())
                Log.info("Database stores tables in a local file");
            if (meta.usesLocalFilePerTable())
                Log.info("Database uses a file for each table");
            if (meta.storesLowerCaseIdentifiers())
                Log.info("Database treats mixed case unquoted SQL identifiers as case insensitive and stores them in lower case.");
            if (meta.storesLowerCaseQuotedIdentifiers())
                Log.info("Database treats mixed case quoted SQL identifiers as case insensitive and stores them in lower case.");
            if (meta.storesMixedCaseIdentifiers())
                Log.info("Database treats mixed case unquoted SQL identifiers as case insensitive and stores them in mixed case.");
            if (meta.storesMixedCaseQuotedIdentifiers())
                Log.info("Database treats mixed case quoted SQL identifiers as case sensitive and as a result stores them in mixed case.");
            if (meta.storesUpperCaseIdentifiers())
                Log.info("Database treats mixed case unquoted SQL identifiers as case insensitive and stores them in upper case.");
            if (meta.storesUpperCaseQuotedIdentifiers())
                Log.info("Database treats mixed case quoted SQL identifiers as case insensitive and stores them in upper case.");
            if (!meta.getIdentifierQuoteString().equals(" "))
                Log.info("String used to quote SQL identifiers : " +  meta.getIdentifierQuoteString());
            Log.info("This database "+(meta.supportsLikeEscapeClause()?"does":"does not")+" support specifying a LIKE escape clause");    
			Log.info("String to escape wildcard characters with this database : " +  meta.getSearchStringEscape());    
            Log.info("String functions available with this database: " + meta.getStringFunctions());
			Log.info("Numeric functions available with this database: " + meta.getNumericFunctions());
			Log.info("System functions available with this database: " + meta.getSystemFunctions());
			Log.info("Time Date functions available with this database: " + meta.getTimeDateFunctions());            
                
        }catch(Exception e){
            Log.error(e.getMessage());
            e.printStackTrace();
        }    
        
    }

	public boolean isFinalized() {
		return finalized;
	}


	public boolean isInitialized() {
		return initialized;
	}


	public DBConnectionPool getPool() {
		return pool;
	}


	public boolean isPostConverted() {
		return postConverted;
	}


	public boolean isPreConverted() {
		return preConverted;
	}

}

