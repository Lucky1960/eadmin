package nl.ibs.jsql.sql;


import java.io.Serializable;
import java.util.Map;
import nl.ibs.jsql.sql.BusinessObjectManager;

/**
 * @author c_hc01
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface SuperQuery extends Serializable {
	
   /**
	* Hints the implementation whether or not it should apply a cached PreparedStatement. <br/>
	* Default value is false: no prepared statements will be used. 
	* <p>
	* Use the default value for all cases where de query is executed once or only several times without modifications (other then changing the parameters).
	* 
	*
	* @param cacheable a boolean hinting the implementation whether or not it should apply a cached PreparedStatement.</A>.
	*/
   public void setCacheable(boolean cacheable);
   
   /**
	* Returns the boolean whether or not the implementation is instructed to use a cached PreparedStatement.
	*
	* @return true if the implementation is instructed to apply a cached PreparedStatement, false otherwise.
	* @see Query#setCacheable(boolean cacheable)
	*/
	   public boolean getCacheable();      
       
   /**
	* Sets the maximum objects the query should return. <br/>
	* Deault value is 0: there is no limit to the number of object to return.
	*
	* @param max a positive number determining the maximum number of objects to return or zero if there should be no limit to 
	* the number of objects to return 
	*/
   public void setMaxObjects(int max);
   
   /**
	* Returns the maximum objects the query returns or zero if there is no limit set. 
	*
	* @return the maximum objects the query returns or zero if there is no limit set.
	* @see Query#setMaxObjects(int max)
	*/
   public int  getMaxObjects();
      
   /**
	* Add an BusinessObject with no alias name to be referenced in the filter. 
	* 
	* @param businessObjectClass a BusinessObject Class to use in a filter statement referenced by its unqualified classname. 
	*/
   public void addImport(Class businessObjectClass) throws Exception;
  
   /**
	* Add an BusinessObject with an alias name to be referneced in the filter. 
	* 
	* @param alias a aliasname to referennce the imported BusinessObject.  
	* @param businessObjectClass a BusinessObject Class to reference in a filter statement. 
	*/
   public void addImport(String alias, Class businessObjectClass) throws Exception;
  
   /**
	* Add an BusinessObject with no alias name to be referenced in the filter. 
	* 
	* @param manager a BusinessObjectMager from a BusinessObject to use in a filter statement referenced by its unqualified classname. 
	*/
   public void addImport(BusinessObjectManager manager) throws Exception;
  
   /**
	* Add an BusinessObject with an alias name to be referenced in the filter. 
	* 
	* @param alias a aliasname to referennce the imported BusinessObject.  
	* @param manager a BusinessObjectMager from a BusinessObject to use in a filter statement. 
	*/
   public void addImport(String alias, BusinessObjectManager manager) throws Exception;
 
  
   /**
	* Replaces current parameter map with the new map.
	* <p>
	* The map should contain the parameter names as keys and Objects als their values. Object types should be in compliance with associated types
	* in the filter statement. 
	* For instance suppose the next filter is set (or will be set):<br/></blockquote>
	* <blockquote><code>" Member.name == ?name && Member.dataOfBirth > ?dateOfBirth && Member.numberOfSChildren < ?numberOfChildren "</code> <br/>
	* and the BusinessObject Member has among others the mext instance variables:
	* <ul>
	*  <li>name of type String</li> 
	*  <li>dateOfBirth of type Date</li> 
	*  <li>numberOfChildren of type int</li> 
	* </ul>
	* Then the supplied map should contain:
	* <ul>
	*  <li>an entry with as key "name" and as value a String object</li> 
	*  <li>an entry with as key "dateOfBirth" and as value a Date object</li> 
	*  <li>an entry with as key "numberOfChildren" and as its value an Integer object</li> 
	* </ul>
	* 
	* @param map new parameter map
	*
	*/
   public void setParameters(Map map);  
 
   /**
	* Set or replaces parameter with given name with a (new) value.
	*
	* @param parameterName the name of the parameter used in the filter
	* @param value the value of the parameter
	* @see Query#setParameters(Map map)
	*/
   public void setParameter(String parameterName,  Object value);
 
   /**
	* Conveniance method for the setting of parameters of type Long or long.<br>
	* Wrappes primitive in Wrapper Class (Long) and calls <a href="Query#setParameter(String parameterName,  Object value)">setParameter(String parameterName,  Object value)</A>.
	*
	* @param parameterName the name of the parameter used in the filter
	* @param value the value of the parameter
	* @see Query#setParameters(Map map)
	*/
   public void setParameter(String parameterName,  long value);
  
   /**
	* Conveniance method for the setting of parameters of type Integer or int.<br>
	* Wrappes primitive in Wrapper Class (Integer) and calls <a href="Query#setParameter(String parameterName,  Object value)">setParameter(String parameterName,  Object value)</A>.
	*
	* @param parameterName the name of the parameter used in the filter
	* @param value the value of the parameter
	* @see Query#setParameters(Map map)
	*/
   public void setParameter(String parameterName,  int value);
  
   /**
	* Conveniance method for the setting of parameters of type Double or double.<br>
	* Wrappes primitive in Wrapper Class (Double) and calls <a href="Query#setParameter(String parameterName,  Object value)">setParameter(String parameterName,  Object value)</A>.
	*
	* @param parameterName the name of the parameter used in the filter
	* @param value the value of the parameter
	* @see Query#setParameters(Map map)
	*/
   public void setParameter(String parameterName,  double value);
 
   /**
	* Conveniance method for the setting of parameters of type Float or float.<br>
	* Wrappes primitive in Wrapper Class (Float) and calls <a href="Query#setParameter(String parameterName,  Object value)">setParameter(String parameterName,  Object value)</A>.
	*
	* @param parameterName the name of the parameter used in the filter
	* @param value the value of the parameter
	* @see Query#setParameters(Map map)
	*/
   public void setParameter(String parameterName,  float value);
 
   /**
	* Conveniance method for the setting of parameters of type Short or short.<br>
	* Wrappes primitive in Wrapper Class (Short) and calls <a href="Query#setParameter(String parameterName,  Object value)">setParameter(String parameterName,  Object value)</A>.
	*
	* @param parameterName the name of the parameter used in the filter
	* @param value the value of the parameter
	* @see Query#setParameters(Map map)
	*/
   public void setParameter(String parameterName,  short value);
  
   /**
	* Conveniance method for the setting of parameters of type Boolean or boolean.<br>
	* Wrappes primitive in Wrapper Class (Boolean) and calls <a href="Query#setParameter(String parameterName,  Object value)">setParameter(String parameterName,  Object value)</A>.
	*
	* @param parameterName the name of the parameter used in the filter
	* @param value the value of the parameter
	* @see Query#setParameters(Map map)
	*/
   public void setParameter(String parameterName,  boolean value);
   
   
}

