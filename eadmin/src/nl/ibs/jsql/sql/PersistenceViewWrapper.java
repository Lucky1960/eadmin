package nl.ibs.jsql.sql;



import java.util.Map;
import java.util.Set;

/**
 * @author c_md07
 *
 */
public class PersistenceViewWrapper implements ExtendedPersistenceMetaData {
	private PersistenceMetaData metaData = null;
	private String viewName = "";
	/**
	 * 
	 */
	public PersistenceViewWrapper(
		PersistenceMetaData metaData,
		String viewName) {
		super();
		this.metaData = metaData;
		this.viewName = viewName;
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.ExtendedPersistenceMetaData#getClassNameConstraint(nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String getClassNameConstraint(ConnectionProvider provider) {
		if (metaData instanceof ExtendedPersistenceMetaData) {

			String raw =
				(
					(
						ExtendedPersistenceMetaData) metaData)
							.getClassNameConstraint(
					provider);
			if (raw == null) return null;

			return ("(" + provider.getPrefix()+
				getTableName(provider) + raw.substring(raw.lastIndexOf('.')));
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getBusinessObjectName()
	 */
	public String getBusinessObjectName() {
		return metaData.getBusinessObjectName();
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getTableName(nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String getTableName(ConnectionProvider provider) {
		return viewName;
	}
	
	public PersistenceMetaData getPersistingPMD(){
    	return metaData.getPersistingPMD();
    }

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getTableName(java.lang.String, nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String getTableName(
		String objectName,
		ConnectionProvider provider) {
		return metaData.getTableName(objectName, provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getTableDescription()
	 */
	public String getTableDescription() {
		return metaData.getTableDescription();
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getFieldName(java.lang.String, nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String getFieldName(String fieldName, ConnectionProvider provider) {
		return metaData.getFieldName(fieldName, provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getFieldName(java.lang.String, java.lang.String, nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String getFieldName(
		String fieldName,
		String objectName,
		ConnectionProvider provider) {
		return metaData.getFieldName(fieldName, objectName, provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getFieldNameForJavaField(java.lang.String, nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String getFieldNameForJavaField(
		String javaFieldName,
		ConnectionProvider provider) {
		return metaData.getFieldNameForJavaField(javaFieldName, provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getFieldDescription(java.lang.String)
	 */
	public String getFieldDescription(String fieldName) {
		return metaData.getFieldDescription(fieldName);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getJavaPKFieldNames()
	 */
	public Set getJavaPKFieldNames() {
		return metaData.getJavaPKFieldNames();
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getIndexDefinitions(nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public Map getIndexDefinitions(ConnectionProvider provider) {
		return metaData.getIndexDefinitions(provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getConstraintDefinitions(nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public Map getConstraintDefinitions(ConnectionProvider provider) {
		return metaData.getConstraintDefinitions(provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getRelationType(java.lang.String)
	 */
	public String getRelationType(String javaVariableNameOfRelation) {
		return metaData.getRelationType(javaVariableNameOfRelation);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getRelationMultiplicity(java.lang.String)
	 */
	public String getRelationMultiplicity(String javaVariableNameOfRelation) {
		return metaData.getRelationMultiplicity(javaVariableNameOfRelation);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#isBackwardReference(java.lang.String)
	 */
	public boolean isBackwardReference(String javaVariableNameOfRelation) {
		return metaData.isBackwardReference(javaVariableNameOfRelation);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getAssociationTableName(java.lang.String, nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String getAssociationTableName(
		String javaVariableNameOfRelation,
		ConnectionProvider provider) {
		return metaData.getAssociationTableName(
			javaVariableNameOfRelation,
			provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getTableDefinition(nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String getTableDefinition(ConnectionProvider provider) {
		return metaData.getTableDefinition(provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getAssociativeTableDefinitions(nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public Map getAssociativeTableDefinitions(ConnectionProvider provider) {
		return metaData.getAssociativeTableDefinitions(provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getColumnDefinitions(nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String[][] getColumnDefinitions(ConnectionProvider provider) {
		return metaData.getColumnDefinitions(provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getRelationPersistenceMetaData(java.lang.String)
	 */
	public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName) {
		return metaData.getRelationPersistenceMetaData(javaFieldName);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getRelationColomnPairs(java.lang.String, nl.ibs.jsql.sql.ConnectionProvider)
	 */
	public String[][] getRelationColomnPairs(
		String javaFieldName,
		ConnectionProvider provider) {
		return metaData.getRelationColomnPairs(javaFieldName, provider);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getManyToManyColomnPairs(java.lang.String, nl.ibs.jsql.sql.ConnectionProvider, java.lang.String)
	 */
	public String[][] getManyToManyColomnPairs(
		String javaFieldName,
		ConnectionProvider provider,
		String type) {
		return metaData.getManyToManyColomnPairs(javaFieldName, provider, type);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#containsJavaVariable(java.lang.String)
	 */
	public boolean containsJavaVariable(String javaFieldName) {
		return metaData.containsJavaVariable(javaFieldName);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#definesTable()
	 */
	public boolean definesTable() {
		return metaData.definesTable();
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#isPersistable(java.lang.Object)
	 */
	public boolean isPersistable(Object object) {
		return metaData.isPersistable(object);
	}
	
	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.PersistenceMetaData#getJavaVariableNames()
	 */
	public Set getJavaVariableNames(){
    	return metaData.getJavaVariableNames();
    }
}

