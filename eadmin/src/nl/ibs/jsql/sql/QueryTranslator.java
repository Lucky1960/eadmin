package nl.ibs.jsql.sql;


import java.util.*;
import java.math.*;

public class QueryTranslator {
	private static HashMap operands = new HashMap();
	private static HashMap functions = new HashMap(); //TODO not yet implemented
	private static Set methodNames = new HashSet();
	private static Set compareOperands = new HashSet();
	private static Set andOperands = new HashSet();
	private static Set orOperands = new HashSet();
	private static String delimeters = "\n\r\t,()[]+-*/<>=:|$&!. ";
	private static final String variablePrefix = "?";
	private static String[] andSymbols = new String[] { "AND", "and", "&&", "&" };

	static {

		methodNames.add("startsWith");
		methodNames.add("endsWith");
		methodNames.add("isEmpty");
		methodNames.add("contains");

		andOperands.add("AND");
		andOperands.add("and");
		andOperands.add("&&");
		andOperands.add("&");

		orOperands.add("OR");
		orOperands.add("or");
		orOperands.add("||");
		orOperands.add("|");

		compareOperands.add("=");
		compareOperands.add("==");
		compareOperands.add("!=");
		compareOperands.add(">");
		compareOperands.add("<");
		compareOperands.add("<>");
		compareOperands.add("<=");
		compareOperands.add(">=");
		compareOperands.add("like");
		compareOperands.add("LIKE");

//Marco changed
		operands.put("select", "SELECT");
		operands.put("create", "CREATE");
		operands.put("view", "VIEW");
		operands.put("as", "AS");
		operands.put("on", "ON");
		operands.put("drop", "DROP");
//Marco end

		operands.put("is","IS");
		operands.put("distinct", "DISTINCT");
		operands.put("from", "FROM");
		operands.put("where", "WHERE");
		operands.put("and", "AND");
		operands.put("or", "OR");
		operands.put("on", "ON");
		operands.put("between", "BETWEEN");
		operands.put("as", "AS");
		operands.put("in", "IN");
		operands.put("having", "HAVING");
		operands.put("order", "ORDER"); //order by
		operands.put("group", "GROUP"); //group by
		operands.put("for", "FOR"); // for all
		operands.put("all", "ALL"); //for all
		operands.put("by", "BY"); //group by and order by;
		operands.put("asc", "ASC");
		operands.put("desc", "DESC");
		operands.put("ascending", "ASC");
		operands.put("descending", "DESC");
		operands.put("exists", "EXISTS");
		operands.put("any", "ANY");
		operands.put("all", "ALL");
		operands.put("sum", "SUM");
		operands.put("min", "MIN");
		operands.put("max", "MAX");
		operands.put("avg", "AVG");
		operands.put("abs", "ABS");
		operands.put("count", "COUNT");
		operands.put("like", "LIKE");
		operands.put("not", "NOT");
		operands.put("some", "SOME");
		operands.put("inner", "INNER");
		operands.put("outher", "OUTHER");
		operands.put("join", "JOIN");
		operands.put("union", "UNION");
		operands.put("except", "EXCEPT");
		operands.put("intersect", "INTERSECT");
		operands.put("escape", "ESCAPE");
		
		functions.put("concat", "CONCAT");
		functions.put("mod", "MOD");
		functions.put("substring", "SUBSTRING");
		
		functions.put("replace", "REPLACE");
		
		functions.put("round", "ROUND");
		
		functions.put("ltrim", "LTRIM");
		functions.put("rtrim", "RTRIM");
		functions.put("trim", "TRIM");
		
		functions.put("upper", "UPPER");
		functions.put("lower", "LOWER");

		functions.put("length", "LENGTH");

		functions.put("truncate", "TRUNCATE");
		functions.put("ceiling", "CEILING");
		functions.put("floor", "FLOOR");

		functions.put("coalesce", "COALESCE");

		functions.put("year", "YEAR");
		functions.put("quarter", "QUARTER");
		functions.put("month", "MONTH");
		functions.put("week", "WEEK");
		functions.put("day", "DAY");
		functions.put("hour", "HOUR");
		functions.put("minute", "MINUTE");
		functions.put("second", "SECOND");
		functions.put("dayOfYear", "DAYOFYEAR");
		functions.put("dayOfMonth","DAYOFMONTH"); // Was temperarely ditched because was not yet supported for DB2
		functions.put("dayOfWeek", "DAYOFWEEK");

		functions.put("hex", "HEX");
		functions.put("cast", "CAST");
		functions.put("decimal", "DECIMAL");
        functions.put("char", "CHAR");
        functions.put("digits", "DIGITS");
        functions.put("left", "LEFT");
        functions.put("strip", "STRIP");
        functions.put("position", "POSITION");
        functions.put("curdate", "CURDATE");
        functions.put("curtime", "CURTIME");
        functions.put("timestamp", "TIMESTAMP");
        functions.put("zoned", "ZONED");
        functions.put("ifnull", "IFNULL");
        functions.put("nullif", "NULLIF");
       
		
	}

	public static String translateFreeQuery(String selectClause, PersistenceMetaData pmd, ConnectionProvider provider, Map imports) throws Exception {
		return translateQuery(selectClause, pmd, provider, imports);
	}

	public static String translateObjectSelectClause(String selectClause, PersistenceMetaData pmd, ConnectionProvider provider, Map imports) throws Exception {
		String ts = translateQuery(selectClause, pmd, provider, imports);
		return provider.getPrefix() + pmd.getTableName(provider) + ".* " + ((ts == null || ts.trim().equals("")) ? "" : " , " + ts);
	}

	public static String translateObjectFromClause(Set tables, PersistenceMetaData pmd, ConnectionProvider provider, Map imports) throws Exception {
		String baseTable = pmd.getTableName(provider);
		String prefix = provider.getPrefix();
		String from = provider.getPrefix() + baseTable;
		if (!tables.isEmpty()) {
			Iterator it = tables.iterator();
			while (it.hasNext()) {
				String table = (String) it.next();
				if (!baseTable.equals(table)) {
					from += ", ";
					from += provider.getPrefix();
					from += table;
				}
			}
		}
		if (!tables.contains(baseTable))
			tables.add(baseTable);
		return from;
	}

	public static String translateObjectFilterClause(String filterClause, PersistenceMetaData pmd, ConnectionProvider provider, Map imports) throws Exception {
		return translateQuery(filterClause, pmd, provider, imports);
	}

	public static String translateObjectSetClause(String setClause, PersistenceMetaData pmd, ConnectionProvider provider, Map imports) throws Exception {
		return translateQuery(setClause, pmd, provider, imports);
	}

	public static void processObjectOrderClause(
		String orderByClause,
		PersistenceMetaData pmd,
		ConnectionProvider provider,
		Map imports,
		Set tables,
		StringBuffer orderBuffer,
		StringBuffer fromBuffer,
		StringBuffer selectBuffer)
		throws Exception {
		processOrder(orderByClause, pmd, provider, imports, tables, orderBuffer, fromBuffer, selectBuffer);
	}

	//temperaly method in order to keep backward compatable
	public static String translateObjectQuery(String query, PersistenceMetaData pmd, ConnectionProvider provider) throws Exception {
		return translateQuery(query, pmd, provider, null);
	}

	private static void processOrder(
		String order,
		PersistenceMetaData pmd,
		ConnectionProvider provider,
		Map imports,
		Set tables,
		StringBuffer orderBuffer,
		StringBuffer fromBuffer,
		StringBuffer selectBuffer)
		throws Exception {
		if (order == null || order.trim().equals(""))
			return;
		Stack stack = new Stack();
		stack.push(pmd);
		Vector components = parseQuery(order);
		processOrder(components, 0, components.size(), provider, imports, stack, new HashSet(tables), orderBuffer, fromBuffer, selectBuffer);
	}

	private static void processOrder(
		Vector components,
		int start,
		int end,
		ConnectionProvider provider,
		Map imports,
		Stack stack,
		Set tables,
		StringBuffer orderBuffer,
		StringBuffer fromBuffer,
		StringBuffer selectBuffer)
		throws Exception {
		end = Math.min(end, components.size());
		for (int x = start; x < end; x++) {
			String component = (String) components.get(x);
			try {
				//First translate Java operators to SQL operators and add them to the query
				if (component.equals(" ")) {
					orderBuffer.append(" ");
				} else if (component.equals(",")) {
					orderBuffer.append(",");
				} else if (component.equals(".")) {
					orderBuffer.append(" ");
				} else if (component.equals("ascending")) {
					orderBuffer.append("ASC");
				} else if (component.equals("descending")) {
					orderBuffer.append("DESC");
				} else {

					PersistenceMetaData pmd = null;
					String variable = null;

					// In next case component will always be a variable name
					if (x >= end - 1 || nextIsMethod(x, end, components) || isSpaceChar(components.get(x + 1))) {
						pmd = getPersistenceMetaDataContainingVariable(components, x, imports, stack);
						// Otherwise we realy don�t know but we start assuming it is a object name
					} else {
						pmd = getPersistenceMetaDataByName(component, imports, stack);
						if (pmd != null) {
							//So its an object so variable should be first nonSpaceChar componend after next dot
							while (x < end - 1 && (isSpaceChar(components.get(++x)) || isDot(components.get(x))));
						} else {
							//So our first gues was wrong it was not a object name lets check if it could be a variable name
							pmd = getPersistenceMetaDataContainingVariable(components, x, imports, stack);
						}
					}
					variable = (String) components.get(x);

					if (pmd == null)
						throw new Exception("JSQL: Can not process " + variable + " in order definition near " + getQueryFragmentNearPointer(x, components));

					PersistenceMetaData relationPersistenceMetaData = pmd.getRelationPersistenceMetaData(variable);

					if (relationPersistenceMetaData == null) {
						// if it is not a relation it must be a variable!!
						if (!selectBuffer.toString().trim().equals(""))
							selectBuffer.append(", ");

						//String name = " JSQLO_" + String.valueOf((pmd.getBusinessObjectName() + "_" + variable).hashCode()).replace('-', 'N')+ "_" + orderBuffer.length();
						String name = " JSQLOA_" + orderBuffer.length();

						selectBuffer.append(getTableName(pmd, provider));
						selectBuffer.append(".");
						String fieldName = pmd.getFieldNameForJavaField(variable, provider);
						if (fieldName == null) {
							fieldName = pmd.getFieldNameForJavaField(getDependentObjectVariableName(components, x), provider);
							while (x < end - 1 && (isSpaceChar(components.get(++x)) || isDot(components.get(x))));
						}
						selectBuffer.append(fieldName);
						selectBuffer.append(" AS ");
						selectBuffer.append(name);

						orderBuffer.append(name);

					} else {

						if (nextNonWhiteSpaceIsDot(x, end, components)) {

							// perform some additional checks
							String multiplicity = pmd.getRelationMultiplicity(variable);
							boolean backwardReference = pmd.isBackwardReference(variable);
							if (PersistenceMetaData.MANY_TO_MANY.equals(multiplicity))
								throw new Exception(
									"JSQL: Can not process "
										+ variable
										+ " in order definition near "
										+ getQueryFragmentNearPointer(x, components)
										+ " because it si not allowed to "
										+ "use 'many to many' relation in a order clause!");
							if (PersistenceMetaData.ONE_TO_MANY.equals(multiplicity) && !backwardReference)
								throw new Exception(
									"JSQL: Can not process "
										+ variable
										+ " in order definition near "
										+ getQueryFragmentNearPointer(x, components)
										+ " because it is not allowed to "
										+ "navigate forwards through a 'one to many' relation in a order clause!");
							if (PersistenceMetaData.MANY_TO_ONE.equals(multiplicity) && backwardReference)
								throw new Exception(
									"JSQL: Can not process "
										+ variable
										+ " in order definition near "
										+ getQueryFragmentNearPointer(x, components)
										+ " because it is not allowed to "
										+ "navigate backwards through a 'many to one' relation in a order clause!");

							String[][] collumnPairs = pmd.getRelationColomnPairs(variable, provider);

							// OK First build left outher join on from component
							String tableName = relationPersistenceMetaData.getTableName(provider);
							if (!tables.contains(tableName)) {
								tables.add(tableName);
								fromBuffer.insert(0, "( ");
								fromBuffer.append(" LEFT OUTER JOIN ");
								fromBuffer.append(getTableName(relationPersistenceMetaData, provider));
								fromBuffer.append(" ON (");

								for (int i = 0; i < collumnPairs.length; i++) {
									if (i > 0)
										fromBuffer.append(" AND ");
									fromBuffer.append(collumnPairs[i][1]);
									fromBuffer.append(" = ");
									fromBuffer.append(collumnPairs[i][0]);
								}

								fromBuffer.append(" )) ");
								tables.add(tableName);

							}

							// OK now add fragment to ordering clause
							x = getIndexNextDot(x, end, components) + 1;
							int y = x;
							while (++y < end && !((String) components.get(y)).equals(","));
							stack.push(relationPersistenceMetaData);
							processOrder(components, x, Math.min(y, components.size()), provider, imports, stack, tables, orderBuffer, fromBuffer, selectBuffer);
							x = y - 1;
							stack.pop();
							continue;

						}

					}
				}
			} catch (Exception e) {
				if (e.getMessage() != null && e.getMessage().startsWith("JSQL"))
					throw e;
				e.printStackTrace();
				throw new Exception("JSQL: Error in order declaration near " + getQueryFragmentNearPointer(x, components));
			}
		}
	}

	private static String translateQuery(String query, PersistenceMetaData pmd, ConnectionProvider provider, Map imports) throws Exception {
		if (query == null || query.trim().equals(""))
			return null;
		Stack stack = new Stack();
		HashMap aliases = new HashMap();
		stack.push(pmd);
		Vector components = parseQuery(query);
		StringBuffer resultBuffer = new StringBuffer();
		translateQuery(components, 0, components.size(), provider, imports, stack, aliases, resultBuffer);
		//System.out.println("result: " + resultBuffer.toString());
		return resultBuffer.toString();
	}

	private static StringBuffer translateQuery(Vector components, int start, int end, ConnectionProvider provider, Map imports, Stack stack, Map aliases, StringBuffer result)
		throws Exception {
		end = Math.min(end, components.size());
		for (int x = start; x < end; x++) {
			String component = (String) components.get(x);

			try {
				//First add preTranslated parts to the query
				if (component.startsWith("###")) {
					result.append(component.substring(3));
					//Next translate Java operators to SQL operators and add them to the query
				} else if (component.equals(".")) {
					result.append(".");
				} else if (component.equals("&")) {
					if (x < end) {
						String next = (String) components.get(x + 1);
						if (next.equals("&"))
							x++;
					}
					result.append("AND");
					if (x < end) {
						String next = (String) components.get(x + 1);
						if (!next.equals(" "))
							result.append(" ");
					}
				} else if (component.equals("|")) {
					if (x < end) {
						String next = (String) components.get(x + 1);
						if (next.equals("|")) {
							x++;
						}
					}
					result.append("OR");
					if (x < end) {
						String next = (String) components.get(x + 1);
						if (!next.equals(" "))
							result.append(" ");
					}
				} else if (isAS(x,components)){
					result.append(" AS ");
					x = getIndexNextNonSpaceChar(x+1,end,components);
					result.append(components.get(x));	
				} else if (isNULL(component)) {
					result.append("NULL");
				} else if (component.equalsIgnoreCase("true") || component.equalsIgnoreCase("false")) {
					String type = provider.getDBMapping().getSQLDefinition("boolean");
					if (type.indexOf("CHAR") >= 0 || type.indexOf("STRING") >= 0)
						result.append("'" + (component.equalsIgnoreCase("true") ? "true" : "false") + "'");
					else if (type.indexOf("BIT") >= 0 || type.indexOf("INT") >= 0 || type.indexOf("NUM") >= 0 || type.indexOf("DEC") >= 0)
						result.append(component.equalsIgnoreCase("true") ? "1" : "0");
					else
						result.append(component);
				} else if (component.equals("!")) {
					if (x < end && ((String) components.get(x + 1)).equals("=")) {
						x++;
						result.append(nextNonWhiteSpaceIsNULL(x, end, components) ? " IS NOT " : "<>");
					} else {
						result.append("NOT");
					}
					if (x < end && !((String) components.get(x + 1)).equals(" "))
						result.append(" ");
				} else if (component.equals("=")) {
					if (x < end && ((String) components.get(x + 1)).equals("="))
						x++;
					result.append(nextNonWhiteSpaceIsNULL(x, end, components) ? " IS " : "=");
					if (x < end && !((String) components.get(x + 1)).equals(" "))
						result.append(" ");
				} else if (component.equals("<")) {
					if (x < end && ((String) components.get(x + 1)).equals("=")) {
						x++;
						result.append(nextNonWhiteSpaceIsNULL(x, end, components) ? " IS " : "<=");
					} else if (x < end && ((String) components.get(x + 1)).equals(">")) {
						x++;
						result.append(nextNonWhiteSpaceIsNULL(x, end, components) ? " IS NOT " : "<>");
					} else {
						result.append("<");
					}
					if (x < end && !((String) components.get(x + 1)).equals(" "))
						result.append(" ");
				} else if (component.equals(">")) {
					if (x < end && ((String) components.get(x + 1)).equals("=")) {
						x++;
						result.append(nextNonWhiteSpaceIsNULL(x, end, components) ? " IS NULL OR NOT " : ">=");
					} else {
						result.append(nextNonWhiteSpaceIsNULL(x, end, components) ? " IS NOT " : ">");
					}
					if (x < end && !((String) components.get(x + 1)).equals(" "))
						result.append(" ");

				// Second add all components explicitly tagged as variables (by starting them with a question mark) as such to result
				}  else if (component.startsWith("?")) {
					result.append(" <");
					result.append(component);
					while (x < end - 1 && !isSpaceChar(components.get(x + 1)) && !isRightBracket(components.get(x + 1)))
						result.append(components.get(++x));
					result.append("?>");

				
				// Third add all SQL delimiters, numbers, quotes, SQL Operands and SQL Functions to the result
				} else if (isDelimiter(component)) {
					result.append(component);
				} else if (isNumber(component)) {
					result.append(component);
				} else if (isStartOfQuote(component)) {
					result.append(component);
					while (!isEndOfQuote(component) && x < end) {
						component = (String) components.get(++x);
						result.append(component);
					}

				} else if (isOperand(components, x, start, end)) {
					if (component.trim().equalsIgnoreCase("LIKE") && !getSearchStringEscape(provider).equals("\\")) {
						int from = openingBracket(x, end, components) + 1;
						int until = closingBracket(from, end, components);
						result.append(component.toUpperCase());
						while (++x < from)
							result.append(components.get(x));
						result.append(getReEscapedLikeClause(components, from, until, provider));
						x = until;
					} else {
						if (operands.containsKey(component))
							result.append(operands.get(component));
						else
							result.append(component.toUpperCase());
					}
					
				}  else if (isFunction(components, x, start, end)) {
				   if (functions.containsKey(component))
				      result.append(functions.get(component));
				   else
					  result.append(component.toUpperCase());
					  
				// Finally translate BusinessObjects, variables, and relations		
				} else {

					PersistenceMetaData pmd = null;
					String variable = null;
					int indexVariableComponent;

					// In next case component must be a variable name
					if (!previousInstructionWasFrom(x, start, components) && (x >= end - 1 || nextIsMethod(x, end, components) || isSpaceChar(components.get(x + 1)))) {
						pmd = getPersistenceMetaDataContainingVariable(components, x, imports, stack);
					} else {
						// Otherwise we realy don�t know but we start assuming it is a object name
						pmd = getPersistenceMetaDataByName(component, imports, stack);
						if (pmd != null) {
							//OK its an object if follows by a dot the variable should be first nonSpaceChar componend after next dot
							if (nextNonWhiteSpaceIsDot(x, end, components)) {
								while (x < end - 1 && (isSpaceChar(components.get(++x)) || isDot(components.get(x))));
							} else {
								//Table reference
								result.append(aliasTableName(getTableName(pmd, provider), aliases));
								continue;
							}
						} else {
							//So our first gues was wrong it was not a object name lets check if it could be a variable name
							pmd = getPersistenceMetaDataContainingVariable(components, x, imports, stack);
						}
					}

					//If the component is neither an object name nor a variable name it has to be a parameter name
					if (pmd == null) {
						result.append(" <?" + component);
						while (x < end - 1
							&& !isSpaceChar(components.get(x + 1))
							&& !",".equals(components.get(x + 1))
							&& !compareOperands.contains(components.get(x + 1))
							&& !isRightBracket(components.get(x + 1)))
							result.append(components.get(++x));
						result.append("?> ");
						continue;
					}

					variable = (String) components.get(x);
					indexVariableComponent = x;

					PersistenceMetaData relationPersistenceMetaData = null;
					if (pmd.containsJavaVariable(getDotExtendedName(components, x)) == false)
						relationPersistenceMetaData = pmd.getRelationPersistenceMetaData(variable);

					if (relationPersistenceMetaData == null) {
						// if it is not a relation it must be a variable!!
						result.append(aliasTableName(getTableName(pmd, provider), aliases));
						result.append(".");
						if (variable.equals("*")) {
							result.append("*");
							continue;
						}

						String fieldName = pmd.getFieldNameForJavaField(variable, provider);
						if (fieldName == null) {
							String javaFieldName = getDotExtendedName(components, x);
							if (javaFieldName != null && (fieldName = pmd.getFieldNameForJavaField(javaFieldName, provider)) != null) {
								while (x < end - 1 && (isSpaceChar(components.get(++x)) || isDot(components.get(x))));
							}
						}

						if (fieldName == null) {
							String javaFieldName = getDependentObjectVariableName(components, x);
							if (javaFieldName != null && (fieldName = pmd.getFieldNameForJavaField(javaFieldName, provider)) != null) {
								while (x < end - 1 && (isSpaceChar(components.get(++x)) || isDot(components.get(x))));
							}
						}

						result.append(fieldName);

						if (nextIsMethod(indexVariableComponent, end, components)) {
							x = getIndexNextMethod(indexVariableComponent, end, components);
							component = (String) components.get(x);
							if (component.equals("startsWith")) {
								result.append(" LIKE CONCAT(");
								int from = openingBracket(x, end, components) + 1;
								int until = closingBracket(from, end, components);
								result.append(getEscapedLikeClause(components, from, until, provider));
								//result = translateQuery(components , from , until,  provider, imports, stack, result);//replaced by previous line HC
								result.append(",'%') ");
								x = until;
							} else if (component.equals("endsWith")) {
								result.append(" LIKE CONCAT('%',");
								int from = openingBracket(x, end, components) + 1;
								int until = closingBracket(from, end, components);
								result.append(getEscapedLikeClause(components, from, until, provider));
								//result = translateQuery(components , from , until ,  provider, imports, stack, result);//replaced by previous line HC
								result.append(") ");
								x = until;
							} else {
								throw new Exception("Illigal use of method " + component + " in query near " + getQueryFragmentNearPointer(x, components));
							}
						}

					} else if (relationPersistenceMetaData != null) {
						// OK its a relation

						// get a taste of some of the posible queries involved:
						// (1) 'object.variabel1.variable2.variable3 = 'something'
						// (2) object.data.contains()
						// (3) object.data.contains(var) && ()
						// (4) object.data.contains(data1="i" and data2="2")
						// (5) object.data.contains(var) && (var.data1="i" and var.data2="2")
						// (6) object.data.data1.contains(data1="i" && data2="2")
						// (7) object.data.data1.contains(var) && (var.data3="i" && var.data4="2")
						// (6) object.data.data1.contains(data1="i" && data2.contains(data3 == "2")
						// (7) object.data.data1.contains(var) && (var.data3="i" && var.contains(var2) && (var2.data4=="2")
						// so operate with care if you have to change the code

						if (nextIsMethod(indexVariableComponent, end, components) || nextNonWhiteSpaceIsDot(indexVariableComponent, end, components)) {

							// Is next is method get the method name, update x, check if methodname if valid and add the the
							// prefix NOT if mehthod 'isEmpty'
							if (nextIsMethod(indexVariableComponent, end, components)) {
								x = getIndexNextMethod(indexVariableComponent, end, components);
								component = (String) components.get(x);
								if (!component.equals("isEmpty") && !component.equals("contains")) {
									throw new Exception(
										"JSQL: Error in filter definition near '" + getQueryFragmentNearPointer(x, components) + "': Illigal use of method " + component);
								} else {
									if (component.equals("isEmpty"))
										result.append(" NOT");
								}
							}

							// OK first manage "object.var.var = '1'" like constructs ...
							if (!nextIsMethod(indexVariableComponent, end, components)) {
								x = getIndexNextDot(indexVariableComponent, end, components) + 1;
								int endpoint = getIndexObjectNavigationEndPoint(x, end, components);
								int y = Math.min(Math.min(endpoint + 1,end), components.size());
								result = addRelationalSubquery(pmd, relationPersistenceMetaData, variable, components, x, y, provider, imports, stack, aliases, result);
								x = endpoint;
								continue;
							}

							// ... next manage "object.var.contains(alias) && (alias.var='1')", "object.var.isEmpty(alias) && (alias.var='1')" and
							// rather stupid "object.var.contains(alias) && ()" or  "object.var.isEmpty(alias) && ()" constructs ...
							if (nextBracketPairContains_1_NonSpaceComponent(x, end, components)) {
								String alias = getFirstNonSpaceComponentContainedInNextBracketPair(x, end, components);

								// TODO (low priority) not yet supported: constructs like "object.var.contains(alias) && alias.var='1' " and  "object.var.isEmpty(alias) && alias.var='1' " (notice the 'missing' brackets)
								int endFirstBracketPair = closingBracket(openingBracket(x, end, components), end, components);
								int openingBracket = openingBracket(endFirstBracketPair + 1, end, components);
								int closingBracket = closingBracket(openingBracket, end, components);
								boolean firstAmpersandFound = false;
								boolean secondAmpersandFound = false;
								boolean andFound = false;

								if (!fragmentEqualsIgnoreCase(endFirstBracketPair + 1, openingBracket, components, andSymbols)) {
									throw new Exception(
										"JSQL: Eror in filter definition near '"
											+ getQueryFragmentNearPointer(endFirstBracketPair + 1, components)
											+ "': Expected 'AND', 'and' , '&&' or '&'  but found "
											+ getFragment(endFirstBracketPair + 1, openingBracket, components));
								}

								if (imports == null)
									imports = new HashMap();
								if (imports.containsKey(alias))
									throw new Exception(
										"JSQL: Eror in filter definition near '"
											+ getQueryFragmentNearPointer(endFirstBracketPair, components)
											+ "': Conflicting variable/alias '"
											+ alias
											+ "'");

								imports.put(alias, relationPersistenceMetaData);
								result =
									addRelationalSubquery(
										pmd,
										relationPersistenceMetaData,
										variable,
										components,
										openingBracket + 1,
										closingBracket,
										provider,
										imports,
										stack,
										aliases,
										result);
								imports.remove(alias);
								x = closingBracket;
								continue;

								// ... finaly manage rather intu�tive "object.var.contains(var='1')", "object.var.isEmpty(var='1')" and 
								// "object.var.contains()" or "object.var.isEmpty()" constructs
							} else {

								int openingBracket = openingBracket(x, end, components);
								int closingBracket = closingBracket(openingBracket, end, components);
								result =
									addRelationalSubquery(
										pmd,
										relationPersistenceMetaData,
										variable,
										components,
										openingBracket + 1,
										closingBracket,
										provider,
										imports,
										stack,
										aliases,
										result);
								x = closingBracket;
								continue;

							}
						} else if ("=".equals(getNextNonWhiteSpaceComponent(indexVariableComponent, end, components))) {
							throw new Exception("JSQL: Object references are not yet supported: " + getQueryFragmentNearPointer(x, components));
						} else {
							throw new Exception("JSQL: Error in filter definition near " + getQueryFragmentNearPointer(x, components));
						}
					}
				}
			} catch (Exception e) {
				if (e.getMessage() != null && e.getMessage().startsWith("JSQL"))
					throw e;
				String message = "JSQL: Error in filter definition near " + getQueryFragmentNearPointer(x, components);
				System.out.println(message);
				e.printStackTrace();
				throw new Exception(message);
			}
		}
		return result;
	}
	/**
	 * @param x
	 * @param end
	 * @param components
	 * @return
	 */
	private static boolean isAS(int x,Vector components) {
		if (components.get(x).equals("as") || components.get(x).equals("AS"))
			if (x > 0 && isSpaceChar(components.get(x-1)) && x < components.size() &&  isSpaceChar(components.get(x+1)))
				return true;
		return false;
	}

	private static StringBuffer addRelationalSubquery(
		PersistenceMetaData fromPMD,
		PersistenceMetaData toPMD,
		String variable,
		Vector components,
		int start,
		int end,
		ConnectionProvider provider,
		Map imports,
		Stack stack,
		Map aliases,
		StringBuffer result)
		throws Exception {

		String multiplicity = fromPMD.getRelationMultiplicity(variable);

		if (PersistenceMetaData.MANY_TO_MANY.equals(multiplicity))
			return addManyToManySubquery(fromPMD, toPMD, variable, components, start, end, provider, imports, stack, aliases, result);
		else
			return addStandardRelationalSubquery(fromPMD, toPMD, variable, components, start, end, provider, imports, stack, aliases, result);

	}

	private static StringBuffer addStandardRelationalSubquery(
		PersistenceMetaData fromPMD,
		PersistenceMetaData toPMD,
		String variable,
		Vector components,
		int start,
		int end,
		ConnectionProvider provider,
		Map imports,
		Stack stack,
		Map aliases,
		StringBuffer result)
		throws Exception {

		String[][] collumnPairs = fromPMD.getRelationColomnPairs(variable, provider);
		String tableName = getTableName(toPMD, provider);
		String previousAlias = (String) aliases.get(tableName);
		String newAlias = getNewAlias(toPMD, stack, tableName, previousAlias);

		result.append(" EXISTS (SELECT * FROM " + tableName);
		if (newAlias != null)
			result.append(" AS " + newAlias);
		result.append(" WHERE ");
		for (int i = 0; i < collumnPairs.length; i++) {
			if (i > 0)
				result.append(" AND ");
			result.append(aliasTableNamePartInColumn(collumnPairs[i][1], aliases));
			result.append(" = ");
			result.append(aliasTableNamePartInColumn(collumnPairs[i][0], newAlias));
		}
		if (!containsOnlySpaceChars(components, start, end)) {
			result.append(" AND ( ");
			stack.push(toPMD);
			if (newAlias != null)
				aliases.put(tableName, newAlias);
			result = translateQuery(components, start, end, provider, imports, stack, aliases, result);
			stack.pop();
			if (newAlias != null)
				aliases.remove(tableName);
			if (previousAlias != null)
				aliases.put(tableName, previousAlias);
			result.append(" ) ");
		}
		result.append(" ) ");
		return result;
	}

	private static String aliasTableName(String name, Map aliases) {
		if (aliases == null || aliases.isEmpty())
			return name;
		if (aliases.containsKey(name))
			return (String) aliases.get(name);
		return name;
	}

	private static Object aliasTableName(String name, String alias) {
		if (alias == null || alias.trim().equals(""))
			return name;
		return alias;
	}
	
	private static String aliasTableNamePartInColumn(String name, Map aliases) {
			if (aliases == null || aliases.isEmpty())
				return name;
			int indx = name.lastIndexOf('.');
			if (indx < 0 && aliases.containsKey(name))
				return (String) aliases.get(name);
			else if (indx > 0 && aliases.containsKey(name.substring(0, indx)))
				return ((String) aliases.get(name.substring(0, indx))) + name.substring(indx);
			return name;
		}

		private static Object aliasTableNamePartInColumn(String name, String alias) {
			if (alias == null || alias.trim().equals(""))
				return name;
			int indx = name.lastIndexOf('.');
			if (indx < 0)
				return alias;
			else
				return alias + name.substring(indx);

		}

	private static String getNewAlias(PersistenceMetaData toPMD, Stack stack, String tableName, String previousAlias) {
		if (stack.contains(toPMD)) {
			if (previousAlias == null)
				return tableName.replace('.','_') + "_TA" + 1;
			int x = Integer.parseInt(previousAlias.substring(previousAlias.length() - 1)) + 1;
			return tableName.replace('.','_') + "_TA" + x;
		}
		return null;
	}

	private static boolean containsOnlySpaceChars(Vector components, int start, int end) {
		boolean empty = true;
		for (int x = start; x < end && x < components.size() && empty; x++) {
			if (!isSpaceChar(components.get(x)))
				empty = false;
		}
		return empty;
	}

	private static StringBuffer addManyToManySubquery(
		PersistenceMetaData fromPMD,
		PersistenceMetaData toPMD,
		String variable,
		Vector components,
		int start,
		int end,
		ConnectionProvider provider,
		Map imports,
		Stack stack,
		Map aliases,
		StringBuffer result)
		throws Exception {

		String[][] collumnPairsToAssociatedTable = fromPMD.getManyToManyColomnPairs(variable, provider, PersistenceMetaData.TO_ASSOCIATIVE_TABLE);
		String[][] collumnPairsFromAssociatedTable = fromPMD.getManyToManyColomnPairs(variable, provider, PersistenceMetaData.FROM_ASSOCIATIVE_TABLE);
		
		result.append(" EXISTS (SELECT * FROM ");
		result.append(provider.getPrefix()+fromPMD.getAssociationTableName(variable, provider));
		result.append(" WHERE ");
		for (int i = 0; i < collumnPairsToAssociatedTable.length; i++) {
			if (i > 0)
				result.append(" AND ");
			result.append(aliasTableNamePartInColumn(collumnPairsToAssociatedTable[i][1],aliases));
			result.append(" = ");
			result.append(aliasTableNamePartInColumn(collumnPairsToAssociatedTable[i][0],aliases));
		}
		
		String tableName = getTableName(toPMD, provider);
		String previousAlias = (String) aliases.get(tableName);
		String newAlias = getNewAlias(toPMD, stack, tableName, previousAlias);

		result.append(" AND EXISTS (SELECT * FROM " + tableName);
		if (newAlias != null)
			result.append(" AS " + newAlias);

		result.append(" WHERE ");
		for (int i = 0; i < collumnPairsFromAssociatedTable.length; i++) {
			if (i > 0)
				result.append(" AND ");
			result.append(aliasTableNamePartInColumn(collumnPairsFromAssociatedTable[i][1],newAlias));
			result.append(" = ");
			result.append(aliasTableNamePartInColumn(collumnPairsFromAssociatedTable[i][0],aliases));
		}
		if (!containsOnlySpaceChars(components, start, end)) {
			result.append(" AND ( ");
			stack.push(toPMD);
			if (newAlias != null)
				aliases.put(tableName, newAlias);
			result = translateQuery(components, start, end, provider, imports, stack, aliases, result);
			stack.pop();
			if (newAlias != null)
				aliases.remove(tableName);
			if (previousAlias != null)
				aliases.put(tableName, previousAlias);
			result.append(" ) ");
		}
		result.append(" ) ) ");
		return result;
	}

	private static PersistenceMetaData getPersistenceMetaDataByName(String name, Map imports, Stack stack) {
		if (imports != null && imports.containsKey(name)) {
			return (PersistenceMetaData) imports.get(name);
		}
		for (int i = stack.size() - 1; i >= 0; i--) {
			PersistenceMetaData pmd = (PersistenceMetaData) stack.get(i);
			if (pmd.getBusinessObjectName().equals(name))
				return pmd;
		}
		return null;
	}

	private static Object getEscapedLikeClause(Vector components, int from, int until, ConnectionProvider provider) {
		String quote = (String) components.get(from);
		int f = from + 1;
		while (f < until)
			quote += (String) components.get(f++);
		if (quote.indexOf('_') < 0 && quote.indexOf('%') < 0)
			return quote;
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < quote.length(); i++) {
			if (quote.charAt(i) == '_' || quote.charAt(i) == '%')
				buffer.append(getSearchStringEscape(provider));
			buffer.append(quote.charAt(i));
		}
		return buffer.toString();
	}

	private static Object getReEscapedLikeClause(Vector components, int from, int until, ConnectionProvider provider) {
		String quote = (String) components.get(from);
		int f = from + 1;
		while (f < until)
			quote += (String) components.get(f++);
		if (quote.indexOf("\\_") < 0 && quote.indexOf("\\%") < 0)
			return quote;
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < quote.length() - 1; i++) {
			if (quote.charAt(i) == '\\') {
				if (i < quote.length() - 3 && quote.charAt(i + 1) == '\\' && (quote.charAt(i + 2) == '_' || quote.charAt(i + 2) == '%'))
					buffer.append(quote.charAt(i++));
				else if (quote.charAt(i + 1) == '_' || quote.charAt(i + 1) == '%')
					buffer.append(getSearchStringEscape(provider));
				else
					buffer.append(quote.charAt(i));
			} else
				buffer.append(quote.charAt(i));
		}
		return buffer.toString();
	}

	private static String getSearchStringEscape(ConnectionProvider provider) {
		return provider.getSearchStringEscape();
	}

	private static PersistenceMetaData getPersistenceMetaDataContainingVariable(Vector components, int pointer, Map imports, Stack stack) {
		String variableName = (String) components.get(pointer);
		String extendedVariableName = null;
		String candidateDOName = null;
		boolean initialisedCandidateDO = false;
		boolean initialisedExtendedVariableName = false;
		for (int i = stack.size() - 1; i >= 0; i--) {
			PersistenceMetaData pmd = (PersistenceMetaData) stack.get(i);
			if (pmd.containsJavaVariable(variableName))
				return pmd;
			if (!initialisedCandidateDO) {
				candidateDOName = getDependentObjectVariableName(components, pointer);
				initialisedCandidateDO = true;
			}
			if (candidateDOName != null && pmd.containsJavaVariable(candidateDOName))
				return pmd;
			if (!initialisedExtendedVariableName) {
				extendedVariableName = getDotExtendedName(components, pointer);
				initialisedExtendedVariableName = true;
			}
			if (extendedVariableName != null && pmd.containsJavaVariable(extendedVariableName))
				return pmd;
		}
		if (imports != null){
			for(Iterator it = imports.values().iterator();it.hasNext();) {
				PersistenceMetaData pmd = (PersistenceMetaData) it.next();
				if (pmd.containsJavaVariable(variableName))
					return pmd;
				if (!initialisedCandidateDO) {
					candidateDOName = getDependentObjectVariableName(components, pointer);
					initialisedCandidateDO = true;
				}
				if (candidateDOName != null && pmd.containsJavaVariable(candidateDOName))
					return pmd;
				if (!initialisedExtendedVariableName) {
					extendedVariableName = getDotExtendedName(components, pointer);
					initialisedExtendedVariableName = true;
				}
				if (extendedVariableName != null && pmd.containsJavaVariable(extendedVariableName))
					return pmd;			
			}
		}			
		return null;
	}

	private static String getDependentObjectVariableName(Vector components, int pointer) {
		if (nextNonWhiteSpaceIsDot(pointer, components.size(), components) && !nextIsMethod(pointer + 1, components.size(), components)) {
			String p1 = (String) components.get(pointer);
			int x = getIndexNextNonSpaceChar(getIndexNextDot(pointer + 1, components.size(), components) + 1, components.size(), components);
			if (x < 0)
				return null;
			String p2 = (String) components.get(x);
			return Character.toLowerCase(p1.charAt(0)) + p1.substring(1) + Character.toUpperCase(p2.charAt(0)) + p2.substring(1);

		}
		return null;
	}

	private static String getDotExtendedName(Vector components, int pointer) { //TODO Start nieuw nieuw nieuw
		if (nextNonWhiteSpaceIsDot(pointer, components.size(), components)
			&& !nextIsMethod(pointer + 1, components.size(), components)
			&& !nextNonWhiteSpaceIsDot(pointer + 2, components.size(), components)) {
			String p1 = (String) components.get(pointer);
			int x = getIndexNextNonSpaceChar(getIndexNextDot(pointer + 1, components.size(), components) + 1, components.size(), components);
			if (x < 0)
				return null;
			else
				return p1 + "." + (String) components.get(x);
		}
		return null;
	} //TODO Einde nieuw nieuw nieuw

	private static String getTableName(PersistenceMetaData pmd, ConnectionProvider provider) {
		return provider.getPrefix() + pmd.getTableName(provider);
	}

	private static boolean fragmentEqualsIgnoreCase(int _start, int _end, Vector components, String[] values) {
		String result = getFragment(_start, _end, components).trim();
		for (int i = 0; i < values.length; i++) {
			if (values[i] == null)
				continue;
			if (result.equals(values[i].trim()))
				return true;
		}
		return false;
	}

	private static String getFragment(int _start, int _end, Vector components) {
		StringBuffer buffer = new StringBuffer();
		for (int i = _start; i < _end; i++) {
			buffer.append(components.get(i));
		}
		return buffer.toString();
	}

	private static boolean previousInstructionWasFrom(int pointer, int underborder, Vector components) {
		for (int i = pointer; i >= underborder; i--) {
			if (operands.containsKey(components.get(i))) {
				if (((String) components.get(i)).equals("FROM") || ((String) components.get(i)).equals("from"))
					return true;
				else
					return false;
			}
		}
		return true;
	}

	private static boolean nextBracketPairIsEmpty(int _start, int _end, Vector components) {
		int openingBracket = openingBracket(_start, _end, components);
		int closingBracket = closingBracket(openingBracket, _end, components);
		for (int i = openingBracket + 1; i < closingBracket; i++) {
			if (!isSpaceChar(components.get(i)))
				return false;
		}
		return true;
	}

	private static String getFirstNonSpaceComponentContainedInNextBracketPair(int _start, int _end, Vector components) {
		int openingBracket = openingBracket(_start, _end, components);
		int closingBracket = closingBracket(openingBracket, _end, components);
		for (int i = openingBracket + 1; i < closingBracket; i++) {
			if (!isSpaceChar(components.get(i)))
				return (String) components.get(i);
		}
		return null;
	}

	private static boolean nextBracketPairContains_1_NonSpaceComponent(int _start, int _end, Vector components) {
		int openingBracket = openingBracket(_start, _end, components);
		int closingBracket = closingBracket(openingBracket, _end, components);
		boolean firstNonSpaceFound = false;
		for (int i = openingBracket + 1; i < closingBracket; i++) {
			if (!isSpaceChar(components.get(i))) {
				if (firstNonSpaceFound)
					return false;
				else
					firstNonSpaceFound = true;
			}
		}
		return firstNonSpaceFound;
	}

	private static int getIndexObjectNavigationEndPoint(int start, int end, Vector components) {
		int from = Math.max(0, start);
		int until = Math.min(components.size(), end);
		for (int i = from; i < until; i++) {
			if (nextIsMethod(i, until, components)) {
				if (nextBracketPairContains_1_NonSpaceComponent(i, until, components)) {
					i = closingBracket(openingBracket(i, until, components), until, components);
					return closingBracket(openingBracket(i, until, components), until, components);
				} else {
					return closingBracket(openingBracket(i, until, components), until, components);
				}
			} else if (compareOperands.contains(components.get(i))) {
				for (int y = i + 1; y < until; y++) {
					if (isStartOfQuote(components.get(y)))
						y = getIndexEndOfQuote(y, until, components);
					if (andOperands.contains(components.get(y)) || orOperands.contains(components.get(y)) || isRightBracket(components.get(y)))
						return y - 1;
				}
				return until;
			} else if (((String) components.get(i)).equalsIgnoreCase("BETWEEN")) {
				boolean andEncountered = false;
				for (int y = i + 1; y < until; y++) {
					if (isStartOfQuote(components.get(y))) {
						y = getIndexEndOfQuote(y, until, components);
						if (andEncountered)
							return y;
					}
					if (andOperands.contains(components.get(y))) {
						andEncountered = true;
						if (andOperands.contains(components.get(y + 1)))
							y++;
						continue;
					}
					if (!isSpaceChar(components.get(y))) {
						if (andEncountered)
							return y;
					}
				}
				return until;
			} else if (((String) components.get(i)).equalsIgnoreCase("IN")) {
				return closingBracket(openingBracket(i, until, components), until, components);
			}
		}
		return until;
	}

	private static int openingBracket(int _start, int _end, Vector components) {
		int pointer = _start;
		int end = Math.min(_end, components.size());
		while (pointer < end && !((String) components.get(pointer)).equals("("))
			pointer++;
		return pointer;
	}

	private static int closingBracket(int _start, int _end, Vector components) {
		int counter = 1;
		int pointer = _start;
		int end = Math.min(_end, components.size());
		while (counter > 0 && pointer < end) {
			String component = (String) components.get(++pointer);
			if (isStartOfQuote(component)) {
				while (pointer < end && !isEndOfQuote((String) components.get(pointer++)));
				component = (String) components.get(pointer);
			}
			if (component.equals("("))
				counter++;
			else if (component.equals(")"))
				counter--;
		}
		return pointer;
	}

	private static String getQueryFragmentNearPointer(int pointer, Vector components) {
		int start = Math.max(0, pointer - 8);
		int end = Math.min(components.size(), pointer + 8);
		StringBuffer buffer = new StringBuffer();
		for (int i = start; i < end; i++) {
			buffer.append(components.get(i));
		}
		return buffer.toString();
	}

	private static boolean nextNonWhiteSpaceIsDot(int start, int end, Vector components) {
		int from = Math.max(0, start + 1);
		int until = Math.min(components.size(), end);
		for (int i = from; i < until; i++) {
			if (isSpaceChar(components.get(i)))
				continue;
			if (isDot(components.get(i)))
				return true;
			return false;
		}
		return false;
	}
	
	private static boolean nextNonWhiteSpaceIsLeftBracket(int start, int end, Vector components) {
		int from = Math.max(0, start + 1);
		int until = Math.min(components.size(), end);
		for (int i = from; i < until; i++) {
			if (isSpaceChar(components.get(i)))
				continue;
			if (isLeftBracket(components.get(i)))
				return true;
			return false;
		}
		return false;
	}
	
	private static String getNextNonWhiteSpaceComponent(int start, int end, Vector components) {
		int from = Math.max(0, start + 1);
		int until = Math.min(components.size(), end);
		for (int i = from; i < until; i++) {
			if (isSpaceChar(components.get(i)))
				continue;
			return (String) components.get(i);
		}
		return null;
	}

	private static boolean previousNonWhiteSpaceIsDot(int start, int end, Vector components) {
		int from = Math.min(components.size(), start - 1);
		int until = Math.max(0, end);
		for (int i = from; i >= until; i--) {
			if (isSpaceChar(components.get(i)))
				continue;
			if (isDot(components.get(i)))
				return true;
			return false;
		}
		return false;
	}

	private static boolean nextNonWhiteSpaceIsNULL(int start, int end, Vector components) {
		int from = Math.max(0, start + 1);
		int until = Math.min(components.size(), end);
		for (int i = from; i < until; i++) {
			if (isSpaceChar(components.get(i)))
				continue;
			if (isNULL(components.get(i)))
				return true;
			return false;
		}
		return false;
	}

	private static boolean isNULL(Object object) {
		return (String.valueOf(object).equalsIgnoreCase("null"));
	}

	private static int getIndexNextDot(int start, int end, Vector components) {
		int from = Math.max(0, start);
		int until = Math.min(components.size(), end);
		for (int i = from; i < until; i++) {
			if (isDot(components.get(i)))
				return i;
		}
		return -1;
	}

	private static int getIndexNextNonSpaceChar(int start, int end, Vector components) {
		int from = Math.max(0, start);
		int until = Math.min(components.size(), end);
		for (int i = from; i < until; i++) {
			if (!isSpaceChar(components.get(i)))
				return i;
		}
		return -1;
	}

	private static boolean nextIsMethod(int start, int end, Vector components) {
		return (getIndexNextMethod(start, end, components) >= 0);
	}

	private static int getIndexNextMethod(int start, int end, Vector components) {
		int from = Math.max(0, start + 1);
		int until = Math.min(components.size(), end);
		for (int i = from; i < until; i++) {
			if (isMethodName(components.get(i)))
				return i;
			if (!isSpaceChar(components.get(i)) && !isDot(components.get(i)))
				return -1;
		}
		return -1;
	}

	private static int getIndexEndOfQuote(int start, int end, Vector components) {
		int from = Math.max(0, start);
		int until = Math.min(components.size(), end) - 1;
		int x = ((String)components.get(from)).length()==1?from+1:from;
		while (x < until && !isEndOfQuote(components.get(x)))
			++x;
		return x;
	}

	private static boolean isSpaceChar(Object object) {
		return isSpaceChar((String) object);
	}

	private static boolean isSpaceChar(String string) {
		if (string == null || string.length() != 1)
			return false;
		return (Character.isSpaceChar(string.charAt(0)));
	}

	private static boolean isDot(Object object) {
		return isDot((String) object);
	}

	private static boolean isDot(String string) {
		return (string != null && string.equals("."));
	}

	private static boolean isLeftBracket(Object object) {
		return isLeftBracket((String) object);
	}

	private static boolean isLeftBracket(String string) {
		return (string != null && string.equals("("));
	}

	private static boolean isRightBracket(Object object) {
		return isRightBracket((String) object);
	}

	private static boolean isRightBracket(String string) {
		return (string != null && string.equals(")"));
	}

	private static boolean isMethodName(Object object) {
		return methodNames.contains(object);
	}

	private static boolean isDelimiter(Object object) {
		return isDelimiter((String) object);
	}

	private static boolean isDelimiter(String string) {
		return (delimeters.indexOf(string) > -1);
	}

	private static boolean isOperand(Object object) {
		return (operands.containsKey(object) || operands.containsValue(object));
	}

	private static boolean isOperand(Vector components, int pointer, int lowerLimit, int upperLimit) {
		return (
			isOperand(components.get(pointer)) && !nextNonWhiteSpaceIsDot(pointer, upperLimit + 2, components) && !previousNonWhiteSpaceIsDot(pointer, lowerLimit - 2, components));
	}
	
	private static boolean isFunction(Object object) {
		return (functions.containsKey(object) || functions.containsValue(object));
	}

	private static boolean isFunction(Vector components, int pointer, int lowerLimit, int upperLimit) {
		return (
			isFunction(components.get(pointer)) && nextNonWhiteSpaceIsLeftBracket(pointer, upperLimit + 2, components) && !previousNonWhiteSpaceIsDot(pointer, lowerLimit - 2, components));
	}	

	private static boolean isStartOfQuote(Object object) {
		return isStartOfQuote((String) object);
	}

	private static boolean isStartOfQuote(String string) {
		return (string.startsWith("'"));
	}

	private static boolean isEndOfQuote(Object object) {
		return isEndOfQuote((String) object);
	}

	private static boolean isEndOfQuote(String string) {
		return (string.endsWith("'"));
	}

	private static boolean isNumber(Object object) {
		return isNumber((String) object);
	}

	private static boolean isNumber(String string) {
		int dotCount = 0;
		for (int x = 0; x < string.length(); x++) {
			if (Character.isDigit(string.charAt(x))) {
				continue;
			}
			if (string.charAt(x) == '.' && dotCount == 0) {
				dotCount++;
				continue;
			}
			if (x == 0 && (string.charAt(x) == '+' || string.charAt(x) == '-')) {
				continue;
			}
			return false;
		}
		return true;
	}
	/*
	private static Vector parseQuery(String query){
	   StringTokenizer tokenizer = new StringTokenizer(query,delimeters, true);
	   Vector vector = new Vector();
	   while(tokenizer.hasMoreTokens())
	      vector.add(tokenizer.nextToken());
	   return vector;
	}
	*/

	private static Vector parseQuery(String query) {
		StringTokenizer tokenizer = new StringTokenizer(query, delimeters, true);
		Vector vector = new Vector();
		while (tokenizer.hasMoreTokens()) {
			String sPart = tokenizer.nextToken();
			// quoted values must be treated as 1 token !!!
			// Fixed By JvdK en GB  dd 6-11-2003 
			if (!sPart.startsWith("'") && !sPart.startsWith("\"")) {
				vector.add(sPart);
			} else if ((sPart.startsWith("'") && !isEndOfQuote(sPart, "'", true)) || (sPart.startsWith("\"") && !isEndOfQuote(sPart, "\"", true))) {
				StringBuffer sQuoted = new StringBuffer(sPart);
				String t = sPart.substring(0, 1);
				while (tokenizer.hasMoreTokens()) {
					sPart = tokenizer.nextToken();
					sQuoted.append(sPart);
					if (isEndOfQuote(sPart, t, false)) {
						String quote;
						if (t.equals("\'"))
							quote = sQuoted.toString();
						else {
							quote = "'" + escapeSingleQuotes(sQuoted.substring(1, sQuoted.length() - 1)) + "'";
						}
						quote = replaceEscapeChars(quote, t);
						vector.add(quote);
						break;
					}
				}
			} else {
				if (sPart.startsWith("\"") && sPart.endsWith("\"") && sPart.length() > 1)
					vector.add("'" + escapeSingleQuotes(replaceEscapeChars(sPart.substring(1, sPart.length() - 1), "\"")) + "'");
				else
					vector.add(sPart);
			}
		}
		return vector;
	}

	private static String escapeSingleQuotes(String string) {
		if (string.indexOf('\'') < 0)
			return string;
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == '\'')
				buffer.append('\'');
			buffer.append(string.charAt(i));
		}
		return buffer.toString();
	}

	private static String replaceEscapeChars(String quote, String quoteIdentifier) {
		char escape = getEscapeChar(quoteIdentifier);
		if (escape == '\'')
			return quote; // if SQL type quote we don't have to do anything anymore
		if (quote.length() == 0 || quote.indexOf(escape) < 0)
			return quote; // if no escapes contained there is nothing to replace
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < quote.length(); i++) {
			if (quote.charAt(i) == escape) {
				if ((i + 1) == quote.length())
					break;
				if (quote.charAt(i + 1) == escape)
					buffer.append(quote.charAt(++i));
				else
					buffer.append(quote.charAt(i)); // Actually we will never come here 
			} else {
				buffer.append(quote.charAt(i));
			}
		}
		return buffer.toString();
	}

	private static boolean isEndOfQuote(String quote, String quoteIdentifier, boolean quoteIncludesStartQuote) {
		if (quoteIncludesStartQuote && quote.length() == 1)
			return false;
		if (!quote.endsWith(quoteIdentifier))
			return false;
		char escape = getEscapeChar(quoteIdentifier);
		int count = 0;
		int start = quoteIncludesStartQuote ? 1 : 0;
		for (int i = quote.length() - 2; i >= start; i--, count++)
			if (quote.charAt(i) != escape)
				break;
		return (count % 2 == 0);
	}

	private static char getEscapeChar(String quoteIdentifier) {
		return quoteIdentifier.charAt(0);
	}

	private static String replaceString(String text, String oldValue, String newValue) {
		return replaceString(text, oldValue, newValue, 0);
	}

	private static String replaceString(String text, String oldValue, String newValue, int from) {
		int n = text.indexOf(oldValue, from);
		if (n > -1) {
			return replaceString(text.substring(0, n) + newValue + text.substring(n + oldValue.length()), oldValue, newValue, n + newValue.length());
		} else {
			return text;
		}
	}
		
	private static void main(String[] args) {
		/* Vector components = parseQuery("('begin')");
		 int _start = openingBracket(0, components.size(), components);
		 int _end   = closingBracket(_start , components.size() ,components);
		 for (int i = _start ; i <= _end; i++){
		    System.out.println(components.get(i));
		 }*/

		Vector components = parseQuery("bla bla");
		for (int i = 0; i < components.size(); i++) {
			System.out.println(i + " - >>" + components.get(i) + "<< - " + ((String) components.get(i)).equals(","));
		}
		int y = 0;
		while (++y < components.size() && !((String) components.get(y)).equals(","));
		System.out.println("index " + y);

	}

}



