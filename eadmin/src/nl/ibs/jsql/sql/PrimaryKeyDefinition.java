package nl.ibs.jsql.sql;



import java.util.*;
import java.sql.*;
import nl.ibs.jeelog.*;


/**
 *
 * @author  c_hc01
 */
public class PrimaryKeyDefinition {

	private TreeMap keyColumns = new TreeMap();

	

	private String tableCatalog; 		// table catalog (may be null)
	private String tableSchema; 		// table schema (may be null)
	private String tableName; 			// table name
	//public String columnName;        	// column name 
	//private String keySequence;      	// sequence number within primary key
	private String name; //primary key name (may be <code>null</code>)

	public static PrimaryKeyDefinition getPrimaryKeyDefinition(String schemaPattern, String tablePattern, DatabaseMetaData meta) {
		PrimaryKeyDefinition definition = null;
		String _schemaPattern = (schemaPattern == null || schemaPattern.equals("")) ? null : schemaPattern;
		ResultSet rs = null;
		try {
			rs = meta.getPrimaryKeys(null, _schemaPattern, tablePattern);
			while (rs.next()) {
				if (definition == null) {
					definition = new PrimaryKeyDefinition(rs, meta);
				} else {
					definition.update(rs, meta);
				}
			}
		} catch (Exception e) {
			Log.error(e.getMessage());
			e.printStackTrace();
		} finally {
        	if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {	
				}
        }
		return definition;
	}

	private PrimaryKeyDefinition(ResultSet rs, DatabaseMetaData meta) {
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			int z = rsmd.getColumnCount();

			if (z < 1)
				return;
			tableCatalog = rs.getString(1); // Table catalog (may be null)
			if (z < 2)
				return;
			tableSchema = rs.getString(2); // Table schema (may be null)
			if (z < 3)
				return;
			tableName = rs.getString(3); // Table name
			if (z < 4)
				return;
			//columnName = rs.getBoolean(4); // columnName
			if (z < 5)
				return;
			//columnSequence = rs.getShort(5); // Index catalog (may be null); null when TYPE is tableIndexStatistic 
			
			update(rs, meta);// add columns
			
			if (z < 6)
				return;
			name = rs.getString(6); // Index name; null when TYPE is tableIndexStatistic 
			
			

		} catch (Exception e) {
			Log.error(e.getMessage());
			e.printStackTrace();
		}

	}

	public void update(ResultSet rs, DatabaseMetaData meta) {
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			if (rsmd.getColumnCount() < 5)
				return;
			PrimaryKeyColumn pkc = new PrimaryKeyColumn();
			pkc.columnName = rs.getString(4);
			short seq = rs.getShort(5);
			keyColumns.put(new Integer(seq - 1), pkc);

		} catch (Exception e) {
			Log.error(e.getMessage());
			e.printStackTrace();
		}
	}


	/** key columns in defined sequence.
	 * 
	 * @return index columns
	 */
	public PrimaryKeyColumn[] getPrimaryKeyColumns() {
		PrimaryKeyColumn[] pks = new PrimaryKeyColumn[keyColumns.size()];
		Iterator it = keyColumns.values().iterator();
		for (int i = 0; i < pks.length && it.hasNext(); i++) 
			pks[i] = (PrimaryKeyColumn)it.next();
		return pks;
	}


	/** Table catalog (may be null).
	 * @return table catalog or null
	 */
	public String getTableCatalog() {
		return tableCatalog;
	}

	/** Table name.
	 * @return table name.
	 */
	public String getTableName() {
		return tableName;
	}

	/** Table schema (may be null).
	 * @return table schema or null.
	 */
	public String getTableSchema() {
		return tableSchema;
	}
	
	/** Primary key name.
	 * @return primary name or null
	 */
	public String getName() {
		return name;
	}

}

