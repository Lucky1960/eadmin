package nl.ibs.jsql.sql;



import nl.ibs.jeelog.*;
import java.util.*;
import java.sql.*;


/**
 *
 * @author  c_hc01
 */
public class TableDefinition {

	

	public String tableCatalog; //(may be null)
	public String tableSchema; // (may be null)
	public String tableName;
	public String tableType;
	// Typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY", "ALIAS", "SYNONYM".
	public String explanatoryComment;
	/*
	public String typesCatalog; //(may be null)
	public String typesSchema;  //(may be null)
	public String typeName;     // (may be null)
	public String selfReferencingColumnName; //name of the designated "identifier" column of a typed table (may be null)
	public String referencingGeneration; //;specifies how values in SELF_REFERENCING_COL_NAME are created. Values are "SYSTEM", "USER", "DERIVED". (may be null)
	*/
	private HashMap columnDefinitions;
	private HashMap indexDefinitions;
	private HashMap foreignKeyDefinitions;
	private PrimaryKeyDefinition primaryKeyDefinition;
	private DatabaseMetaData meta;
	
	public static HashMap getTableDefinitions(DBConnectionPool pool) throws Exception {
			return getTableDefinitions(new String[] { "TABLE" }, pool);
	}

	public static HashMap getTableDefinitions(String[] types, DBConnectionPool pool)throws Exception {
		return getTableDefinitions("_%", types, pool);
	}

	public HashMap getColumnDefinitions() {
		return columnDefinitions;
	}

	public static HashMap getTableDefinitions(String table, String[] types, DBConnectionPool pool)throws Exception {
		HashMap map = new HashMap();
		DatabaseMetaData metaData = pool.getDatabaseMetaData();
		String schema =  pool.getSchema();
		String _schema = (schema == null || schema.equals("")) ? null : schema;
		ResultSet rs = null;
		try {
			rs = metaData.getTables(null, _schema, table, types);
			while (rs.next()) {
				TableDefinition definition = new TableDefinition(rs, metaData);
				String name = definition.tableName.toUpperCase();
				if (!map.containsKey(name))
					map.put(name, definition);
			}
		} catch (Exception e) {
			Log.error(e.getMessage());
			e.printStackTrace();
		} finally {
        	if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {	
				}
        }
		return map;
	}

	private TableDefinition(ResultSet rs, DatabaseMetaData meta) {
		this.meta = meta;	
		try {
			tableCatalog = rs.getString(1); // (may be null)
			tableSchema = rs.getString(2); // (may be null)
			tableName = rs.getString(3);
			tableType = rs.getString(4);
			// Typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY", "ALIAS", "SYNONYM".
			explanatoryComment = rs.getString(5);
			/*
			typesCatalog = rs.getString(6);  //(may be null)
			typesSchema = rs.getString(7);  //(may be null)
			typeName = rs.getString(8); // (may be null)
			selfReferencingColumnName = rs.getString(9);  //name of the designated "identifier" column of a typed table (may be null)
			referencingGeneration = rs.getString(10);  //specifies how values in SELF_REFERENCING_COL_NAME are created. Values are "SYSTEM", "USER", "DERIVED". (may be null)
			*/
		} catch (Exception e) {
			Log.error(e.getMessage());
			e.printStackTrace();
		}
		columnDefinitions = ColumnDefinition.getColumnDefinitions(tableSchema, tableName, meta);
	}

	/**
	 * @return
	 */
	public String getExplanatoryComment() {
		return explanatoryComment;
	}

	/**
	 * @return primaryKeyDefinition or null;
	 */
	public PrimaryKeyDefinition getPrimaryKeyDefinition() {
		if (primaryKeyDefinition == null)
			primaryKeyDefinition = PrimaryKeyDefinition.getPrimaryKeyDefinition(tableSchema, tableName, meta);	
		return primaryKeyDefinition;
	}


	/**
	 * @return foreignKeyDefinitions
	 */
	public HashMap getForeignKeyDefinitions() {
		if (foreignKeyDefinitions == null)
			foreignKeyDefinitions = ForeignKeyDefinition.getForeignKeyDefinitions(tableSchema, tableName, meta);	
		return foreignKeyDefinitions;
	}

	/**
	 * @return indexDefinitions
	 */
	public HashMap getIndexDefinitions() {
		if (indexDefinitions == null)
			indexDefinitions = IndexDefinition.getIndexDefinitions(tableSchema, tableName, meta);
		return indexDefinitions;
	}

	

	/**
	 * @return table catalog or null
	 */
	public String getTableCatalog() {
		return tableCatalog;
	}

	/**
	 * @return
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @return table schema or null
	 */
	public String getTableSchema() {
		return tableSchema;
	}

	/**
	 * @return
	 */
	public String getTableType() {
		return tableType;
	}

}

