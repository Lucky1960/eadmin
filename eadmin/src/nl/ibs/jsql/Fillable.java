package nl.ibs.jsql;



/**
 * @author c_md07
 */
public interface Fillable {
	/**
	 * This method returns the fieldnames of the fields to be filled.
	 * @return the fieldnames
	 */
	public String[] fieldnamesToBeFilled();
	
	/**
	 * Use this method to do something with the field(s) that has to filled.
	 * For example: in the database you have two fields -> TAX PRICEEXTAX
	 * In the fillable class you have an attribute priceIncTax, priceExTax and Tax. In the 
	 * fill method you can fill priceInTax with the value computed from the Tax and 
	 * priceExTax attributes.
	 */
	public void fill();
}



