package nl.ibs.jsql;

import nl.ibs.jeelog.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.impl.*;


import java.util.*;
import java.sql.*;

public class AtomicDBTransaction extends ConnectionPool{

    

	private static long hits = 0, lookups = 0;
	private static boolean printCacheStatistics = DBConfig.getPrintCacheStatistics();
	private static Map tableNames = new ConcurrentHashMap(); 
	
	private boolean available = true/*,autostartTransaction = false*/;

	private int isolationlevel,prev_isolationlevel;
	private DBConnectionPool pool;
	private DBConnection con;
	private HashSet listeners;
	private Map queryCache;
	private BusinessObject_Impl deferedForUpdate;
	private boolean fullClearCacheOnUpdate = true;
	
	public void useQueryResultCache(boolean cache){
		queryCache = cache?(fullClearCacheOnUpdate?new HashMap():new ResultCache()):null;
	}
	
	
	public void fullClearResultCacheOnUpdates(boolean fullClearCaches){
		if (fullClearCacheOnUpdate != fullClearCaches && queryCache != null){
			queryCache = fullClearCaches?new HashMap():new ResultCache();
		}
		fullClearCacheOnUpdate = fullClearCaches;
	}	 
	
	public void clearQueryResultCache(){
		if (queryCache != null)
			queryCache.clear();
	}
	
	public void clearQueryResultCache(BusinessObject_Impl impl){
		if (queryCache != null && !queryCache.isEmpty()){
			if (fullClearCacheOnUpdate  || impl == null){
				queryCache.clear();
			}else{
				String tableName = (String)tableNames.get(impl);
				if (tableName == null){
					tableName = impl.getPersistenceMetaData().getTableName(this);
					tableNames.put(impl,tableName);
				}
				if (tableName == null || tableName.length()==0)
					queryCache.clear();
				else	
					((ResultCache)queryCache).removeKeysContaining(tableName);
			}	
		}	
	}
	
	public Object getCachedResult(String query) throws Exception{
		if (deferedForUpdate != null){
			deferedForUpdate.save();
			deferedForUpdate = null;
		}
		Object c =  queryCache == null?null:queryCache.get(query);
		if (printCacheStatistics && queryCache != null) {
			lookups++;
			hits = c != null?hits+1:hits;
			if (lookups%50==0){
				Log.info("Hit ratio: "+((hits * 100) / lookups)+"%. ("+lookups+" lookups,"+hits+" hits)");
			}
		}
		if (c instanceof ArrayListImpl)
			return ((ArrayListImpl)c).getBaseCopy();
		else
			return c;
	}
	
	public void setCachedResult(String query, Object result){
		if (queryCache != null){
			if (result instanceof ArrayListImpl)
				((ArrayListImpl)result).fixate();
			queryCache.put(query,result);
		}	
	}
	
	public AtomicDBTransaction(DBData data) {
		pool = DBConnectionPool.getInstance(data);
		init();
	}

	public boolean isActive() {
		return (con != null);
	}

	private void init() {
		try {
			
			if (!pool.getDatabaseMetaData().supportsTransactions()) {
				pool = null;
				throw new Exception("msg-transactions-not-suported-by-database");
			}
			isolationlevel = pool.getDatabaseMetaData().getDefaultTransactionIsolation();
			if (isolationlevel == DBTransaction.TRANSACTION_NONE) {
				if (pool.getDatabaseMetaData().supportsTransactionIsolationLevel(DBTransaction.TRANSACTION_READ_COMMITTED))
					isolationlevel = DBTransaction.TRANSACTION_READ_COMMITTED;
				else if (pool.getDatabaseMetaData().supportsTransactionIsolationLevel(DBTransaction.TRANSACTION_REPEATABLE_READ))
					isolationlevel = DBTransaction.TRANSACTION_REPEATABLE_READ;
				else if (pool.getDatabaseMetaData().supportsTransactionIsolationLevel(DBTransaction.TRANSACTION_SERIALIZABLE))
					isolationlevel = DBTransaction.TRANSACTION_SERIALIZABLE;
				else if (pool.getDatabaseMetaData().supportsTransactionIsolationLevel(DBTransaction.TRANSACTION_READ_UNCOMMITTED))
					isolationlevel = DBTransaction.TRANSACTION_READ_UNCOMMITTED;
				else {
					pool = null;
					throw new Exception("msg-transactions-not-upurted-by-database");
				}
			} else if (isolationlevel < DBTransaction.TRANSACTION_READ_COMMITTED && pool.getDatabaseMetaData().supportsTransactionIsolationLevel(DBTransaction.TRANSACTION_READ_COMMITTED)) {
				isolationlevel = DBTransaction.TRANSACTION_READ_COMMITTED;
			}
		} catch (Exception e) {
			Log.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public ConnectionProvider getConnectionProvider() {
		return pool;
	}

	public boolean readUncommitted() {
		return isolationlevel <= DBTransaction.TRANSACTION_READ_UNCOMMITTED;
	}

	public void setTransActionIsolationLevel(int level) throws Exception {
		if (!pool.getDatabaseMetaData().supportsTransactionIsolationLevel(level)) {
			throw new Exception("msg-requested-transaction-isolation-level-not-supported-by-database");
		} else if (level == DBTransaction.TRANSACTION_NONE) {
			throw new Exception("msg-requested-transaction-isolation-level-none-is-not-permitted-here");
		} else {
			isolationlevel = level;
		}
	}

	public int getTransActionIsolationLevel() {
		return isolationlevel;
	}

	void begin() throws Exception {
		if (deferedForUpdate != null)	
			throw new Exception("Bug in JSQL; deferedForUpdate not null at beginning of transaction");
		if (con == null) {
			try {
				con = pool.getConnection();
				prev_isolationlevel = con.getConnection().getTransactionIsolation();
				if (prev_isolationlevel != isolationlevel)
					con.getConnection().setTransactionIsolation(isolationlevel);
				con.getConnection().setAutoCommit(false);
			} catch (Exception e) {
				Log.error(e.getMessage());
				e.printStackTrace();
				throw e;
			}
		}
	}
	
	public DBConnection getConnectionForUpdate() throws Exception {
		return getConnectionForUpdate(null);
	}

	public DBConnection getConnectionForUpdate(BusinessObject_Impl impl) throws Exception {
		clearQueryResultCache(impl);
		if (deferedForUpdate != null){		
			if (deferedForUpdate == impl)
				deferedForUpdate = null;
			else		
				setDeferedForUpdate(null);
		}				
		return internalGetConnection();
	}

	protected synchronized DBConnection internalGetConnection() throws Exception {
		if (con == null) {
			throw new Exception("msg-tried-to-use-a-transaction-connection-that-was-not-yet-active");
		}
		while (available == false) {
			try {
				wait(); // wait for the other thread to return the connection
			} catch (InterruptedException e) {
			}
		}
		available = false;
		
		return con;
	}

	public synchronized DBConnection getConnection() throws Exception {
		if (deferedForUpdate != null){
			deferedForUpdate.save();
			deferedForUpdate = null;
		}
		if (con != null) {
			return internalGetConnection(); 
		} else {
			return pool.getConnection();
		}
	}
	

	public synchronized void returnConnection(DBConnection connection) throws Exception {
		try {
			if (connection == null) {
				throw new Exception("msg-returned-connection-was-null-check-programm");
			} else if (!connection.isConnected()) {
				throw new Exception("msg-returned-connection-lost-connection-check-programm");
			} else if (con != connection) {
				pool.returnConnection(connection);
			} else {
				available = true;
				notifyAll(); // notify threads that are waiting for the connection that it is now available
			}
		} catch (Exception e) {
			Log.error(e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}

	public void addListener(DBTransactionListener listener) {
		if (listeners == null)
			listeners = new HashSet();
		listeners.add(listener);
	}

	void rollback() throws Exception {
		rollback(true);
	}

	void rollback(boolean includeBOFields) throws Exception {
		deferedForUpdate = null;	
		if (con != null) {
			try {
				con.getConnection().rollback();
				try {
					con.getConnection().setAutoCommit(true);
					con.getConnection().setTransactionIsolation(prev_isolationlevel);
					pool.returnConnection(con);
				} catch (Exception e) {
					Log.error(e.getMessage());
					e.printStackTrace();
					Log.error("mdg-could-not-return-connection-to-pool-on-rollback-connection-will-be-closed");
					con.getConnection().close();
				}
				con = null;
				if (listeners != null && !listeners.isEmpty()) {
					Iterator it = listeners.iterator();
					while (it.hasNext())
						 ((DBTransactionListener) it.next()).rollback(includeBOFields);
					listeners.clear();
				}
			} catch (Exception e) {
				Log.error(e.getMessage());
				e.printStackTrace();
				throw e;
			}
		}
	}

	void commit() throws Exception {
		setDeferedForUpdate(null);	
		if (con != null) {
			try {
				con.getConnection().commit();
				con.getConnection().setAutoCommit(true);
				con.getConnection().setTransactionIsolation(prev_isolationlevel);
				pool.returnConnection(con);
				con = null;
				if (listeners != null && !listeners.isEmpty()) {
					for (Iterator it = listeners.iterator();it.hasNext();)
						 ((DBTransactionListener) it.next()).commit();
					listeners.clear();
				}
			} catch (Exception e) {
				Log.error(e.getMessage());
				e.printStackTrace();
				throw e;
			}
		}
	}

	void forceClose() {
		Log.warn("msg-only-call-forseClose-if-rollback-has-thrown-an-exception-as-a-last-resort!");
		if (con != null) {
			try {
				if (!con.getConnection().getAutoCommit())
					con.getConnection().rollback();
			} catch (Exception e) {
				Log.error(e.getMessage());
				e.printStackTrace();
			}
			try {
				con.getConnection().close();
			} catch (Exception e) {
				Log.error(e.getMessage());
				e.printStackTrace();
			}
			con = null;
		}
		if (listeners != null && !listeners.isEmpty()) {
			Iterator it = listeners.iterator();
			while (it.hasNext()) {
				try {
					((DBTransactionListener) it.next()).rollback(true);
				} catch (Exception e) {
					Log.error(e.getMessage());
					e.printStackTrace();
				}
			}
			listeners.clear();
		}
	}

	public String getPrefix() {
		return pool.getPrefix();
	}

	public String getDriver() {
		return pool.getDriver();
	}

	public DBMapping getDBMapping() {
		return pool.getDBMapping();
	}

	public ORMapping getORMapping() {
		return pool.getORMapping();
	}

	public boolean sameDataBase(ConnectionPool otherPool) {
		return (getDBId() == otherPool.getDBId());
	}

	public int getDBId() {
		return pool.getDBId();
	}
	
	public DBData getDBData() {
		return pool.getDBData();
	}
		
	public String getSearchStringEscape() {
		return pool.getSearchStringEscape();
	}
	
	protected void finalize() throws Throwable {
		if (con != null) {
			try {
				if (con.getConnection() != null)
					try{con.getConnection().rollback();
					}catch (Exception e){}
				con = null;
				if (listeners != null && !listeners.isEmpty()) {
					Iterator it = listeners.iterator();
					while (it.hasNext())
						 ((DBTransactionListener) it.next()).rollback(true);
					listeners.clear();
				}
			} catch (Exception e) {
				Log.error(e.getMessage());
				e.printStackTrace();
				throw e;
			}
		}
	}
	
	public void setDeferedForUpdate(BusinessObject_Impl impl) throws Exception {
		if (deferedForUpdate == null){
			deferedForUpdate = impl;
		}else if (deferedForUpdate != impl){
			BusinessObject_Impl updatable = deferedForUpdate;
			deferedForUpdate = null;
			updatable.save();
			deferedForUpdate = impl;	
		}
	}
	
	private final static class ResultCache extends HashMap{
		ArrayList keys;
		
	
		public ResultCache() {
			super(64);
			keys = new ArrayList(64);
		}

		public Object put(Object key, Object value) {
			keys.add(key);
			return super.put(key, value);
		}
		
		public void removeKeysContaining(String substring){
			for (int i = 0; i < keys.size(); i++) {
				String key = (String)keys.get(i);
				if (key.indexOf(substring) >= 0){
					super.remove(key);
					keys.remove(i--);
				}
			}
		}
		
		public void clear() {
			super.clear();
			keys.clear();
		}

	}
	
	
}

