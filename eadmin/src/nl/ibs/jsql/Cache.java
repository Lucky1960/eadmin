package nl.ibs.jsql;



import java.util.Collection;
import java.util.Iterator;
import nl.ibs.jeelog.*;
import nl.ibs.jsql.impl.*;


/**
 * ObjectCache 
 * 
 * @author c_hc01
 *
 */
public class Cache {

	
	private static long lookups, hits;
	protected static boolean printCacheStatistics = DBConfig.getPrintCacheStatistics(); 

	private ConcurrentHashMap cache = new ConcurrentHashMap();
	
	/**
	 * Returns cached object based on hash code, if cache does not contain object with same hash code
	 * object will be added to cache before it is returned.
	 *
	 * @param object to cache or be replaced by cached equivalent.
	 */

	public Object cache(Object o) {
		if (o == null)
			return o;
		Integer key = new Integer(o.hashCode());
		Object c = cache.get(key);
		if (c != null)
			return c;
		cache.put(key, o);
		return o;
	}
	
	

	/**
	 * Removes for every entry in the collection the first object from the cache for wich cachedObject.equals(object) == true.
	 *
	 * @param Collection of objects to be removed from cache.
	 */
	public void removeFromCache(Collection c) {
		if (c == null || c.isEmpty())
			return;
		Iterator it = c.iterator();
		while (it.hasNext()) {
			cache.remove(it.next());
		}
	}

	/**
	 * Removes first object from cache for wich cachedObject.equals(object) == true.
	 *
	 * @param object to be removed from cache.
	 */
	public void removeFromCache(Object o) {
		if (cache == null || o == null)
			return;
		cache.remove(o instanceof Integer?o:new Integer(o.hashCode()));
	}
	
	/**
	 * Get object from cache.
	 *
	 * @param key.
	 */
	public Object get(Object o) {
		Object c = (cache == null || o == null)?null:cache.get(o instanceof Integer?o:new Integer(o.hashCode()));
		if (printCacheStatistics) {
			lookups++;
			hits = c != null?hits+1:hits;
			if (lookups%1000==0){
				Log.info("Hit ratio: "+((hits * 100) / lookups)+"%. ("+lookups+" lookups,"+hits+" hits)");
			}
		}
		return c;
	}
		
	/**
	 * Returns the number of cached objects.
	 *
	 * @return the number of cached objects.
	 */
	public int size(){
		return cache == null?0:cache.size();
	}
	
	/**
	 * Clears this Cache so that it contains no cached objects. 
	 */

	public void clear(){
		if (cache != null)
			cache.clear();
	}
	
	/**
	 * Returns a Collection view of the values contained in this cache.
	 * The Collection is backed by the cache, so changes to the cache
	 * are reflected in the Collection, and vice-versa.  The Collection
	 * supports element removal (which removes the corresponding entry from
	 * the cache), but not element addition.
	 *
	 * @return a collection view of the values contained in this cache.
	 */
	public Collection values(){
		return cache==null?new java.util.ArrayList():cache.values();
	}
	

}

