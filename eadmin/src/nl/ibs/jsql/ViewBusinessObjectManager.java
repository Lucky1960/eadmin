package nl.ibs.jsql;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

import nl.ibs.jsql.impl.NoResultQuery;
import nl.ibs.jsql.sql.BusinessObjectManager;
import nl.ibs.jsql.sql.ConnectionProvider;
import nl.ibs.jsql.sql.DBConnection;
import nl.ibs.jsql.sql.PersistenceMetaData;
import nl.ibs.jsql.sql.PersistenceViewWrapper;
import nl.ibs.jsql.sql.ViewNameGenerator;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

/**
 * @author c_md07
 *
 */
public class ViewBusinessObjectManager extends BusinessObjectManager {
	BusinessObjectManager target = null;
	String viewName = ViewNameGenerator.getInstance().getNewViewName();
	PersistenceViewWrapper pmdvw = null;

	public ViewBusinessObjectManager(
		BusinessObjectManager target,
		String filter,
		Map params,
		Class[] imports)
		throws Exception {
		if (target == null)
			throw new IllegalArgumentException("target on view can't be null");
		this.target = target;
		pmdvw =
			new PersistenceViewWrapper(
				target.getPersistenceMetaData(),
				viewName);

		String intFilter =
			"CREATE VIEW "
				+ target.getConnectionProvider().getPrefix()
				+ viewName
				+ " AS "
				+ filter;
		try {
			executeQuery(target, params, intFilter, imports);
		} catch (Exception e) {
			delete();
			executeQuery(target, params, intFilter, imports);
		}
	}

	private void executeQuery(
		BusinessObjectManager target,
		Map params,
		String intFilter,
		Class[] imports)
		throws Exception {
		NoResultQuery qry = new NoResultQuery(intFilter, 1);
		for (int i = 0; imports != null && i < imports.length; i++)
			qry.addImport(imports[i]);
		qry.setParameters(params);
		qry.execute(target);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.BusinessObjectManager#getConnectionProvider()
	 */
	public ConnectionProvider getConnectionProvider() throws Exception  {
		return target.getConnectionProvider();
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.BusinessObjectManager#getPersistenceMetaData()
	 */
	public PersistenceMetaData getPersistenceMetaData() {
		return pmdvw;
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.BusinessObjectManager#getBusinessObject(java.sql.ResultSet)
	 */
	protected Object getBusinessObject(ResultSet rs) throws Exception {
		return target.getBusinessObjectForView(rs);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.BusinessObjectManager#getCollection(java.sql.ResultSet)
	 */
	protected Collection getCollection(ResultSet rs) throws Exception {
		return target.getCollectionForView(rs);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.BusinessObjectManager#getListIterator(java.sql.ResultSet)
	 */
	protected ListIterator getListIterator(ResultSet rs) throws Exception {
		return target.getListIteratorForView(rs);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.BusinessObjectManager#getDocument(java.sql.ResultSet)
	 */
	protected Document getDocument(ResultSet resultSet) throws Exception {
		return target.getDocumentForView(resultSet);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.BusinessObjectManager#getDocumentFragment(java.sql.ResultSet)
	 */
	protected DocumentFragment getDocumentFragment(ResultSet resultSet)
		throws Exception {
		return target.getDocumentFragmentForView(resultSet);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.BusinessObjectManager#getDocumentFragmentArrayList(java.sql.ResultSet)
	 */
	protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet)
		throws Exception {
		return target.getDocumentFragmentArrayListForView(resultSet);
	}

	public void delete() throws Exception {
		String intFilter =
			"DROP VIEW "
				+ target.getConnectionProvider().getPrefix()
				+ viewName;
		NoResultQuery qry = new NoResultQuery(intFilter, 1);
		qry.execute(target);
	}
	
	public String getViewName() throws Exception {
		return target.getConnectionProvider().getPrefix() + viewName;
	}
}

