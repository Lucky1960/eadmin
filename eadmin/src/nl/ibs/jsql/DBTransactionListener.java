package nl.ibs.jsql;



import java.util.*;

public interface DBTransactionListener extends EventListener{

    public void commit() throws Exception;
    public void rollback(boolean includeBOFields) throws Exception;
    
}

