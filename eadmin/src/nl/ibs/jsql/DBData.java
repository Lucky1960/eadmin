package nl.ibs.jsql;



import java.io.Serializable;
import java.util.HashMap;
import java.util.Stack;
import java.sql.*;
import nl.ibs.jeelog.*;
import java.util.*;

/**
 * Data Base Data
 * @author  c_hc01
 * @version 1.1
 */
public class DBData implements Serializable{
    
	
	private static InheritableThreadLocal currentDBDataMap = new InheritableThreadLocal() {
		protected Object initialValue() {
			return new HashMap(1);
		}
	};

	
	private int hashCode;

	protected String application = ""; // holds the application name
	protected String name = ""; // holds the current Name
	protected String url = ""; // holds the current URL
	protected String driver = ""; // holds the current driver
	protected String user = ""; // holds the current user
	protected String password = ""; // holds the current password
	protected String schema = "";

	protected boolean autoCreateTables = true;
	protected boolean autoAddColumns = true;
	protected boolean autoModifyColumns = false;
	protected boolean autoDropColumns = false;

	private DBData(String application) {
		this(application, null,DBConfig.getJDBCDriver(application),DBConfig.getURL(application),DBConfig.getSchema(application),DBConfig.getUser(application),DBConfig.getPassword(application));
	}

	/**
	 * Get the currently active default DBData for this thread (and all its child threads).
	 * @return currently active DBData;
	 */
	public static DBData getDefaultDBData(String application) {
		DBData dbData = (DBData) ((HashMap)currentDBDataMap.get()).get(application);
		if (dbData == null){
			dbData = new DBData(application);
			((HashMap)currentDBDataMap.get()).put(application,dbData); 
		}
		return dbData;
	}
	
	/**
	 * Set a new default DBData on the current thread and all its child threads.
	 * @param defaultDBD DBData
	 */
	static void setDefaultDBData(DBData defaultDBD) {
		((HashMap)currentDBDataMap.get()).put(defaultDBD.getApplication(),defaultDBD);
	}


     /** Creation a DBData.
      * <P>
      * @param String _application
      * @param String _name
      * @param String _driver
      * @param String _url
      * @param String _schema
      * @param String _user
      * @param String _password
      */
     public DBData(String _application, String _name, String _driver, String _url, String _schema, String _user, String _password){ 
    	 if (_application == null || _application.trim().length()==0 )
    		 throw new RuntimeException("Tried to create a DBData object without providing a name. This is not allowed");
    	application =  _application;
    	name =  _name;
        url= _url;
        driver = _driver;
        schema = _schema;
        user = _user;
        password = _password;
        String dbidString = _driver + _url + _schema + _user;
        if (name == null || name.trim().equals(""))
            name = dbidString; 
        hashCode = dbidString.hashCode();
		autoCreateTables   =  DBConfig.getAutoCreateTables(application);
		autoAddColumns     =  DBConfig.getAutoAddColumns(application);
		autoModifyColumns  =  DBConfig.getAutoModifyColumns(application);
		autoDropColumns    =  DBConfig.getAutoDropColumns(application);        	    
     }

     /** Creation a DBData.
      * <P>
      * @param String _driver
      * @param String _url
      * @param String _schema
      * @param String _user
      * @param String _password
      * @param boolean _autoCreateTables
      * @param boolean _autoAddColumns
      */
     public DBData(String application, String _name, String _driver, String _url, String _schema, String _user, String _password, boolean _autoCreateTables, boolean _autoAddColumns){
        this(application,  _name, _driver, _url, _schema,  _user, _password);
        autoCreateTables   = _autoCreateTables;
        autoAddColumns     = _autoAddColumns;
     }

     /** Creation a DBData.
      * <P>
      * @param String _driver
      * @param String _url
      * @param String _schema
      * @param String _user
      * @param String _password
      * @param boolean _autoCreateTables
      * @param boolean _autoAddColumns
      * @param boolean _autoModifyColumns
      * @param boolean _autoDropColumns        
      */
     public DBData(String application, String _name, String _driver, String _url, String _schema, String _user, String _password, boolean _autoCreateTables, boolean _autoAddColumns, boolean _autoModifyColumns, boolean _autoDropColumns){
        	this(application,  _name, _driver, _url, _schema,  _user, _password, _autoCreateTables, _autoAddColumns );
        	autoModifyColumns  = _autoModifyColumns;
        	autoDropColumns    = _autoDropColumns;
     }

     public String getName(){
        	return name;
     }

     public String getURL(){
        	return url;
     }

     public String getDriver(){
        	return driver;
     }

     public String getSchema(){
        	return schema;
     }

     public String getUser(){
        	return user;
     }

     public String getPassword(){
        	return password;
     }

     public boolean getAutoCreateTables(){
        	return autoCreateTables;
     }

     public boolean getAutoAddColumns(){
        	return autoAddColumns;
     }

     public boolean getAutoModifyColumns(){
        	return autoModifyColumns;
     }

     public boolean getAutoDropColumns(){
        	return autoDropColumns;
     }
    
     public int hashCode() {
		return hashCode;
     }
	
     public int getDBId(){
        	return hashCode;
     }

     public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if ((obj instanceof DBData) == false)
			return false;
		if (hashCode() != obj.hashCode())
			return false;	 
		return true;
     }

	public String getApplication() {
		return application;
	}
	
	/**
	 * One of two utility method used to put content on session and back
	 * @return
	 */
	public static HashMap getMap(){
		return (HashMap)currentDBDataMap.get();
	}
	
	/**
	 * One of two utility method used to put content on session and back
	 * @param map
	 */
	public static void setMap(HashMap map){
		currentDBDataMap.set(map);
	}
}

