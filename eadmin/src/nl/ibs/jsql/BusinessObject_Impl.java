package nl.ibs.jsql;


import nl.ibs.jsql.sql.*;


import java.io.Serializable;
import java.util.*;

public interface BusinessObject_Impl extends BusinessObject, Serializable{
 
    public Object addCachedRelationObject(String relation, Object object);
    public void clearCachedRelation(String string);
    public void assureStorage() throws Exception;
    public PersistenceMetaData getPersistenceMetaData();
    public boolean isDeleted();
    public void save() throws Exception;
	

}

