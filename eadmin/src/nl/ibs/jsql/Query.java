package nl.ibs.jsql;


import java.sql.*;
import java.util.*;
import nl.ibs.jsql.sql.*;



/**
 * Query objects are used to collect data from the persistence layer in a ordered fashion.
 * The most importand methods of the Query interface are:
 * <ul>
 *  <li><code>setFilter(String filter)</code></li>, and
 *  <li><code>setOrdering(String ordering)</code></li>
 * </ul> 
 * The filter sets de conditions to be satisfied for objects to be part of the collection and the ordering determines the order within the collection.
 * <p>
 * The most important concept is the <u>context</u> in which the Query is executed. The context determines amongs other things the <u>type</u> of objects to be collected.
 * <p> 
 * <b><h3><A NAME="Context">Context</A></h3></b>
 * Query's are used in a certain context and the result of the query depends on this context.
 * <p>
 * For example:
 * <p>
 * Suppose there exists a BusinessObject User and a BusinessObject CD that both have an instance variable name of type String. Suppose there also exists an BusinessObject Store that has an OwningRelation with CD called stock.
 * <p>
 * Now suppose we created a Query and set the next simple filter:
 * <blockquote><code>" name == 'ABC' "</code> </blockquote>
 * Now this query will return an collection of Users with the name <code>"ABC"</code>
 * if it is passed to the method:
 * <blockquote><code>UserManager.getCollection(Query query);</code></blockquote>
 * But the same query will return a collection of CDs with name <code>"ABC"</code> if it is passed to the method:
 * <blockquote><code>CDManager.getCollection(Query query);</code></blockquote>
 * And the same query will return a collection of CDs with name <code>"ABC"</code> owned by a particular store if it is passed to the method:
 * <blockquote><code>store.getCDCollection(Query query);</code></blockquote>
 * where store is the particularinstance of Store.
 * <p>
 * A special case is {@link ExecutableQuery} that is created with a Context and thus can be executed to collect objects from that context.
 * <p>
 * <p>
 * <b><h3><A NAME="Filter">Filter</A></h3></b>
 * <p>
 * The filter sets de conditions to be satisfied for objects to be part of the collection.
 * <p>
 * Conceptually it is enlightening to understand that the filter string is translated by the Query implementation in an SQL string that will be inserted in the where clause of an SQL query statement.
 * <p> 
 * For example the filter string:<p>
 * <blockquote><code><b>" name = 'ABC' "</b></code></blockquote>
 * could end up in the following SQL query statement if that that filter is set on a Query that is passed to the method <code>UserManager.getCollection(Query query)</code>:
 * <p>
 * <blockquote><code>"SELECT * FROM MYSCHEMA.USRSTBL WHERE <b>MYSCHEMA.USRSTBL.NAME = 'ABC'</b>"</code></blockquote> 
 * <p>
 * Basically any SQL92 entry level statement that could be part of the WHERE clause of an SQL query statement can also be part of the filter string. Including (correlated) subquerys.
 * <p>
 * For example if there is a User BusinessObject defined with an instance variable <code>dateOfBirth</code> of type <code>Date</code>. The next filter (if used in the proper context)
 * will select the youngest user(s):
 * <p>
 * <blockquote><code>" dateOfBirth = (select max(dateOfBirth) from User) "</code></blockquote>
 * <p>
 * If the User BusinessObject furthermore has a instance variable <code>gender</code> of type <code>String</code>. The next filter can be applied
 * to find the youngest male user(s):
 * <p>
 * <blockquote><code>" gender = 'M' && dateOfBirth = (select max(dateOfBirth) from User where gender = 'M') "</code></blockquote>
 * <p>
 * <b><h4><u><A NAME="Parameters">Parameters</A></u></h4></b>
 * <p>
 * One could use one query object to find first the youngest male user(s) and next the youngest female user(s) by introducing a parameter for gender:
 * <p> 
 * <blockquote><code>" gender = ?gender && dateOfBirth = (select max(dateOfBirth) from User where gender = ?gender) "</code></blockquote>
 * <p>
 * In the latter case before using the query to get youngest male user(s) one would have to invoke:
 * <p>
 * <blockquote><code>query.setParameter("gender","M");</code></blockquote> 
 * <p>
 * And before using the query to get youngest female user(s) one would have to invoke:
 * <p>
 * <blockquote><code>query.setParameter("gender","F");</code></blockquote>
 * <p>
 * <p>Prefixing the names of parameters with question marks in the filter string is,  although advisable, only a necessity when this name
 * can be intepreted by the implementation also as an instance variable name: which is clearly the case in the previous example. If we assume that
 * there is no instance variable with the name "genderValue" in the particular context or on a imported BusinessObject the next filter would do just as well:
 * <blockquote><code>" gender = genderValue && dateOfBirth = (select max(dateOfBirth) from User where gender = genderValue) "</code></blockquote>
 * <p>
 * In which case before using the query to get youngest male user(s) one would now have to invoke:
 * <p>
 * <blockquote><code>query.setParameter("genderValue","M");</code></blockquote> 
 * <p>
 * And before using the query to get youngest female user(s) one would now have to invoke:
 * <p>
 * <blockquote><code>query.setParameter("genderValue","F");</code></blockquote>
 * <p>  
 * <b><h4><u><A NAME="Caching">Caching</A></u></h4></b>
 * If the same query is used mulitple times, preformance can be improved (sometimes dramatically) by invoking:
 * <blockquote><p><code>setCacheable(true);</code></blockquote>
 * <p> 
 * <b><h4><u><A NAME="Imports">Imports</A></u></h4></b>
 * <p>
 * In order to use BusinessObjects in subquerys that are not related to the BusinessObject from the current context one has to
 * import this BusinessObject by invoking one of the <code>addImport</code> methods.
 * <p>
 * For example suppose that  BusinessObject User has also a instance variable named <code>annualIncome</code> and there is a BusinessObject called UtrechtInhabitant with an instanceVariable of
 * the same type with the same name, then after invoking 
 * <blockquote><code>addImport(UtrechtInhabitant.class);</code></blockquote>
 * the next filter could be used to get all users with an annualIcome in excess of the average
 * income of inhabitants of the city of Utrecht:
 * <p>
 * <blockquote><code>" annualIncome > (select avg(UtrechtInhabitant.annualIncome) from UtrechtInhabitant) "</code></blockquote>
 * <p>
 * Alternatively after invoking 
 * <blockquote><code>addImport("xyz" , UtrechtInhabitant.class);</code></blockquote>
 * the next filter could be used to the same effect:
 * <p>
 * <blockquote><code>" annualIncome > (select avg(xyz.annualIncome) from xyz) "</code></blockquote>
 * <p>
 * <b><h4><u><A NAME="Navigation">Navigation</A></u></h4></b>
 * <p>
 * One can navigate within a filter through DependentObjects by simple dot notation and through related BusinessObjects in using the "contains" keyword.
 * <p>
 * <u>Example 1 navigating trough DependentObject:</u>
 * <p>
 * Suppose we have a BusinessObject Store that has a DependentObject instance variable "price" of type CurrencyValue, and CurrencyValue has an instance variable named "currency" of type String.
 * <p>
 * Then next filter would return all Stores with a price in "EUR"
 * <blockquote><code>" Store.price.currency == 'EUR' "</code></blockquote>
 * </p> 
 * <u>Example 2 navigating through related BusinessObject:</u>
 * <p>
 * Suppose we have a BusinessObject Store that has a owning relation with name "cd" that has as its target a BusinessObject CD. 
 * and BusinessOject CD has in instance variable "artist" of type String. Then any of the next filters could be used to find all Stores that have a one ore more CDs from an artist called 'U-roy'  
 * <ul>
 *  <li><code>" cd.contains(artist = 'U-roy') "</code></li>
 *  <li><code>" cd.contains(artist == 'U-roy') "</code></li>
 *  <li><code>" Store.cd.contains(artist = 'U-roy') "</code></li>
 *  <li><code>" Store.cd.contains(artist == 'U-roy') "</code></li>
 *  <li><code>" cd.contains(CD.artist = 'U-roy') "</code></li>
 *  <li><code>" cd.contains(CD.artist == 'U-roy') "</code></li>
 *  <li><code>" Store.cd.contains(CD.artist = 'U-roy') "</code></li>
 *  <li><code>" Store.cd.contains(CD.artist == 'U-roy') "</code></li>
 *  <li><code>" cd.contains(compactDisk) & (compactDisk.artist = 'U-roy') "</code></li>
 *  <li><code>" cd.contains(compactDisk) && (compactDisk.artist == 'U-roy') "</code></li>
 *  <li><code>" cd.contains(compactDisk) AND (compactDisk.artist = 'U-roy') "</code></li>
 *  <li><code>" cd.contains(compactDisk) and (compactDisk.artist == 'U-roy') "</code></li>
 *  <li><code>" Store.cd.contains(compactDisk) & (compactDisk.artist = 'U-roy') "</code></li>
 *  <li><code>" Store.cd.contains(compactDisk) && (compactDisk.artist == 'U-roy') "</code></li>
 *  <li><code>" Store.cd.contains(compactDisk) AND (compactDisk.artist = 'U-roy') "</code></li>
 *  <li><code>" Store.cd.contains(compactDisk) and (compactDisk.artist == 'U-roy') "</code></li>
 * </ul>
 * <p>
 * <b><h4><u><A NAME="Backward navigation">Backward navigation</A></u></h4></b>
 * <p>
 * Furthermore it is also possible to navigate backwards trough objects that have a relation with the BusinessObject in the current context as its target.
 * The default language contruct is:
 * <ul>
 *  <li>"ownedBy" + name(first position in upper case) of owning BusinessObject + "As" + name(first position in upper case) of OwningRelation, for OwningRelations </li>
 *  <li>"referencedBy" + name(first position in upper case) of refering BusinessObject + "As" + name(first position in upper case) of ReferingRelation, for ReferingRelations </li>
 * </ul> 
 * <p>
 * For example if the BusinessObject that forms the current context is CD and on BusinessObject Store is an OwningRelation defined named 'Stock' with CD as its target, then the next
 * filter can be used to return all CD that are owned by a store (or stores) with the name 'Distortion Records':
 * </p>
 * <blockquote><code>" CD.<b>ownedBy</b>Store<b>As</b>Stock.contains(name='Distortion Records') "</code></blockquote>
 * <p>
 * If a name is supplied for the backward reference this can also be used thus if in the previous example the name of the backward reference is 
 * is defined as 'store' the next filter would have the same result:
 * <blockquote><code>" CD.store.contains(name='Distortion Records')" </code></blockquote>
 * <p>
 *
 * Furthermore navigation in both directions can be used simultaniously and there is no limit how deep one can navigate trough related
 * BusinessObjects. For instance the next filter could be used to get all CDs that are owned by a store (or stores) with a owner named 'Smith' and that also ownes Records from
 * an artist called 'Prince':
 * <p>
 * <blockquote><code>" store.contains(owner.contains(name = 'Smith') && cD.contains(artist = "Prince")) "</code></blockquote>
 * <p>
 * <p>
 * <b><u>WARNING:</u></b> Although all filtering is completely delegated to the underlying database including object navigation, still navigation through objects other then dependent objects
 * in a filter can have severe performance penalties.
 * </p>
 * <p>
 * <b><u>TIP:</u></b>
 * If you can not avoid object navigation try to reduce the amount of navigation that the underlying database has to do
 * by reducing the amount of objects to be navigated. You can do this by moving statements
 * that reduce the result set as much as possible to the front of the filter. For example:
 * <p><blockquote><code>" CD.artist == 'Prince' && CD.store.contains(name='Distortion Records')" </code></blockquote></p>
 * will reduce the objects that have to be navigated to only the CD�s of 'Prince'. This filter might return result in a few milliseconds, while
 * the next filter could need minutes or even hours to resolve:
 * <blockquote><code>"CD.store.contains(name='Distortion Records')  && CD.artist == 'Prince'  " </code></blockquote>
 * If you would just need to get all records from the Store with the name 'Distortion Records' it's much more save to first get this store and
 * then invoke:
 * <p><blockquote><code>store.getCDCollection(null);</code></blockquote></p>
 * or something simular.
 * </p>  
 *  
 * <b><h4><u><A NAME="Qualifying names">Qualifying names</A></u></h4></b>
 * <p> 
 * Instance names can be qualified by prefixing them either with the BusinessObject name or an alias name.<br/> 
 * For instance the qualified name of and instance variable named <code>title</code> could be:
 * <ul>
 *  <li><code>" Book.title "</code>,</li>
 *  <li><code>" CD.title "</code>, or </li>
 *  <li><code>" someAlliasName.title "</code>.</li>
 * </ul>
 * <p>
 * <p>
 * <b><h4><u><A NAME="Aliases">Aliases</A></u></h4></b>
 * <p> 
 * Aliases can be supplied either by invoking one of the following methods:
 * <ul>
 *  <li> <code>addParameter(String alias , Class businessObject)</code> or </li>
 *  <li> <code>addParameter(String alias , BusinessObjectManager manager)</code></li>
 * </ul>
 * or by supplying them when navigating through related businessObjects:
 * <p>
 * <blockquote><code>" Store.cd.contains(someAliasName) && (someAliasName.title = 'Do it right' && someAliasName.artist = 'U-roy')) "</code>.
 * </blockquote>
 * <p>
 * <b><h4><u><A NAME="Using unqualified instance names">Using unqualified instance names</A></u></h4></b>
 * <p> 
 * It is important to understand that during the translation of unqualified instance names the Query implementation searches for a BusinessObject that has an the instance variable with the same name <u>backwards</u>
 * trough the stack of BusinessObjects that is the result of the shifting contexts as the Query implementation navigates at translation time trough related objects and/or subqueries. 
 * If not found at the stack of contexts, the implementation continues searching the imports for a BusinessObject with a instance variable with the unqualified name.
 * If still no BusinessObject found, the implementation finally will interpreted the unqualified name as
 * a parameter and will try to replace it during execution time wih a supported parameter value. If no such parameter set the exeqution of the query will fail.
 * <p> 
 * For example:
 * <p>
 * When translating the following filter:
 * <p>
 * <blockquote><code>" Store.cd(CD.title = 'Do it right' && artist in (select artist from Artist where style = 'Toasting') "</code></blockquote>
 * <p>
 * the implementattion first successively searches:
 * <ol>
 * <li><code>Artist</code>,</li>
 * <li><code>CD</code>, and finaly </li>
 * <li><code>Store</code></li>
 * </ol>
 * for an instance variable named <code>style</code>.
 * If still not found it will check the Imported BusinessObjects for an instance variable <code>style</code> and finally will assume it is a parameter. 
 * <p>
 * <b>WARNING: The use of unqualified instance names that are also reserved words in SQL (ie BETWEEN or between) will almost certain result in translation and/or execution errors! </b>
 * <p>
 * <b><h4><u><A NAME="SQL keywords">Escaping</A></u></h4></b>
 * <br/> 
 * If you use single quotes for string literals in your filter clause you can escape in this string literal contained
 * singel quotes by prefixing them with a single quote, if you use double quotes for string literals you can escape in this string literal contained
 * double quotes by prefixing them with a double quote.<br/>
 * You don't need and should not escape quotes in parameter values.<br/><br/> 
 * If you want to escape '_' and '%' in a LIKE string value always use the backslash ('\').
 * </p>
 * <p>
 * <b><h4><u><A NAME="SQL keywords">SQL keywords</A></u></h4></b>
 * <br/> 
 * SQL keywords can be used either in upper or lower case (ie where or WHERE).
 * Currently the following SQL keywords (and functions) are supported (and should thus be considered reserved words, both upper and lower case):
 * <DIR>
 *  <li>SELECT</li>
 *  <li>DISTINCT</li>
 *  <li>FROM</li>
 *  <li>WHERE</li>
 *  <li>AND</li>
 *  <li>OR</li>
 *  <li>BETWEEN</li>
 *  <li>AS</li>
 *  <li>IN</li>
 *  <li>HAVING</li>
 *  <li>ORDER</li>
 *  <li>GROUP</li>
 *  <li>FOR</li>
 *  <li>ALL</li>
 *  <li>BY</li>
 *  <li>ASC</li>
 *  <li>DESC</li>
 *  <li>EXISTS</li>
 *  <li>ANY</li>
 *  <li>ALL</li>
 *  <li>SUM</li>
 *  <li>MIN</li>
 *  <li>MAX</li>
 *  <li>AVG</li>
 *  <li>COUNT</li>
 *  <li>LIKE</li>
 *  <li>CONCAT</li>
 *  <li>NOT</li>
 *  <li>SOME</li>
 *  <li>UNION</li>
 *  <li>EXCEPT</li>
 *  <li>MOD</li>
 *  <li>INTERSECT</li>
 *  <li>SUBSTRING</li>
 * </DIR>
 * Furthermore in one of the upcomming releases the following SQL functions will alse be supported (and should thus be considered reserved words, both upper and lower case):
 * <DIR>
 *  <li>SUBSTRING</li>
 *  <li>CONCAT</li>
 *  <li>LTRIM</li>
 *  <li>RTRIM</li>
 *  <li>REPLACE</li>
 *
 *  <li>CEILING</li>
 *  <li>FLOOR</li>
 *  <li>ROUND</li>
 *  <li>TRUNCATE</li>
 *
 *  <li>COALESCE</li>
 *
 *  <li>YEAR</li>
 *  <li>QUARTER</li>
 *  <li>MONTH</li>
 *  <li>WEEK</li>
 *  <li>HOUR</li>
 *  <li>MINUTE</li>
 *  <li>SECOND</li>
 *  <li>DAYOFWEEK</li>
 *  <li>DAYOFYEAR</li>
 * </DIR>
 * <b><u>Note:</u></b> On request other keywords and functions can be added, but since it is our intent to support a broad range of 
 * databases from different vendors we will tend to honour only broadly supported SQL keywords and functions. 
 * <p>
 * <b><h4><u><A NAME="Alternative notation">Alternative notation</A></u></h4></b>
 * <p> 
 * The following table shows wich java operators can be used as an alternative for SQL operators.
 * <p>
 * <table width="100%" cellspacing="2" cellpadding="4" border="0" style="border: 1px #666666 solid;">
 * 
 * <tr bgcolor="#333399">
 * <th class="tabletext" style="color:#ffffff;" align="left" valign="top" width="200">Description	</th>  
 * <th class="tabletext" style="color: #ffffff" align="left" valign="top" width="200">Java operator	</th>
 * <th class="tabletext" style="color: #ffffff" align="left" valign="top" width="200">SQL operator</th></tr>
 * 
 * <tr bgcolor="#aaaaff"><td class="tabletext" valign="top">Equals</td>
 * <td class="tabletext" valign="top">==</td>
 * <td class="tabletext" valign="top">=</td></tr>
 * 
 * <tr bgcolor="#aaaaff"><td class="tabletext" valign="top">Not equal</td>
 * <td class="tabletext" valign="top">!=</td>
 * <td class="tabletext" valign="top">&lt;&gt;</td></tr>
 * 
 * <tr bgcolor="#aaaaff"><td class="tabletext" valign="top">Boolean logical AND	</td>
 * <td class="tabletext" valign="top">&</td>
 * <td class="tabletext" valign="top">AND</td></tr>
 * 
 * <tr bgcolor="#aaaaff"><td class="tabletext" valign="top">Conditional AND		</td>
 * <td class="tabletext" valign="top">&&</td>
 * <td class="tabletext" valign="top">AND</td></tr>
 * 
 * <tr bgcolor="#aaaaff"><td class="tabletext" valign="top">Boolean logical OR	</td>
 * <td class="tabletext" valign="top">|</td>
 * <td class="tabletext" valign="top">OR</td></tr>
 * 
 * <tr bgcolor="#aaaaff"><td class="tabletext" valign="top">Conditional OR		</td>
 * <td class="tabletext" valign="top">||</td>
 * <td class="tabletext" valign="top">OR</td></tr>
 * 
 * <tr bgcolor="#aaaaff"><td class="tabletext" valign="top">Logical complement	</td>
 * <td class="tabletext" valign="top">!</td>
 * <td class="tabletext" valign="top">NOT</td></tr>
 * 
 * </table>
 * <p>
 * Furthermore it is possible to use:
 * <ul>
 * <li><code>startsWith(String beginString)</code> and </li>
 * <li><code>endsWith(String endString)</code></li>
 * </ul>
 * as an alternative for respectively
 * <ul>
 * <li> <code>like 'beginString%'</code>and </li>
 * <li> <code>like '%endString'</code></li>
 * </ul>
 * For example:<br/>
 * <blockquote><code>" cd.contains(artist.startsWith('U-roy') && artist.endsWith('U-roy') "</code></blockquote>
 * <p>
 * <p>
 * <b><h3><A NAME="Ordering">Ordering</A></h3></b>
 * <p>
 * The ordering string is internaly translated in an SQL ORDER BY clause. 
 * Any SQL92 entry level statement that can be part of the ORDER BY clause can be used in the ordering string.
 * Beyond this:
 * <ul>
 * <li>instead of ASC one can also use asc or ascending</li>
 * <li>instead of DESC one can also use desc or descending</li>
 * <li>one can navigate to Depending object using simple dot notation</li>
 * <li>one can navigate trough related BusinessObjects using simple dot notation permitted that they are <b>"Many to one"</b> or <b>"One to one"</b></li>
 * </ul> 
 * <p>
 * Exampels:
 * <ul>
 *  <li>"name "</li>
 *  <li>"name ascending "</li>
 *  <li>"name asc "</li>
 *  <li>"Store.owner.name , Store.name "</li>
 *  <li>"CD.artist.style.name descending "</li>
 * </ul>
 * <p>
 * 
 * @author Huub Cleutjens
 */
public interface Query extends SuperQuery{

   /**
    * Sets the filter for this Query. <br/>
    * If there is aleady a filter set, this will be replaced by the filter supplied as an argument.
    *
    * @param filter a String defining a valid <A href="#Filter">filter</A>.
    */
   public void setFilter(String filter);
   
   /**
    * Sets the ordering for this query. <br/>
    * If the ordering is allready set, this will be replaced by the ordering supplied as an argument.
    *
    * @param order a String defining a valid <A href="#Ordering">ordering</A>.
    */
   public void setOrdering(String order);

}


