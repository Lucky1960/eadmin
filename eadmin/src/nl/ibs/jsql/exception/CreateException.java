package nl.ibs.jsql.exception;



public class CreateException extends java.lang.Exception  implements JSQLException, nl.ibs.vegas.persistence.exception.CreateException{
	private static final long serialVersionUID = 1L;
	
    public CreateException() {
    }

    public CreateException(String msg) {
        super(msg);
    }
} 

