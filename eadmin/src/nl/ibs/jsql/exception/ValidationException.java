package nl.ibs.jsql.exception;



public class ValidationException extends java.lang.Exception  implements JSQLException, nl.ibs.vegas.persistence.exception.ValidationException{
	private static final long serialVersionUID = 1L;
	
    public ValidationException() {
    }

    public ValidationException(String msg) {
        super(msg);
    }
}

