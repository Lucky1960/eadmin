package nl.ibs.jsql.exception;



public class DeletedException extends java.lang.Exception  implements JSQLException, nl.ibs.vegas.persistence.exception.DeletedException {
	private static final long serialVersionUID = 1L;
	
    public DeletedException() {
    }

    public DeletedException(String msg) {
        super(msg);
    }
}

