package nl.ibs.jsql.exception;



import nl.ibs.jsql.*;


public class UpdateException extends java.lang.Exception  implements JSQLException, nl.ibs.vegas.persistence.exception.UpdateException{
	private static final long serialVersionUID = 1L;
    BusinessObject object;
    
    public UpdateException() {
    }

    public UpdateException(String msg, BusinessObject object) {
        super(msg);
        this.object = object;
    }
} 

