package nl.ibs.jsql.exception;



public class RemoveException extends java.lang.Exception  implements JSQLException, nl.ibs.vegas.persistence.exception.RemoveException{
	private static final long serialVersionUID = 1L;
	
    public RemoveException() {
    }

    public RemoveException(String msg) {
        super(msg);
    }
} 

