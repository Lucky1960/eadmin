package nl.ibs.jsql.exception;



public class InvalidStateException extends java.lang.Exception  implements JSQLException, nl.ibs.vegas.persistence.exception.InvalidStateException {
	private static final long serialVersionUID = 1L;
	
    public static final int UNDEFINED_STATE = 0;
    public static final int DELETED = -1;
    protected int state = 0;
    public InvalidStateException() {
    }

    public InvalidStateException(String msg) {
        super(msg);
    }

    public InvalidStateException(int state, String msg) {
        super(msg);
        setState(state);
    }

    public void setState(int state){
        this.state = state;
    }

    public int getState(){
        return state;
    }

    
}

