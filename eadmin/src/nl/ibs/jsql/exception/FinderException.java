package nl.ibs.jsql.exception;



public class FinderException extends java.lang.Exception  implements JSQLException, nl.ibs.vegas.persistence.exception.FinderException {
	private static final long serialVersionUID = 1L;
	
    public FinderException() {
    }

    public FinderException(String msg) {
        super(msg);
    }
}

