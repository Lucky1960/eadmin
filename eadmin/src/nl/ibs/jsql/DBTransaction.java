package nl.ibs.jsql;


import nl.ibs.jeelog.*;
import nl.ibs.jsql.sql.*;


import java.util.*;
import java.sql.*;

public class DBTransaction {

	

	public static final int TRANSACTION_READ_UNCOMMITTED = Connection.TRANSACTION_READ_UNCOMMITTED;
	public static final int TRANSACTION_READ_COMMITTED = Connection.TRANSACTION_READ_COMMITTED;
	public static final int TRANSACTION_REPEATABLE_READ = Connection.TRANSACTION_REPEATABLE_READ;
	public static final int TRANSACTION_SERIALIZABLE = Connection.TRANSACTION_SERIALIZABLE;
	public static final int TRANSACTION_NONE = Connection.TRANSACTION_NONE;
	public static final boolean defaultUseQueryResultCache = DBConfig.getCacheQueryResults();

	private int isolationlevel;
	private HashMap applicationTransactions;
	private boolean useQueryResultCache = defaultUseQueryResultCache;
	private boolean clearCachesOnTransActionStart = true;
	private boolean fullClearResultCachesOnUpdate = true;
	private Cache transactionCache;
	private boolean active;
	
	/**
	 * User query result cache. If true the query results will be cached. The value 
	 * supplied in jsql_config.propertes for property 'cache_query_results' is the
	 * default.
	 * @param cache
	 */ 
	public void useQueryResultCache(boolean cache){
		if (cache != useQueryResultCache && !applicationTransactions.isEmpty()) {
			for (Iterator atomicTransactions = applicationTransactions.values().iterator(); atomicTransactions.hasNext();) {
				for (Iterator it = ((HashMap) atomicTransactions.next()).values().iterator(); it.hasNext();) {
					AtomicDBTransaction at = (AtomicDBTransaction) it.next();
					if (cache = false)
						at.clearQueryResultCache();
					at.useQueryResultCache(cache);
				}
			}
		}
		useQueryResultCache = cache;
	}
	
	/**
	 * Clear transaction caches on start. Default the caches of this 
	 * transaction will on every invocation of begin.
	 * @param cache
	 */
	public void clearTransactionCachesOnTransactionStart(boolean clearCaches){
		clearCachesOnTransActionStart = clearCaches;
	}

	/**
	 * Clear result cache on every update. Default the caches of this 
	 * result cache will be cleared on every update. If false is provided 
	 * only result of queries that contain the name of the table updated will
	 * be cleared. This is much more expensive. But when you have many queries repeated
	 * and some unrelated updates, invoking this method with false, might 
	 * improve performance.  
	 *
	 * @param fullClearCaches 
	 */	
	public void fullClearResultCacheOnUpdates(boolean fullClearCaches) {
		if (fullClearResultCachesOnUpdate != fullClearCaches && !applicationTransactions.isEmpty()) {
			for (Iterator atomicTransactions = applicationTransactions.values().iterator(); atomicTransactions.hasNext();) {
				for (Iterator it = ((HashMap) atomicTransactions.next()).values().iterator(); it.hasNext();) {
					AtomicDBTransaction at = (AtomicDBTransaction) it.next();
					at.fullClearResultCacheOnUpdates(fullClearCaches);
				}
			}
		}
		fullClearResultCachesOnUpdate = fullClearCaches;
	}
	 
	
	public DBTransaction() {
		applicationTransactions = new HashMap(1);
		if (DBConfig.CACHING_TRANSACTION.equals(DBConfig.getCaching()))
			transactionCache = new Cache();
		isolationlevel = TRANSACTION_READ_COMMITTED; //Default isolationlevel
	}

	public boolean isActive() {
		return active;
	}

	public ConnectionProvider getConnectionProvider(DBData dbdata) throws Exception {
		HashMap atomicTransactions = (HashMap)applicationTransactions.get(dbdata.getApplication());
		if (atomicTransactions == null){
			atomicTransactions = new HashMap(1);
			applicationTransactions.put(dbdata.getApplication(),atomicTransactions);
		}
		AtomicDBTransaction at = (AtomicDBTransaction) atomicTransactions.get(dbdata);
		if (at == null) {
			at = new AtomicDBTransaction(dbdata);
			at.setTransActionIsolationLevel(isolationlevel);
			at.useQueryResultCache(useQueryResultCache);
			at.fullClearResultCacheOnUpdates(fullClearResultCachesOnUpdate);
			if (active)
				at.begin();
			atomicTransactions.put(dbdata, at);
		}
		return at;
	}

	public boolean readUncommitted() {
		return isolationlevel <= TRANSACTION_READ_UNCOMMITTED;
	}

	public void setTransActionIsolationLevel(int level) throws Exception {
		if (active)
			throw new Exception("msg-tried-to-change-transaction-isoloation-level-on-started-transction");
		isolationlevel = level;
	}

	public int getTransActionIsolationLevel() {
		return isolationlevel;
	}

	public void begin() throws Exception {

		DBTransaction activeTransaction = DBPersistenceManager.getTransaction();
		if (activeTransaction == null)
			DBPersistenceManager.setTransaction(this);
		else if (activeTransaction != this)
			throw new Exception("Other transaction already set on this thread");	
		
		if (clearCachesOnTransActionStart){
			if (transactionCache != null)
				transactionCache.clear();
			if (useQueryResultCache){	
				for (Iterator atomicTransactions = applicationTransactions.values().iterator(); atomicTransactions.hasNext();) {
					for (Iterator it = ((HashMap) atomicTransactions.next()).values().iterator(); it.hasNext();) {
						AtomicDBTransaction at = (AtomicDBTransaction) it.next();
						at.clearQueryResultCache();
					}	
				}
			}	
		}	

		if (!active) {
			try {
				for (Iterator atomicTransactions = applicationTransactions.values().iterator(); atomicTransactions.hasNext();) {
					for (Iterator it = ((HashMap) atomicTransactions.next()).values().iterator(); it.hasNext();) {
						AtomicDBTransaction at = (AtomicDBTransaction) it.next();
						at.useQueryResultCache(useQueryResultCache);
						at.fullClearResultCacheOnUpdates(fullClearResultCachesOnUpdate);
						at.begin();
					}	
				}
				active=true;
			} catch (Exception e) {
				for (Iterator atomicTransactions = applicationTransactions.values().iterator(); atomicTransactions.hasNext();) {
					for (Iterator it = ((HashMap) atomicTransactions.next()).values().iterator(); it.hasNext();) {
						AtomicDBTransaction at = (AtomicDBTransaction) it.next();
						at.clearQueryResultCache();
						if (at.isActive())
							at.forceClose();
					}
				}
				active=false;
				throw e;
			}
		} else {
			throw new Exception("msg-tried-to-begin-a-transaction-that-was-allready-active");
		}

	}

	public void rollback() throws Exception {
		rollback(true);
	}

	public void rollback(boolean includeBOFields) throws Exception {
		for (Iterator atomicTransactions = applicationTransactions.values().iterator(); atomicTransactions.hasNext();) {
			for (Iterator it = ((HashMap) atomicTransactions.next()).values().iterator(); it.hasNext();) {
				AtomicDBTransaction at = (AtomicDBTransaction) it.next();
				at.rollback(includeBOFields);
			}	
		}
		DBPersistenceManager.setTransaction(null);
		active=false;
	}

	public void commit() throws Exception {
		for (Iterator atomicTransactions = applicationTransactions.values().iterator(); atomicTransactions.hasNext();) {
			for (Iterator it = ((HashMap) atomicTransactions.next()).values().iterator(); it.hasNext();) {
				AtomicDBTransaction at = (AtomicDBTransaction) it.next();
				at.commit();
			}	
		}
		DBPersistenceManager.setTransaction(null);
		active=false;
	}

	public void forceClose() {
		Log.warn("msg-only-call-forse-close-if-a-call-to-rollback-has-thrown-an-exception-as-last-resort");
		for (Iterator atomicTransactions = applicationTransactions.values().iterator(); atomicTransactions.hasNext();) {
			for (Iterator it = ((HashMap) atomicTransactions.next()).values().iterator(); it.hasNext();) {
				AtomicDBTransaction at = (AtomicDBTransaction) it.next();
				at.forceClose();
			}	
		}
		DBPersistenceManager.setTransaction(null);
		active=false;
	}

	public Cache getCache() {
		return active?transactionCache:null;
	}

	protected void finalize() throws Throwable {
		applicationTransactions.clear();
	}

}

