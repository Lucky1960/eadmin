package nl.ibs.jsql;


import java.io.*;
import org.w3c.dom.*;
import nl.ibs.jeelog.*;

//Replace these imports with your own imports or add your own imports
//{CODE-INSERT-BEGIN:1926037870}import javax.xml.parsers.*;
import org.w3c.dom.*;
//{CODE-INSERT-END:1926037870}





public class DBConfig{
    public static final String CACHING_TRANSACTION = "transaction";
    public static final String CACHING_OFF = "off";
    public static final String CACHING_APPLICATION = "application";
    
    static {
       Log.info("Caching               : " + getCaching());
       Log.info("CacheQueryTranslations: " + getCacheQueryTranslations());
       Log.info("CacheQueryResults     : " + getCacheQueryResults());
       Log.info("CacheObjectRelations  : " + getCacheObjectRelations());
       Log.info("PrintCacheStatistics  : " + getPrintCacheStatistics());
       Log.info("DeferUpdates          : " + getDeferUpdates());
       Log.info("CustomMappings        : " + getCustomMappings());
       /*Log.info("AutoCreateTables      : " + getAutoAddColumns());
       Log.info("AutoAddColumns        : " + getAutoAddColumns());
       Log.info("AutoModifyColumns     : " + getAutoModifyColumns());
       Log.info("AutoDropColumns       : " + getAutoDropColumns());
       Log.info("DBManager             : " + getDBManager());
       Log.info("JDBCDriver            : " + getJDBCDriver());
       Log.info("URL                   : " + getURL());
       Log.info("Schema                : " + getSchema());
       Log.info("User                  : " + getUser());
       Log.info("Password              : XXXXX");*/
    }

    public static boolean getAutoCreateTables (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:1737339272}
	 return DBConfigProperties.getInstance().getAutoCreateTables(application);
//{CODE-INSERT-END:1737339272}
  }
    public static boolean getAutoAddColumns (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:-851339069}
	 return DBConfigProperties.getInstance().getAutoAddColumns(application);
//{CODE-INSERT-END:-851339069}
  }
    public static boolean getAutoModifyColumns (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:1755127708}
	 return DBConfigProperties.getInstance().getAutoModifyColumns(application);
//{CODE-INSERT-END:1755127708}
  }
    public static boolean getAutoDropColumns (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:-2076535289}
	 return DBConfigProperties.getInstance().getAutoDropColumns(application);
//{CODE-INSERT-END:-2076535289}
  }
    public static String getCaching () {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:-1511951097}
	 return DBConfigProperties.getInstance().getCaching();
//{CODE-INSERT-END:-1511951097}
  }
    public static boolean getCacheQueryTranslations () {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:920252192}
	 return DBConfigProperties.getInstance().cacheQueryTranslations();
//{CODE-INSERT-END:920252192}
  }
    public static boolean getCacheQueryResults () {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:-915463688}
	 return DBConfigProperties.getInstance().cacheQueryResults();
//{CODE-INSERT-END:-915463688}
  }
    public static boolean getCacheObjectRelations () {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:-254566066}
	 return DBConfigProperties.getInstance().cacheObjectRelations();
//{CODE-INSERT-END:-254566066}
  }
    public static boolean getPrintCacheStatistics () {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:-10826864}
	 return DBConfigProperties.getInstance().printCacheStatistics();
//{CODE-INSERT-END:-10826864}
  }
    public static boolean getDeferUpdates () {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:-1635877712}
	 return DBConfigProperties.getInstance().deferUpdates();
//{CODE-INSERT-END:-1635877712}
  }
    public static String getJDBCDriver (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:1371374747}
	 return DBConfigProperties.getInstance().getJDBCDriver(application);
//{CODE-INSERT-END:1371374747}
  }
    public static String getURL (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:-76273993}
	 return DBConfigProperties.getInstance().getURL(application);
//{CODE-INSERT-END:-76273993}
  }
    public static String getSchema (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:-281449639}
	 return DBConfigProperties.getInstance().getSchema(application);
//{CODE-INSERT-END:-281449639}
  }
    public static String getUser (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:1930506115}
	 return DBConfigProperties.getInstance().getUser(application);
//{CODE-INSERT-END:1930506115}
  }
    public static String getPassword (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:2024497747}
	 return DBConfigProperties.getInstance().getPassword(application);
//{CODE-INSERT-END:2024497747}
  }
    public static String getDBManager (String application) {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:467526839}
	 return DBConfigProperties.getInstance().getDBManager(application);
//{CODE-INSERT-END:467526839}
  }
    public static org.w3c.dom.DOMImplementation getDOMImplementation () {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:814556380}
	 try{return javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder().getDOMImplementation();}catch(Exception e){e.printStackTrace();return null;}
//{CODE-INSERT-END:814556380}
  }
    public static org.w3c.dom.Document getDocument (File file) throws Exception {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:1669232851}
	 return javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
//{CODE-INSERT-END:1669232851}
  }
    public static String getCustomMappings () {
      //Change the next lines if necessary 
//{CODE-INSERT-BEGIN:1829039534}
	 return "";
//{CODE-INSERT-END:1829039534}
  }
}
