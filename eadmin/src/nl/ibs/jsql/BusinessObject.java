package nl.ibs.jsql;



import java.io.Serializable;
import java.util.*;

public interface BusinessObject extends nl.ibs.vegas.persistence.BusinessObject, Serializable{
 
    // public MetaData getMetaData();
    
    public void delete() throws Exception;
	public void preSet() throws Exception;
	public void assureStorage() throws Exception;
	public Object getPrimaryKey () throws Exception;
	public int getDBId();
	public DBData getDBData();
	public void refreshData() throws Exception;
    
}

