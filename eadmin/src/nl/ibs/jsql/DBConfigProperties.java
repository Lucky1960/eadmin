package nl.ibs.jsql;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Properties;
import nl.ibs.jeelog.*;



public class DBConfigProperties {
        
    

    public static final String _FILE = "jsql_config.properties";
    public static final String _AUTO_CREATE_TABLES = "auto_create_tables";
    public static final String _AUTO_ADD_COLUMNS = "auto_add_columns";
    public static final String _AUTO_MODIFY_COLUMNS = "auto_modify_columns";
    public static final String _AUTO_DROP_COLUMNS = "auto_drop_columns";
    public static final String _CACHING = "caching";
    public static final String _CACHE_QUERY_TRANSLATIONS = "cache_query_translations";
    public static final String _CACHE_QUERY_RESULTS = "cache_query_results";
    public static final String _CACHE_OBJECT_RELATIONS = "cache_object_relations";
    public static final String _PRINT_CACHE_STATISTICS = "print_cache_statistics";
    public static final String _DEFER_UPDATES = "defer_updates";
    public static final String _DB_MANAGER = "db_manager";
    public static final String _JDBC = "db_jdbcDriver";
    public static final String _URLDB = "db_url";
    public static final String _DB = "db_schema";
    public static final String _USERDB = "db_user";
    public static final String _PASSWRDDB = "db_password";
    private static DBConfigProperties dbConfigProperties;
    private static long lastTime = 0;
    
    private Properties properties;
    private File file;
    
    private String caching;
    private boolean cacheQueryTranslations;
    private boolean cacheQueryResults;
    private boolean cacheObjectRelations;
    private boolean printCacheStatistics;
    private boolean deferUpdates;
    
    /** Creates a new instance of DBProperties */
	private DBConfigProperties(File file) {
		this.file = file;
		properties = new Properties();
		try {
			properties.load(new FileInputStream(file));

			caching = properties.getProperty(_CACHING, DBConfig.CACHING_TRANSACTION);
			cacheQueryTranslations = properties.getProperty(_CACHE_QUERY_TRANSLATIONS, "false").trim().equalsIgnoreCase("true");
			cacheQueryResults = properties.getProperty(_CACHE_QUERY_RESULTS, "false").trim().equalsIgnoreCase("true");
			cacheObjectRelations = properties.getProperty(_CACHE_OBJECT_RELATIONS, "false").trim().equalsIgnoreCase("true");
			printCacheStatistics = properties.getProperty(_PRINT_CACHE_STATISTICS, "false").trim().equalsIgnoreCase("true");
			deferUpdates = properties.getProperty(_DEFER_UPDATES, "false").trim().equalsIgnoreCase("true");
			Log.info("Found " + _FILE + "! in " + file.getPath());
		} catch (Exception e) {
			Log.warn("Coundn't find " + _FILE + ", use default: HSQL in memory!");
		}
	}

	public static DBConfigProperties getInstance() {
		File file = null;
		try {
			URL url = DBConfigProperties.class.getClassLoader().getResource(_FILE);
			if (url != null) {
				file = new File(URLDecoder.decode(url.getFile(), System.getProperty("file.encoding")));
				long time = file.lastModified();
				if (lastTime != time) {
					dbConfigProperties = null;
					lastTime = time;
				}
			}
		} catch (UnsupportedEncodingException e) {
		}

		if (dbConfigProperties == null)
			dbConfigProperties = new DBConfigProperties(file);
		return dbConfigProperties;
	}

    public boolean getAutoCreateTables(String application) {
        return getProperty(application,_AUTO_CREATE_TABLES, "true").trim().equalsIgnoreCase("true");
    }

    public boolean getAutoAddColumns(String application) {
        return getProperty(application,_AUTO_ADD_COLUMNS, "true").trim().equalsIgnoreCase("true");
    }

    public boolean getAutoModifyColumns(String application) {
        return getProperty(application,_AUTO_MODIFY_COLUMNS, "false").trim().equalsIgnoreCase("true");
    }

    public boolean getAutoDropColumns(String application) {
        return getProperty(application,_AUTO_DROP_COLUMNS, "false").trim().equalsIgnoreCase("true");
    }
    
    public String getCaching() {
        return caching;
    }

 	public boolean cacheQueryTranslations() {
        return cacheQueryTranslations;
    }    


 	public boolean cacheQueryResults() {
        return cacheQueryResults;
    }    

 	public boolean cacheObjectRelations() {
        return cacheObjectRelations;
    }    

 	public boolean printCacheStatistics() {
        return printCacheStatistics;
    }
    
    public boolean deferUpdates(){
    	return deferUpdates;
    }    
    
    public String getDBManager(String application) {
        return getProperty(application,_DB_MANAGER, "nl.ibs.jsql.sql.DBManager");
    }
    
    public String getJDBCDriver(String application) {
        return getProperty(application,_JDBC, "org.hsqldb.jdbcDriver");
    }
    public String getURL(String application) {
        return getProperty(application,_URLDB,"jdbc:hsqldb:");
    }
    public String getSchema(String application) {
        String schema = getProperty(application,_DB, "");
        if (schema.length() == 0) { //LVL
        	String url = getProperty(application,_URLDB, "");
        	int ix = url.lastIndexOf("/");
        	if (ix >= 0) {
        		schema = url.substring(ix + 1);
        	}
        }
    	return schema;
    }
    public String getUser(String application) {
        return getProperty(application,_USERDB, "sa");
    }
    public String getPassword(String application) {
        return getProperty(application,_PASSWRDDB, "");
    }
    
    public String getProperty(String application, String property, String defaultValue){
    	 return properties.getProperty(application + "_" +property, defaultValue);
    }
    
    public void setProperty(String application, String property, String value) throws Exception{
   	 	properties.put(application + "_" +property, value);
   	 	properties.store(new FileOutputStream(file),"");
    }
    
}

