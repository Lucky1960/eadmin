package nl.ibs.jsql;


import java.sql.*;
import java.util.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.impl.*;



/**
 * Factory to create Querys. 
 *
 * @author Huub Cleutjens
 */
public class QueryFactory {
   
   /**
    * Return new Query
    *
    * @return Query
    */
   public static Query create(){
     return new QueryImplementation(null, null, 0);
   }

   /**
    * Return new Query with given filter set.
    *
    * @param filter filter to apply
    * @return Query
    */
   public static Query create(String filter){
     return new QueryImplementation(filter, null, 0);
   }

   /**
    * Return new Query with given filter and ordering set.
    *
    * @param filter filter to apply
    * @param ordering ordering to apply
    * @return Query
    */
   public static Query create(String filter, String ordering){
     return new QueryImplementation(filter, ordering, 0);
   }

   /**
    * Return new Query with given filter, ordering and maximum of objects to return set.
    *
    * @param filter filter to apply
    * @param ordering ordering to apply
    * @param maxObjects maximum of Objects to return
    * @return Query
    */
   public static Query create(String filter, String ordering, int maxObjects){
     return new QueryImplementation(filter, ordering, maxObjects);
   }

   /**
    * Return new ExecutableQuery
    *
    * @param manager BusinessObjectManager of the BusinessObjects to return 
    * @return Query
    */
   public static ExecutableQuery create(BusinessObjectManager manager){
     return new ExecutableQueryImplementation(manager,null, null, 0);
   }

   /**
    * Return new ExecutableQuery
    *
    * @param manager BusinessObjectManager of the BusinessObjects to return 
    * @param filter filter to apply
    * @return Query
    */
   public static ExecutableQuery create(BusinessObjectManager manager, String filter){
     return new ExecutableQueryImplementation(manager,filter, null, 0);
   }

   /**
    * Return new ExecutableQuery
    *
    * @param manager BusinessObjectManager of the BusinessObjects to return 
    * @param filter filter to apply
    * @param ordering ordering to apply
    * @return Query
    */
   public static ExecutableQuery create(BusinessObjectManager manager, String filter, String ordering){
     return new ExecutableQueryImplementation(manager,filter, ordering, 0);
   }

   /**
    * Return new ExecutableQuery
    *
    * @param manager BusinessObjectManager of the BusinessObjects to return 
    * @param filter filter to apply
    * @param ordering ordering to apply
    * @param maxObjects maximum of Objects to return
    * @return Query
    */
   public static ExecutableQuery create(BusinessObjectManager manager, String filter, String ordering, int maxRows){
     return new ExecutableQueryImplementation(manager,filter, ordering, maxRows);
   }

   /**
    * Return new ExecutableQuery
    *
    * @param boClass BusinessObject to return 
    * @return Query
    */
   public static ExecutableQuery create(Class boClass) throws Exception{
     return new ExecutableQueryImplementation(boClass,null, null, 0);
   }

   /**
    * Return new ExecutableQuery
    *
    * @param boClass BusinessObject to return 
    * @param filter filter to apply
    * @return Query
    */
   public static ExecutableQuery create(Class boClass, String filter) throws Exception{
     return new ExecutableQueryImplementation(boClass,filter, null, 0);
   }

   /**
    * Return new ExecutableQuery
    *
    * @param boClass BusinessObject to return 
    * @param filter filter to apply
    * @param ordering ordering to apply
    * @return Query
    */
   public static ExecutableQuery create(Class boClass, String filter, String ordering) throws Exception{
     return new ExecutableQueryImplementation(boClass, filter, ordering, 0);
   }

   /**
    * Return new ExecutableQuery
    *
    * @param boClass BusinessObject to return 
    * @param filter filter to apply
    * @param ordering ordering to apply
    * @param maxObjects maximum of Objects to return
    * @return Query
    */
   public static ExecutableQuery create(Class boClass, String filter, String orderBy, int maxRows) throws Exception{
     return new ExecutableQueryImplementation(boClass, filter, orderBy, maxRows);
   }
   
   /**
    * Return new FreeQuery
	*
	* @param boClass BusinessObject to return 
	* 
	* @return FreeQuery
	*/
	public static FreeQuery createFreeQuery(Class boClass) throws Exception {
		return new ExecutableQueryImplementation(boClass, null, 0);
	}

   /**
	* Return new FreeQuery
	*
	* @param boClass BusinessObject to return 
	* @param maxObjects maximum of Objects to return
	*
	* @return FreeQuery
	*/
	public static FreeQuery createFreeQuery(Class boClass, int maxRows) throws Exception {
		return new ExecutableQueryImplementation(boClass, null, maxRows);
	}

   /**
	* Return new FreeQuery
	*
	* @param boClass BusinessObject to return 
	* @param query to execute
	*
	* @return FreeQuery
	*/
	public static FreeQuery createFreeQuery(Class boClass, String query) throws Exception {
		return new ExecutableQueryImplementation(boClass, query, 0);
	}

   /**
	* Return new FreeQuery
	*
	* @param boClass BusinessObject to return 
	* @param query to execute
	* @param maxObjects maximum of Objects to return
	*
	* @return FreeQuery
	*/
	public static FreeQuery createFreeQuery(Class boClass, String query, int maxRows) throws Exception {
		return new ExecutableQueryImplementation(boClass, query, maxRows);
	}

	/**
	 * Return new FreeQuery
	 *
	 * @param manager BusinessObjectManager of BusinessObject to return 
	 * 
	 * @return FreeQuery
	 */
	 public static FreeQuery createFreeQuery(BusinessObjectManager manager) throws Exception {
		 return new ExecutableQueryImplementation(manager, null,0);
	 }

	/**
	 * Return new FreeQuery
	 *
	 * @param manager BusinessObjectManager of BusinessObject to return 
	 * @param maxObjects maximum of Objects to return
	 *
	 * @return FreeQuery
	 */
	 public static FreeQuery createFreeQuery(BusinessObjectManager manager, int maxRows) throws Exception {
		 return new ExecutableQueryImplementation(manager, null, maxRows);
	 }

	/**
	 * Return new FreeQuery
	 *
	 * @param manager BusinessObjectManager of BusinessObject to return 
	 * @param query to execute
	 *
	 * @return FreeQuery
	 */
	 public static FreeQuery createFreeQuery(BusinessObjectManager manager, String query) throws Exception {
		 return new ExecutableQueryImplementation(manager, query, 0);
	 }

	/**
	 * Return new FreeQuery
	 *
	 * @param manager BusinessObjectManager of BusinessObject to return 
	 * @param query to execute
	 * @param maxObjects maximum of Objects to return
	 *
	 * @return FreeQuery
	 */
	 public static FreeQuery createFreeQuery(BusinessObjectManager manager, String query, int maxRows) throws Exception {
		 return new ExecutableQueryImplementation(manager, query, maxRows);
	 }

}

