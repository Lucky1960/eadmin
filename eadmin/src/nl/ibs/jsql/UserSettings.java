package nl.ibs.jsql;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import nl.ibs.jsql.sql.ConnectionProvider;
import nl.ibs.jsql.sql.DBConnection;
import nl.ibs.jsql.sql.DBConnectionPool;
import nl.ibs.jsql.sql.TableDefinition;
import nl.ibs.vegas.Setting;

public class UserSettings implements nl.ibs.vegas.SettingsStore {
	private final static long serialVersionUID = 1L;
	private final static short MAX_VALUE_LENGTH = 253;
	private final static String IGNORE = "--1";
	private final static String TABLE = "JSQLUSRST";
	private final static String USER = "US_USER";
	private final static String CONTEXT = "US_CONTEXT";
	private final static String FUNCTION = "US_FUNCTN";
	private final static String TYPE = "US_TYPE";
	private final static String KEY1 = "US_KEY1";
	private final static String KEY2 = "US_KEY2";
	private final static String KEY3 = "US_KEY3";
	private final static String VALUE = "US_VALUE";
	private final static String SEQUENCE_NUMBER = "US_SQNCNBR";
	private final static String USERTYPE = "US_USRTYPE";
	private final static String USEREDIT = "US_USREDIT";
	
	private final static ArrayList EMPTY_LIST = new ArrayList(0){
		private final static long serialVersionUID = 1L;
		public void add(int index, Object element) {}
		public boolean add(Object o) {return false;}
		public boolean addAll(Collection c) {return false;}
		public boolean addAll(int index, Collection c) {return false;}
	};
	
	private static HashSet checkedDBData = new HashSet(1);
	private static HashMap cache = new HashMap(1);
	private static HashMap usersSettings = new HashMap();
	
	private DBData dBData;
	private String[] users;
	private String context;
	private boolean ignoreContext = false;
	private IgnoreContextProvider ignoreContextProvider;
	private StringBuffer buffer = new StringBuffer();
	
	
	
	public UserSettings(String application, String[] users)throws Exception{
		this(DBData.getDefaultDBData(application),users,null);
	}
	
	public UserSettings(String application, String user)throws Exception{
		this(DBData.getDefaultDBData(application),user,null);
	}
	
	public UserSettings(String application, String[] users, String context)throws Exception{
		this(DBData.getDefaultDBData(application),users,context);
	}
	
	public UserSettings(String application, String user, String context)throws Exception{
		this(DBData.getDefaultDBData(application),user,context);
	}
	
	public UserSettings(DBData dbData,String[] users)throws Exception{
		this(dbData,users,null);
	}
	
	public UserSettings(DBData dbData,String user)throws Exception{
		this(dbData,user,null);
	}
	
	public UserSettings(DBData dbData,String user, String context) throws Exception{
		this(dbData,new String[]{user},context);
	}
	
	public UserSettings(DBData dbData,String[] users, String context) throws Exception{
		if(dbData == null)
			throw new IllegalArgumentException("Tried to instantiate UserSettingsManagerOld with DBData == null!");
		this.dBData = dbData;
		this.users = users;
		this.context = context;
		checkDB();
	}
	
	private void checkDB() throws Exception {
		if (!checkedDBData.contains(dBData)) {
			ConnectionProvider provider = getConnectionProvider();
			buffer.setLength(0);
			Statement stat = null;
			boolean exists = false;
			buffer.append("SELECT * FROM ");
			appendTableName(provider, buffer);
			DBConnection dbc = provider.getConnection();
			ResultSet rs = null;
			try {
				Connection con = dbc.getConnection();
				stat = con.createStatement();
				stat.setMaxRows(1);
				rs = stat.executeQuery(buffer.toString());
				rs.next();
				exists = true;
			} catch (java.sql.SQLException e) {
				exists = false;
			} finally {
				if (rs != null)
					try {
						rs.close();
					} catch (Exception e) {	
					}
				
				if (stat != null)
					try {
						stat.close();
					} catch (Exception e) {
					}
				provider.returnConnection(dbc);
			}
			if (!exists) {
				buffer.setLength(0);
				buffer.append("CREATE TABLE ");
				appendTableName(provider, buffer);
				buffer.append(" (");
				buffer.append(getFieldName(provider, "user", USER));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("String", 35, 0));
				buffer.append("	,");
				buffer.append(getFieldName(provider, "context", CONTEXT));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("String", 35, 0));
				buffer.append("	,");
				buffer.append(getFieldName(provider, "function", FUNCTION));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("String", 50, 0));
				buffer.append("	,");
				buffer.append(getFieldName(provider, "type", TYPE));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("String", 2, 0));
				buffer.append("	,");
				buffer.append(getFieldName(provider, "key1", KEY1));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("String", 35, 0));
				buffer.append("	,");
				buffer.append(getFieldName(provider, "key2", KEY2));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("String", 35, 0));
				buffer.append("	,");
				buffer.append(getFieldName(provider, "key3", KEY3));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("String", 35, 0));
				buffer.append("	,");
				buffer.append(getFieldName(provider, "value", VALUE));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("String", 255, 0));
				buffer.append("	,");
				buffer.append(getFieldName(provider, "sequenceNumber", SEQUENCE_NUMBER));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("int", 3, 0));
				buffer.append("	");
				buffer.append(")");
				dbc = provider.getConnection();
				try {
					dbc.executeUpdate(buffer.toString());
				} finally {
					provider.returnConnection(dbc);
				}
			}

			TableDefinition tableDefinition = (TableDefinition) TableDefinition.getTableDefinitions(TABLE, new String[] { "TABLE" }, DBConnectionPool.getInstance(this.dBData)).get(TABLE);
			HashMap columnDefinitions = tableDefinition.getColumnDefinitions();
			if (!columnDefinitions.containsKey(USERTYPE)) {
				buffer.setLength(0);
				buffer.append("ALTER TABLE ");
				appendTableName(provider, buffer);
				buffer.append(" ADD COLUMN ");
				buffer.append(getFieldName(provider, "userType", USERTYPE));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("String", 2, 0));
				buffer.append(" NOT NULL DEFAULT '"+USER_TYPE_U+"'");
				dbc = provider.getConnection();
				try {
					dbc.executeUpdate(buffer.toString());
				} catch (SQLException e) {
					//[SQL0612] US_USRTYPE is a duplicate column name
					if (e.getMessage().indexOf("SQL0612") == -1) {
						throw e;
					}
				} finally {
					provider.returnConnection(dbc);
				}
			}

			if (!columnDefinitions.containsKey(USEREDIT)) {
				buffer.setLength(0);
				buffer.append("ALTER TABLE ");
				appendTableName(provider, buffer);
				buffer.append(" ADD COLUMN ");
				buffer.append(getFieldName(provider, "userEdit", USEREDIT));
				buffer.append(" ");
				buffer.append(provider.getDBMapping().getSQLDefinition("boolean", 0, 0));
				buffer.append(" NOT NULL DEFAULT 'true'");
				dbc = provider.getConnection();
				try {
					dbc.executeUpdate(buffer.toString());
				} catch (SQLException e) {
					//[SQL0612] US_USREDIT is a duplicate column name
					if (e.getMessage().indexOf("SQL0612") == -1) {
						throw e;
					}
				} finally {
					provider.returnConnection(dbc);
				}
			}

			if (tableDefinition.getIndexDefinitions().isEmpty()) {
				dbc = provider.getConnection();
				buffer.setLength(0);
				buffer.append("CREATE UNIQUE INDEX ");
//LVL			buffer.append(provider.getPrefix());
				buffer.append("JSQL_UC_");
				buffer.append(TABLE);
				buffer.append(" ON ");
				appendTableName(provider, buffer);
				buffer.append("(US_USER, US_CONTEXT, US_FUNCTN, US_TYPE, US_KEY1, US_KEY2, US_KEY3, US_SQNCNBR)");
				try {
					dbc.executeUpdate(buffer.toString());
				} finally {
					provider.returnConnection(dbc);
				}
			}
			checkedDBData.add(dBData);
		}
	}

	public void setUsers(String[] users) {
		this.users = users;
	}

	public void setContext(String context) {
		this.context = context;
	}
	
	public String getContext() {
		return context;
	}
	
	public String getUser() {
		return users != null && users.length>0?users[0]:"";
	}
	
	public String[] getUsers() {
		return users;
	}
 
	
	public void remove() throws Exception {
		delete(IGNORE, IGNORE, IGNORE,IGNORE, IGNORE);
	}

	public void remove(String function) throws Exception {
		delete(function, IGNORE, IGNORE,IGNORE, IGNORE);
	}

	public void remove(String function, String type) throws Exception {
		delete(function, type, IGNORE,IGNORE, IGNORE);
	}

	public void remove(String function, String type, String key1) throws Exception {
		delete(function, type, key1, IGNORE, IGNORE);
	}

	public void remove(String function, String type, String key1, String key2) throws Exception {
		delete(function, type, key1,key2, IGNORE);
	}

	public void remove(String function, String type, String key1, String key2, String key3) throws Exception {
		delete(function, type, key1,key2, key3);
	}
	
	public int delete(String function, String type, String key1, String key2, String key3) throws Exception {
		return delete(function, type, key1,key2, key3,getIgnoreContext(function, type, key1,key2, key3));
	}

	public void remove(boolean ignoreContext) throws Exception {
		delete(IGNORE, IGNORE, IGNORE,IGNORE, IGNORE,ignoreContext);
	}

	public void remove(String function, boolean ignoreContext) throws Exception {
		delete(function, IGNORE, IGNORE,IGNORE, IGNORE,ignoreContext);
	}

	public void remove(String function, String type, boolean ignoreContext) throws Exception {
		delete(function, type, IGNORE,IGNORE, IGNORE,ignoreContext);
	}

	public void remove(String function, String type, String key1, boolean ignoreContext) throws Exception {
		delete(function, type, key1, IGNORE, IGNORE,ignoreContext);
	}

	public void remove(String function, String type, String key1, String key2, boolean ignoreContext) throws Exception {
		delete(function, type, key1,key2, IGNORE,ignoreContext);
	}

	public void remove(String function, String type, String key1, String key2, String key3, boolean ignoreContext) throws Exception {
		delete(function, type, key1,key2, key3,ignoreContext);
	}
	
	
	public int delete(String function, String type, String key1, String key2, String key3, boolean ignoreContext) throws Exception {
	      ConnectionProvider provider = getConnectionProvider();
	      DBConnection dbc = provider.getConnection();
	      buffer.setLength(0);
	      try{
	    	buffer.append("DELETE FROM ");
	    	appendTableName(provider, buffer);
			buffer.append(" WHERE "); 
			int startWhere = buffer.length();
	    	appendGeneralFilterClause(provider,buffer,users[0],startWhere,ignoreContext);
	    	appendWhere(buffer,startWhere,provider,"function",FUNCTION,function,true);
			appendWhere(buffer,startWhere,provider,"type",TYPE,type,true);
			appendWhere(buffer,startWhere,provider,"key1",KEY1,key1,true);
			appendWhere(buffer,startWhere,provider,"key2",KEY2,key2,true);
			appendWhere(buffer,startWhere,provider,"key3",KEY3,key3,true);
	        int result = dbc.executeUpdate(buffer.toString());
	        if(result >0)
	        	clearCache(users[0]);
	        return result;
	      }catch(java.sql.SQLException e){
	        throw e;
	      }finally {
	        provider.returnConnection(dbc);
	      }
	      
	 }
	
	public ConnectionProvider getConnectionProvider() throws Exception{
		return DBPersistenceManager.getConnectionProvider(dBData);
	}
	
	public void setValue(String value, String function, String type) throws Exception {
		setValue(value,function,type,IGNORE,IGNORE,IGNORE);
	}

	public void setValue(String value, String function, String type, String key1) throws Exception {
		setValue(value,function,type,key1,IGNORE,IGNORE);
	}

	public void setValue(String value, String function, String type, String key1, String key2) throws Exception {
		setValue(value,function,type,key1,key2,IGNORE);
	}

	public void setValue(String value, String function, String type, String key1, String key2, String key3) throws Exception {
		setValue(value,function,type,key1,key2,key3,getIgnoreContext(function,type,key1,key2,key3));
	}	
	
	
	protected boolean getIgnoreContext(String function, String type, String key1, String key2, String key3) {
		if (ignoreContextProvider != null)
			return ignoreContextProvider.getIgnoreContext(function, type, key1, key2, key3);
		return ignoreContext;
	}

	public void setValue(String value, String function, String type, boolean ignoreContext) throws Exception {
		setValue(value,function,type,IGNORE,IGNORE,IGNORE,ignoreContext);
	}

	public void setValue(String value, String function, String type, String key1, boolean ignoreContext) throws Exception {
		setValue(value,function,type,key1,IGNORE,IGNORE,ignoreContext);
	}

	public void setValue(String value, String function, String type, String key1, String key2, boolean ignoreContext) throws Exception {
		setValue(value,function,type,key1,key2,IGNORE,ignoreContext);
	}

	public void setValue(String value, String function, String type, String key1, String key2, String key3, boolean ignoreContext) throws Exception {
		remove(function, type,key1,key2,key3,ignoreContext);
		if (value == null)
			return; 
		String[] values = getStringArray(value);
		ConnectionProvider provider = getConnectionProvider();
	    DBConnection dbc = provider.getConnection();
	    //StringBuffer buffer = new StringBuffer(values.length*(MAX_VALUE_LENGTH+2));
	    buffer.setLength(0);
	      try{
	    	buffer.append("INSERT INTO ");
	    	appendTableName(provider, buffer);
			buffer.append(" VALUES ");
			for (int i = 0; i < values.length; i++) {
				buffer.append(i>0?",(":"(");
				appendValue(buffer,users[0],",");
		    	appendValue(buffer,ignoreContext?"":context,",");
		    	appendValue(buffer,function,",");
		    	appendValue(buffer,type,",");
		    	appendValue(buffer,key1,",");
		    	appendValue(buffer,key2,",");
		    	appendValue(buffer,key3,",");
		    	appendValue(buffer,values[i],",");
				appendValue(buffer, i, ",");
				appendValue(buffer, USER_TYPE_U, ",");
				appendValue(buffer, "true", ")");
			}
			dbc.executeUpdate(buffer.toString());
			clearCache(users[0]);
	      }catch(java.sql.SQLException e){
	        throw e;
	      }finally {
	        provider.returnConnection(dbc);
	      }
	}
	
	private static final void appendValue(StringBuffer buffer, String value, String separator){
		if (value == null || value == IGNORE){
			buffer.append("NULL");
		}else{
			buffer.append("'");
			escapeSingleQuotes(buffer,value);
			buffer.append("'");
		}
		if (separator != null)
			buffer.append(separator);
	}
	
	private static void escapeSingleQuotes(StringBuffer buffer,String string) {
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == '\'')
				buffer.append('\'');
			buffer.append(string.charAt(i));
		}
	}

	private static final void appendValue(StringBuffer buffer, int number, String separator){
		buffer.append(number);
		if (separator != null)
			buffer.append(separator);
	}

	public String getValue(String function, String type) throws Exception {
		return getValue(function, type, IGNORE, IGNORE, IGNORE);
	}

	public String getValue(String function, String type, String key1) throws Exception {
		return getValue(function, type, key1, IGNORE, IGNORE);
	}

	public String getValue(String function, String type, String key1, String key2) throws Exception {
		return getValue(function, type, key1, key2, IGNORE);
	}
	
	public String getValue(String function, String type, String key1, String key2, String key3) throws Exception {
		return getValue(function, type, key1, key2, key3,getIgnoreContext(function,type,key1,key2,key3));
	}
	
	
	public String getValue(String function, String type, boolean ignoreContext) throws Exception {
		return getValue(function, type, IGNORE, IGNORE, IGNORE,ignoreContext);
	}

	public String getValue(String function, String type, String key1, boolean ignoreContext) throws Exception {
		return getValue(function, type, key1, IGNORE, IGNORE,ignoreContext);
	}

	public String getValue(String function, String type, String key1, String key2, boolean ignoreContext) throws Exception {
		return getValue(function, type, key1, key2, IGNORE,ignoreContext);
	}
	
	public String getValue(String function, String type, String key1, String key2, String key3, boolean ignoreContext) throws Exception {
		if (users.length==1){
			return getValue(users[0],function, type, key1, key2, IGNORE,ignoreContext);	
		}else{
			String key = buildKey( function, type, key1, key2, key3,ignoreContext);
			for (int i = 0; i < users.length; i++) {
				String value = getCachedValue(users[i],key);
				if (value != null && value.length()>0)
					return value;
				value = selectValue(users[i],function, type, key1, key2, IGNORE,ignoreContext);	
				setCachedValue(users[i],key,value);
				if (value.length()>0)
					return value;
			}
		}
		return "";
	}
	
	private String selectValue(String user,String function, String type, String key1, String key2, String key3, boolean ignoreContext) throws Exception {
		ArrayList settings = getSettings(user);
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < settings.size(); i++) {
			Setting setting = (Setting)settings.get(i);
			if (!include(function,setting.getFunction()))
				continue;
			if (!include(type,setting.getType()))
				continue;
			if (!include(key1,setting.getKey1()))
				continue;
			if (!include(key2,setting.getKey2()))
				continue;
			if (!include(key3,setting.getKey3()))
				continue;
			if (!ignoreContext  && !include(context,setting.getContext()))
				continue;
			buffer.append(setting.getValue());
		}
		return buffer.toString();
	}

	
	private String getCachedValue(String user, String key) {
		return (String)readCache("values", user, key) ;
	}
	
	private ArrayList getCachedSettings(String user, String key) {
		return (ArrayList)readCache("settings", user, key) ;
	}
	
	private Object readCache(String cacheType, String user, String key) {
		HashMap _cache = (HashMap)cache.get(dBData);
		if (_cache != null){
			_cache = (HashMap)_cache.get(user);
			if (_cache != null){
				_cache = (HashMap)_cache.get(cacheType);
				if (_cache != null){
					return _cache.get(key);
				}
			}
		}
		return null;
	}
	
	private void setCachedValue(String user, String key, String value) {
		writeCache("values", user, key, value);
	}
	
	private void setCachedSettings(String user, String key, ArrayList settings) {
		writeCache("settings", user, key, settings);
	}
	
	private void writeCache(String cacheType, String user, String key, Object value) {
		HashMap dbDataCache = (HashMap)cache.get(dBData);
		if (dbDataCache == null){
			dbDataCache = new HashMap(2);
			cache.put(dBData,dbDataCache);
		}
		HashMap userCache = (HashMap)dbDataCache.get(user);
		if (userCache == null){
			userCache = new HashMap();
			dbDataCache.put(user,userCache);
		}
		HashMap typeCache = (HashMap)userCache.get(cacheType);
		if (typeCache == null){
			typeCache = new HashMap();
			userCache.put(cacheType,typeCache);
		}
		typeCache.put(key,value);
	}
	
	private void clearCache() {
		clearCache(users[0]);
	}
	private void clearCache(String user) {
		HashMap _cache = (HashMap)cache.get(dBData);
		if (_cache != null)
			_cache = (HashMap)_cache.get(user);
			if (_cache != null)
				_cache.clear();
		_cache = (HashMap)usersSettings.get(dBData);
		if (_cache != null)
			_cache.remove(user);	
	}
	
	private String buildKey(String function, String type, String key1, String key2, String key3, boolean ignoreContext) {
		buffer.setLength(0);
		if (!ignoreContext && context != null){
			buffer.append(context);
		}
		if (function != IGNORE && function != null){
			if (buffer.length()>0)
				buffer.append("|");
			buffer.append(function);
		}
		if (type != IGNORE && type != null){
			if (buffer.length()>0)
				buffer.append("|");
			buffer.append(type);
		}
		if (key1 != IGNORE && key1 != null){
			buffer.append("|");
			buffer.append(key1);
		}
		if (key2 != IGNORE && key2 != null){
			buffer.append("||");
			buffer.append(key2);
			
		}
		if (key3 != IGNORE && key3 != null){
			buffer.append("|||");
			buffer.append(key3);
		}
		return buffer.toString();
	}
	
	private String getValue(String user, String function, String type, String key1, String key2, String key3, boolean ignoreContext) throws Exception {
	      ConnectionProvider provider = getConnectionProvider();
	      DBConnection dbc = provider.getConnection();
	      Statement stat = null;
	      buffer.setLength(0);
	      ResultSet rs = null;
	      try{
	    	buffer.append("SELECT * FROM ");
	    	appendTableName(provider, buffer);
			buffer.append(" WHERE ");
			int startWhere = buffer.length();
	    	appendGeneralFilterClause(provider,buffer,user,startWhere,ignoreContext);
	    	appendWhere(buffer,startWhere,provider,"function",FUNCTION,function,true);
			appendWhere(buffer,startWhere,provider,"type",TYPE,type,true);
			appendWhere(buffer,startWhere,provider,"key1",KEY1,key1,true);
			appendWhere(buffer,startWhere,provider,"key2",KEY2,key2,true);
			appendWhere(buffer,startWhere,provider,"key3",KEY3,key3,true);
			appendOrderBy(buffer,provider);
			Connection con = dbc.getConnection();
			stat = con.createStatement();
			rs = stat.executeQuery(buffer.toString());
			buffer.setLength(0);
			while(rs.next()){
				String value = rs.getString(VALUE);
				if (value != null && value.length()>=2)
					buffer.append(value.substring(1,value.length()-1));
			}
			return buffer.toString();
	      }catch(java.sql.SQLException e){
	        throw e;
	      }finally {
	      	  if (rs != null)
					try {
						rs.close();
					} catch (Exception e) {	
					}
	    	  if (stat != null)
					try {
						stat.close();
					} catch (Exception e) {
					}
	        provider.returnConnection(dbc);
	      }
	 }

	private void appendOrderBy(StringBuffer buffer, ConnectionProvider provider) {
		buffer.append(" ORDER BY ");
		buffer.append(getFieldName(provider,"user",USER));
		buffer.append(", ");
		buffer.append(getFieldName(provider,"context",CONTEXT));
		buffer.append(", ");
		buffer.append(getFieldName(provider,"function",FUNCTION));
		buffer.append(", ");
		buffer.append(getFieldName(provider,"type",TYPE));
		buffer.append(", ");
		buffer.append(getFieldName(provider,"key1",KEY1));
		buffer.append(", ");
		buffer.append(getFieldName(provider,"key2",KEY2));
		buffer.append(", ");
		buffer.append(getFieldName(provider,"key3",KEY3));
		buffer.append(", ");
		buffer.append(getFieldName(provider,"sequenceNumber",SEQUENCE_NUMBER));
	}

	public Collection getSettings(String function, String type, boolean ignoreContext) throws Exception {
		return getSettings(function, type, IGNORE, IGNORE, IGNORE, ignoreContext);
	}

	public Collection getSettings(String function, String type, String key1, boolean ignoreContext) throws Exception {
		return getSettings(function, type, key1, IGNORE, IGNORE, ignoreContext);
	}

	public Collection getSettings(String function, String type, String key1, String key2, boolean ignoreContext) throws Exception {
		return getSettings(function, type, key1, key2, IGNORE, ignoreContext);
	}
	
	public Collection getSettings(String function, String type) throws Exception {
		return getSettings(function, type, IGNORE, IGNORE, IGNORE);
	}

	public Collection getSettings(String function, String type, String key1) throws Exception {
		return getSettings(function, type, key1, IGNORE, IGNORE);
	}

	public Collection getSettings(String function, String type, String key1, String key2) throws Exception {
		return getSettings(function, type, key1, key2, IGNORE);
	}
	
	public Collection getSettings(String function, String type, String key1, String key2, String key3) throws Exception {
		return getSettings(function, type, key1, key2, key3, getIgnoreContext(function,type,key1,key2,key3));
	}
	
	public Collection getSettings(String function, String type, String key1, String key2, String key3, boolean ignoreContext) throws Exception {
		if (users.length==1){
			return getSettings(users[0],function, type, key1, key2, IGNORE,ignoreContext);	
		}else{
			ArrayList list = new ArrayList();
			String key = buildKey( function, type, key1, key2, key3,ignoreContext);
			for (int i = 0; i < users.length; i++) {
				ArrayList settings = getCachedSettings(users[i],key);
				if (settings == null){
					settings = selectSettings(users[i],function, type, key1, key2, IGNORE,ignoreContext);	
					setCachedSettings(users[i],key,settings);
				}
				if (settings != null && settings != EMPTY_LIST)
					list.addAll(settings);
			}
			return list;
		}
	}
	
	public ArrayList selectSettings(String user,String function, String type, String key1, String key2, String key3, boolean ignoreContext) throws Exception {
		ArrayList settings = getSettings(user);
		ArrayList selectedSettings = new ArrayList(4);
		for (int i = 0; i < settings.size(); i++) {
			Setting setting = (Setting)settings.get(i);
			if (!include(function,setting.getFunction()))
				continue;
			if (!include(type,setting.getType()))
				continue;
			if (!include(key1,setting.getKey1()))
				continue;
			if (!include(key2,setting.getKey2()))
				continue;
			if (!include(key3,setting.getKey3()))
				continue;
			if (!ignoreContext  && !include(context,setting.getContext()))
				continue;
			selectedSettings.add(setting);
		}
		return selectedSettings;
	}
	
	private boolean include(String requestedKeyValue, String foundKeyValue){
		if(IGNORE.equals(requestedKeyValue))
			return true;
		if (requestedKeyValue==null)
			return foundKeyValue ==null;
		return requestedKeyValue.equals(foundKeyValue);
	}
	
	private ArrayList getSettings(String user) throws Exception{
		HashMap _cache = (HashMap)usersSettings.get(dBData);
		if (_cache == null){
			_cache = new HashMap();
			usersSettings.put(dBData,_cache);
		}
		ArrayList settings = (ArrayList)_cache.get(user);
		if (settings == null){
			settings =  getSettings(user, IGNORE, IGNORE, IGNORE, IGNORE, IGNORE, true);
			_cache.put(user,settings);
		}
		return settings;
	}

	public ArrayList getSettings(String user,String function, String type, String key1, String key2, String key3, boolean ignoreContext) throws Exception {
	      ConnectionProvider provider = getConnectionProvider();
	      DBConnection dbc = provider.getConnection();
	      Statement stat = null;
	      buffer.setLength(0);
	      ArrayList collection = new ArrayList();
	      ResultSet rs = null;
	      try{
	    	buffer.append("SELECT *  FROM ");
	    	appendTableName(provider, buffer);
			buffer.append(" WHERE ");
			int startWhere = buffer.length();
	    	appendGeneralFilterClause(provider,buffer,user,startWhere,ignoreContext);
	    	appendWhere(buffer,startWhere,provider,"function",FUNCTION,function,true);
			appendWhere(buffer,startWhere,provider,"type",TYPE,type,true);
			appendWhere(buffer,startWhere,provider,"key1",KEY1,key1,true);
			appendWhere(buffer,startWhere,provider,"key2",KEY2,key2,true);
			appendWhere(buffer,startWhere,provider,"key3",KEY3,key3,true);
			appendOrderBy(buffer,provider);
			Connection con = dbc.getConnection();
			stat = con.createStatement();
			rs = stat.executeQuery(buffer.toString());
			buffer.setLength(0);
			ExtendedUserSetting setting = null;
			while(rs.next()){
				if (setting == null){
					buffer.setLength(0);
					setting = new ExtendedUserSetting(rs);
					String value = rs.getString(8);
					if (value != null && value.length()>=2)
						buffer.append(value.substring(1,value.length()-1));
				}else{
					if (!isEqual(setting.getKey3(),rs.getString(7))||
						!isEqual(setting.getKey2(),rs.getString(6))||
						!isEqual(setting.getKey1(),rs.getString(5))||
						!isEqual(setting.getType(),rs.getString(4))||
						!isEqual(setting.getFunction(),rs.getString(3))||
						!isEqual(setting.getContext(),rs.getString(2))||
						!isEqual(setting.getUser(),rs.getString(1))){
						setting.setValue(buffer.toString());
						buffer.setLength(0);
						collection.add(setting);
						setting = new ExtendedUserSetting(rs);
						String value = rs.getString(8);
						if (value != null && value.length()>=2)
							buffer.append(value.substring(1,value.length()-1));
					}else{
						String value = rs.getString(8);
						if (value != null && value.length()>=2)
							buffer.append(value.substring(1,value.length()-1));
					}
				}
			}
			if (setting != null){
				setting.setValue(buffer.toString());
				collection.add(setting);
			}
			return collection.size()>0?collection:EMPTY_LIST;
	      }catch(java.sql.SQLException e){
	        throw e;
	      }finally {
	      	  if (rs != null)
					try {
						rs.close();
					} catch (Exception e) {	
					}
	    	  if (stat != null)
					try {
						stat.close();
					} catch (Exception e) {
					}
	          provider.returnConnection(dbc);
	      }
	 }
	
	private final String[] getStringArray(String value){
		int length = value.length();
		String[] array = new String[1 + (value.length()/MAX_VALUE_LENGTH)];
		for (int i = 0 ; i < array.length; i++) {
			buffer.setLength(0);
			buffer.append("|");
			buffer.append(value.substring(i*MAX_VALUE_LENGTH,Math.min(length,(i+1)*MAX_VALUE_LENGTH)));
			buffer.append("|");
			array[i] = buffer.toString();
		}
		return array;
	}
	
	
	private static void appendWhere(StringBuffer buffer, int startWhere, ConnectionProvider provider,String name, String column, String value, boolean includeNullValuesInSelection) {
		if (value != IGNORE && (value != null || includeNullValuesInSelection)){
			String key = getFieldName(provider,name,column);
			if (buffer.length() > startWhere)
				buffer.append( " AND ");
			buffer.append(key);
			if (value == null){
				buffer.append(" IS NULL ");
			}else{
				buffer.append(" = '");
				escapeSingleQuotes(buffer,value);
				buffer.append("'");
			}
		}
	}
	
	
	private void appendGeneralFilterClause(ConnectionProvider provider,StringBuffer buffer, String user, int startWhere, boolean ignoreContext) {
		StringBuffer sb1 = new StringBuffer();
		appendWhere(sb1, startWhere, provider, "usertype", USERTYPE, USER_TYPE_G, true);
		StringBuffer sb2 = new StringBuffer();
		appendWhere(sb2, startWhere, provider, "user", USER, user, true);
		buffer.append("(");
		buffer.append(sb1);
		buffer.append(" OR ");
		buffer.append(sb2);
		buffer.append(")");
		    if (ignoreContext == false)
		    	appendWhere(buffer,startWhere,provider,"context",CONTEXT,context,true);
	}
	 
    private boolean isEqual(String a, String b){
    	return a==null?b==null:a.equals(b);
    }
    
    
    public void removeAllSettings() throws Exception{
    	ConnectionProvider provider = getConnectionProvider();
	      DBConnection dbc = provider.getConnection();
	      buffer.setLength(0);
	      try{
	    	buffer.append("DELETE FROM ");
	    	buffer.append(getConnectionProvider().getORMapping().getTableName(TABLE , TABLE));
			dbc.executeUpdate(buffer.toString());
			usersSettings.clear();
			cache.clear();
	      }catch(java.sql.SQLException e){
	        throw e;
	      }finally {
	        provider.returnConnection(dbc);
	      }
	}
    
	private void appendTableName(ConnectionProvider provider, StringBuffer buffer) throws Exception {
		buffer.append(provider.getPrefix());
		buffer.append(getTableName(provider));
	}
	private static final String getTableName(ConnectionProvider provider){
		//return provider.getORMapping().getTableName(objectName, TABLE);
		return TABLE;
	}
	
	private static final String getFieldName(ConnectionProvider provider, String name, String column){
		//return provider.getORMapping().getTableFieldName(name, objectName,column);
		return column;
	}
	
	private static final class ExtendedUserSetting extends UserSetting{
		private static final long serialVersionUID = 1L;

		public ExtendedUserSetting(ResultSet resultSet) throws Exception {
			super();
			user = resultSet.getString(1);
			context = resultSet.getString(2);
			function = resultSet.getString(3);
			type = resultSet.getString(4);
			key1 = resultSet.getString(5);
			key2 = resultSet.getString(6);
			key3 = resultSet.getString(7);
			value = resultSet.getString(8);
			userType = resultSet.getString(10); // 9 is sequenceNumber
			userEdit = resultSet.getBoolean(11);
		}
	}

	public boolean isIgnoreContext() {
		return ignoreContext;
	}

	public void setIgnoreContext(boolean ignoreContext) {
		this.ignoreContext = ignoreContext;
	}
	
	public static interface IgnoreContextProvider{
		
		public boolean getIgnoreContext(String function, String type, String key1, String key2, String key3) ;
		
	}

    public static void clearCaches() {
		usersSettings.clear();
		cache.clear();
	}
    
}

