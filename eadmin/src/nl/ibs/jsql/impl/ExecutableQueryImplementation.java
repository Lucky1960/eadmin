package nl.ibs.jsql.impl;

import java.sql.*;
import java.util.*;

import org.w3c.dom.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;

public class ExecutableQueryImplementation extends QueryImplementation implements ExecutableQuery, FreeQuery {

	private BusinessObjectManager manager;

	public ExecutableQueryImplementation(Class businessObject, String query, int maxRows) throws Exception {
		super(query, maxRows);
		this.manager = BusinessObjectManager.getBusinessObjectManager(businessObject);
	}
	
	public ExecutableQueryImplementation(BusinessObjectManager manager, String query, int maxRows) throws Exception {
		super(query, maxRows);
		this.manager = manager;
	}
	
	public ExecutableQueryImplementation(Class businessObject, String filter, String orderBy, int maxRows) throws Exception {
		super(filter, orderBy, maxRows);
		this.manager = BusinessObjectManager.getBusinessObjectManager(businessObject);
	}

	public ExecutableQueryImplementation(BusinessObjectManager manager, String filter, String orderBy, int maxRows) {
		super(filter, orderBy, maxRows);
		this.manager = manager;
	}

	public Object getFirstObject() throws Exception {
		int prevMax = getMaxObjects();
		setMaxObjects(1);
		try {
			return execute(manager, BusinessObjectManager.RETURN_FIRST_OBJECT);
		} catch (Exception e) {
			throw e;
		} finally {
			setMaxObjects(prevMax);
		}
	}

	public Collection getCollection() throws Exception {
		return (Collection) execute(manager, BusinessObjectManager.RETURN_COLLECTION);
	}

	public ListIterator getListIterator() throws Exception {
		return (ListIterator) execute(manager, BusinessObjectManager.RETURN_LISTITERATOR);
	}

	public Document getDocument() throws Exception {
		return (Document) execute(manager, BusinessObjectManager.RETURN_DOCUMENT);
	}

	public DocumentFragment getDocumentFragment() throws Exception {
		return (DocumentFragment) execute(manager, BusinessObjectManager.RETURN_DOCUMENT_FRAGMENT);
	}

	public ArrayList getDocumentFragmentArrayList() throws Exception {
		return (ArrayList) execute(manager, BusinessObjectManager.RETURN_DOCUMENT_FRAGMENT_ARRAYLIST);
	}

	private static String getName(Class _class) throws Exception {
		String name = _class.getName();
		int i = name.lastIndexOf('.');
		if (i < 0) {
			return name;
		}
		else if (i < name.length()) {
			return name.substring(i + 1);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.FreeQuery#getResultArray()
	 */
	public Object[][] getResultArray() throws Exception {
		return (Object[][]) execute(manager, RETURN_TWO_DIMENSIONAL_OBJECT_ARRAY);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.FreeQuery#getResultCollection(java.lang.Class, java.lang.String[])
	 */
	public Collection getResultCollection(Class clss) throws Exception {
		return (Collection) execute(manager, clss, null, RETURN_OBJECT_COLLECTION);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.FreeQuery#getResultCollection(java.lang.Class, java.lang.String[])
	 */
	public Collection getResultCollection(Class clss, String[] variables) throws Exception {
		return (Collection) execute(manager, clss, variables, RETURN_OBJECT_COLLECTION);
	}
	
		public Collection getCollection(int from, int until) throws Exception {
		return (Collection) execute(manager, BusinessObjectManager.RETURN_COLLECTION, from, until);
	}

	public ListIterator getListIterator(int from, int until) throws Exception {
		return (ListIterator) execute(manager, BusinessObjectManager.RETURN_LISTITERATOR, from, until);
	}

	public Document getDocument(int from, int until) throws Exception {
		return (Document) execute(manager, BusinessObjectManager.RETURN_DOCUMENT, from, until);
	}

	public DocumentFragment getDocumentFragment(int from, int until) throws Exception {
		return (DocumentFragment) execute(manager, BusinessObjectManager.RETURN_DOCUMENT_FRAGMENT, from, until);
	}
	
	
	public ArrayList getDocumentFragmentArrayList(int from, int until) throws Exception {
		return (ArrayList) execute(manager, BusinessObjectManager.RETURN_DOCUMENT_FRAGMENT_ARRAYLIST, from, until);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.FreeQuery#getResultArray(int, int)
	 */
	public Object[][] getResultArray(int from, int until) throws Exception {
		return (Object[][]) execute(manager, RETURN_TWO_DIMENSIONAL_OBJECT_ARRAY, from, until);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.FreeQuery#getResultCollection(java.lang.Class, java.lang.String[], int, int)
	 */
	public Collection getResultCollection(Class clss, int from, int until) throws Exception {
		return (Collection) execute(manager, clss, null, RETURN_OBJECT_COLLECTION, from, until);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.FreeQuery#getResultCollection(java.lang.Class, java.lang.String[], int, int)
	 */
	public Collection getResultCollection(Class clss, String[] variables, int from, int until) throws Exception {
		return (Collection) execute(manager, clss, variables, RETURN_OBJECT_COLLECTION, from, until);
	}
	/* (non-Javadoc)
	 * @see nl.ibs.jsql.FreeQuery#getResultCollection(nl.ibs.jsql.Fillable, int, int)
	 */
	 
	public Collection getResultCollection(
		Fillable filled,
		int beginIndex,
		int endIndex)
		throws Exception {
		return (Collection) execute(manager, filled.getClass(), filled.fieldnamesToBeFilled(), RETURN_OBJECT_COLLECTION, beginIndex, endIndex);
	}

}

