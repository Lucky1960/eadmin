package nl.ibs.jsql.impl;


import java.util.ArrayList;
import java.util.Collection;

/**
 * @author c_hc01
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public final class ArrayListImpl extends ArrayList {
	private boolean fixated = false;
	private ArrayList baseCopy;
	
	
	private final void ensureFixedBase(){
		if (fixated && baseCopy == null)
			baseCopy = new ArrayList(this);
	}
	
	/**
	 * After invoking this method for the first time it is ensured that every subsequential invocation of
	 * getBaseCopy() returnes a ArrayList with exactly the same contents as this ArrayListImpl
	 * had at hte moment the method fixate was invoked the first time.  
	 *
	 */
	public final void fixate(){
		fixated = true;
	}
	
	/**
	 * Returnes a ArrayList with exactly the same contents as this ArrayListImpl
	 * had at the moment the method fixate was invoked the first time. If this method
	 * is not invoked it will return an ArrayList with the current contens of this ArrayListImpl();  
	 * 
	 */
	public final ArrayList getBaseCopy(){
		if (baseCopy == null)
			return new ArrayList(this);
		else
			return new ArrayList(baseCopy);	
	}
	
	/* (non-Javadoc)
	 * @see java.util.List#add(int, java.lang.Object)
	 */
	public void add(int index, Object element) {
		ensureFixedBase();
		super.add(index, element);
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#add(java.lang.Object)
	 */
	public final boolean add(Object o) {
		if (fixated)ensureFixedBase();
		return super.add(o);
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#addAll(java.util.Collection)
	 */
	public boolean addAll(Collection c) {
		ensureFixedBase();
		return super.addAll(c);
	}

	/* (non-Javadoc)
	 * @see java.util.List#addAll(int, java.util.Collection)
	 */
	public boolean addAll(int index, Collection c) {
		ensureFixedBase();
		return super.addAll(index, c);
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#clear()
	 */
	public void clear() {
		ensureFixedBase();
		super.clear();
	}

	/* (non-Javadoc)
	 * @see java.util.List#remove(int)
	 */
	public Object remove(int index) {
		ensureFixedBase();
		return super.remove(index);
	}

	/* (non-Javadoc)
	 * @see java.util.AbstractList#removeRange(int, int)
	 */
	protected void removeRange(int fromIndex, int toIndex) {
		ensureFixedBase();
		super.removeRange(fromIndex, toIndex);
	}

	/* (non-Javadoc)
	 * @see java.util.List#set(int, java.lang.Object)
	 */
	public Object set(int index, Object element) {
		ensureFixedBase();
		return super.set(index, element);
	}

}

