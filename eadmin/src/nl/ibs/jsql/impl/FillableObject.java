package nl.ibs.jsql.impl;

import java.lang.reflect.Field;

import nl.ibs.jsql.Fillable;

abstract public class FillableObject implements Fillable {

	/**
	 * 
	 */
	public FillableObject() {
		super();
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.Fillable#fieldnamesToBeFilled()
	 */
	public String[] fieldnamesToBeFilled() {
		Field fields[] = this.getClass().getFields();
		String names[] = new String[fields.length];
		for (int i = 0; i < fields.length; i++) {
			names[i] = fields[i].getName();
		}
		return names;
	}
	
	
}

