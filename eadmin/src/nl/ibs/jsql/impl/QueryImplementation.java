package nl.ibs.jsql.impl;

import java.sql.*;
import java.util.*;
import nl.ibs.jeelog.*;
import java.lang.reflect.Constructor;
import java.math.*;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;


public class QueryImplementation implements Query{

	
	private static final String DROP_VIEW = "DROP VIEW";
	private static final String CREATE_VIEW = "CREATE VIEW";
	protected static final short RETURN_TWO_DIMENSIONAL_OBJECT_ARRAY = 101;
	protected static final short RETURN_OBJECT_COLLECTION = 102;
	protected static final short RETURN_NOTHING = 103;
   
   
   	protected static Class[] emptyClassArray = new Class[] {};
	protected static Object[] emptyObjectArray = new Object[] {};
	protected static ClassLoader classLoader = QueryImplementation.class.getClassLoader();

	private String freeQuery;
	private AttributeAccessor attributeAccessor; 
		
	protected String selectClause = null;
	protected String updateClause = null;
	protected String setClause = null;
	protected String filterClause = null;
	protected String relationFilterClause = null;
	protected String classFilterClause = null;
	protected String orderByClause = null;
	protected String preparedUpdateString = null;
	protected String preparedQueryString = null;
	protected String sqlStatement = null;
	protected boolean modified = true;
	protected boolean parameters_modified = false;
	protected boolean cacheable = false;
	protected Map imports = null;
	protected Map parameters = null;
	protected Set tables = null;
	protected PersistenceMetaData basePmd;
	protected int maxRows = 0;
	protected int dataBaseId = 0;
	protected ArrayList preparedStatementArgumentList = null;
	protected static ConcurrentHashMap preparedUpdateStatements = new ConcurrentHashMap();
	protected static ConcurrentHashMap preparedQueryStatements = new ConcurrentHashMap();
	public static long hits = 0, lookups = 0;
	public static int maxCacheSize = 250;
	private final static boolean printCacheStatistics = DBConfig.getPrintCacheStatistics();
	private final static boolean cacheQueryTranslations = DBConfig.getCacheQueryTranslations();

	
	public QueryImplementation(String query, int maxRows) {
		this.freeQuery = query;
		this.maxRows = maxRows;
	}

	public QueryImplementation(String filter, String orderBy, int maxRows) {
		this.filterClause = filter;
		this.orderByClause = orderBy;
		this.maxRows = maxRows;
	}

	public Object execute(BusinessObjectManager manager, short instruction) throws Exception {
		return execute(manager, instruction, 0, 0);
	}

	public Object execute(BusinessObjectManager manager, short instruction, int begin, int end) throws Exception {
		return execute(manager, null, null, instruction, begin, end);
	}
	
	protected Object execute(BusinessObjectManager manager,Class return_clss,String[] return_mappings,short instruction)throws Exception {
		return execute(manager, return_clss, return_mappings, instruction,0,0);
	}
	
	protected Object execute(BusinessObjectManager manager,Class return_clss,String[] return_mappings,short instruction,int beginIndex,int endIndex)throws Exception {
		try {
			PersistenceMetaData pmd = manager.getPersistenceMetaData();
			ConnectionProvider provider = manager.getConnectionProvider();
			int dbId = provider.getDBId();
			if (basePmd == null || basePmd != pmd) {
				modified = true;
				basePmd = pmd;
			}
			if (dataBaseId != dbId) {
				modified = true;
				dataBaseId = dbId;
			}
			if (modified)
				buildQuery(manager,provider);
			Object object = executeQuery(manager, return_clss, return_mappings, instruction, beginIndex, endIndex);
			modified = false;
			return object;
		} catch (Exception e) {
		    if (selectClause != null)  Log.error("JSQL select: " + selectClause);
		    if (updateClause != null)  Log.error("JSQL update: " + updateClause); 
		    if (filterClause != null)  Log.error("JSQL filter: " + filterClause); 
		    if (orderByClause != null) Log.error("JSQL order: " + orderByClause); 
		    if (freeQuery  != null)    Log.error("JSQL query: " + freeQuery );
		    if (preparedQueryString != null)  Log.error("JSQL prepared query: " + preparedQueryString);
		    if (preparedUpdateString != null)  Log.error("JSQL prepared select: " + preparedUpdateString);
		    if (sqlStatement != null)  Log.error("JSQL sql statement: " + sqlStatement);
			throw e;
		}
	}

	private void buildQuery(BusinessObjectManager manager, ConnectionProvider provider) throws Exception {
		String filter = getFilter();
		String key = "";
		boolean isQuery= false; 
		if (cacheQueryTranslations){
			key = getKey(filter, freeQuery, orderByClause);
			isQuery =(this instanceof FreeQuery && freeQuery != null && !freeQuery.trim().equals("")) || (setClause == null);	
			if (isQuery) {
				preparedUpdateString = null;
				preparedQueryString = getQueryFromCache(isQuery, basePmd, provider, key, imports);
				if (preparedQueryString != null) {
					return;
				}
			} else {
				preparedQueryString = null;
				preparedUpdateString = getQueryFromCache(isQuery, basePmd, provider, key, imports);
				if (preparedUpdateString != null) {
					return;
				}
			}
		}
		String where = QueryTranslator.translateObjectFilterClause(filter, basePmd, provider, imports);
		String select = null, update = null, set = null, from = null, orderBy = null;
		String classNameConstraint = null;
		if (basePmd instanceof ExtendedPersistenceMetaData) {
			classNameConstraint = ((ExtendedPersistenceMetaData) basePmd).getClassNameConstraint(provider);
		}
		if ((this instanceof FreeQuery || this instanceof NoResultQuery) && freeQuery != null && !freeQuery.trim().equals("")){
			String tfq = freeQuery.trim();
			if (tfq.length() < 6 || !(tfq.substring(0,6).equalsIgnoreCase("SELECT")
			//Marco change 
				|| tfq.substring(0,CREATE_VIEW.length()).equalsIgnoreCase(CREATE_VIEW)
				|| tfq.substring(0,DROP_VIEW.length()).equalsIgnoreCase(DROP_VIEW))  )
			//Marco end
				throw new Exception ("JSQL: Your query statement should start with SELECT or select or create view, other values are currently not permitted");
			preparedQueryString = QueryTranslator.translateFreeQuery(freeQuery, basePmd, provider, imports); 
			if (classNameConstraint != null && !classNameConstraint.equals("")
					//DROP don't need subclass enhancements. 
					&& !tfq.startsWith(DROP_VIEW)){
				int index = preparedQueryString.indexOf(" ORDER ");
				if (index > -1){
					if (preparedQueryString.indexOf(" WHERE ")> -1) {
						preparedQueryString = preparedQueryString.substring(0,index) + " AND " + classNameConstraint + " " + preparedQueryString.substring(index);
					} else  {
						preparedQueryString = preparedQueryString.substring(0,index) + " WHERE " + classNameConstraint + " " + preparedQueryString.substring(index);
					}
				}else{
					if (preparedQueryString.indexOf(" WHERE ")> -1) {
						preparedQueryString += " AND " + classNameConstraint;
					} else {
						preparedQueryString += " WHERE " + classNameConstraint;
					}
				}	
			}
		}else{
			if (classNameConstraint != null && !classNameConstraint.equals("")) {
				where = (where == null ? "" : where + " AND ") + classNameConstraint;
			}
			if (setClause != null) {
				update = provider.getPrefix() + basePmd.getTableName(provider);
				set = QueryTranslator.translateObjectSetClause(setClause, basePmd, provider, imports);
				preparedUpdateString = "UPDATE " + update + " SET " + set + (where != null ? " WHERE " + where : "");
				preparedQueryString = null;
			} else {
				if (tables == null) {
					tables = new HashSet();
				}
				from = QueryTranslator.translateObjectFromClause(tables, basePmd, provider, imports);
				StringBuffer orderBuffer = new StringBuffer();
				StringBuffer fromBuffer = new StringBuffer(from);
				StringBuffer selectBuffer = new StringBuffer(QueryTranslator.translateObjectSelectClause(selectClause==null?"":selectClause, basePmd, provider, imports));
				QueryTranslator.processObjectOrderClause(orderByClause, basePmd, provider, imports, tables, orderBuffer, fromBuffer, selectBuffer);
				orderBy = orderBuffer.toString();
				from = fromBuffer.toString();
				select = selectBuffer.toString();			
				// HC Before the introduction of object serialization in JSQL, DISTINCT was used
				// if more then one table was involved in the query.
				// The reason that it was ditched is that the combination of BLOB columns and DISTINCT will cause DB2 implementation 
				// to throw exception SQL0134 (SQLCODE=-134, SQLSTATE=42907). 
				// Check for , and removal of, double rows should not be needed since we use left outher joins, but if
				// it becomes nescecary nevertheless it can never be delegated to the database by the use of 
				// DISTINCT so it has to be done internally.
				// String distinct = (tables != null && tables.size() > 1) ? " DISTINCT " : "";
				// preparedQueryString ="SELECT "+distinct+select+" FROM "+from+(where==null?"":" WHERE "+where)+((orderBy == null || orderBy.trim().equals("")) ? "" : " ORDER BY " + orderBy);
				preparedQueryString ="SELECT "+select+" FROM "+from+(where==null?"":" WHERE "+where)+((orderBy == null || orderBy.trim().length()==0) ? "" : " ORDER BY " + orderBy);
				preparedUpdateString = null;
			}
		}
		if (cacheQueryTranslations){
			if (isQuery) {
				putQueryOnCache(preparedQueryString, isQuery, basePmd, provider, key, imports);
			} else {
				putQueryOnCache(preparedUpdateString, isQuery, basePmd, provider, key, imports);
			}
		}
	}

	private Object executeQuery(BusinessObjectManager manager, Class return_clss, String[] return_mappings, short instruction, int beginIndex, int endIndex)
		throws Exception {
		if (beginIndex < 0) {
			throw new IllegalArgumentException("beginIndex < 0: beginIndex = " + beginIndex);
		}	
		if (endIndex < 0) {
			throw new IllegalArgumentException("endIndex < 0: endIndex = " + endIndex);
		}	
		if (beginIndex > endIndex) {
			throw new IllegalArgumentException("beginIndex > endIndex: beginIndex = " + beginIndex + ", endIndex = " + endIndex);
		}	
		if (cacheable) {
			return executePreparedQueryStatement(manager, return_clss, return_mappings, instruction, beginIndex, endIndex);
		} else {
			return executeQueryStatement(manager, return_clss, return_mappings, instruction, beginIndex, endIndex);
		}
	}

	private Object executeQueryStatement(BusinessObjectManager manager, Class return_clss, String[] return_mappings,short instruction, int beginIndex, int endIndex) throws Exception {
		if (modified || parameters_modified) {
			sqlStatement = preprocesPreparedQueryForExecution(preparedQueryString);
		}

		Object result = null;
		ConnectionProvider provider = manager.getConnectionProvider();
		
		//Check query result cache in AttomicDBTransaction first
		//Note: XML result types are not yet cached
		if (provider instanceof AtomicDBTransaction  && instruction != RETURN_NOTHING){
			result = ((AtomicDBTransaction)provider).getCachedResult(sqlStatement);
			if (result != null) {
				if (instruction == BusinessObjectManager.RETURN_COLLECTION || instruction == RETURN_OBJECT_COLLECTION){
					if (result instanceof Collection) {
						return result;
					} else if (result == null){
						return new ArrayList(0);	 
					} else if (!(result instanceof Object[][])){
						ArrayList col = new ArrayList(1);
						col.add(result);
						return col;
					}
				} else if (instruction == RETURN_TWO_DIMENSIONAL_OBJECT_ARRAY){
					if (result instanceof Object[][]) {
						return result;
					}
				} else if (instruction == BusinessObjectManager.RETURN_FIRST_OBJECT){
					if (result instanceof ArrayList ){
						ArrayList list = (ArrayList)result;
						return list.isEmpty()?null:list.get(0);
					} else if (!(result instanceof Object[][])){
						return result;
					}
				}
			}
			result=null;
		}
		
		
		//Not in cache then retrieve result	
		DBConnection dbc = provider.getConnection();
		Statement stat = null;
		ResultSet rs = null;
		try {
			Connection con = dbc.getConnection();
			stat = con.createStatement();
			if (beginIndex == 0 && endIndex == 0) {
				stat.setMaxRows(maxRows);
			} else {
				stat.setMaxRows(endIndex);
			}	
			if (instruction== RETURN_NOTHING) {
				int r = stat.executeUpdate(sqlStatement);				
			} else {			
				rs = stat.executeQuery(sqlStatement);
				if (beginIndex > 0) 
					for (int i = beginIndex; i > 0 && rs.next(); i--); 
				result = buildResult(manager,rs, return_clss, return_mappings, instruction);
			}
		} catch (java.sql.SQLException e) {
			Log.error("JSQL: Error executing query '" + sqlStatement + "'");
			Log.error(e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			  if (rs != null) {
					try {
						rs.close();
					} catch (Exception e) {	
					}
				}
	    	  if (stat != null) {
				 try {
			 		stat.close();
				 } catch (Exception e) {
				 }
			  }
			provider.returnConnection(dbc);
		}
		
		//Finally add result to query result cache in AttomicDBTransaction before returning result
		//Note: XML result types are not yet cached
		if (provider instanceof AtomicDBTransaction){
			if (instruction == BusinessObjectManager.RETURN_COLLECTION ||
				instruction == RETURN_TWO_DIMENSIONAL_OBJECT_ARRAY ||
			    instruction == RETURN_OBJECT_COLLECTION) {
				((AtomicDBTransaction)provider).setCachedResult(sqlStatement,result);
			} else if (instruction == BusinessObjectManager.RETURN_FIRST_OBJECT) {
				((AtomicDBTransaction)provider).setCachedResult(sqlStatement,result==null?new ArrayList(0):result);	
			}
		}
			
		return result;
	}

	private Object executePreparedQueryStatement(BusinessObjectManager manager, Class return_clss, String[] return_mappings,short instruction, int beginIndex, int endIndex) throws Exception {
		if (modified) {
			sqlStatement = preprocesPreparedQueryForPreparedStatement(preparedQueryString);
		}
		Object result = null;
		ConnectionProvider provider = manager.getConnectionProvider();
		DBConnection dbc = provider.getConnection();
		ResultSet rs = null;
		try {
			PreparedStatement prep = dbc.getPreparedStatement(sqlStatement);
			if (prep == null) {
				prep = dbc.getPreparedStatement(sqlStatement, sqlStatement);
			}
			for (int i = 0; i < preparedStatementArgumentList.size(); i++) {
				if (parameters == null) {
					throw new Exception("JSQL Exception: no value provided for parameter " + preparedStatementArgumentList.get(i) + "!");
				}
				Object object = parameters.get(preparedStatementArgumentList.get(i));
				if (object != null) {
					prep.setObject(i + 1, object);
				} else {
					throw new Exception("JSQL Exception: no value provided for parameter " + preparedStatementArgumentList.get(i) + "!");
				}
			}

			if (beginIndex == 0 && endIndex == 0) {
				prep.setMaxRows(maxRows);
			} else {
				prep.setMaxRows(endIndex);
			}	
				
			if (instruction == RETURN_NOTHING) {
				int r = prep.executeUpdate(sqlStatement);
			} else {
				rs = prep.executeQuery();
				if (beginIndex > 0) {
					for (int i = beginIndex; i > 0 && rs.next(); i--);
				} 
				result = buildResult(manager, rs, return_clss, return_mappings, instruction);
			}
		} catch (java.sql.SQLException e) {
			Log.error("JSQL: Error executing query '" + sqlStatement + "'");
			Log.error(e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {	
				}
	    	provider.returnConnection(dbc);
		}

		return result;
	}
	
	private Object buildResult(BusinessObjectManager manager, ResultSet rs, Class return_clss, String[] return_mappings,short instruction) throws Exception {
		if (instruction == RETURN_TWO_DIMENSIONAL_OBJECT_ARRAY) {
			return buildObjectArray(rs);
		} else if (instruction == RETURN_OBJECT_COLLECTION){
			return buildObjectCollection(rs, return_clss,return_mappings );
		} else {
			return manager.processResultSet(rs, instruction);
		}
	}

	/**
	 * @param rs
	 * @return
	 */
	private Object buildObjectArray(ResultSet rs) throws Exception {
		ArrayList al = new ArrayList();
		ResultSetMetaData md = rs.getMetaData();
		int cc = md.getColumnCount();
		while(rs.next()) {
			Object[] row = new Object[cc];
			for (int i = 0; i < cc; i++) { 
				row[i]= rs.getObject(i+1);
			}
			al.add(row);
		}
		
		return al.toArray(new Object[al.size()][cc]); 
	}
	
	
	private Object buildObjectCollection(ResultSet rs, Class return_clss, String[] return_mappings) throws Exception {
		ArrayList al = new ArrayList();
		ResultSetMetaData md = rs.getMetaData();
		int cc = md.getColumnCount();
		while (rs.next()) {
			Object object = return_clss.newInstance();
			if (return_mappings == null) {
				for (int i = 0; i < cc; i++){ 
					try { 
						getAttributeAccessor().forceAttribute(object,md.getColumnName(i+1),rs.getObject(i+1),true);
					} catch (Throwable e) {
						e.printStackTrace();
						throw new Exception("Exception trying to map column " +md.getColumnName(i+1)+ " from result to an attribute with the same name. See previous messages for details, check aliases in your select clause and the types to correct the problem (you can also provide mappings instead of using aliases)!");
					}
				}
			} else {
				for (int i = 0; i < return_mappings.length && i < cc; i++) {
					try{ 
						if (return_mappings[i] != null && !return_mappings[i].equals("")) {
							getAttributeAccessor().forceAttribute(object,return_mappings[i],rs.getObject(i+1),false);
						}
					} catch (Throwable e) {
						e.printStackTrace();
						throw new Exception("Exception trying to map result set column " + (i+1) + " [" +md.getColumnName(i+1)+ "] to attribute " + return_mappings[i] + ". See previous messages for details, check names and types and correct your mapping!");
					}		
				}
			}	
			al.add(object);
			if (object instanceof Fillable) {
				Fillable fillable = (Fillable) object;
				fillable.fill();
			}
		}
		return al; 
	}
	
	private AttributeAccessor getAttributeAccessor(){
		if (attributeAccessor == null)
			attributeAccessor = new AttributeAccessor();
		return attributeAccessor;
	}

	private String preprocesPreparedQueryForExecution(String preparedString) {
		return preprocesPreparedQuery(preparedString, false);
	}

	private String preprocesPreparedQueryForPreparedStatement(String preparedString) {
		if (preparedStatementArgumentList == null) {
			preparedStatementArgumentList = new ArrayList();
		} else {
			preparedStatementArgumentList.clear();
		}
		return preprocesPreparedQuery(preparedString, true);
	}

	private String preprocesPreparedQuery(String preparedString, boolean preparedStatement) {
		int x = 0;
		int y = preparedString.indexOf("<?");
		if (y < 0) {
			return preparedString;
		}
		StringBuffer buffer = new StringBuffer();
		while (y >= 0) {
			boolean quote = false;
			for (int i = 0; i < y; i++) {
				if (preparedString.charAt(i) == '\'') {
					if (!quote) {
						quote = true;
						continue;
					} else {
						if (i + 1 < y && preparedString.charAt(i + 1) == '\'') {
							i++;
							continue;
						} else {
							quote = false;
						}
					}
				}
			}
			if (quote) {
				y = preparedString.indexOf("<?", y + 2);
				continue;
			}
			buffer.append(preparedString.substring(x, y));
			x = y + 2;
			y = preparedString.indexOf("?>", x);
			String variableName = preparedString.substring(x, y);
			if (preparedStatement) {
				preparedStatementArgumentList.add(variableName);
				buffer.append(" ? ");
			} else {
				if (parameters != null && parameters.containsKey(variableName)) {
					Object object = parameters.get(variableName);
					if (object instanceof String)
						object = escapeSingleQuotes((String) object);
					buffer.append(getValueAsSQLString(object));
				} else {
					buffer.append(variableName);
				}
			}
			x = Math.min(preparedString.length(), y + 2);
			y = preparedString.indexOf("<?", x);
			if (y < 0 && x < preparedString.length())
				buffer.append(preparedString.substring(x));
		}
		return buffer.toString();
	}

	public String getSelectClause() {
		return selectClause;
	}

	public void setSelectClause(String clause) {
		if (selectClause == null || !selectClause.equals(clause)) {
			modified = true;
			selectClause = clause;
		}
	}

	public void setFromTables(Set tableSet) {
		modified = true;
		tables = tableSet;
	}

	public void addTableToFrom(String table) {
		if (tables == null || !tables.contains(table)) {
			if (tables == null) {
				tables = new HashSet();
			}
			tables.add(table);
			modified = true;
		}
	}

	private String getFilter() {
		String filter = classFilterClause;
		if (filter == null) {
			filter = relationFilterClause;
		} else if (relationFilterClause != null && relationFilterClause.trim().equals("") == false) {
			filter = filter + " AND " + relationFilterClause;
		}
		if (filter == null) {
			filter = filterClause;
		} else if (filterClause != null && filterClause.trim().equals("") == false) {
			filter = filter + " AND ( " + filterClause + " )";
		}
		return filter;
	}
	
    public void setQuery(String qry, Map replacements) {
		if (replacements != null) {
			for (Iterator it = replacements.keySet().iterator();it.hasNext();) {
				String org = (String)it.next();
				int index;
				while ((index = qry.indexOf(org))>=0) {
					qry = qry.substring(0,index) + replacements.get(org) + qry.substring(index+org.length(), qry.length()); 	
				}
			}
		}
		setQuery(qry);
	}
	

	public void setQuery(String query) {
		if (query == null || !query.equals(freeQuery)) {
			modified = true;
			freeQuery = query;
			classFilterClause = null;
			relationFilterClause = null;
		}
	}

	public void setFilter(String filter) {
		if (filterClause == null || !filterClause.equals(filter)) {
			modified = true;
			filterClause = filter;
			classFilterClause = null;
			relationFilterClause = null;
		}
	}

	public void setRelationFilter(String relationFilter) {
		if (relationFilterClause == null || !relationFilterClause.equals(relationFilter)) {
			modified = true;
			relationFilterClause = relationFilter;
		}
	}

	public void setClassFilter(String classFilter) {
		if (classFilterClause == null || !classFilterClause.equals(classFilter)) {
			modified = true;
			classFilterClause = classFilter;
		}
	}

	public String getOrdering() {
		return orderByClause;
	}

	public void setOrdering(String orderBy) {
		if (orderByClause == null || !orderByClause.equals(orderBy)) {
			modified = true;
			orderByClause = orderBy;
		}
	}

	public void setCacheable(boolean cache) {
		if (cacheable != cache) {
			modified = true;
			cacheable = cache;
		}
	}

	public boolean getCacheable() {
		return cacheable;
	}

	public void setMaxObjects(int max) {
		maxRows = max;
	}

	public int getMaxObjects() {
		return maxRows;
	}

	public void addImport(Class businessObjectClass) throws Exception {
		String name = getName(businessObjectClass);
		addImport(name, businessObjectClass);
	}

	public void addImport(BusinessObjectManager manager) {
		addImport(manager.getPersistenceMetaData());
	}

	public void addImport(String alias, BusinessObjectManager manager) {
		addImport(alias, manager.getPersistenceMetaData());
	}

	public void addImport(String alias, Class businessObjectClass) throws Exception {
		try {
			PersistenceMetaData pmd = getPersistenceMetaData(businessObjectClass);
			addImport(alias, pmd);
		} catch (Exception e) {
			Log.error(e.getMessage());
			e.printStackTrace();
			String namePMDClass = null;
			try {
				namePMDClass = getNamePersistenceMetaDataClass(businessObjectClass);
			} catch (Exception ex) {
			}
			throw new Exception(
				"Could not import " + businessObjectClass.getName() + "! Verify that that this class implements BusinessObject and that class " + namePMDClass + " exists!");
		}
	}

	public void addImport(PersistenceMetaData pmd) {
		addImport(pmd.getBusinessObjectName(), pmd);
	}

	public void addImport(String name, PersistenceMetaData pmd) {
		if (imports == null) {
			imports = new HashMap();
		}
		if (!imports.containsKey(name) || imports.get(name) != pmd) {
			modified = true;
			imports.put(name, pmd);
		}
	}

	public void setParameters(Map map) {
		if ((parameters == null) != (map == null) || (parameters != null && !parameters.equals(map))) {
			parameters_modified = true;
			if (map == null) {
				parameters = null;
			} else {
				parameters = new HashMap(map);
			}
		}
	}
	
	public void addParameters(Map map) {
		if (map != null && !map.isEmpty()) {
			parameters_modified = true;
			if (parameters == null) {
				parameters = new HashMap(map);
			} else {
				parameters.putAll(map);
			}
		}
	}

	public void setParameter(String name, Object value) {
		if (parameters == null) {
			parameters = new HashMap();
		}
		if (!parameters.containsKey(name) || parameters.get(name) != value) {
			parameters_modified = true;
			parameters.put(name, value);
		}
	}

	public void setParameter(String name, long value) {
		setParameter(name, new Long(value));
	}

	public void setParameter(String name, short value) {
		setParameter(name, new Short(value));
	}

	public void setParameter(String name, int value) {
		setParameter(name, new Integer(value));
	}

	public void setParameter(String name, double value) {
		setParameter(name, new Double(value));
	}

	public void setParameter(String name, float value) {
		setParameter(name, new Float(value));
	}

	public void setParameter(String name, boolean value) {
		setParameter(name, new Boolean(value));
	}

	private static String getName(Class _class) throws Exception {
		String name = _class.getName();
		int i = name.lastIndexOf('.');
		if (i < 0) {
			return name;
		} else if (i < name.length()) {
			return name.substring(i + 1);
		}
		return null;

	}

	private static PersistenceMetaData getPersistenceMetaData(Class _class) throws Exception {
		String name = getNamePersistenceMetaDataClass(_class);
		Class pmdClass = classLoader.loadClass(name);
		java.lang.reflect.Method method = pmdClass.getDeclaredMethod("getInstance", emptyClassArray);
		return (PersistenceMetaData) method.invoke(null, emptyObjectArray);
	}

	private static String getNamePersistenceMetaDataClass(Class _class) throws Exception {
		String prefix = "impl.PersistenceMetaData";
		String name = _class.getName();
		int i = name.lastIndexOf('.');
		if (i < 0) {
			name = prefix + name;
		} else if (i < name.length()) {
			name = name.substring(0, i + 1) + prefix + name.substring(i + 1);
		}
		return name;
	}

	private static String getValueAsSQLString(Object obj) {
		if (obj instanceof Short || obj instanceof Integer || obj instanceof Long || obj instanceof BigInteger || obj instanceof BigDecimal) {
			return String.valueOf(obj);
		} else if (obj instanceof Float || obj instanceof Double) {
			return String.valueOf(new BigDecimal(String.valueOf(obj)));
		} else if (obj instanceof java.util.Date) {
			return "'" + (new java.sql.Date(((java.util.Date) obj).getTime())).toString() + "'";
		} else {
			return "'" + String.valueOf(obj) + "'";
		}
	}

	private static String escapeSingleQuotes(String string) {
		if (string.indexOf('\'') < 0) {
			return string;
		}
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == '\'') {
				buffer.append('\'');
			}
			buffer.append(string.charAt(i));
		}
		return buffer.toString();
	}
	
	private static void resizeCache(Map map){
		long t1 = 1000*60*60*24*2;
		while (map.size() > (0.75 * maxCacheSize)) {
			long t2 = System.currentTimeMillis()-t1;
			Iterator it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry entry = (Map.Entry)it.next();
				if (((PreparedQ)entry.getValue()).lastTime < t2) {
					it.remove();
				}
			}
			t1 =  t1/10;
	    }
	    maxCacheSize = map.size()+ maxCacheSize;
	    if (printCacheStatistics) {
	    	Log.info("Tuned query cache: new current size = "+ map.size() +", new maximum size = " + maxCacheSize);
	    }	
	}
	
	private static void putQueryOnCache(String translation,boolean isQuery,PersistenceMetaData pmd,ConnectionProvider provider,String key,Map imports)throws Exception {
		Map cacheMap = getCacheMap(isQuery, pmd);
		PreparedQ pQ = new PreparedQ();
		pQ.translation = translation;
		pQ.dbId = provider.getDBId();
		pQ.imports = imports;
		cacheMap.put(key, pQ);
		if (cacheMap.size() > maxCacheSize) {
			resizeCache(cacheMap);
		}
	}

	private static String getQueryFromCache(boolean isQuery,PersistenceMetaData pmd,ConnectionProvider provider,String key,Map imports)throws Exception {
		String query = null;
		Map cacheMap = getCacheMap(isQuery, pmd);
		PreparedQ pQ = (PreparedQ) cacheMap.get(key);
		if (pQ != null && pQ.dbId == provider.getDBId()) {
			pQ.lastTime = System.currentTimeMillis();
			query = pQ.translation;
		}
		if (printCacheStatistics) {
			lookups++;
			hits = query != null?hits+1:hits;
			if (lookups%1000==0){
				Log.info("Hit ratio: "+((hits * 100) / lookups)+"%. ("+lookups+" lookups,"+hits+" hits)");
			}
		}
		return query;
	}

	private static Map getCacheMap(boolean isQuery, PersistenceMetaData pmd) {
		Map cache1 = isQuery?preparedQueryStatements:preparedUpdateStatements;
		Map cache2 = (Map) cache1.get(pmd.getClass());
		if (cache2 == null) {
			cache2 = new ConcurrentHashMap();
			cache1.put(pmd.getClass(), cache2);
		}
		return cache2;
	}

	private static String getKey(String s1, String s2, String s3) {
		StringBuffer buffer = new StringBuffer(2+(s1==null?0:s1.length())+(s2==null?0:s2.length())+(s3==null?0:s3.length()));
		if (s1!=null)buffer.append(s1);
		buffer.append("#");
		if (s2!=null)buffer.append(s2);
		buffer.append("#");
		if (s3!=null)buffer.append(s3);
		return buffer.toString();
	}

	static class PreparedQ {
		String translation;
		Map imports;
		int dbId;
		long lastTime;
	}
	
}

