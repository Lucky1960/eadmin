package nl.ibs.jsql.impl;

import java.util.*;
import nl.ibs.jeelog.*;

public class ListIteratorImpl implements java.util.ListIterator{

    

    protected java.util.Vector vector = new Vector();
    public int pointer = 0; //holds the index of the element returned when calling next() 
    protected int prev = -1;
    
    public void add(Object object){
        vector.add(pointer++,object);
    }
    
    public boolean hasNext() {
        return pointer < vector.size();
    }
    
    public boolean hasPrevious() {
        return pointer > 0;
    }
    
    public Object next() throws NoSuchElementException{
        if (hasNext()){
       Object obj =  vector.get(pointer++);
        prev = pointer -1;
        return obj;
        }else{
        throw new NoSuchElementException("Listiterator doesn't contain next value");
        }
    }
    
    public int nextIndex() {
        return pointer;
    }
    
    public Object previous()throws NoSuchElementException {
        if (hasPrevious()){
        Object obj = vector.get(--pointer);
        prev = pointer; 
        return obj;
        }else{
        throw new NoSuchElementException("Listiterator doesn't previous value");
        }
    }
    
    public int previousIndex() {
        return (pointer - 1) ;
    }
    
    public void remove() {
        if (prev > -1) {
            vector.remove(prev);
            prev = -1;
        }else{
            throw new IllegalStateException("Method 'remove()' can only be made once per call to next or previous. And it can be made only if ListIterator.add has not been called after the last call to next or previous.");
        }
    }
    
    public void set(Object obj) {
             if (prev > -1) {
            vector.set(prev, obj);
        }else{
            throw new IllegalStateException("Call to methyod 'set(Object obj)' can be made only if neither ListIterator.remove nor ListIterator.add have been called after the last call to next or previous.");
        }
    }
    
}

