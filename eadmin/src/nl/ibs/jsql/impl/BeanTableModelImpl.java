package nl.ibs.jsql.impl;

import java.lang.reflect.*;
import java.util.*;
import nl.ibs.jeelog.*;

/**
 *
 * @author  c_hc01
 */
public class BeanTableModelImpl extends javax.swing.table.AbstractTableModel {

    
    
    ClassLoader classLoader;
    ArrayList objects;
    Class _class;
    //Method[] methods;
    ArrayList columns = new ArrayList();
    ArrayList getters = new ArrayList();
    ArrayList setters = new ArrayList();
    ArrayList headers = new ArrayList();
    ArrayList editable = new ArrayList();
    ArrayList classes = new ArrayList();
         
    public BeanTableModelImpl(ArrayList list, Class _class) {
        super();
        objects = (ArrayList)list.clone();
        this._class = _class;
        classLoader = _class.getClassLoader();
    }
    
    public BeanTableModelImpl(Collection collection, Class _class) {
        super();
        objects = new ArrayList(collection);
        this._class = _class;
        classLoader = _class.getClassLoader();
    }
    
    
    public BeanTableModelImpl(ListIterator iterator, Class _class) {
        super();
        objects = new ArrayList();
        while (iterator.hasPrevious()) iterator.previous();
        while (iterator.hasNext()) objects.add(iterator.next());
        this._class = _class;
        classLoader = _class.getClassLoader();
    }
    
    public void addColumn(String columnName){
        addColumn(columnName, null, false);
    }
    
    public void addColumn(String columnName, String columnHeader){
        addColumn(columnName, columnHeader, false);
    }
    
    public void addColumn(String columnName, String columnHeader, boolean editable){
        setColumn(columnName, columnHeader,  columns.size(), editable);
    }
    
    public void addColumn(String columnName, boolean editable){
        setColumn(columnName, null, columns.size(), editable);
    }
    
    public void setColumn(String columnName, int index){
        setColumn(columnName, null, index, false);
        
    }
    
    public void setColumn(String columnName, int index, boolean editable){
        setColumn(columnName, null, index, editable);
    }
    
    public void setColumn(String columnName, String columnHeader, int index, boolean _editable){
        try{
            columnName = columnName.substring(0,1).toUpperCase() + columnName.substring(1);
            if (columnHeader == null)
                columnHeader = translateFromCamelBack(columnName);
            Method get = null, set = null;
            try{ get = _class.getMethod("get" + columnName, new Class[]{});}catch(Exception e){}
            try{ if (get != null) set = _class.getMethod("set" + columnName, new Class[]{get.getReturnType()});}catch(Exception e){}
            
            if (get != null && Modifier.isPublic(get.getModifiers())){
                Log.debug("Add column " + columnName);
                columns.add(index, columnName);
                getters.add(index, get);
                setters.add(index, set);
                headers.add(index, columnHeader);
                editable.add(index, new Boolean(_editable && set != null && Modifier.isPublic(set.getModifiers())));

                Class columnClass = get.getReturnType();
                if (columnClass.isPrimitive()){
                    if      (columnClass == Boolean.TYPE) columnClass = Boolean.class;
                    else if (columnClass == Integer.TYPE) columnClass = Integer.class;
                    else if (columnClass == Long.TYPE) columnClass = Long.class;
                    else if (columnClass == Float.TYPE) columnClass = Float.class;
                    else if (columnClass == Double.TYPE) columnClass = Double.class;
                    else if (columnClass == Character.TYPE) columnClass = Character.class;
                    else if (columnClass == Byte.TYPE) columnClass = Byte.class;
                    else if (columnClass == Short.TYPE) columnClass = Short.class;
                }
                classes.add(index, columnClass);
        
            } else {
                Log.error("Column " + columnName + " not valid");
            }
            
        }catch (Exception e){
            Log.error(e.getMessage());
            e.printStackTrace();
        }
    }
    
    public void setAllColumns(boolean editable){
        removeAllColumns();
        Field[] fields = _class.getDeclaredFields();
        for (int x = 0; x < fields.length; x ++){
            if (Modifier.isPrivate(fields[x].getModifiers()) && !Modifier.isStatic(fields[x].getModifiers())){
                addColumn(fields[x].getName(),  editable);
            }
        }
    }
    
    public void removeAllColumns(){
        columns.clear();
        getters.clear();
        setters.clear();
        headers.clear();
        editable.clear();
        classes.clear();
    }
    
     public void removeColumn(String columnName){
        try{ 
            columnName = columnName.substring(0,1).toUpperCase() + columnName.substring(1);
            removeColumn(columns.indexOf(columnName));
       }catch(Exception e){
            Log.debug(" column " + columnName + "  not found!");
       }
    }
    
    public void removeColumn(int index){
        if (index != -1 && index < columns.size() ){
        columns.remove(index);
        getters.remove(index);
        setters.remove(index);
        headers.remove(index);
        editable.remove(index);
        classes.remove(index);
        }
    }
    
    public void setEditable(int column , boolean _editable){
        editable.set(column, 
                     new Boolean(_editable 
                                 && setters.get(column) != null
                                 && Modifier.isPublic(((Method)setters.get(column)).getModifiers())
                     )
        );
        
    }
    
    public void setEditable(String columnName , boolean _editable){
        columnName = columnName.substring(0,1).toUpperCase() + columnName.substring(1);
        try{
            editable.set(columns.indexOf(columnName), new Boolean(_editable));
        }catch(Exception e){
            Log.error(" column " + columnName + "  not found");
        }
    }
    
    public void setColumnHeader(String columnName, String columnHeader){
        columnName = columnName.substring(0,1).toUpperCase() + columnName.substring(1);
        try{headers.set(columns.indexOf(columnName), columnHeader);
        }catch(Exception e){
            Log.error(" column " + columnName + "  not found");
        }
    }
    
    public void replaceColumnHeader(String oldValue, String newValue){
        try{
        headers.set(columns.indexOf(oldValue), newValue);
        } catch (Exception e){
            Log.error("Header " + oldValue + "  not found");
        }
    }
    
    public void setColumnHeader(int index, String header){
        headers.set(index, header);
    }
    
    public ArrayList getHeaderNames(){
        return (ArrayList)headers.clone();
    }
    
    public ArrayList getColumnNames(){
        return (ArrayList)columns.clone();
    }

    public int getColumnIndexOf(String columnName){
        return columns.indexOf(columnName);
    }

    public int getHeaderIndexOf(String headerName){
        return headers.indexOf(headerName);
    }
    
    public String translateFromCamelBack(String in){
        StringBuffer buffer = new StringBuffer();
        boolean prevIsLower = false;
        for (int x = 0; x < in.length(); x++){
            char c = in.charAt(x);
            if (x==0 && Character.isLetter(c)){
                buffer.append(Character.toUpperCase(c));
            } else if (x == 0){
                buffer.append(c);
            } else if (!Character.isLetter(c)){
                buffer.append(c);
            } else if (Character.isLowerCase(c)){
                prevIsLower = true;
                buffer.append(c);
            } else if (prevIsLower){
                prevIsLower = false;
                buffer.append(" ");
                if (x + 1 < in.length() && Character.isUpperCase(in.charAt(x+1)))
                    buffer.append(c);
                else
                    buffer.append(Character.toLowerCase(c));
            } else {
                if (x + 1 < in.length() && Character.isLowerCase(in.charAt(x+1)))
                    buffer.append(" " + Character.toLowerCase(c));
                else
                    buffer.append(c);
            }
        }
        return buffer.toString();
    }
    
    public int getColumnCount() {
        return columns.size();
    }
    
    public int getRowCount() {
        return objects.size();
    }
    
    public Object getValueAt(int row, int column) {
        if(Log.debug())Log.debug("getValue (" + row + " x " + column + ")");
        try{
            return ((Method)getters.get(column)).invoke(objects.get(row), new Object[]{});
        }catch (Exception e){
            Log.error(e.getMessage());
            e.printStackTrace();
        }
        Log.debug("returning null!");
        return null;
    }
    
    public void setValueAt(Object value, int row, int column) {
        if (((Boolean)editable.get(column)).booleanValue()){
            try{
                ((Method)setters.get(column)).invoke(objects.get(row), new Object[]{value});
            }catch (Exception e){
                Log.error(e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    public boolean isCellEditable( int row, int column ) {
        return ((Boolean)editable.get(column)).booleanValue();
    }
    
    public Class getColumnClass(int column) {
        return (Class)classes.get(column);
    }
    
    public String getColumnName(int column)  {
        return (String)headers.get(column);
    }

}

