package nl.ibs.jsql.impl;


import nl.ibs.jsql.sql.BusinessObjectManager;

/**
 * @author c_md07
 *
 */
public class NoResultQuery extends QueryImplementation{

	/**
	 * @param query
	 * @param maxRows
	 */
	public NoResultQuery(String query, int maxRows) {
		super(query, maxRows);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.impl.QueryImplementation#execute(nl.ibs.jsql.sql.BusinessObjectManager, short)
	 */
	public void execute(BusinessObjectManager manager) throws Exception {
		super.execute(manager, RETURN_NOTHING);
	}
	
}

