package nl.ibs.jsql;



public class UserSetting implements nl.ibs.vegas.Setting {
	private static final long serialVersionUID = 1L;

	protected String user;
	protected String context;
	protected String function;
	protected String type;
	protected String key1;
	protected String key2;
	protected String key3;
	protected String value;
	protected String userType;
	protected boolean userEdit;

	public UserSetting() {
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getKey1() {
		return key1;
	}

	public void setKey1(String key1) {
		this.key1 = key1;
	}

	public String getKey2() {
		return key2;
	}

	public void setKey2(String key2) {
		this.key2 = key2;
	}

	public String getKey3() {
		return key3;
	}

	public void setKey3(String key3) {
		this.key3 = key3;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getContext() {
		return context;
	}

	public String getUser() {
		return user;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public boolean getUserEdit() {
		return userEdit;
	}

	public void setUserEdit(boolean userEdit) {
		this.userEdit = userEdit;
	}

}

