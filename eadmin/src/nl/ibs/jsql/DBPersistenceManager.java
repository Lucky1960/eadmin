package nl.ibs.jsql;


import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import nl.ibs.jeelog.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;


/**
 * Sets or gets the ConnectionPool for current Thread and Threads started within this Thread. 
 *
 */
public class DBPersistenceManager {
   
    
    
	private static ArrayList connectionLyfeCycleListeners=new ArrayList(1);
	private static ConnectionPool defaultPool;
	private static Cache applicationCache = null;
	private static boolean cachingOff = DBConfig.CACHING_OFF.equals(DBConfig.getCaching());
	private static boolean transactionCaching = DBConfig.CACHING_TRANSACTION.equals(DBConfig.getCaching());

	private static InheritableThreadLocal activeTransaction = new InheritableThreadLocal();

	static {
		if (DBConfig.CACHING_APPLICATION.equals(DBConfig.getCaching()))
			applicationCache = new Cache();
	}

      /**
	 * Set a new default database on the current thread and all its child threads.
	 * @param defaultDBD DBData
	 */
	public static void setDefaultDataBase(DBData dbdata){
		DBData.setDefaultDBData(dbdata);
	}

	/**
	 * Returns the ConnectionPool currently active for this Thread. 
	 *
	 * @return ConnectionPool
	 */
	public static ConnectionProvider getConnectionProvider(String application) throws Exception {
		return getConnectionProvider(DBData.getDefaultDBData(application));
	}

	/**
	 * Returns the ConnectionProvider currently active for this Thread. 
	 *
	 * @return ConnectionProvider
	 */
	public static ConnectionProvider getConnectionProvider(DBData data) throws Exception {
		DBTransaction transaction = (DBTransaction)activeTransaction.get();
		if (transaction == null)
			return DBConnectionPool.getInstance(data);
		else
			return transaction.getConnectionProvider(data);		
	}

	/**
	 * Sets a new DBTransaction for this Thread. 
	 * 
	 * @param pool The ConnectionPool to set.
	 */
	public static void setTransaction(DBTransaction tx) {
		activeTransaction.set(tx);
	}

	/**
	 * Returns the currently active TransAction for this Thread.</br>
	 * If Currently no Transaction set, new Transaction wil be created, set on Thread and returned.
	 *
	 * @return DBTransaction Currently active Transaction for this thread.
	 */
	public static DBTransaction getCurrentTransaction() {
		DBTransaction tx = (DBTransaction)activeTransaction.get();
		if (tx == null){
			tx = new DBTransaction();
			setTransaction(tx);
		}
		return tx;
	}

	/**
	 * Returns the currently TransAction set for this Thread or null.</br>
	 *
	 * @warning   For internal use only! Use at your own risk!
	 * @return DBTransaction or null.
	 */
	public static DBTransaction getTransaction() {
			return (DBTransaction)activeTransaction.get();
	}


	/**
	 * Creates sets and returns a new TransAction for the current thread.</br>
	 * If currently a Transaction is set on this thread wich is not commited or rollbacked an 
	 * InvalidStateException will be thrown:
	 *
	 * @return DBTransaction newly created active Transaction for this thread.
	 */
	public static DBTransaction getNewTransaction() throws InvalidStateException {
		DBTransaction tx = (DBTransaction)activeTransaction.get();
		if (tx != null && tx.isActive()){
			throw new InvalidStateException("Invoked DBTransaction.getNewTransaction() while current transaction still active. First submit or rollback current transaction before invoking getNewTransaction()");
		}
		tx = new DBTransaction();
		setTransaction(tx);
		return tx;
	}
	

	
	
	/**
	 * Returns the currently active Cache for this application, thread
	 * or transaction, or null if no cache is active.
	 *
	 * @return Cache
	 */
	public static Cache getCache() {
		if (cachingOff)
			return null;
		else if (applicationCache != null)
			return applicationCache;
		else if (transactionCaching) {
			DBTransaction tx = (DBTransaction)activeTransaction.get();
			if (tx != null)
				return tx.getCache();
		}
		return null;
	}

	/**
	 * Sets a new cach for this application.<br/>
     * <br/>
     * <b/>WARNING:</b><br/>
     * You should never use application wide caching for webapplications.
	 *
	 * @param the Cache to use for this application, may be null and may contain values, 
	 * but should not contain duplicates.
	 */
	public static void setApplicationCache(Cache cache) throws Exception {
		if (applicationCache != null)
			applicationCache = cache;
		else
			throw new Exception("Caching is not activated for application, check your configuration!!");
	}

	/**
	 * Returns cached object, if cache does not contain object for wich cachedObject.equals(object) returns true
	 * object will be added to cache before retruned.
	 *
	 * @param object to cache or be replaced by cached equivalent.
	 */

	public static Object cache(Object o) {
		Cache cache;
		if (o == null || (cache = getCache())==null)
			return o;
		else 
			return cache.cache(o);

	}

	/**
	 * Removes for every entry in the collection the first object from the cache.
	 *
	 * @param Collection of objects to be removed from cache.
	 */
	public static void removeFromCache(Collection c) {
		Cache cache;
		if (c != null && !c.isEmpty() && (cache = getCache()) != null)
			cache.removeFromCache(c);
	}

	/**
	 * Removes first object from cache.
	 *
	 * @param object to be removed from cache.
	 */
	public static void removeFromCache(Object o) {
		Cache cache;
		if (o != null && (cache = getCache()) != null)
			cache.removeFromCache(o);
	}
	
	/**
	 * This method is invoked bij JSQL after connection is created.
	 * 
	 * @param con
	 * @throws Exception
	 */
	public static void invokeConnectionLifeCycleListenersPostConnected(Connection con) throws Exception {
		for (int i = 0; i < connectionLyfeCycleListeners.size(); i++) {
			((ConnectionLifeCycleListener)connectionLyfeCycleListeners.get(i)).postConnect(con);
		}
	}
	
	/**
	 * This method is invoked bij JSQL before connection is disconnected.
	 * 
	 * @param con
	 * @throws Exception
	 */
	public static void invokeConnectionLifeCycleListenersPreDisconnected(Connection con) throws Exception{
		for (int i = 0; i < connectionLyfeCycleListeners.size(); i++) {
			((ConnectionLifeCycleListener)connectionLyfeCycleListeners.get(i)).preDisconnect(con);
		}
	}
	
	/**
	 * Invoke this method to add ConnectionLifeCyleListeners.
	 * 
	 * @param listener
	 */
	public static void add(ConnectionLifeCycleListener listener){
		if (listener != null &&!connectionLyfeCycleListeners.contains(listener))
			connectionLyfeCycleListeners.add(listener);
	}
	

}

