package nl.ibs.jsql;



import java.sql.Connection;

/**
 * @author nlhuucle
 *
 */
public interface ConnectionLifeCycleListener {

	/**
	 * @param con
	 */
	void postConnect(Connection con) throws Exception;

	/**
	 * @param con
	 */
	void preDisconnect(Connection con)throws Exception;;

}

