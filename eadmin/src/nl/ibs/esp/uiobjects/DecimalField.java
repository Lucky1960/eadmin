/*
 * Created on 7-nov-03
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package nl.ibs.esp.uiobjects;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map;

import nl.ibs.esp.event.EventTypes;
import nl.ibs.esp.servlet.ESPTranslationContext;
import nl.ibs.esp.servlet.ESPUserContext;
import nl.ibs.esp.util.TranslationHelper;
import nl.ibs.vegas.meta.FieldMeta;

/**
 * @author c_hc01
 *
 */
public class DecimalField extends Field {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8740254153983224343L;
	private static final BigDecimal BIGDECIMAL_MINUS_ONE = new BigDecimal("-1");

	public DecimalField(FieldMeta meta) {
		super(meta.getTextKey(), Field.TYPE_DECIMAL, meta.getName());
		super.setCommonMetaProperties(meta);
		int displayLength = Math.min(50, meta.getFieldLength() + 1 + (meta.getFieldLength() - 1) / 3 + (meta.getFractionalDigits() > 0 ? 1 : 0));
		setLength(displayLength);
		setMaxLength(displayLength);
		String defaultV = meta.getDefaultValue();
		if (defaultV != null && defaultV.trim().length() > 0)
			setValueAsString(defaultV);
		addValidators(meta.getInputValidators());
		setMaxIntegerDigits(meta.getFieldLength() - meta.getFractionalDigits());
		setMaxFractionDigits(meta.getFractionalDigits());
		setMinFractionDigits(meta.getFractionalDigits());
	}

	private DecimalFormat format;
	private Locale formatLocale;
	private BigDecimal maxValue;
	private boolean includeValueUpperBoundery;
	private BigDecimal minValue;
	private boolean includeValueLowerBoundery;
	protected int maxIntegerDigits = 308;
	protected int maxFractionDigits = 340;
	protected int minFractionDigits = -1;
	private boolean inUpdate;
	private boolean nullable;

	/**
	 * Constructor for DecimalField.
	 * 
	 * @param label
	 */
	public DecimalField(String label){
		super(label, Field.TYPE_DECIMAL);
	}
	
	/**
	 * Constructor for DecimalField.
	 * 
	 * @param label
	 * @param value
	 * @param length
	 */
	public DecimalField(String label, double value, String length) {
		super(label, Field.TYPE_DECIMAL);
		setLength(Integer.parseInt(length));
		setValue(getFormat().format(value));
	}

	public DecimalField(String label, int length, int fractionDigits, boolean nullable) {
		super(label, Field.TYPE_DECIMAL);
		setLength(length);
		setMaxLength(length);
		minFractionDigits = fractionDigits;
		maxFractionDigits = fractionDigits;
		maxIntegerDigits = length - fractionDigits;
		this.nullable = nullable;
	}

	/**
	 * Constructor for DecimalField.
	 * 
	 * @param label
	 * @param value
	 * @param length
	 */
	public DecimalField(String label, Object value, String length) {
		super(label, Field.TYPE_DECIMAL);
		setLength(Integer.parseInt(length));
		setValue(getFormat().format(value));
	}

	public void setMaxIntegerDigits(int maxDigits) {
		maxIntegerDigits = maxDigits;
		format = null;
	}

	public void setMaxFractionDigits(int maxDigits) {
		maxFractionDigits = maxDigits;
		format = null;
	}

	public void setMinFractionDigits(int minDigits) {
		minFractionDigits = minDigits;
		format = null;
	}

	public int getMaxIntegerDigits() {
		return maxIntegerDigits;
	}

	public int getMaxFractionDigits() {
		return maxFractionDigits;
	}

	public int getMinFractionDigits() {
		return minFractionDigits;
	}

	public void setValue(double value) {
		setValue(getFormat().format(value));
	}

	public void setValue(Float value) {
		setValue(getFormat().format(value));
	}

	public void setValue(Double value) {
		setValue(getFormat().format(value));
	}

	public void setValue(BigDecimal value) {
		if (nullable && (value == null || value.doubleValue() == 0)) {
			setValue();
			return;
		}
		setValue(getFormat().format(value));
	}

	public float getFloatValue() throws Exception {
		return isEmpty() ? 0f : parse(getValue()).floatValue();
	}

	public double getDoubleValue() throws Exception {
		return isEmpty() ? 0d : getBigDecimal().doubleValue();
	}

	public Float getFloat() throws Exception {
		return isEmpty() ? null : new Float(parse(getValue()).toString());
	}

	public Double getDouble() throws Exception {
		return isEmpty() ? null : new Double(parse(getValue()).toString());
	}

	public BigDecimal getBigDecimal() throws Exception {
		return isEmpty() ? null : new BigDecimal(parse(getValue()).toString());
	}

	public void setMaxValue(BigDecimal max, boolean includeUpperBoundery) {
		maxValue = max;
		includeValueUpperBoundery = includeUpperBoundery;
	}

	public void setMaxValue(double max, boolean includeUpperBoundery) {
		maxValue = new BigDecimal(String.valueOf(max));
		includeValueUpperBoundery = includeUpperBoundery;
	}

	public void setMinValue(BigDecimal min, boolean includeLowerBoundery) {
		minValue = min;
		includeValueLowerBoundery = includeLowerBoundery;
	}

	public void setMinValue(double min, boolean includeLowerBoundery) {
		minValue = new BigDecimal(String.valueOf(min));
		includeValueLowerBoundery = includeLowerBoundery;
	}

	public void validate() throws Exception {
		super.validate();
		try {
			BigDecimal bdv = getBigDecimal();
			if (bdv == null)
				return;
			if (minValue != null) {
				int eq = minValue.compareTo(bdv);
				if (eq > 0 || (eq == 0 && !includeValueLowerBoundery)) {
					setInvalidTag();
					TranslationHelper th = ESPTranslationContext.getTranslationHelper();
					if (includeValueLowerBoundery)
						throw new UserMessageException(th.translate("tmp-number-not-greater-or-equal-then", new Object[] { getTranslatedAndQuotedLabel(), format.format(minValue) }, false));
					else
						throw new UserMessageException(th.translate("tmp-number-not-greater-then", new Object[] { getTranslatedAndQuotedLabel(), format.format(minValue) }, false));
				}
			}
			if (maxValue != null) {
				int eq = maxValue.compareTo(bdv);
				if (eq < 0 || (eq == 0 && !includeValueUpperBoundery)) {
					setInvalidTag();
					TranslationHelper th = ESPTranslationContext.getTranslationHelper();
					if (includeValueUpperBoundery)
						throw new UserErrorMessage(th.translate("tmp-number-not-smaller-or-equal-then", new Object[] { getTranslatedAndQuotedLabel(), format.format(maxValue) }, false));
					else
						throw new UserErrorMessage(th.translate("tmp-number-not-smaller-then", new Object[] { getTranslatedAndQuotedLabel(), format.format(maxValue) }, false));
				}
			}
			if (maxIntegerDigits > 0) {
				BigDecimal max = new BigDecimal(Math.pow(10, maxIntegerDigits));
				int eq = max.compareTo(bdv);
				if (eq <= 0) {
					setInvalidTag();
					TranslationHelper th = ESPTranslationContext.getTranslationHelper();
					throw new UserErrorMessage(th.translate("tmp-number-not-smaller-then", new Object[] { getTranslatedAndQuotedLabel(), getFormat().format(max) }, false));
				}
				BigDecimal min = max.multiply(BIGDECIMAL_MINUS_ONE);
				eq = min.compareTo(bdv);
				if (eq >= 0) {
					setInvalidTag();
					TranslationHelper th = ESPTranslationContext.getTranslationHelper();
					throw new UserErrorMessage(th.translate("tmp-number-not-greater-then", new Object[] { getTranslatedAndQuotedLabel(), getFormat().format(min) }, false));
				}
			}

		} catch (ParseException e) {
			setInvalidTag();
			throw new UserErrorMessage(ESPTranslationContext.getTranslationHelper().translate("tmp-number-not-valid",
					new String[] { getTranslatedAndQuotedLabel(), getValue(), getFormat().toPattern() }, false));
		}
	}

	@SuppressWarnings("rawtypes")
	public void updateField(Map parameterMap) {
		try {
			inUpdate = true;
			super.updateField(parameterMap);
		} finally {
			inUpdate = false;
		}
	}

	public Field setValue(String value) {
		String oldValue = getValue();
		if (value == null || value.trim().length() == 0) {
			if (nullable) {
				super.setValue();
				return this;
			} else {
				value = "0";
			}
		} else {
			value = value.trim();
		}
		if (value.equals(getValue()) == false) {
			boolean doFormat = !inUpdate || !hasEventListeners()
					|| (!hasListenersForEvent(EventTypes.ON_KEY_DOWN) && !hasListenersForEvent(EventTypes.ON_KEY_UP) && !hasListenersForEvent(EventTypes.ON_KEY_PRESS));

			if (doFormat && getFormat() != null) {
				try {
					if (getFormat().getDecimalFormatSymbols().getDecimalSeparator() == ',' && value.lastIndexOf('.') > value.lastIndexOf(',')) {
						value = value.replace('.', ',');
					}
					value = getFormat().format(parse(value));
				} catch (Exception e) {
				}
			}

			super.setValue(value);
			if (inUpdate && oldValue.equals(getValue()))
				resetModified();

		}
		return this;
	}

	public Number parse(String string) throws Exception {
		return getFormat().parse(string);
	}

	protected DecimalFormat getFormat() {
		if (format == null || formatLocale == null || !formatLocale.equals(ESPUserContext.getLocale())) {
			formatLocale = ESPUserContext.getLocale();
			format = getFormat(formatLocale);
			if (minFractionDigits >= 0)
				format.setMinimumFractionDigits(minFractionDigits);
			if (maxFractionDigits >= 0)
				format.setMaximumFractionDigits(maxFractionDigits);
			if (maxIntegerDigits >= 0)
				format.setMaximumIntegerDigits(maxIntegerDigits);
		}
		return format;
	}

	protected DecimalFormat getFormat(Locale locale) {
		format = (DecimalFormat) DecimalFormat.getInstance(locale);
		return format;
	}

	public char getDecimalSeperator() {
		return getFormat().getDecimalFormatSymbols().getDecimalSeparator();
	}

	public char getGroupingSeperator() {
		return getFormat().getDecimalFormatSymbols().getGroupingSeparator();
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	/*public static void main(String[] args) {
	 for (int i = 0; i < 400; i++) {
	 System.out.println("Trying " +i) ;
	 BigDecimal max = new BigDecimal(Math.pow(10, i));
	 System.out.println("Success");
	 
	 }
	 }*/
	
	/*public static void main(String[] args) throws ParseException {
		DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(new Locale("nl"));
		format.setMaximumFractionDigits(2);
		String number = format.format(new BigDecimal("20000.229999"));
		System.out.println(number);
		BigDecimal bd = new BigDecimal(format.parse(number).doubleValue());
		bd = bd.setScale(2,BigDecimal.ROUND_HALF_EVEN);
		System.out.println(bd);
	}*/

}
