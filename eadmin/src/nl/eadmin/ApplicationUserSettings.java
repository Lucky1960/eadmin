/**
 * 
 */
package nl.eadmin;

import nl.ibs.jsql.UserSettings;

/**
 * @author nlhuucle
 *
 */
public class ApplicationUserSettings extends UserSettings {
	private static final long serialVersionUID = 1L;

	/**
	 * @param string
	 */
	public ApplicationUserSettings(String user) throws Exception {
		super(ApplicationConstants.APPLICATION,user);
	}

	/**
	 * @param strings
	 */
	public ApplicationUserSettings(String[] users) throws Exception {
		super(ApplicationConstants.APPLICATION,users);
	}

	/**
	 * @param user
	 * @param context
	 * @throws Exception 
	 */
	public ApplicationUserSettings(String user, String context) throws Exception {
		super(ApplicationConstants.APPLICATION,user, context);
	}

}
