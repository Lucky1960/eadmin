package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.util.Collection;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.helpers.JournaalHelper;
import nl.eadmin.selection.DebCredLijstSelection;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class DebCredLijstDataBean {
    public static final String DCNR = "dcNr";
    public static final String DCNAAM = "dcNaam";
    public static final String BEDRAG_D = "bedragDebet";
    public static final String BEDRAG_C = "bedragCredit";
    public static final String SALDO = "saldo";
	private String bedrijf, dc, dcNr = "";
	private String dcNaam = "";
	private BigDecimal ZERO = ApplicationConstants.ZERO;
	private BigDecimal bedragDebet = ZERO, bedragCredit = ZERO;
	private DecimalToAmountTransformer trf = new DecimalToAmountTransformer();
	private Collection<FacturenLijstDataBean> subBeans;
	private DebCredLijstSelection selection;
	private boolean isValid;
	private DBData dbd;

	public DebCredLijstDataBean(Bedrijf bdr, String dc, String dcNr, String rekNrs, DebCredLijstSelection selection) throws Exception {
		this.bedrijf = bdr.getBedrijfscode();
		this.dbd = bdr.getDBData();
		this.dc = dc;
		this.dcNr = dcNr;
		this.selection = selection;
		if ("D".equals(dc)) {
			DebiteurPK key = new DebiteurPK();
			key.setBedrijf(bedrijf);
			key.setDebNr(dcNr);
			Debiteur deb  = DebiteurManagerFactory.getInstance(dbd).findOrCreate(key);
			dcNaam = deb.getNaam() + " - " + deb.getPlaats();
		} else {
			CrediteurPK key = new CrediteurPK();
			key.setBedrijf(bedrijf);
			key.setCredNr(dcNr);
			Crediteur cred  = CrediteurManagerFactory.getInstance(dbd).findOrCreate(key);
			dcNaam = cred.getNaam() + " - " + cred.getPlaats();
		}
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT SUM(BEDRAGDEB), SUM(BEDRAGCRED)");
		sb.append("  FROM Journaal");
		sb.append(" WHERE BDR = '" + bedrijf + "'");
		sb.append("   AND DCNR = '" + dcNr + "'");
		sb.append("   AND " + rekNrs);
		sb.append("   AND DAGBOEK <> 'SYS1'");
		if (selection.getDateFrom() != null) {
			sb.append("   AND BOEKDTM >= '" + DateHelper.getSQLdateString(selection.getDateFrom()) + "'");
		}
		if (selection.getDateTo() != null) {
			sb.append("   AND BOEKDTM <= '" + DateHelper.getSQLdateString(selection.getDateTo()) + "'");
		}
		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) JournaalManagerFactory.getInstance(dbd), sb.toString());
		Object[][] rslt = qry.getResultArray();
		bedragDebet = rslt[0][0] == null ? ZERO : (BigDecimal) rslt[0][0];
		bedragCredit = rslt[0][1] == null ? ZERO : (BigDecimal) rslt[0][1];
		isValid = selection.getIncludeZero() || getSaldo().doubleValue() != 0d;
	}

	public boolean isValid() throws Exception {
		return isValid;
	}

	public Collection<FacturenLijstDataBean> getSubBeans() throws Exception {
		if (subBeans == null) {
			subBeans = new JournaalHelper(dbd).getSubBeans(bedrijf, dc, dcNr, selection);
		}
		return subBeans;
	}

	public String getBedrijf() {
		return bedrijf;
	}

	public String getDc() {
		return dc;
	}

	public String getDcNr() {
		return dcNr;
	}

	public String getDcNaam() {
		return dcNaam;
	}

	public BigDecimal getBedragDebet() {
		return bedragDebet;
	}

	public String getBedragDebet$() {
		return (String) trf.transform(bedragDebet);
	}

	public BigDecimal getBedragCredit() {
		return bedragCredit;
	}

	public String getBedragCredit$() {
		return (String) trf.transform(bedragCredit);
	}

	public BigDecimal getSaldo() {
		return bedragDebet.subtract(bedragCredit);
	}

	public String getSaldo$() {
		return (String) trf.transform(getSaldo());
	}
}
