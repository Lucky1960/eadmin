package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class GrootboekrekeningDatabean {
	private ArrayList<GrootboekmutatieDatabean> mutaties = new ArrayList<GrootboekmutatieDatabean>();
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();
	private Rekening rekening;
	private BigDecimal ZERO = ApplicationConstants.ZERO;
	private BigDecimal totaalDebet = ZERO;
	private BigDecimal totaalCredit = ZERO;

	public GrootboekrekeningDatabean(Rekening rekening, Date dateFrom, Date dateTo) throws Exception {
		this.rekening = rekening;
		DBData dbd = rekening.getDBData();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Journaal WHERE ");
		sb.append(Journaal.BEDRIJF + "='" + rekening.getBedrijf() + "' AND ");
		sb.append(Journaal.REKENING_NR + "='" + rekening.getRekeningNr() + "' AND ");
		sb.append(Journaal.BOEKDATUM + ">='" + DateHelper.getSQLdateString(dateFrom) + "' AND " + Journaal.BOEKDATUM + "<='" + DateHelper.getSQLdateString(dateTo) + "' ");
		sb.append("ORDER BY " + Journaal.BOEKDATUM + "," + Journaal.BOEKSTUK);
		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) JournaalManagerFactory.getInstance(dbd), sb.toString());
		Journaal journaal;
		Iterator<?> mut = qry.getCollection().iterator();
		while (mut.hasNext()) {
			journaal = (Journaal) mut.next();
			totaalDebet = totaalDebet.add(journaal.getBedragDebet());
			totaalCredit = totaalCredit.add(journaal.getBedragCredit());
			mutaties.add(new GrootboekmutatieDatabean(journaal));
		}
	}

	public ArrayList<GrootboekmutatieDatabean> getMutaties() throws Exception {
		return mutaties;
	}

	public String getRekeningnummer() throws Exception {
		return rekening.getRekeningNr();
	}

	public String getRekeningomschrijving() throws Exception {
		return rekening.getOmschrijving();
	}

	public BigDecimal getTotaalDebet() {
		return totaalDebet;
	}

	public String getTotaalDebet$() {
		return (String) amtTransformer.transform(totaalDebet);
	}

	public BigDecimal getTotaalCredit() {
		return totaalCredit;
	}

	public String getTotaalCredit$() {
		return (String) amtTransformer.transform(totaalCredit);
	}

	public BigDecimal getSaldoDebet() {
		if (totaalDebet.doubleValue() >= totaalCredit.doubleValue()) {
			return totaalDebet.subtract(totaalCredit);
		} else {
			return ZERO;
		}
	}
	public BigDecimal getSaldoCredit() {
		if (totaalCredit.doubleValue() >= totaalDebet.doubleValue()) {
			return totaalCredit.subtract(totaalDebet);
		} else {
			return ZERO;
		}
	}
	public String getSaldoDebet$() {
		return (String) amtTransformer.transform(getSaldoDebet());
	}
	public String getSaldoCredit$() {
		return (String) amtTransformer.transform(getSaldoCredit());
	}
}
