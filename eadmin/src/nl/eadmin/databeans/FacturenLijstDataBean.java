package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import nl.eadmin.SessionKeys;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.helpers.JournaalHelper;
import nl.eadmin.selection.DebCredLijstSelection;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.jsql.DBData;

public class FacturenLijstDataBean {
	public static final String DAGBOEK = "dagboek";
	public static final String FACTUURNR = "factuurNr";
	public static final String FACTUUR_DATUM = "factuurDatum";
	public static final String BEDRAG_D = "bedragDebet";
	public static final String BEDRAG_C = "bedragCredit";
	public static final String SALDO = "saldo";
	private String bedrijf, dc, dcNr, dagboek, factuurNr;
	private Date factuurDatum;
	private BigDecimal bedragDebet, bedragCredit;
	private DecimalToAmountTransformer trf = new DecimalToAmountTransformer();
	private Collection<FactuurMutatieDataBean> subBeans;
	private DebCredLijstSelection selection;

	// @formatter:off
	public FacturenLijstDataBean(String bedrijf, String dc, String dcNr, String dagboek, String factuurNr, Date factuurDatum, BigDecimal bedragDebet, BigDecimal bedragCredit, DebCredLijstSelection selection) throws Exception {
	// @formatter:on
		this.bedrijf = bedrijf;
		this.dc = dc;
		this.dcNr = dcNr;
		this.dagboek = dagboek;
		this.factuurNr = factuurNr;
		this.factuurDatum = factuurDatum;
		this.bedragDebet = bedragDebet;
		this.bedragCredit = bedragCredit;
		this.selection = selection;
	}

	public Collection<FactuurMutatieDataBean> getSubBeans() throws Exception {
		if (subBeans == null) {
			DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
			subBeans = new JournaalHelper(dbd).getSubBeans(bedrijf, dc, dcNr, factuurNr, selection);
		}
		return subBeans;
	}

	public String getBedrijf() {
		return bedrijf;
	}

	public String getDc() {
		return dc;
	}

	public String getDcNr() {
		return dcNr;
	}

	public String getDagboek() {
		return dagboek;
	}

	public String getFactuurNr() {
		return factuurNr;
	}

	public Date getFactuurDatum() {
		return factuurDatum;
	}

	public String getFactuurDatum$() throws Exception {
		return DateHelper.dateToYYYYMMDD(factuurDatum, "-");
	}

	public BigDecimal getBedragDebet() {
		return bedragDebet;
	}

	public String getBedragDebet$() {
		return (String) trf.transform(bedragDebet);
	}

	public BigDecimal getBedragCredit() {
		return bedragCredit;
	}

	public String getBedragCredit$() {
		return (String) trf.transform(bedragCredit);
	}

	public BigDecimal getSaldo() {
		return bedragDebet.subtract(bedragCredit);
	}

	public String getSaldo$() {
		return (String) trf.transform(getSaldo());
	}
}
