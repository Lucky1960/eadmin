package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.sql.Date;

import nl.eadmin.ui.transformer.DatumTransformer;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;

public class DebCredKaartMutatieDatabean {
    public static final String _DAGBOEK = "dagboek";
    public static final String _BOEKSTUK = "boekstuk";
    public static final String _BOEKDATUM = "boekDatum";
    public static final String _FACTUUR = "factuur";
    public static final String _OMSCHR = "omschrijving";
    public static final String _DEBET = "debet";
    public static final String _CREDIT = "credit";

	private String dagboek, boekstuk, factuur, omschr;
	private Date boekDatum;
	private BigDecimal amtDebet, amtCredit;
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();
	private DatumTransformer dtmTransformer = new DatumTransformer();

	public DebCredKaartMutatieDatabean(String dagboek, String boekstuk, Date boekDatum, String factuur, String omschr, BigDecimal amtDebet, BigDecimal amtCredit) throws Exception {
		this.dagboek = dagboek;
		this.boekstuk = boekstuk;
		this.boekDatum = boekDatum;
		this.factuur = factuur;
		this.omschr = omschr;
		this.amtDebet = amtDebet;
		this.amtCredit = amtCredit;
	}

	public String getDagboek() throws Exception {
		return dagboek;
	}

	public String getBoekstuk() throws Exception {
		return boekstuk;
	}

	public Date getBoekDatum() throws Exception {
		return boekDatum;
	}

	public String getBoekDatum$() throws Exception {
		return (String) dtmTransformer.transform(boekDatum);
	}

	public String getFactuur() throws Exception {
		return factuur;
	}

	public String getOmschrijving() throws Exception {
		return omschr;
	}

	public BigDecimal getDebet() throws Exception {
		return amtDebet;
	}

	public String getDebet$() throws Exception {
		return (String) amtTransformer.transform(amtDebet);
	}

	public BigDecimal getCredit() throws Exception {
		return amtCredit;
	}

	public String getCredit$() throws Exception {
		return (String) amtTransformer.transform(amtCredit);
	}
}
