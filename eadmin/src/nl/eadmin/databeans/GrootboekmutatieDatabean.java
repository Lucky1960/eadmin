package nl.eadmin.databeans;

import nl.eadmin.db.Journaal;
import nl.eadmin.ui.transformer.DatumTransformer;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;

public class GrootboekmutatieDatabean {
    public final static String BOEKDTM = "datum";
    public final static String DAGBOEK = "dagboek";
    public final static String BOEKSTUK = "boekstuknummer";
    public final static String OMSCHR = "omschrijving";
    public final static String CREDIT = "credit";
    public final static String DEBET = "debet";

	private Journaal journaal;
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();
	private DatumTransformer dtmTransformer = new DatumTransformer();

	public GrootboekmutatieDatabean(Journaal journaal) throws Exception {
		this.journaal = journaal;
	}

	public Journaal getJournaal() throws Exception {
		return journaal;
	}

	public String getDatum() throws Exception {
		return (String) dtmTransformer.transform(journaal.getBoekdatum());
	}

	public String getDagboek() throws Exception {
		return journaal.getDagboekId();
	}

	public String getBoekstuknummer() throws Exception {
		return journaal.getBoekstuk();
	}

	public String getOmschrijving() throws Exception {
		return (journaal.getOmschr1() + " " + journaal.getOmschr2() + " " + journaal.getOmschr3()).replaceAll("  ", " ").trim();
	}

	public String getDebet() throws Exception {
		return (String) amtTransformer.transform(journaal.getBedragDebet());
	}

	public String getCredit() throws Exception {
		return (String) amtTransformer.transform(journaal.getBedragCredit());
	}
}
