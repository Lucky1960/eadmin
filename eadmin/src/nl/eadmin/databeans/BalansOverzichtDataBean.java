package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Rekening;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;

public class BalansOverzichtDataBean {
	private HashMap<String, BalansRekeningSoortDataBean> rekeningSoortBeans = new HashMap<String, BalansRekeningSoortDataBean>();
	private String title, soortForGrandTotal = "";
	private BigDecimal grandTotalDebet, grandTotalCredit;
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();

	public BalansOverzichtDataBean(String title, Bedrijf bedrijf, Collection<?> rekeningen, int boekjaar, boolean excludeEmpty) throws Exception {
		this.title = title;
		this.grandTotalDebet = ApplicationConstants.ZERO;
		this.grandTotalCredit = ApplicationConstants.ZERO;
		Date dateFrom = DateHelper.getStartDate(bedrijf, boekjaar);
		Date dateTo = DateHelper.getEndDate(bedrijf, boekjaar);
		// JournaalHelper.createJournals(bedrijf, dateFrom, dateTo);
		boolean first = true;
		Iterator<?> iter = rekeningen.iterator();
		while (iter.hasNext()) {
			BalansRekeningDataBean bean = new BalansRekeningDataBean((Rekening) iter.next(), dateFrom, dateTo);
			if (first) {
				soortForGrandTotal = bean.getRekSoort();
				first = false;
			}
			if (!excludeEmpty || bean.getRekTotaalDebet().doubleValue() != 0d || bean.getRekTotaalCredit().doubleValue() != 0d) {
				if (bean.getRekSoort().equals(soortForGrandTotal)) {
					grandTotalDebet = grandTotalDebet.add(bean.getRekTotaalDebet());
					grandTotalCredit = grandTotalCredit.add(bean.getRekTotaalCredit());
				}
				add(bean);
			}
		}
	}

	public void add(BalansRekeningDataBean bean) throws Exception {
		String rekSoort = bean.getRekSoort();
		BalansRekeningSoortDataBean rekeningSoortBean = rekeningSoortBeans.get(rekSoort);
		if (rekeningSoortBean == null) {
			rekeningSoortBean = new BalansRekeningSoortDataBean(rekSoort);
			rekeningSoortBeans.put(rekSoort, rekeningSoortBean);
		}
		rekeningSoortBean.addRekening(bean);
	}

	public Object[] getRekeningSoort() {
		return rekeningSoortBeans.values().toArray();
	}

	public String getTitle() {
		return title;
	}

	public String getWinstVerlies() {
		if (soortForGrandTotal.equals(RekeningSoortEnum.BALANS)) {
			return grandTotalDebet.doubleValue() >= grandTotalCredit.doubleValue() ? "Winst" : "Verlies";
		} else {
			return grandTotalDebet.doubleValue() >= grandTotalCredit.doubleValue() ? "Verlies" : "Winst";
		}
	}

	public String getGrandTotalCredit$() {
		return (String) amtTransformer.transform(grandTotalCredit);
	}

	public String getGrandTotalDebet$() {
		return (String) amtTransformer.transform(grandTotalDebet);
	}

	public BigDecimal getGrandTotalSaldo() {
		return grandTotalDebet.subtract(grandTotalCredit);
	}

	public BigDecimal getSaldoDebet() {
		if (grandTotalDebet.doubleValue() >= grandTotalCredit.doubleValue()) {
			return grandTotalDebet.subtract(grandTotalCredit);
		} else {
			return ApplicationConstants.ZERO;
		}
	}

	public BigDecimal getSaldoCredit() {
		if (grandTotalCredit.doubleValue() >= grandTotalDebet.doubleValue()) {
			return grandTotalCredit.subtract(grandTotalDebet);
		} else {
			return ApplicationConstants.ZERO;
		}
	}

	public String getSaldoDebet$() {
		return (String) amtTransformer.transform(getSaldoDebet());
	}

	public String getSaldoCredit$() {
		return (String) amtTransformer.transform(getSaldoCredit());
	}
}
