package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class BalansRekeningDataBean {
    public final static String REKNR = "rekNr";
    public final static String OMSCHR = "rekOmschrijving";
    public final static String CREDIT = "rekTotaalCredit";
    public final static String DEBET = "rekTotaalDebet";

	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();
	private Rekening rekening;
	private Date dateFrom, dateTo;
	private BigDecimal ZERO = ApplicationConstants.ZERO;
	private BigDecimal totaalDebet = ZERO;
	private BigDecimal totaalCredit = ZERO;
	private DBData dbd;

	public BalansRekeningDataBean(Rekening rekening, Date dateFrom, Date dateTo) throws Exception {
		this.dbd = rekening.getDBData();
		this.rekening = rekening;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		Journaal journaal;
		Iterator<?> mut = getJournals().iterator();
		while (mut.hasNext()) {
			journaal = (Journaal) mut.next();
			totaalDebet = totaalDebet.add(journaal.getBedragDebet());
			totaalCredit = totaalCredit.add(journaal.getBedragCredit());
		}
	}

	private Collection<?> getJournals() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Journaal WHERE ");
		sb.append(Journaal.BEDRIJF + "='" + rekening.getBedrijf() + "' AND ");
		sb.append(Journaal.REKENING_NR + "='" + rekening.getRekeningNr() + "' AND ");
		sb.append(Journaal.BOEKDATUM + ">='" + DateHelper.getSQLdateString(dateFrom) + "' AND " + Journaal.BOEKDATUM + "<='" + DateHelper.getSQLdateString(dateTo) + "' ");
		sb.append("ORDER BY " + Journaal.BOEKDATUM);
		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) JournaalManagerFactory.getInstance(dbd), sb.toString());
		return qry.getCollection();
	}

	public String getRekNr() throws Exception {
		return rekening.getRekeningNr();
	}

	public String getRekOmschrijving() throws Exception {
		return rekening.getOmschrijving();
	}

	public String getRekSoort() throws Exception {
		return rekening.getRekeningSoort();
	}

	public String getRekSoort$() throws Exception {
		return RekeningSoortEnum.getValue(rekening.getRekeningSoort());
	}

	public BigDecimal getRekTotaalDebet() {
		return totaalDebet;
	}

	public String getRekTotaalDebet$() {
		return (String) amtTransformer.transform(totaalDebet);
	}

	public BigDecimal getRekTotaalCredit() {
		return totaalCredit;
	}

	public String getRekTotaalCredit$() {
		return (String) amtTransformer.transform(totaalCredit);
	}

	public Collection<GrootboekmutatieDatabean> getGrootboekmutatieBeans() throws Exception {
		Collection<GrootboekmutatieDatabean> beans = new ArrayList<GrootboekmutatieDatabean>();
		Iterator<?> mut = getJournals().iterator();
		while (mut.hasNext()) {
			beans.add(new GrootboekmutatieDatabean((Journaal) mut.next()));
		}
		return beans;
	}
}
