package nl.eadmin.databeans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import nl.eadmin.db.Adres;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.helpers.HtmlTemplateHelper;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.jsql.DBData;
import nl.ibs.oxml.Base64;

public class FactuurDataBean {
	private HeaderData factuur;
	private Bedrijf bedrijf;
	private boolean isInkoopFactuur;
	private ArrayList<FactuurRegelDataBean> factuurRegels = new ArrayList<FactuurRegelDataBean>();
	private String naam, contact, kvkNr, btwNr;
	private Debiteur debiteur;
	private Crediteur crediteur;
	private Adres adres, postAdres;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();
	private HtmlTemplate layout;
	private ArrayList<String> adresRegel;
	private DBData dbd;

	public FactuurDataBean(HeaderData factuur) throws Exception {
		this(factuur, null);
	}

	public FactuurDataBean(HeaderData factuur, HtmlTemplate layout) throws Exception {
		this.dbd = factuur.getDBData();
		this.factuur = factuur;
		this.bedrijf = BedrijfManagerFactory.getInstance(dbd).findOrCreate(factuur.getBedrijf());
		if (layout == null) {
			this.layout = HtmlTemplateHelper.createStdHtmlTemplate(bedrijf);
		} else {
			this.layout = layout;
		}
		this.adres = bedrijf.getPostAdres();
		if (adres.isEmpty()) {
			this.adres = bedrijf.getBezoekAdres();
		}
		this.factuurRegels = new ArrayList<FactuurRegelDataBean>();
		this.isInkoopFactuur = factuur.getDagboekObject().isInkoopboek();
		Iterator<?> details = factuur.getDetails().iterator();
		while (details.hasNext()) {
			factuurRegels.add(new FactuurRegelDataBean((DetailData) details.next()));
		}
		if (isInkoopFactuur) {
			CrediteurPK key = new CrediteurPK();
			key.setBedrijf(factuur.getBedrijf());
			key.setCredNr(factuur.getDcNummer());
			crediteur = CrediteurManagerFactory.getInstance(dbd).findOrCreate(key);
			naam = crediteur.getNaam();
			contact = crediteur.getContactPersoon();
			postAdres = crediteur.getAdres(true);
			kvkNr = crediteur.getKvkNr();
			btwNr = crediteur.getBtwNr();
		} else {
			DebiteurPK key = new DebiteurPK();
			key.setBedrijf(factuur.getBedrijf());
			key.setDebNr(factuur.getDcNummer());
			debiteur = DebiteurManagerFactory.getInstance(dbd).findOrCreate(key);
			naam = debiteur.getNaam();
			contact = debiteur.getContactPersoon();
			postAdres = debiteur.getAdres(true);
			kvkNr = debiteur.getKvkNr();
			btwNr = debiteur.getBtwNr();
		}
		adresRegel = new ArrayList<String>();
		adresRegel.add(naam);
		adresRegel.add(contact);
		adresRegel.add(postAdres.getAdresRegel1());
		adresRegel.add(postAdres.getAdresRegel2());
		adresRegel.add(postAdres.getAdresRegel3());
		for (int r = 0; r < adresRegel.size(); r++) {
			if (adresRegel.get(r) == null || adresRegel.get(r).trim().length() == 0) {
				adresRegel.remove(r--);
			}
		}
	}

	public String getBase64BedrijfsLogo() throws Exception {
		byte[] bytes = layout.getLogo();
		if (bytes != null && bytes.length > 0) {
			char[] chars = Base64.encode(bytes);
			return new String(chars);
		} else {
			return "";
		}
	}

	public String getLogoHoogte() throws Exception {
		int hoogte = layout.getLogoHeight(); 
		return "" + (hoogte == 0 ? 60 : layout.getLogoHeight());
	}

	public String getFactuurOmschrijving() throws Exception {
		return factuur.getOmschr1() + " " + factuur.getOmschr2();
	}

	public String getFactuurOmschrijving1() throws Exception {
		return factuur.getOmschr1();
	}

	public String getFactuurOmschrijving2() throws Exception {
		return factuur.getOmschr2();
	}

	public String getBedrijfsNaam() throws Exception {
		return bedrijf.getOmschrijving();
	}

	public String getBedrijfsAdresRegel1() throws Exception {
		return bedrijf.getBezoekAdres().getAdresRegel1();
	}

	public String getBedrijfsAdresRegel2() throws Exception {
		return bedrijf.getBezoekAdres().getAdresRegel2();
	}

	public String getBedrijfsAdresRegel3() throws Exception {
		return bedrijf.getBezoekAdres().getAdresRegel3();
	}

	public String getBedrijfsPostCode() throws Exception {
		return "";
	}

	public String getBedrijfsPlaats() throws Exception {
		return "";
	}

	public String getBedrijfsTelefoon() throws Exception {
		return bedrijf.getBezoekAdres().getTelefoon();
	}

	public String getBedrijfsEmail() throws Exception {
		return bedrijf.getEmail();
	}

	public String getBedrijfsKvkNr() throws Exception {
		return bedrijf.getKvkNr();
	}

	public String getBedrijfsBtwNr() throws Exception {
		return bedrijf.getBtwNr();
	}

	public String getBedrijfsIbanNr() throws Exception {
		return bedrijf.getIbanNr();
	}

	public String getBedrijfsBicNr() throws Exception {
		return bedrijf.getBic();
	}

	public byte[] getFactuurTemplate() throws Exception {
		return layout.getHtmlString().getBytes();
	}

	public String getFactuurNr() throws Exception {
		return factuur.getFactuurNummer();
	}

	public String getFactuurDatum() throws Exception {
		return sdf.format(factuur.getFactuurdatum());
	}

	public String getVervalDatum() throws Exception {
		return sdf.format(factuur.getVervaldatum());
	}

	public String getDebiteurNr() throws Exception {
		return factuur.getDcNummer();
	}

	public String getCrediteurenNr() throws Exception {
		return factuur.getDcNummer();
	}

	public String getNaam() throws Exception {
		return naam;
	}

	public String getContactPersoon() throws Exception {
		return contact;
	}

	public String getAdresRegel1() throws Exception {
		return getAdresRegel(1);
	}

	public String getAdresRegel2() throws Exception {
		return getAdresRegel(2);
	}

	public String getAdresRegel3() throws Exception {
		return getAdresRegel(3);
	}

	public String getAdresRegel4() throws Exception {
		return getAdresRegel(4);
	}

	public String getAdresRegel5() throws Exception {
		return getAdresRegel(5);
	}

	private String getAdresRegel(int r) throws Exception {
		if (r <= adresRegel.size()) {
			return adresRegel.get(r - 1);
		} else {
			return "";
		}
	}

	public String getPostAdresRegel1() throws Exception {
		return postAdres.getAdresRegel1();
	}

	public String getPostAdresRegel2() throws Exception {
		return postAdres.getAdresRegel2();
	}

	public String getPostAdresRegel3() throws Exception {
		return postAdres.getAdresRegel3();
	}

	public String getPostPostcode() throws Exception {
		return "";
	}

	public String getPostPlaats() throws Exception {
		return "";
	}

	public String getKvkNr() throws Exception {
		return kvkNr;
	}

	public String getBtwNr() throws Exception {
		return btwNr;
	}

	public String getTotaalExcl() throws Exception {
		return (String) amtTransformer.transform(factuur.getTotaalExcl());
	}

	public String getTotaalBtw() throws Exception {
		return (String) amtTransformer.transform(factuur.getTotaalBtw());
	}

	public String getTotaalIncl() throws Exception {
		return (String) amtTransformer.transform(factuur.getTotaalIncl());
	}

	public ArrayList<FactuurRegelDataBean> getFactuurRegels() {
		return factuurRegels;
	}
}
