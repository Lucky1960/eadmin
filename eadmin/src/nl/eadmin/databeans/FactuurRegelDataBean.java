package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.BtwCodePK;
import nl.eadmin.db.DetailData;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;

public class FactuurRegelDataBean {
	private DetailData detailData;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();
	private BtwCode btwCode;

	public FactuurRegelDataBean(DetailData detailData) throws Exception {
		this.detailData = detailData;
		BtwCodePK key = new BtwCodePK();
		key.setBedrijf(detailData.getBedrijf());
		key.setCode(detailData.getBtwCode());
		btwCode = BtwCodeManagerFactory.getInstance(detailData.getDBData()).findOrCreate(key);
	}

	public String getDatumLevering() throws Exception {
		Date date = detailData.getDatumLevering();
		if (date == null) {
			return "";
		} else {
			return sdf.format(date);
		}
	}

	public String getAantal() throws Exception {
		return "" + detailData.getAantal();
	}

	public String getEenheid() throws Exception {
		return detailData.getEenheid();
	}

	public String getOmschrijving() throws Exception {
		StringBuilder omschrijving = new StringBuilder();
		String aflDatum = detailData.getDatumLevering() == null ? null : sdf.format(detailData.getDatumLevering());
		if (aflDatum != null && aflDatum.length() > 0) {
			omschrijving.append(aflDatum);
			omschrijving.append(": ");
		}
		omschrijving.append(detailData.getOmschr1());
		if (detailData.getOmschr2().length() > 0) {
			omschrijving.append(" ");
			omschrijving.append(detailData.getOmschr2());
		}
		if (detailData.getOmschr3().length() > 0) {
			omschrijving.append(" ");
			omschrijving.append(detailData.getOmschr3());
		}
		return omschrijving.toString();
	}

	public String getBtwPerc() throws Exception {
		BigDecimal percentage = btwCode.getPercentage();
		if (percentage.doubleValue() == 0d) {
			return "";
		} else {
			return "" + amtTransformer.transform(percentage);
		}
	}

	public String getPrijs() throws Exception {
		BigDecimal prijs = detailData.getPrijs();
		if (btwCode.getInEx().equals("I") && btwCode.getPercentage().doubleValue() != 0d) { // Toon prijs excl. BTW
			prijs = detailData.getBedragExcl().divide(detailData.getAantal(), BigDecimal.ROUND_HALF_UP);
		}
		return (String) amtTransformer.transform(prijs);
	}

	public String getBedragBtw() throws Exception {
		return (String) amtTransformer.transform(detailData.getBedragBtw());
	}

	public String getBedragExcl() throws Exception {
		return (String) amtTransformer.transform(detailData.getBedragExcl());
	}

	public String getBedragIncl() throws Exception {
		return (String) amtTransformer.transform(detailData.getBedragIncl());
	}

}
