package nl.eadmin.databeans;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.enums.VrijeRubriekSoortEnum;

public class VrijeRubriekDefinitie {
	private String dataType, prompt, tabelNaam;

	public VrijeRubriekDefinitie(String definitie) {
		if (definitie == null || definitie.trim().length() == 0) {
			definitie = VrijeRubriekSoortEnum.NOT_IN_USE;
		}
		String[] def = definitie.split(ApplicationConstants.SEPARATOR);
		dataType = def[0];
		if (def.length > 1)
			prompt = def[1];
		else
			prompt = "";
		if (def.length > 2)
			tabelNaam = def[2];
		else
			tabelNaam = "";
	}

	public String getDataType() throws Exception {
		return dataType;
	}

	public String getPrompt() throws Exception {
		return prompt;
	}

	public String getTabelNaam() throws Exception {
		return tabelNaam;
	}
}
