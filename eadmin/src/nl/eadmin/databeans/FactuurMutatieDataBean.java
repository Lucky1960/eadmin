package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;

public class FactuurMutatieDataBean {
    public static final String BOEKDATUM = "boekDatum";
    public static final String DAGBOEK = "dagboek";
    public static final String BOEKSTUK = "boekstuk";
    public static final String OMSCHR1 = "omschr1";
    public static final String OMSCHR2 = "omschr2";
    public static final String OMSCHR3 = "omschr3";
    public static final String BEDRAG_D = "bedragDebet";
    public static final String BEDRAG_C = "bedragCredit";
	private String dagboek, boekstuk, omschr1, omschr2, omschr3;
	private Date boekdatum;
	private BigDecimal ZERO = ApplicationConstants.ZERO;
	private BigDecimal bedragDebet = ZERO, bedragCredit = ZERO;
	private DecimalToAmountTransformer trf = new DecimalToAmountTransformer();

	public FactuurMutatieDataBean(Date boekdatum, String dagboek, String boekstuk, String omschr1, String omschr2, String omschr3, BigDecimal bedragDebet, BigDecimal bedragCredit) throws Exception {
		this.boekdatum = boekdatum;
		this.dagboek = dagboek;
		this.boekstuk = boekstuk;
		this.omschr1 = omschr1;
		this.omschr2 = omschr2;
		this.omschr3 = omschr3;
		this.bedragDebet = bedragDebet;
		this.bedragCredit = bedragCredit;
	}

	public Date getBoekDatum() {
		return boekdatum;
	}

	public String getBoekDatum$() throws Exception {
		return DateHelper.dateToYYYYMMDD(boekdatum, "-");
	}

	public String getDagboek() {
		return dagboek;
	}

	public String getBoekstuk() {
		return boekstuk;
	}

	public String getOmschr1() {
		return omschr1;
	}

	public String getOmschr2() {
		return omschr2;
	}

	public String getOmschr3() {
		return omschr3;
	}

	public BigDecimal getBedragDebet() {
		return bedragDebet;
	}

	public String getBedragDebet$() {
		return (String) trf.transform(bedragDebet);
	}

	public BigDecimal getBedragCredit() {
		return bedragCredit;
	}

	public String getBedragCredit$() {
		return (String) trf.transform(bedragCredit);
	}
}
