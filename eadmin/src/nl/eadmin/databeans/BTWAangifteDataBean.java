package nl.eadmin.databeans;

public class BTWAangifteDataBean {
	String title;
	
	String rubriek_1a_Omzet;
	String rubriek_1a_BTW;
	String rubriek_1b_Omzet;
	String rubriek_1b_BTW;
	String rubriek_1c_Omzet;
	String rubriek_1c_BTW;
	String rubriek_1d_Omzet;
	String rubriek_1d_BTW;
	String rubriek_1e_Omzet;
	
	String rubriek_2a_Omzet;
	String rubriek_2a_BTW;
	
	String rubriek_3a_Omzet;
	String rubriek_3b_Omzet;
	String rubriek_3c_Omzet;
	
	String rubriek_4a_Omzet;
	String rubriek_4a_BTW;
	String rubriek_4b_Omzet;
	String rubriek_4b_BTW;
	
	String rubriek_5a_BTW;
	String rubriek_5b_BTW;
	String rubriek_5c_BTW;
	String rubriek_5d_BTW;
	String rubriek_5e_BTW;
	String rubriek_5f_BTW;
	String rubriek_Totaal_Te_betalen_BTW;
	String rubriek_Totaal_Terug_te_vragen_BTW;
	
	String rubriek_6a_BTW;
	String rubriek_6b_BTW;
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the rubriek_1a_Omzet
	 */
	public String getRubriek_1a_Omzet() {
		return rubriek_1a_Omzet;
	}
	/**
	 * @param rubriek_1a_Omzet the rubriek_1a_Omzet to set
	 */
	public void setRubriek_1a_Omzet(String rubriek_1a_Omzet) {
		this.rubriek_1a_Omzet = rubriek_1a_Omzet;
	}
	/**
	 * @return the rubriek_1a_BTW
	 */
	public String getRubriek_1a_BTW() {
		return rubriek_1a_BTW;
	}
	/**
	 * @param rubriek_1a_BTW the rubriek_1a_BTW to set
	 */
	public void setRubriek_1a_BTW(String rubriek_1a_BTW) {
		this.rubriek_1a_BTW = rubriek_1a_BTW;
	}
	/**
	 * @return the rubriek_1b_Omzet
	 */
	public String getRubriek_1b_Omzet() {
		return rubriek_1b_Omzet;
	}
	/**
	 * @param rubriek_1b_Omzet the rubriek_1b_Omzet to set
	 */
	public void setRubriek_1b_Omzet(String rubriek_1b_Omzet) {
		this.rubriek_1b_Omzet = rubriek_1b_Omzet;
	}
	/**
	 * @return the rubriek_1b_BTW
	 */
	public String getRubriek_1b_BTW() {
		return rubriek_1b_BTW;
	}
	/**
	 * @param rubriek_1b_BTW the rubriek_1b_BTW to set
	 */
	public void setRubriek_1b_BTW(String rubriek_1b_BTW) {
		this.rubriek_1b_BTW = rubriek_1b_BTW;
	}
	/**
	 * @return the rubriek_1c_Omzet
	 */
	public String getRubriek_1c_Omzet() {
		return rubriek_1c_Omzet;
	}
	/**
	 * @param rubriek_1c_Omzet the rubriek_1c_Omzet to set
	 */
	public void setRubriek_1c_Omzet(String rubriek_1c_Omzet) {
		this.rubriek_1c_Omzet = rubriek_1c_Omzet;
	}
	/**
	 * @return the rubriek_1c_BTW
	 */
	public String getRubriek_1c_BTW() {
		return rubriek_1c_BTW;
	}
	/**
	 * @param rubriek_1c_BTW the rubriek_1c_BTW to set
	 */
	public void setRubriek_1c_BTW(String rubriek_1c_BTW) {
		this.rubriek_1c_BTW = rubriek_1c_BTW;
	}
	/**
	 * @return the rubriek_1d_Omzet
	 */
	public String getRubriek_1d_Omzet() {
		return rubriek_1d_Omzet;
	}
	/**
	 * @param rubriek_1d_Omzet the rubriek_1d_Omzet to set
	 */
	public void setRubriek_1d_Omzet(String rubriek_1d_Omzet) {
		this.rubriek_1d_Omzet = rubriek_1d_Omzet;
	}
	/**
	 * @return the rubriek_1d_BTW
	 */
	public String getRubriek_1d_BTW() {
		return rubriek_1d_BTW;
	}
	/**
	 * @param rubriek_1d_BTW the rubriek_1d_BTW to set
	 */
	public void setRubriek_1d_BTW(String rubriek_1d_BTW) {
		this.rubriek_1d_BTW = rubriek_1d_BTW;
	}
	/**
	 * @return the rubriek_1e_Omzet
	 */
	public String getRubriek_1e_Omzet() {
		return rubriek_1e_Omzet;
	}
	/**
	 * @param rubriek_1e_Omzet the rubriek_1e_Omzet to set
	 */
	public void setRubriek_1e_Omzet(String rubriek_1e_Omzet) {
		this.rubriek_1e_Omzet = rubriek_1e_Omzet;
	}
	/**
	 * @return the rubriek_2a_Omzet
	 */
	public String getRubriek_2a_Omzet() {
		return rubriek_2a_Omzet;
	}
	/**
	 * @param rubriek_2a_Omzet the rubriek_2a_Omzet to set
	 */
	public void setRubriek_2a_Omzet(String rubriek_2a_Omzet) {
		this.rubriek_2a_Omzet = rubriek_2a_Omzet;
	}
	/**
	 * @return the rubriek_2a_BTW
	 */
	public String getRubriek_2a_BTW() {
		return rubriek_2a_BTW;
	}
	/**
	 * @param rubriek_2a_BTW the rubriek_2a_BTW to set
	 */
	public void setRubriek_2a_BTW(String rubriek_2a_BTW) {
		this.rubriek_2a_BTW = rubriek_2a_BTW;
	}
	/**
	 * @return the rubriek_3a_Omzet
	 */
	public String getRubriek_3a_Omzet() {
		return rubriek_3a_Omzet;
	}
	/**
	 * @param rubriek_3a_Omzet the rubriek_3a_Omzet to set
	 */
	public void setRubriek_3a_Omzet(String rubriek_3a_Omzet) {
		this.rubriek_3a_Omzet = rubriek_3a_Omzet;
	}
	/**
	 * @return the rubriek_3b_Omzet
	 */
	public String getRubriek_3b_Omzet() {
		return rubriek_3b_Omzet;
	}
	/**
	 * @param rubriek_3b_Omzet the rubriek_3b_Omzet to set
	 */
	public void setRubriek_3b_Omzet(String rubriek_3b_Omzet) {
		this.rubriek_3b_Omzet = rubriek_3b_Omzet;
	}
	/**
	 * @return the rubriek_3c_Omzet
	 */
	public String getRubriek_3c_Omzet() {
		return rubriek_3c_Omzet;
	}
	/**
	 * @param rubriek_3c_Omzet the rubriek_3c_Omzet to set
	 */
	public void setRubriek_3c_Omzet(String rubriek_3c_Omzet) {
		this.rubriek_3c_Omzet = rubriek_3c_Omzet;
	}
	/**
	 * @return the rubriek_4a_Omzet
	 */
	public String getRubriek_4a_Omzet() {
		return rubriek_4a_Omzet;
	}
	/**
	 * @param rubriek_4a_Omzet the rubriek_4a_Omzet to set
	 */
	public void setRubriek_4a_Omzet(String rubriek_4a_Omzet) {
		this.rubriek_4a_Omzet = rubriek_4a_Omzet;
	}
	/**
	 * @return the rubriek_4a_BTW
	 */
	public String getRubriek_4a_BTW() {
		return rubriek_4a_BTW;
	}
	/**
	 * @param rubriek_4a_BTW the rubriek_4a_BTW to set
	 */
	public void setRubriek_4a_BTW(String rubriek_4a_BTW) {
		this.rubriek_4a_BTW = rubriek_4a_BTW;
	}
	/**
	 * @return the rubriek_4b_Omzet
	 */
	public String getRubriek_4b_Omzet() {
		return rubriek_4b_Omzet;
	}
	/**
	 * @param rubriek_4b_Omzet the rubriek_4b_Omzet to set
	 */
	public void setRubriek_4b_Omzet(String rubriek_4b_Omzet) {
		this.rubriek_4b_Omzet = rubriek_4b_Omzet;
	}
	/**
	 * @return the rubriek_4b_BTW
	 */
	public String getRubriek_4b_BTW() {
		return rubriek_4b_BTW;
	}
	/**
	 * @param rubriek_4b_BTW the rubriek_4b_BTW to set
	 */
	public void setRubriek_4b_BTW(String rubriek_4b_BTW) {
		this.rubriek_4b_BTW = rubriek_4b_BTW;
	}
	/**
	 * @return the rubriek_5a_BTW
	 */
	public String getRubriek_5a_BTW() {
		return rubriek_5a_BTW;
	}
	/**
	 * @param rubriek_5a_BTW the rubriek_5a_BTW to set
	 */
	public void setRubriek_5a_BTW(String rubriek_5a_BTW) {
		this.rubriek_5a_BTW = rubriek_5a_BTW;
	}
	/**
	 * @return the rubriek_5b_BTW
	 */
	public String getRubriek_5b_BTW() {
		return rubriek_5b_BTW;
	}
	/**
	 * @param rubriek_5b_BTW the rubriek_5b_BTW to set
	 */
	public void setRubriek_5b_BTW(String rubriek_5b_BTW) {
		this.rubriek_5b_BTW = rubriek_5b_BTW;
	}
	/**
	 * @return the rubriek_5c_BTW
	 */
	public String getRubriek_5c_BTW() {
		return rubriek_5c_BTW;
	}
	/**
	 * @param rubriek_5c_BTW the rubriek_5c_BTW to set
	 */
	public void setRubriek_5c_BTW(String rubriek_5c_BTW) {
		this.rubriek_5c_BTW = rubriek_5c_BTW;
	}
	/**
	 * @return the rubriek_5d_BTW
	 */
	public String getRubriek_5d_BTW() {
		return rubriek_5d_BTW;
	}
	/**
	 * @param rubriek_5d_BTW the rubriek_5d_BTW to set
	 */
	public void setRubriek_5d_BTW(String rubriek_5d_BTW) {
		this.rubriek_5d_BTW = rubriek_5d_BTW;
	}
	/**
	 * @return the rubriek_5e_BTW
	 */
	public String getRubriek_5e_BTW() {
		return rubriek_5e_BTW;
	}
	/**
	 * @param rubriek_5e_BTW the rubriek_5e_BTW to set
	 */
	public void setRubriek_5e_BTW(String rubriek_5e_BTW) {
		this.rubriek_5e_BTW = rubriek_5e_BTW;
	}
	/**
	 * @return the rubriek_5f_BTW
	 */
	public String getRubriek_5f_BTW() {
		return rubriek_5f_BTW;
	}
	/**
	 * @param rubriek_5f_BTW the rubriek_5f_BTW to set
	 */
	public void setRubriek_5f_BTW(String rubriek_5f_BTW) {
		this.rubriek_5f_BTW = rubriek_5f_BTW;
	}
	/**
	 * @return the rubriek_Totaal_Te_betalen_BTW
	 */
	public String getRubriek_Totaal_Te_betalen_BTW() {
		return rubriek_Totaal_Te_betalen_BTW;
	}
	/**
	 * @param rubriek_Totaal_Te_betalen_BTW the rubriek_Totaal_Te_betalen_BTW to set
	 */
	public void setRubriek_Totaal_Te_betalen_BTW(
			String rubriek_Totaal_Te_betalen_BTW) {
		this.rubriek_Totaal_Te_betalen_BTW = rubriek_Totaal_Te_betalen_BTW;
	}
	/**
	 * @return the rubriek_Totaal_Terug_te_vragen_BTW
	 */
	public String getRubriek_Totaal_Terug_te_vragen_BTW() {
		return rubriek_Totaal_Terug_te_vragen_BTW;
	}
	/**
	 * @param rubriek_Totaal_Terug_te_vragen_BTW the rubriek_Totaal_Terug_te_vragen_BTW to set
	 */
	public void setRubriek_Totaal_Terug_te_vragen_BTW(
			String rubriek_Totaal_Terug_te_vragen_BTW) {
		this.rubriek_Totaal_Terug_te_vragen_BTW = rubriek_Totaal_Terug_te_vragen_BTW;
	}
	/**
	 * @return the rubriek_6a_BTW
	 */
	public String getRubriek_6a_BTW() {
		return rubriek_6a_BTW;
	}
	/**
	 * @param rubriek_6a_BTW the rubriek_6a_BTW to set
	 */
	public void setRubriek_6a_BTW(String rubriek_6a_BTW) {
		this.rubriek_6a_BTW = rubriek_6a_BTW;
	}
	/**
	 * @return the rubriek_6b_BTW
	 */
	public String getRubriek_6b_BTW() {
		return rubriek_6b_BTW;
	}
	/**
	 * @param rubriek_6b_BTW the rubriek_6b_BTW to set
	 */
	public void setRubriek_6b_BTW(String rubriek_6b_BTW) {
		this.rubriek_6b_BTW = rubriek_6b_BTW;
	}
	

}
