package nl.eadmin.databeans;

import java.util.ArrayList;
import java.util.Iterator;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.selection.DebCredLijstSelection;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.QueryFactory;

public class DebCredOverzichtDataBean {
	private DBData dbd;
	private Bedrijf bedrijf;
	private String dc, rekNrs, title;

	public DebCredOverzichtDataBean(Bedrijf bedrijf, String dc, String title) throws Exception {
		this.dbd = bedrijf.getDBData();
		this.bedrijf = bedrijf;
		this.dc = dc;
		this.title = title;
		this.rekNrs = GeneralHelper.getDebCredRekeningAsString(dbd, bedrijf.getBedrijfscode(), dc);
	}

	public ArrayList<DebCredLijstDataBean> getDebCredBeans(DebCredLijstSelection selection) throws Exception {
		if (dc.equals("D")) {
			return createDebBeans(selection);
		} else {
			return createCredBeans(selection);
		}
	}

	private ArrayList<DebCredLijstDataBean> createDebBeans(DebCredLijstSelection selection) throws Exception {
		DebCredLijstDataBean bean;
		ArrayList<DebCredLijstDataBean> beans = new ArrayList<DebCredLijstDataBean>();
		Iterator<?> iter = DebiteurManagerFactory.getInstance(dbd).getCollection(QueryFactory.create(Debiteur.class, Debiteur.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'")).iterator();
		while (iter.hasNext()) {
			bean = new DebCredLijstDataBean(bedrijf, dc, ((Debiteur) iter.next()).getDebNr(), rekNrs, selection);
			if (bean.isValid()) {
				beans.add(bean);
			}
		}
		return beans;
	}

	private ArrayList<DebCredLijstDataBean> createCredBeans(DebCredLijstSelection selection) throws Exception {
		DebCredLijstDataBean bean;
		ArrayList<DebCredLijstDataBean> beans = new ArrayList<DebCredLijstDataBean>();
		Iterator<?> iter = CrediteurManagerFactory.getInstance(dbd).getCollection(QueryFactory.create(Crediteur.class, Crediteur.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'")).iterator();
		while (iter.hasNext()) {
			bean = new DebCredLijstDataBean(bedrijf, dc, ((Crediteur) iter.next()).getCredNr(), rekNrs, selection);
			if (bean.isValid()) {
				beans.add(bean);
			}
		}
		return beans;
	}

	public String getTitle() {
		return title;
	}
}
