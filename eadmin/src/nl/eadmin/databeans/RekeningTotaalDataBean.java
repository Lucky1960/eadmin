package nl.eadmin.databeans;

import java.math.BigDecimal;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Journaal;
import nl.ibs.jsql.FreeQuery;

public class RekeningTotaalDataBean {
	private FreeQuery jrnQry;
	private Bedrijf bedrijf;
	private String dc, dcNr, factNr;
	private BigDecimal amtDebet, amtCredit;
	private int boekjaar;

	public RekeningTotaalDataBean(Bedrijf bedrijf, int boekjaar, FreeQuery jrnQry) throws Exception {
		this.bedrijf = bedrijf;
		this.boekjaar = boekjaar;
		this.jrnQry = jrnQry;
	}

	public void query(String rekening, String dcNr, String factNr) throws Exception {
		this.dcNr = dcNr;
		this.factNr = factNr;
		StringBuilder filter = new StringBuilder();
		filter.append("SELECT SUM(" + Journaal.BEDRAG_DEBET + "), SUM(" + Journaal.BEDRAG_CREDIT + ") FROM Journaal");
		filter.append(" WHERE " + Journaal.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		filter.append("   AND " + Journaal.BOEKJAAR + "=" + boekjaar);
		filter.append("   AND " + Journaal.REKENING_NR + "='" + rekening + "'");
		if (dcNr.length() > 0) {
			filter.append(" AND " + Journaal.DC_NUMMER + "='" + rekening + "'");
			filter.append(" AND " + Journaal.FACTUUR_NR + "='" + rekening + "'");
		}
		jrnQry.setQuery(filter.toString());
		Object[][] rslt = jrnQry.getResultArray();
		if (rslt != null && rslt.length > 0 && rslt[0].length > 0 && rslt[0][0] != null) {
			this.amtDebet = ((BigDecimal) rslt[0][0]);
			this.amtCredit = ((BigDecimal) rslt[0][1]);
		} else {
			this.amtDebet = ApplicationConstants.ZERO;
			this.amtCredit = ApplicationConstants.ZERO;
		}
		this.dc = amtDebet.doubleValue() >= amtCredit.doubleValue() ? "D" : "C";
	}

	public void setValues(String dcNr, String factNr, BigDecimal amtDebet, BigDecimal amtCredit) throws Exception {
		this.dcNr = dcNr;
		this.factNr = factNr;
		this.amtDebet = amtDebet;
		this.amtCredit = amtCredit;
		this.dc = amtDebet.doubleValue() >= amtCredit.doubleValue() ? "D" : "C";
	}

	public String getDC() throws Exception {
		return dc;
	}

	public String getDcNr() throws Exception {
		return dcNr;
	}

	public String getFactNr() throws Exception {
		return factNr;
	}

	public BigDecimal getDebet() throws Exception {
		return amtDebet;
	}

	public BigDecimal getCredit() throws Exception {
		return amtCredit;
	}

	public BigDecimal getSaldo() throws Exception {
		return amtDebet.subtract(amtCredit).abs();
	}
}
