package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Rekening;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;

public class GrootboekoverzichtDatabean {
	private ArrayList<GrootboekrekeningDatabean> rekeningBeans = new ArrayList<GrootboekrekeningDatabean>();
	private String title;
	private BigDecimal grandTotalDebet, grandTotalCredit;
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();

	public GrootboekoverzichtDatabean(String title, Bedrijf bedrijf, Collection<?> rekeningen, Date dateFrom, Date dateTo, boolean excludeEmpty) throws Exception {
		this.title = title;
		this.grandTotalDebet = ApplicationConstants.ZERO;
		this.grandTotalCredit = ApplicationConstants.ZERO;
		Iterator<?> iter = rekeningen.iterator();
		while (iter.hasNext()) {
			GrootboekrekeningDatabean bean = new GrootboekrekeningDatabean((Rekening) iter.next(), dateFrom, dateTo); 
			if (!excludeEmpty || bean.getTotaalDebet().doubleValue() != 0d || bean.getTotaalCredit().doubleValue() != 0d) {
				grandTotalDebet = grandTotalDebet.add(bean.getTotaalDebet());
				grandTotalCredit = grandTotalCredit.add(bean.getTotaalCredit());
				rekeningBeans.add(bean);
			}
		}
	}

	public ArrayList<GrootboekrekeningDatabean> getRekeningen() {
		return rekeningBeans;
	}

	public String getTitle() {
		return title;
	}

	public BigDecimal getGrandTotalCredit() {
		BigDecimal total = ApplicationConstants.ZERO;
		Iterator<?> iter = rekeningBeans.iterator();
		while (iter.hasNext()) {
			total = total.add(((GrootboekrekeningDatabean) iter.next()).getTotaalCredit());
		}
		return total;
	}

	public String getGrandTotalCredit$() {
		return (String) amtTransformer.transform(getGrandTotalCredit());
	}

	public BigDecimal getGrandTotalDebet() {
		BigDecimal total = ApplicationConstants.ZERO;
		Iterator<?> iter = rekeningBeans.iterator();
		while (iter.hasNext()) {
			total = total.add(((GrootboekrekeningDatabean) iter.next()).getTotaalDebet());
		}
		return total;
	}

	public String getGrandTotalDebet$() {
		return (String) amtTransformer.transform(getGrandTotalDebet());
	}

	public BigDecimal getGrandTotalSaldo() {
		return getGrandTotalDebet().subtract(getGrandTotalCredit());
	}
}
