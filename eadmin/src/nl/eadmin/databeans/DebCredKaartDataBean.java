package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class DebCredKaartDataBean {
	private String dcNr = "";
    private String dcNaam = "";
	private ArrayList<DebCredKaartMutatieDatabean> mutaties;
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();
	private BigDecimal ZERO = ApplicationConstants.ZERO;
	private BigDecimal totaalDebet = ZERO;
	private BigDecimal totaalCredit = ZERO;
	private DBData dbd;

	public DebCredKaartDataBean(String bedrijfsCode, String dcNr, String dc) throws Exception {
		this.dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		this.dcNr = dcNr;
		if ("D".equals(dc)) {
			DebiteurPK key = new DebiteurPK();
			key.setBedrijf(bedrijfsCode);
			key.setDebNr(dcNr);
			Debiteur deb  = DebiteurManagerFactory.getInstance(dbd).findOrCreate(key);
			dcNaam = deb.getNaam() + " - " + deb.getPlaats();
		} else {
			CrediteurPK key = new CrediteurPK();
			key.setBedrijf(bedrijfsCode);
			key.setCredNr(dcNr);
			Crediteur cred  = CrediteurManagerFactory.getInstance(dbd).findOrCreate(key);
			dcNaam = cred.getNaam() + " - " + cred.getPlaats();
		}
		mutaties = getMutaties(bedrijfsCode, dcNr, dc);
	}

	private ArrayList<DebCredKaartMutatieDatabean> getMutaties(String bedrijfsCode, String dcNr, String dc) throws Exception {
		boolean first = true;
		Iterator<?> rekeningNrs = GeneralHelper.getDebCredRekening(dbd, bedrijfsCode, dc).iterator();
		ArrayList<DebCredKaartMutatieDatabean> beans = new ArrayList<DebCredKaartMutatieDatabean>();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT DAGBOEK, BOEKSTUK, BOEKDTM, FACTUUR, OMSCHR1, DC, (BEDRAGDEB + BEDRAGCRED)");
		sb.append("  FROM Journaal");
		sb.append(" WHERE BDR = '" + bedrijfsCode + "'");
		sb.append("   AND DCNR = '" + dcNr + "'");
		sb.append("   AND (");
		while (rekeningNrs.hasNext()) {
			sb.append(first ? "" : " OR ");
			sb.append(" REKNR='" + ((Rekening)rekeningNrs.next()).getRekeningNr() + "'");
			first = false;
		}
		sb.append(") ORDER BY FACTUUR, BOEKDTM");
		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) JournaalManagerFactory.getInstance(dbd), sb.toString());

		Object[][] maxRslt = qry.getResultArray();
		Date boekDatum;
		String dagboek, boekstuk, factuur, omschr, dcCode;
		BigDecimal amount, amtDebet, amtCredit;
		if (maxRslt.length > 0 && maxRslt[0].length > 0) {
			for (int i = 0; i < maxRslt.length; i++) {
				dagboek = (maxRslt[i][0] == null) ? "" : ((String) maxRslt[i][0]);
				boekstuk = (maxRslt[i][1] == null) ? "" : ((String) maxRslt[i][1]);
				boekDatum = (maxRslt[i][2] == null) ? null : ((Date) maxRslt[i][2]);
				factuur = (maxRslt[i][3] == null) ? "" : ((String) maxRslt[i][3]);
				omschr = (maxRslt[i][4] == null) ? "" : ((String) maxRslt[i][4]);
				dcCode = (maxRslt[i][5] == null) ? "" : ((String) maxRslt[i][5]);
				amount = (maxRslt[i][6] == null) ? ZERO : ((BigDecimal) maxRslt[i][6]);
				if ("D".equals(dcCode)) {
					amtDebet = amount;
					amtCredit = ZERO;
					totaalDebet = totaalDebet.add(amount);
				} else {
					amtDebet = ZERO;
					amtCredit = amount;
					totaalCredit = totaalCredit.add(amount);
				}
				beans.add(new DebCredKaartMutatieDatabean(dagboek, boekstuk, boekDatum, factuur, omschr, amtDebet, amtCredit));
			}
		}
		return beans;
	}
	public String getDcNr() {
		return dcNr;
	}

	public String getDcNaam() {
		return dcNaam;
	}

	public ArrayList<DebCredKaartMutatieDatabean> getMutaties() throws Exception {
		return mutaties;
	}

	public BigDecimal getTotaalDebet() {
		return totaalDebet;
	}

	public String getTotaalDebet$() {
		return (String) amtTransformer.transform(totaalDebet);
	}

	public BigDecimal getTotaalCredit() {
		return totaalCredit;
	}

	public String getTotaalCredit$() {
		return (String) amtTransformer.transform(totaalCredit);
	}

	public BigDecimal getSaldoDebet() {
		if (totaalDebet.doubleValue() >= totaalCredit.doubleValue()) {
			return totaalDebet.subtract(totaalCredit);
		} else {
			return ZERO;
		}
	}
	public BigDecimal getSaldoCredit() {
		if (totaalCredit.doubleValue() >= totaalDebet.doubleValue()) {
			return totaalCredit.subtract(totaalDebet);
		} else {
			return ZERO;
		}
	}
	public String getSaldoDebet$() {
		return (String) amtTransformer.transform(getSaldoDebet());
	}
	public String getSaldoCredit$() {
		return (String) amtTransformer.transform(getSaldoCredit());
	}
}
