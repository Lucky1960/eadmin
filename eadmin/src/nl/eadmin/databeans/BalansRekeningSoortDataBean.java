package nl.eadmin.databeans;

import java.math.BigDecimal;
import java.util.ArrayList;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;

public class BalansRekeningSoortDataBean {
	private ArrayList<BalansRekeningDataBean> rekeningBeans = new ArrayList<BalansRekeningDataBean>();
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();
	private String rekeningSoort;
	private BigDecimal ZERO = ApplicationConstants.ZERO;
	private BigDecimal totaalDebet = ZERO;
	private BigDecimal totaalCredit = ZERO;

	public BalansRekeningSoortDataBean(String rekeningSoort) throws Exception {
		this.rekeningSoort = rekeningSoort;
	}

	public void addRekening(BalansRekeningDataBean rekeningBean) throws Exception {
		rekeningBeans.add(rekeningBean);
		totaalDebet = totaalDebet.add(rekeningBean.getRekTotaalDebet());
		totaalCredit = totaalCredit.add(rekeningBean.getRekTotaalCredit());
	}

	public ArrayList<BalansRekeningDataBean> getRekening() {
		return rekeningBeans;
	}

	public String getRekSoort() throws Exception {
		return RekeningSoortEnum.getValue(rekeningSoort);
	}

	public BigDecimal getRekSoortTotaalDebet() {
		return totaalDebet;
	}

	public String getRekSoortTotaalDebet$() {
		return (String) amtTransformer.transform(totaalDebet);
	}

	public BigDecimal getRekSoortTotaalCredit() {
		return totaalCredit;
	}

	public String getRekSoortTotaalCredit$() {
		return (String) amtTransformer.transform(totaalCredit);
	}
}
