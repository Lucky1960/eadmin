package nl.eadmin.databeans;

import java.math.BigDecimal;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Adres;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.jsql.DBData;

public class IcpOverzichtDataBean {
	public static String LEV_DIENST = "levDienst";
	public static String FACTNR = "factuurNr";
	public static String DEBNR = "debiteurNr";
	public static String DEBNAAM = "debiteurNaam";
	public static String ADRES_RGL1 = "adresRegel1";
	public static String ADRES_RGL2 = "adresRegel2";
	public static String LAND = "land";
	public static String BTWNR = "btwNr";
	public static String BEDRAG_EXCL = "bedragExcl";
	private String levDienst, factNr;
	private BigDecimal bedragExcl;
	private Debiteur debiteur;
	private Adres adres;
	

	public IcpOverzichtDataBean(String _bedrijf, String _debNr, String factNr, BigDecimal bedragExcl, String levDienst) throws Exception {
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		DebiteurPK key = new DebiteurPK();
		key.setBedrijf(_bedrijf);
		key.setDebNr(_debNr);
		this.debiteur = DebiteurManagerFactory.getInstance(dbd).findOrCreate(key);
		this.adres = debiteur.getAdres(false);
		this.factNr = factNr;
		this.bedragExcl = bedragExcl;
		this.levDienst = levDienst;
	}

	public String getLevDienst() throws Exception {
		return levDienst;
	}

	public String getFactuurNr() throws Exception {
		return factNr;
	}

	public String getDebiteurNr() throws Exception {
		return debiteur.getDebNr();
	}

	public String getDebiteurNaam() throws Exception {
		return debiteur.getNaam();
	}

	public String getAdresRegel1() throws Exception {
		return adres.getAdresRegel1();
	}

	public String getAdresRegel2() throws Exception {
		return adres.getAdresRegel2();
	}

	public String getLand() throws Exception {
		return adres.getLand();
	}

	public String getBtwNr() throws Exception {
		return debiteur.getBtwNr();
	}

	public BigDecimal getBedragExcl() throws Exception {
		return bedragExcl;
	}
}
