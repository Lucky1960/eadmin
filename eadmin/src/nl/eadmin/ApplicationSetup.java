package nl.eadmin;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Iterator;

import nl.eadmin.db.Autorisatie;
import nl.eadmin.db.AutorisatieManagerFactory;
import nl.eadmin.db.AutorisatiePK;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfDataBean;
import nl.eadmin.db.BedrijfManager;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieDataBean;
import nl.eadmin.db.BtwCategorieManager;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeDataBean;
import nl.eadmin.db.BtwCodeManager;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.BtwCodePK;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekDataBean;
import nl.eadmin.db.DagboekManager;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.DagboekPK;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerDataBean;
import nl.eadmin.db.GebruikerManager;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateManager;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.db.Instellingen;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeDataBean;
import nl.eadmin.db.PeriodeManager;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.PeriodePK;
import nl.eadmin.db.impl.BtwCategorieManager_Impl;
import nl.eadmin.db.impl.BtwCodeManager_Impl;
import nl.eadmin.db.impl.DagboekManager_Impl;
import nl.eadmin.db.impl.HtmlTemplateManager_Impl;
import nl.eadmin.db.impl.PeriodeManager_Impl;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.helpers.HtmlTemplateHelper;
import nl.eadmin.ui.uiobjects.panels.GebruikerAutPanel;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.jsql.exception.FinderException;
import nl.ibs.util.Scrambler;

public class ApplicationSetup {
	private Gebruiker gebruiker;

	public static void initialize() throws Exception {
		new ApplicationSetup().setup(null, null);
	}

	public static void initialize(String adminId, String adminName) throws Exception {
		new ApplicationSetup().setup(adminId, adminName);
	}

	public static void setupEnvironment(String env) throws Exception {
		
	}

	private void setup(String adminId, String adminNaam) throws Exception {
		if (adminId == null)
			adminId = ApplicationConstants.DEFAULT_ADMIN_NAME;
		if (adminNaam == null)
			adminNaam = "Setup administrator";
		createAdmin(adminId, adminNaam);
		createBedrijf();
		checkDatabase();
	}

	private void createAdmin(String id, String naam) throws Exception {
		GebruikerManager gebruikerManager = GebruikerManagerFactory.getInstance();
		gebruiker = gebruikerManager.getFirstObject(null);
		if (gebruiker == null) {
			GebruikerDataBean bean = new GebruikerDataBean();
			bean.setId(id);
			bean.setNaam(naam);
			bean.setWachtwoord(Scrambler.scramble("eadmin"));
			bean.setBedrijf("001");
			gebruiker = gebruikerManager.create(bean);
		}
	}

	private void createBedrijf() throws Exception {
		BedrijfManager bedrijfManager = BedrijfManagerFactory.getInstance();
		Bedrijf bedrijf = bedrijfManager.getFirstObject(null);
		if (bedrijf == null) {
			BedrijfDataBean bean = new BedrijfDataBean();
			bean.setBedrijfscode("001");
			bean.setOmschrijving("Setup bedrijf");
			bedrijf = bedrijfManager.create(bean);
			initBedrijf(bedrijf, gebruiker);
		}
	}

	public void initBedrijf(Bedrijf bedrijf, Gebruiker gebruiker) throws Exception {
		createAuthorisatie(bedrijf, gebruiker);
		createInstellingen(bedrijf);
		createPeriode(bedrijf);
		createDagboeken(bedrijf);
		createBtwCodes(bedrijf);
	}

	private void createAuthorisatie(Bedrijf bedrijf, Gebruiker gebruiker) throws Exception {
		AutorisatiePK key = new AutorisatiePK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setGebruikerId(gebruiker.getId());
		Autorisatie autorisatie = AutorisatieManagerFactory.getInstance().findOrCreate(key);
		autorisatie.setAutMenu(GebruikerAutPanel.getAllAuthorisations());
	}

	private void createInstellingen(Bedrijf bedrijf) throws Exception {
		Instellingen settings = bedrijf.getInstellingen();
		if (settings.getStdBetaalTermijn() == 0 && settings.getLengteRekening() == 0) {
			settings.setStdBetaalTermijn(14);
			settings.setLengteRekening(4);
		}
	}

	private void createPeriode(Bedrijf bedrijf) throws Exception {
		PeriodeManager periodeManager = PeriodeManagerFactory.getInstance();
		Periode periode = periodeManager.getFirstObject(QueryFactory.create((PeriodeManager_Impl) periodeManager, Periode.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'"));
		if (periode == null) {
			Calendar cal1 = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			cal1.setLenient(true);
			cal2.setLenient(true);
			PeriodePK key = new PeriodePK();
			key.setBedrijf(bedrijf.getBedrijfscode());
			for (int jaar = 2010; jaar <= 2025; jaar++) {
				for (int per = 1; per <= 12; per++) {
					key.setBoekjaar(jaar);
					key.setBoekperiode(per);
					try {
						periodeManager.findByPrimaryKey(key);
					} catch (FinderException e) {
						cal1.set(Calendar.YEAR, jaar);
						cal1.set(Calendar.MONTH, per - 1);
						cal1.set(Calendar.DAY_OF_MONTH, 1);
						cal2.set(Calendar.YEAR, jaar);
						cal2.set(Calendar.MONTH, per - 1);
						cal2.set(Calendar.DAY_OF_MONTH, cal1.getActualMaximum(Calendar.DAY_OF_MONTH));
						PeriodeDataBean bean = new PeriodeDataBean();
						bean.setBedrijf(key.getBedrijf());
						bean.setBoekjaar(key.getBoekjaar());
						bean.setBoekperiode(key.getBoekperiode());
						bean.setStartdatum(cal1.getTime());
						bean.setEinddatum(cal2.getTime());
						periodeManager.create(bean);
					}
				}
			}
		}
	}

	private void createDagboeken(Bedrijf bedrijf) throws Exception {
		// @formatter:off
		String[][] data = new String[][]{
		{"BNK1", "Bankboek", DagboekSoortEnum.BANK, "", "B@@@@&&#####"},
		{"KAS1", "Kasboek", DagboekSoortEnum.KAS, "", "K@@@@&&#####"},
		{"INK1", "Inkoopboek", DagboekSoortEnum.INKOOP, "", "I@@@@&&#####"},
		{"VRK1", "Verkoopboek", DagboekSoortEnum.VERKOOP, "", "@@@@&&#####"},
		{"MEM1", "Memoriaal", DagboekSoortEnum.MEMO, "", "@@@@&&#####"},
		{"SYS1", "Beginbalans", DagboekSoortEnum.MEMO, "", "BB@@@@#####"} };
		// @formatter:on
		DagboekManager manager = DagboekManagerFactory.getInstance();
		DagboekDataBean bean = new DagboekDataBean();
		DagboekPK key = new DagboekPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		for (int d = 0; d < data.length; d++) {
			try {
				key.setId(data[d][0]);
				manager.findByPrimaryKey(key);
			} catch (FinderException e) {
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setId(data[d][0]);
				bean.setOmschrijving(data[d][1]);
				bean.setSoort(data[d][2]);
				bean.setRekening(data[d][3]);
				bean.setBoekstukMask(data[d][4]);
				bean.setLaatsteBoekstuk("");
				bean.setSystemValue(data[d][0].startsWith("SYS"));
				manager.create(bean);
			}
		}
	}

	private void createBtwCodes(Bedrijf bedrijf) throws Exception {
		// @formatter:off
		String[][] data = new String[][] {
		{ "L", "Geen BTW", "E", "0.00", "", "" },
		{ "X", "0%", "E", "0.00", "5B", "1E" },
		{ "V", "BTW verlegd", "I", "0.00", "5B", "3B" },
		{ "E06", "6% Excl.", "E", "6.00", "5B", "1B" },
		{ "E21", "21% Excl.", "E", "21.00", "5B", "1A" },
		{ "I06", "6% Incl.", "I", "6.00", "5B", "1B" },
		{ "I21", "21% Incl.", "I", "21.00", "5B", "1A" } };
		// @formatter:on
		BtwCodeManager manager = BtwCodeManagerFactory.getInstance();
		BtwCodeDataBean bean = new BtwCodeDataBean();
		BtwCodePK key = new BtwCodePK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		for (int d = 0; d < data.length; d++) {
			try {
				key.setCode(data[d][0]);
				manager.findByPrimaryKey(key);
			} catch (FinderException e) {
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setCode(data[d][0]);
				bean.setOmschrijving(data[d][1]);
				bean.setInEx(data[d][2]);
				bean.setPercentage(new BigDecimal(data[d][3]));
				bean.setBtwCatInkoop(data[d][4]);
				bean.setBtwCatVerkoop(data[d][5]);
				manager.create(bean);
			}
		}
	}

	private void checkDatabase() throws Exception {
		BedrijfManager bedrijfManager = BedrijfManagerFactory.getInstance();
		Iterator<?> iter = bedrijfManager.getCollection(null).iterator();
		while (iter.hasNext()) {
			Bedrijf bedrijf = ((Bedrijf) iter.next());
			checkBtwCategorie(bedrijf);
			checkBtwCode(bedrijf);
			checkDagboek(bedrijf);
			checkLayouts(bedrijf);
		}
	}

	private void checkBtwCategorie(Bedrijf bedrijf) throws Exception {
		BtwCategorieManager mgr = BtwCategorieManagerFactory.getInstance();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM BtwCategorie WHERE ");
		sb.append(BtwCategorie.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' ");
		FreeQuery qry = QueryFactory.createFreeQuery((BtwCategorieManager_Impl) mgr, sb.toString());
		if (qry.getFirstObject() == null) {
			BtwCategorieDataBean bean = new BtwCategorieDataBean();
			bean.setBedrijf(bedrijf.getBedrijfscode());
			bean.setRekeningNr("");
			// @formatter:off
			String[][] data = new String[][] {
			{ "1A", "Btw-formulier cat. 1A" },
			{ "1B", "Btw-formulier cat. 1B" },
			{ "1C", "Btw-formulier cat. 1C" },
			{ "1D", "Btw-formulier cat. 1D" },
			{ "1E", "Btw-formulier cat. 1E" },
			{ "2A", "Btw-formulier cat. 2A" },
			{ "3A", "Btw-formulier cat. 3A" },
			{ "3B", "Btw-formulier cat. 3B" },
			{ "3C", "Btw-formulier cat. 3C" },
			{ "4A", "Btw-formulier cat. 4A" },
			{ "4B", "Btw-formulier cat. 4B" },
			{ "5B", "Btw-formulier cat. 5B" },
			{ "5D", "Btw-formulier cat. 5D" } };
			// @formatter:on
			for (int d = 0; d < data.length; d++) {
				bean.setCode(data[d][0]);
				bean.setOmschrijving(data[d][1]);
				mgr.create(bean);
			}
		}
	}

	private void checkBtwCode(Bedrijf bedrijf) throws Exception {
		BtwCodeManager mgr = BtwCodeManagerFactory.getInstance();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM BtwCode WHERE ");
		sb.append(BtwCode.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		sb.append(BtwCode.CODE + "='L'");
		FreeQuery qry = QueryFactory.createFreeQuery((BtwCodeManager_Impl) mgr, sb.toString());
		if (qry.getFirstObject() == null) {
			BtwCodeDataBean bean = new BtwCodeDataBean();
			bean.setBedrijf(bedrijf.getBedrijfscode());
			bean.setBtwCatInkoop("");
			bean.setBtwCatVerkoop("");
			bean.setCode("L");
			bean.setInEx("I");
			bean.setOmschrijving("Geen BTW");
			bean.setPercentage(BigDecimal.ZERO);
			mgr.create(bean);
		}
	}

	private void checkDagboek(Bedrijf bedrijf) throws Exception {
		DagboekManager mgr = DagboekManagerFactory.getInstance();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM Dagboek WHERE ");
		sb.append(Dagboek.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		sb.append(Dagboek.ID + "='SYS1'");
		FreeQuery qry = QueryFactory.createFreeQuery((DagboekManager_Impl) mgr, sb.toString());
		if (qry.getFirstObject() == null) {
			DagboekDataBean bean = new DagboekDataBean();
			bean.setBedrijf(bedrijf.getBedrijfscode());
			bean.setId("SYS1");
			bean.setOmschrijving("Beginbalans");
			bean.setSoort(DagboekSoortEnum.MEMO);
			bean.setRekening("");
			bean.setBoekstukMask("BB@@@@###");
			bean.setLaatsteBoekstuk("");
			bean.setSystemValue(true);
			mgr.create(bean);
		}
	}

	private void checkLayouts(Bedrijf bedrijf) throws Exception {
		HtmlTemplateManager layoutManager = HtmlTemplateManagerFactory.getInstance();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM HtmlTemplate WHERE ");
		sb.append(HtmlTemplate.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' ");
		FreeQuery qry = QueryFactory.createFreeQuery((HtmlTemplateManager_Impl) layoutManager, sb.toString());
		if (qry.getFirstObject() == null) {
			HtmlTemplateHelper.createStdHtmlTemplate(bedrijf);
		}
	}
}