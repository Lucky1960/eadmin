package nl.eadmin;

import java.math.BigDecimal;

public class ApplicationConstants {
	public static final String APPLICATION = "eadmin";
	public static final String APPLICATION_VERSION = "00.102";
	public static final String EOL = System.getProperty("line.separator");
	public static final String DEFAULT_ADMIN_NAME = "ADMIN";
	public static final String DUMMYSTRING = "@#$DUMMY%$#@";
	public static final String LANG = "nl.eadmin.language";
	public static final String MAIN_ESP_PROCESS_ID = "MainEspProcessId";
	public static final String SEPARATOR = "#@#";

	public static final int	NUMBEROFTABLEROWS = 12;
	public static final int STD_LABELWIDTH = 150;
	public static final int STD_TABLEWIDTH_1 = 750;
	public static final int STD_TABLEWIDTH_2 = 1000;

	public static final BigDecimal ZERO = new BigDecimal("0.00");
}