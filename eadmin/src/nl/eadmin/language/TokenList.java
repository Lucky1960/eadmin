package nl.eadmin.language;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TokenList {

	private List<Token> list = new ArrayList<Token>();

	public TokenList() {
	}

	public TokenList addToken(Token token) {
		list.add(token);
		return this;
	}

	public TokenList addNonTranslatable(Object o) {
		list.add(new NonTranslatableToken(o));
		return this;
	}

	public TokenList addTranslatable(String packageName, String msgToken) {
		list.add(new TranslatableToken(packageName, msgToken));
		return this;
	}

	public Iterator<Token> iterator() {
		return list.iterator();
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

	public int size() {
		return list.size();
	}

	public String getValue(int index) {
		Token token = (Token) list.get(index);
		return token.getValue();
	}

	public String[] getValues() {
		String[] result = null;
		if (!isEmpty()) {
			result = new String[size()];
			for (int i = 0; i < size(); i++) {
				result[i] = getValue(i);
			}
		}
		return result;
	}

}
