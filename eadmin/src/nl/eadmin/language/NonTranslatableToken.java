package nl.eadmin.language;

public class NonTranslatableToken implements Token {

	private Object object;

	public NonTranslatableToken(Object object) {
		this.object = object;
	}

	public String getValue() {
		return (object != null ? object.toString() : "null");
	}

	public String toString() {
		return getValue();
	}
}
