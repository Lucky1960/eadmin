package nl.eadmin.language;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import nl.ibs.jeelog.Log;

public class TranslatableToken implements Token {
	public static final String ERRORS_BUNDLE = "Texts";

	private String packageName;

	private String msgToken;

	public TranslatableToken(String packageName, String msgToken) {
		this.packageName = packageName;
		this.msgToken = msgToken;
	}

	protected String translate(Locale locale) {
		String result = null;
		try {
			ResourceBundle resource = ResourceBundle.getBundle(getBundleName(), locale);
			result = resource.getString(msgToken);
		} catch (MissingResourceException e) {
			Log.warn("Missing Resource for: Token[" + msgToken + "] Package [" + packageName + "] Locale [" + locale + "]");
			result = msgToken;
		}
		return result;
	}

	public String getValue() {
		return translate(UserContext.getLocale());
	}

	public String toString() {
		return getValue();
	}

	protected String getBundleName() {
		String result = null;
		if (packageName == null || "".equals(packageName)) {
			result = ERRORS_BUNDLE;
		} else {
			result = packageName + "." + ERRORS_BUNDLE;
		}
		return result;
	}

}
