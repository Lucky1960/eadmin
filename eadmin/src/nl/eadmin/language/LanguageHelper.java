package nl.eadmin.language;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import nl.eadmin.ApplicationConstants;

public abstract class LanguageHelper {

	private static final String DEFAULT_RESOURCE_BUNDLE = "Texts";

	private static String getBundleName(String packageName) {
		String result = null;
		if (packageName == null || packageName.trim().length() == 0) {
			result = DEFAULT_RESOURCE_BUNDLE;
		} else {
			result = packageName + "." + DEFAULT_RESOURCE_BUNDLE;
		}
		return result;
	}

	public static String getString(boolean b) {
		return getString(ApplicationConstants.LANG, b ? "Yes" : "No", null);
	}

	public static String getString(String msgKey) {
		return getString(ApplicationConstants.LANG, msgKey, null);
	}

	public static String getString(String packagePrefix, String msgKey) {
		return getString(packagePrefix, msgKey, null);
	}

	public static String getString(String prefix, String msgKey, TokenList list) {
		Locale locale = UserContext.getLocale();
		String bundleName = getBundleName(prefix);
		return getString(bundleName, msgKey, list, locale);
	}

	public static String getString(String bundleName, String msgKey, TokenList list, Locale locale) {
		if (msgKey == null || msgKey.equals("")) {
			return msgKey;
		}
		ResourceBundle resource = ResourceBundle.getBundle(bundleName, locale);
		String result = "";
		try {
			result = resource.getString(msgKey);
			if (list != null && !list.isEmpty()) {
				result = MessageFormat.format(result, (Object[])list.getValues());
			}
		} catch (MissingResourceException mre) {
			try {
				result = resource.getString(Character.toUpperCase(msgKey.charAt(0)) + msgKey.substring(1));
				if (list != null && !list.isEmpty()) {
					result = MessageFormat.format(result, (Object[])list.getValues());
				}
			} catch (MissingResourceException mre2) {
				result = msgKey;
			}
		}
		return result;
	}

}
