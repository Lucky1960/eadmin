package nl.eadmin.language;

import java.io.Serializable;
import java.util.Locale;

public class UserContext implements Serializable {

	private static final long serialVersionUID = 3770944138201377472L;
	public static final String LOCALE = "locale";

	private static ThreadLocal<Object> threadUserLocale = new ThreadLocal<Object>() {
		public synchronized Object initialValue() {
			return Locale.getDefault();
		}
	};

	public static Locale getLocale() {
		return (Locale) threadUserLocale.get();
	}

	public static void setLocale(Locale locale) {
		threadUserLocale.set(locale);
	}
}