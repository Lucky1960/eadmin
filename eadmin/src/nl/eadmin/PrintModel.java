package nl.eadmin;

import java.io.File;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class PrintModel {

	public static void main(String argv[]) {

		try {
			// ApplicationModel . . .
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File("D:\\EclipseGit\\eadmin\\eadmin\\model\\model.xml"));
			doc.getDocumentElement().normalize();
			System.out.println("");
			System.out.println(doc.getDocumentElement().getNodeName() + " MijnAdmini");
			// File list . . .
			NodeList fileList = doc.getElementsByTagName("BusinessObject");
			System.out.println("");
			for (int i = 0; i < fileList.getLength(); i++) {
				Node fileNode = fileList.item(i);
				if (fileNode.getNodeType() == Node.ELEMENT_NODE) {
					Element fileElement = (Element) fileNode;
					String table = fileElement.getAttribute("table");
					if (table.length() < 10) {
						int x = 10 - table.length();
						do {
							table = table + " ";
							x--;
						} while (x > 0);
					}
					System.out.println(table + "\t" + fileElement.getAttribute("name"));
				}
			}
			// Total number of files . . .
			System.out.println("");
			System.out.println("Total number of files : " + fileList.getLength());
			System.out.println("");

			// Individual files . . .
			for (int i = 0; i < fileList.getLength(); i++) {
				Node fileNode = fileList.item(i);
				if (fileNode.getNodeType() == Node.ELEMENT_NODE) {
					Element fileElement = (Element) fileNode;
					System.out.println("");
					System.out.println("");
					// iSeries name + class name . . .
					String table = fileElement.getAttribute("table");
					if (table.length() < 10) {
						int x = 10 - table.length();
						do {
							table = table + " ";
							x--;
						} while (x > 0);
					}
					System.out.println("=======================================================================");
					System.out.println(table + "\t" + fileElement.getAttribute("name"));

					// File details . . .
					NodeList fileAttributes = fileNode.getChildNodes();
					for (int j = 0; j < fileAttributes.getLength(); j++) {
						Node fileAttributeNode = fileAttributes.item(j);
						if (fileAttributeNode instanceof Element) {
							Element fileAttributeElement = (Element) fileAttributeNode;

							if ("Relations".equals(fileAttributeElement.getTagName())) {
								NodeList relationList = fileAttributeNode.getChildNodes();
								for (int m = 0; m < relationList.getLength(); m++) {
									Node relationNode = relationList.item(m);
									if (relationNode instanceof Element) {
										Element relationElement = (Element) relationNode;
										System.out.println("");
										System.out.println(relationElement.getNodeName() + " " + relationElement.getAttribute("businessObject"));
									}
								}
							}
							if ("Indexes".equals(fileAttributeElement.getTagName())) {
								NodeList indexList = fileAttributeNode.getChildNodes();
								for (int m = 0; m < indexList.getLength(); m++) {
									Node indexNode = indexList.item(m);
									if (indexNode instanceof Element) {
										Element indexElement = (Element) indexNode;
										System.out.println("");
										System.out.println("Index " + indexElement.getAttribute("name"));
										NodeList keyList = indexNode.getChildNodes();
										for (int l = 0; l < keyList.getLength(); l++) {
											Node keyFieldNode = keyList.item(l);
											if (keyFieldNode instanceof Element) {
												Element keyFieldElement = (Element) keyFieldNode;
												System.out.println("- " + keyFieldElement.getAttribute("name"));
											}
										}
									}
								}
							}
							if ("PrimaryKey".equals(fileAttributeElement.getTagName())) {
								System.out.println("");
								System.out.println("Primary Key");
								NodeList keyList = fileAttributeNode.getChildNodes();
								for (int l = 0; l < keyList.getLength(); l++) {
									Node keyFieldNode = keyList.item(l);
									if (keyFieldNode instanceof Element) {
										Element keyFieldElement = (Element) keyFieldNode;
										System.out.println("- " + keyFieldElement.getAttribute("name"));
									}
								}
							}
							if ("Attributes".equals(fileAttributeElement.getTagName())) {
								System.out.println("");
								System.out.println("RPG Name  " + "\t" + "Type" + "\t" + "Length" + "\t" + "Dec." + "\t" + "JAVA Name");
								System.out.println("=======================================================================");
								NodeList fieldAttributes = fileAttributeNode.getChildNodes();
								for (int k = 0; k < fieldAttributes.getLength(); k++) {
									Node fieldAttributeNode = fieldAttributes.item(k);
									if (fieldAttributeNode instanceof Element) {
										Element fieldAttributeElement = (Element) fieldAttributeNode;
										String column = fieldAttributeElement.getAttribute("column");
										if (column.length() < 10) {
											int x = 10 - column.length();
											do {
												column = column + " ";
												x--;
											} while (x > 0);
										}
										System.out.println(column + "\t" + fieldAttributeElement.getTagName() + "\t" + fieldAttributeElement.getAttribute("length") + "\t"
												+ fieldAttributeElement.getAttribute("decimals") + "\t" + fieldAttributeElement.getAttribute("name"));
									}
								}
							}
						}
					}
				}
			}

		} catch (SAXParseException err) {
			System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
			System.out.println(" " + err.getMessage());

		} catch (SAXException e) {
			Exception x = e.getException();
			((x == null) ? e : x).printStackTrace();

		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	static String retrieveOptions(Element fieldAttributeElement) {
		String options = "";
		String result = "";

		result = fieldAttributeElement.getAttribute("hidden");
		if (result == null || result == "") {
			options = options + "  ";
		} else {
			if (result.equals("true")) {
				options = options + "1 ";
			} else {
				options = options + "0 ";
			}
		}
		result = fieldAttributeElement.getAttribute("read_only");
		if (result == null || result == "") {
			options = options + "  ";
		} else {
			if (result.equals("true")) {
				options = options + "1 ";
			} else {
				options = options + "0 ";
			}
		}
		result = fieldAttributeElement.getAttribute("initialise");
		if (result == null || result == "") {
			options = options + "  ";
		} else {
			if (result.equals("true")) {
				options = options + "1 ";
			} else {
				options = options + "0 ";
			}
		}
		result = fieldAttributeElement.getAttribute("password_field");
		if (result == null || result == "") {
			options = options + "  ";
		} else {
			if (result.equals("true")) {
				options = options + "1 ";
			} else {
				options = options + "0 ";
			}
		}
		result = fieldAttributeElement.getAttribute("scramble");
		if (result == null || result == "") {
			options = options + "  ";
		} else {
			if (result.equals("true")) {
				options = options + "1 ";
			} else {
				options = options + "0 ";
			}
		}
		return options;

	}

}
