/**
 * 
 */
package nl.eadmin;

import nl.ibs.jsql.DBPersistenceManager;
import nl.ibs.jsql.sql.ConnectionProvider;

public class AppDBPersistenceManager extends DBPersistenceManager{

	public static ConnectionProvider getConnectionProvider() throws Exception {
		return getConnectionProvider(ApplicationConstants.APPLICATION);
	}
}