package nl.eadmin;

import nl.eadmin.helpers.MailHelper;



public class MyException extends java.lang.Exception {
	private static final long serialVersionUID = 1L;

	public MyException() {
		super();
		sendErrorMsg();
	}

	public MyException(String msg) {
		super(msg);
		sendErrorMsg();
	}

	public MyException(String msg, Throwable t) {
		super(msg, t);
		sendErrorMsg();
	}

	public MyException(Throwable t) {
		super(t);
		sendErrorMsg();
	}

	public MyException(String msg, Throwable t, boolean b1, boolean b2) {
		super(msg, t, b1, b2);
		sendErrorMsg();
	}

	private void sendErrorMsg() {
		try {
			MailHelper.mailErrorMsg(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
