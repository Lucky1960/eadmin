package nl.eadmin.helpers;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.BankAfschrift;
import nl.eadmin.db.BankAfschriftManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.DagboekPK;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataDataBean;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataDataBean;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.RekeningPK;
import nl.eadmin.db.impl.BankAfschriftManager_Impl;
import nl.eadmin.db.impl.BedrijfManager_Impl;
import nl.eadmin.db.impl.BijlageManager_Impl;
import nl.eadmin.db.impl.CrediteurManager_Impl;
import nl.eadmin.db.impl.DagboekManager_Impl;
import nl.eadmin.db.impl.DebiteurManager_Impl;
import nl.eadmin.db.impl.DetailDataManager_Impl;
import nl.eadmin.db.impl.HeaderDataManager_Impl;
import nl.eadmin.db.impl.RekeningManager_Impl;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.enums.HeaderDataStatusEnum;
import nl.eadmin.ui.datareaders.DetailDataReader;
import nl.eadmin.ui.datareaders.HeaderDataReader;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.AbstractTable;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CommonTable;
import nl.ibs.esp.uiobjects.ContextAction;
import nl.ibs.esp.uiobjects.TableRow;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.BusinessObject;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.DBPersistenceManager;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.jsql.sql.ConnectionProvider;
import nl.ibs.jsql.sql.DBConnection;
import nl.sf.api.InitializationRequest;
import nl.sf.api.UserData;

public abstract class GeneralHelper {

// @formatter:off
	public static String[] getColumnNamesForHeaderData(Dagboek dagboek) throws Exception {
		switch (dagboek.getSoort()) {
		case DagboekSoortEnum.BANK:
			return new String[] { HeaderData.BOEKSTUK, HeaderData.BOEKDATUM, HeaderData.TOTAAL_CREDIT, HeaderData.TOTAAL_DEBET, HeaderData.OMSCHR1, HeaderData.OMSCHR2, HeaderData.STATUS, HeaderDataReader.BIJLAGE };
		case DagboekSoortEnum.KAS:
			return new String[] { HeaderData.BOEKSTUK, HeaderData.BOEKDATUM, HeaderData.DC, HeaderData.TOTAAL_EXCL, HeaderData.TOTAAL_BTW, HeaderData.TOTAAL_INCL, HeaderData.OMSCHR1, HeaderData.OMSCHR2, HeaderDataReader.BIJLAGE };
		case DagboekSoortEnum.MEMO:
			return new String[] { HeaderData.BOEKSTUK, HeaderData.BOEKDATUM, HeaderData.TOTAAL_DEBET, HeaderData.TOTAAL_CREDIT, HeaderData.OMSCHR1, HeaderData.OMSCHR2, HeaderData.STATUS, HeaderDataReader.BIJLAGE };
		case DagboekSoortEnum.INKOOP:
			return new String[] { HeaderData.FAVORIET, HeaderData.FACTUUR_NUMMER, HeaderData.DC_NUMMER, HeaderDataReader.CRED_NAAM, HeaderData.FACTUURDATUM, HeaderData.VERVALDATUM, HeaderData.REFERENTIE_NUMMER, HeaderData.TOTAAL_GEBOEKT, /*HeaderData.TOTAAL_EXCL, HeaderData.TOTAAL_BTW, HeaderData.TOTAAL_INCL,*/ HeaderData.TOTAAL_BETAALD, HeaderData.OMSCHR1, HeaderData.OMSCHR2, HeaderData.STATUS, HeaderDataReader.BIJLAGE };
		case DagboekSoortEnum.VERKOOP:
			return new String[] { HeaderData.FACTUUR_NUMMER, HeaderData.DC_NUMMER, HeaderDataReader.DEBT_NAAM, HeaderData.FACTUURDATUM, HeaderData.VERVALDATUM, HeaderData.REFERENTIE_NUMMER, HeaderData.TOTAAL_GEBOEKT, HeaderData.TOTAAL_EXCL, HeaderData.TOTAAL_BTW, HeaderData.TOTAAL_INCL, HeaderData.TOTAAL_BETAALD, HeaderData.OMSCHR1, HeaderData.OMSCHR2, HeaderData.STATUS, HeaderDataReader.BIJLAGE };
		default:
			return new String[] {};
		}
	}

	public static String[] getColumnNamesForDetailData(Dagboek dagboek) throws Exception {
		switch (dagboek.getSoort()) {
		case DagboekSoortEnum.BANK:
			return new String[] { DetailData.REKENING, DetailData.DC_NUMMER, DetailDataReader.DC_NAAM, DetailData.FACTUUR_NUMMER, DetailData.BEDRAG_EXCL, DetailData.BTW_CODE, DetailData.BEDRAG_BTW, DetailData.BEDRAG_CREDIT, DetailData.BEDRAG_DEBET, DetailData.OMSCHR1, DetailData.OMSCHR2, DetailData.OMSCHR3 };
		case DagboekSoortEnum.KAS:
			return new String[] { DetailData.REKENING, DetailData.DATUM_LEVERING, DetailData.AANTAL, DetailData.EENHEID, DetailData.PRIJS, DetailData.BEDRAG_EXCL, DetailData.BTW_CODE, DetailData.BEDRAG_BTW, DetailData.BEDRAG_INCL, DetailData.OMSCHR1, DetailData.OMSCHR2, DetailData.OMSCHR3 };
		case DagboekSoortEnum.MEMO:
			return new String[] { DetailData.REKENING, DetailData.DC_NUMMER, DetailData.FACTUUR_NUMMER, DetailData.BTW_CODE, DetailData.BEDRAG_BTW, DetailData.BEDRAG_DEBET, DetailData.BEDRAG_CREDIT, DetailData.OMSCHR1, DetailData.OMSCHR2, DetailData.OMSCHR3 };
		case DagboekSoortEnum.INKOOP:
			return new String[] { DetailData.REKENING, DetailData.DATUM_LEVERING, DetailData.AANTAL, DetailData.EENHEID, DetailData.PRIJS, DetailData.BEDRAG_EXCL, DetailData.BTW_CODE, DetailData.BEDRAG_BTW, DetailData.BEDRAG_INCL, DetailData.OMSCHR1, DetailData.OMSCHR2, DetailData.OMSCHR3 };
		case DagboekSoortEnum.VERKOOP:
			return new String[] { DetailData.REKENING, DetailData.DC_NUMMER, DetailData.DATUM_LEVERING, DetailData.AANTAL, DetailData.EENHEID, DetailData.OMSCHR1, DetailData.OMSCHR2, DetailData.OMSCHR3, DetailData.PRIJS, DetailData.BEDRAG_EXCL, DetailData.BTW_CODE, DetailData.BEDRAG_BTW, DetailData.BEDRAG_INCL };
		default:
			return new String[] {};
		}
	}

	public static String[] getColumnNamesForJournals() throws Exception {
		return new String[] { Journaal.DAGBOEK_ID, Journaal.BOEKSTUK, Journaal.BOEKDATUM, Journaal.BOEKPERIODE, Journaal.REKENING_NR, Journaal.DC_NUMMER, Journaal.FACTUUR_NR, Journaal.BTW_CATEGORIE, Journaal.OMSCHR1, Journaal.OMSCHR2, Journaal.OMSCHR3, Journaal.BEDRAG_DEBET, Journaal.BEDRAG_CREDIT };
	}
	// @formatter:on

	public static String getColumnLabelDC(Dagboek dagboek, String dc, boolean isHeaderData) throws Exception {
		switch (dagboek.getSoort()) {
		case DagboekSoortEnum.BANK:
			return dc.equals("D") ? "Credit (Af)" : "Debet (Bij)";
		case DagboekSoortEnum.KAS:
			if (dc.equals("D")) {
				return isHeaderData ? "Opname" : "Storting";
			} else {
				return isHeaderData ? "Storting" : "Opname";
			}
		case DagboekSoortEnum.MEMO:
			return dc.equals("D") ? "Debet" : "Credit";
		default:
			return dc.equals("D") ? "Debet" : "Credit";
		}
	}

	public static UserData getUserData(String userIdOrEmail) throws Exception {
		InitializationRequest request = (InitializationRequest) ESPSessionContext.getSessionAttribute(SessionKeys.INIT_REQUEST);
		if (request == null) {
			return null;
		} else {
			return request.getEnvironment().getUserData(userIdOrEmail);
		}
	}

	public static BigDecimal getBigDecimalFromString(String str, int scale) throws Exception {
		if (str.lastIndexOf(",") > str.lastIndexOf(".")) {
			str = str.replaceAll("\\.", "");
			str = str.replaceAll("\\,", "\\.");
		}
		try {
			return new BigDecimal(str).setScale(scale, BigDecimal.ROUND_HALF_UP);
		} catch (Exception e) {
			return BigDecimal.ZERO;
		}
	}

	public static Object getRelatie(Bedrijf bedrijf, String dc, String dcNr) throws Exception {
		if (dc.equals("C"))
			return getCrediteur(bedrijf, dcNr);
		else if (dc.equals("D"))
			return getDebiteur(bedrijf, dcNr);
		else 
			return null;
	}

	public static Debiteur getDebiteur(Bedrijf bedrijf, String dcNr) throws Exception {
		DebiteurPK key = new DebiteurPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setDebNr(dcNr);
		try {
			return DebiteurManagerFactory.getInstance(bedrijf.getDBData()).findByPrimaryKey(key);
		} catch (Exception e) {
			return null;
		}
	}

	public static Crediteur getCrediteur(Bedrijf bedrijf, String dcNr) throws Exception {
		CrediteurPK key = new CrediteurPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setCredNr(dcNr);
		try {
			return CrediteurManagerFactory.getInstance(bedrijf.getDBData()).findByPrimaryKey(key);
		} catch (Exception e) {
			return null;
		}
	}

	public static String getSchemaName() throws Exception {
		String url = DBData.getDefaultDBData(ApplicationConstants.APPLICATION).getURL();
		return url.substring(url.lastIndexOf("/") + 1);
	}

	public static boolean isColumnLabelsEditable() throws Exception {
		return true;
	}

	public static Collection<?> getDagboeken(Bedrijf bedrijf, String dagboekSoort) throws Exception {
		StringBuilder filter = new StringBuilder();
		filter.append(Dagboek.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' ");
		if (dagboekSoort != null && dagboekSoort.trim().length() > 0) {
			filter.append("AND " + Dagboek.SOORT + "='" + dagboekSoort + "'");
		}
		ExecutableQuery qry = QueryFactory.create((DagboekManager_Impl) DagboekManagerFactory.getInstance(bedrijf.getDBData()), filter.toString());
		return qry.getCollection();
	}

	public static String getDagboekSoort(DBData dbd, String bedrijf, String dagboekId) throws Exception {
		DagboekPK key = new DagboekPK();
		key.setBedrijf(bedrijf);
		key.setId(dagboekId);
		try {
			return DagboekManagerFactory.getInstance(dbd).findByPrimaryKey(key).getSoort();
		} catch (Exception e) {
			return "";
		}
	}

	public static Collection<?> getDebCredRekening(DBData dbd, String bedrijf, String dc) throws Exception {
		StringBuilder filter = new StringBuilder();
		filter.append(Rekening.BEDRIJF + "='" + bedrijf + "' ");
		filter.append("AND " + Rekening.SUB_ADMINISTRATIE_TYPE + "='" + dc + "'");
		ExecutableQuery qry = QueryFactory.create((RekeningManager_Impl) RekeningManagerFactory.getInstance(dbd), filter.toString());
		return qry.getCollection();
	}

	public static String getDebCredRekeningAsString(DBData dbd, String bedrijf, String dc) throws Exception {
		boolean first = true;
		Iterator<?> rekeningNrs = getDebCredRekening(dbd, bedrijf, dc).iterator();
		StringBuffer sb = new StringBuffer("(");
		while (rekeningNrs.hasNext()) {
			sb.append(first ? "" : " OR ");
			sb.append(" REKNR='" + ((Rekening) rekeningNrs.next()).getRekeningNr() + "'");
			first = false;
		}
		sb.append(")");
		return sb.toString();
	}

	public static int getOpenStatus(Dagboek dagboek) throws Exception {
		if (dagboek.getSoort().equals(DagboekSoortEnum.BANK)) {
			return HeaderDataStatusEnum.BANK_OPEN;
		} else if (dagboek.getSoort().equals(DagboekSoortEnum.KAS)) {
			return HeaderDataStatusEnum.KAS_OPEN;
		} else if (dagboek.getSoort().equals(DagboekSoortEnum.MEMO)) {
			return HeaderDataStatusEnum.MEMO_OPEN;
		} else if (dagboek.getSoort().equals(DagboekSoortEnum.INKOOP)) {
			return HeaderDataStatusEnum.INKOOP_OPEN;
		} else if (dagboek.getSoort().equals(DagboekSoortEnum.VERKOOP)) {
			return HeaderDataStatusEnum.VERKOOP_OPEN;
		} else {
			return 0;
		}
	}

	public static int getClosedStatus(Dagboek dagboek) throws Exception {
		if (dagboek.getSoort().equals(DagboekSoortEnum.BANK)) {
			return HeaderDataStatusEnum.BANK_CLOSED;
		} else if (dagboek.getSoort().equals(DagboekSoortEnum.KAS)) {
			return HeaderDataStatusEnum.KAS_CLOSED;
		} else if (dagboek.getSoort().equals(DagboekSoortEnum.MEMO)) {
			return HeaderDataStatusEnum.MEMO_CLOSED;
		} else if (dagboek.getSoort().equals(DagboekSoortEnum.INKOOP)) {
			return HeaderDataStatusEnum.INKOOP_CLOSED;
		} else if (dagboek.getSoort().equals(DagboekSoortEnum.VERKOOP)) {
			return HeaderDataStatusEnum.VERKOOP_CLOSED;
		} else {
			return 0;
		}
	}

    public static Bedrijf getBedrijf(DBData dbd, String bedrijfscode) throws Exception {
		StringBuilder filter = new StringBuilder();
		filter.append("SELECT * FROM Bedrijf");
		filter.append(" WHERE " + Bedrijf.BEDRIJFSCODE + "='" + bedrijfscode + "'");
		FreeQuery qry = QueryFactory.createFreeQuery((BedrijfManager_Impl) BedrijfManagerFactory.getInstance(dbd), filter.toString());
		qry.setMaxObjects(1);
		return (Bedrijf)qry.getFirstObject();
    }

	public static Rekening getRekening(Bedrijf bedrijf, String rekeningNr) throws Exception {
		RekeningPK key = new RekeningPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setRekeningNr(rekeningNr);
		return RekeningManagerFactory.getInstance(bedrijf.getDBData()).findOrCreate(key);
	}

	public static HeaderData copyFavorite(Bedrijf bedrijf, Dagboek dagboek, HeaderData headerData, String newBoekstuk, Date newBoekdatum, int newBoekjaar, int newBoekperiode, String newDcNr)
			throws Exception {
		DBData dbd = bedrijf.getDBData();
		HeaderDataManager hdrMgr = HeaderDataManagerFactory.getInstance(dbd);
		HeaderDataDataBean hdrBean = headerData.getHeaderDataDataBean();
		hdrBean.setBoekstuk(newBoekstuk);
		hdrBean.setBoekdatum(newBoekdatum);
		hdrBean.setBoekjaar(newBoekjaar);
		hdrBean.setBoekperiode(newBoekperiode);
		if (newDcNr != null && newDcNr.length() > 0) {
			hdrBean.setDcNummer(newDcNr);
		}
		hdrBean.setFavoriet(false);
		hdrBean.setStatus(getOpenStatus(dagboek));
		hdrBean.setTotaalBetaald(ApplicationConstants.ZERO);
		hdrBean.setTotaalOpenstaand(hdrBean.getTotaalIncl());
		if (dagboek.isInkoopboek() || dagboek.isVerkoopboek()) {
			hdrBean.setFactuurNummer(hdrBean.getBoekstuk());
			hdrBean.setFactuurdatum(hdrBean.getBoekdatum());
			if (hdrBean.getVervaldatum() != null) {
				Calendar cal = Calendar.getInstance();
				cal.setLenient(true);
				cal.setTime(newBoekdatum);
				cal.add(Calendar.DAY_OF_MONTH, bedrijf.getInstellingen().getStdBetaalTermijn());
				hdrBean.setVervaldatum(cal.getTime());
			}
		} else {
			hdrBean.setFactuurNummer("");
			hdrBean.setFactuurdatum(null);
			hdrBean.setVervaldatum(null);
		}
		HeaderData newHeaderData = hdrMgr.create(hdrBean);

		DetailDataManager dtlMgr = DetailDataManagerFactory.getInstance(dbd);
		DetailDataDataBean dtlBean = null;
		Iterator<?> details = headerData.getDetails().iterator();
		while (details.hasNext()) {
			dtlBean = ((DetailData) details.next()).getDetailDataDataBean();
			dtlBean.setBoekstuk(newBoekstuk);
			dtlMgr.create(dtlBean);
		}
		return newHeaderData;
	}

	public static HeaderData getFactuurRecord(Bedrijf bedrijf, String dcNr, String factuurNr, String dagboekSoort) throws Exception {
		if (bedrijf == null || dcNr.trim().length() == 0 || factuurNr.trim().length() == 0) {
			return null;
		} else {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM HeaderData WHERE ");
			sb.append(HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
			sb.append(HeaderData.DAGBOEK_SOORT + "='" + dagboekSoort + "' AND ");
			sb.append(HeaderData.FACTUUR_NUMMER + "='" + factuurNr + "' AND ");
			sb.append(HeaderData.DC_NUMMER + "='" + dcNr + "' ");
			FreeQuery qry = QueryFactory.createFreeQuery((HeaderDataManager_Impl) HeaderDataManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
			return (HeaderData) qry.getFirstObject();
		}
	}

	public static int genereerNieuwBoekstukRegelNummer(Dagboek dagboek, HeaderData headerData) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT MAX( " + DetailData.BOEKSTUK_REGEL + " ) FROM DetailData WHERE ");
		sb.append(DetailData.BEDRIJF + "='" + dagboek.getBedrijf() + "' AND ");
		sb.append(DetailData.DAGBOEK_ID + "='" + dagboek.getId() + "' AND ");
		if (headerData == null) {
			sb.append(DetailData.BOEKSTUK + "='' ");
		} else {
			sb.append(DetailData.BOEKSTUK + "='" + headerData.getBoekstuk() + "' ");
		}
		FreeQuery max = QueryFactory.createFreeQuery((DetailDataManager_Impl) DetailDataManagerFactory.getInstance(dagboek.getDBData()), sb.toString());
		Object[][] maxRslt = max.getResultArray();
		int regelNr = 1;
		if (maxRslt.length > 0 && maxRslt[0].length > 0) {
			regelNr = (maxRslt[0][0] == null) ? 1 : ((Number) maxRslt[0][0]).intValue() + 1;
		}
		return regelNr;
	}

	public static int genereerNieuwBoekstukRegelNummer(DBData dbd, String bedrijfsCode, String dagboekId, String boekstuk) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT MAX( " + DetailData.BOEKSTUK_REGEL + " ) FROM DetailData WHERE ");
		sb.append(DetailData.BEDRIJF + "='" + bedrijfsCode + "' AND ");
		sb.append(DetailData.DAGBOEK_ID + "='" + dagboekId + "' AND ");
		sb.append(DetailData.BOEKSTUK + "='" + boekstuk + "' ");
		FreeQuery max = QueryFactory.createFreeQuery((DetailDataManager_Impl) DetailDataManagerFactory.getInstance(dbd), sb.toString());
		Object[][] maxRslt = max.getResultArray();
		int regelNr = 1;
		if (maxRslt.length > 0 && maxRslt[0].length > 0) {
			regelNr = (maxRslt[0][0] == null) ? 1 : ((Number) maxRslt[0][0]).intValue() + 1;
		}
		return regelNr;
	}

	public static int genereerNieuwBankAfschriftRunNummer(Bedrijf bedrijf) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT MAX( " + BankAfschrift.RUN_NR + " ) FROM BankAfschrift WHERE ");
		sb.append(BankAfschrift.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' ");
		FreeQuery max = QueryFactory.createFreeQuery((BankAfschriftManager_Impl) BankAfschriftManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		Object[][] maxRslt = max.getResultArray();
		int runNr = 1;
		if (maxRslt.length > 0 && maxRslt[0].length > 0) {
			runNr = (maxRslt[0][0] == null) ? 1 : ((Number) maxRslt[0][0]).intValue() + 1;
		}
		return runNr;
	}

	public static int genereerNieuwBijlageNummer(Bedrijf bedrijf, String dagboekId, String boekstuk) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT MAX( " + Bijlage.VOLG_NR + " ) FROM Bijlage WHERE ");
		sb.append(Bijlage.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		sb.append(Bijlage.DAGBOEK_ID + "='" + dagboekId + "' AND ");
		sb.append(Bijlage.BOEKSTUK + "='" + boekstuk + "' ");
		FreeQuery max = QueryFactory.createFreeQuery((BijlageManager_Impl) BijlageManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		Object[][] maxRslt = max.getResultArray();
		int regelNr = 0;
		if (maxRslt.length > 0 && maxRslt[0].length > 0) {
			regelNr = (maxRslt[0][0] == null) ? 1 : ((Number) maxRslt[0][0]).intValue() + 1;
		}
		return regelNr;
	}

	public static String genereerNieuwBoekstukNummer(Dagboek dagboek, int boekjaar, int periode) throws Exception {
		String boekstuk = dagboek.getBoekstukMask().trim();
		if (boekstuk.length() == 0 || boekstuk.indexOf("##") < 0) {
			return "";
		}
		int p = boekstuk.indexOf("@@@@");
		if (p >= 0) {
			boekstuk = boekstuk.replaceFirst("@@@@", "" + boekjaar);
		}
		p = boekstuk.indexOf("@@");
		if (p >= 0) {
			boekstuk = boekstuk.replaceFirst("@@", "" + ("" + boekjaar).substring(2));
		}
		p = boekstuk.indexOf("&&");
		if (p >= 0) {
			boekstuk = boekstuk.replaceFirst("&&", periode < 10 ? "0" + periode : "" + periode);
		}
		int p1 = boekstuk.indexOf("#");
		int p2 = boekstuk.lastIndexOf("#");
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT MAX(SUBSTRING(BOEKSTUK, " + (p1 + 1) + ", " + (p2 - p1 + 1) + ")) FROM HeaderData WHERE ");
		sb.append("BDR ='" + dagboek.getBedrijf() + "' AND DAGBOEK = '" + dagboek.getId() + "' AND ");
		sb.append("SUBSTRING(BOEKSTUK, 1, " + p1 + ") = '" + boekstuk.substring(0, p1) + "' ");
		FreeQuery qry = QueryFactory.createFreeQuery((HeaderDataManager_Impl) HeaderDataManagerFactory.getInstance(dagboek.getDBData()), sb.toString());
		Object[][] maxRslt = qry.getResultArray();
		int result = 1;
		if (maxRslt.length > 0 && maxRslt[0].length > 0) {
			result = (maxRslt[0][0] == null) ? 1 : Integer.parseInt((String) maxRslt[0][0]) + 1;
		}
		String s1 = boekstuk.substring(p1, p2 + 1);
		String s2 = "000000000000000" + result;
		s2 = s2.substring(s2.length() - s1.length());
		boekstuk = boekstuk.replaceFirst(s1, s2);
		return boekstuk;
	}

	public static String genereerDcNr(DBData dbd, String bedrijf, String dc, String mask) throws Exception {
		int boekjaar = Calendar.getInstance().get(Calendar.YEAR);
		if (mask.length() == 0) {
			return "";
		}
		int p = mask.indexOf("@@@@");
		if (p >= 0) {
			mask = mask.replaceFirst("@@@@", "" + boekjaar);
		}
		p = mask.indexOf("@@");
		if (p >= 0) {
			mask = mask.replaceFirst("@@", "" + ("" + boekjaar).substring(2));
		}
		int p1 = mask.indexOf("#");
		int p2 = mask.lastIndexOf("#");
		String file = dc.toUpperCase().equals("D") ? "Debiteur" : "Crediteur";
		String field = dc.toUpperCase().equals("D") ? "DEBNR" : "CREDNR";
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT MAX(CAST(SUBSTRING(" + field + ", " + (p1 + 1) + ", " + (p2 - p1 + 1) + ") AS UNSIGNED)) FROM " + file + " WHERE ");
		sb.append("BDR ='" + bedrijf + "' AND ");
		sb.append("SUBSTRING(" + field + ", 1, " + p1 + ") = '" + mask.substring(0, p1) + "' ");
		FreeQuery qry = null;
		if (dc.toUpperCase().equals("D")) {
			qry = QueryFactory.createFreeQuery((DebiteurManager_Impl) DebiteurManagerFactory.getInstance(dbd), sb.toString());
		} else {
			qry = QueryFactory.createFreeQuery((CrediteurManager_Impl) CrediteurManagerFactory.getInstance(dbd), sb.toString());
		}
		Object[][] maxRslt = qry.getResultArray();
		int result = 1;
		if (maxRslt.length > 0 && maxRslt[0].length > 0) {
			result = (maxRslt[0][0] == null) ? 1 : ((BigInteger) maxRslt[0][0]).intValue() + 1;
		}
		String s1 = mask.substring(p1, p2 + 1);
		String s2 = "000000000000000" + result;
		s2 = s2.substring(s2.length() - s1.length());
		mask = mask.replaceFirst(s1, s2);
		return mask;
	}

	public static Object[][] executeQuery(String sqlCommand, DBData dbData) throws Exception {
		Object[][] obj = null;
		ResultSet rs = null;
		ConnectionProvider provider = DBPersistenceManager.getConnectionProvider(dbData);
		DBConnection dbConnection = provider.getConnection();
		try {
			rs = dbConnection.executeQuery(sqlCommand);
			obj = (Object[][]) buildObjectArray(rs);
		} catch (Exception e) {
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
			}
			provider.returnConnection(dbConnection);
		}
		return obj;
	}

	private static Object buildObjectArray(ResultSet rs) throws Exception {
		ArrayList<Object> al = new ArrayList<Object>();
		ResultSetMetaData md = rs.getMetaData();
		int cc = md.getColumnCount();
		while (rs.next()) {
			Object[] row = new Object[cc];
			for (int i = 0; i < cc; i++) {
				row[i] = rs.getObject(i + 1);
			}
			al.add(row);
		}
		return al.toArray(new Object[al.size()][cc]);
	}

	public static void executeUpdate(String sqlUpdateCommand, DBData dbData) throws Exception {
		ConnectionProvider provider = DBPersistenceManager.getConnectionProvider(dbData);
		DBConnection dbConnection = provider.getConnection();
		try {
			dbConnection.executeUpdate(sqlUpdateCommand);
		} catch (Exception e) {
		} finally {
			provider.returnConnection(dbConnection);
		}
	}

	@SuppressWarnings("rawtypes")
	public static Object getSelectedItem(AbstractTable table, DataObject object) throws Exception {
		if (!table.isSelectable()) {
			TableRow row = CommonTable.getClickedTableRow(object);
			if (row != null)
				return row.getObject();
			else
				return null;
		}

		Object obj = null;
		ArrayList al = null;
		try {
			al = table.getSelectedObjects();
		} catch (Exception e) {
			throw new UserErrorMessage(e.getMessage());
		}
		if (al.size() == 0) {
			obj = TableHelper.getSelectedItem(object);
			if (obj == null) {
				throw new UserErrorMessage("NothingSelected");
			}
		} else {
			obj = al.get(0);
			if (al.size() > 1) {
				throw new UserErrorMessage("OnlyOneSelectionAllowed");
			}
		}
		if (obj instanceof BusinessObject) {
			((BusinessObject) obj).refreshData();
		}
		return obj;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList getAtLeastOneSelectedItem(AbstractTable table, DataObject object) throws Exception {
		ArrayList al = null;
		try {
			al = table.getSelectedObjects();
		} catch (Exception e) {
			throw new UserErrorMessage(e.getMessage());
		}
		if (al.size() == 0) {
			Object obj = TableHelper.getSelectedItem(object);
			if (obj == null) {
				throw new UserErrorMessage("NothingSelected");
			} else {
				al = new ArrayList();
				al.add(obj);
			}
		}
		return al;
	}

	@SuppressWarnings("rawtypes")
	public static Object getSelectedRow(AbstractTable table, DataObject object) throws Exception {
		Object obj = null;
		ArrayList al = table.getSelectedRows();
		if (al.size() == 0) {
			Action action = (Action) object.getExecutedActionObject();
			if (action instanceof ContextAction) {
				Object contextedUIObject = ((ContextAction) action).getContextedUIObject();
				if (contextedUIObject instanceof TableRow) {
					obj = (TableRow) contextedUIObject;
				}
			}
			if (obj == null && table instanceof CommonTable) {
				obj = CommonTable.getClickedTableRow(object);
			}
			if (obj == null) {
				throw new UserErrorMessage("NothingSelected");
			}
		} else {
			obj = al.get(0);
			if (al.size() > 1) {
				throw new UserErrorMessage("OnlyOneSelectionAllowed");
			}
		}
		return obj;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList getSelectedRows(AbstractTable table, DataObject object) throws Exception {
		ArrayList rows = table.getSelectedRows();
		if (rows.size() == 0) {
			Action action = (Action) object.getExecutedActionObject();
			if (action instanceof ContextAction) {
				Object contextedUIObject = ((ContextAction) action).getContextedUIObject();
				if (contextedUIObject instanceof TableRow) {
					rows.add(contextedUIObject);
				}
			}
		}
		if (rows.size() == 0) {
			throw new UserErrorMessage("NothingSelected");
		}
		return rows;
	}

	public static String[] convertArrayListToStringArray(ArrayList<String> list) throws Exception {
		String[] result = new String[list.size()];
		System.arraycopy(list.toArray(), 0, result, 0, list.size());
		return result;
	}

	public static String toString(String[] s) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < s.length; i++) {
			sb.append(s[i] + (i < s.length - 1 ? ", " : ""));
		}
		sb.append("]");
		return sb.toString();
	}
}