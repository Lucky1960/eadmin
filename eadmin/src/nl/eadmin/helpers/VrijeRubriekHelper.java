package nl.eadmin.helpers;

import java.util.Date;

import nl.eadmin.databeans.VrijeRubriekDefinitie;
import nl.eadmin.db.CodeTabelElementManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.enums.DataTypeEnum;
import nl.eadmin.enums.VrijeRubriekSoortEnum;
import nl.eadmin.ui.uiobjects.referencefields.CodeTabelElementReferenceField;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.NumberField;

public class VrijeRubriekHelper {

	public static VrijeRubriekDefinitie[] getVrijeRubriekDefinities(Dagboek dagboek) throws Exception {
		VrijeRubriekDefinitie[] definitie = new VrijeRubriekDefinitie[5];
		definitie[0] = new VrijeRubriekDefinitie(dagboek == null ? null : dagboek.getVrijeRub1());
		definitie[1] = new VrijeRubriekDefinitie(dagboek == null ? null : dagboek.getVrijeRub2());
		definitie[2] = new VrijeRubriekDefinitie(dagboek == null ? null : dagboek.getVrijeRub3());
		definitie[3] = new VrijeRubriekDefinitie(dagboek == null ? null : dagboek.getVrijeRub4());
		definitie[4] = new VrijeRubriekDefinitie(dagboek == null ? null : dagboek.getVrijeRub5());
		return definitie;
	}

	public static InputComponent[] getInputComponenten(Dagboek dagboek) throws Exception {
		InputComponent[] field = new InputComponent[5];
		for (int i = 0; i < 5; i++) {
			field[i] = getInputComponent(dagboek, i, false);
		}
		return field;
	}

	public static InputComponent getInputComponent(Dagboek dagboek, int index, boolean inSearchPanel) throws Exception {
		InputComponent cmp = null;
		VrijeRubriekDefinitie[] definitie = getVrijeRubriekDefinities(dagboek);
		String label = definitie[index].getPrompt();
		switch (definitie[index].getDataType()) {
		case VrijeRubriekSoortEnum.TABELWAARDE:
			cmp = new CodeTabelElementReferenceField(label, dagboek.getBedrijf(), definitie[index].getTabelNaam(), CodeTabelElementManagerFactory.getInstance(dagboek.getDBData()));
			break;
		case VrijeRubriekSoortEnum.ALFA:
			Field fld = new Field(label);
			fld.setLength(10);
			fld.setMaxLength(10);
			cmp = fld;
			break;
		case VrijeRubriekSoortEnum.NUM:
			cmp = new NumberField(label, 10, false);
			break;
		case VrijeRubriekSoortEnum.DEC:
			cmp = new DecimalField(label, 10, 2, false);
			break;
		case VrijeRubriekSoortEnum.DATUM:
			cmp = new DateSelectionField(label, (Date)null, "dd-MM-yyyy");
			break;
		case VrijeRubriekSoortEnum.BOOL:
			if (inSearchPanel) {
				cmp = new ComboBox(label);
				((ComboBox)cmp).setWidth("50");
				((ComboBox)cmp).addOption("Ja", "true");
				((ComboBox)cmp).addOption("Nee", "false");
			} else {
				cmp = new CheckBox(label);
			}
			break;
		}
		return cmp;
	}

	public static InputComponent getInputComponent(String label, String dataType, int lengte, int decimalen) throws Exception {
		switch (dataType) {
		case DataTypeEnum.ALFA:
			Field fld = new Field(label);
			fld.setLength(lengte);
			fld.setMaxLength(lengte);
			return fld;
		case DataTypeEnum.NUM:
			NumberField numfld = new NumberField(label, lengte, false);
			return numfld;
		case DataTypeEnum.DEC:
			return new DecimalField(label, lengte + 1, decimalen, false);
		case DataTypeEnum.DATUM:
			return new DateSelectionField(label, (Date)null, "dd-MM-yyyy");
		case DataTypeEnum.BOOL:
			return new CheckBox(label);
		}
		return null;
	}

}
