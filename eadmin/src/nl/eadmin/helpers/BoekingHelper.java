package nl.eadmin.helpers;

import java.math.BigDecimal;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.databeans.RekeningTotaalDataBean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataDataBean;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataDataBean;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManager;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.impl.HeaderDataManager_Impl;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.db.impl.RekeningManager_Impl;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.enums.HeaderDataStatusEnum;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.Query;
import nl.ibs.jsql.QueryFactory;

public abstract class BoekingHelper {

	public static void delete(HeaderData headerData) throws Exception {
		DBData dbd = headerData.getDBData();
		String bdr = headerData.getBedrijf();
		String dbk = headerData.getDagboekId();
		String bst = headerData.getBoekstuk();
		StringBuilder whereClause = null;
		whereClause = new StringBuilder();
		whereClause.append(DetailData.BEDRIJF + "='" + bdr + "' AND ");
		whereClause.append(DetailData.DAGBOEK_ID + "='" + dbk + "' AND ");
		whereClause.append(DetailData.BOEKSTUK + "='" + bst + "'");
		DetailDataManagerFactory.getInstance(dbd).generalDelete(whereClause.toString());

		whereClause = new StringBuilder();
		whereClause.append(Bijlage.BEDRIJF + "='" + bdr + "' AND ");
		whereClause.append(Bijlage.DAGBOEK_ID + "='" + dbk + "' AND ");
		whereClause.append(Bijlage.BOEKSTUK + "='" + bst + "'");
		BijlageManagerFactory.getInstance(dbd).generalDelete(whereClause.toString());

		whereClause = new StringBuilder();
		whereClause.append(Journaal.BEDRIJF + "='" + bdr + "' AND ");
		whereClause.append(Journaal.DAGBOEK_ID + "='" + dbk + "' AND ");
		whereClause.append(Journaal.BOEKSTUK + "='" + bst + "'");
		JournaalManagerFactory.getInstance(dbd).generalDelete(whereClause.toString());

		headerData.delete();
	}

	public static void closeBookYear(Bedrijf bedrijf, int boekjaar) throws Exception {
		int nieuwjaar = boekjaar + 1;
		BigDecimal ZERO = ApplicationConstants.ZERO;
		DBData dbd = bedrijf.getDBData();
		Dagboek dagboek = bedrijf.getDagboek("SYS1");
		if (dagboek.getRekeningObject() == null) {
			throw new UserErrorMessage("Afsluiten niet mogelijk: Er is bij dagboek 'SYS1' nog geen rekeningnummer opgegeven!");
		}

		// Schrijf koprecord
		HeaderDataDataBean hdrBean = new HeaderDataDataBean();
		hdrBean.setBedrijf(bedrijf.getBedrijfscode());
		hdrBean.setBoekdatum(PeriodeHelper.getStartDatum(bedrijf, nieuwjaar));
		hdrBean.setBoekjaar(nieuwjaar);
		hdrBean.setBoekperiode(1);
		hdrBean.setBoekstuk(GeneralHelper.genereerNieuwBoekstukNummer(dagboek, nieuwjaar, 1));
		hdrBean.setDagboekId(dagboek.getId());
		hdrBean.setDagboekSoort(DagboekSoortEnum.MEMO);
		hdrBean.setDC("D");
		hdrBean.setOmschr1("Beginbalans boekjaar " + nieuwjaar);
		hdrBean.setStatus(HeaderDataStatusEnum.MEMO_OPEN);
		HeaderData hdrDta = HeaderDataManagerFactory.getInstance(dbd).create(hdrBean);

		// Schrijf detailrecords
		BigDecimal totDebet = ZERO;
		BigDecimal totCredit = ZERO;
		DetailDataManager dtlMgr = DetailDataManagerFactory.getInstance(dbd);
		DetailDataDataBean dtlBean = new DetailDataDataBean();
		dtlBean.setAantal(BigDecimal.ONE);
		dtlBean.setBedragBtw(ZERO);
		dtlBean.setBedrijf(hdrDta.getBedrijf());
		dtlBean.setBoekstuk(hdrDta.getBoekstuk());
		dtlBean.setBtwCode("L");
		dtlBean.setDagboekId(hdrBean.getDagboekId());
		dtlBean.setOmschr1(hdrBean.getOmschr1());

		String rekNr, dcNr, factNr;
		BigDecimal amtD, amtC;
		JournaalManager jrnMgr = JournaalManagerFactory.getInstance(dbd);
		FreeQuery jrnQry = QueryFactory.createFreeQuery((JournaalManager_Impl) jrnMgr);
		RekeningTotaalDataBean totBean = new RekeningTotaalDataBean(bedrijf, boekjaar, jrnQry);
		Rekening rekening = null;
		RekeningManager rekMgr = RekeningManagerFactory.getInstance(dbd);
		Query qry = QueryFactory.create((RekeningManager_Impl) rekMgr, Rekening.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		Iterator<?> iter = rekMgr.getCollection(qry).iterator();
		while (iter.hasNext()) {
			rekening = (Rekening) iter.next();
			if (!rekening.getRekeningSoort().equals(RekeningSoortEnum.BALANS)) {
				continue;
			}
			rekNr = rekening.getRekeningNr();
			dtlBean.setRekening(rekNr);
			if (rekening.getSubAdministratieType().trim().length() == 0) {
				totBean.query(rekNr, "", "");
				write(dtlMgr, dtlBean, totBean);
				totDebet = totDebet.add(totBean.getDebet());
				totCredit = totCredit.add(totBean.getCredit());
			} else {
				StringBuilder filter = new StringBuilder();
				filter.append("SELECT " + Journaal.DC_NUMMER + "," + Journaal.FACTUUR_NR + ", SUM(" + Journaal.BEDRAG_DEBET + ") AS AMTD, SUM(" + Journaal.BEDRAG_CREDIT + ") AS AMTC");
				filter.append("  FROM " + "JOURNAAL");
				filter.append(" WHERE " + Journaal.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
				filter.append("   AND " + Journaal.BOEKJAAR + "=" + boekjaar);
				filter.append("   AND " + Journaal.REKENING_NR + "='" + rekNr + "'");
				filter.append("   AND " + Journaal.DC_NUMMER + "<>''");
				filter.append(" GROUP BY " + Journaal.DC_NUMMER + "," + Journaal.FACTUUR_NR);
				filter.append(" HAVING AMTD <> AMTC");
				jrnQry.setQuery(filter.toString());
				Object[][] rslt = jrnQry.getResultArray();
				if (rslt != null && rslt.length > 0 && rslt[0].length > 0) {
					for (int i = 0; i < rslt.length; i++) {
						dcNr = (rslt[i][0] == null) ? "" : ((String) rslt[i][0]);
						factNr = (rslt[i][1] == null) ? "" : ((String) rslt[i][1]);
						amtD = (rslt[i][2] == null) ? ZERO : ((BigDecimal) rslt[i][2]);
						amtC = (rslt[i][3] == null) ? ZERO : ((BigDecimal) rslt[i][3]);
						totBean.setValues(dcNr, factNr, amtD, amtC);
						write(dtlMgr, dtlBean, totBean);
						totDebet = totDebet.add(totBean.getDebet());
						totCredit = totCredit.add(totBean.getCredit());
					}
				}
			}
		}
		// Schrijf detailrecord met saldo op Resultaatrekening
		if (totDebet.doubleValue() != totCredit.doubleValue()) {
			String dc = totDebet.doubleValue() >= totCredit.doubleValue() ? "C" : "D";
			BigDecimal saldo = totDebet.subtract(totCredit).abs();
			dtlBean.setBoekstukRegel(dtlBean.getBoekstukRegel() + 1);
			dtlBean.setBedragCredit(dc.equals("C") ? saldo : ZERO);
			dtlBean.setBedragDebet(dc.equals("D") ? saldo : ZERO);
			dtlBean.setBedragExcl(saldo);
			dtlBean.setBedragIncl(saldo);
			dtlBean.setDC(dc);
			dtlBean.setDcNummer("");
			dtlBean.setFactuurNummer("");
			dtlBean.setPrijs(saldo);
			dtlBean.setRekening(dagboek.getRekening());
			dtlMgr.create(dtlBean);
		}
		hdrDta.recalculate();
	}

	private static void write(DetailDataManager dtlMgr, DetailDataDataBean dtlBean, RekeningTotaalDataBean totBean) throws Exception {
		BigDecimal saldo = totBean.getSaldo();
		if (saldo.doubleValue() != 0d) {
			String dc = totBean.getDC();
			dtlBean.setBoekstukRegel(dtlBean.getBoekstukRegel() + 1);
			dtlBean.setBedragCredit(dc.equals("C") ? saldo : ApplicationConstants.ZERO);
			dtlBean.setBedragDebet(dc.equals("D") ? saldo : ApplicationConstants.ZERO);
			dtlBean.setBedragExcl(saldo);
			dtlBean.setBedragIncl(saldo);
			dtlBean.setDC(totBean.getDC());
			dtlBean.setDcNummer(totBean.getDcNr());
			dtlBean.setFactuurNummer(totBean.getFactNr());
			dtlBean.setPrijs(saldo);
			dtlMgr.create(dtlBean);
		}
	}

	public static void openBookYear(Bedrijf bedrijf, int boekjaar) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM HeaderData");
		sb.append(" WHERE " + HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		sb.append("   AND " + HeaderData.DAGBOEK_ID + "='SYS1'");
		sb.append("   AND " + HeaderData.BOEKJAAR + "=" + (boekjaar + 1));
		HeaderDataManager hdrMgr = HeaderDataManagerFactory.getInstance(bedrijf.getDBData());
		FreeQuery qry = QueryFactory.createFreeQuery((HeaderDataManager_Impl) hdrMgr, sb.toString());
		Iterator<?> records = qry.getCollection().iterator();
		while (records.hasNext()) {
			delete((HeaderData) records.next());
		}
	}
}