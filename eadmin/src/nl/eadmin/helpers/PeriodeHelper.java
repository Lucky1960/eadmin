package nl.eadmin.helpers;

import java.util.Date;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManager;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManager;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.PeriodePK;
import nl.eadmin.db.impl.HeaderDataManager_Impl;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.db.impl.PeriodeManager_Impl;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public abstract class PeriodeHelper {

	public static Date getStartDatum(Bedrijf bedrijf, int boekjaar) throws Exception {
		PeriodeManager periodMgr = PeriodeManagerFactory.getInstance(bedrijf.getDBData());
		StringBuilder filter = new StringBuilder();
		filter.append("SELECT MIN(" + Periode.BOEKPERIODE + ") FROM Periode");
		filter.append(" WHERE " + Periode.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		filter.append("   AND " + Periode.BOEKJAAR + "=" + boekjaar);
		FreeQuery qry = QueryFactory.createFreeQuery((PeriodeManager_Impl) periodMgr, filter.toString());
		Object[][] rslt = qry.getResultArray();
		if (rslt != null && rslt.length > 0 && rslt[0].length > 0) {
			PeriodePK key = new PeriodePK();
			key.setBedrijf(bedrijf.getBedrijfscode());
			key.setBoekjaar(boekjaar);
			key.setBoekperiode(((Integer) rslt[0][0]).intValue());
			return periodMgr.findOrCreate(key).getStartdatum();
		}
		return null;
	}

	public static Date getEindDatum(Bedrijf bedrijf, int boekjaar) throws Exception {
		PeriodeManager periodMgr = PeriodeManagerFactory.getInstance(bedrijf.getDBData());
		StringBuilder filter = new StringBuilder();
		filter.append("SELECT MAX(" + Periode.BOEKPERIODE + ") FROM Periode");
		filter.append(" WHERE " + Periode.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		filter.append("   AND " + Periode.BOEKJAAR + "=" + boekjaar);
		FreeQuery qry = QueryFactory.createFreeQuery((PeriodeManager_Impl) periodMgr, filter.toString());
		Object[][] rslt = qry.getResultArray();
		if (rslt != null && rslt.length > 0 && rslt[0].length > 0) {
			PeriodePK key = new PeriodePK();
			key.setBedrijf(bedrijf.getBedrijfscode());
			key.setBoekjaar(boekjaar);
			key.setBoekperiode(((Integer) rslt[0][0]).intValue());
			return periodMgr.findOrCreate(key).getEinddatum();
		}
		return null;
	}

	public static int getFirstOpenBookYear(Bedrijf bedrijf) throws Exception {
		JournaalManager jrnMgr = JournaalManagerFactory.getInstance(bedrijf.getDBData());
		StringBuilder filter = new StringBuilder();
		filter.append("SELECT MAX(" + Journaal.BOEKJAAR + ") FROM Journaal");
		filter.append(" WHERE " + Journaal.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		filter.append("   AND " + Journaal.DAGBOEK_ID + "='SYS1'");
		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) jrnMgr, filter.toString());
		Object[][] rslt = qry.getResultArray();
		if (rslt != null && rslt.length > 0 && rslt[0].length > 0 && rslt[0][0] != null) {
			return ((Integer) rslt[0][0]).intValue();
		} else {
			HeaderDataManager hdrMgr = HeaderDataManagerFactory.getInstance(bedrijf.getDBData());
			filter = new StringBuilder();
			filter.append("SELECT MIN(" + HeaderData.BOEKJAAR + ") FROM HeaderData");
			filter.append(" WHERE " + HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
			qry = QueryFactory.createFreeQuery((HeaderDataManager_Impl) hdrMgr, filter.toString());
			rslt = qry.getResultArray();
			if (rslt != null && rslt.length > 0 && rslt[0].length > 0 && rslt[0][0] != null) {
				return ((Integer) rslt[0][0]).intValue();
			} else {
				return 0;
			}
		}
	}

	public static int getLastClosedBookYear(Bedrijf bedrijf) throws Exception {
		JournaalManager jrnMgr = JournaalManagerFactory.getInstance(bedrijf.getDBData());
		StringBuilder filter = new StringBuilder();
		filter.append("SELECT MAX(" + Journaal.BOEKJAAR + ") FROM Journaal");
		filter.append(" WHERE " + Journaal.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		filter.append("   AND " + Journaal.DAGBOEK_ID + "='SYS1'");
		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) jrnMgr, filter.toString());
		Object[][] rslt = qry.getResultArray();
		if (rslt != null && rslt.length > 0 && rslt[0].length > 0 && rslt[0][0] != null) {
			return ((Integer) rslt[0][0]).intValue() - 1;
		} else {
			return 0;
		}
	}

	public static boolean isClosed(Bedrijf bedrijf, int boekjaar) throws Exception {
		StringBuilder filter = new StringBuilder();
		filter.append("SELECT * FROM Journaal");
		filter.append(" WHERE " + Journaal.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		filter.append("   AND " + Journaal.DAGBOEK_ID + "='SYS1'");
		filter.append("   AND " + Journaal.BOEKJAAR + "=" + (boekjaar + 1));
		JournaalManager jrnMgr = JournaalManagerFactory.getInstance(bedrijf.getDBData());
		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) jrnMgr, filter.toString());
		qry.setMaxObjects(1);
		return qry.getCollection().size() > 0;
	}

	public static boolean isOpen(Bedrijf bedrijf, int boekjaar) throws Exception {
		return !isClosed(bedrijf, boekjaar);
	}
}