package nl.eadmin.helpers;

import java.util.HashMap;

public class TemplateHelper {
	public static final String AANMANING_HTML = "/nl/eadmin/templates/Aanmaning.html";
	public static final String BALANSOVERZICHT_HTML = "/nl/eadmin/templates/Balans.html";
	public static final String BTW_AANGIFTE_HTML = "/nl/eadmin/templates/BTWAangifte.html";
	public static final String CREDITEURENOVERZICHT_HTML = "/nl/eadmin/templates/Crediteurenoverzicht.html";
	public static final String DEBITEURENOVERZICHT_HTML = "/nl/eadmin/templates/Debiteurenoverzicht.html";
	public static final String FACTUUR_HTML = "/nl/eadmin/templates/Factuur.html";
	public static final String GROOTBOEKOVERZICHT_HTML = "/nl/eadmin/templates/Grootboekoverzicht.html";

	private static HashMap<String, String> templateCache = new HashMap<String, String>();

	public static String getTemplate(String path) throws Exception {
		String template = templateCache.get(path);
		if (template == null) {
			template = StringHelper.convertStreamToString(TemplateHelper.class.getResourceAsStream(path));
			templateCache.put(path, template);
		}
		return template;
	}
}