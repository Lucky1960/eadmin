package nl.eadmin.helpers;

import java.util.HashMap;
import java.util.Iterator;

import nl.eadmin.db.Autorisatie;
import nl.eadmin.db.AutorisatieManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Gebruiker;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.util.NameValuePair;

public abstract class UserAutHelper {
	public static final String SEPARATOR = "##";

// @formatter:off
	public static final String OND_USR = "OND_USR";
	public static final String ACT_USR = "ACT_USR";
	public static final String UPD_APP = "UPD_APP";
	public static final String RST_JRN = "RST_JRN";
	public static final String RST_DBLVL = "RST_DBLVL";
	public static final String OND_AUT = "OND_AUT";
	public static final String OND_HFDVERD = "OND_HFDVERD";
	public static final String OND_VERDICHT = "OND_VERDICHT";
	public static final String OND_REKSCHEMA = "OND_REKSCHEMA";
	public static final String OND_DAGBOEK = "OND_DAGBOEK";
	public static final String OND_CODETBL = "OND_CODETBL";
	public static final String OND_BTWCODE = "OND_BTWCODE";
	public static final String OND_DEB = "OND_DEB";
	public static final String OND_CRED = "OND_CRED";
	public static final String OVZ_GROOTB = "OVZ_GROOTB";
	public static final String OVZ_DEB = "OVZ_DEB";
	public static final String OVZ_CRED = "OVZ_CRED";
	public static final String OVZ_BTW = "OVZ_BTW";
	public static final String EXP_SYS = "EXP_SYS";
	public static final String IMP_SYS = "IMP_SYS";
	public static final String SEL_BOEKJR = "SEL_BOEKJR";
	
	public static final NameValuePair[] MNU_OPTIONS = new NameValuePair[] {
		new NameValuePair(OND_USR, "Gebruikers"),
		new NameValuePair(ACT_USR, "Actieve gebruikers"),
		new NameValuePair(UPD_APP, "Update applicatie"),
		new NameValuePair(RST_JRN, "Reset journaalposten"),
		new NameValuePair(RST_DBLVL, "Reset DB-level"),
		new NameValuePair(OND_AUT, "Menu-autorisatie"),
		new NameValuePair(OND_HFDVERD, "Hoofverdichtingen"),
		new NameValuePair(OND_VERDICHT, "Verdichtingen"),
		new NameValuePair(OND_REKSCHEMA, "Grootboekrekeningen"),
		new NameValuePair(OND_DAGBOEK, "Dagboeken"),
		new NameValuePair(OND_CODETBL, "CodeTabellen"),
		new NameValuePair(OND_BTWCODE, "BTW-codes"),
		new NameValuePair(OND_DEB, "Debiteuren"),
		new NameValuePair(OND_CRED, "Crediteuren"),
		new NameValuePair(OVZ_GROOTB, "Grootboek"),
		new NameValuePair(OVZ_DEB, "Debiteuren"),
		new NameValuePair(OVZ_CRED, "Crediteuren"),
		new NameValuePair(OVZ_BTW, "BTW-aangifte"),
		new NameValuePair(EXP_SYS, "Exporteren systeembestand"),
		new NameValuePair(IMP_SYS, "Importeren systeembestand"),
		new NameValuePair(SEL_BOEKJR, "Selecteer boekjaar")
	};
// @formatter:on

	// public static final NameValuePair OND_USR = new NameValuePair("OND_USR",
	// "Gebruikers");
	// public static final NameValuePair ACT_USR = new NameValuePair("ACT_USR",
	// "Actieve gebruikers");
	// public static final NameValuePair UPD_APP = new NameValuePair("UPD_APP",
	// "Update applicatie");
	// public static final NameValuePair RST_JRN = new NameValuePair("RST_JRN",
	// "Reset journaalposten");
	// public static final NameValuePair RST_DBLVL = new
	// NameValuePair("RST_DBLVL", "Reset DB-level");
	// public static final NameValuePair OND_AUT = new NameValuePair("OND_AUT",
	// "Menu-autorisatie");
	// public static final NameValuePair OND_HFDVERD = new
	// NameValuePair("OND_HFDVERD", "Hoofverdichtingen");
	// public static final NameValuePair OND_VERDICHT = new
	// NameValuePair("OND_VERDICHT", "Verdichtingen");
	// public static final NameValuePair OND_REKSCHEMA = new
	// NameValuePair("OND_REKSCHEMA", "Grootboekrekeningen");
	// public static final NameValuePair OND_DAGBOEK = new
	// NameValuePair("OND_DAGBOEK", "Dagboeken");
	// public static final NameValuePair OND_CODETBL = new
	// NameValuePair("OND_CODETBL", "CodeTabellen");
	// public static final NameValuePair OND_BTWCODE = new
	// NameValuePair("OND_BTWCODE", "BTW-codes");
	// public static final NameValuePair OND_DEB = new NameValuePair("OND_DEB",
	// "Debiteuren");
	// public static final NameValuePair OND_CRED = new
	// NameValuePair("OND_CRED", "Crediteuren");
	// public static final NameValuePair MNU = new NameValuePair("OVZ_GROOTB",
	// "Grootboek");
	// public static final NameValuePair OVZ_GROOTB = new
	// NameValuePair("OVZ_DEB", "Debiteuren");
	// public static final NameValuePair OVZ_CRED = new
	// NameValuePair("OVZ_CRED", "Crediteuren");
	// public static final NameValuePair OVZ_BTW = new NameValuePair("OVZ_BTW",
	// "BTW-aangifte");
	// public static final NameValuePair EXP_SYS = new NameValuePair("EXP_SYS",
	// "Exporteren systeembestand");
	// public static final NameValuePair IMP_SYS = new NameValuePair("IMP_SYS",
	// "Importeren systeembestand");
	// public static final NameValuePair SEL_BOEKJR = new
	// NameValuePair("SEL_BOEKJR", "Selecteer boekjaar");
	// public static final NameValuePair SEL_THEMA = new
	// NameValuePair("SEL_THEMA", "Thema");

	public static HashMap<String, String> getMenuOptions() throws Exception {
		HashMap<String, String> options = new HashMap<String, String>();
		options.put("Users", "Gebruikers");
		options.put("ActUsers", "Actieve gebruikers");
		options.put("UpdApp", "Update applicatie");
		options.put("ResetJrn", "Reset journaalposten");
		options.put("ResetDB", "Reset");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		options.put("", "");
		return options;
	}

	public static String getKey(Object object) throws Exception {
		// if (object instanceof MatchSource) {
		// return "" + ((MatchSource) object).getMatchSourceId();
		// }
		return "";
	}

	public static boolean isAuthorisedForButton(String authorisations, String buttonName) throws Exception {
		if (authorisations == null) {
			return false;
		} else {
			StringBuffer sb = new StringBuffer(buttonName).append("=true");
			return authorisations.indexOf(sb.toString()) >= 0;
		}
	}

	public static boolean isAuthorisedForMenu(String authorisations, NameValuePair menuOption) throws Exception {
		if (authorisations == null) {
			return false;
		} else {
			StringBuffer sb = new StringBuffer(menuOption.getName()).append("=true");
			return authorisations.indexOf(sb.toString()) >= 0;
		}
	}

	public static boolean isAuthorisedForMatchSource(String authorisations, int matchSourceId) throws Exception {
		if (authorisations == null) {
			return false;
		} else {
			StringBuffer sb = new StringBuffer(matchSourceId).append("=true");
			return authorisations.indexOf(sb.toString()) >= 0;
		}
	}

	public static String getAdminFilter(DBData dbd, Gebruiker gebruiker) throws Exception {
		StringBuilder filter = new StringBuilder();
		if (gebruiker.isAdmin()) {
			filter.append(Bedrijf.BEDRIJFSCODE + "<>'#@#DUMMY#@#'");
		} else {
			String sb = Autorisatie.GEBRUIKER_ID + "='" + gebruiker.getId() + "'";
			Iterator<?> autorisaties = AutorisatieManagerFactory.getInstance(dbd).getCollection(QueryFactory.create(sb.toString(), Autorisatie.BEDRIJF)).iterator();
			while (autorisaties.hasNext()) {
				String bedrijf = ((Autorisatie) autorisaties.next()).getBedrijf();
				filter.append(filter.length() == 0 ? "" : " || ");
				filter.append(Bedrijf.BEDRIJFSCODE + "='" + bedrijf + "'");
			}
		}
		return filter.toString();
	}
}