package nl.eadmin.helpers;

import java.io.Serializable;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateManager;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.db.HtmlTemplatePK;
import nl.eadmin.db.impl.HtmlTemplateManager_Impl;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.QueryFactory;

public class HtmlTemplateHelper implements Serializable {
	private static final long serialVersionUID = 1L;

	public static HtmlTemplate createStdHtmlTemplate(Bedrijf bedrijf) throws Exception {
//		String fileName = URLDecoder.decode(bedrijf.getClass().getClassLoader().getResource(bedrijf.getClass().getName().replace('.', '/') + ".class").getPath()).replace('\\', '/');
//		fileName = fileName.substring(0, fileName.indexOf("/WEB-INF/")).trim() + "/eadmin/skin/login.png";
//		InputStream is = new FileInputStream(new File(fileName));
		String id = "STD_LAYOUT";
		HtmlTemplateManager layoutManager = HtmlTemplateManagerFactory.getInstance(bedrijf.getDBData());
		HtmlTemplatePK htmlTemplatePK = new HtmlTemplatePK();
		htmlTemplatePK.setBedrijf(bedrijf.getBedrijfscode());
		htmlTemplatePK.setId(id);
		HtmlTemplate htmlTemplate = null;
		try {
			htmlTemplate = layoutManager.findByPrimaryKey(htmlTemplatePK);
		} catch (Exception e) {
			htmlTemplate = layoutManager.create(bedrijf.getBedrijfscode(), id);
			htmlTemplate.setOmschrijving("Standaard layout");
			htmlTemplate.setIsDefault(true);
//			htmlTemplate.setLogo(StringHelper.getBytes(is));
			htmlTemplate.setHtmlString(TemplateHelper.getTemplate(TemplateHelper.FACTUUR_HTML));
		}
		return htmlTemplate;
	}

	public static HtmlTemplate getStdLayout(Bedrijf bedrijf) throws Exception {
		HtmlTemplateManager mgr = HtmlTemplateManagerFactory.getInstance(bedrijf.getDBData());
		StringBuilder filter = new StringBuilder();
		filter.append(HtmlTemplate.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		filter.append(HtmlTemplate.IS_DEFAULT + "= 'true'");
		ExecutableQuery qry = QueryFactory.create((HtmlTemplateManager_Impl) mgr, filter.toString());
		HtmlTemplate htmlTemplate = (HtmlTemplate) qry.getFirstObject();
		if (htmlTemplate == null) {
			filter = new StringBuilder();
			filter.append(HtmlTemplate.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' ");
			qry = QueryFactory.create((HtmlTemplateManager_Impl) mgr, filter.toString());
			htmlTemplate = (HtmlTemplate) qry.getFirstObject();
			if (htmlTemplate == null) {
				htmlTemplate = createStdHtmlTemplate(bedrijf);
			}
			htmlTemplate.setIsDefault(true);
		}
		return htmlTemplate;
	}

}
