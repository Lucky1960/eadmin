package nl.eadmin.helpers;

import java.net.URLDecoder;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.HtmlTemplate;
import nl.ibs.esp.uiobjects.Image;
import nl.ibs.esp.uiobjects.UserMessageException;

import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;

public class LogoHelper {
	public static final String PRINT = "P";
	public static final String SCHERM = "S";

	public static Image getEmptyImage(int height, int width) throws Exception {
		Image img = new Image("");
		img.setSource("question-mark.png");
		img.setAlignment("center");
		img.setHeight("" + height);
		img.setWidth("" + width);
		img.getSource();
		return img;
	}
//LL
//	public static Image getLogo(Bedrijf bedrijf, String type, int height, int width) throws Exception {
//		Image img = null;
//		if (bedrijf == null) {
//			img = getEmptyImage(height, width);
//		} else {
//			byte[] bytes = type.equals(PRINT) ? bedrijf.getLogoPrint() : bedrijf.getLogoScherm();
//			if (bytes == null || bytes.length == 0) {
//				img = getEmptyImage(height, width);
//			} else {
//				ContentInfo fileInfo = new ContentInfoUtil().findMatch(bytes);
//				img = new Image(bytes, getLogoNaam(bedrijf, type, false), fileInfo == null ? "image/bmp" : fileInfo.getMimeType());
//			}
//		}
//		img.setAlignment("center");
//		img.setHeight("" + height);
//		img.setWidth("" + width);
//		return img;
//	}

	public static Image getLogo(HtmlTemplate htmlTemplate, int height, int width) throws Exception {
		Image img = null;
		if (htmlTemplate == null) {
			img = getEmptyImage(height, width);
		} else {
			byte[] bytes = htmlTemplate.getLogo();
			if (bytes == null || bytes.length == 0) {
				img = getEmptyImage(height, width);
			} else {
				ContentInfo fileInfo = new ContentInfoUtil().findMatch(bytes);
				img = new Image(bytes, "Logo_" + htmlTemplate.getBedrijf() + "_" + htmlTemplate.getId(), fileInfo == null ? "image/bmp" : fileInfo.getMimeType());
			}
		}
		img.setAlignment("center");
		img.setHeight("" + height);
//		img.setWidth("" + width);
		return img;
	}

	@SuppressWarnings("deprecation")
	public static String getLogoNaam(Bedrijf bedrijf, String type, boolean includePath) throws Exception {
		String path = URLDecoder.decode(bedrijf.getClass().getClassLoader().getResource(bedrijf.getClass().getName().replace('.', '/') + ".class").getPath()).replace('\\', '/');
		path = includePath ? path.substring(0, path.indexOf("/WEB-INF/")).trim() + "/images/" : "";
		switch (type) {
		case (PRINT):
			return path + "PrintLogo_" + bedrijf.getBedrijfscode() + ".png";
		case (SCHERM):
			return path + "SchermLogo_" + bedrijf.getBedrijfscode() + ".png";
		default:
			throw new UserMessageException("Ongeldig logo-type: " + type);
		}
	}

	public static boolean isValidFileName(String naam) throws Exception {
		if (naam == null) {
			return false;
		} else {
			String lowerNaam = naam.toLowerCase();
			return lowerNaam.endsWith(".jpg") || lowerNaam.endsWith(".png") || lowerNaam.endsWith(".bmp");
		}
	}
}
