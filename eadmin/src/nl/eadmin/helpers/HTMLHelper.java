package nl.eadmin.helpers;

import java.io.Serializable;

import nl.eadmin.databeans.AanmaningDataBean;
import nl.eadmin.databeans.BTWAangifteDataBean;
import nl.eadmin.databeans.BalansOverzichtDataBean;
import nl.eadmin.databeans.DebCredOverzichtDataBean;
import nl.eadmin.databeans.FactuurDataBean;
import nl.eadmin.databeans.GrootboekoverzichtDatabean;
import nl.ibs.esp.reporting.DataSource;
import nl.ibs.esp.reporting.ObjectDataSource;
import nl.ibs.esp.reporting.XHTMLMerger;
import nl.ibs.esp.util.CommonObjectDataReader;
import nl.ibs.esp.util.ObjectDataReader;

public class HTMLHelper implements Serializable {
	private static final long serialVersionUID = 1L;
	private static String btwTemplate;
	private static ObjectDataReader btwAangifteReader;

	// private static String webAppURL;

	public static String getBTWAangifteHTML(final BTWAangifteDataBean btwAangifteDataBean) throws Exception {
		if (btwAangifteReader == null)
			btwAangifteReader = CommonObjectDataReader.getInstance(BTWAangifteDataBean.class);
		if (btwTemplate == null) {
			// btwTemplate =
			// StringHelper.convertStreamToString(MailHelper.class.getResourceAsStream("/nl/eadmin/templates/BTWAangifte.html"));
			btwTemplate = TemplateHelper.getTemplate(TemplateHelper.BTW_AANGIFTE_HTML);
		}
		return new XHTMLMerger().process(new DataSource() {
			private static final long serialVersionUID = 1L;

			public boolean hasNext(String level) throws Exception {
				return false;
			}

			public Object getValue(String key) throws Exception {
				return btwAangifteReader.getValue(btwAangifteDataBean, key);
			}
		}, btwTemplate);
	}

	public static String getAanmaningOverzichtHTML(final AanmaningDataBean aanmaningDataBean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.AANMANING_HTML);
		return new XHTMLMerger().process(new ObjectDataSource(aanmaningDataBean), template);
	}

	public static String getGrootboekOverzichtHTML(final GrootboekoverzichtDatabean grootboekoverzichtDatabean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.GROOTBOEKOVERZICHT_HTML);
		return new XHTMLMerger().process(new ObjectDataSource(grootboekoverzichtDatabean), template);
	}

	public static String getBalansOverzichtHTML(final BalansOverzichtDataBean balansOverzichtDataBean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.BALANSOVERZICHT_HTML);
		return new XHTMLMerger().process(new ObjectDataSource(balansOverzichtDataBean), template);
	}

	public static String getDebiteurenOverzichtHTML(final DebCredOverzichtDataBean debCredOverzichtDataBean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.DEBITEURENOVERZICHT_HTML);
		return new XHTMLMerger().process(new ObjectDataSource(debCredOverzichtDataBean), template);
	}

	public static String getCrediteurenOverzichtHTML(final DebCredOverzichtDataBean debCredOverzichtDataBean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.CREDITEURENOVERZICHT_HTML);
		return new XHTMLMerger().process(new ObjectDataSource(debCredOverzichtDataBean), template);
	}

	public static String getFactuurHTML(final FactuurDataBean factuurDatabean) throws Exception {
		byte[] ftBytes = factuurDatabean.getFactuurTemplate();
		String template = ftBytes == null || ftBytes.length < 15 ? TemplateHelper.getTemplate(TemplateHelper.FACTUUR_HTML) : new String(ftBytes, "UTF8");
		return new XHTMLMerger().process(new ObjectDataSource(factuurDatabean), template);
	}

	/*
	 * public static String getWebAppURL() { if (webAppURL==null) webAppURL=
	 * ESPSessionContext.getConfiguration().getWebAppURL(); return webAppURL; }
	 */

}
