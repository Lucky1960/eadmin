package nl.eadmin.helpers;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Adres;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.CodeTabel;
import nl.eadmin.db.CodeTabelElement;
import nl.eadmin.db.CodeTabelElementManagerFactory;
import nl.eadmin.db.CodeTabelManagerFactory;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Hoofdverdichting;
import nl.eadmin.db.HoofdverdichtingManagerFactory;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.db.Instellingen;
import nl.eadmin.db.InstellingenManagerFactory;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.Product;
import nl.eadmin.db.ProductManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.Verdichting;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.enums.ImportTypeEnum;
import nl.eadmin.ui.adapters.ExportPanel;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.Query;
import nl.ibs.jsql.QueryFactory;

public class ExportHelper {
	public static final String FILE_ID = "SYSTEEMBESTAND";
	public static final String FLD_SEP = "@~@";
	public static final String RCD_SEP = "@#@";
	public static final String BLANKS = "@_@";
	public static final String NULL = "@NULL@";
	private Bedrijf bedrijf;
	private DBData dbd;
	private StringBuffer exportString;
	private String bedrijfsCode;
	private Query query;
	private Collection<?> collection;
	private Iterator<?> records;

	public String export(ExportPanel exportPanel) throws Exception {
		this.bedrijf = exportPanel.getBedrijf();
		this.bedrijfsCode = bedrijf.getBedrijfscode();
		this.dbd = bedrijf.getDBData();
		this.exportString = new StringBuffer();
		exportPrefix();
		if (exportPanel.getValue(ImportTypeEnum.ADRES)) {
			exportAdres();
		}
		if (exportPanel.getValue(ImportTypeEnum.BEDRIJF)) {
			exportBedrijf();
		}
		if (exportPanel.getValue(ImportTypeEnum.BIJLAGE)) {
			exportBijlage();
		}
		if (exportPanel.getValue(ImportTypeEnum.BTWCATEGORIE)) {
			exportBtwCategorie();
		}
		if (exportPanel.getValue(ImportTypeEnum.BTWCODE)) {
			exportBtwCode();
		}
		if (exportPanel.getValue(ImportTypeEnum.CODETABEL)) {
			exportCodeTabel();
		}
		if (exportPanel.getValue(ImportTypeEnum.CODETABELELM)) {
			exportCodeTabelElementen();
		}
		if (exportPanel.getValue(ImportTypeEnum.CREDITEUR)) {
			exportCrediteur();
		}
		if (exportPanel.getValue(ImportTypeEnum.DAGBOEK)) {
			exportDagboek();
		}
		if (exportPanel.getValue(ImportTypeEnum.DEBITEUR)) {
			exportDebiteur();
		}
		if (exportPanel.getValue(ImportTypeEnum.DETAILDATA)) {
			exportDetailData();
		}
		if (exportPanel.getValue(ImportTypeEnum.HEADERDATA)) {
			exportHeaderData();
		}
		if (exportPanel.getValue(ImportTypeEnum.HFDVERDICHTING)) {
			exportHfdVerdichting();
		}
		if (exportPanel.getValue(ImportTypeEnum.HTMLTEMPLATE)) {
			exportHtmlTemplates();
		}
		if (exportPanel.getValue(ImportTypeEnum.INSTELLING)) {
			exportInstellingen();
		}
		if (exportPanel.getValue(ImportTypeEnum.PERIODE)) {
			exportPeriode();
		}
		if (exportPanel.getValue(ImportTypeEnum.PRODUCT)) {
			exportProduct();
		}
		if (exportPanel.getValue(ImportTypeEnum.REKENING)) {
			exportRekeningen();
		}
		if (exportPanel.getValue(ImportTypeEnum.VERDICHTING)) {
			exportVerdichting();
		}
		return exportString.toString();
	}

	private void exportPrefix() throws Exception {
		append(FILE_ID);
		append(ApplicationConstants.APPLICATION_VERSION);
		append("" + System.currentTimeMillis());
		append(bedrijf.getOmschrijving());
		append(RCD_SEP);
	}

	private void exportAdres() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Adres.BEDRIJF + "='" + bedrijfsCode + "'");
		Adres record = null;
		query = QueryFactory.create(Bedrijf.class, sb.toString());
		collection = AdresManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Adres) records.next();
			append(ImportTypeEnum._ADRES);
			append(record.getDc());
			append(record.getDcNr());
			append(record.getAdresType());
			append(record.getStraat());
			append(record.getHuisNr());
			append(record.getHuisNrToev());
			append(record.getExtraAdresRegel());
			append(record.getPostcode());
			append(record.getPlaats());
			append(record.getLand());
			append(record.getTelefoon());
			append(record.getMobiel());
			append(RCD_SEP);
		}
	}

	private void exportBedrijf() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Bedrijf.BEDRIJFSCODE + "='" + bedrijfsCode + "'");
		Bedrijf record = null;
		query = QueryFactory.create(Bedrijf.class, sb.toString());
		collection = BedrijfManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Bedrijf) records.next();
			append(ImportTypeEnum._BEDRIJF);
			append(record.getOmschrijving());
			append(record.getEmail());
			append(record.getKvkNr());
			append(record.getBtwNr());
			append(record.getIbanNr());
			append(record.getBic());
			append(RCD_SEP);
		}
	}

	private void exportBijlage() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Bijlage.BEDRIJF + "='" + bedrijfsCode + "'");
		Bijlage record = null;
		query = QueryFactory.create(Bijlage.class, sb.toString());
		collection = BijlageManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Bijlage) records.next();
			append(ImportTypeEnum._BIJLAGE);
			append(record.getDagboekId());
			append(record.getBoekstuk());
			append(record.getVolgNr());
			append(record.getBestandsnaam());
			append(record.getOmschrijving());
			append(RCD_SEP);
		}
	}

	private void exportBtwCategorie() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(BtwCategorie.BEDRIJF + "='" + bedrijfsCode + "'");
		BtwCategorie record = null;
		query = QueryFactory.create(BtwCategorie.class, sb.toString());
		collection = BtwCategorieManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (BtwCategorie) records.next();
			append(ImportTypeEnum._BTWCATEGORIE);
			append(record.getCode());
			append(record.getOmschrijving());
			append(record.getRekeningNr());
			append(RCD_SEP);
		}
	}

	private void exportBtwCode() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(BtwCode.BEDRIJF + "='" + bedrijfsCode + "'");
		BtwCode record = null;
		query = QueryFactory.create(BtwCode.class, sb.toString());
		collection = BtwCodeManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (BtwCode) records.next();
			append(ImportTypeEnum._BTWCODE);
			append(record.getCode());
			append(record.getOmschrijving());
			append(record.getPercentage());
			append(record.getInEx());
			append(record.getBtwCatInkoop());
			append(record.getBtwCatVerkoop());
			append(RCD_SEP);
		}
	}

	private void exportCodeTabel() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(CodeTabel.BEDRIJF + "='" + bedrijfsCode + "'");
		CodeTabel record = null;
		query = QueryFactory.create(CodeTabel.class, sb.toString());
		collection = CodeTabelManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (CodeTabel) records.next();
			append(ImportTypeEnum._CODETABEL);
			append(record.getTabelNaam());
			append(record.getOmschrijving());
			append(record.getDataType());
			append(record.getLengte());
			append(record.getDecimalen());
			append(RCD_SEP);
		}
	}

	private void exportCodeTabelElementen() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(CodeTabelElement.BEDRIJF + "='" + bedrijfsCode + "'");
		CodeTabelElement record = null;
		query = QueryFactory.create(CodeTabelElement.class, sb.toString());
		collection = CodeTabelElementManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (CodeTabelElement) records.next();
			append(ImportTypeEnum._CODETBLELM);
			append(record.getTabelNaam());
			append(record.getId());
			append(record.getOmschrijving());
			append(RCD_SEP);
		}
	}

	private void exportCrediteur() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Crediteur.BEDRIJF + "='" + bedrijfsCode + "'");
		Crediteur record = null;
		query = QueryFactory.create(Crediteur.class, sb.toString());
		collection = CrediteurManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Crediteur) records.next();
			append(ImportTypeEnum._CREDITEUR);
			append(record.getCredNr());
			append(record.getNaam());
			append(record.getContactPersoon());
			append(record.getEmail());
			append(record.getKvkNr());
			append(record.getBtwNr());
			append(record.getIbanNr());
			append(RCD_SEP);
		}
	}

	private void exportDagboek() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Dagboek.BEDRIJF + "='" + bedrijfsCode + "'");
		Dagboek record = null;
		query = QueryFactory.create(Dagboek.class, sb.toString());
		collection = DagboekManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Dagboek) records.next();
			append(ImportTypeEnum._DAGBOEK);
			append(record.getId());
			append(record.getSoort());
			append(record.getOmschrijving());
			append(record.getBoekstukMask());
			append(record.getLaatsteBoekstuk());
			append(record.getRekening());
			append(record.getTegenRekening());
			append(Boolean.valueOf(record.getSystemValue()).toString());
			append(record.getVrijeRub1());
			append(record.getVrijeRub2());
			append(record.getVrijeRub3());
			append(record.getVrijeRub4());
			append(record.getVrijeRub5());
			append(RCD_SEP);
		}
	}

	private void exportDebiteur() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Debiteur.BEDRIJF + "='" + bedrijfsCode + "'");
		Debiteur record = null;
		query = QueryFactory.create(Debiteur.class, sb.toString());
		collection = DebiteurManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Debiteur) records.next();
			append(ImportTypeEnum._DEBITEUR);
			append(record.getDebNr());
			append(record.getNaam());
			append(record.getContactPersoon());
			append(record.getEmail());
			append(record.getKvkNr());
			append(record.getBtwNr());
			append(record.getIbanNr());
			append(record.getBetaalTermijn());
			append(record.getDtmLaatsteAanmaning());
			append(RCD_SEP);
		}
	}

	private void exportDetailData() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(DetailData.BEDRIJF + "='" + bedrijfsCode + "'");
		DetailData record = null;
		query = QueryFactory.create(DetailData.class, sb.toString());
		collection = DetailDataManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (DetailData) records.next();
			append(ImportTypeEnum._DETAILDATA);
			append(record.getDagboekId());
			append(record.getBoekstuk());
			append(record.getBoekstukRegel());
			append((Date) record.getDatumLevering());
			append(record.getOmschr1());
			append(record.getOmschr2());
			append(record.getOmschr3());
			append(record.getAantal());
			append(record.getEenheid());
			append(record.getPrijs());
			append(record.getBtwCode());
			append(record.getBedragExcl());
			append(record.getBedragBtw());
			append(record.getBedragIncl());
			append(record.getBedragDebet());
			append(record.getBedragCredit());
			append(record.getDC());
			append(record.getRekening());
			append(record.getDcNummer());
			append(record.getFactuurNummer());
			append(record.getVrijeRub1());
			append(record.getVrijeRub2());
			append(record.getVrijeRub3());
			append(record.getVrijeRub4());
			append(record.getVrijeRub5());
			append(RCD_SEP);
		}
	}

	private void exportHeaderData() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(HeaderData.BEDRIJF + "='" + bedrijfsCode + "'");
		HeaderData record = null;
		query = QueryFactory.create(HeaderData.class, sb.toString());
		collection = HeaderDataManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (HeaderData) records.next();
			append(ImportTypeEnum._HEADERDATA);
			append(record.getDagboekId());
			append(record.getDagboekSoort());
			append(record.getBoekstuk());
			append(record.getStatus());
			append(Boolean.valueOf(record.getFavoriet()).toString());
			append((Date)record.getBoekdatum());
			append(record.getBoekjaar());
			append(record.getBoekperiode());
			append(record.getDcNummer());
			append(record.getFactuurNummer());
			append(record.getFactuurLayout());
			append(record.getReferentieNummer());
			append((Date)record.getFactuurdatum());
			append((Date)record.getVervaldatum());
			append(record.getAantalAanmaningen());
			append(record.getDC());
			append(record.getTotaalExcl());
			append(record.getTotaalBtw());
			append(record.getTotaalIncl());
			append(record.getTotaalGeboekt());
			append(record.getTotaalBetaald());
			append(record.getTotaalOpenstaand());
			append(record.getTotaalDebet());
			append(record.getTotaalCredit());
			append(record.getBeginsaldo());
			append(record.getEindsaldo());
			append(record.getOmschr1());
			append(record.getOmschr2());
			append(record.getVrijeRub1());
			append(record.getVrijeRub2());
			append(record.getVrijeRub3());
			append(record.getVrijeRub4());
			append(record.getVrijeRub5());
			append(RCD_SEP);
		}
	}

	private void exportHfdVerdichting() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Hoofdverdichting.BEDRIJF + "='" + bedrijfsCode + "'");
		Hoofdverdichting record = null;
		query = QueryFactory.create(Hoofdverdichting.class, sb.toString());
		collection = HoofdverdichtingManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Hoofdverdichting) records.next();
			append(ImportTypeEnum._HFDVERDICHT);
			append(record.getId());
			append(record.getOmschrijving());
			append(record.getRekeningsoort());
			append(RCD_SEP);
		}
	}

	private void exportInstellingen() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Instellingen.BEDRIJF + "='" + bedrijfsCode + "'");
		Instellingen record = null;
		query = QueryFactory.create(Instellingen.class, sb.toString());
		collection = InstellingenManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Instellingen) records.next();
			append(ImportTypeEnum._INSTELLING);
			append(record.getStdBetaalTermijn());
			append(record.getLengteRekening());
			append(record.getMaskCredNr());
			append(record.getMaskDebNr());
			append(RCD_SEP);
		}
	}

	private void exportPeriode() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Periode.BEDRIJF + "='" + bedrijfsCode + "'");
		Periode record = null;
		query = QueryFactory.create(Periode.class, sb.toString());
		collection = PeriodeManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Periode) records.next();
			append(ImportTypeEnum._PERIODE);
			append(record.getBoekjaar());
			append(record.getBoekperiode());
			append((Date)record.getStartdatum());
			append((Date)record.getEinddatum());
			append(RCD_SEP);
		}
	}

	private void exportProduct() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Product.BEDRIJF + "='" + bedrijfsCode + "'");
		Product record = null;
		query = QueryFactory.create(Product.class, sb.toString());
		collection = ProductManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Product) records.next();
			append(ImportTypeEnum._PRODUCT);
			append(record.getEenheid());
			append(record.getOmschr1());
			append(record.getOmschr2());
			append(record.getOmschr3());
			append(record.getPrijs());
			append(record.getPrijsInkoop());
			append(record.getProduct());
			append(record.getRekeningNr());
			append(RCD_SEP);
		}
	}

	private void exportRekeningen() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Rekening.BEDRIJF + "='" + bedrijfsCode + "'");
		Rekening record = null;
		query = QueryFactory.create(Rekening.class, sb.toString());
		collection = RekeningManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Rekening) records.next();
			append(ImportTypeEnum._REKENING);
			append(record.getRekeningNr());
			append(record.getOmschrijving());
			append(record.getOmschrijvingKort());
			append(record.getVerdichting());
			append(record.getICPSoort());
			append(record.getBtwCode());
			append(record.getBtwSoort());
			append(Boolean.valueOf(record.getBtwRekening()).toString());
			append(record.getSubAdministratieType());
			append(record.getToonKolom());
			append(Boolean.valueOf(record.getBlocked()).toString());
			append(RCD_SEP);
		}
	}

	private void exportVerdichting() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(Verdichting.BEDRIJF + "='" + bedrijfsCode + "'");
		Verdichting record = null;
		query = QueryFactory.create(Verdichting.class, sb.toString());
		collection = VerdichtingManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (Verdichting) records.next();
			append(ImportTypeEnum._VERDICHTING);
			append(record.getId());
			append(record.getOmschrijving());
			append(record.getHoofdverdichting());
			append(RCD_SEP);
		}
	}

	private void exportHtmlTemplates() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(HtmlTemplate.BEDRIJF + "='" + bedrijfsCode + "'");
		HtmlTemplate record = null;
		query = QueryFactory.create(HtmlTemplate.class, sb.toString());
		collection = HtmlTemplateManagerFactory.getInstance(dbd).getCollection(query);
		records = collection.iterator();
		while (records.hasNext()) {
			record = (HtmlTemplate) records.next();
			append(ImportTypeEnum._HTMLTEMPLATE);
			append(record.getId());
			append(record.getOmschrijving());
			append(Boolean.valueOf(record.getIsDefault()).toString());
			append(record.getHtmlString());
			append((byte[])record.getLogo());
			append(record.getLogoHeight());
			append(RCD_SEP);
		}
	}

	private void append(String s) throws Exception {
		if (s == null) {
			s = NULL;
		} else if (s.equals("")) {
			s = BLANKS;
		}
		exportString.append(s);
		exportString.append(!s.equals(RCD_SEP) ? FLD_SEP : "");
	}

	private void append(long i) throws Exception {
		append("" + i);
	}

	private void append(byte[] b) throws Exception {
		append(b == null ? "" : new String(b));
	}

	private void append(BigDecimal d) throws Exception {
		append(d == null ? "0" : "" + d);
	}

	private void append(Date d) throws Exception {
		append(d == null ? null : "" + d.getTime());
	}
}
