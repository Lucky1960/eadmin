package nl.eadmin.helpers;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Adres;
import nl.eadmin.db.AdresDataBean;
import nl.eadmin.db.AdresManager;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.AdresPK;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfDataBean;
import nl.eadmin.db.BedrijfManager;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageDataBean;
import nl.eadmin.db.BijlageManager;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.BijlagePK;
import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieDataBean;
import nl.eadmin.db.BtwCategorieManager;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.db.BtwCategoriePK;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeDataBean;
import nl.eadmin.db.BtwCodeManager;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.BtwCodePK;
import nl.eadmin.db.CodeTabel;
import nl.eadmin.db.CodeTabelDataBean;
import nl.eadmin.db.CodeTabelElement;
import nl.eadmin.db.CodeTabelElementDataBean;
import nl.eadmin.db.CodeTabelElementManager;
import nl.eadmin.db.CodeTabelElementManagerFactory;
import nl.eadmin.db.CodeTabelElementPK;
import nl.eadmin.db.CodeTabelManager;
import nl.eadmin.db.CodeTabelManagerFactory;
import nl.eadmin.db.CodeTabelPK;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurDataBean;
import nl.eadmin.db.CrediteurManager;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekDataBean;
import nl.eadmin.db.DagboekManager;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.DagboekPK;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurDataBean;
import nl.eadmin.db.DebiteurManager;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataDataBean;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.DetailDataPK;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataDataBean;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.HeaderDataPK;
import nl.eadmin.db.Hoofdverdichting;
import nl.eadmin.db.HoofdverdichtingDataBean;
import nl.eadmin.db.HoofdverdichtingManager;
import nl.eadmin.db.HoofdverdichtingManagerFactory;
import nl.eadmin.db.HoofdverdichtingPK;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateDataBean;
import nl.eadmin.db.HtmlTemplateManager;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.db.HtmlTemplatePK;
import nl.eadmin.db.Instellingen;
import nl.eadmin.db.InstellingenDataBean;
import nl.eadmin.db.InstellingenManager;
import nl.eadmin.db.InstellingenManagerFactory;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeDataBean;
import nl.eadmin.db.PeriodeManager;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.PeriodePK;
import nl.eadmin.db.Product;
import nl.eadmin.db.ProductDataBean;
import nl.eadmin.db.ProductManager;
import nl.eadmin.db.ProductManagerFactory;
import nl.eadmin.db.ProductPK;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningDataBean;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.RekeningPK;
import nl.eadmin.db.Verdichting;
import nl.eadmin.db.VerdichtingDataBean;
import nl.eadmin.db.VerdichtingManager;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.db.VerdichtingPK;
import nl.eadmin.enums.ImportTypeEnum;
import nl.eadmin.ui.adapters.ImportPanel;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.jsql.DBData;

public class ImportHelper {
	public static final String FLD_SEP = ExportHelper.FLD_SEP;
	public static final String RCD_SEP = ExportHelper.RCD_SEP;
	public static final String BLANKS = ExportHelper.BLANKS;
	public static final String NULL = ExportHelper.NULL;
	private ImportPanel importPanel;
	private String importString;
	private String[] records, fields;
	private String bedrijfsCode;
	private boolean overWrite;

	private Adres adres = null;
	private AdresManager adresMgr;
	private AdresPK adresPK = new AdresPK();
	private AdresDataBean adresBean = new AdresDataBean();

	private Bedrijf bedrijf = null;
	private BedrijfManager bedrijfMgr;
	private BedrijfDataBean bedrijfBean = new BedrijfDataBean();

	private Bijlage bijlage = null;
	private BijlageManager bijlageMgr;
	private BijlagePK bijlagePK = new BijlagePK();
	private BijlageDataBean bijlageBean = new BijlageDataBean();

	private BtwCategorie btwCat = null;
	private BtwCategorieManager btwCatMgr;
	private BtwCategoriePK btwCatPK = new BtwCategoriePK();
	private BtwCategorieDataBean btwCatBean = new BtwCategorieDataBean();

	private BtwCode btwCode = null;
	private BtwCodeManager btwCodeMgr;
	private BtwCodePK btwCodePK = new BtwCodePK();
	private BtwCodeDataBean btwCodeBean = new BtwCodeDataBean();

	private CodeTabel codeTabel = null;
	private CodeTabelManager codeTabelMgr;
	private CodeTabelPK codeTabelPK = new CodeTabelPK();
	private CodeTabelDataBean codeTabelBean = new CodeTabelDataBean();

	private CodeTabelElement codeTabelElement = null;
	private CodeTabelElementManager codeTabelElementMgr;
	private CodeTabelElementPK codeTabelElementPK = new CodeTabelElementPK();
	private CodeTabelElementDataBean codeTabelElementBean = new CodeTabelElementDataBean();

	private Crediteur crediteur = null;
	private CrediteurManager crediteurMgr;
	private CrediteurPK crediteurPK = new CrediteurPK();
	private CrediteurDataBean crediteurBean = new CrediteurDataBean();

	private Dagboek dagboek = null;
	private DagboekManager dagboekMgr;
	private DagboekPK dagboekPK = new DagboekPK();
	private DagboekDataBean dagboekBean = new DagboekDataBean();

	private Debiteur debiteur = null;
	private DebiteurManager debiteurMgr;
	private DebiteurPK debiteurPK = new DebiteurPK();
	private DebiteurDataBean debiteurBean = new DebiteurDataBean();

	private DetailData detailData = null;
	private DetailDataManager detailDataMgr;
	private DetailDataPK detailDataPK = new DetailDataPK();
	private DetailDataDataBean detailDataBean = new DetailDataDataBean();

	private HeaderData headerData = null;
	private HeaderDataManager headerDataMgr;
	private HeaderDataPK headerDataPK = new HeaderDataPK();
	private HeaderDataDataBean headerDataBean = new HeaderDataDataBean();

	private Hoofdverdichting hoofdVerdichting = null;
	private HoofdverdichtingManager hoofdVerdichtingMgr;
	private HoofdverdichtingPK hoofdVerdichtingPK = new HoofdverdichtingPK();
	private HoofdverdichtingDataBean hoofdVerdichtingBean = new HoofdverdichtingDataBean();

	private HtmlTemplate htmlTemplate = null;
	private HtmlTemplateManager htmlTemplateMgr;
	private HtmlTemplatePK htmlTemplatePK = new HtmlTemplatePK();
	private HtmlTemplateDataBean htmlTemplateBean = new HtmlTemplateDataBean();

	private Instellingen instelling = null;
	private InstellingenManager instellingMgr;
	private InstellingenDataBean instellingBean = new InstellingenDataBean();

	private Periode periode = null;
	private PeriodeManager periodeMgr;
	private PeriodePK periodePK = new PeriodePK();
	private PeriodeDataBean periodeBean = new PeriodeDataBean();

	private Product product = null;
	private ProductManager productMgr;
	private ProductPK productPK = new ProductPK();
	private ProductDataBean productBean = new ProductDataBean();

	private Rekening rekening = null;
	private RekeningManager rekeningMgr;
	private RekeningPK rekeningPK = new RekeningPK();
	private RekeningDataBean rekeningBean = new RekeningDataBean();

	private Verdichting verdichting = null;
	private VerdichtingManager verdichtingMgr;
	private VerdichtingPK verdichtingPK = new VerdichtingPK();
	private VerdichtingDataBean verdichtingBean = new VerdichtingDataBean();

	public ImportHelper(String importString, ImportPanel importPanel, DBData dbd, String bedrijfsCode, boolean overWrite) throws Exception {
		this.importString = importString;
		this.importPanel = importPanel;
		this.bedrijfsCode = bedrijfsCode;
		this.overWrite = overWrite;

		adresMgr = AdresManagerFactory.getInstance(dbd);
		bedrijfMgr = BedrijfManagerFactory.getInstance(dbd);
		bijlageMgr = BijlageManagerFactory.getInstance(dbd);
		btwCatMgr = BtwCategorieManagerFactory.getInstance(dbd);
		btwCodeMgr = BtwCodeManagerFactory.getInstance(dbd);
		codeTabelMgr = CodeTabelManagerFactory.getInstance(dbd);
		codeTabelElementMgr = CodeTabelElementManagerFactory.getInstance(dbd);
		crediteurMgr = CrediteurManagerFactory.getInstance(dbd);
		dagboekMgr = DagboekManagerFactory.getInstance(dbd);
		debiteurMgr = DebiteurManagerFactory.getInstance(dbd);
		detailDataMgr = DetailDataManagerFactory.getInstance(dbd);
		headerDataMgr = HeaderDataManagerFactory.getInstance(dbd);
		hoofdVerdichtingMgr = HoofdverdichtingManagerFactory.getInstance(dbd);
		htmlTemplateMgr = HtmlTemplateManagerFactory.getInstance(dbd);
		instellingMgr = InstellingenManagerFactory.getInstance(dbd);
		periodeMgr = PeriodeManagerFactory.getInstance(dbd);
		productMgr = ProductManagerFactory.getInstance(dbd);
		rekeningMgr = RekeningManagerFactory.getInstance(dbd);
		verdichtingMgr = VerdichtingManagerFactory.getInstance(dbd);
	}

	public void importFromString() throws Exception {
		try {
			records = importString.split(RCD_SEP);
			for (int i = 0; i < records.length; i++) {
				fields = records[i].split(FLD_SEP);
				for (int j = 0; j < fields.length; j++) {
					if (fields[j].equals(BLANKS)) {
						fields[j] = "";
					} else if (fields[j].equals(NULL)) {
						fields[j] = null;
					}
				}
				if (fields[0].equals(ExportHelper.FILE_ID)) {
					importSystemValues(fields);
				} else if (fields[0].equals(ImportTypeEnum._ADRES) && importPanel.getValue(ImportTypeEnum.ADRES)) {
					importAdres(fields);
				} else if (fields[0].equals(ImportTypeEnum._BEDRIJF) && importPanel.getValue(ImportTypeEnum.BEDRIJF)) {
					importBedrijf(fields);
				} else if (fields[0].equals(ImportTypeEnum._BIJLAGE) && importPanel.getValue(ImportTypeEnum.BIJLAGE)) {
					importBijlage(fields);
				} else if (fields[0].equals(ImportTypeEnum._BTWCATEGORIE) && importPanel.getValue(ImportTypeEnum.BTWCATEGORIE)) {
					importBtwCategorie(fields);
				} else if (fields[0].equals(ImportTypeEnum._BTWCODE) && importPanel.getValue(ImportTypeEnum.BTWCODE)) {
					importBtwCode(fields);
				} else if (fields[0].equals(ImportTypeEnum._CODETABEL) && importPanel.getValue(ImportTypeEnum.CODETABEL)) {
					importCodeTabel(fields);
				} else if (fields[0].equals(ImportTypeEnum._CODETBLELM) && importPanel.getValue(ImportTypeEnum.CODETABELELM)) {
					importCodeTabelElement(fields);
				} else if (fields[0].equals(ImportTypeEnum._CREDITEUR) && importPanel.getValue(ImportTypeEnum.CREDITEUR)) {
					importCrediteur(fields);
				} else if (fields[0].equals(ImportTypeEnum._DAGBOEK) && importPanel.getValue(ImportTypeEnum.DAGBOEK)) {
					importDagboek(fields);
				} else if (fields[0].equals(ImportTypeEnum._DEBITEUR) && importPanel.getValue(ImportTypeEnum.DEBITEUR)) {
					importDebiteur(fields);
				} else if (fields[0].equals(ImportTypeEnum._DETAILDATA) && importPanel.getValue(ImportTypeEnum.DETAILDATA)) {
					importDetailData(fields);
				} else if (fields[0].equals(ImportTypeEnum._HEADERDATA) && importPanel.getValue(ImportTypeEnum.HEADERDATA)) {
					importHeaderData(fields);
				} else if (fields[0].equals(ImportTypeEnum._HFDVERDICHT) && importPanel.getValue(ImportTypeEnum.HFDVERDICHTING)) {
					importHoofdverdichting(fields);
				} else if (fields[0].equals(ImportTypeEnum._HTMLTEMPLATE) && importPanel.getValue(ImportTypeEnum.HTMLTEMPLATE)) {
					importHtmlTemplate(fields);
				} else if (fields[0].equals(ImportTypeEnum._INSTELLING) && importPanel.getValue(ImportTypeEnum.INSTELLING)) {
					importInstelling(fields);
				} else if (fields[0].equals(ImportTypeEnum._PERIODE) && importPanel.getValue(ImportTypeEnum.PERIODE)) {
					importPeriode(fields);
				} else if (fields[0].equals(ImportTypeEnum._PRODUCT) && importPanel.getValue(ImportTypeEnum.PRODUCT)) {
					importProduct(fields);
				} else if (fields[0].equals(ImportTypeEnum._REKENING) && importPanel.getValue(ImportTypeEnum.REKENING)) {
					importRekening(fields);
				} else if (fields[0].equals(ImportTypeEnum._VERDICHTING) && importPanel.getValue(ImportTypeEnum.VERDICHTING)) {
					importVerdichting(fields);
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new UserMessageException("Fout bij importeren: ongeldig aantal rubrieken gevonden voor bestand " + fields[0]);
		} catch (Exception e) {
			throw new UserMessageException("Fout bij importeren van bestand " + fields[0] + ": " + e.getMessage());
		}
	}

	private void importSystemValues(String[] fields) throws Exception {
		int f = 0;
		String fileId = fields[f++];
		if (!fileId.equals(ExportHelper.FILE_ID)) {
			throw new UserMessageException("Dit bestand is geen geldig importbestand");
		}
		String version = fields[f++];
		if (!version.equals(ApplicationConstants.APPLICATION_VERSION)) {
			throw new UserMessageException("Importbestand (versie " + version + ") is niet geschikt voor de huidige versie (" + ApplicationConstants.APPLICATION_VERSION + ") van deze applicatie");
		}
	}

	private void importAdres(String[] fields) throws Exception {
		int f = 1;
		adresBean.setBedrijf(bedrijfsCode);
		adresBean.setDc(fields[f++]);
		adresBean.setDcNr(fields[f++]);
		adresBean.setAdresType(Integer.parseInt(fields[f++]));
		adresBean.setStraat(fields[f++]);
		adresBean.setHuisNr(Integer.parseInt(fields[f++]));
		adresBean.setHuisNrToev(fields[f++]);
		adresBean.setExtraAdresRegel(fields[f++]);
		adresBean.setPostcode(fields[f++]);
		adresBean.setPlaats(fields[f++]);
		adresBean.setLand(fields[f++]);
		adresBean.setTelefoon(fields[f++]);
		adresBean.setMobiel(fields[f++]);
		try {
			adresPK.setBedrijf(adresBean.getBedrijf());
			adresPK.setDc(adresBean.getDc());
			adresPK.setDcNr(adresBean.getDcNr());
			adresPK.setAdresType(adresBean.getAdresType());
			adres = adresMgr.findByPrimaryKey(adresPK);
			if (overWrite) {
				adres.setStraat(adresBean.getStraat());
				adres.setHuisNr(adresBean.getHuisNr());
				adres.setHuisNrToev(adresBean.getHuisNrToev());
				adres.setExtraAdresRegel(adresBean.getExtraAdresRegel());
				adres.setPostcode(adresBean.getPostcode());
				adres.setPlaats(adresBean.getPlaats());
				adres.setLand(adresBean.getLand());
				adres.setTelefoon(adresBean.getTelefoon());
				adres.setMobiel(adresBean.getMobiel());
			}
		} catch (Exception e) {
			adresMgr.create(adresBean);
		}
	}

	private void importBedrijf(String[] fields) throws Exception {
		int f = 1;
		bedrijfBean.setBedrijfscode(bedrijfsCode);
		bedrijfBean.setOmschrijving(fields[f++]);
		bedrijfBean.setEmail(fields[f++]);
		bedrijfBean.setKvkNr(fields[f++]);
		bedrijfBean.setBtwNr(fields[f++]);
		bedrijfBean.setIbanNr(fields[f++]);
		bedrijfBean.setBic(fields[f++]);
		try {
			bedrijf = bedrijfMgr.findByPrimaryKey(bedrijfsCode);
			if (overWrite) {
				bedrijf.setOmschrijving(bedrijfBean.getOmschrijving());
				bedrijf.setEmail(bedrijfBean.getEmail());
				bedrijf.setKvkNr(bedrijfBean.getKvkNr());
				bedrijf.setBtwNr(bedrijfBean.getBtwNr());
				bedrijf.setIbanNr(bedrijfBean.getIbanNr());
				bedrijf.setBic(bedrijfBean.getBic());
			}
		} catch (Exception e) {
			bedrijfMgr.create(bedrijfBean);
		}
	}

	private void importBijlage(String[] fields) throws Exception {
		int f = 1;
		bijlageBean.setBedrijf(bedrijfsCode);
		bijlageBean.setDagboekId(fields[f++]);
		bijlageBean.setBoekstuk(fields[f++]);
		bijlageBean.setVolgNr(Integer.parseInt(fields[f++]));
		bijlageBean.setBestandsnaam(fields[f++]);
		bijlageBean.setOmschrijving(fields[f++]);
		try {
			bijlagePK.setBedrijf(bijlageBean.getBedrijf());
			bijlagePK.setDagboekId(bijlageBean.getDagboekId());
			bijlagePK.setBoekstuk(bijlageBean.getBoekstuk());
			bijlagePK.setVolgNr(bijlageBean.getVolgNr());
			bijlage = bijlageMgr.findByPrimaryKey(bijlagePK);
			if (overWrite) {
				bijlage.setBestandsnaam(bijlageBean.getBestandsnaam());
				bijlage.setOmschrijving(bijlageBean.getOmschrijving());
			}
		} catch (Exception e) {
			bijlageMgr.create(bijlageBean);
		}
	}

	private void importBtwCategorie(String[] fields) throws Exception {
		int f = 1;
		btwCatBean.setBedrijf(bedrijfsCode);
		btwCatBean.setCode(fields[f++]);
		btwCatBean.setOmschrijving(fields[f++]);
		btwCatBean.setRekeningNr(fields[f++]);
		try {
			btwCatPK.setBedrijf(btwCatBean.getBedrijf());
			btwCatPK.setCode(btwCatBean.getCode());
			btwCat = btwCatMgr.findByPrimaryKey(btwCatPK);
			if (overWrite) {
				btwCat.setOmschrijving(btwCatBean.getOmschrijving());
				btwCat.setRekeningNr(btwCatBean.getRekeningNr());
			}
		} catch (Exception e) {
			btwCatMgr.create(btwCatBean);
		}
	}

	private void importBtwCode(String[] fields) throws Exception {
		int f = 1;
		btwCodeBean.setBedrijf(bedrijfsCode);
		btwCodeBean.setCode(fields[f++]);
		btwCodeBean.setOmschrijving(fields[f++]);
		btwCodeBean.setPercentage(new BigDecimal(fields[f++]));
		btwCodeBean.setInEx(fields[f++]);
		btwCodeBean.setBtwCatInkoop(fields[f++]);
		btwCodeBean.setBtwCatVerkoop(fields[f++]);
		try {
			btwCodePK.setBedrijf(btwCodeBean.getBedrijf());
			btwCodePK.setCode(btwCodeBean.getCode());
			btwCode = btwCodeMgr.findByPrimaryKey(btwCodePK);
			if (overWrite) {
				btwCode.setOmschrijving(btwCodeBean.getOmschrijving());
				btwCode.setPercentage(btwCodeBean.getPercentage());
				btwCode.setInEx(btwCodeBean.getInEx());
				btwCode.setBtwCatInkoop(btwCodeBean.getBtwCatInkoop());
				btwCode.setBtwCatVerkoop(btwCodeBean.getBtwCatVerkoop());
			}
		} catch (Exception e) {
			btwCodeMgr.create(btwCodeBean);
		}
	}

	private void importCodeTabel(String[] fields) throws Exception {
		int f = 1;
		codeTabelBean.setBedrijf(bedrijfsCode);
		codeTabelBean.setTabelNaam(fields[f++]);
		codeTabelBean.setOmschrijving(fields[f++]);
		codeTabelBean.setDataType(fields[f++]);
		codeTabelBean.setLengte(Integer.parseInt(fields[f++]));
		codeTabelBean.setDecimalen(Integer.parseInt(fields[f++]));
		try {
			codeTabelPK.setBedrijf(codeTabelBean.getBedrijf());
			codeTabelPK.setTabelNaam(codeTabelBean.getTabelNaam());
			codeTabel = codeTabelMgr.findByPrimaryKey(codeTabelPK);
			if (overWrite) {
				codeTabel.setOmschrijving(codeTabelBean.getOmschrijving());
				codeTabel.setDataType(codeTabelBean.getDataType());
				codeTabel.setLengte(codeTabelBean.getLengte());
				codeTabel.setDecimalen(codeTabelBean.getDecimalen());
			}
		} catch (Exception e) {
			codeTabelMgr.create(codeTabelBean);
		}
	}

	private void importCodeTabelElement(String[] fields) throws Exception {
		int f = 1;
		codeTabelElementBean.setBedrijf(bedrijfsCode);
		codeTabelElementBean.setTabelNaam(fields[f++]);
		codeTabelElementBean.setId(fields[f++]);
		codeTabelElementBean.setOmschrijving(fields[f++]);
		try {
			codeTabelElementPK.setBedrijf(codeTabelElementBean.getBedrijf());
			codeTabelElementPK.setTabelNaam(codeTabelElementBean.getTabelNaam());
			codeTabelElementPK.setId(codeTabelElementBean.getId());
			codeTabelElement = codeTabelElementMgr.findByPrimaryKey(codeTabelElementPK);
			if (overWrite) {
				codeTabelElement.setOmschrijving(codeTabelBean.getOmschrijving());
			}
		} catch (Exception e) {
			codeTabelElementMgr.create(codeTabelElementBean);
		}
	}

	private void importCrediteur(String[] fields) throws Exception {
		int f = 1;
		crediteurBean.setBedrijf(bedrijfsCode);
		crediteurBean.setCredNr(fields[f++]);
		crediteurBean.setNaam(fields[f++]);
		crediteurBean.setContactPersoon(fields[f++]);
		crediteurBean.setEmail(fields[f++]);
		crediteurBean.setKvkNr(fields[f++]);
		crediteurBean.setBtwNr(fields[f++]);
		crediteurBean.setIbanNr(fields[f++]);
		try {
			crediteurPK.setBedrijf(crediteurBean.getBedrijf());
			crediteurPK.setCredNr(crediteurBean.getCredNr());
			crediteur = crediteurMgr.findByPrimaryKey(crediteurPK);
			if (overWrite) {
				crediteur.setNaam(crediteurBean.getNaam());
				crediteur.setContactPersoon(crediteurBean.getContactPersoon());
				crediteur.setEmail(crediteurBean.getEmail());
				crediteur.setKvkNr(crediteurBean.getKvkNr());
				crediteur.setBtwNr(crediteurBean.getBtwNr());
				crediteur.setIbanNr(crediteurBean.getIbanNr());
			}
		} catch (Exception e) {
			crediteurMgr.create(crediteurBean);
		}
	}

	private void importDagboek(String[] fields) throws Exception {
		int f = 1;
		dagboekBean.setBedrijf(bedrijfsCode);
		dagboekBean.setId(fields[f++]);
		dagboekBean.setSoort(fields[f++]);
		dagboekBean.setOmschrijving(fields[f++]);
		dagboekBean.setBoekstukMask(fields[f++]);
		dagboekBean.setLaatsteBoekstuk(fields[f++]);
		dagboekBean.setRekening(fields[f++]);
		dagboekBean.setTegenRekening(fields[f++]);
		dagboekBean.setSystemValue(Boolean.getBoolean(fields[f++]));
		dagboekBean.setVrijeRub1(fields[f++]);
		dagboekBean.setVrijeRub2(fields[f++]);
		dagboekBean.setVrijeRub3(fields[f++]);
		dagboekBean.setVrijeRub4(fields[f++]);
		dagboekBean.setVrijeRub5(fields[f++]);
		try {
			dagboekPK.setBedrijf(dagboekBean.getBedrijf());
			dagboekPK.setId(dagboekBean.getId());
			dagboek = dagboekMgr.findByPrimaryKey(dagboekPK);
			if (overWrite) {
				dagboek.setSoort(dagboekBean.getSoort());
				dagboek.setOmschrijving(dagboekBean.getOmschrijving());
				dagboek.setBoekstukMask(dagboekBean.getBoekstukMask());
				dagboek.setLaatsteBoekstuk(dagboekBean.getLaatsteBoekstuk());
				dagboek.setRekening(dagboekBean.getRekening());
				dagboek.setTegenRekening(dagboekBean.getTegenRekening());
				dagboek.setSystemValue(dagboekBean.getSystemValue());
				dagboek.setVrijeRub1(dagboekBean.getVrijeRub1());
				dagboek.setVrijeRub2(dagboekBean.getVrijeRub2());
				dagboek.setVrijeRub3(dagboekBean.getVrijeRub3());
				dagboek.setVrijeRub4(dagboekBean.getVrijeRub4());
				dagboek.setVrijeRub5(dagboekBean.getVrijeRub5());
			}
		} catch (Exception e) {
			dagboekMgr.create(dagboekBean);
		}
	}

	private void importDebiteur(String[] fields) throws Exception {
		int f = 1;
		debiteurBean.setBedrijf(bedrijfsCode);
		debiteurBean.setDebNr(fields[f++]);
		debiteurBean.setNaam(fields[f++]);
		debiteurBean.setContactPersoon(fields[f++]);
		debiteurBean.setEmail(fields[f++]);
		debiteurBean.setKvkNr(fields[f++]);
		debiteurBean.setBtwNr(fields[f++]);
		debiteurBean.setIbanNr(fields[f++]);
		debiteurBean.setBetaalTermijn(Integer.parseInt(fields[f++]));
		debiteurBean.setDtmLaatsteAanmaning(createDateFrom(fields[f++]));
		try {
			debiteurPK.setBedrijf(debiteurBean.getBedrijf());
			debiteurPK.setDebNr(debiteurBean.getDebNr());
			debiteur = debiteurMgr.findByPrimaryKey(debiteurPK);
			if (overWrite) {
				debiteur.setNaam(debiteurBean.getNaam());
				debiteur.setContactPersoon(debiteurBean.getContactPersoon());
				debiteur.setEmail(debiteurBean.getEmail());
				debiteur.setKvkNr(debiteurBean.getKvkNr());
				debiteur.setBtwNr(debiteurBean.getBtwNr());
				debiteur.setIbanNr(debiteurBean.getIbanNr());
				debiteur.setBetaalTermijn(debiteurBean.getBetaalTermijn());
				debiteur.setDtmLaatsteAanmaning(debiteurBean.getDtmLaatsteAanmaning());
			}
		} catch (Exception e) {
			debiteurMgr.create(debiteurBean);
		}
	}

	private void importDetailData(String[] fields) throws Exception {
		int f = 1;
		detailDataBean.setBedrijf(bedrijfsCode);
		detailDataBean.setDagboekId(fields[f++]);
		detailDataBean.setBoekstuk(fields[f++]);
		detailDataBean.setBoekstukRegel(Integer.parseInt(fields[f++]));
		detailDataBean.setDatumLevering(createDateFrom(fields[f++]));
		detailDataBean.setOmschr1(fields[f++]);
		detailDataBean.setOmschr2(fields[f++]);
		detailDataBean.setOmschr3(fields[f++]);
		detailDataBean.setAantal(new BigDecimal(fields[f++]));
		detailDataBean.setEenheid(fields[f++]);
		detailDataBean.setPrijs(new BigDecimal(fields[f++]));
		detailDataBean.setBtwCode(fields[f++]);
		detailDataBean.setBedragExcl(new BigDecimal(fields[f++]));
		detailDataBean.setBedragBtw(new BigDecimal(fields[f++]));
		detailDataBean.setBedragIncl(new BigDecimal(fields[f++]));
		detailDataBean.setBedragDebet(new BigDecimal(fields[f++]));
		detailDataBean.setBedragCredit(new BigDecimal(fields[f++]));
		detailDataBean.setDC(fields[f++]);
		detailDataBean.setRekening(fields[f++]);
		detailDataBean.setDcNummer(fields[f++]);
		detailDataBean.setFactuurNummer(fields[f++]);
		detailDataBean.setVrijeRub1(fields[f++]);
		detailDataBean.setVrijeRub2(fields[f++]);
		detailDataBean.setVrijeRub3(fields[f++]);
		detailDataBean.setVrijeRub4(fields[f++]);
		detailDataBean.setVrijeRub5(fields[f++]);
		try {
			detailDataPK.setBedrijf(detailDataBean.getBedrijf());
			detailDataPK.setDagboekId(detailDataBean.getDagboekId());
			detailDataPK.setBoekstuk(detailDataBean.getBoekstuk());
			detailDataPK.setBoekstukRegel(detailDataBean.getBoekstukRegel());
			detailData = detailDataMgr.findByPrimaryKey(detailDataPK);
			if (overWrite) {
				detailData.setDatumLevering(detailDataBean.getDatumLevering());
				detailData.setOmschr1(detailDataBean.getOmschr1());
				detailData.setOmschr2(detailDataBean.getOmschr2());
				detailData.setOmschr3(detailDataBean.getOmschr3());
				detailData.setAantal(detailDataBean.getAantal());
				detailData.setEenheid(detailDataBean.getEenheid());
				detailData.setPrijs(detailDataBean.getPrijs());
				detailData.setBtwCode(detailDataBean.getBtwCode());
				detailData.setBedragExcl(detailDataBean.getBedragExcl());
				detailData.setBedragBtw(detailDataBean.getBedragBtw());
				detailData.setBedragIncl(detailDataBean.getBedragIncl());
				detailData.setBedragDebet(detailDataBean.getBedragDebet());
				detailData.setBedragCredit(detailDataBean.getBedragCredit());
				detailData.setDC(detailDataBean.getDC());
				detailData.setRekening(detailDataBean.getRekening());
				detailData.setDcNummer(detailDataBean.getDcNummer());
				detailData.setFactuurNummer(detailDataBean.getFactuurNummer());
				detailData.setVrijeRub1(detailDataBean.getVrijeRub1());
				detailData.setVrijeRub2(detailDataBean.getVrijeRub2());
				detailData.setVrijeRub3(detailDataBean.getVrijeRub3());
				detailData.setVrijeRub4(detailDataBean.getVrijeRub4());
				detailData.setVrijeRub5(detailDataBean.getVrijeRub5());
			}
		} catch (Exception e) {
			detailDataMgr.create(detailDataBean);
		}
	}

	private void importHeaderData(String[] fields) throws Exception {
		int f = 1;
		headerDataBean.setBedrijf(bedrijfsCode);
		headerDataBean.setDagboekId(fields[f++]);
		headerDataBean.setDagboekSoort(fields[f++]);
		headerDataBean.setBoekstuk(fields[f++]);
		headerDataBean.setStatus(Integer.parseInt(fields[f++]));
		headerDataBean.setFavoriet(Boolean.getBoolean(fields[f++]));
		headerDataBean.setBoekdatum(createDateFrom(fields[f++]));
		headerDataBean.setBoekjaar(Integer.parseInt(fields[f++]));
		headerDataBean.setBoekperiode(Integer.parseInt(fields[f++]));
		headerDataBean.setDcNummer(fields[f++]);
		headerDataBean.setFactuurNummer(fields[f++]);
		headerDataBean.setFactuurLayout(fields[f++]);
		headerDataBean.setReferentieNummer(fields[f++]);
		headerDataBean.setFactuurdatum(createDateFrom(fields[f++]));
		headerDataBean.setVervaldatum(createDateFrom(fields[f++]));
		headerDataBean.setAantalAanmaningen(Integer.parseInt(fields[f++]));
		headerDataBean.setDC(fields[f++]);
		headerDataBean.setTotaalExcl(new BigDecimal(fields[f++]));
		headerDataBean.setTotaalBtw(new BigDecimal(fields[f++]));
		headerDataBean.setTotaalIncl(new BigDecimal(fields[f++]));
		headerDataBean.setTotaalGeboekt(new BigDecimal(fields[f++]));
		headerDataBean.setTotaalBetaald(new BigDecimal(fields[f++]));
		headerDataBean.setTotaalOpenstaand(new BigDecimal(fields[f++]));
		headerDataBean.setTotaalDebet(new BigDecimal(fields[f++]));
		headerDataBean.setTotaalCredit(new BigDecimal(fields[f++]));
		headerDataBean.setBeginsaldo(new BigDecimal(fields[f++]));
		headerDataBean.setEindsaldo(new BigDecimal(fields[f++]));
		headerDataBean.setOmschr1(fields[f++]);
		headerDataBean.setOmschr2(fields[f++]);
		headerDataBean.setVrijeRub1(fields[f++]);
		headerDataBean.setVrijeRub2(fields[f++]);
		headerDataBean.setVrijeRub3(fields[f++]);
		headerDataBean.setVrijeRub4(fields[f++]);
		headerDataBean.setVrijeRub5(fields[f++]);
		try {
			headerDataPK.setBedrijf(headerDataBean.getBedrijf());
			headerDataPK.setDagboekId(headerDataBean.getDagboekId());
			headerDataPK.setBoekstuk(headerDataBean.getBoekstuk());
			headerData = headerDataMgr.findByPrimaryKey(headerDataPK);
			if (overWrite) {
				headerData.setStatus(headerDataBean.getStatus());
				headerData.setFavoriet(headerDataBean.getFavoriet());
				headerData.setBoekdatum(headerDataBean.getBoekdatum());
				headerData.setBoekjaar(headerDataBean.getBoekjaar());
				headerData.setBoekperiode(headerDataBean.getBoekperiode());
				headerData.setDcNummer(headerDataBean.getDcNummer());
				headerData.setFactuurNummer(headerDataBean.getFactuurNummer());
				headerData.setReferentieNummer(headerDataBean.getReferentieNummer());
				headerData.setFactuurdatum(headerDataBean.getFactuurdatum());
				headerData.setVervaldatum(headerDataBean.getVervaldatum());
				headerData.setAantalAanmaningen(headerDataBean.getAantalAanmaningen());
				headerData.setDC(headerDataBean.getDC());
				headerData.setTotaalExcl(headerDataBean.getTotaalExcl());
				headerData.setTotaalBtw(headerDataBean.getTotaalBtw());
				headerData.setTotaalIncl(headerDataBean.getTotaalIncl());
				headerData.setTotaalGeboekt(headerDataBean.getTotaalGeboekt());
				headerData.setTotaalBetaald(headerDataBean.getTotaalBetaald());
				headerData.setTotaalOpenstaand(headerDataBean.getTotaalOpenstaand());
				headerData.setOmschr1(headerDataBean.getOmschr1());
				headerData.setOmschr2(headerDataBean.getOmschr2());
				headerData.setVrijeRub1(headerDataBean.getVrijeRub1());
				headerData.setVrijeRub2(headerDataBean.getVrijeRub2());
				headerData.setVrijeRub3(headerDataBean.getVrijeRub3());
				headerData.setVrijeRub4(headerDataBean.getVrijeRub4());
				headerData.setVrijeRub5(headerDataBean.getVrijeRub5());
			}
		} catch (Exception e) {
			headerDataMgr.create(headerDataBean);
		}
	}

	private void importHoofdverdichting(String[] fields) throws Exception {
		int f = 1;
		hoofdVerdichtingBean.setBedrijf(bedrijfsCode);
		hoofdVerdichtingBean.setId(Integer.parseInt(fields[f++]));
		hoofdVerdichtingBean.setOmschrijving(fields[f++]);
		hoofdVerdichtingBean.setRekeningsoort(fields[f++]);
		try {
			hoofdVerdichtingPK.setBedrijf(hoofdVerdichtingBean.getBedrijf());
			hoofdVerdichtingPK.setId(hoofdVerdichtingBean.getId());
			hoofdVerdichting = hoofdVerdichtingMgr.findByPrimaryKey(hoofdVerdichtingPK);
			if (overWrite) {
				hoofdVerdichting.setOmschrijving(hoofdVerdichtingBean.getOmschrijving());
				hoofdVerdichting.setRekeningsoort(hoofdVerdichtingBean.getRekeningsoort());
			}
		} catch (Exception e) {
			hoofdVerdichtingMgr.create(hoofdVerdichtingBean);
		}
	}

	private void importHtmlTemplate(String[] fields) throws Exception {
		int f = 1;
		htmlTemplateBean.setBedrijf(bedrijfsCode);
		htmlTemplateBean.setId(fields[f++]);
		htmlTemplateBean.setOmschrijving(fields[f++]);
		htmlTemplateBean.setIsDefault(Boolean.getBoolean(fields[f++]));
		htmlTemplateBean.setHtmlString(fields[f++]);
		htmlTemplateBean.setLogo((fields[f++]).getBytes());
		htmlTemplateBean.setLogoHeight(Integer.parseInt(fields[f++]));
		try {
			htmlTemplatePK.setBedrijf(htmlTemplateBean.getBedrijf());
			htmlTemplatePK.setId(htmlTemplateBean.getId());
			htmlTemplate = htmlTemplateMgr.findByPrimaryKey(htmlTemplatePK);
			if (overWrite) {
				htmlTemplate.setOmschrijving(htmlTemplateBean.getOmschrijving());
				htmlTemplate.setIsDefault(htmlTemplateBean.getIsDefault());
				htmlTemplate.setHtmlString(htmlTemplateBean.getHtmlString());
				htmlTemplate.setLogo(htmlTemplateBean.getLogo());
				htmlTemplate.setLogoHeight(htmlTemplateBean.getLogoHeight());
			}
		} catch (Exception e) {
			htmlTemplateMgr.create(htmlTemplateBean);
		}
	}

	private void importInstelling(String[] fields) throws Exception {
		int f = 1;
		instellingBean.setBedrijf(bedrijfsCode);
		instellingBean.setStdBetaalTermijn(Integer.parseInt(fields[f++]));
		instellingBean.setLengteRekening(Integer.parseInt(fields[f++]));
		instellingBean.setMaskCredNr(fields[f++]);
		instellingBean.setMaskDebNr(fields[f++]);
		try {
			instelling = instellingMgr.findByPrimaryKey(bedrijfsCode);
			if (overWrite) {
				instelling.setStdBetaalTermijn(instellingBean.getStdBetaalTermijn());
				instelling.setLengteRekening(instellingBean.getLengteRekening());
				instelling.setMaskCredNr(instellingBean.getMaskCredNr());
				instelling.setMaskDebNr(instellingBean.getMaskDebNr());
			}
		} catch (Exception e) {
			instellingMgr.create(instellingBean);
		}
	}

	private void importPeriode(String[] fields) throws Exception {
		int f = 1;
		periodeBean.setBedrijf(bedrijfsCode);
		periodeBean.setBoekjaar(Integer.parseInt(fields[f++]));
		periodeBean.setBoekperiode(Integer.parseInt(fields[f++]));
		periodeBean.setStartdatum(createDateFrom(fields[f++]));
		periodeBean.setEinddatum(createDateFrom(fields[f++]));
		try {
			periodePK.setBedrijf(periodeBean.getBedrijf());
			periodePK.setBoekjaar(periodeBean.getBoekjaar());
			periodePK.setBoekperiode(periodeBean.getBoekperiode());
			periode = periodeMgr.findByPrimaryKey(periodePK);
			if (overWrite) {
				periode.setStartdatum(periodeBean.getStartdatum());
				periode.setEinddatum(periodeBean.getEinddatum());
			}
		} catch (Exception e) {
			periodeMgr.create(periodeBean);
		}
	}

	private void importProduct(String[] fields) throws Exception {
		int f = 1;
		productBean.setBedrijf(bedrijfsCode);
		productBean.setEenheid(fields[f++]);
		productBean.setOmschr1(fields[f++]);
		productBean.setOmschr2(fields[f++]);
		productBean.setOmschr3(fields[f++]);
		productBean.setPrijs(new BigDecimal(fields[f++]));
		productBean.setPrijsInkoop(new BigDecimal(fields[f++]));
		productBean.setProduct(fields[f++]);
		productBean.setRekeningNr(fields[f++]);
		try {
			productPK.setBedrijf(productBean.getBedrijf());
			productPK.setProduct(productBean.getProduct());
			product = productMgr.findByPrimaryKey(productPK);
			if (overWrite) {
				product.setEenheid(productBean.getEenheid());
				product.setOmschr1(productBean.getOmschr1());
				product.setOmschr2(productBean.getOmschr2());
				product.setOmschr3(productBean.getOmschr3());
				product.setPrijs(productBean.getPrijs());
				product.setPrijsInkoop(productBean.getPrijsInkoop());
				product.setRekeningNr(productBean.getRekeningNr());
			}
		} catch (Exception e) {
			productMgr.create(productBean);
		}
	}

	private void importRekening(String[] fields) throws Exception {
		int f = 1;
		rekeningBean.setBedrijf(bedrijfsCode);
		rekeningBean.setRekeningNr(fields[f++]);
		rekeningBean.setOmschrijving(fields[f++]);
		rekeningBean.setOmschrijvingKort(fields[f++]);
		rekeningBean.setVerdichting(Integer.parseInt(fields[f++]));
		rekeningBean.setICPSoort(fields[f++]);
		rekeningBean.setBtwCode(fields[f++]);
		rekeningBean.setBtwSoort(fields[f++]);
		rekeningBean.setBtwRekening(Boolean.getBoolean(fields[f++]));
		rekeningBean.setSubAdministratieType(fields[f++]);
		rekeningBean.setToonKolom(fields[f++]);
		rekeningBean.setBlocked(Boolean.getBoolean(fields[f++]));
		try {
			rekeningPK.setBedrijf(rekeningBean.getBedrijf());
			rekeningPK.setRekeningNr(rekeningBean.getRekeningNr());
			rekening = rekeningMgr.findByPrimaryKey(rekeningPK);
			if (overWrite) {
				rekening.setOmschrijving(rekeningBean.getOmschrijving());
				rekening.setOmschrijvingKort(rekeningBean.getOmschrijvingKort());
				rekening.setVerdichting(rekeningBean.getVerdichting());
				rekening.setICPSoort(rekeningBean.getICPSoort());
				rekening.setBtwCode(rekeningBean.getBtwCode());
				rekening.setBtwSoort(rekeningBean.getBtwSoort());
				rekening.setBtwRekening(rekeningBean.getBtwRekening());
				rekening.setSubAdministratieType(rekeningBean.getSubAdministratieType());
				rekening.setToonKolom(rekeningBean.getToonKolom());
				rekening.setBlocked(rekeningBean.getBlocked());
			}
		} catch (Exception e) {
			rekeningMgr.create(rekeningBean);
		}
	}

	private void importVerdichting(String[] fields) throws Exception {
		int f = 1;
		verdichtingBean.setBedrijf(bedrijfsCode);
		verdichtingBean.setId(Integer.parseInt(fields[f++]));
		verdichtingBean.setOmschrijving(fields[f++]);
		verdichtingBean.setHoofdverdichting(Integer.parseInt(fields[f++]));
		try {
			verdichtingPK.setBedrijf(verdichtingBean.getBedrijf());
			verdichtingPK.setId(verdichtingBean.getId());
			verdichting = verdichtingMgr.findByPrimaryKey(verdichtingPK);
			if (overWrite) {
				verdichting.setOmschrijving(verdichtingBean.getOmschrijving());
				verdichting.setHoofdverdichting(verdichtingBean.getHoofdverdichting());
			}
		} catch (Exception e) {
			verdichtingMgr.create(verdichtingBean);
		}
	}

	private Date createDateFrom(String d) throws Exception {
		if (d == null || d.trim().length() == 0) {
			return null;
		} else {
			return new Date(Long.parseLong(d));
		}
	}
}
