package nl.eadmin.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;
//import nl.looppunt.db.Bijlage;
//import nl.looppunt.db.BijlageManager;
//import nl.looppunt.db.BijlageManagerFactory;
//import nl.looppunt.db.Klant;
//import nl.looppunt.db.Opdracht;
//import nl.looppunt.enums.BijlageTypeEnum;

import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import nl.eadmin.MyException;

public class MailHelper implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Properties mailConfig;

	public static void mailErrorMsg(MyException myException) throws Exception {
		StackTraceElement[] ste = myException.getStackTrace();
		StringBuffer htmlBody = new StringBuffer();
		htmlBody.append("<br/>");
		htmlBody.append("<table>");
		for (int i = 0; i < ste.length; i++) {
			htmlBody.append("<tr><td>xxx:</td><td>").append(ste[i]).append("</td></tr>");
		}
		htmlBody.append("</table>");

		Multipart mp = new MimeMultipart();
		MimeBodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(htmlBody.toString(), "text/html");
		mp.addBodyPart(htmlPart);

		Session session = getSession();
		MimeMessage message = new MimeMessage(session);
		message.setContent(mp);
		message.setFrom(new InternetAddress("error@hpvl.nl"));
		message.setSubject("Applicatiefout: " + myException.getMessage());
		message.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse("luc.van.laere@zonnet.nl"));
		Transport.send(message);
	}

	private static Session getSession() {
		final Properties props = getMailConfig();
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props.getProperty("username"), props.getProperty("password"));
			}
		});
		return session;
	}

	private static Properties getMailConfig() {
		if (mailConfig == null) {
			mailConfig = new Properties();
			try (InputStream input = MailHelper.class.getClassLoader().getResourceAsStream("mail.properties")) {
				mailConfig.load(input);
			} catch (IOException ex) {
				System.err.println("Cannot open and load mail server properties file.");
			}
		}
		return mailConfig;
	}

}
