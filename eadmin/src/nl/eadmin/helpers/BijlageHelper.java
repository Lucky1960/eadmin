package nl.eadmin.helpers;

import nl.eadmin.db.Bijlage;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;

import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;
import com.j256.simplemagic.ContentType;

public class BijlageHelper {
	private static final ContentInfoUtil contentInfoUtil = new ContentInfoUtil();

	public static BinaryObject getBinaryObject(DataObject object, Bijlage bijlage) throws Exception {
		return getBinaryObject(object, bijlage.getData(), bijlage.getBestandsnaam(), bijlage.getOmschrijving());
	}

	public static BinaryObject getBinaryObject(DataObject object, byte[] bytes, String name, String description) throws Exception {
		if (name == null || name.trim().length() == 0)
			name = "Bijlage";
		if ((description == null || description.trim().length() == 0)) {
			int n = 0;
			if (name != null && (n = name.lastIndexOf(".")) >= 0)
				description = name.substring(0, n);
			else
				description = name;
		}

		String mimeType = null;
		ContentInfo fileInfo = contentInfoUtil.findMatch(bytes);

		if (fileInfo != null) {
			String[] extensions = fileInfo.getFileExtensions();
			mimeType = fileInfo.getMimeType();
			boolean extended = false;
			for (int i = 0; i < extensions.length; i++) {
				if (name.toLowerCase().endsWith("." + extensions[i])) {
					extended = true;
					break;
				}
			}
			if (extended == false) {
				String extension = null;
				for (int i = 0; i < extensions.length; i++) {
					if (extensions[i].equals(fileInfo.getName())) {
						extension = fileInfo.getName();
						break;
					}
				}
				if (extension == null)
					extension = extensions.length > 0 ? extensions[0] : fileInfo.getName();
				name = name + "." + extension;
			}

		} else {
			String fileExtention = "txt";
			ContentType contentType = ContentType.fromFileExtension(fileExtention);
			int n = 0;
			if ((n = name.lastIndexOf(".")) >= 0 && n + 1 < name.length()) {
				String ext = name.substring(n + 1).toLowerCase();
				ContentType type = ContentType.fromFileExtension(ext);
				if (type != null) {
					contentType = type;
				} else {
					name = name + "." + fileExtention;
				}
			} else {
				name = name + "." + fileExtention;
			}
			mimeType = contentType.getMimeType();
		}

		BinaryObject obj = new BinaryObject(bytes, mimeType, object);
		obj.setName(name);
		obj.setDescription(description);
		obj.openSaveAsDialog(name);
		return obj;
	}
}
