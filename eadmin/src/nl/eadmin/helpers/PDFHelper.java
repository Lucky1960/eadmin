package nl.eadmin.helpers;

import java.io.IOException;
import java.io.Serializable;
import java.util.Base64;

import nl.eadmin.databeans.AanmaningDataBean;
import nl.eadmin.databeans.BTWAangifteDataBean;
import nl.eadmin.databeans.BalansOverzichtDataBean;
import nl.eadmin.databeans.DebCredOverzichtDataBean;
import nl.eadmin.databeans.FactuurDataBean;
import nl.eadmin.databeans.GrootboekoverzichtDatabean;
import nl.ibs.esp.reporting.DataSource;
import nl.ibs.esp.reporting.ObjectDataSource;
import nl.ibs.esp.reporting.XHTMLMerger;
import nl.ibs.esp.util.CommonObjectDataReader;
//import nl.looppunt.db.Klant;
//import nl.looppunt.db.Opdracht;
//import nl.looppunt.ui.datareaders.KlantDataReader;
//import nl.looppunt.ui.datareaders.OpdrachtDataReader;
import nl.ibs.esp.util.ObjectDataReader;

import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Image;

public class PDFHelper implements Serializable {
	private static final long serialVersionUID = 1L;
	private static String btwTemplate;
	private static ObjectDataReader btwAangifteReader;
	private static B64ImgReplacedElementFactory base64ImgReplacedElementFactory;

	// private static String webAppURL;

	public static byte[] getBTWAangiftePDF(final BTWAangifteDataBean btwAangifteDataBean) throws Exception {
		if (btwAangifteReader == null)
			btwAangifteReader = CommonObjectDataReader.getInstance(BTWAangifteDataBean.class);
		if (btwTemplate == null)
			btwTemplate = StringHelper.convertStreamToString(MailHelper.class.getResourceAsStream("/nl/eadmin/templates/BTWAangifte.html"));
		String result = new XHTMLMerger().process(new DataSource() {
			private static final long serialVersionUID = 1L;

			public boolean hasNext(String level) throws Exception {
				return false;
			}

			public Object getValue(String key) throws Exception {
				return btwAangifteReader.getValue(btwAangifteDataBean, key);
			}
		}, btwTemplate);
		return nl.ibs.esp.reporting.PDFHelper.createPDFByteArrayfromXHTML(result);
	}

	public static byte[] getGrootboekOverzichtPDF(final GrootboekoverzichtDatabean grootboekoverzichtDatabean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.GROOTBOEKOVERZICHT_HTML);
		String result = new XHTMLMerger().process(new ObjectDataSource(grootboekoverzichtDatabean), template);
		return nl.ibs.esp.reporting.PDFHelper.createPDFByteArrayfromXHTML(result);
	}

	public static byte[] getBalansOverzichtPDF(final BalansOverzichtDataBean balansOverzichtDatabean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.BALANSOVERZICHT_HTML);
		String result = new XHTMLMerger().process(new ObjectDataSource(balansOverzichtDatabean), template);
		return nl.ibs.esp.reporting.PDFHelper.createPDFByteArrayfromXHTML(result);
	}

	public static byte[] getDebiteurenOverzichtPDF(final DebCredOverzichtDataBean debCredOverzichtDataBean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.DEBITEURENOVERZICHT_HTML);
		String result = new XHTMLMerger().process(new ObjectDataSource(debCredOverzichtDataBean), template);
		return nl.ibs.esp.reporting.PDFHelper.createPDFByteArrayfromXHTML(result);
	}

	public static byte[] getCrediteurenOverzichtPDF(final DebCredOverzichtDataBean debCredOverzichtDataBean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.CREDITEURENOVERZICHT_HTML);
		String result = new XHTMLMerger().process(new ObjectDataSource(debCredOverzichtDataBean), template);
		return nl.ibs.esp.reporting.PDFHelper.createPDFByteArrayfromXHTML(result);
	}

	public static byte[] getAanmaningPDF(final AanmaningDataBean aanmaningDataBean) throws Exception {
		String template = TemplateHelper.getTemplate(TemplateHelper.AANMANING_HTML);
		String result = new XHTMLMerger().process(new ObjectDataSource(aanmaningDataBean), template);
		return nl.ibs.esp.reporting.PDFHelper.createPDFByteArrayfromXHTML(result);
	}

	public static byte[] getFactuurPDF(FactuurDataBean factuurDataBean) throws Exception {
		byte[] ftBytes = factuurDataBean.getFactuurTemplate();
		String template = ftBytes == null || ftBytes.length < 15 ? TemplateHelper.getTemplate(TemplateHelper.FACTUUR_HTML) : new String(ftBytes, "UTF8");
		String result = new XHTMLMerger().process(new ObjectDataSource(factuurDataBean), template);
		return nl.ibs.esp.reporting.PDFHelper.createPDFByteArrayfromXHTML(result, getB64ImgReplacedElementFactory());
	}

	public final static ReplacedElementFactory getB64ImgReplacedElementFactory() {
		if (base64ImgReplacedElementFactory == null)
			base64ImgReplacedElementFactory = new B64ImgReplacedElementFactory();
		return base64ImgReplacedElementFactory;
	}

	/*
	 * public static String getWebAppURL() { if (webAppURL==null) webAppURL=
	 * ESPSessionContext.getConfiguration().getWebAppURL(); return webAppURL; }
	 */

	public final static class B64ImgReplacedElementFactory implements ReplacedElementFactory {

		public ReplacedElement createReplacedElement(LayoutContext c, BlockBox box, UserAgentCallback uac, int cssWidth, int cssHeight) {
			Element e = box.getElement();
			if (e == null) {
				return null;
			}
			String nodeName = e.getNodeName();
			if (nodeName.equals("img")) {
				String src = e.getAttribute("src");
				FSImage fsImage;
				try {
					if (src.startsWith("data:image/")) {
						String b64encoded = src.substring(src.indexOf("base64,") + "base64,".length(), src.length());
						byte[] decodedBytes = Base64.getDecoder().decode(b64encoded);
						fsImage = new ITextFSImage(Image.getInstance(decodedBytes));
					} else {
						fsImage = uac.getImageResource(src).getImage();
					}
				} catch (BadElementException e1) {
					fsImage = null;
				} catch (IOException e1) {
					fsImage = null;
				}
				if (fsImage != null) {
					if (cssWidth != -1 || cssHeight != -1) {
						fsImage.scale(cssWidth, cssHeight);
					}
					return new ITextImageElement(fsImage);
				}
			}
			return null;
		}

		public void remove(Element e) {
		}

		public void reset() {
		}

		@Override
		public void setFormSubmissionListener(FormSubmissionListener arg0) {
			// TODO Auto-generated method stub

		}
	}

}
