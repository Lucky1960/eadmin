package nl.eadmin.helpers;

import java.util.Collection;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieManager;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.impl.BtwCategorieManager_Impl;
import nl.eadmin.db.impl.RekeningManager_Impl;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class RekeningHelper {
	private RekeningManager rekeningMgr;
	private BtwCategorieManager btwCatMgr;

	public RekeningHelper(DBData dbd) throws Exception {
		this.rekeningMgr = RekeningManagerFactory.getInstance(dbd);
		this.btwCatMgr = BtwCategorieManagerFactory.getInstance(dbd);
	}

	public Collection<?> getSubAdmRekeningen(Bedrijf bedrijf, String subAdministratie) throws Exception {
		StringBuilder filter = new StringBuilder();
		filter.append(Rekening.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' ");
		filter.append("AND " + Rekening.SUB_ADMINISTRATIE_TYPE + "='" + subAdministratie + "'"); // D of C
		ExecutableQuery qry = QueryFactory.create((RekeningManager_Impl) rekeningMgr, filter.toString());
		return qry.getCollection();
	}

	public void resetBtwCategorie(Rekening rekening) throws Exception {
		btwCatMgr.generalUpdate(BtwCategorie.REKENING_NR + "=''", BtwCategorie.BEDRIJF + "='" + rekening.getBedrijf() + "' AND " + BtwCategorie.REKENING_NR + "='" + rekening.getRekeningNr() + "'");
	}

	public void resetBtwCategorie(Bedrijf bedrijf, String btwCat) throws Exception {
		StringBuilder filter = new StringBuilder();
		filter.append("SELECT " + BtwCategorie.REKENING_NR + " FROM BtwCategorie");
		filter.append(" WHERE " + BtwCategorie.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		filter.append("   AND " + BtwCategorie.CODE + "='" + btwCat + "'");
		FreeQuery qry = QueryFactory.createFreeQuery((BtwCategorieManager_Impl) btwCatMgr, filter.toString());
		Object[][] rslt = qry.getResultArray();
		String rekNr;
		if (rslt.length > 0 && rslt[0].length > 0) {
			for (int i = 0; i < rslt.length; i++) {
				rekNr = (rslt[i][0] == null) ? "" : ((String) rslt[i][0]);
				String setClause = Rekening.BTW_REKENING + "='false', " + Rekening.BTW_CODE + "='L'";
				String whereClause = Rekening.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Rekening.REKENING_NR + "='" + rekNr + "'";
				rekeningMgr.generalUpdate(setClause, whereClause);
			}
		}
	}

	public void setBtwCategorie(Rekening rekening, String btwCat) throws Exception {
		if (btwCat.trim().length() == 0) {
			rekening.setBtwRekening(false);
		} else {
			rekening.setBtwRekening(true);
			rekening.setBtwCode("L");
		}
	}

	public void checkBtwCategorie(Rekening rekening, String btwCat) throws Exception {
		if (rekening == null || btwCat.trim().length() == 0) {
			return;
		}
		// Controleer of btwCode niet reeds is toegewezen aan een andere
		// grootboekrekening
		StringBuilder filter = new StringBuilder();
		filter.append(BtwCategorie.BEDRIJF + "='" + rekening.getBedrijf() + "' AND ");
		filter.append("(" + BtwCategorie.REKENING_NR + "<>'" + rekening.getRekeningNr() + "' AND " + BtwCategorie.REKENING_NR + "<>'' AND " + BtwCategorie.CODE + "='" + btwCat + "') OR ");
		filter.append("(" + BtwCategorie.REKENING_NR + "='" + rekening.getRekeningNr() + "' AND " + BtwCategorie.CODE + "<>'" + btwCat + "')");
		ExecutableQuery qry = QueryFactory.create((BtwCategorieManager_Impl) btwCatMgr, filter.toString());
		BtwCategorie cat = (BtwCategorie) qry.getFirstObject();
		if (cat != null) {
			throw new UserMessageException("BTW-categorie '" + cat.getCode() + "' is al gekoppeld aan grootboekrekening '" + cat.getRekeningNr() + " - " + cat.getOmschrijving() + "'");
		}
	}
}