package nl.eadmin.helpers;

import java.util.Calendar;
import java.util.Date;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.PeriodePK;
import nl.eadmin.db.impl.PeriodeManager_Impl;
import nl.eadmin.enums.DateTypeEnum;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public abstract class DateHelper {

	public static Date getStartDate(Bedrijf bedrijf, int boekjaar) throws Exception {
		PeriodePK key = new PeriodePK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setBoekjaar(boekjaar);
		key.setBoekperiode(1);
		try {
			return PeriodeManagerFactory.getInstance(bedrijf.getDBData()).findByPrimaryKey(key).getStartdatum();
		} catch (Exception e) {
			throw new UserMessageException("Geen startdatum gevonden voor dit boekjaar");
		}
	}

	public static Date getEndDate(Bedrijf bedrijf, int boekjaar) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Periode WHERE ");
		sb.append(Periode.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		sb.append(Periode.BOEKJAAR + "=" + boekjaar + " ");
		sb.append("ORDER BY " + Periode.BOEKPERIODE + " descending");
		FreeQuery qry = QueryFactory.createFreeQuery((PeriodeManager_Impl) PeriodeManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		try {
			return ((Periode) qry.getFirstObject()).getEinddatum();
		} catch (Exception e) {
			throw new UserMessageException("Geen einddatum gevonden voor dit boekjaar");
		}
	}

	@SuppressWarnings("deprecation")
	public static String getSQLdateString(Date date) throws Exception {
		return (date.getYear() + 1900) + "-" + (date.getMonth() + 1) + "-" + date.getDate();
	}

	public static int[] bepaalBoekperiode(Bedrijf bedrijf, Date boekDatum) throws Exception {
		int[] result = new int[] { 0, 0 };
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Periode WHERE ");
		sb.append(Periode.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		sb.append(Periode.STARTDATUM + " <= '" + dateToYYYYMMDD(boekDatum, "-") + " 00:00:00' AND ");
		sb.append(Periode.EINDDATUM + " >= '" + dateToYYYYMMDD(boekDatum, "-") + " 00:00:00'");
		FreeQuery qry = QueryFactory.createFreeQuery((PeriodeManager_Impl) PeriodeManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		Periode periode = (Periode) qry.getFirstObject();
		if (periode != null) {
			result[0] = periode.getBoekjaar();
			result[1] = periode.getBoekperiode();
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	public static String dateToYYYYMMDD(Date date, String separator) throws Exception {
		int y = date.getYear() + 1900;
		int m = date.getMonth() + 1;
		int d = date.getDate();
		return y + separator + (m < 10 ? "0" : "") + m + separator + (d < 10 ? "0" : "") + d;
	}

	@SuppressWarnings("deprecation")
	public static String dateToDDMMYYYY(Date date, String separator) throws Exception {
		int y = date.getYear() + 1900;
		int m = date.getMonth() + 1;
		int d = date.getDate();
		return (d < 10 ? "0" : "") + d + separator + (m < 10 ? "0" : "") + m + separator + y;
	}

	public static Date YYYYMMDDToDate(String date) throws Exception {
		try {
			String[] s = null;
			if (date.indexOf("-") >= 0) {
				s = date.split("-");
			} else {
				s = new String[3];
				s[0] = date.substring(0, 4);
				s[1] = date.substring(4, 6);
				s[2] = date.substring(6);
			}
			Calendar tmpCal = Calendar.getInstance();
			tmpCal.setLenient(false);
			tmpCal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(s[2]));
			tmpCal.set(Calendar.MONTH, Integer.parseInt(s[1]) - 1);
			tmpCal.set(Calendar.YEAR, Integer.parseInt(s[0]));
			tmpCal.set(Calendar.AM_PM, Calendar.AM);
			tmpCal.set(Calendar.HOUR, 0);
			tmpCal.set(Calendar.MINUTE, 0);
			tmpCal.set(Calendar.SECOND, 0);
			tmpCal.set(Calendar.MILLISECOND, 0);
			return tmpCal.getTime();
		} catch (Exception e) {
			return null;
		}
	}

	public static Date DDMMYYYYToDate(String date) throws Exception {
		try {
			String[] s = null;
			if (date.indexOf("-") >= 0) {
				s = date.split("-");
			} else {
				s = new String[3];
				s[0] = date.substring(0, 2);
				s[1] = date.substring(2, 4);
				s[2] = date.substring(4);
			}
			Calendar tmpCal = Calendar.getInstance();
			tmpCal.setLenient(false);
			tmpCal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(s[0]));
			tmpCal.set(Calendar.MONTH, Integer.parseInt(s[1]) - 1);
			tmpCal.set(Calendar.YEAR, Integer.parseInt(s[2]));
			tmpCal.set(Calendar.AM_PM, Calendar.AM);
			tmpCal.set(Calendar.HOUR, 0);
			tmpCal.set(Calendar.MINUTE, 0);
			tmpCal.set(Calendar.SECOND, 0);
			tmpCal.set(Calendar.MILLISECOND, 0);
			return tmpCal.getTime();
		} catch (Exception e) {
			return null;
		}
	}

	public static Date stringToDate(String s) throws Exception {
		int type = -1;
		int p1 = s.indexOf("-");
		int p2 = s.lastIndexOf("-");
		if (p1 > 0) {
			if (p1 > s.length() - p2) {
				type = DateTypeEnum.YYYY_MM_DD;
			} else {
				type = DateTypeEnum.DD_MM_YYYY;
			}
		} else {
			try {
				DateHelper.YYYYMMDDToDate(s);
				type = DateTypeEnum.YYYYMMDD;
			} catch (Exception e) {
				try {
					DateHelper.DDMMYYYYToDate(s);
					type = DateTypeEnum.DDMMYYYY;
				} catch (Exception e1) {
					return null;
					// throw new UserMessageException("Onbekend datumformaat: "
					// + s);
				}
			}
		}
		return stringToDate(s, type);
	}

	public static Date stringToDate(String s, int type) throws Exception {
		switch (type) {
		case DateTypeEnum.YYYYMMDD:
			return YYYYMMDDToDate(s);
		case DateTypeEnum.YYYY_MM_DD:
			return YYYYMMDDToDate(s);
		case DateTypeEnum.DDMMYYYY:
			return DDMMYYYYToDate(s);
		case DateTypeEnum.DD_MM_YYYY:
			return DDMMYYYYToDate(s);
		default:
			return null;
		}
	}
}