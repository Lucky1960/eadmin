package nl.eadmin.helpers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.databeans.FacturenLijstDataBean;
import nl.eadmin.databeans.FactuurMutatieDataBean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalDataBean;
import nl.eadmin.db.JournaalManager;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManager;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.PeriodePK;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.impl.BtwCategorieManager_Impl;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.selection.DebCredLijstSelection;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.Query;
import nl.ibs.jsql.QueryFactory;

public class JournaalHelper {
	private DBData dbd;
	private JournaalManager jrnMgr;
	private RekeningManager rekMgr;

	public JournaalHelper(DBData dbd) throws Exception {
		this.dbd = dbd;
		this.jrnMgr = JournaalManagerFactory.getInstance(dbd);
		this.rekMgr = RekeningManagerFactory.getInstance(dbd);
	}

	public void createJournals(Bedrijf bedrijf, int bookYearFrom, int bookPeriodFrom, int bookYearTo, int bookPeriodTo) throws Exception {
		if (bookYearFrom != 0 && bookPeriodTo != 0) {
			PeriodeManager mgr = PeriodeManagerFactory.getInstance(bedrijf.getDBData());
			PeriodePK key = new PeriodePK();
			key.setBedrijf(bedrijf.getBedrijfscode());
			key.setBoekjaar(bookYearFrom);
			key.setBoekperiode(bookPeriodFrom);
			Periode periodFrom = mgr.findByPrimaryKey(key);
			key.setBoekjaar(bookYearTo);
			key.setBoekperiode(bookPeriodTo);
			Periode periodTo = mgr.findByPrimaryKey(key);
			createJournals(bedrijf, periodFrom.getStartdatum(), periodTo.getEinddatum());
		}
	}

	public void createJournals(Bedrijf bedrijf, Date dateFrom, Date dateTo) throws Exception {
		StringBuilder sbJrn = new StringBuilder();
		sbJrn.append(Journaal.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		sbJrn.append(Journaal.BOEKDATUM + ">='" + DateHelper.getSQLdateString(dateFrom) + "' AND ");
		sbJrn.append(Journaal.BOEKDATUM + "<='" + DateHelper.getSQLdateString(dateTo) + "' ");
		jrnMgr.generalDelete(sbJrn.toString());
		StringBuilder sbHdr = new StringBuilder();
		sbHdr.append(HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		sbHdr.append(HeaderData.BOEKDATUM + ">='" + DateHelper.getSQLdateString(dateFrom) + "' AND ");
		sbHdr.append(HeaderData.BOEKDATUM + "<='" + DateHelper.getSQLdateString(dateTo) + "' ");
		Iterator<?> headers = HeaderDataManagerFactory.getInstance(dbd).getCollection(QueryFactory.create(sbHdr.toString(), HeaderData.BOEKDATUM)).iterator();
		while (headers.hasNext()) {
			createJournals((HeaderData) headers.next(), false);
		}
	}

	public void createJournals(HeaderData headerData, boolean deleteFirst) throws Exception {
		DBData dbd = headerData.getDBData();
		JournaalManager jrnMgr = JournaalManagerFactory.getInstance(dbd);
		JournaalDataBean bean = new JournaalDataBean();
		BigDecimal ZERO = ApplicationConstants.ZERO;
		Rekening rekening = null;
		int seqNr = 0;

		// Wis bestaande records
		if (deleteFirst) {
			deleteJournals(headerData);
		}

		// Schrijf journaal voor kopregel
		try {
			rekening = headerData.getDagboekObject().getRekeningObject();
		} catch (Exception e) {
			rekening = null;
		}
		if (rekening != null) {
			bean.setBedrijf(headerData.getBedrijf());
			bean.setBoekdatum(headerData.getBoekdatum());
			bean.setBoekjaar(headerData.getBoekjaar());
			bean.setBoekperiode(headerData.getBoekperiode());
			bean.setBoekstuk(headerData.getBoekstuk());
			bean.setBtwCode("L");
			bean.setBtwCategorie("");
			bean.setBtwAangifteKolom("");
			bean.setDagboekId(headerData.getDagboekId());
			bean.setDagboekSoort(GeneralHelper.getDagboekSoort(dbd, headerData.getBedrijf(), headerData.getDagboekId()));
			bean.setDC(headerData.getDC());
			bean.setDcNummer(headerData.getDcNummer());
			bean.setFactuurNr(headerData.getFactuurNummer());
			bean.setICPSoort(rekening.getICPSoort());
			bean.setOmschr1(headerData.getOmschr1());
			bean.setOmschr2(headerData.getOmschr2());
			bean.setOmschr3("");
			bean.setRekeningNr(rekening.getRekeningNr());
			bean.setRekeningOmschr(rekening.getOmschrijving());
			bean.setRekeningsoort(rekening.getRekeningSoort());
			bean.setSeqNr(seqNr++);
			if (headerData.getDC().equals("C")) {
				bean.setBedragCredit(headerData.getTotaalIncl());
				bean.setBedragDebet(ZERO);
			} else {
				bean.setBedragCredit(ZERO);
				bean.setBedragDebet(headerData.getTotaalIncl());
			}
			jrnMgr.create(bean);
		}

		// Schrijf journaal voor detailRegels
		boolean isBtwRekening;
		String btwCat = null;
		BtwCode btwCode = null;
		DetailData detailData;
		Iterator<?> details = headerData.getDetails().iterator();
		while (details.hasNext()) {
			detailData = (DetailData) details.next();
			btwCode = detailData.getBtwCodeObject();
			rekening = detailData.getRekeningObject();
			isBtwRekening = rekening.getBtwRekening();
			btwCat = rekening.getBtwSoort().equals("V") ? btwCode.getBtwCatVerkoop() : btwCode.getBtwCatInkoop();

			// 1. Schrijf journaal voor detailRegel excl BTW
			if (!isBtwRekening) {
				bean.setBedrijf(detailData.getBedrijf());
				bean.setBoekdatum(headerData.getBoekdatum());
				bean.setBoekjaar(headerData.getBoekjaar());
				bean.setBoekperiode(headerData.getBoekperiode());
				bean.setBoekstuk(detailData.getBoekstuk());
				bean.setBtwCode(detailData.getBtwCode());
				bean.setBtwCategorie(btwCat);
				bean.setBtwAangifteKolom("O");
				bean.setDagboekId(detailData.getDagboekId());
				bean.setDagboekSoort(GeneralHelper.getDagboekSoort(dbd, detailData.getBedrijf(), detailData.getDagboekId()));
				bean.setDC(detailData.getDC());
				bean.setDcNummer(detailData.getDcNummer());
				bean.setFactuurNr(detailData.getFactuurNummer());
				bean.setICPSoort(rekening.getICPSoort());
				bean.setOmschr1(detailData.getOmschr1());
				bean.setOmschr2(detailData.getOmschr2());
				bean.setOmschr3(detailData.getOmschr3());
				bean.setRekeningNr(rekening.getRekeningNr());
				bean.setRekeningOmschr(rekening.getOmschrijving());
				bean.setRekeningsoort(rekening.getRekeningSoort());
				bean.setSeqNr(seqNr++);
				if (detailData.getDC().equals("C")) {
					bean.setBedragCredit(detailData.getBedragExcl());
					bean.setBedragDebet(ZERO);
				} else {
					bean.setBedragCredit(ZERO);
					bean.setBedragDebet(detailData.getBedragExcl());
				}
				jrnMgr.create(bean);
			}

			// 2. Schrijf journaal voor BTW-bedrag
			if (detailData.getBedragBtw().doubleValue() != 0d || isBtwRekening) {
				if (isBtwRekening) {
					btwCat = getBtwCategorie(rekening.getBedrijf(), rekening.getRekeningNr());
				} else {
					rekening = getBtwRekening(detailData.getBedrijf(), btwCat);
				}
				if (rekening != null) {
					bean.setBedrijf(detailData.getBedrijf());
					bean.setBoekdatum(headerData.getBoekdatum());
					bean.setBoekjaar(headerData.getBoekjaar());
					bean.setBoekperiode(headerData.getBoekperiode());
					bean.setBoekstuk(detailData.getBoekstuk());
					bean.setBtwCode("");
					bean.setBtwCategorie(btwCat);
					bean.setBtwAangifteKolom("B");
					bean.setDagboekId(detailData.getDagboekId());
					bean.setDagboekSoort(GeneralHelper.getDagboekSoort(dbd, detailData.getBedrijf(), detailData.getDagboekId()));
					bean.setDC(detailData.getDC());
					bean.setDcNummer(detailData.getDcNummer());
					bean.setFactuurNr(detailData.getFactuurNummer());
					bean.setICPSoort(rekening.getICPSoort());
					bean.setOmschr1(detailData.getOmschr1());
					bean.setOmschr2(detailData.getOmschr2());
					bean.setOmschr3(detailData.getOmschr3());
					bean.setRekeningNr(rekening.getRekeningNr());
					bean.setRekeningOmschr(rekening.getOmschrijving());
					bean.setRekeningsoort(rekening.getRekeningSoort());
					bean.setSeqNr(seqNr++);
					if (detailData.getDC().equals("C")) {
						bean.setBedragCredit(isBtwRekening ? detailData.getBedragIncl() : detailData.getBedragBtw());
						bean.setBedragDebet(ZERO);
					} else {
						bean.setBedragCredit(ZERO);
						bean.setBedragDebet(isBtwRekening ? detailData.getBedragIncl() : detailData.getBedragBtw());
					}
					jrnMgr.create(bean);
				}
			}
		}
	}

	private String getBtwCategorie(String bedrijf, String rekNr) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT BTWCAT FROM BtwCategorie WHERE ");
		sb.append("BDR ='" + bedrijf + "' AND REKNR = '" + rekNr + "'");
		FreeQuery qry = QueryFactory.createFreeQuery((BtwCategorieManager_Impl) BtwCategorieManagerFactory.getInstance(dbd), sb.toString());
		Object[][] rslt = qry.getResultArray();
		if (rslt.length > 0 && rslt[0].length > 0) {
			return (String) rslt[0][0];
		} else {
			return "";
		}
	}

	private Rekening getBtwRekening(String bedrijf, String btwCat) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT REKNR FROM BtwCategorie WHERE ");
		sb.append("BDR ='" + bedrijf + "' AND BTWCAT = '" + btwCat + "'");
		FreeQuery qry = QueryFactory.createFreeQuery((BtwCategorieManager_Impl) BtwCategorieManagerFactory.getInstance(dbd), sb.toString());
		Object[][] rslt = qry.getResultArray();
		if (rslt.length > 0 && rslt[0].length > 0) {
			String rekNr = (String) rslt[0][0];
			sb = new StringBuilder();
			sb.append(Rekening.BEDRIJF + "='" + bedrijf + "' AND ");
			sb.append(Rekening.REKENING_NR + "='" + rekNr + "'");
			Query query = QueryFactory.create(Rekening.class, sb.toString());
			return rekMgr.getFirstObject(query);
		} else {
			return null;
		}
	}

	public void deleteJournals(HeaderData headerData) throws Exception {
		if (headerData == null) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(Journaal.BEDRIJF + "='" + headerData.getBedrijf() + "' AND ");
		sb.append(Journaal.DAGBOEK_ID + "='" + headerData.getDagboekId() + "' AND ");
		sb.append(Journaal.BOEKSTUK + "='" + headerData.getBoekstuk() + "'");
		jrnMgr.generalDelete(sb.toString());
	}

	public Collection<?> getJournals(String bedrijf, String dagboek, String boekstuk) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append(Journaal.BEDRIJF + "='" + bedrijf + "' AND ");
		sb.append(Journaal.DAGBOEK_ID + "='" + dagboek + "' AND ");
		sb.append(Journaal.BOEKSTUK + "='" + boekstuk + "'");
		Query query = QueryFactory.create(Journaal.class, sb.toString());
		query.setOrdering(Journaal.DC + " descending, " + Journaal.SEQ_NR);
		return jrnMgr.getCollection(query);
	}

	public Collection<FacturenLijstDataBean> getSubBeans(String bedrijf, String dc, String dcNr, DebCredLijstSelection selection) throws Exception {
		Collection<FacturenLijstDataBean> beans = new ArrayList<FacturenLijstDataBean>();
		String rekNrs = GeneralHelper.getDebCredRekeningAsString(dbd, bedrijf, dc);
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DAGBOEK, BOEKSTUK, BOEKDTM, FACTUUR FROM Journaal WHERE ");
		sb.append(Journaal.BEDRIJF + "='" + bedrijf + "' AND ");
		sb.append(Journaal.DC_NUMMER + "='" + dcNr + "' AND ");
		sb.append("(" + Journaal.DAGBOEK_SOORT + "='" + (dc.equals("C") ? DagboekSoortEnum.INKOOP : DagboekSoortEnum.VERKOOP) + "' OR ");
		sb.append(Journaal.DAGBOEK_SOORT + "='" + DagboekSoortEnum.MEMO + "')");
		sb.append(" AND DAGBOEK <> 'SYS1'");
		sb.append(" AND " + rekNrs);
		if (selection != null && selection.getDateFrom() != null) {
			sb.append(" AND BOEKDTM >= '" + DateHelper.getSQLdateString(selection.getDateFrom()) + "'");
		}
		if (selection != null && selection.getDateTo() != null) {
			sb.append(" AND BOEKDTM <= '" + DateHelper.getSQLdateString(selection.getDateTo()) + "'");
		}
		sb.append(" GROUP BY " + Journaal.DAGBOEK_ID + ", " + Journaal.BOEKSTUK + ", " + Journaal.FACTUUR_NR);
		sb.append(" ORDER BY " + Journaal.BOEKDATUM + " descending");
		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) jrnMgr, sb.toString());
		Object[][] rslt = qry.getResultArray();
		BigDecimal ZERO = ApplicationConstants.ZERO;
		Date boekDatum;
		String dagboek, factuur;
		BigDecimal bedragDebet, bedragCredit;
		if (rslt.length > 0 && rslt[0].length > 0) {
			for (int i = 0; i < rslt.length; i++) {
				dagboek = (rslt[i][0] == null) ? "" : ((String) rslt[i][0]);
//				boekstuk = (rslt[i][1] == null) ? "" : ((String) rslt[i][1]);
				boekDatum = (rslt[i][2] == null) ? null : ((Date) rslt[i][2]);
				factuur = (rslt[i][3] == null) ? "" : ((String) rslt[i][3]);
				StringBuffer sb2 = new StringBuffer();
				sb2.append("SELECT SUM(BEDRAGDEB), SUM(BEDRAGCRED)");
				sb2.append("  FROM Journaal");
				sb2.append(" WHERE BDR = '" + bedrijf + "'");
				sb2.append("   AND DCNR = '" + dcNr + "'");
				sb2.append("   AND FACTUUR = '" + factuur + "' AND " + rekNrs);
				sb2.append("   AND DAGBOEK <> 'SYS1'");
				if (selection != null && selection.getDateFrom() != null) {
					sb2.append(" AND BOEKDTM >= '" + DateHelper.getSQLdateString(selection.getDateFrom()) + "'");
				}
				if (selection != null && selection.getDateTo() != null) {
					sb2.append(" AND BOEKDTM <= '" + DateHelper.getSQLdateString(selection.getDateTo()) + "'");
				}
				FreeQuery qry2 = QueryFactory.createFreeQuery((JournaalManager_Impl) jrnMgr, sb2.toString());
				Object[][] rslt2 = qry2.getResultArray();
				bedragDebet = rslt2[0][0] == null ? ZERO : (BigDecimal) rslt2[0][0];
				bedragCredit = rslt2[0][1] == null ? ZERO : (BigDecimal) rslt2[0][1];
				beans.add(new FacturenLijstDataBean(bedrijf, dc, dcNr, dagboek, factuur, boekDatum, bedragDebet, bedragCredit, selection));
			}
		}
		return beans;
	}

	public Collection<FactuurMutatieDataBean> getSubBeans(String bedrijf, String dc, String dcNr, String factuurNr, DebCredLijstSelection selection) throws Exception {
		String rekNrs = GeneralHelper.getDebCredRekeningAsString(dbd, bedrijf, dc);
		Collection<FactuurMutatieDataBean> beans = new ArrayList<FactuurMutatieDataBean>();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT BOEKDTM, DAGBOEK, BOEKSTUK, OMSCHR1, OMSCHR2, OMSCHR3, BEDRAGDEB, BEDRAGCRED FROM Journaal WHERE ");
		sb.append(Journaal.BEDRIJF + "='" + bedrijf + "' AND ");
		sb.append(Journaal.DC_NUMMER + "='" + dcNr + "' AND ");
		sb.append(Journaal.FACTUUR_NR + "='" + factuurNr + "' AND " + rekNrs);
		sb.append(" AND DAGBOEK <> 'SYS1'");
		if (selection != null && selection.getDateFrom() != null) {
			sb.append(" AND " + Journaal.BOEKDATUM + " >= '" + DateHelper.getSQLdateString(selection.getDateFrom()) + "'");
		}
		if (selection != null && selection.getDateTo() != null) {
			sb.append(" AND " + Journaal.BOEKDATUM + " <= '" + DateHelper.getSQLdateString(selection.getDateTo()) + "'");
		}
		sb.append(" ORDER BY " + Journaal.BOEKDATUM + " ascending");
		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) jrnMgr, sb.toString());
		Object[][] rslt = qry.getResultArray();
		BigDecimal ZERO = ApplicationConstants.ZERO;
		Date boekDatum;
		String dagboek, boekstuk, omschr1, omschr2, omschr3;
		BigDecimal bedragDebet, bedragCredit;
		if (rslt.length > 0 && rslt[0].length > 0) {
			for (int i = 0; i < rslt.length; i++) {
				boekDatum = (rslt[i][0] == null) ? null : ((Date) rslt[i][0]);
				dagboek = (rslt[i][1] == null) ? "" : ((String) rslt[i][1]);
				boekstuk = (rslt[i][2] == null) ? "" : ((String) rslt[i][2]);
				omschr1 = (rslt[i][3] == null) ? "" : ((String) rslt[i][3]);
				omschr2 = (rslt[i][4] == null) ? "" : ((String) rslt[i][4]);
				omschr3 = (rslt[i][5] == null) ? "" : ((String) rslt[i][5]);
				bedragDebet = rslt[i][6] == null ? ZERO : (BigDecimal) rslt[i][6];
				bedragCredit = rslt[i][7] == null ? ZERO : (BigDecimal) rslt[i][7];
				beans.add(new FactuurMutatieDataBean(boekDatum, dagboek, boekstuk, omschr1, omschr2, omschr3, bedragDebet, bedragCredit));
			}
		}
		return beans;
	}
}