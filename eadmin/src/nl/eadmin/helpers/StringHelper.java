package nl.eadmin.helpers;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class StringHelper {

	public static String truncate(String value, int len) throws Exception {
		if (value.length() <= len) {
			return value.trim();
		} else {
			return value.substring(0, len).trim();
		}
	}

	public static String suppressDoubleSpaces(String value) throws Exception {
		while (value.contains("  ")) {
			value = value.replaceFirst("  ", " ");
		}
		return value.trim();
	}

	public static String[] split(String s, char separator, boolean removeQuotes) throws Exception {
		String[] result = new String[0];
		if (s != null) {
			StringBuilder sb = new StringBuilder();
			ArrayList<String> list = new ArrayList<String>(); 
			boolean quoted = false;
			char[] c = s.toCharArray();
			for (int i = 0; i < c.length; i++) {
				if (c[i] == '"') {
					quoted = !quoted;
					if (removeQuotes) {
						continue;
					}
				}
				if (c[i] == separator && !quoted) {
					list.add(sb.toString());
					sb = new StringBuilder();
					continue;
				}
				sb.append(c[i]);
			}
			if (sb.length() > 0) {
				list.add(sb.toString());
			}
			result = new String[list.size()];
			System.arraycopy(list.toArray(), 0, result, 0, list.size());
		}
		return result;
	}

	static public String convertStreamToString(java.io.InputStream is) throws IOException {
		StringBuffer result = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF8"));
		String line;
		while ((line = br.readLine()) != null) {
			result.append(line);
			result.append("\n");
		}
		return result.toString();
	}

	public static byte[] getBytes(InputStream is) throws IOException {
		int len;
		int size = 1024;
		byte[] buf;

		if (is instanceof ByteArrayInputStream) {
			size = is.available();
			buf = new byte[size];
			len = is.read(buf, 0, size);
		} else {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			buf = new byte[size];
			while ((len = is.read(buf, 0, size)) != -1)
				bos.write(buf, 0, len);
			buf = bos.toByteArray();
		}
		return buf;
	}

	public static final String replace(String string, String s1, String s2) {
		int y = string.indexOf(s1);
		if (y >= 0) {
			StringBuffer buffer = new StringBuffer(string.length());
			int x = 0;
			while (y >= 0) {
				buffer.append(string.substring(x, y));
				buffer.append(s2);
				x = y + s1.length();
				y = string.length() <= x ? -1 : string.indexOf(s1, x);
			}
			if (x < string.length())
				buffer.append(string.substring(x, string.length()));
			return buffer.toString();
		} else {
			return string;
		}
	}

}
