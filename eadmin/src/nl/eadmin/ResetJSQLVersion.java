package nl.eadmin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class ResetJSQLVersion {

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/eadmin";
	private static final String SQLSTMNT = "UPDATE EADMIN.JSQLVRSN SET VERSION_NUMBER = '0' WHERE VERSION_TYPE = 'APPLICATION_VERSION'";

	public static void main(String[] args) throws Exception {
		Class.forName(DRIVER).newInstance();
		Connection con = DriverManager.getConnection(URL, "root", "Admin123!");
		Statement stmt = con.createStatement();
		stmt.execute(SQLSTMNT);
		stmt.close();
		con.close();
		System.out.println("Databaselevel teruggezet");
		System.exit(0);
	}
}