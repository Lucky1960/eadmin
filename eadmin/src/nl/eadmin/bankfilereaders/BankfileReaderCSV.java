package nl.eadmin.bankfilereaders;

import java.util.Date;

import nl.eadmin.db.BankAfschriftDataBean;
import nl.eadmin.db.Dagboek;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.StringHelper;
import nl.ibs.esp.uiobjects.UserMessageException;

public abstract class BankfileReaderCSV extends BankfileReader {
	protected char separator;
	protected String[] mask, fieldValues;

	protected BankfileReaderCSV(Dagboek dagboek, String[] record, Date dateFrom, Date dateTo) throws Exception {
		super(dagboek, record, dateFrom, dateTo);
		this.separator = getSeparator();
		this.mask = getRecordMask();
	}

	protected void validateFile() throws Exception {
		String[] vrlp = getVoorloopRecord();
		if (vrlp != null) {
			if (record.length == 0) {
				throw new UserMessageException("Geen voorlooprecord gevonden");
			} else {
				String[] flds = StringHelper.split(record[0], separator, true);
				if (flds.length == vrlp.length) {
					for (int i = 0; i < flds.length; i++) {
						if (!flds[i].trim().equals(vrlp[i].trim())) {
							throw new UserMessageException("Voorlooprecord voldoet niet aan formaat: " + GeneralHelper.toString(vrlp) + " > vrlp/flds: " + vrlp.length + "/" + flds.length);
						}
					}
				} else {
					throw new UserMessageException("Voorlooprecord voldoet niet aan formaat: " + GeneralHelper.toString(vrlp));
				}
			}
		}
	}

	protected BankAfschriftDataBean getNextDataRecord(BankAfschriftDataBean bean) throws Exception {
		if (++recordPointer >= record.length) {
			return null;
		} else {
			fieldValues = StringHelper.split(record[recordPointer], separator, true);
			if (fieldValues.length == mask.length) {
				fillBean(bean, fieldValues);
			} else {
				bean.addRemark("Onjuist aantal rubrieken in record");
			}
			bean.setInputRecord(record[recordPointer]);
			bean.setWriteFlag(true);
			return bean;
		}
	}

	protected abstract char getSeparator() throws Exception;

	protected abstract String[] getVoorloopRecord() throws Exception;

	protected abstract String[] getRecordMask() throws Exception;

	protected abstract void fillBean(BankAfschriftDataBean bean, String[] recordField) throws Exception;
}