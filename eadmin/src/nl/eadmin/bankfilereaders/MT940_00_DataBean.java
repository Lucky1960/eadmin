package nl.eadmin.bankfilereaders;


public class MT940_00_DataBean {
	protected String recordId;
	protected String data;

	public MT940_00_DataBean(String record) {
		if (record == null) {
			this.recordId = "";
			this.data = "";
		} else {
			int start = record.indexOf(":", 1);
			this.recordId = record.substring(1, start);
			this.data = record.substring(start + 1);
		}
	}
}