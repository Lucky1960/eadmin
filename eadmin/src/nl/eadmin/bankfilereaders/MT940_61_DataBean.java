package nl.eadmin.bankfilereaders;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import nl.eadmin.enums.MT940TransactionTypeEnum;
import nl.eadmin.helpers.DateHelper;

public class MT940_61_DataBean extends MT940_00_DataBean {
	private Date valutaDatum, boekDatum;
	private String dc, transactieType, betKenmerk, transactieRef, transactieCode;
	private BigDecimal bedrag;

	public MT940_61_DataBean(String record) throws Exception {
		super(record);
		initialize();
	}

	@SuppressWarnings("deprecation")
	private void initialize() throws Exception {
		if (data == null || data.trim().length() == 0) {
			return;
		}

		int p1, p2;
		//ValutaDatum
		try {
        	valutaDatum = DateHelper.YYYYMMDDToDate("20" + data.substring(0, 6));
		} catch (Exception e1) {
			valutaDatum = null;
		}

		//BoekDatum
		String mmdd = data.substring(6, 10);
		if (mmdd.equals("0000")) {
			boekDatum = valutaDatum;
		} else {
			try {
				int jaar = 1900 + (valutaDatum != null ? valutaDatum.getYear() : new Date().getYear());
				boekDatum = DateHelper.YYYYMMDDToDate("" + jaar + mmdd);
				if (boekDatum.before(valutaDatum)){
					Calendar tmpCal = Calendar.getInstance();
					tmpCal.setTime(boekDatum);
					tmpCal.add(Calendar.YEAR, 1);
					boekDatum = tmpCal.getTime();
				}
			} catch (Exception e1) {
				boekDatum = null;
			}
		}

		//Credit/Debet
		dc = data.substring(10, 11).toUpperCase();
		dc = dc.equals("C") ? "D" : "C";

		//Bedrag
		p1 = 11;
		p2 = data.indexOf("N", p1);
		String bedrag$ = "0.00";
		if (p2 > p1) {
			bedrag$ = data.substring(p1, p2).replaceAll(",", ".");
		}
		bedrag = new BigDecimal(bedrag$);

		//TransactieType
		if (p2 > 0) {
			p1 = p2 + 1;
			p2 = p1 + 3;
			transactieType = data.substring(p1, p2);
		} else {
			transactieType = "";
		}

		//Betalingskenmerk
		if (p2 > 0) {
			p1 = p2;
			p2 = data.indexOf("//", p1);
			if (p2 < 0) {
				betKenmerk = data.substring(p1);
			} else {
				betKenmerk = data.substring(p1, p2);
			}
		} else {
			betKenmerk = "";
		}

		//TransactieReferentie
		p1 = data.indexOf("//", p1);
		if (p1 > 0) {
			p1 = p1 + 2;
			p2 = p1 + 14;
			if (p2 < data.length()) {
				transactieRef = data.substring(p1, p2);
			} else {
				transactieRef = data.substring(p1);
			}
		} else {
			transactieRef = "";
		}

		//TransactieCode/Aanvullende gegevens
		p1 = data.indexOf("/TRCD/", p1);
		if (p1 > 0) {
			transactieCode = data.substring(p1 + 6);
		} else {
			transactieCode = "";
		}
	}

	public Date getValutaDatum() throws Exception {
		return valutaDatum;
	}

	public Date getBoekDatum() throws Exception {
		return boekDatum;
	}

	public String getDC() throws Exception {
		return dc;
	}

	public BigDecimal getBedrag() throws Exception {
		return bedrag;
	}

	public String getTransactieType() throws Exception {
		return transactieType;
	}

	public String getTransactieType$() throws Exception {
		return transactieType == null || transactieType.length() == 0 ? "" : MT940TransactionTypeEnum.getValue(transactieType);
	}

	public String getBetalingsKenmerk() throws Exception {
		return betKenmerk;
	}

	public String getTransactieReferentie() throws Exception {
		return transactieRef;
	}

	public String getTransactieCode() throws Exception {
		return transactieCode;
	}
}