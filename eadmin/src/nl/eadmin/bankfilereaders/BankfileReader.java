package nl.eadmin.bankfilereaders;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.BankAfschrift;
import nl.eadmin.db.BankAfschriftDataBean;
import nl.eadmin.db.BankAfschriftManager;
import nl.eadmin.db.BankAfschriftManagerFactory;
import nl.eadmin.db.BankImport;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DetailDataDataBean;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataDataBean;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.impl.BankAfschriftManager_Impl;
import nl.eadmin.enums.BankImportTypeEnum;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.enums.HeaderDataStatusEnum;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.StringHelper;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public abstract class BankfileReader {
	private Bedrijf bedrijf;
	protected int runNr, recordPointer;
	protected Dagboek dagboek;
	protected String[] record;
	protected Date dateFrom = null, dateTo = null;
	protected BigDecimal ZERO = new BigDecimal(0.00);
	private BankAfschriftManager bankMgr;
	private DBData dbd;

	protected BankfileReader(Dagboek dagboek, String[] record, Date dateFrom, Date dateTo) throws Exception {
		this.dbd = dagboek.getDBData();
		this.bankMgr = BankAfschriftManagerFactory.getInstance(dbd);
		this.dagboek = dagboek;
		this.record = record;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.bedrijf = BedrijfManagerFactory.getInstance(dbd).findByPrimaryKey(dagboek.getBedrijf());
		this.runNr = GeneralHelper.genereerNieuwBankAfschriftRunNummer(bedrijf);
	}

	public static BankfileReader getInstance(Dagboek dagboek, String[] record, Date dateFrom, Date dateTo) throws Exception {
		if (dagboek == null || !dagboek.getSoort().equals(DagboekSoortEnum.BANK)) {
			throw new UserMessageException("Ongeldig bankboek opgegeven!");
		}
		if (dagboek.getBoekstukMask().indexOf("##") < 0) {
			throw new UserMessageException("In het bankboek is nog geen geldig boekstukmasker opgegeven!");
		}
		if (dagboek.getTegenRekening().trim().length() == 0) {
			throw new UserMessageException("In het bankboek is nog geen tegenrekening opgegeven!");
		}
		BankImport bankImport = dagboek.getBankImport();
		if (bankImport.getIbanNr().trim().length() == 0) {
			throw new UserMessageException("In het bankboek is nog geen rekeningnummer opgegeven!");
		}
		switch (bankImport.getFormat()) {
		case BankImportTypeEnum.TYPE_ABN_CSV:
			return new ReaderABN(dagboek, record, dateFrom, dateTo);
		case BankImportTypeEnum.TYPE_ABN_MT940:
			return new ReaderMT940_ABN(dagboek, record, dateFrom, dateTo);
		case BankImportTypeEnum.TYPE_DB_CSV:
			return new ReaderDB(dagboek, record, dateFrom, dateTo);
		case BankImportTypeEnum.TYPE_ING_CSV:
			return new ReaderING(dagboek, record, dateFrom, dateTo);
		case BankImportTypeEnum.TYPE_ING_MT940:
			return new ReaderMT940_ING(dagboek, record, dateFrom, dateTo);
		case BankImportTypeEnum.TYPE_KNAB_CSV:
			return new ReaderKNAB(dagboek, record, dateFrom, dateTo);
		case BankImportTypeEnum.TYPE_RABO_CSV:
			return new ReaderRABO(dagboek, record, dateFrom, dateTo);
		case BankImportTypeEnum.TYPE_TRIODOS_CSV:
			return new ReaderTRIO(dagboek, record, dateFrom, dateTo);
		default:
			throw new UserMessageException("In het bankboek is nog geen of ongeldig importformaat opgegeven!");
		}
	}

	public String importeer() throws Exception {
		validateFile();
		int seqNr = 0;
		String ibanNr = dagboek.getBankImport().getIbanNr();
		BankAfschriftDataBean bean = new BankAfschriftDataBean();
		bean.setBedrijf(dagboek.getBedrijf());
		bean.setRunNr(runNr);
		while (getNextDataRecord(bean) != null) {
			if (bean.getWriteFlag()) {
				String rekening = bean.getRekening();
				Date boekDatum = bean.getBoekdatum();
				if (!ibanNr.equals(rekening)) {
					bean.addRemark("Rekeningnummer '" + rekening + "' wijkt af van bankboek");
				}
				if (boekDatum == null) {
					bean.addRemark("Boekdatum in onjuist formaat");
				} else if (boekDatum.before(dateFrom) || boekDatum.after(dateTo)) {
					bean.addRemark("Boekdatum ligt buiten de opgegeven periode");
				}
				bean.setSeqNr(++seqNr);
				try {
					bankMgr.create(bean);
				} catch (Exception e) {
					throw new UserMessageException("Fout bij schrijven bankafschriftregel: " + bean.getInputRecord() + ApplicationConstants.EOL + " - " + e.getMessage() + ApplicationConstants.EOL);
				}
				bean.reset();
			}
		}
		return transferToDataBase();
	}

	private String transferToDataBase() throws Exception {
		HeaderData headerData;
		HeaderDataManager headerMgr = HeaderDataManagerFactory.getInstance(dbd);
		HeaderDataDataBean headerBean = new HeaderDataDataBean();
		headerBean.setBedrijf(dagboek.getBedrijf());
		headerBean.setDagboekId(dagboek.getId());
		headerBean.setDagboekSoort(dagboek.getSoort());
		headerBean.setDcNummer("");
		headerBean.setFactuurdatum(null);
		headerBean.setFactuurNummer("");
		headerBean.setReferentieNummer("");
		headerBean.setStatus(HeaderDataStatusEnum.BANK_OPEN);
		headerBean.setTotaalBtw(ZERO);
		headerBean.setVervaldatum(null);

		DetailDataManager detailMgr = DetailDataManagerFactory.getInstance(dbd);
		DetailDataDataBean detailBean = new DetailDataDataBean();
		detailBean.setBedrijf(dagboek.getBedrijf());
		detailBean.setDagboekId(dagboek.getId());
		detailBean.setBoekstukRegel(1);
		detailBean.setAantal(new BigDecimal(1.00));
		detailBean.setBedragBtw(ZERO);
		detailBean.setBtwCode("L");
		detailBean.setDatumLevering(null);
		detailBean.setDcNummer("");
		detailBean.setEenheid("");
		detailBean.setFactuurNummer("");
		detailBean.setRekening(dagboek.getTegenRekening());

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM BankAfschrift WHERE ");
		sb.append(BankAfschrift.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		sb.append(BankAfschrift.RUN_NR + "=" + runNr);
		sb.append(" ORDER BY " + BankAfschrift.BOEKDATUM + " ascending, " + BankAfschrift.SEQ_NR);
		FreeQuery qry = QueryFactory.createFreeQuery((BankAfschriftManager_Impl) bankMgr, sb.toString());

		StringBuilder result = new StringBuilder();
		String remark;
		Date boekDatum;
		BigDecimal bedrag;
		BankAfschrift record = null;
		Iterator<?> records = qry.getCollection().iterator();
		while (records.hasNext()) {
			record = (BankAfschrift) records.next();
			result.append(truncate(record.getInputRecord(), 50) + " - ");
			// result.append(getRecordKey(record) + " - ");
			remark = record.getRemark();
			if (remark == null || remark.length() == 0) {
				boekDatum = record.getBoekdatum();
				bedrag = record.getBedrag();
				int[] boekperiode = DateHelper.bepaalBoekperiode(bedrijf, boekDatum);
				String boekstuk = GeneralHelper.genereerNieuwBoekstukNummer(dagboek, boekperiode[0], boekperiode[1]);
				if (boekstuk.length() == 0) {
					throw new UserMessageException("Er kon geen boekstuknummer worden vastgesteld. Mogelijk is er een onjuist masker bij het dagboek opgegeven.");
				}
				String dc = record.getDC();
				headerBean.setBoekdatum(boekDatum);
				headerBean.setBoekjaar(boekperiode[0]);
				headerBean.setBoekperiode(boekperiode[1]);
				headerBean.setBoekstuk(boekstuk);
				headerBean.setDC(dc);
				headerBean.setOmschr1(record.getHdrOmschr1());
				headerBean.setOmschr2(record.getHdrOmschr2());
				headerBean.setTotaalExcl(bedrag);
				headerBean.setTotaalIncl(bedrag);
				headerBean.setTotaalGeboekt(bedrag);
				headerBean.setTotaalDebet(dc.equals("D") ? ZERO : bedrag);
				headerBean.setTotaalCredit(dc.equals("D") ? bedrag : ZERO);
				headerData = headerMgr.create(headerBean);
				dagboek.setLaatsteBoekstuk(headerBean.getBoekstuk());

				dc = dc.equals("D") ? "C" : "D";
				detailBean.setBedragExcl(bedrag);
				detailBean.setBedragIncl(bedrag);
				detailBean.setBedragDebet(dc.equals("D") ? bedrag : ZERO);
				detailBean.setBedragCredit(dc.equals("C") ? bedrag : ZERO);
				detailBean.setBoekstuk(headerBean.getBoekstuk());
				detailBean.setDC(dc);
				detailBean.setOmschr1(record.getDtlOmschr1());
				detailBean.setOmschr2(record.getDtlOmschr2());
				detailBean.setOmschr3(record.getDtlOmschr3());
				detailBean.setPrijs(bedrag);
				detailMgr.create(detailBean);
				headerData.recalculate();
				headerData.setStatus(HeaderDataStatusEnum.BANK_OPEN);
				result.append("Record toegevoegd met boekstuknummer '" + boekstuk + "'");
			} else {
				result.append("Record NIET toegevoegd" + ApplicationConstants.EOL + remark);
			}
			result.append(ApplicationConstants.EOL);
		}
		return result.toString();
	}

	protected String getRecordKey(BankAfschrift record) throws Exception {
		StringBuilder sb = new StringBuilder();
		return sb.toString();
	}

	protected String truncate(String value, int len) throws Exception {
		return StringHelper.truncate(value, len);
	}

	protected abstract void validateFile() throws Exception;

	protected abstract BankAfschriftDataBean getNextDataRecord(BankAfschriftDataBean bean) throws Exception;
}