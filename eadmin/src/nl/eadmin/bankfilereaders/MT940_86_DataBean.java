package nl.eadmin.bankfilereaders;

import java.util.Arrays;
import java.util.HashMap;

import nl.eadmin.enums.MT940TransactionTypeEnum;

public class MT940_86_DataBean extends MT940_00_DataBean {
	private int[] tagDataLen = new int[] { 4, 35, 35, 35, 35, 131, 255, 255, 255, 255, 4, 106, 106 };
	private String[] tag = new String[] { "RTRN", "EREF", "PREF", "MARF", "CSID", "CNTP", "REMI/USTD/", "REMI/STRD/CUR", "REMI/STRD/ISO", "PURP", "ULTC", "ULTD" };
	private HashMap<String, String> tagData = new HashMap<String, String>();

	public MT940_86_DataBean(String record) {
		super(record);
		int ix;
		for (int t = 0; t < tag.length; t++) {
			ix = data.indexOf("/" + tag[t] + "/");
			String ss;
			if (ix < 0) {
				char[] c = new char[tagDataLen[t]];
				Arrays.fill(c, ' ');
				ss = new String(c);
			} else {
				ix = ix + tag[t].length() + 2;
				if (ix + tagDataLen[t] > data.length()) {
					ss = data.substring(ix);
				} else {
					ss = data.substring(ix, ix + tagDataLen[t]);
				}
			}
			tagData.put(tag[t], ss);
		}
	}

	public String getTegenRekening() throws Exception {
		String[] s = tagData.get("CNTP").split("/");
		return s.length >= 1 ? s[0].trim() : "";
	}

	public String getNaam() throws Exception {
		String naam = "";
		String[] s = tagData.get("CNTP").split("/");
		if (s.length >= 3) {
			naam = s[2].trim();
		} else {
			String ss = tagData.get("ULTC");
			if (ss.length() == 0) {
				ss = tagData.get("ULTD");
			}
			if (ss.length() >= 70) {
				naam = ss.substring(0, 69);
			}
		}
		return naam;
	}

	public String getPlaats() throws Exception {
		String[] s = tagData.get("CNTP").split("/");
		return s.length >= 4 ? s[3].trim() : "";
	}

	public String getOmschrijving() throws Exception {
		String omschrijving = "";
		String s = tagData.get("REMI/USTD/").trim();
		if (s.length() > 0) {
			omschrijving = s;
		} else {
			s = tagData.get("REMI/STRD/CUR").trim();
			if (s.length() > 0) {
				omschrijving = "Bet.kenm. " + s;
			} else {
				s = tagData.get("REMI/STRD/ISO").trim();
				if (s.length() > 0) {
					omschrijving = MT940TransactionTypeEnum.getValue(s);
				} else {
					omschrijving = "???";
				}
			}
		}
		return omschrijving;
	}

	public String getMutatieSoort() throws Exception {
		String s = tagData.get("REMI/STRD/ISO").trim();
		if (s.length() > 0) {
			return MT940TransactionTypeEnum.getValue(s);
		} else {
			return "";
		}
	}
}