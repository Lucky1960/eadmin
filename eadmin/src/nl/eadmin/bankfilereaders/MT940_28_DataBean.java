package nl.eadmin.bankfilereaders;


public class MT940_28_DataBean extends MT940_00_DataBean {

	public MT940_28_DataBean(String record) {
		super(record);
	}

	public String getAfschriftNr() throws Exception {
		if (data.length() >= 5)
			return data.substring(0, 5).trim();
		else
			return data;
	}

	public String getBladNr() throws Exception {
		if (data.length() >= 6)
			return data.substring(6).trim();
		else
			return "";
	}
}