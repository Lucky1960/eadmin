package nl.eadmin.bankfilereaders;


public class MT940_25_DataBean extends MT940_00_DataBean {

	public MT940_25_DataBean(String record) {
		super(record);
	}

	public String getRekeningNr() throws Exception {
		if (data.length() >= 18)
			return data.substring(0, 18);
		else
			return data;
	}

	public String getMuntsoort() throws Exception {
		if (data.length() >= 18)
			return data.substring(18).trim();
		else
			return "";
	}
}