package nl.eadmin.bankfilereaders;

import java.util.Date;

import nl.eadmin.helpers.DateHelper;


public class MT940_20_DataBean extends MT940_00_DataBean {

	public MT940_20_DataBean(String record) {
		super(record);
	}

	public Date getBoekDatum() throws Exception {
		Date boekDatum = null;
		try {
			boekDatum = DateHelper.YYYYMMDDToDate("20" + data.substring(4));
		} catch (Exception e1) {
		}
		return boekDatum;
	}
}