package nl.eadmin.bankfilereaders;

import java.util.ArrayList;
import java.util.Date;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.BankAfschriftDataBean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.enums.BankImportTypeEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.StringHelper;
import nl.ibs.esp.uiobjects.UserMessageException;

public abstract class BankfileReaderMT940 extends BankfileReader {
	protected String format, ibanNr;
	protected Bedrijf bedrijf;
	private Date boekdatumAll;
	protected MT940_20_DataBean mt940_20 = new MT940_20_DataBean(null);
	protected MT940_25_DataBean mt940_25 = new MT940_25_DataBean(null);
	protected MT940_28_DataBean mt940_28 = new MT940_28_DataBean(null);
	protected MT940_61_DataBean mt940_61 = new MT940_61_DataBean(null);
	protected MT940_86_DataBean mt940_86 = new MT940_86_DataBean(null);

	protected BankfileReaderMT940(Dagboek dagboek, String[] record, Date dateFrom, Date dateTo, String format) throws Exception {
		super(dagboek, record, dateFrom, dateTo);
		this.bedrijf = BedrijfManagerFactory.getInstance(dagboek.getDBData()).findByPrimaryKey(dagboek.getBedrijf());
		this.record = concatRecords(record);
		this.format = format;
		this.ibanNr = dagboek.getBankImport().getIbanNr();
	}

	private static String[] concatRecords(String[] record) throws Exception {
		StringBuffer tmpRecord = new StringBuffer();
		ArrayList<String> records = new ArrayList<String>();
		for (int r = 0; r < record.length; r++) {
			if (record[r].startsWith(":") && tmpRecord.length() > 0) {
				records.add(tmpRecord.toString());
				tmpRecord = new StringBuffer();
			}
			tmpRecord.append(record[r]);
		}
		if (tmpRecord.length() > 0) {
			records.add(tmpRecord.toString());
		}
		return GeneralHelper.convertArrayListToStringArray(records);
	}

	protected void validateFile() throws Exception {
		boolean valid = false;
		if (record != null && record.length > 0) {
			if (format.equals(BankImportTypeEnum.TYPE_ABN_MT940)) {
				valid = record[0].indexOf("ABNANL2A") >= 0;
			} else if (format.equals(BankImportTypeEnum.TYPE_ING_MT940)) {
				valid = record[0].indexOf("INGBNL2A") >= 0;
			} else if (format.equals(BankImportTypeEnum.TYPE_KNAB_MT940)) {
				valid = record[0].indexOf("KNABNL2H") >= 0;
			} else if (format.equals(BankImportTypeEnum.TYPE_RABO_MT940)) {
				valid = record[0].indexOf("RABONL2U") >= 0;
			}
		}
		if (!valid) {
			throw new UserMessageException("Dit is een leeg of ongeldig MT940-invoerbestand");
		}
	}

	// public String importeer() throws Exception {
	// StringBuilder result = new StringBuilder();
	// String record = null, record86 = null;
	// boolean validRekening = false;
	//
	// String[] ascendingDataRecords = this.record;
	// for (int r = 0; r < ascendingDataRecords.length; r++) {
	// record = ascendingDataRecords[r];
	// if (record.startsWith(":25:")) {
	// mt940_25 = new MT940_25_DataBean(record);
	// validRekening = ibanNr.equals(mt940_25.getRekeningNr());
	// if (!validRekening) {
	// result.append("Rekeningnummer '" + mt940_25.getRekeningNr() +
	// "' wijkt af van bankboek: mutaties overgeslagen" +
	// ApplicationConstants.EOL + ApplicationConstants.EOL);
	// }
	// continue;
	// }
	// if (record.startsWith(":20:")) {
	// mt940_20 = new MT940_20_DataBean(record);
	// continue;
	// }
	// if (record.startsWith(":28C:")) {
	// mt940_28 = new MT940_28_DataBean(record);
	// continue;
	// }
	// if (!validRekening) {
	// continue;
	// }
	// if (record.startsWith(":61:")) {
	// mt940_61 = new MT940_61_DataBean(record);
	// if (r + 1 < ascendingDataRecords.length) {
	// record86 = ascendingDataRecords[r + 1];
	// if (!record86.startsWith(":86:")) {
	// record86 = null;
	// }
	// } else {
	// record86 = null;
	// }
	// mt940_86 = new MT940_86_DataBean(record86);
	// //fillBean();
	// //write();
	// result.append("> " + ascendingDataRecords[r] + ApplicationConstants.EOL +
	// dataBean.getRemark() + ApplicationConstants.EOL);
	// continue;
	// }
	// }
	// return result.toString();
	// }

	protected BankAfschriftDataBean getNextDataRecord(BankAfschriftDataBean bean) throws Exception {
		if (++recordPointer >= this.record.length) {
			return null;
		}
		String recordXX = record[recordPointer];
		String record86 = null;
		if (recordXX.startsWith(":25:")) {
			mt940_25 = new MT940_25_DataBean(recordXX);
			bean.setRekening(mt940_25.getRekeningNr());
			bean.setMuntsoort(mt940_25.getMuntsoort());
			bean.setWriteFlag(false);
		} else if (recordXX.startsWith(":20:")) {
			mt940_20 = new MT940_20_DataBean(recordXX);
			boekdatumAll = mt940_20.getBoekDatum();
			if (boekdatumAll != null) {
				bean.setBoekdatum(boekdatumAll);
			}
			bean.setWriteFlag(false);
		} else if (recordXX.startsWith(":28C:")) {
			mt940_28 = new MT940_28_DataBean(recordXX);
			bean.setAfschriftNr(mt940_28.getAfschriftNr());
			bean.setBladNr(mt940_28.getBladNr());
			bean.setWriteFlag(false);
		} else if (recordXX.startsWith(":61:")) {
			mt940_61 = new MT940_61_DataBean(recordXX);
			if (recordPointer + 1 < record.length) {
				record86 = record[recordPointer + 1];
				if (!record86.startsWith(":86:")) {
					record86 = null;
				} else {
					recordPointer++;
				}
			}
			mt940_86 = new MT940_86_DataBean(record86);
			StringBuilder sb = new StringBuilder();
			sb.append(mt940_86.getNaam() + " ");
			sb.append(mt940_86.getPlaats() + " ");
			sb.append(mt940_86.getOmschrijving() + " ");
			sb.append(mt940_86.getTegenRekening() + " ");
			sb.append(mt940_86.getMutatieSoort() + " ");
			sb.append(mt940_61.getBetalingsKenmerk() + " ");
			sb.append(mt940_61.getTransactieType$() + " ");
			String s = StringHelper.suppressDoubleSpaces(sb.toString());

			bean.setBedrag(mt940_61.getBedrag());
			bean.setBoekdatum(mt940_61.getBoekDatum() != null ? mt940_61.getBoekDatum() : boekdatumAll);
			bean.setDC(mt940_61.getDC());
			bean.setHeaderOmschrijving(s);
			bean.setDetailOmschrijving(s);
			bean.setNaam(mt940_86.getNaam());
			bean.setTegenrekening(mt940_86.getTegenRekening());
			bean.setInputRecord(recordXX + ApplicationConstants.EOL + record86);
			bean.setWriteFlag(true);
		} else {
			bean.setWriteFlag(false);
		}
		return bean;
	}
}