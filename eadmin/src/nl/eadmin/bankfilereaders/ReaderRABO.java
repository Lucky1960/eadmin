package nl.eadmin.bankfilereaders;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.db.BankAfschriftDataBean;
import nl.eadmin.db.Dagboek;
import nl.eadmin.helpers.DateHelper;

public class ReaderRABO extends BankfileReaderCSV {

	protected ReaderRABO(Dagboek dagboek, String[] record, Date dateFrom, Date dateTo) throws Exception {
		super(dagboek, record, dateFrom, dateTo);
	}

	protected String[] getVoorloopRecord() throws Exception {
		return null;
	}

	protected String[] getRecordMask() throws Exception {
		return new String[] { "A", "A", "YMD", "A", "B", "A", "A", "YMD", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A" };
	}

	protected char getSeparator() throws Exception {
		return ',';
	}

	protected void fillBean(BankAfschriftDataBean dataBean, String[] fldValue) throws Exception {
		String f_datum = fldValue[7];
		String f_naam = fldValue[6];
		String f_rekening = fldValue[0];
		String f_tegenrekening = fldValue[5];
		String f_afbij = fldValue[3];
		String f_bedrag = fldValue[4];
		String f_mutatiesoort = fldValue[10].trim() + fldValue[11].trim();
		String f_mededeling = f_mutatiesoort + fldValue[12].trim();

		Date boekDatum; 
        try {
			boekDatum = DateHelper.YYYYMMDDToDate(f_datum);
		} catch (Exception e1) {
			boekDatum = null;
			dataBean.addRemark("Ongeldige boekdatum: " + f_datum + " (moet zijn in formaat YYYYMMDD");
		}

        BigDecimal boekingsbedrag;
        try {
			boekingsbedrag = new BigDecimal(f_bedrag).setScale(2, BigDecimal.ROUND_HALF_UP);
		} catch (NumberFormatException e) {
			boekingsbedrag = ZERO;
			dataBean.addRemark("Ongeldig bedrag: " + f_bedrag + " (moet zijn in formaat x.xx");
		}

        if ("DC".indexOf(f_afbij) < 0) {
			dataBean.addRemark("Ongeldig waarde voor D/C: " + f_afbij);
        }

        dataBean.setBoekdatum(boekDatum);
        dataBean.setDC(f_afbij.equals("C") ? "D" : "C");
        dataBean.setBedrag(boekingsbedrag);
        dataBean.setRekening(f_rekening);
        dataBean.setTegenrekening(f_tegenrekening);
        dataBean.setHdrOmschr1(truncate(f_naam, 50));
        dataBean.setHdrOmschr2(truncate(f_mutatiesoort, 50));
        dataBean.setDetailOmschrijving(truncate(f_mededeling, 150));
	}
}