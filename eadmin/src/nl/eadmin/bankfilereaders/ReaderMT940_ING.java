package nl.eadmin.bankfilereaders;

import java.util.Date;

import nl.eadmin.db.Dagboek;
import nl.eadmin.enums.BankImportTypeEnum;

public class ReaderMT940_ING extends BankfileReaderMT940 {

	protected ReaderMT940_ING(Dagboek dagboek, String[] record, Date dateFrom, Date dateTo) throws Exception {
		super(dagboek, record, dateFrom, dateTo, BankImportTypeEnum.TYPE_ING_MT940);
	}

//	protected void fillBean() throws Exception {
//		dataBean.reset();
//		String f_naam = mt940_86.getNaam();
//		String f_tegenrekening = mt940_86.getTegenRekening();
//		String f_afbij = mt940_61.getDC();
//		String f_mutatiesoort = mt940_86.getMutatieSoort();
//		String f_mededeling = mt940_86.getOmschrijving();
//
//		Date boekDatum = mt940_61.getBoekDatum();
//		if (boekDatum == null) {
//			boekDatum = mt940_20.getBoekDatum();
//		}
//		if (boekDatum == null) {
//			dataBean.addRemark("Ongeldige boekdatum: '" + boekDatum + "' (moet zijn in formaat YYMMDD", true);
//		}
//
//		BigDecimal boekingsbedrag = mt940_61.getBedrag();
//		if (boekingsbedrag.doubleValue() == 0d ) {
//			dataBean.addRemark("Ongeldig bedrag: " + boekingsbedrag + " (moet zijn in formaat x.xx", true);
//		}
//
//		if ("DC".indexOf(f_afbij) < 0) {
//			dataBean.addRemark("Ongeldig waarde voor Af/Bij: " + f_afbij, true);
//		}
//
//		dataBean.setBoekdatum(boekDatum);
//		dataBean.setDC(f_afbij);
//		dataBean.setBedrag(boekingsbedrag);
//		dataBean.setRekening(ibanNr);
//		dataBean.setTegenRekening(f_tegenrekening);
//		dataBean.setHeaderOmschrijving1(truncate(f_naam, 50));
//		dataBean.setHeaderOmschrijving2(truncate(f_mutatiesoort, 50));
//		dataBean.setDetailOmschrijving(truncate(f_mededeling, 150));
//	}
}