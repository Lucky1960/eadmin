package nl.eadmin.bankfilereaders;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.db.BankAfschriftDataBean;
import nl.eadmin.db.Dagboek;
import nl.eadmin.helpers.DateHelper;

public class ReaderABN extends BankfileReaderCSV {

	protected ReaderABN(Dagboek dagboek, String[] record, Date dateFrom, Date dateTo) throws Exception {
		super(dagboek, record, dateFrom, dateTo);
	}

	protected String[] getVoorloopRecord() throws Exception {
		return new String[] { "Rekeningnummer", "Muntsoort", "Transactiedatum", "Rentedatum", "Beginsaldo", "Eindsaldo", "Transactiebedrag", "Omschrijving" };
	}

	protected String[] getRecordMask() throws Exception {
		return new String[] { "A", "A", "YMD", "YMD", "D", "D", "D", "A" };
	}

	protected char getSeparator() throws Exception {
		return ';';
	}

	@SuppressWarnings("unused")
	protected void fillBean(BankAfschriftDataBean dataBean, String[] fldValue) throws Exception {
		String f_rekening = fldValue[0];
		String f_muntsoort = fldValue[1];
		String f_transdatum = fldValue[2];
		String f_datum = fldValue[3];
		String f_saldoBegin = fldValue[4];
		String f_saldoEind = fldValue[5];
		String f_bedrag = fldValue[6];
		String f_omschrijving = fldValue[7];

		if (!"EUR".equals(f_muntsoort)) {
			dataBean.addRemark("Ongeldige muntsoort: " + f_muntsoort);
		}

		Date boekDatum; 
        try {
			boekDatum = DateHelper.YYYYMMDDToDate(f_datum);
		} catch (Exception e1) {
			boekDatum = null;
			dataBean.addRemark("Ongeldige boekdatum: " + f_datum + " (moet zijn in formaat YYYYMMDD");
		}

        BigDecimal saldo1;
        try {
        	saldo1 = new BigDecimal(f_saldoBegin.replaceAll(",", ".")).setScale(2, BigDecimal.ROUND_HALF_UP);
		} catch (NumberFormatException e) {
			saldo1 = ZERO;
			dataBean.addRemark("Ongeldig beginsaldo: " + f_saldoBegin + " (moet zijn in formaat x.xx of x,xx");
		}

        BigDecimal saldo2;
        try {
        	saldo2 = new BigDecimal(f_saldoEind.replaceAll(",", ".")).setScale(2, BigDecimal.ROUND_HALF_UP);
		} catch (NumberFormatException e) {
			saldo2 = ZERO;
			dataBean.addRemark("Ongeldig eindsaldo: " + f_saldoEind + " (moet zijn in formaat x.xx of x,xx");
		}

        BigDecimal boekingsbedrag;
        try {
			boekingsbedrag = new BigDecimal(f_bedrag.replaceAll(",", ".")).setScale(2, BigDecimal.ROUND_HALF_UP).abs();
		} catch (NumberFormatException e) {
			boekingsbedrag = ZERO;
			dataBean.addRemark("Ongeldig bedrag: " + f_bedrag + " (moet zijn in formaat x.xx of x,xx");
		}

        while (f_omschrijving.indexOf("  ") > 0) {
            f_omschrijving = f_omschrijving.replaceFirst("  ", " ");
        }
        String[] s = f_omschrijving.split("/");
        dataBean.setBoekdatum(boekDatum);
        dataBean.setDC(saldo1.doubleValue() < saldo2.doubleValue() ? "D" : "C");
        dataBean.setBedrag(boekingsbedrag);
        dataBean.setRekening(f_rekening);
        dataBean.setTegenrekening(s.length > 4 ? s[4] : "");
        dataBean.setHdrOmschr1(s.length > 8 ? truncate(s[8], 50) : "");
        dataBean.setHdrOmschr2(s.length > 10 ? truncate(s[10], 50) : "" );
        dataBean.setDetailOmschrijving(truncate(f_omschrijving, 150));
	}
}