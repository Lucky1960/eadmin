package nl.eadmin.bankfilereaders;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.db.BankAfschriftDataBean;
import nl.eadmin.db.Dagboek;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.helpers.StringHelper;
import nl.ibs.esp.uiobjects.UserMessageException;

public class ReaderDB extends BankfileReaderCSV {

	protected ReaderDB(Dagboek dagboek, String[] record, Date dateFrom, Date dateTo) throws Exception {
		super(dagboek, record, dateFrom, dateTo);
		String[] result = new String[record.length - 1];
		System.arraycopy(record, 0, result, 0, result.length);
		this.record = result;
	}

	protected String[] getVoorloopRecord() throws Exception {
		return record == null || record.length == 0 ? null : StringHelper.split(record[0], separator, true);
	}

	protected String[] getRecordMask() throws Exception {
		return new String[49];
	}

	protected char getSeparator() throws Exception {
		return ';';
	}

	protected void validateFile() throws Exception {
		String[] vrlp = getVoorloopRecord();
		String ibanNr = dagboek.getBankImport().getIbanNr();
		if (vrlp == null || ibanNr.indexOf(vrlp[1]) < 0 || record[0].indexOf(";OPENING BALANCE;") < 0) {
			throw new UserMessageException("Geen geldig voorlooprecord gevonden");
		}
	}

	protected void fillBean(BankAfschriftDataBean dataBean, String[] fldValue) throws Exception {
		String tmpStr = fldValue[30] + fldValue[31] + fldValue[32] + fldValue[33] + fldValue[34]; 
		String f_datum = fldValue[11];
		String f_naam = fldValue[42];
		String f_rekening = fldValue[2];
		String f_tegenrekening = "";
		String f_afbij = fldValue[10];
		String f_bedrag = fldValue[18];
		String f_mutatiesoort = fldValue[30];
		String f_mededeling = "";
		int ix, p1, p2;

		Date boekDatum; 
        try {
			boekDatum = DateHelper.DDMMYYYYToDate(f_datum.replaceAll("\\.", ""));
		} catch (Exception e1) {
			boekDatum = null;
			dataBean.addRemark("Ongeldige boekdatum: " + f_datum + " (moet zijn in formaat DDMMYYYY");
		}

        BigDecimal boekingsbedrag;
        try {
			boekingsbedrag = new BigDecimal(f_bedrag.replaceAll(",", ".")).setScale(2, BigDecimal.ROUND_HALF_UP).abs();
		} catch (NumberFormatException e) {
			boekingsbedrag = ZERO;
			dataBean.addRemark("Ongeldig bedrag: " + f_bedrag + " (moet zijn in formaat x.xx");
		}

        if ("DC".indexOf(f_afbij) < 0) {
			dataBean.addRemark("Ongeldig waarde voor Debet/Credit: " + f_afbij);
        } else {
        	f_afbij = f_afbij.startsWith("C") ? "D" : "C";
        }

        ix = f_mutatiesoort.indexOf("/");
        if (ix >= 0) {
        	f_mutatiesoort = f_mutatiesoort.substring(0, ix).trim(); 
        }

        if (f_naam.trim().length() == 0) {
            ix = tmpStr.indexOf("//ORDP/");
            if (ix >= 0) {
            	p1 = ix + 7;
            	p2 = tmpStr.indexOf("//", p1);
            	p2 = p2 >= 0 ? p2 : tmpStr.length();
               	f_naam = tmpStr.substring(p1, p2); 
            } else {
            	f_naam = fldValue[14];
            }
        }

        ix = tmpStr.indexOf("//ACCW/");
        if (ix >= 0 && tmpStr.length() >= ix + 17) {
        	p1 = ix + 7;
        	p2 = tmpStr.indexOf(",", p1);
        	p2 = p2 >= 0 ? p2 : tmpStr.length();
           	f_tegenrekening = tmpStr.substring(p1, p2); 
        }

        ix = tmpStr.indexOf("//REMI/");
        if (ix >= 0) {
        	p1 = ix + 7;
        	p2 = tmpStr.indexOf("//", p1);
        	p2 = p2 >= 0 ? p2 : tmpStr.length();
       		f_mededeling = tmpStr.substring(p1, p2).trim(); 
        }

        dataBean.setBoekdatum(boekDatum);
        dataBean.setDC(f_afbij);
        dataBean.setBedrag(boekingsbedrag);
        dataBean.setRekening(f_rekening);
        dataBean.setTegenrekening(f_tegenrekening);
        dataBean.setHdrOmschr1(truncate(f_naam, 50));
        dataBean.setHdrOmschr2(truncate(f_mutatiesoort, 50));
        dataBean.setDetailOmschrijving(truncate(f_mededeling, 150));
	}
}