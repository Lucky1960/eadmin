package nl.eadmin.selection;

public interface SelectionChangeListener {

	public void updateTableAfterSelectionChange()throws Exception;

}