package nl.eadmin.selection;

import java.util.Date;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.helpers.DateHelper;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.HeaderPanel;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UserMessageException;

public class DebCredLijstSelection extends Selection {
	public static final int P_DATE_FROM = 2;
	public static final int P_DATE_TO = 3;
	public static final int P_INCL_ZERO = 4;

	private DateSelectionField fldDateFrom, fldDateTo;
	private Label lblDateFromTo, lblInclZero;
	private CheckBox fldInclZero;
	private Panel optionsPanel;

	public DebCredLijstSelection(Bedrijf bedrijf) throws Exception {
		super(bedrijf);
		initialize();
	}

	private void initialize() throws Exception {
		lblDateFromTo = new Label("Datum van - t/m");
		fldDateFrom = new DateSelectionField("", (Date) null, "dd-MM-yyyy");
		fldDateFrom.setDiscardLabel(true);
		fldDateTo = new DateSelectionField("", new Date(), "dd-MM-yyyy");
		fldDateTo.setDiscardLabel(true);
		lblInclZero = new Label("Toon inclusief nul-saldi");
		fldInclZero = new CheckBox("", false);
		fldInclZero.setDiscardLabel(true);
	}

	public Date getDateFrom() throws Exception {
		return fldDateFrom.getValueAsDate();
	}

	public Date getDateTo() throws Exception {
		return fldDateTo.getValueAsDate();
	}

	public boolean getIncludeZero() throws Exception {
		return fldInclZero.getValueAsBoolean();
	}

	public Panel getPanel() throws Exception {
		if (optionsPanel == null) {
			ESPGridLayout grid = new ESPGridLayout();
			grid.setColumnWidths(new short[] { 120, 100, 150, 20, 150 });
			grid.add(lblDateFromTo, 0, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
			grid.add(fldDateFrom, 0, 1, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
			grid.add(fldDateTo, 0, 2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
			grid.add(fldInclZero, 0, 3, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
			grid.add(lblInclZero, 0, 4, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
			grid.add(apply, 0, 5, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
			optionsPanel = new HeaderPanel("");
			optionsPanel.addUIObject(grid);
		}
		return optionsPanel;
	}

	public void load(String values) throws Exception {
		if (values != null && values.trim().length() > 0) {
			String[] parameters = values.split(SEPARATOR_PARM);
			String[] parameter = new String[2];
			int key;
			String val;
			for (int i = 0; i < parameters.length; i++) {
				parameter = parameters[i].split("=");
				key = Integer.parseInt(parameter[0]);
				val = "";
				if (parameter.length == 2) {
					val = parameter[1];
				}
				switch (key) {
				case P_DATE_FROM:
					fldDateFrom.setValue(DateHelper.DDMMYYYYToDate(val));
					break;
				case P_DATE_TO:
					fldDateTo.setValue(DateHelper.DDMMYYYYToDate(val));
					break;
				case P_INCL_ZERO:
					fldInclZero.setValue(new Boolean(val));
					break;
				}
			}
		}
	}

	public String getSelectionAsString() throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append(P_BEDRIJF).append("=").append(bedrijf.getBedrijfscode()).append(SEPARATOR_PARM);
		sb.append(P_DATE_FROM).append("=").append(DateHelper.dateToDDMMYYYY(fldDateFrom.getValueAsDate(), "")).append(SEPARATOR_PARM);
		sb.append(P_DATE_TO).append("=").append(DateHelper.dateToDDMMYYYY(fldDateTo.getValueAsDate(), "")).append(SEPARATOR_PARM);
		sb.append(P_INCL_ZERO).append("=").append(fldInclZero.getValueAsBoolean()).append(SEPARATOR_PARM);
		return sb.toString();
	}

	public void validatePanel() throws Exception {
		Date date1 = getDateFrom();
		Date date2 = getDateTo();
		if (date1 != null && date2 != null && date1.after(date2)) {
			fldDateFrom.setInvalidTag();
			fldDateTo.setInvalidTag();
			throw new UserMessageException("Deze selectie is niet mogelijk");
		}
	}
}
