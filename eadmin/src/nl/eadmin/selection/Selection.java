package nl.eadmin.selection;

import java.util.ArrayList;
import java.util.Iterator;

import nl.eadmin.db.Bedrijf;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.Panel;

public abstract class Selection {
	public static final int P_DBDATA = 0;
	public static final int P_BEDRIJF = 1;
	public static final String SEPARATOR_PARM = ";";
	protected Bedrijf bedrijf;
	protected Action apply;
	protected ArrayList<SelectionChangeListener> myListeners;

	public Selection(Bedrijf bedrijf) throws Exception {
		this.bedrijf = bedrijf;
		this.myListeners = new ArrayList<SelectionChangeListener>();
		this.apply = new Action("Toepassen"){
			private static final long serialVersionUID = 1L;
			public boolean execute(DataObject object) throws Exception {
				validatePanel();
				notifyMyListeners();
				return true;
			}};
	}

	public void addSelectionChangeListener(SelectionChangeListener listener) throws Exception {
		myListeners.add(listener);
	}

	private void notifyMyListeners() throws Exception {
		Iterator<SelectionChangeListener> iter = myListeners.iterator();
		while (iter.hasNext()) {
			iter.next().updateTableAfterSelectionChange();
		}
	}

	public abstract Panel getPanel() throws Exception;
	public abstract String getSelectionAsString() throws Exception;
	public abstract void validatePanel() throws Exception;
	public abstract void load(String values) throws Exception;
}