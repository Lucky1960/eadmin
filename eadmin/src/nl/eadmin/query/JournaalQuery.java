package nl.eadmin.query;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManager;
import nl.eadmin.db.JournaalManagerFactory;
import nl.ibs.esp.uiobjects.ODBQuery;
import nl.ibs.esp.util.ODBCalculator;
import nl.ibs.esp.util.ODBCalculatorImpl;
import nl.ibs.esp.util.ODBCounter;
import nl.ibs.esp.util.ODBCounterImpl;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.QueryFactory;

public class JournaalQuery implements ODBQuery, ODBCounter, ODBCalculator {
	private static final long serialVersionUID = -7489982161829511842L;
	private JournaalManager manager;
	private ODBCounter counter;
	private ODBCalculator calculator;
	private ExecutableQuery qry;
	private String sql = null;
	private String orderBy = null;

	public JournaalQuery(Bedrijf bedrijf, int boekjaar) throws Exception {
		StringBuilder filter = new StringBuilder();
		filter.append(Journaal.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		filter.append(Journaal.BOEKJAAR + "=" + boekjaar);
		this.manager = JournaalManagerFactory.getInstance(bedrijf.getDBData());
		this.orderBy = Journaal.DAGBOEK_ID + ", " + Journaal.BOEKSTUK + ", " + Journaal.DC + " descending ";
		this.sql = filter.toString();
		this.qry = QueryFactory.create(Journaal.class, sql, orderBy);
		this.qry.setCacheable(true);
		this.counter = new ODBCounterImpl(manager);
	}

	@SuppressWarnings("rawtypes")
	public Collection fetchData(String filter, String order, Map parameters, int maxObjects) throws Exception {
		if (filter != null) {
			qry.setFilter(sql + " AND " + filter);
		} else {
			qry.setFilter(sql);
		}
//		if (order == null || order.length() == 0) {
			qry.setOrdering(orderBy);
//		} else {
//			qry.setOrdering(order);
//		}
		qry.setMaxObjects(maxObjects);
		return manager.getCollection(qry);
	}

	@SuppressWarnings("rawtypes")
	public long count(String filter, Map parameters) throws Exception {
		if (filter != null && filter.trim().length() > 0) {
			return counter.count(sql + " AND " + filter, parameters);
		} else {
			return counter.count(sql, parameters);
		}
	}

	@SuppressWarnings("rawtypes")
	public Map<?, ?> calculate(String filter, Map parameters) throws Exception {
		if (calculator == null) {
			return new HashMap<String, Object>(0);
		} else if (filter != null && filter.trim().length() > 0) {
			return calculator.calculate(sql + " AND " + filter, parameters);
		} else {
			return calculator.calculate(sql, parameters);
		}
	}

	public void setColumnsToCalculate(HashMap<String, String> columnsToCalculate) {
		calculator = new ODBCalculatorImpl(manager, columnsToCalculate);
	}
}