package nl.eadmin.query;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.db.Dagboek;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManager;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.helpers.DateHelper;
import nl.ibs.esp.uiobjects.ODBQuery;
import nl.ibs.esp.util.ODBCalculator;
import nl.ibs.esp.util.ODBCalculatorImpl;
import nl.ibs.esp.util.ODBCounter;
import nl.ibs.esp.util.ODBCounterImpl;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.QueryFactory;

public class JournaalQuery2 implements ODBQuery, ODBCounter, ODBCalculator {
	private static final long serialVersionUID = -7489982161829511842L;
	private JournaalManager manager;
	private ODBCounter counter;
	private ODBCalculator calculator;
	private ExecutableQuery qry;
	private String sql = null;

	public JournaalQuery2(Dagboek dagboek, Date dateFrom, Date dateTo) throws Exception {
		this.manager = JournaalManagerFactory.getInstance(dagboek.getDBData());
		StringBuilder filter = new StringBuilder();
		filter.append(Journaal.BEDRIJF + "='" + dagboek.getBedrijf() + "' AND ");
		filter.append(Journaal.DAGBOEK_ID + "='" + dagboek.getId() + "' AND ");
		filter.append(Journaal.BOEKDATUM + ">='" + DateHelper.getSQLdateString(dateFrom) + "' AND ");
		filter.append(Journaal.BOEKDATUM + "<='" + DateHelper.getSQLdateString(dateTo) + "' ");
		sql = filter.toString();
		qry = QueryFactory.create(Journaal.class, null, Journaal.BOEKSTUK + " ascending, " + Journaal.DC + " descending ");
		qry.setCacheable(true);
		counter = new ODBCounterImpl(manager);
	}

	@SuppressWarnings("rawtypes")
	public Collection fetchData(String filter, String order, Map parameters, int maxObjects) throws Exception {
		if (filter != null) {
			qry.setFilter(sql + " AND " + filter);
		} else {
			qry.setFilter(sql);
		}
		if (order == null || order.length() == 0) {
			qry.setOrdering(Journaal.BOEKSTUK + " ascending, " + Journaal.DC + " descending ");
		} else {
			qry.setOrdering(order);
		}
		qry.setMaxObjects(maxObjects);
		return manager.getCollection(qry);
	}

	@SuppressWarnings("rawtypes")
	public long count(String filter, Map parameters) throws Exception {
		if (filter != null && filter.trim().length() > 0) {
			return counter.count(sql + " AND " + filter, parameters);
		} else {
			return counter.count(sql, parameters);
		}
	}

	@SuppressWarnings("rawtypes")
	public Map<?, ?> calculate(String filter, Map parameters) throws Exception {
		if (calculator == null) {
			return new HashMap<String, Object>(0);
		} else if (filter != null && filter.trim().length() > 0) {
			return calculator.calculate(sql + " AND " + filter, parameters);
		} else {
			return calculator.calculate(sql, parameters);
		}
	}

	public void setColumnsToCalculate(HashMap<String, String> columnsToCalculate) {
		calculator = new ODBCalculatorImpl(manager, columnsToCalculate);
	}
}