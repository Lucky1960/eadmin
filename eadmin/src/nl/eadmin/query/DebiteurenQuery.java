package nl.eadmin.query;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.ibs.esp.uiobjects.ODBQuery;
import nl.ibs.esp.util.ODBCalculator;
import nl.ibs.esp.util.ODBCalculatorImpl;
import nl.ibs.esp.util.ODBCounter;
import nl.ibs.esp.util.ODBCounterImpl;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.QueryFactory;

public class DebiteurenQuery implements ODBQuery, ODBCounter, ODBCalculator {
	private static final long serialVersionUID = -7477736748934095487L;
	private HeaderDataManager manager;
	private ODBCounter counter;
	private ODBCalculator calculator;
	private ExecutableQuery qry;
	private String sql = null;

	public DebiteurenQuery(Bedrijf bedrijf) throws Exception {
		this.manager = HeaderDataManagerFactory.getInstance(bedrijf.getDBData());
		qry = QueryFactory.create(HeaderData.class, null, HeaderData.FACTUUR_NUMMER);
		qry.setCacheable(true);
		counter = new ODBCounterImpl(manager);
		sql = HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + HeaderData.DAGBOEK_SOORT + "='" + DagboekSoortEnum.VERKOOP + "'";
	}

	@SuppressWarnings("rawtypes")
	public Collection fetchData(String filter, String order, Map parameters, int maxObjects) throws Exception {
		if (filter != null) {
			qry.setFilter(sql + " AND " + filter);
		} else {
			qry.setFilter(sql);
		}
		if (order == null || order.length() == 0) {
			qry.setOrdering(HeaderData.FACTUUR_NUMMER + " ascending ");
		} else {
			qry.setOrdering(order);
		}
		qry.setMaxObjects(maxObjects);
		return manager.getCollection(qry);
	}

	@SuppressWarnings("rawtypes")
	public long count(String filter, Map parameters) throws Exception {
		if (filter != null && filter.trim().length() > 0) {
			return counter.count(sql + " AND " + filter, parameters);
		} else {
			return counter.count(sql, parameters);
		}
	}

	@SuppressWarnings("rawtypes")
	public Map<?, ?> calculate(String filter, Map parameters) throws Exception {
		if (calculator == null) {
			return new HashMap<String, Object>(0);
		} else if (filter != null && filter.trim().length() > 0) {
			return calculator.calculate(sql + " AND " + filter, parameters);
		} else {
			return calculator.calculate(sql, parameters);
		}
	}

	public void setColumnsToCalculate(HashMap<String, String> columnsToCalculate) {
		calculator = new ODBCalculatorImpl(manager, columnsToCalculate);
	}
}