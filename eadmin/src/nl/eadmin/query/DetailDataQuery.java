package nl.eadmin.query;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.ibs.esp.uiobjects.ODBQuery;
import nl.ibs.esp.util.ODBCalculator;
import nl.ibs.esp.util.ODBCalculatorImpl;
import nl.ibs.esp.util.ODBCounter;
import nl.ibs.esp.util.ODBCounterImpl;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.QueryFactory;

public class DetailDataQuery implements ODBQuery, ODBCounter, ODBCalculator {
	private static final long serialVersionUID = -7477736748934095487L;
	private HeaderData headerData;
	private DetailDataManager manager;
	private ODBCounter counter;
	private ODBCalculator calculator;
	private ExecutableQuery qry;
	private String sql = null;

	public DetailDataQuery(Dagboek dagboek, HeaderData headerData) throws Exception {
		this.manager = DetailDataManagerFactory.getInstance(dagboek.getDBData());
		this.headerData = headerData;
		qry = QueryFactory.create(DetailData.class, null, DetailData.BOEKSTUK + " ascending, " + DetailData.BOEKSTUK_REGEL + " ascending ");
		qry.setCacheable(true);
		counter = new ODBCounterImpl(manager);
		sql = DetailData.BEDRIJF + "='" + dagboek.getBedrijf() + "' AND " + DetailData.DAGBOEK_ID + "='" + dagboek.getId() + "'";
		if (headerData == null) {
			sql = sql + " AND " + DetailData.BOEKSTUK + "=''";
		} else {
			sql = sql + " AND " + DetailData.BOEKSTUK + "='" + headerData.getBoekstuk() + "'";
		}
	}

	@SuppressWarnings("rawtypes")
	public Collection fetchData(String filter, String order, Map parameters, int maxObjects) throws Exception {
		if (filter != null) {
			qry.setFilter(sql + " AND " + filter);
		} else {
			qry.setFilter(sql);
		}
		if (order == null || order.length() == 0) {
			if (headerData == null) {
				qry.setOrdering(DetailData.DC_NUMMER + " ascending, " + DetailData.BOEKSTUK_REGEL + " ascending ");
			} else {
				qry.setOrdering(DetailData.BOEKSTUK + " ascending, " + DetailData.BOEKSTUK_REGEL + " ascending ");
			}
		} else {
			qry.setOrdering(order);
		}
		qry.setMaxObjects(maxObjects);
		return manager.getCollection(qry);
	}

	@SuppressWarnings("rawtypes")
	public long count(String filter, Map parameters) throws Exception {
		if (filter != null && filter.trim().length() > 0) {
			return counter.count(sql + " AND " + filter, parameters);
		} else {
			return counter.count(sql, parameters);
		}
	}

	@SuppressWarnings("rawtypes")
	public Map<?, ?> calculate(String filter, Map parameters) throws Exception {
		if (calculator == null) {
			return new HashMap<String, Object>(0);
		} else if (filter != null && filter.trim().length() > 0) {
			return calculator.calculate(sql + " AND " + filter, parameters);
		} else {
			return calculator.calculate(sql, parameters);
		}
	}

	public void setColumnsToCalculate(HashMap<String, String> columnsToCalculate) {
		calculator = new ODBCalculatorImpl(manager, columnsToCalculate);
	}
}