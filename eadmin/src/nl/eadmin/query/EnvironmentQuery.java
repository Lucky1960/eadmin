package nl.eadmin.query;

import java.util.Collection;
import java.util.Map;

import nl.eadmin.db.Environment;
import nl.eadmin.db.EnvironmentManager;
import nl.eadmin.db.EnvironmentManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.ibs.esp.uiobjects.ODBQuery;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.QueryFactory;

public class EnvironmentQuery implements ODBQuery {

	private static final long serialVersionUID = -6271117058326762664L;
	private ExecutableQuery qry;
	private EnvironmentManager manager = EnvironmentManagerFactory.getInstance();

	public EnvironmentQuery(Gebruiker user) throws Exception {
		qry = QueryFactory.create(Environment.class, null, Environment.DTA_LIB);
		qry.setCacheable(true);
	}

	@SuppressWarnings({ "rawtypes" })
	public Collection fetchData(String filter, String order, Map parameters, int maxObjects) throws Exception {
		if (filter != null) {
			qry.setFilter(filter);
		}
		qry.setMaxObjects(maxObjects);
		qry.setOrdering(order);
		return manager.getCollection(qry);
	}
}