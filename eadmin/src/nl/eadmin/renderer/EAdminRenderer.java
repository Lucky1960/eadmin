package nl.eadmin.renderer;

import nl.ibs.esp.renderer.AjaxRenderer;
import nl.ibs.esp.servlet.Configuration;
import nl.ibs.esp.uiobjects.Screen;
import nl.ibs.esp.uiobjects.UIObject;

public class EAdminRenderer extends AjaxRenderer {
	private static final long serialVersionUID = -514246326336397166L;

	public EAdminRenderer(Configuration config, Screen screen) {
		super(config, screen);
		// TODO Auto-generated constructor stub
	}
	
	protected void rendererExtensionPoint(UIObject parent, UIObject object, Buffer buffer) {
		if (EAdminRendererExtension.getInstance().render(parent,object,buffer,this)==false)
			super.rendererExtensionPoint(parent, object, buffer);
	};

}
