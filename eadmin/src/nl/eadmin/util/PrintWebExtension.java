package nl.eadmin.util;

import nl.eadmin.helpers.StringHelper;
import nl.ibs.esp.renderer.Renderer.Buffer;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.WebExtension;

public class PrintWebExtension extends WebExtension {
	private static final long serialVersionUID = -8053147855399923216L;
	//private static final String cleanUp="/*if (window.printWindow){*/ try{ window.printWindow.close(); delete window.printWindow; } catch (e){window[\"printWindow\"] = undefined;}/*}*/";
	String title;
	String content;
	UIObject parent;
	

	public PrintWebExtension(UIObject parent, String name, String html) {
		if (name.length() <= 16)
			this.title=name;
		else
			this.title=StringHelper.replace(StringHelper.replace(name.substring(15).replace("\"", "\\\""), "\r\n", " "), "\n", " ");
		if (html.length() <= 16)
			this.content=html;
		else
			this.content=StringHelper.replace(StringHelper.replace(html.substring(15).replace("\"", "\\\""), "\r\n", " "), "\n", " ");
		this.parent=parent;
	}
	
	public void appendHTML(Buffer buffer) {
	}

	public String getJavaScript() {
		parent.removeUIObject(this, false);
		String x = "printWindow"/*+System.currentTimeMillis()*/;
		String cleanUp="if (" + x + "){ try{ " + x + ".close(); delete "+x+"; } catch (e){window[\""+x+"\"] = undefined;}}";
		return cleanUp+"\nvar " + x + " = window.open(\"\", \""+title+"\", \"location=no, menubar=no, status=no, titlebar=no, toolbar=no, t_op=20, l_eft=20, w_idth=400, h_eight=400\");\n"
				+ "var content = \"" + content + "\";\n" + x+".document.documentElement.innerHTML = content;\n"
				+ "window.setTimeout(function(){\n"+x+".print();\n},750);\n" + "window.setTimeout(function(){"+cleanUp+"},1500); "  /*+"window.onfocus=function(){"+cleanUp+"}"*/;
	}
	

}
