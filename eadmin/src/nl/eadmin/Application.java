package nl.eadmin;

import java.util.Calendar;
import java.util.HashSet;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.DBVersionManager;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.renderer.EAdminRendererExtension;
import nl.eadmin.ui.adapters.MainAdapter;
import nl.eadmin.ui.adapters.MainProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.util.Scrambler;
import nl.sf.api.CustomerData;
import nl.sf.api.InitializationRequest;
import nl.sf.api.Module;
import nl.sf.api.ProductData;
import nl.sf.api.RendererExtension;
import nl.sf.api.UserData;

public class Application implements nl.sf.api.Application {
	private static final long serialVersionUID = -4531699162529697627L;
	private static final Module[] modules = new Module[] { new Module("module01", "online factureren"), new Module("module02", "online boekhouden") };
	private static HashSet<String> dbInitialized = new HashSet<String>();

	@Override
	public String getName() {
		return "eadmin";
	}

	@Override
	public String getLabel() {
		return "Mijnadmini";
	}

	@Override
	public Module[] getModules() {
		return modules;
	}

	@Override
	public void initialize(DataObject dataObject, InitializationRequest request) throws Exception {
		ProductData productData = request.getProductData();
		CustomerData customer = request.getCustomerData();
		UserData user = request.getUserData();
		String userId = user.getUserId();
		String customerId = customer.getCustomerId();
		boolean isAdmin = customerId.equals(userId);
		if (dbInitialized.contains(productData.getDatabase()) == false) {
			String name = customer.getFirstName();
			if (hasValue(customer.getPrefixes()))
				name += " " + customer.getPrefixes().trim();
			name += customer.getLastName();
			ApplicationSetup.initialize(customer.getCustomerId(), name);
			dbInitialized.add(productData.getDatabase());
		}
		dataObject.setSessionAttribute(SessionKeys.INIT_REQUEST, request);
		dataObject.setSessionAttribute(SessionKeys.PRODUCT_DATA, productData);
		Gebruiker gebruiker = GebruikerManagerFactory.getInstance().findOrCreate(userId);
		if (hasValue(gebruiker.getNaam()) == false) {
			gebruiker.setNaam(user.getFirstName() + " " + user.getLastName());
		}
		if (hasValue(gebruiker.getBedrijf()) == false) {
			Bedrijf bedrijf = BedrijfManagerFactory.getInstance(gebruiker.determineDBData()).getFirstObject(null);
			if (bedrijf != null) {
				gebruiker.setBedrijf(bedrijf.getBedrijfscode());
			}
			gebruiker.setWachtwoord(Scrambler.scramble(""));
		}
		if (gebruiker.getBoekjaar() == 0) {
			gebruiker.setBoekjaar(Calendar.getInstance().get(Calendar.YEAR));
		}
		dataObject.setSessionAttribute(SessionKeys.USER_IS_ADMIN, isAdmin ? "true" : null);
		Bedrijf bedrijf = hasValue(gebruiker.getBedrijf()) ? BedrijfManagerFactory.getInstance(gebruiker.determineDBData()).findByPrimaryKey(gebruiker.getBedrijf()) : null;
		Integer boekjaar = gebruiker.getBoekjaar();
		dataObject.setSessionAttribute(SessionKeys.GEBRUIKER, gebruiker);
		dataObject.setSessionAttribute(SessionKeys.GEBRUIKER_ID, gebruiker.getId());
		dataObject.setSessionAttribute(SessionKeys.BEDRIJF, bedrijf);
		dataObject.setSessionAttribute(SessionKeys.BEDRIJF_ID, bedrijf.getBedrijfscode());
		dataObject.setSessionAttribute(SessionKeys.BOEKJAAR, boekjaar);
		dataObject.setSessionAttribute(SessionKeys.ROLES, user.getRoles());
		// ActiveUsers.addUser(gebruiker.getId(), gebruiker.getNaam(),
		// dataObject);
		ESPSessionContext.setSettingStore(new ApplicationUserSettings(new String[] { userId, "default" }));
		ESPSessionContext.getSettingsStore().setContext("");
	}

	private boolean hasValue(String string) {
		return string != null && string.trim().length() > 0;
	}

	@Override
	public RendererExtension getRendererExtension() {
		return EAdminRendererExtension.getInstance();
	}

	@Override
	public String getLanguageCodeBasePaths() {
		// TODO Auto-generated method stub
		// return "nl/eadmin/language/Texts";
		return null;
	}

	@Override
	public String getMainPage() {
		return MainProcess.class.getName() + ":" + MainAdapter.class.getName();
		// return
		// "nl.eadmin.ui.adapters.MainProcess:nl.eadmin.ui.adapters.MainAdapter"
	}

	@Override
	public String getMainAction() {
		return MainAdapter.START_ACTION;
	}

	@Override
	public String getDBManager() {
		return DBVersionManager.class.getName();
	}
}