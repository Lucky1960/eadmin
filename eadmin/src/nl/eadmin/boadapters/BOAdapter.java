package nl.eadmin.boadapters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import nl.eadmin.AppDBConfigProperties;
import nl.eadmin.AppDBData;
import nl.eadmin.ApplicationConstants;
import nl.ibs.esp.language.LanguageHelper;
import nl.ibs.jeelog.Log;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.DBPersistenceManager;
import nl.ibs.jsql.DBTransaction;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.exception.FinderException;
import nl.ibs.jsql.exception.UpdateException;

public abstract class BOAdapter {

	public static final String SYSTEM = "System";
	public static final String USER = "User";
	public static final String PASSWORD = "Password";
	public static final String ORDER_BY = "orderBy";
	public static final String SEPARATOR = ",";
	public static final String LEVELS = "levels";
	public static final BigDecimal ZERO = ApplicationConstants.ZERO;

	// orderBy terms are , separated f.i. "level, id"
	protected static void addOrdering(ExecutableQuery query, Map<?, ?> map, String defaultOrdering) throws Exception {
		String ordering = defaultOrdering;
		if (map != null) {
			String orderBy = (String) map.get(ORDER_BY);
			if (orderBy == null || orderBy.length() == 0) {
				ordering = defaultOrdering;
			} else {
				if (defaultOrdering != null && orderBy.indexOf(SEPARATOR + defaultOrdering) == -1) {
					ordering = orderBy + SEPARATOR + defaultOrdering;
				} else {
					ordering = orderBy;
				}
			}
		}

		if (Log.debug()) {
			Log.debug("Ordering: " + ordering);
		}
		if (ordering != null) {
			query.setOrdering(ordering);
		}
	}

	public static String[] getOrderBy(String orderBy) {
		if (orderBy == null) {
			return new String[0];
		}
		int i = 0;
		StringTokenizer tokenizer = new StringTokenizer(orderBy, SEPARATOR);
		String[] result = new String[tokenizer.countTokens()];
		while (tokenizer.hasMoreTokens()) {
			result[i++] = tokenizer.nextToken();
		}
		return result;
	}

	public static String transLateOrderBy(String orderBy) {
		StringBuffer sb = new StringBuffer();
		String string;
		String[] strings = getOrderBy(orderBy);
		for (int i = 0; i < strings.length; i++) {
			string = strings[i];
			sb.append(LanguageHelper.getString(string));
			if (i < strings.length - 1) {
				sb.append(SEPARATOR);
			}
		}
		return sb.toString();
	}

	public static String[] getSortFields(Map<?, ?> map) {
		return getOrderBy((String) map.get(BOAdapter.ORDER_BY));
	}

	public static List<?> getFields(Map<?, ?> map, final String[] labels) {
		List<String> result = new ArrayList<String>();
		String[] fields = new String[labels.length];
		System.arraycopy(labels, 0, fields, 0, fields.length);
		// add fields in the right order
		String[] sortFields = getSortFields(map);
		if (sortFields != null) {
			for (int i = 0; i < sortFields.length; i++) {
				result.add(sortFields[i]);
				for (int j = 0; j < labels.length; j++) {
					if (sortFields[i].equals(labels[j])) {
						fields[j] = null;
					}
				}
			}
		}
		// add the fields which are not in the order by
		for (int i = 0; i < fields.length; i++) {
			if (fields[i] != null) {
				result.add(fields[i]);
			}
		}
		return result;
	}

	public static String[] getTotalLevels(Map<?, ?> map) {
		String levels = (String) map.get(LEVELS);
		if (levels == null) {
			return null;
		}
		String[] level = new String[levels.length() + 1];
		int value;
		for (int i = 0; i < levels.length(); i++) {
			value = Integer.parseInt(levels.substring(i, i + 1));
			if (value == 0) {
				continue;
			}
			level[value] = "" + i;
		}
		return level;
	}

	public static String addFilterTerm(String filter, String term) throws Exception {
		if (term == null || term.length() == 0) {
			return filter;
		}
		if (filter != null) {
			return filter + " AND " + term;
		}
		return term;
	}

	public static boolean beginTransaction() throws Exception {
		DBTransaction dbTransaction = DBPersistenceManager.getCurrentTransaction();
		boolean alreadyActive = dbTransaction.isActive();
		if (Log.debug() && alreadyActive) {
			Log.debug("transaction is already active");
		}
		if (!alreadyActive) {
			Log.debug("begin transaction");
			dbTransaction.begin();
		}
		return alreadyActive;
	}

	// invoke this method to commit an active transaction
	public static void commitTransaction() throws Exception {
		commitTransaction(false);
	}

	public static void commitTransaction(boolean alreadyActive) throws Exception {
		if (Log.debug() && alreadyActive) {
			Log.debug("transaction is already active");
		}
		if (!alreadyActive) {
			DBTransaction dbTransaction = DBPersistenceManager.getCurrentTransaction();
			Log.debug("commit transaction");
			dbTransaction.commit();
		}
	}

	public static String investigate(Exception e, DBTransaction dbTransaction) throws Exception {
		if (dbTransaction != null && dbTransaction.isActive()) {
			try {
				Log.debug("rollback transaction");
				dbTransaction.rollback();
			} catch (Exception ex) {
				Log.error(e);
				Log.error("Severe problem: rollback failed");
				throw ex;
			}
		}

		if (e == null) {
			return null;
		}
		if (e instanceof NullPointerException || e.getMessage() == null) {
			Log.error(e);
			return "system_error";
		}
		if (e instanceof FinderException) {
			return "Key_not_found";
		}
		if (e instanceof UpdateException) {
			return "record_changed";
		}
		if (e.getMessage().startsWith("[SQL0803]")) {
			return "duplicate_key";
		}
		if (e.getMessage().startsWith("Data truncation")) {
			return "data_truncation";
		}
		Log.error(e);
		return e.getMessage();
	}

	protected static String escapeSingleQuotes(String value) {
		if (value.indexOf('\'') >= 0) {
			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < value.length(); i++) {
				if (value.charAt(i) == '\'') {
					buffer.append('\'');
				}
				buffer.append(value.charAt(i));
			}
			value = buffer.toString();
		}
		return "'" + value + "'";
	}

	public static DBData getDBData(String library) {
		String url = AppDBConfigProperties.getURL();
		int ix = url.lastIndexOf("/");
		if (ix > -1) {
			url = url.substring(0, ix) + "/" + library;
		}

		Log.debug(url);
		Log.debug(library);
		return new AppDBData(library, AppDBConfigProperties.getJDBCDriver(), url, library, AppDBConfigProperties.getUser(), AppDBConfigProperties.getPassword());
	}

}
