package nl.eadmin.boadapters;

import nl.eadmin.AppDBConfigProperties;
import nl.eadmin.ApplicationConstants;
import nl.eadmin.ApplicationSetup;
import nl.eadmin.db.Environment;
import nl.eadmin.db.EnvironmentDataBean;
import nl.eadmin.db.EnvironmentManager;
import nl.eadmin.db.EnvironmentManagerFactory;
import nl.eadmin.db.impl.EnvironmentManager_Impl;
import nl.eadmin.language.LanguageHelper;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.DBPersistenceManager;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.jsql.exception.FinderException;
import nl.ibs.jsql.sql.ConnectionProvider;
import nl.ibs.jsql.sql.DBConnection;
import nl.ibs.jsql.sql.DBConnectionPool;

public abstract class EnvironmentBO extends BOAdapter {

	public static Object[] get(String dtaLib) throws Exception {
		Object[] result = new Object[2];
		try {
			result[0] = EnvironmentManagerFactory.getInstance().findByPrimaryKey(dtaLib);
		} catch (FinderException e) {
			result[1] = "key_not_found";
		}
		return result;
	}

	public static Object[] create(String dtaLib, String description) throws Exception {
		Object[] result = new Object[2];
		String message = validateForCreate(dtaLib);
		if (message != null && message.trim().length() > 0) {
			result[1] = message;
			return result;
		}
		EnvironmentManager manager = EnvironmentManagerFactory.getInstance();
		EnvironmentDataBean bean = new EnvironmentDataBean();
		bean.setDtaLib(dtaLib);
		bean.setDescription(description);
		Environment environment = manager.create(bean);
		result[0] = environment;

		// Now 'touch' the new database, jsql will create the guy if not existing. . !
		DBData dbd = getDBData(dtaLib);
		manager = EnvironmentManagerFactory.getInstance(dbd);
		if (manager.getFirstObject(null) == null) {
			ApplicationSetup.setupEnvironment(dbd);
		} else {
			environment.delete();
			result[0] = null;
			result[1] = LanguageHelper.getString("DefaultLibNotAllowed");
		}

		return result;
	}

	private static String validateForCreate(String dtaLib) throws Exception {
		String message = null;
		if (dtaLib.equalsIgnoreCase(AppDBConfigProperties.getSchema())) {
			return LanguageHelper.getString("DefaultLibNotAllowed");
		}
		String s = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + dtaLib + "'";
		FreeQuery qry = QueryFactory.createFreeQuery((EnvironmentManager_Impl) EnvironmentManagerFactory.getInstance(), s);
		Object[][] rslt = qry.getResultArray();
		if (rslt != null && rslt.length > 0 && rslt[0].length > 0) {
			return LanguageHelper.getString("Library_already_exists");
		}
		return message;
	}

	public static String delete(Environment environment) throws Exception {
		try {
			DBData dbBData = getDBData(environment.getDtaLib());
			DBConnectionPool.getInstance(dbBData).disconnectAll();
/*LVL
			String[] result = BatchJobHelper.runCommand(loginHelper, "DLTLIB LIB(" + environment.getDtaLib() + ")");
			if (result != null && result[1] != null && !"CPC2194".equals(result[1])) {
				return result[0];
			}
*/
			removeUserSettings(environment.getDtaLib());
			environment.delete();
		} catch (Exception e) {
			return investigate(e, null);
		}
		return null;
	}

	public static void removeUserSettings(String context) throws Exception {
		ConnectionProvider provider = DBPersistenceManager.getConnectionProvider(DBData.getDefaultDBData(ApplicationConstants.APPLICATION));
		DBConnection dbc = provider.getConnection();
		StringBuffer buffer = new StringBuffer(64);
		try {
			buffer.append("DELETE FROM ");
			buffer.append(provider.getPrefix());
			buffer.append("JSQLUSRST");
			buffer.append(" WHERE ");
			buffer.append(" US_CONTEXT");
			buffer.append(" = '");
			buffer.append(context);
			buffer.append("'");
			dbc.executeUpdate(buffer.toString());
		} finally {
			provider.returnConnection(dbc);
		}
	}

}
