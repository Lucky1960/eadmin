/**
 * 
 */
package nl.eadmin.usertool;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bedrijf;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.jsql.DBData;

/**
 * Deze class verwijdert een gegevensbronrubriek uit de MM-database
 */

public class EA0001 implements UserTool, UserToolBedrijfChangeListener {

	UserToolInvokeAdapter invokeAdapter;
	UserToolFieldGroup01 fieldGroup1;
	FieldGroup fieldGroup2;
	Bedrijf matchSource;
	boolean primary;
	DBData dbd;
	ComboBox cbLocation;
	Field dummyFld = new Field();

	// ***********************************************************
	// This is the guy who should do the job . . !
	// ***********************************************************
	public void execute(String[] parameterValues) throws Exception {
		matchSource = fieldGroup1.getBedrijf();
		primary = fieldGroup1.getPrimary();
		dbd = matchSource.getDBData();
		update();
	}

	private void update() throws Exception {
	}

	// ***********************************************************
	// This method is called by the invoker always. Therefor each
	// userTool is able to use invoker's convenience methods like
	// print(), printLine(), etc.
	// ***********************************************************
	public void setInvoker(UserToolInvokeAdapter invokeAdapter) throws Exception {
		this.invokeAdapter = invokeAdapter;
	}

	public void listenToBedrijfChange(Bedrijf matchSource, boolean primary) throws Exception {
		this.matchSource = matchSource;
		this.primary = primary;
		this.dbd = matchSource == null ? DBData.getDefaultDBData(ApplicationConstants.APPLICATION) : matchSource.getDBData();
		if (matchSource == null) {
			fieldGroup2.setHidden(true);
			return;
		}
		fieldGroup2.setHidden(false);
	}

	// ***********************************************************
	// Enhancing the methods underneath is optional . . !
	// ***********************************************************
	public FieldGroup getFieldGroup() throws Exception {
		fieldGroup1 = new UserToolFieldGroup01(true);
		fieldGroup1.forceBorder(false);
		fieldGroup1.addOnChangeListener(this);
		cbLocation = new ComboBox("Label.removeSourceFieldFrom");
		cbLocation.addOption("Label.removeSourceFieldMatchSourceOnly", "M", true);
		cbLocation.addOption("Label.removeSourceFieldAll", "A");
		cbLocation.setWidth("200");
		dummyFld.setHidden(true);
		fieldGroup2 = new FieldGroup();
		fieldGroup2.forceBorder(false);
		fieldGroup2.add(dummyFld);
		fieldGroup2.add(cbLocation);
		fieldGroup2.setHidden(true);
		FieldGroup fg = new FieldGroup();
		fg.add(fieldGroup1);
		fg.add(fieldGroup2);
		return fg;
	}

	public String[] validate() throws Exception {
		return null;
	}

	public String[] getParameterDescriptions() throws Exception {
		return null;
	}

	public String getProgramDescription() throws Exception {
		return "Gegevensbron-rubriek verwijderen uit de MM-database";
	}

	public String getCompletionMessage() throws Exception {
		return "Conversie succesvol uitgevoerd!";
	}
}
