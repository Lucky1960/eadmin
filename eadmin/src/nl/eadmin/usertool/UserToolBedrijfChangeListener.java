/**
 * 
 */
package nl.eadmin.usertool;

import nl.eadmin.db.Bedrijf;


public interface UserToolBedrijfChangeListener {
	
	public void listenToBedrijfChange(Bedrijf bedrijf, boolean primary) throws Exception;
}
