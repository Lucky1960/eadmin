/**
 * 
 */
package nl.eadmin.usertool;

import nl.ibs.esp.uiobjects.FieldGroup;

/**
 * Classes implementing this interface are handled by the UserToolInvokeAdapter (= invoker).
 * As a matter of fact three "userTool types" are supported :
 * 
 * 1. The userTool defines its "own" FieldGroup and provides it to the invoker by means of
 *    getFieldGroup(). The FieldGroup will be displayed and after <OK> the method validate()
 *    will be called by the invoker. This method may return a String[] or null. If null is
 *    returned the invoker will call the execute() method with null as the argument.
 *     
 * 2. The userTool defines one or more "parameter descriptions" provides them to the invoker
 *    by means of getParameterDescriptions(). A panel (based on the parameter descriptions)
 *    will be created and displayed by the invoker. After <OK> the method validate()
 *    will be called by the invoker. This method may return a String[] or null. If null is
 *    returned the invoker will call the execute() method with parameterValues[] as the
 *    argument.
 *     
 * 3. The userTool provides neither a fieldGroup nor parameterDescription() to the invoker.
 *    As a result the invoker will call the method validate() directly. If null is returned
 *    the invoker will call the execute() method with null as the argument.
 *
 * The userTool may use some convenience methods like invoker.print() and invoker.printLine() in 
 * order to produce the AS400 report.
 *      
 */
public interface UserTool {
	
	public FieldGroup getFieldGroup() throws Exception;
	
	public String[] validate() throws Exception;

	public String[] getParameterDescriptions() throws Exception;

	public String getProgramDescription() throws Exception;

	public void execute(String[] parameterValues) throws Exception;

	public String getCompletionMessage() throws Exception;

	public void setInvoker(UserToolInvokeAdapter invokeAdapter) throws Exception;
}
