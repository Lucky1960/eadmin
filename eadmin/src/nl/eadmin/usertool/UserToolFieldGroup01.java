/**
 * 
 */
package nl.eadmin.usertool;

public class UserToolFieldGroup01 extends UserToolFieldGroup {
	private static final long serialVersionUID = 1091853544193668423L;

	public UserToolFieldGroup01(boolean showPrimarySecundary) throws Exception {
		super(showPrimarySecundary);
		add(environmentRefFld);
		add(matchSourceRefFld);
		add(cbPrimary);
	}
}
