/**
 * 
 */
package nl.eadmin.usertool;

import java.util.ArrayList;

import nl.eadmin.SessionKeys;
import nl.eadmin.boadapters.EnvironmentBO;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Environment;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.ui.adapters.EnvironmentReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.BedrijfReferenceField;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.jsql.DBData;

public abstract class UserToolFieldGroup extends FieldGroup {
	private static final long serialVersionUID = 1L;
	protected EnvironmentReferenceField environmentRefFld;
	protected BedrijfReferenceField matchSourceRefFld;
	protected Environment environment;
	protected Bedrijf matchSource;
	protected DBData dbd;
	protected Gebruiker user;
	protected ComboBox cbPrimary;
	protected boolean showPrimarySecundary, primary = true;
	private ArrayList<UserToolBedrijfChangeListener> listeners;

	public UserToolFieldGroup(boolean showPrimarySecundary) throws Exception {
		this.showPrimarySecundary = showPrimarySecundary;
		initialize();
	}

	private void initialize() throws Exception {
		user = (Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
		dbd = (DBData) ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		environment = (Environment) EnvironmentBO.get(dbd.getSchema())[0];
		matchSource = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		listeners = new ArrayList<UserToolBedrijfChangeListener>();
		BedrijfListener matchSourceListener = new BedrijfListener();
		environmentRefFld = new EnvironmentReferenceField(user);
		environmentRefFld.setDescriptionLength(50);
		environmentRefFld.setMandatory(true);
		environmentRefFld.setSelectedObject(environment);
		environmentRefFld.addOnChangeListener(new EnvironmentListener());
		matchSourceRefFld = new BedrijfReferenceField("Bedrijf", dbd, BedrijfManagerFactory.getInstance(dbd), user);
		matchSourceRefFld.setLength(12);
		matchSourceRefFld.setMaxLength(3);
		matchSourceRefFld.setDescriptionLength(50);
		matchSourceRefFld.setMandatory(true);
		matchSourceRefFld.setSelectedObject(matchSource);
		matchSourceRefFld.addOnChangeListener(matchSourceListener);
		cbPrimary = new ComboBox("Label.source");
		cbPrimary.addOption("A", "A");
		cbPrimary.addOption("B", "B");
		cbPrimary.setWidth("40");
		if (showPrimarySecundary) {
			cbPrimary.setHidden(false);
			cbPrimary.addOnChangeListener(matchSourceListener);
		} else {
			cbPrimary.setHidden(true);
		}
	}

	public Environment getEnvironment() throws Exception {
		return environment;
	}

	public Bedrijf getBedrijf() throws Exception {
		return matchSource;
	}

	public boolean getPrimary() throws Exception {
		return primary;
	}

	public void addOnChangeListener(UserToolBedrijfChangeListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	//***************************************************************************************
	private class EnvironmentListener implements EventListener {
		private static final long serialVersionUID = 990100619209012156L;

		public void event(UIObject object, String type) throws Exception {
			environment = (Environment) environmentRefFld.getSelectedObject();
			matchSource = null;
			dbd = EnvironmentBO.getDBData(environment.getDtaLib());
			BedrijfReferenceField newBedrijfRefFld = new BedrijfReferenceField("Administratie", dbd, BedrijfManagerFactory.getInstance(dbd), user);
			newBedrijfRefFld.setLength(12);
			newBedrijfRefFld.setMaxLength(3);
			newBedrijfRefFld.setDescriptionLength(50);
			newBedrijfRefFld.setMandatory(true);
			replaceUIObject(matchSourceRefFld, newBedrijfRefFld);
			matchSourceRefFld = newBedrijfRefFld;
			BedrijfListener matchSourceListener = new BedrijfListener();
			matchSourceListener.event(null, "");
			matchSourceRefFld.addOnChangeListener(matchSourceListener);
		}
	}

	private class BedrijfListener implements EventListener {
		private static final long serialVersionUID = 4131978105592724934L;

		public void event(UIObject object, String type) throws Exception {
			matchSource = (Bedrijf) matchSourceRefFld.getSelectedObject();
			primary = cbPrimary.getSelectedOptionValue().equals("A");
			for (int i = 0; i < listeners.size(); i++) {
				listeners.get(i).listenToBedrijfChange(matchSource, primary);
			}
		}
	}
}
