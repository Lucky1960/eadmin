/**
 * 
 */
package nl.eadmin.usertool;

import nl.ibs.esp.uiobjects.FieldGroup;

/**
 * This class blah-blah-blah : Don't forget to add some meaningful comment here !
 *  
 */

public class UserToolTemplate implements UserTool {
	
	UserToolInvokeAdapter invokeAdapter;
	
	// ***********************************************************
	// This is the guy who should do the job . . !
	// ***********************************************************
	public void execute(String[] parameterValues) throws Exception {
		
	}

	// ***********************************************************
	// This method is called by the invoker always. Therefor each
	// userTool is able to use invoker's convenience methods like
	// print(), printLine(), etc.
	// ***********************************************************
	public void setInvoker(UserToolInvokeAdapter invokeAdapter) throws Exception {
		this.invokeAdapter = invokeAdapter;
	}
	
	// ***********************************************************
	// Enhancing the methods underneath is optional . . !
	// ***********************************************************
	public FieldGroup getFieldGroup() throws Exception {
		// If there's no need for a tailor made FieldGroup, then . . .
		return null;
		// Otherwise : create and return your FieldGroup here !
	}
	
	public String[] validate() throws Exception {
		// If there's no need for validations, then . . .
		return null;
		// Otherwise : define them and return error messages (or null) here !
	}

	public String[] getParameterDescriptions() throws Exception {
		// If there's no need for a FieldGroup (created by the invoker) . . .
		return null;
		// Otherwise return the field descriptions here !
	}

	public String getProgramDescription() throws Exception {
		// If returning a program description is irrelevant return null . . .
		return null;
		// Otherwise return a meaningful description !
	}

	public String getCompletionMessage() throws Exception {
		// If there's no need for a specific completion message return null . . .
		return null;
		// Otherwise return a meaningful completion message !
	}
}
