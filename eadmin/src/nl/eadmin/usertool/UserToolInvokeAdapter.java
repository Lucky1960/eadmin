/**
 * 
 */
package nl.eadmin.usertool;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigDecimal;

import nl.eadmin.SessionKeys;
import nl.eadmin.ui.adapters.ProjectProcessAwareAdapter;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.Message;
import nl.ibs.jsql.DBData;

/**
 * This class loads a UserTool (= userTool). It's method execute() is the guy
 * who does the job :
 * 
 * 1. If "userTool.getFieldGroup() returns a FieldGroup (not null) that
 * FieldGroup is displayed as second panel (showParameterPanel()). After <OK>
 * the method userTool.execute() (with null as the argument) is called.
 * 
 * 2. If "userTool.getParameterDescriptions()" returns null the method
 * userTool.execute() (with null as the argument) is called directly, else
 * 
 * 3. A panel (based on the parameter descriptions) is created and displayed.
 * After <OK> the method userTool.execute() (with parameterValues[] as the
 * argument) is called.
 * 
 * Besides that the class offers the possibility for userTools to create
 * (simple) AS400 spooled files. If userTool.getAS400WriterNeeded() returns
 * true, convenience methods like invoker.print() and invoker.printLine() are at
 * its disposal. Note : printing may be enhanced in the future, however, the
 * current (simple) implementation will do for now (I hope).
 * 
 */

public class UserToolInvokeAdapter extends ProjectProcessAwareAdapter {

	private static final long serialVersionUID = -967472151154123889L;

	public static String PAGE = UserToolInvokeAdapter.class.getName();
	private Header header = new Header("Title.InvokeUserTool", Header.TYPE_SCREEN_NAME, null);
	private FieldGroup fieldGroup;
	private Field utilityField;
	private Field descriptionField;
	private FloatBar floatBar_1;
	private FloatBar floatBar_2;

	private DBData dbData = null;
// LVL private Environment env = null;

	private boolean as400WriterNeeded = false;
	private PrintWriter writer;
	private StringBuffer line;
	private String blanks = "                                                                       ";

	private UserTool userTool;
	private String[] parameterDescriptions;
	private int numberOfParameters;
	private FieldGroup parameterPanel;
	private Field[] fields;

	// **********************************************
	// Constructor + initialization . . .
	// **********************************************
	public UserToolInvokeAdapter(ESPProcess process, DataObject object) throws Exception {
		super(process, object);
		dbData = (DBData) object.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
// LVL env = (Environment) EnvironmentBO.get(dbData.getSchema())[0];
	}

	private void initialize(DataObject object) throws Exception {
		File file = new File("UserToolOutput.txt");
//LVL	file.getParentFile().mkdirs();
		writer = new PrintWriter(file);

		fieldGroup = new FieldGroup();
		utilityField = new Field("Hulpprogramma");
		utilityField.setMandatory(true);
		fieldGroup.add(utilityField);
		// fieldGroup.add(new
		// UserToolReferenceField(getUserToolDataCollection()) {
		// private static final long serialVersionUID = 5580689145576582126L;
		// });
		descriptionField = new Field("Omschrijving");
		descriptionField.setLength(70);
		descriptionField.setReadonly(true);
		descriptionField.setHidden(true);
		fieldGroup.add(descriptionField);
		Action action = new Action("OK").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("execute").setIcon("tick.png");
		action.setDefault(true);
		action.setValidationEnabled(true);
		floatBar_1 = new FloatBar();
		floatBar_1.addAction(action);
		floatBar_1.addAction(finish);
		floatBar_2 = new FloatBar();
		line = new StringBuffer();
		numberOfParameters = 0;
		initialized = true;
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false, true);
		object.addUIObject(header);
		object.addUIObject(fieldGroup);
		object.addUIObject(floatBar_1);
		return object;
	}

	// private Collection<UserToolData> getUserToolDataCollection() throws
	// Exception {
	// ArrayList<UserToolData> list = new ArrayList<UserToolData>();
	// Class<?> myClass = null;
	// Object myObject = null;
	// DecimalFormat format = new DecimalFormat("0000");
	// String name;
	// for (int i = 0; i < 1000; i++) {
	// try {
	// name = "MM" + format.format(i);
	// myClass = Class.forName(("nl.ibs.matching.usertool." + name), true,
	// this.getClass().getClassLoader());
	// myObject = myClass.newInstance();
	// if (myObject instanceof UserTool) {
	// list.add(new UserToolData(name, ((UserTool)
	// myObject).getProgramDescription()));
	// }
	// } catch (Exception e) {
	// }
	// }
	// return list;
	// }

	// ************************************************************
	// Getting the userTool and calling its execute() method . . .
	// ************************************************************
	public DataObject execute(DataObject object) throws Exception {
		numberOfParameters = 0;
		// This method instantiates the FACUserTool class in the first place . .
		// .
		Class<?> myClass = null;
		Object myObject = null;
		try {
			myClass = Class.forName(("nl.eadmin.usertool." + utilityField.getValue().trim()), true, this.getClass().getClassLoader());
			myObject = myClass.newInstance();
		} catch (Exception e) {
			object.addUIObject(new Message("Geen geldige naam opgegeven (1)!", Message.TYPE_ERROR));
			return object;
		}
		if (!(myObject instanceof UserTool)) {
			object.addUIObject(new Message("Geen geldige naam opgegeven (2)!", Message.TYPE_ERROR));
			return object;
		}
		// It's a valid MMUserTool so, . . .
		userTool = (UserTool) myObject;
		userTool.setInvoker(this);
		parameterPanel = userTool.getFieldGroup();
		if (parameterPanel != null) {
			// 1. Either we get a second panel directly from the MMUserTool . .
			// .
			return showParameterPanel(object);
		}
		parameterDescriptions = userTool.getParameterDescriptions();
		if (parameterDescriptions != null) {
			numberOfParameters = parameterDescriptions.length;
		}
		// 2. or we have to build the second panel ourselves . . .
		return showParameterPanel(object);
	}

	// This method takes care of the second (FACUserTool-specific) panel. Either
	// it is
	// created and displayed or displayed only . . .
	private DataObject showParameterPanel(DataObject object) throws Exception {
		handleFirstPanel(object);
		if (numberOfParameters > 0) {
			parameterPanel = createParameterPanel();
		}
		object.addUIObject(parameterPanel);

		floatBar_2 = new FloatBar();
		Action action = new Action("Button.OK", null, null, getProcessAdapterPageName(PAGE), "executeAfterSecondPanelDisplay").setIcon("tick.png");
		action.setDefault(true);
		action.setValidationEnabled(true);
		floatBar_2.addAction(action);
		floatBar_2.addAction(back.setMethod(RESET_ACTION));
		floatBar_2.addAction(finish);
		object.addUIObject(floatBar_2);
		return object;
	}

	// First of all this method calls userTool.validate(). If no errors are
	// detected this method
	// calls userTool.execute(). If the panel was build 'in here' the
	// parameterValues are passed
	// as a string array. Finally the job is ended with an appropriate
	// completion message . . .
	public DataObject executeAfterSecondPanelDisplay(DataObject object) throws Exception {
		String[] messageTexts = userTool.validate();
		if (messageTexts != null) {
			handleMessageTexts(object, messageTexts);
			return object;
		}
		if (numberOfParameters > 0) {
			userTool.execute(getParameterValues());
		} else {
			userTool.execute(null);
		}
		if (parameterPanel != null) {
			parameterPanel.setReadonly();
		}
		floatBar_2.removeAction("Button.OK");
		floatBar_2.removeAction("Button.Back");
		floatBar_2.getAction("Button.Exit").setDefault(true);
		object.addUIObject(new Message(getCompletionMessage(), Message.TYPE_CONFIRM));
		return object;
	}

	// **********************************************
	// Underneath some (private) helper methods . . .
	// **********************************************
	private FieldGroup createParameterPanel() throws Exception {
		FieldGroup parameterFG = new FieldGroup();
		numberOfParameters = parameterDescriptions.length;
		fields = new Field[numberOfParameters];
		Field field;
		for (int i = 0; i < numberOfParameters; i++) {
			field = new Field(parameterDescriptions[i]);
			field.setMandatory(true);
			parameterFG.add(field);
			fields[i] = field;
		}
		return parameterFG;
	}

	private String[] getParameterValues() throws Exception {
		String[] parameterValues = new String[numberOfParameters];
		Field field;
		for (int i = 0; i < numberOfParameters; i++) {
			field = fields[i];
			parameterValues[i] = field.getValue().trim();
		}
		return parameterValues;
	}

	private String getCompletionMessage() throws Exception {
		if (as400WriterNeeded) {
			// writer.close();
		}
		String completionMessage = userTool.getCompletionMessage();
		if (completionMessage == null) {
			completionMessage = "Hulpprogramma succesvol uitgevoerd . . !";
		}
		return completionMessage;
	}

	private void handleFirstPanel(DataObject object) throws Exception {
		clearScreen(true, false, true);
		object.addUIObject(header);
		utilityField.setReadonly(true);
		if (userTool.getProgramDescription() != null) {
			descriptionField.setValue(userTool.getProgramDescription());
			descriptionField.setHidden(false);
		}
		object.addUIObject(fieldGroup);
	}

	private DataObject handleMessageTexts(DataObject object, String[] messageTexts) throws Exception {
		for (int i = 0; i < messageTexts.length; i++) {
			object.addUIObject(new Message(messageTexts[i], Message.TYPE_ERROR));
		}
		return object;
	}

	void print(String s, int width) throws Exception {
		append(s, width, false);
	}

	void print(BigDecimal dec, int width) throws Exception {
		append(dec == null ? "0" : dec.toString(), width, true);
	}

	void print(boolean b, int width) throws Exception {
		append(b ? "Ja" : "Nee", width, false);
	}

	void printBoolean(boolean b, int width) throws Exception {
		append(b ? "1" : "0", width, false);
	}

	private void append(String s, int width, boolean rightAlign) throws Exception {
		if (s == null)
			s = "";

		String fld;
		if (rightAlign) {
			fld = blanks + s;
			fld = fld.substring(fld.length() - width);
		} else {
			fld = s + blanks;
			fld = fld.substring(0, width);
		}
		line.append(fld);
	}

	void printLine() throws Exception {
		writer.write(line.toString());
		writer.println();
		line = new StringBuffer();
	}

	// ************************************************
	// Other helper methods . . .
	// ************************************************
	/*
	 * LVL Environment getEnvironment() throws Exception { return env; }
	 */

	DBData getDBData() throws Exception {
		return dbData;
	}

	// ************************************************
	// Miscellaneous . . .
	// ************************************************
	public String getDescription() {
		return this.translate("Hulpprogramma's");
	}

	public String getShortDescription() {
		return "Hulpprogramma's";
	}

	public String getProcessIconUrl() {
		return "brick_go.png";
	}

}
