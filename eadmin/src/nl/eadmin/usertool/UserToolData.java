/**
 * 
 */
package nl.eadmin.usertool;

public class UserToolData {
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
	private String name, description;

	public UserToolData(String name, String description) throws Exception {
		this.name = name;
		this.description = description;
	}

	public String getName() throws Exception {
		return name;
	}

	public String getDescription() throws Exception {
		return description;
	}
}