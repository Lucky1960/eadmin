/**
 * 
 */
package nl.eadmin.usertool;

import java.util.Collection;

import nl.ibs.esp.uiobjects.CollectionReferenceField;

public abstract class UserToolReferenceField extends CollectionReferenceField {
	public static final long serialVersionUID = 1L;
	private static final String[] NAMES = new String[] { UserToolData.NAME, UserToolData.DESCRIPTION };
	private static final String[] LABELS = new String[] { "Label.Name", "Label.Description" };
	private static final short[] SIZES = new short[] { 90, 400 };

	public UserToolReferenceField(Collection<UserToolData> col) throws Exception {
		super("Label.UserTool", col, UserToolData.class);
		setName(this.getClass().getName());
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(12);
		setMaxLength(10);
		setDescriptionLength(60);
	}
}