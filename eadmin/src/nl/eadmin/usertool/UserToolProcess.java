package nl.eadmin.usertool;

import nl.eadmin.ui.adapters.ProjectProcess;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class UserToolProcess extends ProjectProcess {
	private static final long serialVersionUID = 9096228911637408313L;
	public static final String PAGE = UserToolProcess.class.getName();
	public static final String START_PAGE = UserToolInvokeAdapter.PAGE;
	public static final String START_ACTION = UserToolInvokeAdapter.START_ACTION;

	public UserToolProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new UserToolInvokeAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return this.translate("Hulpprogrammas");
	}

	public String getShortDescription() {
		return "Hulpprogrammas";
	}

	public String getProcessIconUrl() {
		return "application_edit.png";
	}
}