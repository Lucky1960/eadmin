package nl.eadmin;

import nl.eadmin.helpers.MailHelper;



public class MyExceptionTest {
	public static void main(String[] args) throws Exception {
		MyExceptionTest test = new MyExceptionTest();
		test.run();
		System.exit(0);
	}

	private void run() throws Exception {
		try {
			System.out.println(1/0);
		} catch (Exception e) {
			MailHelper.mailErrorMsg(new MyException("Er is een fout opgetreden: " + e.getMessage()));
		}
	}
}
