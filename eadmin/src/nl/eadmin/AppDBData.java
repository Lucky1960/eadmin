/**
 * 
 */
package nl.eadmin;

import nl.ibs.jsql.DBData;

/**
 * @author nlhuucle
 *
 */
public class AppDBData extends DBData {
	private static final long serialVersionUID = 1L;

	/**
	 * @param _name
	 * @param _driver
	 * @param _url
	 * @param _schema
	 * @param _user
	 * @param _password
	 */
	public AppDBData(String _name, String _driver, String _url, String _schema, String _user, String _password) {
		super(ApplicationConstants.APPLICATION, _name, _driver, _url, _schema, _user, _password);
	}

	/**
	 * @param _name
	 * @param _driver
	 * @param _url
	 * @param _schema
	 * @param _user
	 * @param _password
	 * @param _autoCreateTables
	 * @param _autoAddColumns
	 * @param _autoModifyColumns
	 * @param _autoDropColumns
	 */
	public AppDBData(String _name, String _driver, String _url, String _schema, String _user, String _password, boolean _autoCreateTables, boolean _autoAddColumns, boolean _autoModifyColumns, boolean _autoDropColumns) {
		super(ApplicationConstants.APPLICATION, _name, _driver, _url, _schema, _user, _password, _autoCreateTables, _autoAddColumns, _autoModifyColumns, _autoDropColumns);
	}

	/**
	 * @param _name
	 * @param _driver
	 * @param _url
	 * @param _schema
	 * @param _user
	 * @param _password
	 * @param _autoCreateTables
	 * @param _autoAddColumns
	 */
	public AppDBData(String _name, String _driver, String _url, String _schema, String _user, String _password, boolean _autoCreateTables, boolean _autoAddColumns) {
		super(ApplicationConstants.APPLICATION, _name, _driver, _url, _schema, _user, _password, _autoCreateTables, _autoAddColumns);
	}

}
