package nl.eadmin.ui.transformer;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.VerdichtingManager;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.db.VerdichtingPK;
import nl.ibs.esp.util.Transformer;

public class VerdichtingingTransformer implements Transformer {
	private static final long serialVersionUID = 8489026803495216563L;
	private VerdichtingPK key = new VerdichtingPK();
	private VerdichtingManager vrdMgr;

	public VerdichtingingTransformer(Bedrijf bedrijf) throws Exception {
		vrdMgr = VerdichtingManagerFactory.getInstance(bedrijf.getDBData());
		key.setBedrijf(bedrijf.getBedrijfscode());
	}

	public Object transform(Object object) {
		int id = (Integer) object;
		key.setId(id);
		try {
			return id + " - " + vrdMgr.findByPrimaryKey(key).getOmschrijving();
		} catch (Exception e) {
			return id + " - ???";
		}
	}
}