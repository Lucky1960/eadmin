package nl.eadmin.ui.transformer;

import nl.eadmin.enums.HeaderDataStatusEnum;
import nl.ibs.esp.uiobjects.Image;
import nl.ibs.esp.util.Transformer;

public class HeaderDataStatusTransformer implements Transformer {
	private static final long serialVersionUID = -2825242553755964481L;

	public Object transform(Object object) {
		int sts = (Integer) object;
		Image img = new Image("");
		img.setAlignment("center");
		if (sts == HeaderDataStatusEnum.BANK_CLOSED || sts == HeaderDataStatusEnum.KAS_CLOSED || sts == HeaderDataStatusEnum.MEMO_CLOSED || sts == HeaderDataStatusEnum.INKOOP_CLOSED || sts == HeaderDataStatusEnum.VERKOOP_CLOSED ) {
			img.setSource("tick.png");
			img.setTooltip("Boeking is volledig");
		} else {
			img.setSource("cross.png");
			img.setTooltip("Boeking is onvolledig");
		}
		return img;
	}
}