package nl.eadmin.ui.transformer;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.RekeningPK;
import nl.ibs.esp.util.Transformer;

public class RekeningTransformer implements Transformer {
	private static final long serialVersionUID = -2003260354759226923L;
	private RekeningPK key = new RekeningPK();
	private RekeningManager rekMgr;

	public RekeningTransformer(Bedrijf bedrijf) throws Exception {
		rekMgr = RekeningManagerFactory.getInstance(bedrijf.getDBData());
		key.setBedrijf(bedrijf.getBedrijfscode());
	}

	public Object transform(Object object) {
		String rekeningNr = (String) object;
		key.setRekeningNr(rekeningNr);
		try {
			return rekeningNr + " - " + rekMgr.findByPrimaryKey(key).getOmschrijving();
		} catch (Exception e) {
			return rekeningNr + " - ???";
		}
	}
}