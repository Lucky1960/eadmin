package nl.eadmin.ui.transformer;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCodeManager;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.BtwCodePK;
import nl.ibs.esp.util.Transformer;

public class BtwCodeTransformer implements Transformer {
	private static final long serialVersionUID = 125959312513953156L;
	private BtwCodePK key = new BtwCodePK();
	private BtwCodeManager btwMgr;

	public BtwCodeTransformer(Bedrijf bedrijf) throws Exception {
		btwMgr = BtwCodeManagerFactory.getInstance(bedrijf.getDBData());
		key.setBedrijf(bedrijf.getBedrijfscode());
	}

	public Object transform(Object object) {
		key.setCode((String) object);
		try {
			return btwMgr.findByPrimaryKey(key).getOmschrijving();
		} catch (Exception e) {
			return "?-" + (String) object;
		}
	}
}