package nl.eadmin.ui.transformer;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import nl.ibs.esp.servlet.ESPUserContext;
import nl.ibs.esp.util.Transformer;

public class DecimalToAmountTransformer implements Transformer {
	private static final long serialVersionUID = 6701918123452206874L;
	private DecimalFormat format;

	public DecimalToAmountTransformer() {
		this(2);
	}

	public DecimalToAmountTransformer(int decimals) {
		if (decimals == 0) {
			this.format = new DecimalFormat("###,###,###,##0");
		} else {
			this.format = new DecimalFormat("###,###,###,##0." + "0000000000".substring(0, decimals));
		}
		format.setDecimalFormatSymbols(new DecimalFormatSymbols(ESPUserContext.getLocale()));
	}

	public Object transform(Object object) {
		BigDecimal dec = (BigDecimal) object;
		if (dec == null || dec.doubleValue() == 0) {
			return "";
		} else {
			return format.format(dec);
		}
	}
}