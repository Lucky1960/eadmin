package nl.eadmin.ui.transformer;

import java.util.Calendar;
import java.util.Date;

import nl.ibs.esp.util.Transformer;

public class DatumTransformer implements Transformer {
	private static final long serialVersionUID = 1323414019230042846L;

	public Object transform(Object object) {
		Date datum = (Date) object;
		if (datum == null) {
			return "";
		} else {
			Calendar cal = Calendar.getInstance();
			cal.setTime(datum);
			int d = cal.get(Calendar.DAY_OF_MONTH);
			int m = cal.get(Calendar.MONTH) + 1;
			int y = cal.get(Calendar.YEAR);
			StringBuffer sb = new StringBuffer();
			sb.append(d < 10 ? "0" + d : d);
			sb.append("-");
			sb.append(m < 10 ? "0" + m : m);
			sb.append("-" + y);
			return sb.toString();
		}
	}
}