package nl.eadmin.ui.transformer;

import java.math.BigDecimal;
import java.util.Date;

import nl.ibs.esp.uiobjects.Image;
import nl.ibs.esp.util.Transformer;

public class VrijeRubriekTransformer implements Transformer {
	private static final long serialVersionUID = 1323414019230042846L;
	private DatumTransformer datumTransformer = new DatumTransformer();
	private DecimalToAmountTransformer decTransformer = new DecimalToAmountTransformer();
	private NumberTransformer numTransformer = new NumberTransformer();

	public Object transform(Object object) {
		try {
			if (object instanceof Boolean) {
				if (((Boolean)object).booleanValue()){
					Image image = new Image("");
					image.setSource("tick.png");
					image.setAlignment("center");
					return image;
				} else {
					return null;
				}
			} else if (object instanceof Date) {
				return datumTransformer.transform(object);
			} else if (object instanceof BigDecimal) {
				return decTransformer.transform(object);
			} else if (object instanceof Number) {
				return numTransformer.transform(object);
			}
		} catch (Exception e) {
		}
		return object;
	}
}