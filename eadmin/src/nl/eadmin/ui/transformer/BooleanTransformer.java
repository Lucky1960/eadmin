package nl.eadmin.ui.transformer;

import nl.ibs.esp.uiobjects.Image;
import nl.ibs.esp.util.Transformer;

public class BooleanTransformer implements Transformer {
	private static final long serialVersionUID = 1601830704419657412L;
	private Image image;

	public BooleanTransformer() {
		image = new Image("");
		image.setSource("tick.png");
		image.setAlignment("center");
	}

	public Object transform(Object object) {
		if (((Boolean) object).booleanValue()) {
			return image;
		} else {
			return null;
		}
	}
}