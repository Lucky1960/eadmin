package nl.eadmin.ui.transformer;

import nl.eadmin.db.Dagboek;
import nl.ibs.esp.util.Transformer;

public class DebCredTransformer implements Transformer {
	private static final long serialVersionUID = 9014165615454735542L;
	private Dagboek dagboek;
	private boolean isHeader;

	public DebCredTransformer() {
		this(null, true);
	}

	public DebCredTransformer(Dagboek dagboek, boolean isHeader) {
		this.dagboek = dagboek;
		this.isHeader = isHeader;
	}

	public Object transform(Object object) {
		String dc = (String) object;
		if (dagboek.isBankboek() && isHeader) {
			if (dc.equals("D")) {
				return "Bij";
			} else {
				return "Af";
			}
		} else if (dagboek.isBankboek() && !isHeader) {
				if (dc.equals("D")) {
					return "Af";
				} else {
					return "Bij";
				}
		} else if (dagboek.isKasboek()) {
			if (dc.equals("D")) {
				return "Ontvangst";
			} else {
				return "Uitgave";
			}
		} else {
			if (dc.equals("D")) {
				return "Debet";
			} else {
				return "Credit";
			}
		}
	}
}