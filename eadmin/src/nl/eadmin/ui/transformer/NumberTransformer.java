package nl.eadmin.ui.transformer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import nl.ibs.esp.servlet.ESPUserContext;
import nl.ibs.esp.util.Transformer;

public class NumberTransformer implements Transformer {
	private static final long serialVersionUID = 6701918123452206874L;
	private DecimalFormat format;

	public NumberTransformer() {
		this.format = new DecimalFormat("###,###,###,##0");
		format.setDecimalFormatSymbols(new DecimalFormatSymbols(ESPUserContext.getLocale()));
	}

	public Object transform(Object object) {
		Number num = (Number) object;
		if (num == null || num.doubleValue() == 0) {
			return "";
		} else {
			return format.format(num);
		}
	}
}