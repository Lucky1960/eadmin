package nl.eadmin.ui.transformer;

import nl.eadmin.db.HeaderData;
import nl.eadmin.ui.uiobjects.actions.BijlageAction;
import nl.ibs.esp.uiobjects.Image;
import nl.ibs.esp.util.Transformer;

public class BijlageTransformer implements Transformer {
	private static final long serialVersionUID = -1581294202843784016L;

	public Object transform(Object object) {
		HeaderData headerData = (HeaderData) object;
		if (object == null) {
			return "";
		}
		Image image = new Image("Bijlage(n)");
		image.setSource("page_white.png");
		image.setAlignment("center");
		try {
			if (!headerData.getBijlagen().isEmpty()) {
				image.setSource("page_white_magnify.png");
			}
			image.addAction(new BijlageAction(headerData));
		} catch (Exception e) {
		}
		return image;
	}
}