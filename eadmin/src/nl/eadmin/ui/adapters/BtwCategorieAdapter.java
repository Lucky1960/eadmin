package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieManager;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.editors.BtwCategorieEditor;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.DefaultEditor;

public class BtwCategorieAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = -665618379622610564L;
	public static final String PAGE = BtwCategorieAdapter.class.getName();
	private static final String[] NAMES = new String[] { BtwCategorie.CODE, BtwCategorie.OMSCHRIJVING, BtwCategorie.REKENING_NR };
	private static final String[] LABELS = new String[] { "Categorie", "Omschrijving", "Rekening" };
	private static final short[] SIZES = new short[] { 80, 200, 80 };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true };
	private MaintenancePanel maintenancePanel;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public BtwCategorieAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		BtwCategorieManager manager = BtwCategorieManagerFactory.getInstance(bedrijf.getDBData());
		ODBTable table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(BtwCategorie.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("BtwCategorieAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("BtwCategorie�n");
		table.addDownloadPDFContextAction("BtwCategorie�n");
		TableHelper.createMenu(table, new Action[] { refresh }, null);

		DefaultEditor editor = new BtwCategorieEditor(bedrijf, manager);
		maintenancePanel = new MaintenancePanel(BtwCategorie.class, "BtwCategorie", "BtwCategorie�n", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		initialized = true;
	}
}