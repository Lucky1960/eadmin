package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class GebruikerProcess extends ProjectProcess {
	private static final long serialVersionUID = -4738132188350217108L;
	public static final String PAGE = GebruikerProcess.class.getName();
	public static final String START_PAGE = GebruikerAdapter.PAGE;
	public static final String START_ACTION = GebruikerAdapter.START_ACTION;

	public GebruikerProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new GebruikerAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Gebruikers";
	}

	public String getShortDescription() {
		return "Gebruikers";
	}

	public String getProcessIconUrl() {
		return "user.png";
	}
}