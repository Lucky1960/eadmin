package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeManager;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.editors.BtwCodeEditor;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.DefaultEditor;

public class BtwCodeAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 4506310584197112794L;
	public static final String PAGE = BtwCodeAdapter.class.getName();
	private static final String[] NAMES = new String[] { BtwCode.CODE, BtwCode.OMSCHRIJVING, BtwCode.PERCENTAGE };
	private static final String[] LABELS = new String[] { "Code", "Omschrijving", "Percentage" };
	private static final short[] SIZES = new short[] { 50, 200, 80 };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true };
	private MaintenancePanel maintenancePanel;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public BtwCodeAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		BtwCodeManager manager = BtwCodeManagerFactory.getInstance(bedrijf.getDBData());
		ODBTable table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(BtwCode.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("BtwCodeAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("BtwCodeen");
		table.addDownloadPDFContextAction("BtwCodeen");
		TableHelper.createMenu(table, new Action[] { refresh }, null);

		DefaultEditor editor = new BtwCodeEditor(bedrijf, manager);
		maintenancePanel = new MaintenancePanel(BtwCode.class, "BtwCode", "BtwCodes", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		initialized = true;
	}
}