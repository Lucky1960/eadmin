package nl.eadmin.ui.adapters;

import java.util.ArrayList;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.boadapters.EnvironmentBO;
import nl.eadmin.db.Environment;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.query.EnvironmentQuery;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.HeaderPanel;
import nl.ibs.esp.uiobjects.Message;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.CommonTable.SearchPanel;

public class EnvironmentAdapter extends ProjectProcessAwareAdapter {

	private static final long serialVersionUID = 5351420532423933482L;
	public static final String PAGE = EnvironmentAdapter.class.getName();
	private FloatBar fbShow;
	private FloatBar fbCreate;
	private FloatBar fbDelete;
	private Action[] rowActions;
	private Action[] headerActions;
	private ODBTable table = null;
	private SearchPanel searchPanel;
	private static final String[] NAMES = new String[] { Environment.DTA_LIB, Environment.DESCRIPTION };
	private static final String[] LABELS = new String[] { "Label.Environment", "Label.Description" };
	private static final short[] SIZES = new short[] { 90, 290 };
	private Object[] environments;
	// Create panel
	private Panel header;
	private Field mmLib;
	private Field envDescription;

	public EnvironmentAdapter(ESPProcess process, DataObject object) throws Exception {
		super(process, object);
		initialize(object);
	}

	// ****************************************************************
	// Initialize
	// ****************************************************************
	protected void initialize(DataObject object) throws Exception {
		fbShow = new FloatBar();
		fbCreate = new FloatBar();
		fbDelete = new FloatBar();

		Action refresh = new Action("Button.Refresh").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("refresh").setIcon("arrow_refresh.png");
		Action create = new Action("Button.Create").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("initCreateForm").setIcon("add.png");
		Action confirmDelete = new Action("Button.Delete").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("confirmDelete").setIcon("delete.png");
		fbShow.addAction(refresh);
		fbShow.addAction(create);
		fbShow.addAction(confirmDelete);
		fbShow.addAction(finish);

		rowActions = new Action[] { confirmDelete };
		headerActions = new Action[] { refresh, create };

		Action createOK = new Action("Button.OK").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("create");
		createOK.setDefault(true);
//		createOK.setValidationEnabled(true);
		fbCreate.addAction(createOK);
		fbCreate.addAction(cancel);
		fbCreate.addAction(finish);

		Action deleteOK = new Action("Button.OK").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("delete");
		fbDelete.addAction(deleteOK);
		fbDelete.addAction(back);
		fbDelete.addAction(finish);
	}

	// ****************************************************************
	// Create
	// ****************************************************************
	public DataObject initCreateForm(DataObject object) throws Exception {
		header = new HeaderPanel();
		mmLib = new Field("Databibliotheek", Field.TYPE_UPPER);
		mmLib.setLength(10);
		mmLib.setMaxLength(10);
		mmLib.setMandatory(true);
		header.addUIObject(mmLib);
		envDescription = new Field("Omschrijving");
		envDescription.setLength(50);
		header.addUIObject(envDescription);
		return createForm(object);
	}

	public DataObject createForm(DataObject object) throws Exception {
		clearScreen(true, false, true);
		object.addUIObject(new Header("Title.CreateEnvironment", Header.TYPE_SCREEN_NAME));
		object.addUIObject(header);
		object.addUIObject(fbCreate);
		return object;
	}

	public DataObject create(DataObject object) throws Exception {
		Object[] result = EnvironmentBO.create(mmLib.getValue(), envDescription.getValue());
		String message = (String) result[1];
		if (message != null) {
			object.addUIObject(new Message(message, Message.TYPE_ERROR));
			return object;
		}

		Environment env = (Environment) result[0];
		if (table != null) {
			table.setPointer(Environment.DTA_LIB, env.getDtaLib(), true);
			table.reload();
		}
		return show(object);
	}

	// ****************************************************************
	// Show table
	// ****************************************************************
	public DataObject show(DataObject object) throws Exception {
		clearScreen(true, false, true);
		object.addUIObject(new Header("Title.Environments", Header.TYPE_SCREEN_NAME));
		if (table == null) {
			createTable();
		} else {
			if (table.isSelectable()) {
				table.undoSelections();
			}
		}
		object.addUIObject(searchPanel);
		object.addUIObject(table);
		object.addUIObject(fbShow);
		return object;
	}

	private void createTable() throws Exception {
		table = new ODBTable(Environment.class, new EnvironmentQuery(null), new String[] { Environment.DTA_LIB }, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setName(PAGE);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(((Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER)).isAdmin());
		table.setColumnSizes(SIZES);
		table.setSortable(true);
		table.setMultipleSelectable();
		table.orderByColumn(NAMES[0]);
		TableHelper.createMenu(table, headerActions, rowActions);
		table.setRowAction(null);
		searchPanel = table.createSearch(NAMES, LABELS);
		searchPanel.setCSSClass("headerpanel");
		table.reload();
	}

	public DataObject refresh(DataObject object) throws Exception {
		table = null;
		return show(object);
	}

	// ****************************************************************
	// Delete
	// ****************************************************************
	public DataObject confirmDelete(DataObject object) throws Exception {
		environments = TableHelper.getSelectedItems(table, object);
		clearScreen(true, false, true);
		object.addUIObject(new Header("Title.DeleteEnvironments", Header.TYPE_SCREEN_NAME));
		object.addUIObject(createConfirmationTable());
		object.addUIObject(fbDelete);
		return object;
	}

	private CollectionTable createConfirmationTable() throws Exception {
		ArrayList<Object> list = new ArrayList<Object>(environments.length);
		for (int i = 0; i < environments.length; i++) {
			list.add(environments[i]);
		}
		CollectionTable table = new CollectionTable(Environment.class, list, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setName(PAGE);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(((Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER)).isAdmin());
		table.orderByColumn(NAMES[0]);
		table.setColumnSizes(SIZES);
		table.setSortable(true);
		table.reload();
		return table;
	}

	public DataObject delete(DataObject object) throws Exception {
		String message = null;
		Environment env;
		for (int i = 0; i < environments.length; i++) {
			env = (Environment) environments[i];
			if (env == null) {
				continue;
			}
			message = EnvironmentBO.delete(env);
			if (message != null) {
				object.addUIObject(new Message(message, Message.TYPE_ERROR));
			}
		}
		if (message != null) {
			return object;
		}
		table = null;
		return show(object);
	}
}