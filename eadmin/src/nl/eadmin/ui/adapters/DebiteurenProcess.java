package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class DebiteurenProcess extends ProjectProcess {
	private static final long serialVersionUID = 8754054959920646115L;
	public static final String PAGE = DebiteurenProcess.class.getName();
	public static final String START_PAGE = DebiteurenAdapter.PAGE;
	public static final String START_ACTION = DebiteurenAdapter.START_ACTION;

	public DebiteurenProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new DebiteurenAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Debiteuren";
	}

	public String getShortDescription() {
		return "Debiteuren";
	}

	public String getProcessIconUrl() {
		return "group.png";
	}
}