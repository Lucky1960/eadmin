package nl.eadmin.ui.adapters;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.ui.uiobjects.MainMenuBar;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.Message;

public class MainAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = -4417049881055294914L;
	public static final String PAGE = MainAdapter.class.getName();

	public MainAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		object.getActiveProcesses().killActiveProcesses(this, object);
		Bedrijf bedrijf = (Bedrijf) object.getSessionAttribute(SessionKeys.BEDRIJF);
		Integer boekjaar = ((Integer)object.getSessionAttribute(SessionKeys.BOEKJAAR));
//		Image img = new Image("");
//		img.setSource("../eadmin/images/main.png");
//		img.setAlignment("center");

		setMenuBar(null, new MainMenuBar());
		clearScreen(true, false, false);
		object.addUIObject(new Header("&nbsp;",Header.TYPE_SCREEN_NAME));
//		object.addUIObject(img);
		String licenseMsg = (String)ESPSessionContext.getSessionAttribute(SessionKeys.LICENSE_MSG);
		if (licenseMsg != null) {
			object.addUIObject(new Message(licenseMsg));
		}
		if (bedrijf == null) {
			object.setApplicationIdentifyingString("Administratie: <Niet gevonden>");
		} else {
			object.setApplicationIdentifyingString("Administratie: " + bedrijf.getOmschrijving() + " - " + "Boekjr: " + boekjaar);
		}
		return object;
	}
}