package nl.eadmin.ui.adapters;

import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.session.ActiveUserAdapter;
import nl.ibs.esp.uiobjects.RedirectAction;

public class ProjectActiveUserAdapter extends ActiveUserAdapter {
	private static final long serialVersionUID = 2935194437097343675L;

	public ProjectActiveUserAdapter(DataObject object) throws Exception {
		super(object);
	}

	public DataObject finish(DataObject dataObject) throws Exception {
		destroyAdapter(dataObject);
		dataObject.addUIObject(new RedirectAction(dataObject.getConfiguration().getDefaultPage(), SHOW));
		return dataObject;
	}

	public String getProcessIconUrl() {
		return "group.png";
	}
}