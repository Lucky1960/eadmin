package nl.eadmin.ui.adapters;

import java.util.Collection;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.databeans.FactuurDataBean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.db.HtmlTemplatePK;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.HTMLHelper;
import nl.eadmin.helpers.HtmlTemplateHelper;
import nl.eadmin.helpers.PDFHelper;
import nl.eadmin.helpers.PeriodeHelper;
import nl.eadmin.ui.datareaders.DetailDataReader;
import nl.eadmin.ui.datareaders.HeaderDataReader;
import nl.eadmin.ui.editors.DetailDataEditor;
import nl.eadmin.ui.editors.HeaderDataEditor;
import nl.eadmin.ui.uiobjects.MyMaintenancePanel;
import nl.eadmin.ui.uiobjects.actions.ChangeLayoutAction;
import nl.eadmin.ui.uiobjects.actions.CopyFavoriteAction;
import nl.eadmin.ui.uiobjects.tables.DetailDataTable;
import nl.eadmin.ui.uiobjects.tables.HeaderDataTable;
import nl.eadmin.ui.uiobjects.windows.JournaalPostWindow;
import nl.eadmin.util.PrintWebExtension;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.BinaryAction;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.RedirectAction;

public class BoekingAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 8782355121966110309L;
	public static final String PAGE = BoekingAdapter.class.getName();
	public static final String RESET_ACTION2 = "reset2";
	private Bedrijf bedrijf;
	private String dagboekSoort;
	private int boekjaar;
	private boolean editMode, initialized2;
	private MaintenancePanel headerDataMaintenancePanel;
	private MaintenancePanel detailDataMaintenancePanel;
	private HeaderDataTable headerDataTable;
	private DetailDataTable detailDataTable;
	private HeaderDataManager headerManager;
	private DetailDataManager detailManager;
	private HeaderData headerData;
	private Dagboek dagboek;
	private CollectionTable dagboekTable;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(BoekingAdapter.PAGE)).setMethod(RESET_ACTION2).setIcon("arrow_refresh.png");
	private Action editDetails = new Action("Details").setAdapter(getProcessAdapterPageName(BoekingAdapter.PAGE)).setMethod("editDetails").setIcon("pencil.png");
	private Action favorite = new Action("Favoriet Aan/Uit").setAdapter(getProcessAdapterPageName(BoekingAdapter.PAGE)).setMethod("favoriteOnOff").setIcon("asterisk_yellow.png");
	private Action journaal = new Action("Journaalpost").setAdapter(getProcessAdapterPageName(BoekingAdapter.PAGE)).setMethod("journaalPost").setIcon("sum.png");
	private Action ok = new Action("OK") {
		private static final long serialVersionUID = 1L;

		public boolean execute(DataObject object) throws Exception {
			dagboek = (Dagboek)GeneralHelper.getSelectedItem(dagboekTable, object);
			object.addUIObject(new RedirectAction(getProcessAdapterPageName(BoekingAdapter.PAGE), RESET_ACTION2, getProcess().getProcessId()));
			return super.execute(object);
		}
	}.setIcon("tick.png");

	public BoekingAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		if (dagboek == null) {
			clearScreen(true, false);
			FloatBar fb = new FloatBar();
			fb.addAction(ok);
			fb.addAction(finish);
			object.addUIObject(new Header("Onderhouden Boekingen", Header.TYPE_SCREEN_NAME));
			object.addUIObject(dagboekTable);
			object.addUIObject(fb);
		} else {
			object.addUIObject(new RedirectAction(getProcessAdapterPageName(BoekingAdapter.PAGE), RESET_ACTION2, getProcess().getProcessId()));
		}
		return object;
	}

	public DataObject show2(DataObject object) throws Exception {
		if (!initialized2) {
			initialize2(object);
		}
		clearScreen(true, false);
		object.addUIObject(new Header("Onderhouden Boekingen", Header.TYPE_SCREEN_NAME));
		object.addUIObject(new Header("Dagboek: " + dagboek.getId() + " - " + dagboek.getOmschrijving(), Header.TYPE_SUB_SCREEN_NAME));
		object.addUIObject(headerDataMaintenancePanel);
		object.addUIObject(detailDataMaintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		boekjaar = ((Integer) ESPSessionContext.getSessionAttribute(SessionKeys.BOEKJAAR)).intValue();
		dagboekSoort = (String) ESPSessionContext.getSessionAttribute(SessionKeys.DAGBOEKSOORT);
		headerManager = HeaderDataManagerFactory.getInstance(bedrijf.getDBData());
		detailManager = DetailDataManagerFactory.getInstance(bedrijf.getDBData());
		Collection<?> dagBoeken = GeneralHelper.getDagboeken(bedrijf, dagboekSoort);
		if (dagBoeken.size() == 1) {
			dagboek = (Dagboek) dagBoeken.iterator().next();
		} else {
			dagboek = null;
			dagboekTable = new CollectionTable(Dagboek.class, dagBoeken, ApplicationConstants.NUMBEROFTABLEROWS);
			dagboekTable.setName("Bijlagen");
			dagboekTable.setColumnNames(new String[] { Dagboek.ID, Dagboek.OMSCHRIJVING });
			dagboekTable.setColumnLabels(new String[] { "Dagboek", "Omschrijving" });
			dagboekTable.setColumnSizes(new short[] { 100, 300 });
			dagboekTable.setSingleSelectable();
			Action showAction = new Action("OK") {
				private static final long serialVersionUID = 1L;

				public boolean execute(DataObject object) throws Exception {
					dagboek = (Dagboek) GeneralHelper.getSelectedItem(dagboekTable, object);
					object.addUIObject(new RedirectAction(getProcessAdapterPageName(BoekingAdapter.PAGE), RESET_ACTION2, getProcess().getProcessId()));
					return super.execute(object);
				}
			};
			dagboekTable.setRowAction(showAction);
			dagboekTable.reload();
		}
		initialized = true;
	}

	public DataObject reset2(DataObject object) throws Exception {
		initialized2 = false;
		return show2(object);
	}

	private void initialize2(DataObject object) throws Exception {
		editMode = PeriodeHelper.isOpen(bedrijf, boekjaar) && !dagboek.getSystemValue();
		headerDataTable = new HeaderDataTable(new HeaderDataReader(dagboek), bedrijf, dagboek, boekjaar);
		ChangeLayoutAction changeLayout = new ChangeLayoutAction(bedrijf, headerDataTable);
		TableHelper.createMenu(headerDataTable, new Action[] { refresh }, new Action[] { journaal, (dagboek.isInkoopboek() ? favorite : null), (dagboek.isVerkoopboek() ? changeLayout : null) });
		headerDataTable.setRowAction(dagboek.isKasboek() ? null : editDetails);
		headerDataMaintenancePanel = new MyMaintenancePanel(HeaderData.class, "Boeking", "Boekingen", headerDataTable, new HeaderDataEditor(bedrijf, dagboek, headerManager), finish) {
			private static final long serialVersionUID = -4964612315898619799L;

			protected void handleAction(DataObject object, int action) throws Exception {
				if (action == ACTION_ADD && detailDataMaintenancePanel != null) {
					detailDataMaintenancePanel.setHidden(true);
					detailDataMaintenancePanel.setModified();
				}
				super.handleAction(object, action);
			}
		};
		Action copyFavorite = new CopyFavoriteAction(null, bedrijf, dagboek);
		headerDataMaintenancePanel.setMainTopPanel(headerDataTable.getSearchPanel());
		headerDataMaintenancePanel.setCreateEnabled(editMode && !dagboek.isVerkoopboek());
		headerDataMaintenancePanel.setEditEnabled(editMode);
		headerDataMaintenancePanel.setDeleteEnabled(editMode);
		headerDataMaintenancePanel.addActionForMainButtonBar(journaal);
		if (dagboek.isInkoopboek()) {
			headerDataMaintenancePanel.addActionForMainButtonBar(favorite);
			headerDataMaintenancePanel.addActionForCreateButtonBar(copyFavorite);
		}
		if (dagboek.isVerkoopboek()) {
			headerDataMaintenancePanel.addActionForMainButtonBar(changeLayout);
			headerDataMaintenancePanel.addActionForMainButtonBar(/* print= */new PrintAction());
			headerDataMaintenancePanel.addActionForMainButtonBar(/* download= */new BinaryDownloadAction());
		}
		headerDataMaintenancePanel.setUseWindows(true);
		headerDataMaintenancePanel.setHeaderForMainScreen(null);
		detailDataMaintenancePanel = null;
		initialized2 = true;
	}

	public DataObject editDetails(DataObject object) throws Exception {
		headerData = (HeaderData) GeneralHelper.getSelectedItem(headerDataTable, object);
		headerDataTable.unSelect(headerData);

		detailDataTable = new DetailDataTable(new DetailDataReader(dagboek), bedrijf, dagboek, headerData);
		TableHelper.createMenu(detailDataTable, new Action[] { refresh }, new Action[] {});
		detailDataMaintenancePanel = new MyMaintenancePanel(DetailData.class, "Boekingsregel", "Boekingsregels", detailDataTable, new DetailDataEditor(bedrijf, dagboek, headerData, headerDataTable,
			detailManager), null);
		detailDataMaintenancePanel.setCreateEnabled(editMode);
		detailDataMaintenancePanel.setEditEnabled(editMode);
		detailDataMaintenancePanel.setDeleteEnabled(editMode);
		detailDataMaintenancePanel.setUseWindows(true);
		detailDataMaintenancePanel.setHeaderForMainScreen(new Header("Boekingsregels", Header.TYPE_SUB_SCREEN_NAME));
		return show2(object);
	}

	public DataObject favoriteOnOff(DataObject object) throws Exception {
		headerData = (HeaderData) GeneralHelper.getSelectedItem(headerDataTable, object);
		headerDataTable.unSelect(headerData);
		headerData.setFavoriet(!headerData.getFavoriet());
		headerDataTable.reloadPage();
		return object;
	}

	public DataObject journaalPost(DataObject object) throws Exception {
		headerData = (HeaderData) GeneralHelper.getSelectedItem(headerDataTable, object);
		headerDataTable.unSelect(headerData);
		object.addUIObject(new JournaalPostWindow(headerData.getBedrijf(), headerData.getDagboekId(), headerData.getBoekstuk()));
		return object;
	}

	private BinaryObject downloadPDF(DataObject object) throws Exception {
		headerData = (HeaderData) GeneralHelper.getSelectedItem(headerDataTable, object);
		headerDataTable.unSelect(headerData);
		FactuurDataBean factuurDataBean = new FactuurDataBean(headerData, getFactuurLayout());
		String factuurNummer = factuurDataBean.getFactuurNr();
		byte[] bytes = PDFHelper.getFactuurPDF(factuurDataBean);
		BinaryObject obj = new BinaryObject(bytes, BinaryObject.TYPE_PDF, object);
		obj.setName("Factuur_" + factuurNummer + ".pdf");
		return obj;
	}

	public DataObject openPrintDialog(DataObject object) throws Exception {
		headerData = (HeaderData) GeneralHelper.getSelectedItem(headerDataTable, object);
		headerDataTable.unSelect(headerData);
		FactuurDataBean factuurDataBean = new FactuurDataBean(headerData, getFactuurLayout());
		String html = HTMLHelper.getFactuurHTML(factuurDataBean);
		String name = "Factuur_" + factuurDataBean.getFactuurNr();
		object.addUIObject(new PrintWebExtension(object.getScreen(), name, html));
		return object;
	}

	private HtmlTemplate getFactuurLayout() throws Exception {
		HtmlTemplatePK key = new HtmlTemplatePK();
		key.setBedrijf(headerData.getBedrijf());
		key.setId(headerData.getFactuurLayout());
		try {
			return HtmlTemplateManagerFactory.getInstance(bedrijf.getDBData()).findByPrimaryKey(key);
		} catch (Exception e) {
			HtmlTemplate htmlTemplate = HtmlTemplateHelper.getStdLayout(bedrijf);
			headerData.setFactuurLayout(htmlTemplate.getId());
			return htmlTemplate;
		}
	}

	private class BinaryDownloadAction extends BinaryAction {
		private static final long serialVersionUID = 1L;

		public BinaryDownloadAction() throws Exception {
			super("Opslaan als PDF");
			setIcon("page_white_acrobat.png");
		}

		public BinaryObject getBinaryObject(DataObject object) throws Exception {
			return downloadPDF(object);
		}
	}

	private class PrintAction extends Action {
		private static final long serialVersionUID = 1L;

		public PrintAction() {
			super("Afdrukken Factuur");
			setIcon("printer.png");
		}

		public boolean execute(DataObject object) throws Exception {
			openPrintDialog(object);
			return true;
		};
	}
}