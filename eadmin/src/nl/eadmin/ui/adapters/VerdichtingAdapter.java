package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Verdichting;
import nl.eadmin.db.VerdichtingManager;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.editors.VerdichtingEditor;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.DefaultEditor;

public class VerdichtingAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 6791936282314889748L;
	public static final String PAGE = VerdichtingAdapter.class.getName();
	private static final String[] NAMES = new String[] { Verdichting.ID, Verdichting.OMSCHRIJVING, Verdichting.HOOFDVERDICHTING, Verdichting.HOOFDVERDICHTING_OMSCHR, Verdichting.REKENING_SOORT };
	private static final String[] LABELS = new String[] { "Id", "Omschrijving", "HfdVerd.", "HfdVerd.Omschrijving", "Rekeningsoort" };
	private static final short[] SIZES = new short[] { 80, 250, 50, 200, 100 };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true, false, false };
	private MaintenancePanel maintenancePanel;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public VerdichtingAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		VerdichtingManager manager = VerdichtingManagerFactory.getInstance(bedrijf.getDBData());
		ODBTable table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(Verdichting.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("VerdichtingAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("Verdichtingen");
		table.addDownloadPDFContextAction("Verdichtingen");
		TableHelper.createMenu(table, new Action[] { refresh }, null);

		DefaultEditor editor = new VerdichtingEditor(bedrijf, manager);
		maintenancePanel = new MaintenancePanel(Verdichting.class, "Verdichting", "Verdichtingen", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		initialized = true;
	}
}