package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class SluitBoekjaarProcess extends ProjectProcess {
	private static final long serialVersionUID = -8079397642359012861L;
	public static final String PAGE = SluitBoekjaarProcess.class.getName();
	public static final String START_PAGE = SluitBoekjaarAdapter.PAGE;
	public static final String START_ACTION = SluitBoekjaarAdapter.START_ACTION;

	public SluitBoekjaarProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new SluitBoekjaarAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Afsluiten boekjaar";
	}

	public String getShortDescription() {
		return "Afsluiten boekjaar";
	}

	public String getProcessIconUrl() {
		return "date.png";
	}
}