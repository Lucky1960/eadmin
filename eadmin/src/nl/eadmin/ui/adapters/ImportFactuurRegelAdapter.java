package nl.eadmin.ui.adapters;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeManager;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.BtwCodePK;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.DebiteurManager;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.DetailDataDataBean;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.RekeningPK;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.uiobjects.referencefields.DagboekReferenceField;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.FileSelectionField;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.TextArea;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.jsql.DBData;

public class ImportFactuurRegelAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 8414775573117047536L;
	public static final String PAGE = ImportFactuurRegelAdapter.class.getName();
	private String EOL = ApplicationConstants.EOL;
	private BigDecimal ZERO = ApplicationConstants.ZERO;
	private ComboBox separator;
	private String[] voorloopRecord, record;
	private Bedrijf bedrijf;
	private DBData dbd;
	private DagboekReferenceField refDagboek;
	private FileSelectionField filename = new FileSelectionField("Importbestand");
	private FloatBar fb1, fb2;
	private Window window;
	private StringBuilder report;

	private boolean doAppend = true, errorDetail = false;
	private String debitNr, omschr1, omschr2, omschr3, eenheid;
	private BigDecimal aantal = ZERO, prijs = ZERO, bedragBtw = ZERO, bedragExcl = ZERO, bedragIncl = ZERO;
	private int rekLengte;
	private Date levDtm = null;
	private BtwCode btwCode;
	private BtwCodePK btwKey = new BtwCodePK();
	private BtwCodeManager btwMgr;
	private DebiteurPK debKey = new DebiteurPK();
	private DebiteurManager debMgr;
	private Rekening rekening;
	private RekeningPK rekKey = new RekeningPK();
	private RekeningManager rekMgr;
	{
		separator = new ComboBox("");
		separator.setDiscardLabel(true);
		separator.setWidth("150");
		separator.addOption("Puntkomma (;)", ";");
		separator.addOption("Komma (,)", ",");
	}

	public ImportFactuurRegelAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(new Header("Importeren Factuurregels", Header.TYPE_SCREEN_NAME));
		FieldGroup fg = new FieldGroup();
		fg.add(refDagboek);
		fg.add(filename);
		fg.add(separator);
		object.addUIObject(fg);
		object.addUIObject(fb1);
		return object;
	}

	public DataObject show2(DataObject object) throws Exception {
		try {
			report = new StringBuilder();
			append("==================================================");
			append("Importverslag van bestand " + filename.getFileName());
			append("==================================================");

			try {
				validateFile();
				if (record.length == 1) {
					append(" (Bestand is leeg) ");
				} else {
					boolean succes = !importeer(false);
					if (succes) {
						importeer(true);
						append(" (Geen fouten: alle records ge�mporteerd)");
					} else {
						append(" FOUTEN geconstateerd !!! ");
					}

				}
			} catch (Exception e) {
				append(e.getMessage());
			}
			append("*** Einde verslag ***");
		} catch (Exception e) {
		}

		window = new Window();
		window.setLabel("Importeren Factuurregels");
		TextArea txt = new TextArea();
		txt.setValueAsString(report.toString());
		txt.setDiscardLabel(true);
		txt.setLength(80);
		txt.setHeight(15);
		Panel panel = new Panel();
		panel.addUIObject(txt);
		panel.addUIObject(fb2);
		window.add(panel);
		object.addUIObject(window);
		return object;
	}

	private void validateFile() throws Exception {
		byte[] b = filename.getFileContent();
		boolean validFile = (b != null && b.length > 0);
		if (validFile) {
			String importString = new String(b);
			record = importString.split(ApplicationConstants.EOL);
			voorloopRecord = getVoorloopRecord();
			String[] splitValues = record[0].split(separator.getValue());
			validFile = (splitValues.length == voorloopRecord.length);
			int ix = 0;
			while (validFile && ix < splitValues.length) {
				validFile = (splitValues[ix].replace('"', ' ').trim().toLowerCase().equals(voorloopRecord[ix].trim().toLowerCase()));
				ix++;
			}
		}
		if (!validFile) {
			throw new UserMessageException("'" + filename.getFileName() + "' is geen geldig importbestand" + EOL);
		}
	}

	private boolean importeer(boolean doWrite) throws Exception {
		boolean err = false;
		String dagboekId = ((Dagboek) refDagboek.getSelectedObject()).getId();
		DetailDataManager mgr = DetailDataManagerFactory.getInstance(dbd);
		DetailDataDataBean bean = new DetailDataDataBean();
		String[] splitValues;
		for (int r = 1; r < record.length; r++) {
			errorDetail = false;
			append(record[r]);
			splitValues = convertToStringArray(record[r], separator.getValue().toCharArray()[0]);
			if (splitValues.length != voorloopRecord.length) {
				append(" - Onjuist aantal rubrieken ");
				errorDetail = true;
				continue;
			}
			debitNr = convertToDebiteur(splitValues[0]);
			levDtm = convertToDate(splitValues[1]);
			aantal = convertToDecimal(splitValues[2], 2);
			eenheid = convertToString(splitValues[3], 10, false);
			prijs = convertToDecimal(splitValues[4], 2);
			btwCode = convertToBtwCode(splitValues[5]);
			rekening = convertToRekening(splitValues[6]);
			omschr1 = convertToString(splitValues[7], 50, false);
			omschr2 = convertToString(splitValues[8], 50, false);
			omschr3 = convertToString(splitValues[9], 50, false);
			calculate(aantal, prijs, btwCode);
			if (doWrite && !errorDetail) {
				bean.setAantal(aantal);
				bean.setBedragBtw(bedragBtw);
				bean.setBedragExcl(bedragExcl);
				bean.setBedragIncl(bedragIncl);
				bean.setBedragDebet(ZERO);
				bean.setBedragCredit(bedragIncl);
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setBoekstuk("");
				bean.setBoekstukRegel(GeneralHelper.genereerNieuwBoekstukRegelNummer(bedrijf.getDBData(), bedrijf.getBedrijfscode(), dagboekId, ""));
				bean.setBtwCode(btwCode.getCode());
				bean.setDagboekId(dagboekId);
				bean.setDatumLevering(levDtm);
				bean.setDC("C");
				bean.setDcNummer(debitNr);
				bean.setEenheid(eenheid);
				bean.setFactuurNummer("");
				bean.setOmschr1(omschr1);
				bean.setOmschr2(omschr2);
				bean.setOmschr3(omschr3);
				bean.setPrijs(prijs);
				bean.setRekening(rekening.getRekeningNr());
				bean.setVrijeRub1("");
				bean.setVrijeRub2("");
				bean.setVrijeRub3("");
				bean.setVrijeRub4("");
				bean.setVrijeRub5("");
				mgr.create(bean);
			}
			if (errorDetail) {
				err = true;
			}
		}
		return err;
	}

	private void append(String str) throws Exception {
		if (doAppend)
			report.append(str + EOL);
	}

	private String[] convertToStringArray(String str, char separator) throws Exception {
		str = str.replaceAll(new String(new char[] { separator }), new String(new char[] { separator, ' ' }));
		char[] c = str.toCharArray();
		boolean quoted = false;
		for (int i = 0; i < c.length; i++) {
			if (c[i] == 34) {
				quoted = !quoted;
			}
			if (c[i] == separator && !quoted) {
				c[i] = 254;
			}
		}
		String[] val = new String(c).split(new String(new char[] { 254 }));
		for (int i = 0; i < val.length; i++) {
			val[i] = val[i].replace('"', ' ').trim();
		}
		return val;
	}

	private String convertToString(String str, int len, boolean toUpperCase) throws Exception {
		str = str.trim();
		if (len > 0 && str.length() > len) {
			str = str.substring(0, len);
			append(" - De lengte van de opgegeven string is groter dan de maximumlengte van " + len);
			errorDetail = true;
		}
		if (toUpperCase) {
			str = str.toUpperCase();
		}
		return str;
	}

	private BigDecimal convertToDecimal(String amt, int scale) throws Exception {
		BigDecimal dec = GeneralHelper.getBigDecimalFromString(amt, scale);
		if (dec.doubleValue() == 0d && amt.length() > 0) {
			append(" - Fout in aantal of prijs: " + amt + " (Gewenste opmaak: -0.00 of -0,00)");
			errorDetail = true;
		}
		return dec;
	}

	private Date convertToDate(String s) throws Exception {
		Date date = DateHelper.stringToDate(s);
		if (s.length() > 0 && date == null) {
			append(" - Fout in datum: " + s);
			errorDetail = true;
		}
		return date;
	}

	private String convertToDebiteur(String debNr) throws Exception {
		debKey.setDebNr(debNr);
		try {
			debMgr.findByPrimaryKey(debKey).getDebNr();
		} catch (Exception e) {
			append(" - Ongeldige debiteur: " + debNr);
			errorDetail = true;
		}
		return debNr;
	}

	private BtwCode convertToBtwCode(String s) throws Exception {
		btwKey.setCode(s.toUpperCase());
		try {
			return btwMgr.findByPrimaryKey(btwKey);
		} catch (Exception e) {
			append(" - Ongeldige BTW-code: " + s);
			errorDetail = true;
			return null;
		}
	}

	private Rekening convertToRekening(String s) throws Exception {
		String rekNr = "0000000000" + s;
		rekNr = rekNr.substring(rekNr.length() - rekLengte);
		rekKey.setRekeningNr(rekNr);
		try {
			return rekMgr.findByPrimaryKey(rekKey);
		} catch (Exception e) {
			append(" - Ongeldig rekeningnummer: " + s);
			errorDetail = true;
			return null;
		}
	}

	private void initialize(DataObject object) throws Exception {
		bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		dbd = bedrijf.getDBData();
		rekMgr = RekeningManagerFactory.getInstance(dbd);
		rekLengte = bedrijf.getInstellingen().getLengteRekening();
		btwKey.setBedrijf(bedrijf.getBedrijfscode());
		debKey.setBedrijf(bedrijf.getBedrijfscode());
		rekKey.setBedrijf(bedrijf.getBedrijfscode());
		refDagboek = new DagboekReferenceField("Verkoopboek", bedrijf, DagboekManagerFactory.getInstance(dbd), DagboekSoortEnum.VERKOOP);
		refDagboek.hideIfOnlyOneOption(Dagboek.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Dagboek.SOORT + "='" + DagboekSoortEnum.VERKOOP + "'");
		refDagboek.setMandatory(true);
		btwMgr = BtwCodeManagerFactory.getInstance(dbd);
		debMgr = DebiteurManagerFactory.getInstance(dbd);
		rekMgr = RekeningManagerFactory.getInstance(dbd);

		filename.setMandatory(true);
		Action ok1 = new Action("OK").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("show2").setIcon("tick.png");
		ok1.setDefault(true);
		Action ok2 = new Action("OK").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("finish").setIcon("tick.png");
		fb1 = new FloatBar();
		fb1.addAction(ok1);
		fb1.addAction(finish);
		fb2 = new FloatBar();
		fb2.addAction(ok2);
		initialized = true;
	}

	protected String[] getVoorloopRecord() throws Exception {
		return new String[] { "DebiteurNr", "LeverDtm", "Aantal", "Eenheid", "Prijs", "BtwCode", "GrootboekRek", "Omschr1", "Omschr2", "Omschr3" };
	}

	protected String[] getRecordMask() throws Exception {
		return new String[] { "A(10)", "A(15)", "A(10)", "A(15)", "D(10)", "A(50)", "A(50)", "A(50)", "D(5,2)", "A(10)", "D(15,2)", "A(3)", "D(15,2)", "D(15,2)", "D(15,2)", "A(1)", "A(9)" };
	}

	private void calculate(BigDecimal aantal, BigDecimal prijs, BtwCode btwCode) throws Exception {
		BigDecimal bedrag = aantal.multiply(prijs).setScale(2);
		double percentage = btwCode == null ? 0d : btwCode.getPercentage().doubleValue();
		if (percentage == 0d) {
			bedragBtw = ZERO;
			bedragExcl = bedrag;
			bedragIncl = bedrag;
		} else {
			if (btwCode.getInEx().equals("E")) {
				bedragExcl = bedrag;
				bedragBtw = bedrag.multiply(new BigDecimal(percentage / 100)).setScale(2, BigDecimal.ROUND_HALF_UP);
				bedragIncl = new BigDecimal(bedragExcl.doubleValue() + bedragBtw.doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
			} else {
				bedragIncl = bedrag;
				bedragExcl = bedrag.divide(new BigDecimal(100 + percentage), BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100.00));
				bedragExcl = (bedrag.multiply(new BigDecimal(100.00)).divide(new BigDecimal(100 + percentage), BigDecimal.ROUND_HALF_UP));
				bedragBtw = new BigDecimal(bedragIncl.doubleValue() - bedragExcl.doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
			}
		}
	}
}