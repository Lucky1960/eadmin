package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class DagboekProcess extends ProjectProcess {
	private static final long serialVersionUID = 8451296867791971797L;
	public static final String PAGE = DagboekProcess.class.getName();
	public static final String START_PAGE = DagboekAdapter.PAGE;
	public static final String START_ACTION = DagboekAdapter.START_ACTION;

	public DagboekProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new DagboekAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Dagboeken";
	}

	public String getShortDescription() {
		return "Dagboeken";
	}

	public String getProcessIconUrl() {
		return "application_cascade.png";
	}
}