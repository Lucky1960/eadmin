package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Autorisatie;
import nl.eadmin.db.AutorisatieManager;
import nl.eadmin.db.AutorisatieManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.datareaders.AutorisatieDataReader;
import nl.eadmin.ui.editors.AutorisatieEditor;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.DefaultEditor;

public class AutorisatieAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 8782355121966110309L;
	public static final String PAGE = AutorisatieAdapter.class.getName();
	private static final String[] NAMES = new String[] { Autorisatie.GEBRUIKER_ID, AutorisatieDataReader.GEBRUIKER_NAAM };
	private static final String[] LABELS = new String[] { "Gebruiker", "Naam" };
	private static final short[] SIZES = new short[] { 150, 300 };
	private static final boolean[] SORTABLE = new boolean[] { true, false };
	private MaintenancePanel maintenancePanel;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public AutorisatieAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		Bedrijf bedrijf  = (Bedrijf)ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		AutorisatieManager manager = AutorisatieManagerFactory.getInstance(bedrijf.getDBData());
		ODBTable table = new ODBTable(manager, new AutorisatieDataReader(), ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(Autorisatie.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("AutorisatieAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("Autorisaties");
		table.addDownloadPDFContextAction("Autorisaties");
		TableHelper.createMenu(table, new Action[] { refresh }, null);

		DefaultEditor editor = new AutorisatieEditor(manager, bedrijf);
		String txt = "Autorisaties voor administratie '" + bedrijf.getOmschrijvingLang() + "'";
		maintenancePanel = new MaintenancePanel(Autorisatie.class, txt, txt, table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		initialized = true;
	}
}