package nl.eadmin.ui.adapters;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.helpers.BoekingHelper;
import nl.eadmin.helpers.PeriodeHelper;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.Panel;

public class SluitBoekjaarAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = -665618379622610564L;
	public static final String PAGE = SluitBoekjaarAdapter.class.getName();
	private Bedrijf bedrijf;
	private Label lblBookYearToClose, lblBookYearToOpen;
	private NumberField fldBookYearToClose, fldBookYearToOpen;
	private Action startClose = new Action("Start").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("close").setConfirmationMessage("Weet u het zeker?");
	private Action startOpen = new Action("Start").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("open").setConfirmationMessage("Weet u het zeker?");
	private Label lblEmpty = new Label("Afsluiten of heropenen niet mogelijk want er zijn nog geen boekingen gemaakt");
	private boolean isEmpty;

	public SluitBoekjaarAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(new Header("Afsluiten boekjaar", Header.TYPE_SCREEN_NAME));
		if (isEmpty) {
			object.addUIObject(lblEmpty);
		} else {
			ESPGridLayout grid = new ESPGridLayout();
			grid.setColumnWidths(new short[] { 160, 80, 100 });
			grid.add(lblBookYearToClose, 0, 0);
			grid.add(fldBookYearToClose, 0, 1);
			grid.add(startClose, 0, 2);
			grid.add(lblBookYearToOpen, 1, 0);
			grid.add(fldBookYearToOpen, 1, 1);
			grid.add(startOpen, 1, 2);
			Panel panel = new Panel("");
			panel.addUIObject(grid);
			object.addUIObject(panel);
		}
		FloatBar fb = new FloatBar();
		fb.addAction(finish);
		object.addUIObject(fb);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		int year1 = PeriodeHelper.getFirstOpenBookYear(bedrijf);
		int year2 = PeriodeHelper.getLastClosedBookYear(bedrijf);
		isEmpty = (year1 == 0 && year2 == 0);
		lblBookYearToClose = new Label("Af te sluiten boekjaar");
		lblBookYearToClose.setHidden(year1 == 0);
		fldBookYearToClose = new NumberField("", 4, false);
		fldBookYearToClose.setDiscardLabel(true);
		fldBookYearToClose.setReadonly(true);
		fldBookYearToClose.setValue(year1);
		fldBookYearToClose.setHidden(year1 == 0);
		startClose.setHidden(year1 == 0);
		lblBookYearToOpen = new Label("Heropenen boekjaar");
		lblBookYearToOpen.setHidden(year2 == 0);
		fldBookYearToOpen = new NumberField("", 4, false);
		fldBookYearToOpen.setDiscardLabel(true);
		fldBookYearToOpen.setReadonly(true);
		fldBookYearToOpen.setValue(year2);
		fldBookYearToOpen.setHidden(year2 == 0);
		startOpen.setHidden(year2 == 0);
		lblEmpty.setCSSClass("journaalPost");
		initialized = true;
	}

	public DataObject close(DataObject object) throws Exception {
		BoekingHelper.closeBookYear(bedrijf, fldBookYearToClose.getIntValue());
		return reset(object);
	}

	public DataObject open(DataObject object) throws Exception {
		BoekingHelper.openBookYear(bedrijf, fldBookYearToOpen.getIntValue());
		return reset(object);
	}
}