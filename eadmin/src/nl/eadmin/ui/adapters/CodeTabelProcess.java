package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class CodeTabelProcess extends ProjectProcess {
	private static final long serialVersionUID = 7756486149770293369L;
	public static final String PAGE = CodeTabelProcess.class.getName();
	public static final String START_PAGE = CodeTabelAdapter.PAGE;
	public static final String START_ACTION = CodeTabelAdapter.START_ACTION;

	public CodeTabelProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new CodeTabelAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Codetabellen";
	}

	public String getShortDescription() {
		return "Codetabellen";
	}

	public String getProcessIconUrl() {
		return "application_view_columns.png";
	}
}