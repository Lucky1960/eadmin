package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class MainProcess extends ProjectProcess {
	private static final long serialVersionUID = -3257898888666411591L;
	public static final String PAGE = MainProcess.class.getName();
	public static final String START_PAGE = MainAdapter.PAGE;
	public static final String START_ACTION = MainAdapter.START_ACTION;

	public MainProcess(DataObject object) throws Exception {
		super(object, PAGE);
		object.setSessionAttribute(ApplicationConstants.MAIN_ESP_PROCESS_ID, getESPProcessId());
		addAdapter(new MainAdapter(this, object));
		//addAdapter(new SelecteerBedrijfAdapter(this, object));
		//addAdapter(new OpdrachtStatusAdapter(this, object));
		//addAdapter(new SelecteerKlantAdapter(this, object));
		//addAdapter(new KlantkaartAdapter(this, object));
		setHidden(true);
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}
	
	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return this.translate("Klantkaart");
	}

	public String getShortDescription() {
		return "Klantkaart";
	}

	public String getProcessIconUrl() {
		return "group.png";
	}
}