package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class EnvironmentProcess extends ProjectProcess {
	private static final long serialVersionUID = 2431818226375119175L;
	public static final String PAGE = EnvironmentProcess.class.getName();
	public static final String START_PAGE = EnvironmentAdapter.PAGE;
	public static final String START_ACTION = EnvironmentAdapter.START_ACTION;

	public EnvironmentProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new EnvironmentAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return this.translate("Omgevingen");
	}

	public String getShortDescription() {
		return "Omgevingen";
	}
}