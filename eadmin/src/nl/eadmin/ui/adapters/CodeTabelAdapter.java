package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.CodeTabel;
import nl.eadmin.db.CodeTabelElement;
import nl.eadmin.db.CodeTabelElementManager;
import nl.eadmin.db.CodeTabelElementManagerFactory;
import nl.eadmin.db.CodeTabelManager;
import nl.eadmin.db.CodeTabelManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.editors.CodeTabelEditor;
import nl.eadmin.ui.editors.CodeTabelElementEditor;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.esp.util.DefaultEditor;

public class CodeTabelAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = -8765516603242933085L;
	public static final String PAGE = CodeTabelAdapter.class.getName();
	private static final String[] NAMES = new String[] { CodeTabel.TABEL_NAAM, CodeTabel.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Naam", "Omschrijving" };
	private static final short[] SIZES = new short[] { 100, 300 };
	private static final boolean[] SORTABLE = new boolean[] { true, true };
	private MaintenancePanel maintenancePanel;
	private Bedrijf bedrijf;
	private ODBTable table;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");
	private Action editCodes = new Action("Codes").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("editCodes").setIcon("flag_green.png");

	public CodeTabelAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		bedrijf = (Bedrijf)ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		CodeTabelManager manager = CodeTabelManagerFactory.getInstance(bedrijf.getDBData());
		table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(CodeTabel.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("CodeTabelAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("CodeTabellen");
		table.addDownloadPDFContextAction("CodeTabellen");
		TableHelper.createMenu(table, new Action[] { refresh }, new Action[] {editCodes});

		DefaultEditor editor = new CodeTabelEditor(manager);
		maintenancePanel = new MaintenancePanel(CodeTabel.class, "CodeTabel", "CodeTabellen", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		maintenancePanel.addActionForMainButtonBar(editCodes);
		initialized = true;
	}

	public DataObject editCodes(DataObject object) throws Exception {
		CodeTabel codeTabel = (CodeTabel) GeneralHelper.getSelectedItem(table, object);
		table.unSelect(codeTabel);
		String[] NAMES2 = new String[] { CodeTabelElement.ID, CodeTabelElement.OMSCHRIJVING };
		String[] LABELS2 = new String[] { "Code", "Omschrijving" };
		short[] SIZES2 = new short[] { 100, 300 };
		CodeTabelElementManager manager2 = CodeTabelElementManagerFactory.getInstance(bedrijf.getDBData());
		ODBTable table2 = new ODBTable(manager2, ApplicationConstants.NUMBEROFTABLEROWS);
		table2.setFilter(CodeTabelElement.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + CodeTabelElement.TABEL_NAAM + "='" + codeTabel.getTabelNaam() + "' ", null);
		table2.setName("CodeTabelElementAdapter");
		table2.setColumnNames(NAMES2);
		table2.setColumnLabels(LABELS2);
		table2.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table2.setColumnSizes(SIZES2);
		table2.setMultipleSelectable();
		table2.addDownloadCSVContextAction("CodeTabellen");
		table2.addDownloadPDFContextAction("CodeTabellen");
		TableHelper.createMenu(table2, new Action[] { refresh }, null);

		CodeTabelElementEditor editor2 = new CodeTabelElementEditor(manager2, codeTabel);
		MaintenancePanel maintenancePanel2 = new MaintenancePanel(CodeTabelElement.class, "Code", "Codes", table2, editor2, back);
		maintenancePanel2.setMainTopPanel(table2.createSearchWithFilterPanel(NAMES2, LABELS2, NAMES2, LABELS2, true));
		maintenancePanel2.setUseWindows(true);
		maintenancePanel2.setShowHeaders(false);
		Window window = new Window();
		window.setLabel("Onderhouden codes van tabel '" + codeTabel.getTabelNaam() + "'");
		Panel panel = new Panel();
		panel.addUIObject(maintenancePanel2);
		window.add(panel);
		object.addUIObject(window);
		return object;
	}
}