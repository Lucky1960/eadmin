package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class BtwCodeProcess extends ProjectProcess {
	private static final long serialVersionUID = 3046915450264804360L;
	public static final String PAGE = BtwCodeProcess.class.getName();
	public static final String START_PAGE = BtwCodeAdapter.PAGE;
	public static final String START_ACTION = BtwCodeAdapter.START_ACTION;

	public BtwCodeProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new BtwCodeAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "BTW-codes";
	}

	public String getShortDescription() {
		return "BTW-codes";
	}

	public String getProcessIconUrl() {
		return "sum.png";
	}
}