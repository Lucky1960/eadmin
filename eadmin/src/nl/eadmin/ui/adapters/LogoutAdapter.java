package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.Adapter;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.renderer.JavaScriptInstructionSet;
import nl.ibs.esp.session.ActiveUsers;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.LoginPanel;

public class LogoutAdapter extends Adapter {

	private static final long serialVersionUID = -8240892015866618931L;
	public static String PAGE = LogoutAdapter.class.getName();
	public static final String LOGOUT = "logout";

	public LogoutAdapter(DataObject object) throws Exception {
		super(object);
		showTaskBar(false);
		setHidden(true);
		object.setApplicationIdentifyingString("");
	}

	/**
	 * logs out, invalidating the session.
	 */
	public DataObject logout(DataObject object) throws Exception {
		//see also: LoginAdapter.newLogin()
		killOtherProcesses();
		ActiveUsers.removeUser(object);

		clearScreen(false, false);
		LoginPanel panel = new LoginPanel();
		panel.setHeader(new Header("Label.SignedOff", Header.TYPE_SCREEN_NAME));
		object.addUIObject(panel);

		logoffCompleted();
		JavaScriptInstructionSet.addInstruction("setTimeout(closeTop, 1000);");
		return object;
	}

	public String getDefaultAction() {
		return LOGOUT;
	}
}
