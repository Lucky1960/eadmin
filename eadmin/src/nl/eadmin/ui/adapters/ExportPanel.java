package nl.eadmin.ui.adapters;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.enums.ImportTypeEnum;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.HeaderPanel;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;

public class ExportPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private CheckBox[] checkBox;
	private Bedrijf bedrijf;

	public ExportPanel(Bedrijf bedrijf) throws Exception {
		this.bedrijf = bedrijf;
		initialize();
	}

	public Bedrijf getBedrijf() throws Exception {
		return bedrijf;
	}

	private void initialize() throws Exception {
		Panel selection = new Panel("Export-gegevens");
		ESPGridLayout grid = new ESPGridLayout();
		selection.addUIObject(grid);
		int nr = ImportTypeEnum.getCollection().size();
		checkBox = new CheckBox[nr];
		for (int i = 0; i < nr; i++) {
			checkBox[i] = new CheckBox(ImportTypeEnum.getValue(i));
			checkBox[i].setHidden(false);
			checkBox[i].setValue(true);
			checkBox[i].setOrientationCheckBFirst();
			checkBox[i].setHelpId("ExportType");
			grid.add(checkBox[i], i, 0);
		}
		Field bedrijfOmschr = new Field("Bedrijf", Field.TYPE_TEXT, null, bedrijf.getOmschrijving(), 50);
		bedrijfOmschr.setReadonly(true);
		HeaderPanel headerPanel = new HeaderPanel();
		headerPanel.addUIObject(bedrijfOmschr);
		addUIObject(headerPanel);
		addUIObject(new Label("\nSelecteer de onderdelen die in het systeembestand moeten worden opgenomen\n\n"));
		addUIObject(selection);
		setValues();
	}

	public void setValues() throws Exception {
//		ExecutableQuery query = null;
//		query = QueryFactory.create(Tolerance.class, Tolerance.MATCH_SOURCE_ID + "=" + matchSource.getMatchSourceId());
//		query.setMaxObjects(1);
//		checkBox[ImportTypeEnum.TOLERANCE].setHidden(query.getCollection().size() == 0);
//		checkBox[ImportTypeEnum.TOLERANCE].setValue(!checkBox[ImportTypeEnum.TOLERANCE].isHidden());
	}

	public boolean getValue(int importType) throws Exception {
		return checkBox[importType].getValueAsBoolean();
	}
}