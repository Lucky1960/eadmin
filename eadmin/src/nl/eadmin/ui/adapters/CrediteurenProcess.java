package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class CrediteurenProcess extends ProjectProcess {
	private static final long serialVersionUID = -1535454462213357802L;
	public static final String PAGE = CrediteurenProcess.class.getName();
	public static final String START_PAGE = CrediteurenAdapter.PAGE;
	public static final String START_ACTION = CrediteurenAdapter.START_ACTION;

	public CrediteurenProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new CrediteurenAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Crediteuren";
	}

	public String getShortDescription() {
		return "Crediteuren";
	}

	public String getProcessIconUrl() {
		return "group_error.png";
	}
}