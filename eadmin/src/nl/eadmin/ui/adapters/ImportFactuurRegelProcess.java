package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class ImportFactuurRegelProcess extends ProjectProcess {
	private static final long serialVersionUID = 4312319874775790259L;
	public static final String PAGE = ImportFactuurRegelProcess.class.getName();
	public static final String START_PAGE = ImportFactuurRegelAdapter.PAGE;
	public static final String START_ACTION = ImportFactuurRegelAdapter.START_ACTION;

	public ImportFactuurRegelProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new ImportFactuurRegelAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Importeren factuurregels";
	}

	public String getShortDescription() {
		return "Importeren fact.rgls";
	}

	public String getProcessIconUrl() {
		return "database_download.png";
	}
}