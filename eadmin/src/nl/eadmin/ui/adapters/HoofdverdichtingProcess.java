package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class HoofdverdichtingProcess extends ProjectProcess {
	private static final long serialVersionUID = -2154238949961407529L;
	public static final String PAGE = HoofdverdichtingProcess.class.getName();
	public static final String START_PAGE = HoofdverdichtingAdapter.PAGE;
	public static final String START_ACTION = HoofdverdichtingAdapter.START_ACTION;

	public HoofdverdichtingProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new HoofdverdichtingAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Hoofdverdichtingen";
	}

	public String getShortDescription() {
		return "Hfdverdichtingen";
	}

	public String getProcessIconUrl() {
		return "arrow_in.png";
	}
}