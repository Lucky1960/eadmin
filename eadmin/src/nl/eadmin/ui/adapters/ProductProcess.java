package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class ProductProcess extends ProjectProcess {
	private static final long serialVersionUID = 117279811872147799L;
	public static final String PAGE = ProductProcess.class.getName();
	public static final String START_PAGE = ProductAdapter.PAGE;
	public static final String START_ACTION = ProductAdapter.START_ACTION;

	public ProductProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new ProductAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Producten";
	}

	public String getShortDescription() {
		return "Producten";
	}

	public String getProcessIconUrl() {
		return "cart.png";
	}
}