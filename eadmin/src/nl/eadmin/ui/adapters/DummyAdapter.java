package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Label;

public class DummyAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 8782355121966110309L;
	public static final String PAGE = DummyAdapter.class.getName();
//	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public DummyAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		clearScreen(true, false);
		object.addUIObject(new Label("Deze functie is nog niet ge�mplementeerd!"));
		object.addUIObject(finish);
		return object;
	}
}