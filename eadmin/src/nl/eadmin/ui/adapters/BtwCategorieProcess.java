package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class BtwCategorieProcess extends ProjectProcess {
	private static final long serialVersionUID = -8773187700062398353L;
	public static final String PAGE = BtwCategorieProcess.class.getName();
	public static final String START_PAGE = BtwCategorieAdapter.PAGE;
	public static final String START_ACTION = BtwCategorieAdapter.START_ACTION;

	public BtwCategorieProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new BtwCategorieAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "BTW-categorieŽn";
	}

	public String getShortDescription() {
		return "BTW-categorieŽn";
	}

	public String getProcessIconUrl() {
		return "sum.png";
	}
}