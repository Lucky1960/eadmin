package nl.eadmin.ui.adapters;

import nl.eadmin.boadapters.EnvironmentBO;
import nl.eadmin.db.Environment;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.query.EnvironmentQuery;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UserErrorMessage;

public class EnvironmentReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = 7414536380636540082L;
	private static final String[] NAMES = new String[] { Environment.DTA_LIB, Environment.DESCRIPTION };
	private static final String[] LABELS = new String[] { "Label.Environment", "Label.Description" };
	private static final short[] SIZES = new short[] { 90, 290 };
	private Environment environment;

	public EnvironmentReferenceField(Gebruiker user) throws Exception {
		super(LABELS[0], TYPE_UPPER, new EnvironmentQuery(user), Environment.class);
		setName(this.getClass().getName());
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(12);
		setMaxLength(10);
		setDescriptionLength(40);
	}

	public void setValue(Environment environment) throws Exception {
		if (environment == null) {
			setDescription("");
			setValue();
			return;
		}
		setValue(environment.getDtaLib());
		setDescription(environment.getDescription());
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			environment = (Environment) getSelectedObject();
			setValue(environment);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		environment = getEnvironment();
		if (environment == null) {
			setInvalidTag();
			throw new UserErrorMessage("key_not_found");
		}
		setDescription(environment.getDescription());
	}

	public Environment getEnvironment() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		return (Environment) EnvironmentBO.get(getValue().trim())[0];
	}
}