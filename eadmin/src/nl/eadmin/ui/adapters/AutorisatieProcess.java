package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class AutorisatieProcess extends ProjectProcess {
	private static final long serialVersionUID = 7758144258802281235L;
	public static final String PAGE = AutorisatieProcess.class.getName();
	public static final String START_PAGE = AutorisatieAdapter.PAGE;
	public static final String START_ACTION = AutorisatieAdapter.START_ACTION;

	public AutorisatieProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new AutorisatieAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Autorisaties";
	}

	public String getShortDescription() {
		return "Autorisaties";
	}

	public String getProcessIconUrl() {
		return "key.png";
	}
}