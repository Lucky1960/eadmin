package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class BedrijfProcess extends ProjectProcess {
	private static final long serialVersionUID = 8906359619823331436L;
	public static final String PAGE = BedrijfProcess.class.getName();
	public static final String START_PAGE = BedrijfAdapter.PAGE;
	public static final String START_ACTION = BedrijfAdapter.START_ACTION;

	public BedrijfProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new BedrijfAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Administraties";
	}

	public String getShortDescription() {
		return "Administraties";
	}

	public String getProcessIconUrl() {
		return "building.png";
	}
}