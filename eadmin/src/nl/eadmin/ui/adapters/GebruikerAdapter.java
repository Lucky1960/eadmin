package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerManager;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.datareaders.GebruikerDataReader;
import nl.eadmin.ui.editors.GebruikerEditor;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.DefaultEditor;

public class GebruikerAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 8782355121966110309L;
	public static final String PAGE = GebruikerAdapter.class.getName();
	private static final String[] NAMES = new String[] { Gebruiker.ID, Gebruiker.NAAM, Gebruiker.BEDRIJF };
	private static final String[] LABELS = new String[] { "Gebruikersnaam", "Omschrijving", "Administratie" };
	private static final short[] SIZES = new short[] { 110, 200, 250 };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true };
	private MaintenancePanel maintenancePanel;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public GebruikerAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		GebruikerManager manager = GebruikerManagerFactory.getInstance();
		ODBTable table = new ODBTable(manager, new GebruikerDataReader(), ApplicationConstants.NUMBEROFTABLEROWS);
		table.setName("GebruikerAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("Gebruikers");
		table.addDownloadPDFContextAction("Gebruikers");
		TableHelper.createMenu(table, new Action[] { refresh }, null);

		DefaultEditor editor = new GebruikerEditor(manager);
		maintenancePanel = new MaintenancePanel(Gebruiker.class, "Gebruiker", "Gebruikers", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		initialized = true;
	}
}