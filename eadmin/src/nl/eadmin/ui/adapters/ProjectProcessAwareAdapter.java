package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.ibs.esp.adapter.Adapter;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.adapter.ProcessAwareAdapter;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.Configuration;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.RedirectAction;
import nl.ibs.jsql.DBData;

public abstract class ProjectProcessAwareAdapter extends ProcessAwareAdapter {
	private static final long serialVersionUID = -8333514075122682818L;
	protected static final String ACTION_FINISH = "finish";
	protected static final String ACTION_CANCEL = "cancel";
	public static final String START_ACTION = "show";
	public static final String RESET_ACTION = "reset";
	private String adapterName = getClass().getName();
	protected boolean isFirstInitialize = true, initialized = false, invokedExternally = false;
	protected DBData dbData = null;

	public ProjectProcessAwareAdapter(ESPProcess process, DataObject object) throws Exception {
		super(process, object);
		initialize(object);
	}

	private void initialize(DataObject object) throws Exception {
		dbData = DBData.getDefaultDBData((ApplicationConstants.APPLICATION));
		showSideBar(false);
	}

	public void setProcessAttribute(String name, Object value) throws Exception {
		((ProjectProcess) getProcess()).setAttribute(name, value);
	}

	public Object getProcessAttribute(String name) throws Exception {
		return ((ProjectProcess) getProcess()).getAttribute(name);
	}

	public DataObject reset(DataObject object) throws Exception {
		initialized = false;
		return show(object);
	}

	public abstract DataObject show(DataObject object) throws Exception;

	protected Action cancel = new Action("Button.Cancel").setAdapter(getProcessAdapterPageName(adapterName)).setMethod(START_ACTION).setIcon("cancel.png");
	protected Action back = new Action("Button.Back").setAdapter(getProcessAdapterPageName(adapterName)).setMethod(START_ACTION).setIcon("arrow_left.png");
	protected Action finish = new Action("Afsluiten").setAdapter(getProcessAdapterPageName(adapterName)).setMethod(FINISH_ACTION).setIcon("cancel.png");

	public DataObject finish(DataObject object) throws Exception {
		ESPProcess process = getProcess();
//		if (process instanceof MatchProcess) {
//			Configuration conf = object.getConfiguration();
//			object.addUIObject(new RedirectAction(conf.getDefaultPage(), conf.getDefaultAction()));
//			return object;
//		} else {
			process.destroyAdapter(object);
//		}
		return showMainPage(object);
	}

	public DataObject showMainPage(DataObject object) throws Exception {
		String id = (String) object.getSessionAttribute(ApplicationConstants.MAIN_ESP_PROCESS_ID);
		if (id != null && id.trim().length() > 0) {
			Adapter main = object.getActiveProcesses().getActiveProcessById(id);
			if (main != null) {
				return main.resume(object);
			}
		}
//		setMenuBar(object, new MainMenuBar());
		Configuration conf = object.getConfiguration();
		object.addUIObject(new RedirectAction(conf.getDefaultPage(), conf.getDefaultAction()));
		return object;
	}
}