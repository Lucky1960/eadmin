package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekManager;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.datareaders.DagboekDataReader;
import nl.eadmin.ui.editors.DagboekEditor;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.DefaultEditor;

public class DagboekAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 436272868462866942L;
	public static final String PAGE = DagboekAdapter.class.getName();
	private static final String[] NAMES = new String[] { Dagboek.ID, Dagboek.SOORT, Dagboek.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Naam", "Soort", "Omschrijving" };
	private static final short[] SIZES = new short[] { 80, 100, 300 };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true };
	private MaintenancePanel maintenancePanel;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public DagboekAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		DagboekManager manager = DagboekManagerFactory.getInstance(bedrijf.getDBData());
		ODBTable table = new ODBTable(manager, new DagboekDataReader(), ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(Dagboek.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("DagboekAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("Dagboeken");
		table.addDownloadPDFContextAction("Dagboeken");
		TableHelper.createMenu(table, new Action[] { refresh }, null);

		DefaultEditor editor = new DagboekEditor(manager);
		maintenancePanel = new MaintenancePanel(Dagboek.class, "Dagboek", "Dagboeken", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		initialized = true;
	}
}