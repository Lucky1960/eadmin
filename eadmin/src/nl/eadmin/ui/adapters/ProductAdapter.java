package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Product;
import nl.eadmin.db.ProductManager;
import nl.eadmin.db.ProductManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.editors.ProductEditor;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.DefaultEditor;

public class ProductAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = -5158415974527351174L;
	public static final String PAGE = ProductAdapter.class.getName();
	private static final String[] NAMES = new String[] { Product.PRODUCT, Product.OMSCHR1, Product.OMSCHR2, Product.OMSCHR3, Product.EENHEID, Product.PRIJS };
	private static final String[] LABELS = new String[] { "Code", "Omschrijving-1", "Omschrijving-2", "Omschrijving-3", "Eenheid", "Prijs" };
	private static final short[] SIZES = new short[] { 80, 150, 150, 150, 80, 80 };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true, true, true, true };
	private MaintenancePanel maintenancePanel;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public ProductAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		ProductManager manager = ProductManagerFactory.getInstance(bedrijf.getDBData());
		ODBTable table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(Product.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("ProductAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("Producten");
		table.addDownloadPDFContextAction("Producten");
		TableHelper.createMenu(table, new Action[] { refresh }, null);

		DefaultEditor editor = new ProductEditor(bedrijf, manager);
		maintenancePanel = new MaintenancePanel(Product.class, "Product", "Producten", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		initialized = true;
	}
}