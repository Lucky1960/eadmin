package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManager;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.selection.DebCredLijstSelection;
import nl.eadmin.ui.editors.DebiteurenEditor;
import nl.eadmin.ui.uiobjects.windows.FacturenLijstWindow;
import nl.eadmin.ui.uiobjects.windows.RenumberDCWindow;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.DefaultEditor;

public class DebiteurenAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 1505975204071417877L;
	public static final String PAGE = DebiteurenAdapter.class.getName();
	private static final String[] NAMES = new String[] { Debiteur.DEB_NR, Debiteur.NAAM, Debiteur.CONTACT_PERSOON, Debiteur.ADRESREGEL1, Debiteur.PLAATS };
	private static final String[] LABELS = new String[] { "DebNr", "Naam", "Contactpersoon", "Adres", "Plaats" };
	private static final short[] SIZES = new short[] { 60, 200, 150, 200, 200 };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true, false, false };
	private MaintenancePanel maintenancePanel;
	private ODBTable table;
	private Bedrijf bedrijf;
	private Action debKaart = new Action("Debiteurenkaart").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("debKaart").setIcon("report.png");
	private Action renumber = new Action("Hernummer").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("renumber").setIcon("text_replace.png");
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public DebiteurenAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		DebiteurManager manager = DebiteurManagerFactory.getInstance(bedrijf.getDBData());
		Gebruiker user = (Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
		table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(Debiteur.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("DebiteurAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_2);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("Debiteuren");
		table.addDownloadPDFContextAction("Debiteuren");
		TableHelper.createMenu(table, new Action[] { refresh }, new Action[] { debKaart, (user.isAdmin() ? renumber : null) });
		table.setRowAction(null);

		DefaultEditor editor = new DebiteurenEditor(manager, table);
		maintenancePanel = new MaintenancePanel(Debiteur.class, "Debiteur", "Debiteuren", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		maintenancePanel.addActionForMainButtonBar(debKaart);
		maintenancePanel.addActionForMainButtonBar(user.isAdmin() ? renumber : null);
		initialized = true;
	}

	public DataObject debKaart(DataObject object) throws Exception {
		Debiteur debiteur = (Debiteur) GeneralHelper.getSelectedItem(table, object);
		table.unSelect(debiteur);
//		object.addUIObject(new DebCredKaartWindow(debiteur.getBedrijf(), debiteur.getDebNr(), "D"));
		object.addUIObject(new FacturenLijstWindow(bedrijf, "D", debiteur.getDebNr(), (DebCredLijstSelection) null));
		return object;
	}

	public DataObject renumber(DataObject object) throws Exception {
		Debiteur debiteur = (Debiteur) GeneralHelper.getSelectedItem(table, object);
		table.unSelect(debiteur);
		object.addUIObject(new RenumberDCWindow(bedrijf, "D", debiteur.getDebNr(), getProcessAdapterPageName(PAGE), RESET_ACTION));
		return object;
	}
}
