package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManager;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.helpers.ExportHelper;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.UserAutHelper;
import nl.eadmin.ui.editors.BedrijfEditor;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.jsql.DBData;
import nl.ibs.util.Scrambler;

public class ExportSystemFileAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = -5328652360340090570L;
	public static final String PAGE = ExportSystemFileAdapter.class.getName();
	private Bedrijf bedrijf;
	private DBData dbd;
	private ExportPanel exportPanel;
	private Window window;
	private FloatBar fb2;
	private MaintenancePanel maintenancePanel;
	private ODBTable table;
	private Action export = new Action("Exporteer").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("show2").setIcon("database_upload.png");

	public ExportSystemFileAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(new Header("Exporteren Systeembestand", Header.TYPE_SCREEN_NAME));
		object.addUIObject(maintenancePanel);
		return object;
	}

	public DataObject show2(DataObject object) throws Exception {
		bedrijf = (Bedrijf) GeneralHelper.getSelectedItem(table, object);
		table.unSelect(bedrijf);
		exportPanel = new ExportPanel(bedrijf);
		window = new Window();
		window.setLabel("Exporteren Systeembestand");
		Panel panel = new Panel();
		panel.addUIObject(exportPanel);
		panel.addUIObject(fb2);
		window.add(panel);
		object.addUIObject(window);
		return object;
	}

	public DataObject export(DataObject object) throws Exception {
		String result = Scrambler.scramble(new ExportHelper().export(exportPanel));
		BinaryObject bobj = new BinaryObject(result.getBytes(), BinaryObject.TYPE_BIN, object);
		bobj.openSaveAsDialog("Systeembestand_" + ApplicationConstants.APPLICATION_VERSION + ".bin");
		window.closeWindow(object);
		return bobj;
	}

	private void initialize(DataObject object) throws Exception {
		Action ok2 = new Action("OK").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("export").setIsDownloadAction(true).setDefault(true).setIcon("tick.png");
		Action back2 = new Action("Terug") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				window.closeWindow(object);
				return super.execute(object);
			}
		}.setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_left.png");
		createMaintenancePanel();
		fb2 = new FloatBar();
		fb2.addAction(ok2);
		fb2.addAction(back2);
		initialized = true;
	}

	private void createMaintenancePanel() throws Exception {
		dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		BedrijfManager manager = BedrijfManagerFactory.getInstance(dbd);
		Gebruiker gebruiker = (Gebruiker)ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
		table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(UserAutHelper.getAdminFilter(dbd, gebruiker), null);
		table.setName("ExportAdapter");
		table.setColumnNames(new String[] { Bedrijf.OMSCHRIJVING });
		table.setColumnLabels(new String[] { "Omschrijving" });
		table.setColumnSizes(new short[] { 300 });
		table.setSelectable(true);
		TableHelper.createMenu(table, null, new Action[] { export });
		BedrijfEditor editor = new BedrijfEditor(manager);
		maintenancePanel = new MaintenancePanel(Bedrijf.class, "Bedrijf", "Bedrijven", table, editor, null);
		maintenancePanel.setCreateEnabled(false);
		maintenancePanel.setDeleteEnabled(false);
		maintenancePanel.setEditEnabled(false);
		maintenancePanel.setDetailsEnabled(false);
		maintenancePanel.setShowHeaders(false);
		maintenancePanel.addActionForMainButtonBar(export);
		maintenancePanel.addActionForMainButtonBar(finish);
	}
}