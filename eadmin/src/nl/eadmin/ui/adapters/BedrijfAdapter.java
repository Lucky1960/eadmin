package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManager;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateManager;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.UserAutHelper;
import nl.eadmin.ui.editors.BedrijfEditor;
import nl.eadmin.ui.editors.HtmlTemplateEditor;
import nl.eadmin.ui.transformer.BooleanTransformer;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.TextArea;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.esp.util.Transformer;
import nl.ibs.jsql.DBData;

public class BedrijfAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 8782355121966110309L;
	public static final String PAGE = BedrijfAdapter.class.getName();
	public static final String ACTION_TEMPLATES = "htmlTemplates";
	private static final String[] NAMES = new String[] { Bedrijf.BEDRIJFSCODE, Bedrijf.OMSCHRIJVING, Bedrijf.PLAATS, Bedrijf.ACTIVE };
	private static final String[] LABELS = new String[] { "Admin.", "Omschrijving", "Plaats", "Actief" };
	private static final short[] SIZES = new short[] { 60, 200, 150, 40 };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true, false };
	private MaintenancePanel maintenancePanel;
	private ODBTable table, table2;
	private Action factTemplates = new Action("Factuur-layouts").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(ACTION_TEMPLATES).setIcon("html.png");
	private Action showTemplate = new Action("Toon html").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("showHtmlTemplate").setIcon("html.png");
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public BedrijfAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		Gebruiker gebruiker = (Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		BedrijfManager manager = BedrijfManagerFactory.getInstance(dbd);
		table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(UserAutHelper.getAdminFilter(dbd, gebruiker), null);
		table.setName("BedrijfAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setDisplayTransformers(new Transformer[] { null, null, null, new BooleanTransformer() });
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("Administraties");
		table.addDownloadPDFContextAction("Administraties");
		TableHelper.createMenu(table, new Action[] { refresh }, new Action[] { factTemplates });
		table.setRowAction(null);

		DefaultEditor editor = new BedrijfEditor(manager);
		maintenancePanel = new MaintenancePanel(Bedrijf.class, "Administratie", "Administraties", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		maintenancePanel.setCreateEnabled(gebruiker.isAdmin());
		maintenancePanel.addActionForMainButtonBar(factTemplates);
		initialized = true;
	}

	public DataObject htmlTemplates(DataObject object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) GeneralHelper.getSelectedItem(table, object);
		table.unSelect(bedrijf);
		String[] NAMES2 = new String[] { HtmlTemplate.ID, HtmlTemplate.OMSCHRIJVING, HtmlTemplate.IS_DEFAULT };
		String[] LABELS2 = new String[] { "Layoutnaam", "Omschrijving", "Std." };
		short[] SIZES2 = new short[] { 100, 300, 60 };
		HtmlTemplateManager manager2 = HtmlTemplateManagerFactory.getInstance(bedrijf.getDBData());
		table2 = new ODBTable(manager2, ApplicationConstants.NUMBEROFTABLEROWS);
		table2.setFilter(HtmlTemplate.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' ", null);
		table2.setName("HtmlTemplateAdapter");
		table2.setColumnNames(NAMES2);
		table2.setColumnLabels(LABELS2);
		table2.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table2.setColumnSizes(SIZES2);
		table2.setMultipleSelectable();
		table2.addDownloadCSVContextAction("HtmlTemplates");
		table2.addDownloadPDFContextAction("HtmlTemplates");

		HtmlTemplateEditor editor2 = new HtmlTemplateEditor(bedrijf, manager2, table2);
		MaintenancePanel maintenancePanel2 = new MaintenancePanel(HtmlTemplate.class, "Template", "Templates", table2, editor2, null);
		maintenancePanel2.setMainTopPanel(table2.createSearchWithFilterPanel(NAMES2, LABELS2, NAMES2, LABELS2, true));
		maintenancePanel2.setUseWindows(true);
		maintenancePanel2.setShowHeaders(false);
		maintenancePanel2.addActionForMainButtonBar(showTemplate);
		Window window = new Window();
		window.setLabel("Onderhouden Factuur-layouts ");
		Panel panel = new Panel();
		panel.addUIObject(maintenancePanel2);
		window.add(panel);
		object.addUIObject(window);
		return object;
	}

	public DataObject showHtmlTemplate(DataObject object) throws Exception {
		HtmlTemplate htmlTemplate = (HtmlTemplate) GeneralHelper.getSelectedItem(table2, object);
		table2.unSelect(htmlTemplate);
		Window window = new Window();
		window.setLabel("'Toon Factuurlayout-html ");
		Panel panel = new Panel();
		TextArea txt = new TextArea("", Field.TYPE_TEXT, "", htmlTemplate.getHtmlString());
		txt.setLength(80);
		txt.setHeight(15);
		panel.addUIObject(txt);
		window.add(panel);
		object.addUIObject(window);
		return object;
	}
}