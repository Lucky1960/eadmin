package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class DummyProcess extends ProjectProcess {
	private static final long serialVersionUID = 8906359619823331436L;
	public static final String PAGE = DummyProcess.class.getName();
	public static final String START_PAGE = DummyAdapter.PAGE;
	public static final String START_ACTION = DummyAdapter.START_ACTION;

	public DummyProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new DummyAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Dummy";
	}

	public String getShortDescription() {
		return "Dummy";
	}

	public String getProcessIconUrl() {
		return "building.png";
	}
}