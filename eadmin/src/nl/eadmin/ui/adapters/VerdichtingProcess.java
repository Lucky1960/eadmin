package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class VerdichtingProcess extends ProjectProcess {
	private static final long serialVersionUID = 4028540085763437533L;
	public static final String PAGE = VerdichtingProcess.class.getName();
	public static final String START_PAGE = VerdichtingAdapter.PAGE;
	public static final String START_ACTION = VerdichtingAdapter.START_ACTION;

	public VerdichtingProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new VerdichtingAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Verdichtingen";
	}

	public String getShortDescription() {
		return "Verdichtingen";
	}

	public String getProcessIconUrl() {
		return "arrow_join.png";
	}
}