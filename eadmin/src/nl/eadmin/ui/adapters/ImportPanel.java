package nl.eadmin.ui.adapters;

import java.util.Calendar;

import nl.eadmin.enums.ImportTypeEnum;
import nl.eadmin.helpers.ExportHelper;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.HeaderPanel;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UserMessageException;

public class ImportPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private Field fldFileName, fldVersion, fldSystemfileDate, fldBedrijf;
	private CheckBox[] checkBox;

	public ImportPanel() throws Exception {
		initialize();
	}

	private void initialize() throws Exception {
		Panel selection = new Panel("Importgegevens");
		ESPGridLayout grid = new ESPGridLayout();
		selection.addUIObject(grid);
		int nr = ImportTypeEnum.getCollection().size();
		checkBox = new CheckBox[nr];
		for (int i = 0; i < nr; i++) {
			checkBox[i] = new CheckBox(ImportTypeEnum.getValue(i));
			checkBox[i].setHidden(true);
			checkBox[i].setValue(true);
			checkBox[i].setOrientationCheckBFirst();
			checkBox[i].setHelpId("ImportType");
			grid.add(checkBox[i], i, 0);
		}
		fldFileName = new Field("Importbestand", Field.TYPE_TEXT, null, "", 50);
		fldFileName.setReadonly(true);
		fldVersion = new Field("Versie", Field.TYPE_TEXT, null, "", 50);
		fldVersion.setReadonly(true);
		fldSystemfileDate = new Field("Datum systeembestand", Field.TYPE_TEXT, null, "", 50);
		fldSystemfileDate.setReadonly(true);
		fldBedrijf = new Field("Bedrijf", Field.TYPE_TEXT, null, "", 50);
		fldBedrijf.setReadonly(true);
		HeaderPanel headerPanel = new HeaderPanel();
		headerPanel.addUIObject(fldFileName);
		headerPanel.addUIObject(fldVersion);
		headerPanel.addUIObject(fldSystemfileDate);
		headerPanel.addUIObject(fldBedrijf);
		addUIObject(headerPanel);
		addUIObject(new Label("\nSelecteer de onderdelen die u uit het systeembestand wilt importeren\n(LET OP: Maak eerst een backup van uw gegevens!)\n\n"));
		addUIObject(selection);
	}

	public void setValues(String fileName, String importString) throws Exception {
		try {
			String[] records = importString.split(ExportHelper.RCD_SEP);
			String[] fields;
			for (int i = 0; i < records.length; i++) {
				fields = records[i].split(ExportHelper.FLD_SEP);
				for (int j = 0; j < fields.length; j++) {
					if (fields[j].equals(ExportHelper.BLANKS)) {
						fields[j] = "";
					} else if (fields[j].equals(ExportHelper.NULL)) {
						fields[j] = null;
					}
				}
				if (fields[0].equals(ExportHelper.FILE_ID)) {
					int f = 1;
					String version = fields[f++];
					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(Long.parseLong(fields[f++]));
					int d = cal.get(Calendar.DAY_OF_MONTH);
					int m = cal.get(Calendar.MONTH) + 1;
					int y = cal.get(Calendar.YEAR);
					String bedrijf = fields[f++];
					fldFileName.setValue(fileName);
					fldVersion.setValue(version);
					fldSystemfileDate.setValue(d + "-" + m + "-" + y);
					fldBedrijf.setValue(bedrijf);
				} else if (fields[0].equals(ImportTypeEnum._ADRES)) {
					checkBox[ImportTypeEnum.ADRES].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._BEDRIJF)) {
					checkBox[ImportTypeEnum.BEDRIJF].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._BIJLAGE)) {
					checkBox[ImportTypeEnum.BIJLAGE].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._BTWCATEGORIE)) {
					checkBox[ImportTypeEnum.BTWCATEGORIE].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._BTWCODE)) {
					checkBox[ImportTypeEnum.BTWCODE].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._CODETABEL)) {
					checkBox[ImportTypeEnum.CODETABEL].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._CODETBLELM)) {
					checkBox[ImportTypeEnum.CODETABELELM].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._CREDITEUR)) {
					checkBox[ImportTypeEnum.CREDITEUR].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._DAGBOEK)) {
					checkBox[ImportTypeEnum.DAGBOEK].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._DEBITEUR)) {
					checkBox[ImportTypeEnum.DEBITEUR].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._DETAILDATA)) {
					checkBox[ImportTypeEnum.DETAILDATA].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._HEADERDATA)) {
					checkBox[ImportTypeEnum.HEADERDATA].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._HFDVERDICHT)) {
					checkBox[ImportTypeEnum.HFDVERDICHTING].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._HTMLTEMPLATE)) {
					checkBox[ImportTypeEnum.HTMLTEMPLATE].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._INSTELLING)) {
					checkBox[ImportTypeEnum.INSTELLING].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._PERIODE)) {
					checkBox[ImportTypeEnum.PERIODE].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._PRODUCT)) {
					checkBox[ImportTypeEnum.PRODUCT].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._REKENING)) {
					checkBox[ImportTypeEnum.REKENING].setHidden(false);
				} else if (fields[0].equals(ImportTypeEnum._VERDICHTING)) {
					checkBox[ImportTypeEnum.VERDICHTING].setHidden(false);
				}
			}
		} catch (Exception e) {
			throw new UserMessageException("Fout bij importeren: " + e.getMessage());
		}
	}

	public boolean getValue(int importType) throws Exception {
		return checkBox[importType].getValueAsBoolean();
	}
}