/**
 * 
 */
package nl.eadmin.ui.adapters;

import java.util.Date;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.ApplicationSetup;
import nl.eadmin.ApplicationUserSettings;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Autorisatie;
import nl.eadmin.db.AutorisatieManager;
import nl.eadmin.db.AutorisatieManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.db.impl.AutorisatieManager_Impl;
import nl.eadmin.ui.uiobjects.actions.InlogAction;
import nl.ibs.esp.adapter.AbstractLoginAdapter;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.session.ActiveUsers;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.LoginPanel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.jsql.Query;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.jsql.exception.FinderException;
import nl.ibs.util.Scrambler;

public class LoginAdapter extends AbstractLoginAdapter {
	private static final long serialVersionUID = 1L;
	private static boolean dbInitialized;
	private Gebruiker gebruiker;
	private MyListener myListener = new MyListener();
	private Field fldUser, fldPssw;
	private InlogAction inlogAction;

	public LoginAdapter(DataObject dataObject) throws Exception {
		super(dataObject);
	}

	@Override
	protected void customize(LoginPanel loginPanel) throws Exception {
		super.customize(loginPanel);
		loginPanel.setHeader("");
		fldUser = getUserField();
		fldUser.setType(Field.TYPE_UPPER);
		fldUser.addOnChangeListener(myListener);
		fldPssw = getPasswordField();
		fldPssw.addOnChangeListener(myListener);
		inlogAction = new InlogAction(fldUser, fldPssw);
		myListener.event(null, "");
		FloatBar fb = (FloatBar)loginPanel.getUIObjects(FloatBar.class, true).get(0);
		fb.addAction(inlogAction);
		if (dbInitialized == false) {
			(new Thread(new Runnable() {
				public void run() {
					if (dbInitialized)
						return;
					try {
						ApplicationSetup.initialize();
						dbInitialized = true;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			})).start();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void completeLogin(final String gebruikerID) throws Exception {
		if (gebruiker.getBedrijf().trim().length() == 0) {
			AutorisatieManager autMgr = AutorisatieManagerFactory.getInstance();
			Query query = QueryFactory.create((AutorisatieManager_Impl)autMgr, Autorisatie.GEBRUIKER_ID + "='" + gebruiker.getId() + "'");
			Autorisatie aut = autMgr.getFirstObject(query);
			if (aut == null) {
				throw new UserMessageException("U bent voor geen enkele administratie geautoriseerd");
			} else {
				gebruiker.setBedrijf(aut.getBedrijf());
			}
		}
		if (gebruiker.getBoekjaar() == 0) {
			gebruiker.setBoekjaar(new Date().getYear() + 1900);
		}
		Bedrijf bedrijf = BedrijfManagerFactory.getInstance().findByPrimaryKey(gebruiker.getBedrijf());
		setSessionAttribute(SessionKeys.GEBRUIKER, gebruiker);
		setSessionAttribute(SessionKeys.GEBRUIKER_ID, gebruiker.getId());
		setSessionAttribute(SessionKeys.BEDRIJF, bedrijf);
		setSessionAttribute(SessionKeys.BEDRIJF_ID, bedrijf.getBedrijfscode());
		setSessionAttribute(SessionKeys.BOEKJAAR, gebruiker.getBoekjaar());
		setSessionAttribute(SessionKeys.ACTIVE_DB_DATA, bedrijf.getDBData());
		ActiveUsers.addUser(gebruiker.getId(), gebruiker.getNaam(), dataObject);
		ESPSessionContext.setSettingStore(new ApplicationUserSettings(new String[] { gebruikerID, "default" }));
		ESPSessionContext.getSettingsStore().setContext("");
	}

	@Override
	protected boolean isValidUser(String user) throws Exception {
		try {
			gebruiker = GebruikerManagerFactory.getInstance().findByPrimaryKey(user);
			return true;
		} catch (FinderException e) {
			gebruiker = null;
			getLoginPanel().setMessage("Onbekende gebruiker of ongeldige gebruiker/wachtwoord combinatie.");
			return false;
		}
	}

	@Override
	protected boolean isValidUserPasswordCombination(String user, String password) throws Exception {
		return Scrambler.unscramble(gebruiker.getWachtwoord()).equals(password);
	}

	private class MyListener implements EventListener {
		private static final long serialVersionUID = 1L;

		public void event(UIObject object, String type) throws Exception {
			boolean hide = true;
			String user = fldUser.getValue();
			String pssw = fldPssw.getValue();
			if (user.equals(ApplicationConstants.DEFAULT_ADMIN_NAME) && isValidUser(user) && isValidUserPasswordCombination(user,pssw)) {
				hide = false;
			}
			inlogAction.setHidden(hide);
		}
	}
}
