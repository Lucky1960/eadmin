package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.datareaders.RekeningDataReader;
import nl.eadmin.ui.editors.RekeningEditor;
import nl.eadmin.ui.transformer.VerdichtingingTransformer;
import nl.eadmin.ui.uiobjects.panels.RekeningSpeciaalPanel;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.esp.util.Transformer;

public class RekeningAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = -8215226379709634037L;
	public static final String PAGE = RekeningAdapter.class.getName();
	private static final String[] NAMES = new String[] { Rekening.REKENING_NR, Rekening.OMSCHRIJVING, Rekening.VERDICHTING, RekeningDataReader.BTW_CAT, Rekening.BLOCKED };
	private static final String[] LABELS = new String[] { "RekNr", "Omschrijving", "Verdichting", "Btw-cat.", "Geblokkeerd" };
	private static final short[] SIZES = new short[] { 60, 300, 250, 80, 80 };
	private static final String[] TYPE = new String[] { Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_BOOLEAN };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true, true, true };
	private Bedrijf bedrijf;
	private MaintenancePanel maintenancePanel;
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public RekeningAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		object.addUIObject(new Paragraph(""));
		object.addUIObject(new RekeningSpeciaalPanel(bedrijf, 200));
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		RekeningManager manager = RekeningManagerFactory.getInstance(bedrijf.getDBData());
		ODBTable table = new ODBTable(manager, new RekeningDataReader(), ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(Rekening.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("RekeningAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setColumnTypes(TYPE);
		table.setDisplayTransformers(new Transformer[] { null, null, new VerdichtingingTransformer(bedrijf), null });
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("Grootboekrekeningen");
		table.addDownloadPDFContextAction("Grootboekrekeningen");
		TableHelper.createMenu(table, new Action[] { refresh }, new Action[] {});
		table.setRowAction(null);

		DefaultEditor editor = new RekeningEditor(bedrijf, manager);
		maintenancePanel = new MaintenancePanel(Bedrijf.class, "Rekening", "Rekeningen", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		initialized = true;
	}
}