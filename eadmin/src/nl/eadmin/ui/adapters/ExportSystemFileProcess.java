package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class ExportSystemFileProcess extends ProjectProcess {
	private static final long serialVersionUID = -7685017455154360730L;
	public static final String PAGE = ExportSystemFileProcess.class.getName();
	public static final String START_PAGE = ExportSystemFileAdapter.PAGE;
	public static final String START_ACTION = ExportSystemFileAdapter.START_ACTION;

	public ExportSystemFileProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new ExportSystemFileAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Exporteren systeembestand";
	}

	public String getShortDescription() {
		return "Exporteren sys.bst.";
	}

	public String getProcessIconUrl() {
		return "database_upload.png";
	}
}