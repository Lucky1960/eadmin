package nl.eadmin.ui.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.databeans.FactuurDataBean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.HTMLHelper;
import nl.eadmin.ui.datareaders.DetailDataReader;
import nl.eadmin.ui.editors.DetailDataEditor;
import nl.eadmin.ui.uiobjects.panels.FactureerPanel;
import nl.eadmin.ui.uiobjects.tables.DetailDataTable;
import nl.eadmin.util.PrintWebExtension;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.Message;
import nl.ibs.esp.uiobjects.RedirectAction;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.Window;

public class FactureerAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 8782355121966110309L;
	public static final String PAGE = FactureerAdapter.class.getName();
	public static final String FACTUREER = "factureer";
	public static final String RESET_ACTION2 = "reset2";
	public static final String START_ACTION2 = "show2";
	public static final String START_ACTION3 = "show3";
	public static final int MODE_ENTRY = 0;
	public static final int MODE_PRINT = 1;
	private ArrayList<?> selectedDetails;
	private Bedrijf bedrijf;
	private Dagboek dagboek;
	private String dagboekSoort;
	private CollectionTable dagboekTable;
	private FactureerPanel factureerPanel;
	private Window factureerWindow;
	private HeaderData headerData;
	private MaintenancePanel detailDataMaintenancePanel;
	private DetailDataTable detailDataTable;
	private DetailDataManager detailManager;
	private boolean initialized2 = false;
	private int mode;
	private Action factureer = new Action("Factureren").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(FACTUREER).setIcon("application_view_columns.png");
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(RESET_ACTION2).setIcon("arrow_refresh.png");
	private Action ok = new Action("OK") {
		private static final long serialVersionUID = 1L;

		public boolean execute(DataObject object) throws Exception {
			dagboek = (Dagboek)GeneralHelper.getSelectedItem(dagboekTable, object);
			object.addUIObject(new RedirectAction(getProcessAdapterPageName(FactureerAdapter.PAGE), RESET_ACTION2, getProcess().getProcessId()));
			return super.execute(object);
		}
	}.setIcon("tick.png");

	public FactureerAdapter(ESPProcess process, DataObject dataObject, int mode) throws Exception {
		super(process, dataObject);
		this.mode = mode;
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		if (dagboek == null) {
			clearScreen(true, false);
			FloatBar fb = new FloatBar();
			fb.addAction(ok);
			fb.addAction(finish);
			object.addUIObject(new Header("Selecteren Verkoopboek", Header.TYPE_SCREEN_NAME));
			object.addUIObject(dagboekTable);
			object.addUIObject(fb);
		} else {
			object.addUIObject(new RedirectAction(getProcessAdapterPageName(FactureerAdapter.PAGE), RESET_ACTION2, getProcess().getProcessId()));
		}
		return object;
	}

	public DataObject show2(DataObject object) throws Exception {
		if (!initialized2) {
			initialize2(object);
		}
		clearScreen(true, false);
		if (mode == MODE_ENTRY) {
			object.addUIObject(new Header("Onderhouden Factuurregels", Header.TYPE_SCREEN_NAME));
		} else {
			object.addUIObject(new Header("Facturen maken", Header.TYPE_SCREEN_NAME));
		}
		object.addUIObject(new Header("Dagboek: " + dagboek.getId() + " - " + dagboek.getOmschrijving(), Header.TYPE_SUB_SCREEN_NAME));
		object.addUIObject(detailDataMaintenancePanel);
		return object;
	}

	public DataObject show3(DataObject object) throws Exception {
		factureerPanel = new FactureerPanel(bedrijf, dagboek, selectedDetails);
		factureerWindow = new Window();
		factureerWindow.setLabel("Genereren Factuur");
		factureerWindow.add(factureerPanel);
		FloatBar fb = new FloatBar();
		fb.addAction(new Action("Terug") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				factureerWindow.closeWindow(object);
				object.addUIObject(new RedirectAction(getProcessAdapterPageName(FactureerAdapter.PAGE), START_ACTION2, getProcess().getProcessId()));
				return true;
			}
		}.setIcon("arrow_left.png"));
		fb.addAction(new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				headerData = factureerPanel.factureer();
				detailDataTable.reload();
				factureerWindow.closeWindow(object);
				initialized2 = false;
				object.addUIObject(new Message("Factuur " + headerData.getBoekstuk() + " is aangemaakt"));
				object.addUIObject(new RedirectAction(getProcessAdapterPageName(FactureerAdapter.PAGE), "print", getProcess().getProcessId()));
				return true;
			}
		}.setIcon("tick.png").setValidationEnabled(true));
		factureerWindow.add(fb);
		object.addUIObject(factureerWindow);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		dagboekSoort = DagboekSoortEnum.VERKOOP;
		detailManager = DetailDataManagerFactory.getInstance(bedrijf.getDBData());
		Collection<?> dagBoeken = GeneralHelper.getDagboeken(bedrijf, dagboekSoort);
		if (dagBoeken.size() == 1) {
			dagboek = (Dagboek) dagBoeken.iterator().next();
		} else {
			dagboek = null;
			dagboekTable = new CollectionTable(Dagboek.class, dagBoeken, ApplicationConstants.NUMBEROFTABLEROWS);
			dagboekTable.setName("Bijlagen");
			dagboekTable.setColumnNames(new String[] { Dagboek.ID, Dagboek.OMSCHRIJVING });
			dagboekTable.setColumnLabels(new String[] { "Dagboek", "Omschrijving" });
			dagboekTable.setColumnSizes(new short[] { 100, 300 });
			dagboekTable.setSingleSelectable();
			Action showAction = new Action("OK") {
				private static final long serialVersionUID = 1L;

				public boolean execute(DataObject object) throws Exception {
					dagboek = (Dagboek) GeneralHelper.getSelectedItem(dagboekTable, object);
					object.addUIObject(new RedirectAction(getProcessAdapterPageName(FactureerAdapter.PAGE), RESET_ACTION2, getProcess().getProcessId()));
					return super.execute(object);
				}
			};
			dagboekTable.setRowAction(showAction);
			dagboekTable.reload();
		}
		initialized = true;
	}

	public DataObject reset2(DataObject object) throws Exception {
		initialized2 = false;
		return show2(object);
	}

	public void initialize2(DataObject object) throws Exception {
		detailDataTable = new DetailDataTable(new DetailDataReader(dagboek), bedrijf, dagboek, null);
		detailDataTable.setFilter(DetailData.BOEKSTUK + "=''", null);
		detailDataTable.setMultipleSelect(true);
		TableHelper.createMenu(detailDataTable, new Action[] { refresh }, new Action[] {});
		detailDataMaintenancePanel = new MaintenancePanel(DetailData.class, "Factuurregel", "Factuurregels", detailDataTable, new DetailDataEditor(bedrijf, dagboek, null, null, detailManager), finish);
		detailDataMaintenancePanel.setMainTopPanel(detailDataTable.getSearchPanel());
		detailDataMaintenancePanel.setCreateEnabled(mode == MODE_ENTRY);
		detailDataMaintenancePanel.setDeleteEnabled(mode == MODE_ENTRY);
		detailDataMaintenancePanel.setEditEnabled(mode == MODE_ENTRY);
		detailDataMaintenancePanel.setUseWindows(true);
		detailDataMaintenancePanel.setHeaderForMainScreen(null);
		detailDataMaintenancePanel.showMultipleDeletionConfirmationScreen(true);
		detailDataMaintenancePanel.addActionForMainButtonBar(mode == MODE_ENTRY ? null : factureer);
		initialized2 = true;
	}

	public DataObject factureer(DataObject object) throws Exception {
		selectedDetails = detailDataTable.getSelectedObjects();
		if (selectedDetails == null || selectedDetails.isEmpty()) {
			throw new UserMessageException("Voor deze functie moet minimaal 1 regel worden geselecteerd");
		}
		Iterator<?> details = selectedDetails.iterator();
		String dcNr = ((DetailData) details.next()).getDcNummer();
		while (details.hasNext()) {
			if (!((DetailData) details.next()).getDcNummer().equals(dcNr)) {
				throw new UserMessageException("Alle geselecteerde regels moeten van dezelfde debiteur zijn");
			}
		}
		object.addUIObject(new RedirectAction(getProcessAdapterPageName(PAGE), START_ACTION3, getProcess().getProcessId()));
		return object;
	}

	public DataObject print(DataObject object) throws Exception {
		FactuurDataBean factuurDataBean = new FactuurDataBean(headerData, factureerPanel.getHtmlTemplate());
		final String html = HTMLHelper.getFactuurHTML(factuurDataBean);
		String name = "Factuur_" + factuurDataBean.getFactuurNr();
		object.addUIObject(new PrintWebExtension(object.getScreen(),name,html));
		return object;
	}
}