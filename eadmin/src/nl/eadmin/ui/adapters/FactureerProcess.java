package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class FactureerProcess extends ProjectProcess {
	private static final long serialVersionUID = 7432280509849530605L;
	public static final String PAGE = FactureerProcess.class.getName();
	public static final String START_PAGE = FactureerAdapter.PAGE;
	public static final String START_ACTION = FactureerAdapter.START_ACTION;

	public FactureerProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new FactureerAdapter(this, object, FactureerAdapter.MODE_PRINT));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Factuurregels";
	}

	public String getShortDescription() {
		return "Factuurregels";
	}

	public String getProcessIconUrl() {
		return "application_view_columns.png";
	}
}