package nl.eadmin.ui.adapters;

import java.util.HashMap;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public abstract class ProjectProcess extends ESPProcess {
	private static final long serialVersionUID = 1L;
	public static final String PROCESSNAME = "ProcessName";
	private HashMap<String, Object> myAttributes;

	public ProjectProcess(DataObject object, String processName) throws Exception {
		super(object);
		setSingleton(true);
		myAttributes = new HashMap<String, Object>();
		myAttributes.put(PROCESSNAME, processName);
	}

	public void setAttribute(String name, Object value) {
		if (name == null || name.equals(PROCESSNAME)) {
			return;
		} else {
			myAttributes.put(name, value);
		}
	}

	public Object getAttribute(String name) {
		if (name == null) {
			return null;
		} else {
			return myAttributes.get(name);
		}
	}
}