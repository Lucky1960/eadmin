package nl.eadmin.ui.adapters;

import nl.eadmin.SessionKeys;
import nl.eadmin.helpers.ExportHelper;
import nl.eadmin.helpers.ImportHelper;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.FileSelectionField;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Header;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.jsql.DBData;
import nl.ibs.util.Scrambler;

public class ImportSystemFileAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 8414775573117047536L;
	public static final String PAGE = ImportSystemFileAdapter.class.getName();
	private String importString;
	private ImportPanel importPanel = new ImportPanel();
	private FileSelectionField filename = new FileSelectionField("Importbestand");
	private Field fldBedrijf = new Field("Bedrijfscode (Evt. nieuw)");
	private CheckBox cbOverWrite = new CheckBox("Aanwezige gegevens overschrijven");
	private FloatBar fb1, fb2;
	private Window window;

	public ImportSystemFileAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(new Header("Importeren Systeembestand", Header.TYPE_SCREEN_NAME));
		FieldGroup fg = new FieldGroup();
		fg.add(filename);
		object.addUIObject(fg);
		object.addUIObject(fb1);
		return object;
	}

	public DataObject show2(DataObject object) throws Exception {
		try {
			importString = Scrambler.unscramble(new String(filename.getFileContent()));
			if (!importString.startsWith(ExportHelper.FILE_ID)) {
				importString = null;
			}
		} catch (Exception e) {
		}
		if (importString == null) {
			throw new UserMessageException("'" + filename.getFileName() + "' is geen geldig importbestand");
		}

		importPanel.setValues(filename.getFileName(), importString);
		window = new Window();
		window.setLabel("Importeren Systeembestand");
		Panel panel = new Panel();
		panel.addUIObject(importPanel);
		ESPGridLayout grid = new ESPGridLayout();
		grid.add(fldBedrijf, 0, 0);
		grid.add(cbOverWrite, 1, 0);
		panel.addUIObject(grid);
		panel.addUIObject(fb2);
		window.add(panel);
		object.addUIObject(window);
		return object;
	}

	public DataObject executeImport(DataObject object) throws Exception {
		if (fldBedrijf.getValue().trim().length() == 0) {
			fldBedrijf.setInvalidTag();
			throw new UserMessageException("Dit is een verplichte rubriek");
		}
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		new ImportHelper(importString, importPanel, dbd, fldBedrijf.getValue(), cbOverWrite.getValueAsBoolean()).importFromString();
		return finish(object);
	}

	private void initialize(DataObject object) throws Exception {
		filename.setMandatory(true);
		fldBedrijf.setLength(10);
		fldBedrijf.setMaxLength(10);
		fldBedrijf.setType(Field.TYPE_UPPER);
		fldBedrijf.setMandatory(true);
		cbOverWrite.setOrientationCheckBFirst();
		Action ok1 = new Action("OK").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("show2").setIcon("tick.png");
//		ok1.setValidationEnabled(true);
		ok1.setDefault(true);
		Action ok2 = new Action("OK").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("executeImport").setIcon("tick.png");
		Action back2 = new Action("Terug") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				window.closeWindow(object);
				return super.execute(object);
			}
		}.setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_left.png");
		fb1 = new FloatBar();
		fb1.addAction(ok1);
		fb1.addAction(finish);
		fb2 = new FloatBar();
		fb2.addAction(ok2);
		fb2.addAction(back2);
		initialized = true;
	}
}