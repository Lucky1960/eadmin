package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class ImportSystemFileProcess extends ProjectProcess {
	private static final long serialVersionUID = 6705914674834624372L;
	public static final String PAGE = ImportSystemFileProcess.class.getName();
	public static final String START_PAGE = ImportSystemFileAdapter.PAGE;
	public static final String START_ACTION = ImportSystemFileAdapter.START_ACTION;

	public ImportSystemFileProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new ImportSystemFileAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Importeren systeembestand";
	}

	public String getShortDescription() {
		return "Importeren sys.bst.";
	}

	public String getProcessIconUrl() {
		return "database_download.png";
	}
}