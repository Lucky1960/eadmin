package nl.eadmin.ui.adapters;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManager;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.selection.DebCredLijstSelection;
import nl.eadmin.ui.editors.CrediteurenEditor;
import nl.eadmin.ui.uiobjects.windows.FacturenLijstWindow;
import nl.eadmin.ui.uiobjects.windows.RenumberDCWindow;
import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.helpers.TableHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.DefaultEditor;

public class CrediteurenAdapter extends ProjectProcessAwareAdapter {
	private static final long serialVersionUID = 1505975204071417877L;
	public static final String PAGE = CrediteurenAdapter.class.getName();
	private static final String[] NAMES = new String[] { Crediteur.CRED_NR, Crediteur.NAAM, Crediteur.CONTACT_PERSOON, Crediteur.ADRESREGEL1, Crediteur.PLAATS };
	private static final String[] LABELS = new String[] { "CredNr", "Naam", "Contactpersoon", "Adres", "Plaats" };
	private static final short[] SIZES = new short[] { 60, 200, 150, 200, 200 };
	private static final boolean[] SORTABLE = new boolean[] { true, true, true, false, false };
	private MaintenancePanel maintenancePanel;
	private ODBTable table;
	private Bedrijf bedrijf;
	private Action credKaart = new Action("Crediteurenkaart").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("credKaart").setIcon("report.png");
	private Action renumber = new Action("Hernummer").setAdapter(getProcessAdapterPageName(PAGE)).setMethod("renumber").setIcon("text_replace.png");
	private Action refresh = new Action("Vernieuwen").setAdapter(getProcessAdapterPageName(PAGE)).setMethod(START_ACTION).setIcon("arrow_refresh.png");

	public CrediteurenAdapter(ESPProcess process, DataObject dataObject) throws Exception {
		super(process, dataObject);
	}

	public DataObject show(DataObject object) throws Exception {
		if (!initialized) {
			initialize(object);
		}
		clearScreen(true, false);
		object.addUIObject(maintenancePanel);
		return object;
	}

	private void initialize(DataObject object) throws Exception {
		bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		CrediteurManager manager = CrediteurManagerFactory.getInstance(bedrijf.getDBData());
		Gebruiker user = (Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
		table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(Crediteur.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("CrediteurAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_2);
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		table.setColumnSortable(SORTABLE);
		table.setColumnSizes(SIZES);
		table.setMultipleSelectable();
		table.addDownloadCSVContextAction("Crediteuren");
		table.addDownloadPDFContextAction("Crediteuren");
		TableHelper.createMenu(table, new Action[] { refresh }, new Action[] { credKaart, (user.isAdmin() ? renumber : null) });
		table.setRowAction(null);

		DefaultEditor editor = new CrediteurenEditor(manager);
		maintenancePanel = new MaintenancePanel(Crediteur.class, "Crediteur", "Crediteuren", table, editor, finish);
		maintenancePanel.setMainTopPanel(table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true));
		maintenancePanel.setUseWindows(true);
		maintenancePanel.addActionForMainButtonBar(credKaart);
		maintenancePanel.addActionForMainButtonBar(user.isAdmin() ? renumber : null);
		initialized = true;
	}

	public DataObject credKaart(DataObject object) throws Exception {
		Crediteur crediteur = (Crediteur) GeneralHelper.getSelectedItem(table, object);
		table.unSelect(crediteur);
		// object.addUIObject(new DebCredKaartWindow(crediteur.getBedrijf(),
		// crediteur.getCredNr(), "C"));
		object.addUIObject(new FacturenLijstWindow(bedrijf, "C", crediteur.getCredNr(), (DebCredLijstSelection) null));
		return object;
	}

	public DataObject renumber(DataObject object) throws Exception {
		Crediteur crediteur = (Crediteur) GeneralHelper.getSelectedItem(table, object);
		table.unSelect(crediteur);
		object.addUIObject(new RenumberDCWindow(bedrijf, "C", crediteur.getCredNr(), getProcessAdapterPageName(PAGE), RESET_ACTION));
		return object;
	}
}