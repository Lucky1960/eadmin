package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class BoekingProcess extends ProjectProcess {
	private static final long serialVersionUID = -2415633884205966811L;
	public static final String PAGE = BoekingProcess.class.getName();
	public static final String START_PAGE = BoekingAdapter.PAGE;
	public static final String START_ACTION = BoekingAdapter.START_ACTION;

	public BoekingProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new BoekingAdapter(this, object));
//		addAdapter(new BoekingWijzigAdapter(this, object));
//		addAdapter(new BoekingKasAdapter(this, object));
//		addAdapter(new BoekingMemAdapter(this, object));
//		addAdapter(new BoekingInkAdapter(this, object));
//		addAdapter(new BoekingVrkAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Boekingen";
	}

	public String getShortDescription() {
		return "Boekingen";
	}

	public String getProcessIconUrl() {
		return "page_edit.png";
	}
}