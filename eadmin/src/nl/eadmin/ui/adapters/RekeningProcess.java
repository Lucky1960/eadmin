package nl.eadmin.ui.adapters;

import nl.ibs.esp.adapter.ESPProcess;
import nl.ibs.esp.dataobject.DataObject;

public class RekeningProcess extends ProjectProcess {
	private static final long serialVersionUID = -2415633884205966811L;
	public static final String PAGE = RekeningProcess.class.getName();
	public static final String START_PAGE = RekeningAdapter.PAGE;
	public static final String START_ACTION = RekeningAdapter.START_ACTION;

	public RekeningProcess(DataObject object) throws Exception {
		super(object, PAGE);
		addAdapter(new RekeningAdapter(this, object));
	}

	public static String getStartPage() {
		return ESPProcess.getProcessAdapterName(PAGE, START_PAGE);
	}

	public String getProcessPageName() {
		return PAGE;
	}

	public String getDefaultAction() {
		return START_ACTION;
	}

	public String getDescription() {
		return "Grootboekrek.";
	}

	public String getShortDescription() {
		return "Grootboekrek.";
	}

	public String getProcessIconUrl() {
		return "text_list_numbers.png.png";
	}
}