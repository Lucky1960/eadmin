package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeDataBean;
import nl.eadmin.db.BtwCodeManager;
import nl.eadmin.ui.uiobjects.referencefields.BtwCategorieReferenceField;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class BtwCodeEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private BtwCodeManager manager;
	private Bedrijf bedrijf;
	private Field fld;

	public BtwCodeEditor(Bedrijf bedrijf, BtwCodeManager manager) throws Exception {
		super(manager);
		this.bedrijf = bedrijf;
		this.manager = manager;
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(BtwCode.CODE));
		fg1.add(getInputComponent(BtwCode.OMSCHRIJVING));
		FieldGroup fg2 = new FieldGroup();
		fg2.add(getInputComponent(BtwCode.PERCENTAGE));
		fg2.add(getInputComponent(BtwCode.IN_EX));
		// fg2.add(getInputComponent(BtwCode.REKENING_INKOOP));
		// fg2.add(getInputComponent(BtwCode.REKENING_VERKOOP));
		fg2.add(getInputComponent(BtwCode.BTW_CAT_INKOOP));
		fg2.add(getInputComponent(BtwCode.BTW_CAT_VERKOOP));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		return panel;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		BtwCodeDataBean bean = new BtwCodeDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		super.updateObject(bean, createScreen);

		try {
			return manager.create(bean);
		} catch (Exception e) {
			getInputComponent(BtwCode.CODE).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public boolean allowDelete(Object object) throws Exception {
		BtwCode btwCode = (BtwCode) object;
		return !btwCode.getCode().equals("L"); // Code "Geen BTW" mag niet worden verwijderd
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (BtwCode.CODE.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(3);
			fld.setLength(3);
			return fld;
		} else if (BtwCode.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (BtwCode.PERCENTAGE.equals(am.getName())) {
			fld = new DecimalField("Percentage", 5, 2, false);
			return fld;
		} else if (BtwCode.IN_EX.equals(am.getName())) {
			ComboBox cbox = new ComboBox();
			cbox.addOption("Inclusief BTW", "I");
			cbox.addOption("Exclusief BTW", "E");
			return cbox;
		} else if (BtwCode.BTW_CAT_INKOOP.equals(am.getName())) {
			return new BtwCategorieReferenceField("BTW-categorie Inkoop", bedrijf, BtwCategorieManagerFactory.getInstance());
		} else if (BtwCode.BTW_CAT_VERKOOP.equals(am.getName())) {
			return new BtwCategorieReferenceField("BTW-categorie Verkoop", bedrijf, BtwCategorieManagerFactory.getInstance());
		} else {
			return super.getNewInputComponent(am);
		}
	}
}