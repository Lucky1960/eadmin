package nl.eadmin.ui.editors;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Adres;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurDataBean;
import nl.eadmin.db.DebiteurManager;
import nl.eadmin.db.Instellingen;
import nl.eadmin.db.InstellingenManagerFactory;
import nl.eadmin.enums.AdresTypeEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.uiobjects.panels.AdresPanel;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.CommonTable;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class DebiteurenEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private DebiteurManager manager;
	private Bedrijf bedrijf;
	private Field fld;
	private AdresPanel adresPanel1, adresPanel2;
	private CommonTable tableToUpdate;
	private String mask;

	public DebiteurenEditor(DebiteurManager manager, CommonTable tableToUpdate) throws Exception {
		super(manager);
		this.bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		this.mask = bedrijf.getInstellingen().getMaskDebNr().trim();
		this.manager = manager;
		this.tableToUpdate = tableToUpdate;
		this.adresPanel1 = new AdresPanel("D", AdresTypeEnum.ADRESTYPE_POST, 120);
		this.adresPanel2 = new AdresPanel("D", AdresTypeEnum.ADRESTYPE_BEZOEK, 120);
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(Debiteur.DEB_NR));
		fg1.add(getInputComponent(Debiteur.NAAM));
		FieldGroup fg2 = new FieldGroup();
		fg2.add(getInputComponent(Debiteur.CONTACT_PERSOON));
		fg2.add(getInputComponent(Debiteur.EMAIL));
		fg2.add(getInputComponent(Debiteur.BTW_NR));
		fg2.add(getInputComponent(Debiteur.KVK_NR));
		fg2.add(getInputComponent(Debiteur.IBAN_NR));
		fg2.add(getInputComponent(Debiteur.BETAAL_TERMIJN));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		panel.addUIObject(adresPanel1);
		panel.addUIObject(adresPanel2);
		return panel;
	}

	protected UIObject getScreenForCreate() throws Exception {
		UIObject screen = super.getScreenForCreate();
		if (mask.length() > 0) {
			InputComponent cmp = getInputComponent(Debiteur.DEB_NR);
			cmp.setValueAsString(GeneralHelper.genereerDcNr(bedrijf.getDBData(), bedrijf.getBedrijfscode(), "D", mask));
			getInputComponent(Debiteur.NAAM).requestFocus(true);
		}
		adresPanel1.loadFrom(null, "");
		adresPanel2.loadFrom(null, "");
		Instellingen settings = InstellingenManagerFactory.getInstance().findOrCreate(bedrijf.getBedrijfscode());
		getInputComponent(Debiteur.BETAAL_TERMIJN).setValueAsString("" + settings.getStdBetaalTermijn());
		return screen;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		String dcNr = ((Debiteur) object).getDebNr();
		adresPanel1.loadFrom(bedrijf, dcNr);
		adresPanel2.loadFrom(bedrijf, dcNr);
		return super.getScreenForUpdate(object);
	}

	protected UIObject getScreenForCopy(Object object) throws Exception {
		String dcNr = ((Debiteur) object).getDebNr();
		adresPanel1.loadFrom(bedrijf, dcNr);
		adresPanel2.loadFrom(bedrijf, dcNr);
		return super.getScreenForCopy(object);
	}

	protected UIObject getScreenForDetails(Object object) throws Exception {
		String dcNr = ((Debiteur) object).getDebNr();
		adresPanel1.loadFrom(bedrijf, dcNr);
		adresPanel2.loadFrom(bedrijf, dcNr);
		return super.getScreenForDetails(object);
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		DebiteurDataBean bean = new DebiteurDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		super.updateObject(bean, createScreen);
		
		try {
			Debiteur debiteur = manager.create(bean);
			adresPanel1.saveTo(bedrijf, bean.getDebNr());
			adresPanel2.saveTo(bedrijf, bean.getDebNr());
			return debiteur;
		} catch (Exception e) {
			getInputComponent(Debiteur.DEB_NR).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		super.updateObject(obj, screen);
		Debiteur debiteur = (Debiteur) obj;
		debiteur.resetTransientAdres();
		adresPanel1.saveTo(bedrijf, debiteur.getDebNr());
		adresPanel2.saveTo(bedrijf, debiteur.getDebNr());
		if (tableToUpdate != null) {
			tableToUpdate.updateRow(debiteur);
			tableToUpdate.reloadPage();
		}
	}

	public boolean allowDelete(Object object) throws Exception {
		return true;
		// Bedrijf application = (Bedrijf) object;
		// StringBuffer sb = new StringBuffer();
		// sb.append("SELECT * FROM MatchDtaHor WHERE ");
		// sb.append(MatchDtaHor.CURRENCY + "='" + application.getIsoCode() +
		// "'");
		// FreeQuery qry =
		// QueryFactory.createFreeQuery((MatchDtaHorManager_Impl)
		// MatchDtaHorManagerFactory.getInstance(), sb.toString());
		// return qry.getFirstObject() == null;
	}

	public void deleteObject(Object object) throws Exception {
		Debiteur debiteur = (Debiteur) object;
		StringBuilder sb = new StringBuilder();
		sb.append(Adres.BEDRIJF + "='" + debiteur.getBedrijf() + "' AND ");
		sb.append(Adres.DC + "='D' AND ");
		sb.append(Adres.DC_NR + "='" + debiteur.getDebNr() + "'");
		AdresManagerFactory.getInstance().generalDelete(sb.toString());
		super.deleteObject(object);
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (Debiteur.DEB_NR.equals(am.getName())) {
			fld = new Field("Debiteurennummer");
			fld.setType(Field.TYPE_TEXT);
			fld.setMaxLength(10);
			fld.setLength(10);
			return fld;
		} else if (Debiteur.NAAM.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Debiteur.CONTACT_PERSOON.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Debiteur.EMAIL.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Debiteur.BTW_NR.equals(am.getName())) {
			fld = new Field("BTW-nummer");
			fld.setType(Field.TYPE_TEXT);
			fld.setMaxLength(18);
			fld.setLength(18);
			return fld;
		} else if (Debiteur.KVK_NR.equals(am.getName())) {
			fld = new Field("KvK-nummer");
			fld.setType(Field.TYPE_TEXT);
			fld.setMaxLength(15);
			fld.setLength(15);
			return fld;
		} else if (Debiteur.IBAN_NR.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(18);
			fld.setLength(18);
			return fld;
		} else if (Debiteur.BETAAL_TERMIJN.equals(am.getName())) {
			fld = new Field("Betaaltermijn in dagen");
			fld.setType(Field.TYPE_NUMBER);
			fld.setMaxLength(3);
			fld.setLength(3);
			return fld;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}