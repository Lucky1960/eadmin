package nl.eadmin.ui.editors;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Adres;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurDataBean;
import nl.eadmin.db.CrediteurManager;
import nl.eadmin.enums.AdresTypeEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.uiobjects.panels.AdresPanel;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class CrediteurenEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private CrediteurManager manager;
	private Bedrijf bedrijf;
	private Field fld;
	private AdresPanel adresPanel1, adresPanel2;
	private String mask;

	public CrediteurenEditor(CrediteurManager manager) throws Exception {
		super(manager);
		this.bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		this.mask = bedrijf.getInstellingen().getMaskCredNr().trim();
		this.manager = manager;
		this.adresPanel1 = new AdresPanel("C", AdresTypeEnum.ADRESTYPE_POST, 120);
		this.adresPanel2 = new AdresPanel("C", AdresTypeEnum.ADRESTYPE_BEZOEK, 120);
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(Crediteur.CRED_NR));
		fg1.add(getInputComponent(Crediteur.NAAM));
		FieldGroup fg2 = new FieldGroup();
		fg2.add(getInputComponent(Crediteur.CONTACT_PERSOON));
		fg2.add(getInputComponent(Crediteur.EMAIL));
		fg2.add(getInputComponent(Crediteur.BTW_NR));
		fg2.add(getInputComponent(Crediteur.KVK_NR));
		fg2.add(getInputComponent(Crediteur.IBAN_NR));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		panel.addUIObject(adresPanel1);
		panel.addUIObject(adresPanel2);
		return panel;
	}

	protected UIObject getScreenForCreate() throws Exception {
		UIObject screen = super.getScreenForCreate();
		if (mask.length() > 0) {
			InputComponent cmp = getInputComponent(Crediteur.CRED_NR);
			cmp.setValueAsString(GeneralHelper.genereerDcNr(bedrijf.getDBData(), bedrijf.getBedrijfscode(), "C", mask));
			getInputComponent(Crediteur.NAAM).requestFocus(true);
		}
		adresPanel1.loadFrom(null, "");
		adresPanel2.loadFrom(null, "");
		return screen;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		String dcNr = ((Crediteur) object).getCredNr();
		adresPanel1.loadFrom(bedrijf, dcNr);
		adresPanel2.loadFrom(bedrijf, dcNr);
		return super.getScreenForUpdate(object);
	}

	protected UIObject getScreenForCopy(Object object) throws Exception {
		String dcNr = ((Crediteur) object).getCredNr();
		adresPanel1.loadFrom(bedrijf, dcNr);
		adresPanel2.loadFrom(bedrijf, dcNr);
		return super.getScreenForCopy(object);
	}

	protected UIObject getScreenForDetails(Object object) throws Exception {
		String dcNr = ((Crediteur) object).getCredNr();
		adresPanel1.loadFrom(bedrijf, dcNr);
		adresPanel2.loadFrom(bedrijf, dcNr);
		return super.getScreenForDetails(object);
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		CrediteurDataBean bean = new CrediteurDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		super.updateObject(bean, createScreen);
		
		try {
			Crediteur crediteur = manager.create(bean);
			adresPanel1.saveTo(bedrijf, bean.getCredNr());
			adresPanel2.saveTo(bedrijf, bean.getCredNr());
			return crediteur;
		} catch (Exception e) {
			getInputComponent(Crediteur.CRED_NR).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		super.updateObject(obj, screen);
		Crediteur crediteur = (Crediteur) obj;
		adresPanel1.saveTo(bedrijf, crediteur.getCredNr());
		adresPanel2.saveTo(bedrijf, crediteur.getCredNr());
	}

	public boolean allowDelete(Object object) throws Exception {
		return true;
		// Bedrijf application = (Bedrijf) object;
		// StringBuffer sb = new StringBuffer();
		// sb.append("SELECT * FROM MatchDtaHor WHERE ");
		// sb.append(MatchDtaHor.CURRENCY + "='" + application.getIsoCode() +
		// "'");
		// FreeQuery qry =
		// QueryFactory.createFreeQuery((MatchDtaHorManager_Impl)
		// MatchDtaHorManagerFactory.getInstance(), sb.toString());
		// return qry.getFirstObject() == null;
	}

	public void deleteObject(Object object) throws Exception {
		Crediteur crediteur = (Crediteur) object;
		StringBuilder sb = new StringBuilder();
		sb.append(Adres.BEDRIJF + "='" + crediteur.getBedrijf() + "' AND ");
		sb.append(Adres.DC + "='C' AND ");
		sb.append(Adres.DC_NR + "='" + crediteur.getCredNr() + "'");
		AdresManagerFactory.getInstance().generalDelete(sb.toString());
		super.deleteObject(object);
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (Crediteur.CRED_NR.equals(am.getName())) {
			fld = new Field("Crediteurennummer");
			fld.setType(Field.TYPE_TEXT);
			fld.setMaxLength(10);
			fld.setLength(10);
			return fld;
		} else if (Crediteur.NAAM.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Crediteur.CONTACT_PERSOON.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Crediteur.EMAIL.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Crediteur.BTW_NR.equals(am.getName())) {
			fld = new Field("BTW-nummer");
			fld.setType(Field.TYPE_TEXT);
			fld.setMaxLength(18);
			fld.setLength(18);
			return fld;
		} else if (Crediteur.KVK_NR.equals(am.getName())) {
			fld = new Field("KvK-nummer");
			fld.setType(Field.TYPE_TEXT);
			fld.setMaxLength(15);
			fld.setLength(15);
			return fld;
		} else if (Crediteur.IBAN_NR.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(18);
			fld.setLength(18);
			return fld;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}