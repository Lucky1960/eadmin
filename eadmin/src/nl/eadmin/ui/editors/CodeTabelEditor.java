package nl.eadmin.ui.editors;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.CodeTabel;
import nl.eadmin.db.CodeTabelDataBean;
import nl.eadmin.db.CodeTabelManager;
import nl.eadmin.enums.CodeTypeEnum;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class CodeTabelEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private CodeTabelManager manager;
	private Bedrijf bedrijf;
	private Field fld;
	private ComboBox cbDataType = CodeTypeEnum.createComboBox("Datatype van de codes", CodeTypeEnum.getCollection(), false);
	private EventListener dataTypeListener = new DataTypeListener();

	public CodeTabelEditor(CodeTabelManager manager) throws Exception {
		super(manager);
		this.bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		this.manager = manager;
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(CodeTabel.TABEL_NAAM));
		fg1.add(getInputComponent(CodeTabel.OMSCHRIJVING));
		FieldGroup fg2 = new FieldGroup();
		fg2.add(getInputComponent(CodeTabel.DATA_TYPE));
		fg2.add(getInputComponent(CodeTabel.LENGTE));
		fg2.add(getInputComponent(CodeTabel.DECIMALEN));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		return panel;
	}

	protected UIObject getScreenForCreate() throws Exception {
		UIObject screen = super.getScreenForCreate();
		((ComboBox) getInputComponent(CodeTabel.DATA_TYPE)).setSelectedOptionValue(CodeTypeEnum.ALFA);
		dataTypeListener.event(cbDataType, "");
		return screen;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		UIObject screen = super.getScreenForUpdate(object);
		dataTypeListener.event(cbDataType, "");
		return screen;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		CodeTabelDataBean bean = new CodeTabelDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		super.updateObject(bean, createScreen);

		try {
			return manager.create(bean);
		} catch (Exception e) {
			getInputComponent(CodeTabel.TABEL_NAAM).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (CodeTabel.TABEL_NAAM.equals(am.getName())) {
			fld = new Field("Tabelnaam");
			fld.setType(Field.TYPE_TEXT);
			fld.setMaxLength(10);
			fld.setLength(10);
			fld.setMandatory(true);
			return fld;
		} else if (CodeTabel.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (CodeTabel.DATA_TYPE.equals(am.getName())) {
			cbDataType.setWidth("100");
			cbDataType.addOnChangeListener(dataTypeListener);
			return cbDataType;
		} else if (CodeTabel.LENGTE.equals(am.getName())) {
			NumberField fld2 = new NumberField("Codelengte", 2, false);
			fld2.setMinValue(1l, true);
			fld2.setMaxValue(10l, true);
			return fld2;
		} else if (CodeTabel.DECIMALEN.equals(am.getName())) {
			NumberField fld2 = new NumberField("Aantal decimalen", 1, false);
			fld2.setMinValue(1l, true);
			fld2.setMaxValue(5l, true);
			return fld2;
		} else {
			return super.getNewInputComponent(am);
		}
	}

	private class DataTypeListener implements EventListener {
		private static final long serialVersionUID = 1L;

		public void event(UIObject object, String type) throws Exception {
			InputComponent cmpLen = getInputComponent(CodeTabel.LENGTE);
			InputComponent cmpDec = getInputComponent(CodeTabel.DECIMALEN);
			switch (((ComboBox) object).getSelectedOptionValue()) {
			case CodeTypeEnum.ALFA:
				cmpLen.setHidden(false);
				cmpDec.setHidden(true);
				break;
			case CodeTypeEnum.NUM:
				cmpLen.setHidden(false);
				cmpDec.setHidden(true);
				break;
			case CodeTypeEnum.DEC:
				cmpLen.setHidden(false);
				cmpDec.setHidden(false);
				break;
			}
		};
	}
}