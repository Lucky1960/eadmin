package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Product;
import nl.eadmin.db.ProductDataBean;
import nl.eadmin.db.ProductManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.ui.uiobjects.referencefields.RekeningReferenceField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class ProductEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private ProductManager manager;
	private Bedrijf bedrijf;
	private Field fld;

	public ProductEditor(Bedrijf bedrijf, ProductManager manager) throws Exception {
		super(manager);
		this.bedrijf = bedrijf;
		this.manager = manager;
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(Product.PRODUCT));
		fg1.add(getInputComponent(Product.OMSCHR1));
		fg1.add(getInputComponent(Product.OMSCHR2));
		fg1.add(getInputComponent(Product.OMSCHR3));
		FieldGroup fg2 = new FieldGroup();
		fg2.add(getInputComponent(Product.EENHEID));
		fg2.add(getInputComponent(Product.PRIJS));
		fg2.add(getInputComponent(Product.PRIJS_INKOOP));
		fg2.add(getInputComponent(Product.REKENING_NR));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		return panel;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		ProductDataBean bean = new ProductDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		super.updateObject(bean, createScreen);

		try {
			return manager.create(bean);
		} catch (Exception e) {
			getInputComponent(Product.PRODUCT).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (Product.PRODUCT.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setLabel("Productcode");
			fld.setMandatory(true);
			fld.setMaxLength(30);
			fld.setLength(30);
			return fld;
		} else if (Product.OMSCHR1.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMandatory(true);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Product.OMSCHR2.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Product.OMSCHR3.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Product.EENHEID.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(10);
			fld.setLength(10);
			return fld;
		} else if (Product.PRIJS.equals(am.getName())) {
			fld = new DecimalField("Verkooprijs", 15, 2, false);
			fld.setMandatory(true);
			return fld;
		} else if (Product.PRIJS_INKOOP.equals(am.getName())) {
			fld = new DecimalField("Inkoopprijs", 15, 2, false);
			return fld;
		} else if (Product.REKENING_NR.equals(am.getName())) {
			return new RekeningReferenceField("Grootboekrekening", bedrijf, RekeningManagerFactory.getInstance());
		} else {
			return super.getNewInputComponent(am);
		}
	}
}