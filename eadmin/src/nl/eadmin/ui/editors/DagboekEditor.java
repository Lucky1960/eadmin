package nl.eadmin.ui.editors;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.BankImport;
import nl.eadmin.db.BankImportManager;
import nl.eadmin.db.BankImportManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekDataBean;
import nl.eadmin.db.DagboekManager;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.impl.DagboekManager_Impl;
import nl.eadmin.db.impl.HeaderDataManager_Impl;
import nl.eadmin.enums.BankImportTypeEnum;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.eadmin.ui.uiobjects.panels.DagboekCodeTabelPanel;
import nl.eadmin.ui.uiobjects.referencefields.RekeningReferenceField;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.vegas.meta.AttributeMeta;

public class DagboekEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private DagboekManager manager;
	private DagboekCodeTabelPanel codeTabelPnl;
	private Bedrijf bedrijf;
	private Field ibanNr;
	private ComboBox bankImportType;
	private MyListener myListener = new MyListener();
	private IdListener idListener = new IdListener();
	private FieldGroup fg1, fg2;

	public DagboekEditor(DagboekManager manager) throws Exception {
		super(manager);
		this.bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		this.manager = manager;
		this.codeTabelPnl = new DagboekCodeTabelPanel(bedrijf);
		this.ibanNr = new Field("Eigen rekeningnummer (in bestand)");
		this.ibanNr.setType(Field.TYPE_UPPER);
		this.ibanNr.setMaxLength(18);
		this.ibanNr.setLength(18);
		this.bankImportType = BankImportTypeEnum.createComboBox("Import-formaat", BankImportTypeEnum.getCollection(), true);
		this.bankImportType.setWidth("140");
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		fg1 = new FieldGroup();
		fg1.setLabelWidth(250);
		fg1.add(getInputComponent(Dagboek.ID));
		fg1.add(getInputComponent(Dagboek.SOORT));
		fg1.add(getInputComponent(Dagboek.OMSCHRIJVING));
		fg1.add(getInputComponent(Dagboek.BOEKSTUK_MASK));
		fg1.add(getInputComponent(Dagboek.REKENING));
		fg1.add(getInputComponent(Dagboek.TEGEN_REKENING));
		fg2 = new FieldGroup("Importeren bankmutaties");
		fg2.setLabelWidth(250);
		fg2.add(ibanNr);
		fg2.add(bankImportType);
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(codeTabelPnl);
		return panel;
	}

	protected UIObject getScreenForCreate() throws Exception {
		UIObject screen = super.getScreenForCreate();
		getInputComponent(Dagboek.TEGEN_REKENING).setHidden(false);
		ibanNr.setValue();
		bankImportType.setSelectedOptionValue("");
		myListener.event(getInputComponent(Dagboek.SOORT), "");
		codeTabelPnl.loadFrom(null);
		codeTabelPnl.setHidden(false);
		return screen;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		UIObject screen = super.getScreenForUpdate(object);
		Dagboek dagboek = (Dagboek) object;
		boolean isSysval = dagboek.getSystemValue();
		String dagboekSoort = dagboek.getSoort();
		getInputComponent(Dagboek.SOORT).setReadonly(true);
		getInputComponent(Dagboek.REKENING).setMandatory(isSysval);
		getInputComponent(Dagboek.TEGEN_REKENING).setHidden((dagboekSoort == null || !dagboekSoort.equals(DagboekSoortEnum.BANK)));
		BankImport bankImport = dagboek.getBankImport();
		if (bankImport != null && bankImport.getFormat().trim().length() > 0) {
			ibanNr.setValue(bankImport.getIbanNr());
			bankImportType.setSelectedOptionValue(bankImport.getFormat());
		}
		myListener.event(getInputComponent(Dagboek.SOORT), "");
		codeTabelPnl.loadFrom(dagboek);
		codeTabelPnl.setHidden(isSysval);
		return screen;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		validateInput(true);
		DagboekDataBean bean = new DagboekDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		bean.setSystemValue(false);
		super.updateObject(bean, createScreen);

		try {
			Dagboek dagboek = manager.create(bean);
			updateBankImportFormat(dagboek);
			codeTabelPnl.saveTo(dagboek);
			return dagboek;
		} catch (Exception e) {
			getInputComponent(Dagboek.ID).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		validateInput(false);
		Dagboek dagboek = (Dagboek) obj;
		super.updateObject(dagboek, screen);
		updateBankImportFormat(dagboek);
		codeTabelPnl.saveTo(dagboek);
	}

	private void validateInput(boolean newObject) throws Exception {
		InputComponent cmp = getInputComponent(Dagboek.BOEKSTUK_MASK);
		String id = getInputComponent(Dagboek.ID).getValue();
		String soort = getInputComponent(Dagboek.SOORT).getValue();
		String boekstukMask = cmp.getValue().trim();
		if (id.startsWith("SYS")) {
			if (newObject) {
				getInputComponent(Dagboek.ID).setInvalidTag();
				throw new UserMessageException("Dagboeknamen die u zelf toevoegt mogen niet met 'SYS' beginnen");
			}
			Rekening rekNr = (Rekening)((RekeningReferenceField)getInputComponent(Dagboek.REKENING)).getSelectedObject();
			if (rekNr == null || !rekNr.getRekeningSoort().equals(RekeningSoortEnum.BALANS)) {
				getInputComponent(Dagboek.REKENING).setInvalidTag();
				throw new UserMessageException("Dit is geen Balans-rekening!");
			}
		}
		if ((soort.equals(DagboekSoortEnum.BANK) || soort.equals(DagboekSoortEnum.VERKOOP)) && boekstukMask.length() == 0) {
			cmp.setInvalidTag();
			throw new UserMessageException("Masker is verplicht voor deze dagboeksoort");
		}
		if (boekstukMask.length() > 0 && boekstukMask.indexOf("##") < 0) {
			cmp.setInvalidTag();
			throw new UserMessageException("Ongeldig masker. Moet minimaal '##' bevatten");
		}
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Dagboek WHERE ");
		sb.append(Dagboek.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND ");
		sb.append(Dagboek.SOORT + "='" + soort + "' AND ");
		sb.append(Dagboek.ID + "<>'" + id + "' AND ");
		sb.append(Dagboek.BOEKSTUK_MASK + "='" + boekstukMask + "' ");
		FreeQuery qry = QueryFactory.createFreeQuery((DagboekManager_Impl) DagboekManagerFactory.getInstance(), sb.toString());
		if (qry.getFirstObject() != null) {
			cmp.setInvalidTag();
			throw new UserMessageException("Deze boekstukopmaak is reeds gebruikt in een ander dagboek van deze dagboeksoort");
		}
	}

	private void updateBankImportFormat(Dagboek dagboek) throws Exception {
		BankImportManager biMgr = BankImportManagerFactory.getInstance();
		if (dagboek.getSoort().equals(DagboekSoortEnum.BANK)) {
			BankImport bankImport = dagboek.getBankImport();
			bankImport.setIbanNr(ibanNr.getValue());
			bankImport.setFormat(bankImportType.getSelectedOptionValue());
		} else {
			biMgr.generalDelete(BankImport.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + BankImport.DAGBOEK_ID + "='" + dagboek.getId() + "'");
		}
	}

	public void deleteObject(Object object) throws Exception {
		String bedrijf = ((Dagboek) object).getBedrijf();
		String dagboekId = ((Dagboek) object).getId();
		BankImportManagerFactory.getInstance().generalDelete(BankImport.BEDRIJF + "='" + bedrijf + "' AND " + BankImport.DAGBOEK_ID + "='" + dagboekId + "'");
		BijlageManagerFactory.getInstance().generalDelete(Bijlage.BEDRIJF + "='" + bedrijf + "' AND " + Bijlage.DAGBOEK_ID + "='" + dagboekId + "'");
		DetailDataManagerFactory.getInstance().generalDelete(DetailData.BEDRIJF + "='" + bedrijf + "' AND " + DetailData.DAGBOEK_ID + "='" + dagboekId + "'");
		HeaderDataManagerFactory.getInstance().generalDelete(HeaderData.BEDRIJF + "='" + bedrijf + "' AND " + HeaderData.DAGBOEK_ID + "='" + dagboekId + "'");
		JournaalManagerFactory.getInstance().generalDelete(Journaal.BEDRIJF + "='" + bedrijf + "' AND " + Journaal.DAGBOEK_ID + "='" + dagboekId + "'");
		super.deleteObject(object);
	}

	public boolean allowDelete(Object object) throws Exception {
		Dagboek dagboek = (Dagboek) object;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM HeaderData WHERE ");
		sb.append(HeaderData.BEDRIJF + "='" + dagboek.getBedrijf() + "' AND " + HeaderData.DAGBOEK_ID + "='" + dagboek.getId() + "'");
		FreeQuery qry = QueryFactory.createFreeQuery((HeaderDataManager_Impl) HeaderDataManagerFactory.getInstance(), sb.toString());
		return qry.getFirstObject() == null;
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		Field fld;
		if (Dagboek.ID.equals(am.getName())) {
			fld = new Field("Dagboeknaam");
			fld.setType(Field.TYPE_TEXT);
			fld.setMaxLength(10);
			fld.setLength(10);
			fld.addOnChangeListener(idListener);
			return fld;
		} else if (Dagboek.SOORT.equals(am.getName())) {
			ComboBox cbox = DagboekSoortEnum.createComboBox("Dagboeksoort", DagboekSoortEnum.getCollection(), false);
			cbox.addOnChangeListener(myListener);
			return cbox;
		} else if (Dagboek.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Dagboek.BOEKSTUK_MASK.equals(am.getName())) {
			fld = new Field("Opmaak boekstuk");
			fld.setMaxLength(15);
			fld.setLength(15);
			return fld;
		} else if (Dagboek.REKENING.equals(am.getName())) {
			return new RekeningReferenceField("Rekening", bedrijf, RekeningManagerFactory.getInstance());
		} else if (Dagboek.TEGEN_REKENING.equals(am.getName())) {
			return new RekeningReferenceField("Tegenrekening", bedrijf, RekeningManagerFactory.getInstance());
		} else {
			return super.getNewInputComponent(am);
		}
	}

	private class IdListener implements EventListener {
		private static final long serialVersionUID = 1L;

		public void event(UIObject object, String type) throws Exception {
			if (getInputComponent(Dagboek.ID).getValue().equals("SYS1")) {
				getInputComponent(Dagboek.REKENING).setLabel("Rekening winst-saldo");
			} else {
				getInputComponent(Dagboek.REKENING).setLabel("Rekening");
			}
		}
	}

	private class MyListener implements EventListener {
		private static final long serialVersionUID = 1L;

		public void event(UIObject object, String type) throws Exception {
			String soort = ((InputComponent) object).getValue().trim();
			getInputComponent(Dagboek.BOEKSTUK_MASK).setMandatory(soort.equals(DagboekSoortEnum.BANK) || soort.equals(DagboekSoortEnum.VERKOOP));
			fg2.setHidden(!soort.equals(DagboekSoortEnum.BANK));
			ibanNr.setMandatory(!fg2.isHidden());
			// bankImportType.setHidden(!soort.equals(DagboekSoortEnum.BANK));
			getInputComponent(Dagboek.TEGEN_REKENING).setHidden(soort.length() == 0 || !soort.equals(DagboekSoortEnum.BANK));
		}
	}
}