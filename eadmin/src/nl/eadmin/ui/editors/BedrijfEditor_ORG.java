package nl.eadmin.ui.editors;

import nl.eadmin.ApplicationSetup;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Adres;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.Autorisatie;
import nl.eadmin.db.AutorisatieManagerFactory;
import nl.eadmin.db.BankImport;
import nl.eadmin.db.BankImportManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfDataBean;
import nl.eadmin.db.BedrijfManager;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.CodeTabel;
import nl.eadmin.db.CodeTabelElement;
import nl.eadmin.db.CodeTabelElementManagerFactory;
import nl.eadmin.db.CodeTabelManagerFactory;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Hoofdverdichting;
import nl.eadmin.db.HoofdverdichtingManagerFactory;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.db.Instellingen;
import nl.eadmin.db.InstellingenManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.Verdichting;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.db.impl.GebruikerManager_Impl;
import nl.eadmin.enums.AdresTypeEnum;
import nl.eadmin.helpers.LogoHelper;
import nl.eadmin.ui.uiobjects.panels.AdresPanel;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.Image;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.vegas.meta.AttributeMeta;

public class BedrijfEditor_ORG extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private BedrijfManager manager;
//LL	private String template;
	private Field fld;
	private AdresPanel adresPanel1, adresPanel2;
	private Image logoPrint, logoScherm;
	private Image newLogoPrint, newLogoScherm;
	private ESPGridLayout gridLogo;
	private CheckBox cbAdres2;
	private EventListener cbAdres2Listener;
	private Field fldDebNrMask = new Field("Opmaak debiteurennummer");
	private Field fldCredNrMask = new Field("Opmaak crediteurennummer");
	private NumberField stdBetaalTrm = new NumberField("Std. betaaltermijn (in dagen)", 14, 2);
	private NumberField rekeningLngt = new NumberField("Lengte grootboekrekeningnrs", 4, 1);
	{
		rekeningLngt.setMinValue(3, true);
		rekeningLngt.setMaxValue(9, true);
	}
//	private boolean suppressEvent;

	public BedrijfEditor_ORG(BedrijfManager manager) throws Exception {
		super(manager);
		this.manager = manager;
		this.logoPrint = new Image("");
		this.logoPrint.setSource("");
		this.logoPrint.setHeight("150");
		this.logoPrint.setWidth("150");
		this.logoScherm = new Image("");
		this.logoScherm.setSource("");
		this.logoScherm.setHeight("150");
		this.logoScherm.setWidth("150");
		this.adresPanel1 = new AdresPanel("", AdresTypeEnum.ADRESTYPE_BEZOEK, 120);
		this.adresPanel2 = new AdresPanel("", AdresTypeEnum.ADRESTYPE_POST, 120);
		this.cbAdres2Listener = new EventListener() {
			private static final long serialVersionUID = 1L;

			public void event(UIObject object, String type) throws Exception {
				adresPanel2.setHidden(!cbAdres2.getValueAsBoolean());
			}
		};
		this.cbAdres2 = new CheckBox("Er is een afwijkend postadres");
		this.cbAdres2.setOrientationCheckBFirst();
		this.cbAdres2.addOnChangeListener(cbAdres2Listener);
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(Bedrijf.BEDRIJFSCODE));
		fg1.add(getInputComponent(Bedrijf.OMSCHRIJVING));
		FieldGroup fg2 = new FieldGroup();
		fg2.add(getInputComponent(Bedrijf.EMAIL));
		fg2.add(getInputComponent(Bedrijf.KVK_NR));
		fg2.add(getInputComponent(Bedrijf.BTW_NR));
		fg2.add(getInputComponent(Bedrijf.IBAN_NR));
		FieldGroup fg3 = new FieldGroup();
		fg3.forceBorder(false);
		fg3.setLabelWidth(20);
		fg3.add(cbAdres2);
		FieldGroup fg4 = new FieldGroup();
		fg4.add(stdBetaalTrm);
		fg4.add(rekeningLngt);
		fg4.add(fldDebNrMask);
		fg4.add(fldCredNrMask);
//LL	gridLogo = new ESPGridLayout();
//		gridLogo.add(new Label("Logo printuitvoer"), 0, 0);
//		gridLogo.add(logoPrint, 1, 0);
//		gridLogo.add(getInputComponent(Bedrijf.LOGO_PRINT), 2, 0);
//		gridLogo.add(new Label("Schermlogo"), 0, 1);
//		gridLogo.add(logoScherm, 1, 1);
//		gridLogo.add(getInputComponent(Bedrijf.LOGO_SCHERM), 2, 1);
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		panel.addUIObject(fg4);
		panel.addUIObject(gridLogo);
//LL		panel.addUIObject(getOnderhoudenFactuurActie());
		panel.addUIObject(adresPanel1);
		panel.addUIObject(fg3);
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(adresPanel2);
		return panel;
	}
//LL
//	private UIObject getOnderhoudenFactuurActie() {
//		Action action = new Action("Factuur template aanpassen"){
//			private static final long serialVersionUID = 1L;
//			public boolean execute(DataObject object) throws Exception {
//				final Window window = new Window();
//				window.setLabel("Factuur template aanpassen");
//				Panel panel = new Panel();
//				//final HTMLEditor editor = new HTMLEditor(HTMLEditor.THEME_TEMPLATE);
//				final TextArea editor = new TextArea();
//				//editor.setWidth(225);
//				editor.setLength(140);
//				editor.setHeight(35);
//				editor.setValueAsString(getTemplateForScreen());
//				panel.addUIObject(editor);
//				window.add(panel);
//				FloatBar bar = new FloatBar();
//				bar.addAction(new Action("Klaar"){
//					private static final long serialVersionUID = 1L;
//					@Override
//					public boolean execute(DataObject object) throws Exception {
//						setTemplateFromScreen(editor.getValue());
//						window.closeWindow(object);
//						return true;
//					}
//				}.setIcon("disk.png"));
//				bar.addAction((Action)new CloseWindowAction(window).setIcon("cancel.png").setLabel("Annuleren"));
//				window.add(bar);
//				object.addUIObject(window);
//				return true;
//			}
//
//			
//		};
//		return action;
//	}
//	
//	private String getTemplateForScreen() throws Exception{
//		if (template==null)
//			template=TemplateHelper.getFactuurHTML();
//		return template.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");
//	}
//	
//	private void setTemplateFromScreen(String template) {
//		this.template=template;
//	}

	protected UIObject getScreenForCreate() throws Exception {
		adresPanel1.loadFrom(null, "");
		adresPanel2.loadFrom(null, "");
		cbAdres2.setValue(false);
		cbAdres2Listener.event(null, "");
		newLogoPrint = LogoHelper.getEmptyImage(150, 150);
		//newLogoPrint.setLayoutConstraints(new ESPGridLayoutConstraints(1, 0));
		newLogoScherm = LogoHelper.getEmptyImage(150, 150);
		//newLogoScherm.setLayoutConstraints(new ESPGridLayoutConstraints(1, 1));
		gridLogo.replaceUIObject(logoPrint, newLogoPrint);
		gridLogo.replaceUIObject(logoScherm, newLogoScherm);
		logoPrint = newLogoPrint;
		logoScherm = newLogoScherm;
		stdBetaalTrm.setValue(14);
		rekeningLngt.setValue(04);
		fldDebNrMask.setValue();
		fldCredNrMask.setValue();
//LL		template=null;
		return super.getScreenForCreate();
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) object;
		adresPanel1.loadFrom(bedrijf, "");
		adresPanel2.loadFrom(bedrijf, "");
		cbAdres2.setValue(!adresPanel2.isEmpty());
		cbAdres2Listener.event(null, "");
//LL		newLogoPrint = LogoHelper.getLogo(bedrijf, LogoHelper.PRINT, 150, 150);
		//newLogoPrint.setLayoutConstraints(new ESPGridLayoutConstraints(1, 0));
//LL		newLogoScherm = LogoHelper.getLogo(bedrijf, LogoHelper.SCHERM, 150, 150);
		//newLogoScherm.setLayoutConstraints(new ESPGridLayoutConstraints(1, 1));
		gridLogo.replaceUIObject(logoPrint, newLogoPrint);
		gridLogo.replaceUIObject(logoScherm, newLogoScherm);
		logoPrint = newLogoPrint;
		logoScherm = newLogoScherm;
		Instellingen settings = InstellingenManagerFactory.getInstance().findOrCreate(bedrijf.getBedrijfscode());
		stdBetaalTrm.setValue(settings.getStdBetaalTermijn());
		rekeningLngt.setValue(settings.getLengteRekening());
		fldDebNrMask.setValue(settings.getMaskDebNr());
		fldCredNrMask.setValue(settings.getMaskCredNr());
//LL		
//		byte[] templateBytes=bedrijf.getFactuurTemplate();
//		template=templateBytes==null||templateBytes.length<15?null:new String(templateBytes,"UTF8");
					
		return super.getScreenForUpdate(object);
	}

	protected UIObject getScreenForCopy(Object object) throws Exception {
//LL	Bedrijf bedrijf = (Bedrijf) object;

		adresPanel1.loadFrom((Bedrijf) object, "");
		adresPanel2.loadFrom((Bedrijf) object, "");
		gridLogo.replaceUIObject(logoPrint, LogoHelper.getEmptyImage(150, 150));
		gridLogo.replaceUIObject(logoScherm, LogoHelper.getEmptyImage(150, 150));
//LL		
//		byte[] templateBytes=bedrijf.getFactuurTemplate();
//		template=templateBytes==null||templateBytes.length<15?null:new String(templateBytes,"UTF8");

		return super.getScreenForCopy(object);
	}

	protected UIObject getScreenForDetails(Object object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) object;
		adresPanel1.loadFrom(bedrijf, "");
		adresPanel2.loadFrom(bedrijf, "");
//LL	gridLogo.replaceUIObject(logoPrint, LogoHelper.getLogo(bedrijf, LogoHelper.PRINT, 150, 150));
//LL	gridLogo.replaceUIObject(logoScherm, LogoHelper.getLogo(bedrijf, LogoHelper.SCHERM, 150, 150));
//LL
//		byte[] templateBytes=bedrijf.getFactuurTemplate();
//		template=templateBytes==null||templateBytes.length<15?null:new String(templateBytes,"UTF8");
		
		return super.getScreenForDetails(object);
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		BedrijfDataBean bean = new BedrijfDataBean();
		super.updateObject(bean, createScreen);
//LL	byte[] printLogo = ((FileSelectionField) getInputComponent(Bedrijf.LOGO_PRINT)).getFileContent();
//		byte[] schermLogo = ((FileSelectionField) getInputComponent(Bedrijf.LOGO_SCHERM)).getFileContent();
//		byte[] factuurTemplate=template==null||template.length()<15?null:template.getBytes("UTF8");
//		bean.setLogoPrint(printLogo);
//		bean.setLogoScherm(schermLogo);
//		bean.setFactuurTemplate(factuurTemplate);
		if (!cbAdres2.getBoolean()) {
			adresPanel2.loadFrom(null, "");// Wis
		}

		try {
			Bedrijf bedrijf = manager.create(bean);
			Gebruiker gebruiker = (Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
			adresPanel1.saveTo(bedrijf, "");
			adresPanel2.saveTo(bedrijf, "");
			Instellingen settings = InstellingenManagerFactory.getInstance().findOrCreate(bedrijf.getBedrijfscode());
			settings.setStdBetaalTermijn(stdBetaalTrm.getIntValue());
			settings.setLengteRekening(rekeningLngt.getIntValue());
			settings.setMaskDebNr(fldDebNrMask.getValue());
			settings.setMaskCredNr(fldCredNrMask.getValue());
			new ApplicationSetup().initBedrijf(bedrijf, gebruiker);
			return bedrijf;
		} catch (Exception e) {
			getInputComponent(Bedrijf.BEDRIJFSCODE).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		super.updateObject(obj, screen);
		Bedrijf bedrijf = (Bedrijf) obj;
//LL		byte[] logo;
//		logo = ((FileSelectionField) getInputComponent(Bedrijf.LOGO_PRINT)).getFileContent();
//		if (logo.length > 0) {
//			bedrijf.setLogoPrint(logo);
//		}
//		logo = ((FileSelectionField) getInputComponent(Bedrijf.LOGO_SCHERM)).getFileContent();
//		if (logo.length > 0) {
//			bedrijf.setLogoScherm(logo);
//		}
//		byte[] factuurTemplate=template==null||template.length()<15?null:template.getBytes("UTF8");
//		bedrijf.setFactuurTemplate(factuurTemplate);
		if (!cbAdres2.getBoolean()) {
			adresPanel2.loadFrom(null, "");// Wis
		}
		adresPanel1.saveTo(bedrijf, "");
		adresPanel2.saveTo(bedrijf, "");
		Instellingen settings = InstellingenManagerFactory.getInstance().findOrCreate(bedrijf.getBedrijfscode());
		settings.setStdBetaalTermijn(stdBetaalTrm.getIntValue());
		settings.setLengteRekening(rekeningLngt.getIntValue());
		settings.setMaskDebNr(fldDebNrMask.getValue());
		settings.setMaskCredNr(fldCredNrMask.getValue());
	}

	public void deleteObject(Object object) throws Exception {
		String bedrijfscode = ((Bedrijf) object).getBedrijfscode();
		AdresManagerFactory.getInstance().generalDelete(Adres.BEDRIJF + "='" + bedrijfscode + "'");
		AutorisatieManagerFactory.getInstance().generalDelete(Autorisatie.BEDRIJF + "='" + bedrijfscode + "'");
		BankImportManagerFactory.getInstance().generalDelete(BankImport.BEDRIJF + "='" + bedrijfscode + "'");
		BijlageManagerFactory.getInstance().generalDelete(Bijlage.BEDRIJF + "='" + bedrijfscode + "'");
		BtwCodeManagerFactory.getInstance().generalDelete(BtwCode.BEDRIJF + "='" + bedrijfscode + "'");
		CodeTabelManagerFactory.getInstance().generalDelete(CodeTabel.BEDRIJF + "='" + bedrijfscode + "'");
		CodeTabelElementManagerFactory.getInstance().generalDelete(CodeTabelElement.BEDRIJF + "='" + bedrijfscode + "'");
		CrediteurManagerFactory.getInstance().generalDelete(Crediteur.BEDRIJF + "='" + bedrijfscode + "'");
		DagboekManagerFactory.getInstance().generalDelete(Dagboek.BEDRIJF + "='" + bedrijfscode + "'");
		DebiteurManagerFactory.getInstance().generalDelete(Debiteur.BEDRIJF + "='" + bedrijfscode + "'");
		DetailDataManagerFactory.getInstance().generalDelete(DetailData.BEDRIJF + "='" + bedrijfscode + "'");
		HeaderDataManagerFactory.getInstance().generalDelete(HeaderData.BEDRIJF + "='" + bedrijfscode + "'");
		HoofdverdichtingManagerFactory.getInstance().generalDelete(Hoofdverdichting.BEDRIJF + "='" + bedrijfscode + "'");
		HtmlTemplateManagerFactory.getInstance().generalDelete(HtmlTemplate.BEDRIJF + "='" + bedrijfscode + "'");
		InstellingenManagerFactory.getInstance().generalDelete(Instellingen.BEDRIJF + "='" + bedrijfscode + "'");
		JournaalManagerFactory.getInstance().generalDelete(Journaal.BEDRIJF + "='" + bedrijfscode + "'");
		PeriodeManagerFactory.getInstance().generalDelete(Periode.BEDRIJF + "='" + bedrijfscode + "'");
		RekeningManagerFactory.getInstance().generalDelete(Rekening.BEDRIJF + "='" + bedrijfscode + "'");
		VerdichtingManagerFactory.getInstance().generalDelete(Verdichting.BEDRIJF + "='" + bedrijfscode + "'");
		super.deleteObject(object);
	}

	public boolean allowDelete(Object object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) object;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Gebruiker WHERE ");
		sb.append(Gebruiker.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		FreeQuery qry = QueryFactory.createFreeQuery((GebruikerManager_Impl) GebruikerManagerFactory.getInstance(), sb.toString());
		return qry.getFirstObject() == null;
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (Bedrijf.BEDRIJFSCODE.equals(am.getName())) {
			fld = new Field("Administratiecode");
			fld.setType(Field.TYPE_UPPER);
			fld.setMaxLength(3);
			fld.setLength(3);
			return fld;
		} else if (Bedrijf.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Bedrijf.EMAIL.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Bedrijf.KVK_NR.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(15);
			fld.setLength(15);
			return fld;
		} else if (Bedrijf.BTW_NR.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(18);
			fld.setLength(18);
			return fld;
		} else if (Bedrijf.IBAN_NR.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(18);
			fld.setLength(18);
			return fld;
//LL	} else if (Bedrijf.LOGO_PRINT.equals(am.getName())) {
//			fld = new FileSelectionField("Bestandsnaam printlogo")
			/*{
				private byte[] content;
				private String fileName;
				//private Exception savedException;
				
				@Override
				public String getFileName (){
					return fileName;
				}
				@Override
				public void setValueAsString(String value) {
					//super.setValueAsString(value);
				}
				protected void notifyChangeListeners() {
					//super.notifyChangeListeners();
				}
				
				@Override
				public void notifyListeners(String event) throws Exception {
					// TODO Auto-generated method stub
					//super.notifyListeners(event);
				}
				
				@Override
				public void updateField(Map parameterMap){
					String[] values = (String[]) parameterMap.get(this.getId());
					if (values != null && values.length > 0){
						if (parameterMap.get("updates") != null) {
							fileName = values[0];
							int previousLength=content==null?0:content.length;
							if (fileName == null || fileName.trim().length()==0)
								content = new byte[0];
							try {
								DataObject dataObject = ESPSessionContext.getCurrentDataObject();
								content = dataObject.getMultiPartFormFile(fileName);
							} catch (Exception e) {
								if (Log.warn())Log.error(e);
							}
							if (previousLength != (content==null?0:content.length)){
								super.notifyChangeListeners();
								try {
									super.notifyListeners(EventTypes.ON_CHANGE);
								} catch (Exception e) {
									throw new RuntimeException(e); 
								}
							}
						}
					}
				}
				
				@Override
				public byte[] getFileContent() throws Exception {
					if ((content == null || content.length==0) && (fileName != null && fileName.trim().length()>0))
						throw new UserMessageException(ESPTranslationContext.getTranslationHelper().translate("tmp-file-not-found",fileName,false));
					return content;
				}
				
			};*/
//LL			fld.setDiscardLabel(true);
//			fld.addOnChangeListener(new EventListener() {
//				public void event(UIObject object, String type) throws Exception {
//					if (suppressEvent) return;
//					FileSelectionField fsf = ((FileSelectionField) object);
//					byte[] bytes = fsf.getFileContent();
//					String mimeType = null;
//					ContentInfo fileInfo = new ContentInfoUtil().findMatch(bytes);
//					if (fileInfo != null) {
//						mimeType = fileInfo.getMimeType();
//						Image newObject = new Image(bytes, fsf.getFileName(), mimeType);
//						gridLogo.replaceUIObject(logoPrint, newObject);
//						logoPrint = newObject;
//					}
//				}
//			});
//			return fld;
//		} else if (Bedrijf.LOGO_SCHERM.equals(am.getName())) {
//			fld = new FileSelectionField("Bestandsnaam schermlogo");
//			fld.setDiscardLabel(true);
//			return fld;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}