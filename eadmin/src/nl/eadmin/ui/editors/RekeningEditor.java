package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningDataBean;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.enums.RekeningIcpSoortEnum;
import nl.eadmin.enums.RekeningSubAdminTypeEnum;
import nl.eadmin.enums.RekeningToonKolomEnum;
import nl.eadmin.helpers.RekeningHelper;
import nl.eadmin.ui.uiobjects.referencefields.BtwCategorieReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.BtwCodeReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.VerdichtingReferenceField;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class RekeningEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private RekeningManager manager;
	private RekeningHelper rekeningHelper;
	private Bedrijf bedrijf;
	private Field fld;
	private int stdRekeningLengte;
	private MyListener myListener = new MyListener();
	private MyListener2 myListener2 = new MyListener2();
	private BtwCategorieReferenceField refBtwCat;

	public RekeningEditor(Bedrijf bedrijf, RekeningManager manager) throws Exception {
		super(manager);
		this.bedrijf = bedrijf;
		this.manager = manager;
		this.rekeningHelper = new RekeningHelper(bedrijf.getDBData());
		this.stdRekeningLengte = bedrijf.getInstellingen().getLengteRekening();
		this.refBtwCat = new BtwCategorieReferenceField("BTW-categorie", bedrijf, BtwCategorieManagerFactory.getInstance());
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(Rekening.REKENING_NR));
		fg1.add(getInputComponent(Rekening.OMSCHRIJVING));
		fg1.add(getInputComponent(Rekening.OMSCHRIJVING_KORT));
		FieldGroup fg2 = new FieldGroup();
		fg2.add(getInputComponent(Rekening.BTW_REKENING));
		fg2.add(refBtwCat);
		fg2.add(getInputComponent(Rekening.BTW_CODE));
		fg2.add(getInputComponent(Rekening.BTW_SOORT));
		fg2.add(getInputComponent(Rekening.VERDICHTING));
		fg2.add(getInputComponent(Rekening.ICP_SOORT));
		fg2.add(getInputComponent(Rekening.SUB_ADMINISTRATIE_TYPE));
		fg2.add(getInputComponent(Rekening.TOON_KOLOM));
		fg2.add(getInputComponent(Rekening.BLOCKED));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		return panel;
	}

	protected UIObject getScreenForCreate() throws Exception {
		UIObject screen = super.getScreenForCreate();
		((CheckBox)getInputComponent(Rekening.BTW_REKENING)).setValue(false);
		refBtwCat.setSelectedObject(null);
		myListener.event(getInputComponent(Rekening.SUB_ADMINISTRATIE_TYPE), "");
		myListener2.event(getInputComponent(Rekening.BTW_REKENING), "");
		return screen;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		UIObject screen = super.getScreenForUpdate(object);
		refBtwCat.setSelectedObject(((Rekening)object).getBtwCategorie());
		myListener.event(getInputComponent(Rekening.SUB_ADMINISTRATIE_TYPE), "");
		myListener2.event(getInputComponent(Rekening.BTW_REKENING), "");
		return screen;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		RekeningDataBean bean = new RekeningDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		super.updateObject(bean, createScreen);

		try {
			Rekening rekening = manager.create(bean);
			boolean isBtwRekening = ((CheckBox)getInputComponent(Rekening.BTW_REKENING)).getValueAsBoolean();
			if (isBtwRekening) {
				rekeningHelper.checkBtwCategorie(rekening, refBtwCat.getValue());
				rekeningHelper.setBtwCategorie(rekening, refBtwCat.getValue());
				((BtwCategorie)refBtwCat.getSelectedObject()).setRekeningNr(rekening.getRekeningNr());
			}
			return rekening;
		} catch (Exception e) {
			getInputComponent(Rekening.REKENING_NR).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		Rekening rekening = (Rekening) obj;
		boolean isBtwRekening = ((CheckBox)getInputComponent(Rekening.BTW_REKENING)).getValueAsBoolean();
		if (!isBtwRekening) {
			rekeningHelper.resetBtwCategorie(rekening);
		} else {
			String btwCat = refBtwCat.getValue().trim();
			if (btwCat.length() > 0) {
				rekeningHelper.checkBtwCategorie(rekening, btwCat);
				rekeningHelper.setBtwCategorie(rekening, btwCat);
				((BtwCategorie)refBtwCat.getSelectedObject()).setRekeningNr(rekening.getRekeningNr());
			}
		}
		super.updateObject(obj, screen);
	}

	public boolean allowDelete(Object object) throws Exception {
		return true;
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (Rekening.REKENING_NR.equals(am.getName())) {
			fld = new Field("Rekeningnummer");
			fld.setType(Field.TYPE_NUMBER);
			fld.setLength(stdRekeningLengte);
			fld.setMaxLength(stdRekeningLengte);
			fld.addOnChangeListener(new EventListener() {
				private static final long serialVersionUID = 1L;

				public void event(UIObject object, String type) throws Exception {
					InputComponent cmp = (InputComponent) object;
					String val = "000000000" + cmp.getValue();
					val = val.substring(val.length() - stdRekeningLengte);
					cmp.setValueAsString(val);
				}
			});
			return fld;
		} else if (Rekening.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Rekening.OMSCHRIJVING_KORT.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(10);
			fld.setLength(10);
			return fld;
		} else if (Rekening.BTW_CODE.equals(am.getName())) {
			return new BtwCodeReferenceField("BTW-code", bedrijf, BtwCodeManagerFactory.getInstance());
		} else if (Rekening.BTW_SOORT.equals(am.getName())) {
			ComboBox cbox = new ComboBox("BTW-soort");
			cbox.addOption("Inkoop", "I");
			cbox.addOption("Verkoop", "V");
			return cbox;
		} else if (Rekening.BTW_REKENING.equals(am.getName())) {
			CheckBox cb = new CheckBox("Dit is een BTW-rekening");
			cb.addOnChangeListener(myListener2);
			return cb;
		} else if (Rekening.TOON_KOLOM.equals(am.getName())) {
			return RekeningToonKolomEnum.createComboBox("Toon saldo D/C", RekeningToonKolomEnum.getCollection(), true);
		} else if (Rekening.ICP_SOORT.equals(am.getName())) {
			return RekeningIcpSoortEnum.createComboBox("ICP soort", RekeningIcpSoortEnum.getCollection(), true);
		} else if (Rekening.SUB_ADMINISTRATIE_TYPE.equals(am.getName())) {
			ComboBox cb = RekeningSubAdminTypeEnum.createComboBox("Subadministratie-type", RekeningSubAdminTypeEnum.getCollection(), true);
			cb.addOnChangeListener(myListener);
			return cb;
		} else if (Rekening.VERDICHTING.equals(am.getName())) {
			return new VerdichtingReferenceField("Verdichtingsnr.", bedrijf, VerdichtingManagerFactory.getInstance());
		} else if (Rekening.BLOCKED.equals(am.getName())) {
			return new CheckBox("Geblokkeerd");
		} else {
			return super.getNewInputComponent(am);
		}
	}

	private class MyListener implements EventListener {
		private static final long serialVersionUID = 1L;

		public void event(UIObject object, String type) throws Exception {
			InputComponent cmp = getInputComponent(Rekening.BTW_CODE);
			String val = ((InputComponent) object).getValue().trim();
			if (val.equals("C") || val.equals("D")) {
				cmp.setValueAsString("");
				cmp.setHidden(true);
			} else {
				cmp.setHidden(false);
			}
		}
	}

	private class MyListener2 implements EventListener {
		private static final long serialVersionUID = 1L;

		public void event(UIObject object, String type) throws Exception {
			if (object == null) {
				return;
			}
			InputComponent cmp;
			boolean checked = ((CheckBox) object).getValueAsBoolean();
			if (checked) {
				cmp = getInputComponent(Rekening.BTW_CODE);
				cmp.setValueAsString("L");
				cmp.setHidden(true);
				cmp = getInputComponent(Rekening.BTW_SOORT);
				cmp.setHidden(true);
				refBtwCat.setHidden(false);
				refBtwCat.setMandatory(true);
			} else {
				getInputComponent(Rekening.BTW_CODE).setHidden(false);
				getInputComponent(Rekening.BTW_SOORT).setHidden(false);
				refBtwCat.setHidden(true);
				refBtwCat.setMandatory(false);
			}
		}
	}
}