package nl.eadmin.ui.editors;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.CodeTabel;
import nl.eadmin.db.CodeTabelElement;
import nl.eadmin.db.CodeTabelElementDataBean;
import nl.eadmin.db.CodeTabelElementManager;
import nl.eadmin.helpers.VrijeRubriekHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class CodeTabelElementEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private CodeTabel codeTabel;
	private CodeTabelElementManager manager;
	private Bedrijf bedrijf;
	private Field fld;

	public CodeTabelElementEditor(CodeTabelElementManager manager, CodeTabel codeTabel) throws Exception {
		super(manager);
		this.bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		this.codeTabel = codeTabel;
		this.manager = manager;
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(CodeTabelElement.ID));
		fg1.add(getInputComponent(CodeTabelElement.OMSCHRIJVING));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		return panel;
	}

	// protected UIObject getScreenForCreate() throws Exception {
	// UIObject screen = super.getScreenForCreate();
	// return screen;
	// }

	// protected UIObject getScreenForUpdate(Object object) throws Exception {
	// UIObject screen = super.getScreenForUpdate(object);
	// return screen;
	// }

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		CodeTabelElementDataBean bean = new CodeTabelElementDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		bean.setTabelNaam(codeTabel.getTabelNaam());
		super.updateObject(bean, createScreen);

		try {
			return manager.create(bean);
		} catch (Exception e) {
			getInputComponent(CodeTabelElement.ID).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (CodeTabelElement.ID.equals(am.getName())) {
			fld = (Field) VrijeRubriekHelper.getInputComponent("Code", codeTabel.getDataType(), codeTabel.getLengte(), codeTabel.getDecimalen());
			fld.setMandatory(true);
			return fld;
		} else if (CodeTabelElement.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}