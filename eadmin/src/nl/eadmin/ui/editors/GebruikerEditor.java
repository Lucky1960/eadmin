package nl.eadmin.ui.editors;

import nl.eadmin.db.Autorisatie;
import nl.eadmin.db.AutorisatieManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerDataBean;
import nl.eadmin.db.GebruikerManager;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.util.Scrambler;
import nl.ibs.vegas.meta.AttributeMeta;

public class GebruikerEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private GebruikerManager manager;
	private InputComponent cmp;
	private Field fld;

	public GebruikerEditor(GebruikerManager manager) throws Exception {
		super(manager);
		this.manager = manager;
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(Gebruiker.ID));
		fg1.add(getInputComponent(Gebruiker.NAAM));
		fg1.add(getInputComponent(Gebruiker.WACHTWOORD));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		return panel;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		GebruikerDataBean bean = new GebruikerDataBean();
		super.updateObject(bean, createScreen);
		bean.setWachtwoord(Scrambler.scramble(bean.getWachtwoord()));
		try {
			return manager.create(bean);
		} catch (Exception e) {
			getInputComponent(Gebruiker.ID).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		UIObject screen = super.getScreenForUpdate(object);
		cmp = getInputComponent(Gebruiker.WACHTWOORD);
		cmp.setValueAsString(Scrambler.unscramble(cmp.getValue()));
		return screen;
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		cmp = getInputComponent(Gebruiker.WACHTWOORD);
		cmp.setValueAsString(Scrambler.scramble(cmp.getValue()));
		super.updateObject(obj, screen);
	}

	public void deleteObject(Object object) throws Exception {
		AutorisatieManagerFactory.getInstance().generalDelete(Autorisatie.GEBRUIKER_ID + "='" + ((Gebruiker) object).getId() + "'");
		super.deleteObject(object);
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (Gebruiker.ID.equals(am.getName())) {
			fld = new Field("Gebruikersnaam");
			fld.setType(Field.TYPE_UPPER);
			fld.setMaxLength(15);
			fld.setLength(15);
			return fld;
		} else if (Gebruiker.NAAM.equals(am.getName())) {
			fld = new Field("Omschrijving");
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Gebruiker.WACHTWOORD.equals(am.getName())) {
			fld = new Field("Wachtwoord");
			fld.setType(Field.TYPE_PASSWORD);
			fld.setMaxLength(15);
			fld.setLength(15);
			return fld;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}