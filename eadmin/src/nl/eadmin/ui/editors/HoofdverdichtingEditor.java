package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Hoofdverdichting;
import nl.eadmin.db.HoofdverdichtingDataBean;
import nl.eadmin.db.HoofdverdichtingManager;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class HoofdverdichtingEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private HoofdverdichtingManager manager;
	private Bedrijf bedrijf;
	private Field fld;

	public HoofdverdichtingEditor(Bedrijf bedrijf, HoofdverdichtingManager manager) throws Exception {
		super(manager);
		this.bedrijf = bedrijf;
		this.manager = manager;
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(Hoofdverdichting.ID));
		fg1.add(getInputComponent(Hoofdverdichting.OMSCHRIJVING));
		fg1.add(getInputComponent(Hoofdverdichting.REKENINGSOORT));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		return panel;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		HoofdverdichtingDataBean bean = new HoofdverdichtingDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		super.updateObject(bean, createScreen);
		
		try {
			return manager.create(bean);
		} catch (Exception e) {
			getInputComponent(Hoofdverdichting.ID).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (Hoofdverdichting.ID.equals(am.getName())) {
			fld = new NumberField("Hoofdverdichtingsnummer", 3, false);
			return fld;
		} else if (Hoofdverdichting.REKENINGSOORT.equals(am.getName())) {
			return RekeningSoortEnum.createComboBox("Hoofdverdichtingsoort", RekeningSoortEnum.getCollection(), false);
		} else if (Hoofdverdichting.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}