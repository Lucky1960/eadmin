package nl.eadmin.ui.editors;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Autorisatie;
import nl.eadmin.db.AutorisatieDataBean;
import nl.eadmin.db.AutorisatieManager;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerDataBean;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.uiobjects.MyUserReferenceField;
import nl.eadmin.ui.uiobjects.panels.GebruikerAutPanel;
import nl.eadmin.ui.uiobjects.referencefields.GebruikerReferenceField;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.HeaderPanel;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.util.Scrambler;
import nl.sf.api.UserData;

public class AutorisatieEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private Bedrijf bedrijf;
	private AutorisatieManager manager;
	private GebruikerAutPanel userAutPanel = new GebruikerAutPanel();
	private GebruikerReferenceField fldExistingUser;
	private MyUserReferenceField fldNewUser;
	private Label lblOF = new Label("OF:");

	public AutorisatieEditor(AutorisatieManager manager, Bedrijf bedrijf) throws Exception {
		super(manager);
		this.manager = manager;
		this.bedrijf = bedrijf;
		this.fldExistingUser = new GebruikerReferenceField("", GebruikerManagerFactory.getInstance());
		this.fldExistingUser.setSelectableOnly(true);
		this.fldNewUser = new MyUserReferenceField("Nieuwe gebruiker");
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		HeaderPanel pnl1 = new HeaderPanel("");
		pnl1.addUIObject(fldExistingUser);
		pnl1.addUIObject(lblOF);
		pnl1.addUIObject(fldNewUser);
		Panel pnl2 = new Panel("Authorisaties");
		pnl2.addUIObject(new Paragraph(""));
		pnl2.addUIObject(userAutPanel);
		Panel panel = new Panel();
		panel.addUIObject(pnl1);
		panel.addUIObject(pnl2);
		return panel;
	}

	protected UIObject getScreenForCreate() throws Exception {
		UIObject screen = super.getScreenForCreate();
		fldExistingUser.setLabel("Bestaande gebruiker");
		fldExistingUser.setValue();
		fldExistingUser.setReadonly(false);
		lblOF.setHidden(false);
		fldNewUser.setHidden(false);
		fldNewUser.setValue();
		return screen;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		Autorisatie autorisatie = (Autorisatie) object;
		Gebruiker gebruiker = (Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
		if (gebruiker.getId().equals(autorisatie.getGebruikerId()) && !gebruiker.isAdmin()) {
			throw new UserMessageException("U mag uw eigen autorisaties niet aanpassen");
		}
		UIObject screen = super.getScreenForUpdate(object);
		userAutPanel.load(((Autorisatie) object).getAutMenu());
		fldExistingUser.setLabel("Gebruiker");
		fldExistingUser.setGebruikerId(autorisatie.getGebruikerId());
		fldExistingUser.setReadonly(true);
		lblOF.setHidden(true);
		fldNewUser.setHidden(true);
		return screen;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		String gebruikerId = validateScreen();
		AutorisatieDataBean autBean = new AutorisatieDataBean();
		autBean.setBedrijf(bedrijf.getBedrijfscode());
		autBean.setGebruikerId(gebruikerId);
		try {
			Autorisatie autorisatie = manager.create(autBean);
			userAutPanel.save(autorisatie);
			return autorisatie;
		} catch (Exception e) {
			getInputComponent(Autorisatie.GEBRUIKER_ID).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		userAutPanel.save((Autorisatie) obj);
	}

	private String validateScreen() throws Exception {
		String userId = fldExistingUser.getValue().trim();
		String userIdOrEmail = fldNewUser.getValue().trim();
		if ((userId.length() == 0 && userIdOrEmail.length() == 0) || (userId.length() > 0 && userIdOrEmail.length() > 0)) {
			fldExistingUser.setInvalidTag();
			fldNewUser.setInvalidTag();
			throw new UserMessageException("��n rubriek invullen aub");
		}
		if (userId.length() == 0) {
			UserData userData = GeneralHelper.getUserData(userIdOrEmail);
			if (userData == null) {
				fldNewUser.setInvalidTag();
				throw new UserMessageException("Dit is geen geregistreerde gebruiker");
			} else {
				userId = userData.getUserId();
				GebruikerDataBean gebruikerBean = new GebruikerDataBean();
				gebruikerBean.setId(userId);
				gebruikerBean.setNaam(userData.getFirstName() + " " + userData.getLastName());
				gebruikerBean.setWachtwoord(Scrambler.scramble("xxx"));
				gebruikerBean.setBedrijf(bedrijf.getBedrijfscode());
				gebruikerBean.setBoekjaar((Integer) ESPSessionContext.getSessionAttribute(SessionKeys.BOEKJAAR));
				GebruikerManagerFactory.getInstance().create(gebruikerBean);
			}
		}
		return userId;
	}
}