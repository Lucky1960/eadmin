package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateDataBean;
import nl.eadmin.db.HtmlTemplateManager;
import nl.eadmin.helpers.HtmlTemplateHelper;
import nl.eadmin.helpers.LogoHelper;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.FileSelectionField;
import nl.ibs.esp.uiobjects.Image;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.TextArea;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class HtmlTemplateEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private HtmlTemplateManager manager;
	private Bedrijf bedrijf;
	private Field fld;
	private Image imgLogo, newLogo;
	private ESPGridLayout gridLogo;
	private FileSelectionField htmlFile;
	private ODBTable tableToUpdate;

	public HtmlTemplateEditor(Bedrijf bedrijf, HtmlTemplateManager manager, ODBTable tableToUpdate) throws Exception {
		super(manager);
		this.bedrijf = bedrijf;
		this.manager = manager;
		this.tableToUpdate = tableToUpdate;
		this.imgLogo = new Image("");
		this.imgLogo.setSource("");
		this.imgLogo.setHeight("150");
		this.imgLogo.setWidth("150");
		this.htmlFile = new FileSelectionField("Html-bestandsnaam");
		this.htmlFile.setDiscardLabel(true);

		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		gridLogo = new ESPGridLayout();
		gridLogo.setColumnWidths(new short[]{200});
		gridLogo.add(new Label(""), 0, 0);
		gridLogo.add(imgLogo, 0, 1, 2, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_SOUTH);
		gridLogo.add(getInputComponent(HtmlTemplate.LOGO_HEIGHT), 0, 2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_WEST);
		gridLogo.add(getInputComponent(HtmlTemplate.LOGO), 1, 2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_SOUTH);

		FieldGroup fg1 = new FieldGroup();
		fg1.setLabelWidth(250);
		fg1.add(getInputComponent(HtmlTemplate.ID));
		fg1.add(getInputComponent(HtmlTemplate.OMSCHRIJVING));
		fg1.add(getInputComponent(HtmlTemplate.IS_DEFAULT));
		FieldGroup fg2 = new FieldGroup("Logo");
		fg2.add(gridLogo);
		ESPGridLayout htmlGrid = new ESPGridLayout();
		htmlGrid.add(new Label("Html-code"), 0, 0);
		htmlGrid.add(htmlFile, 0, 1);
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(fg2);
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(htmlGrid);
//		panel.addUIObject(getInputComponent(HtmlTemplate.HTML_STRING));
		return panel;
	}

	protected UIObject getScreenForCreate() throws Exception {
		UIObject screen = super.getScreenForCreate();
		newLogo = LogoHelper.getEmptyImage(150, 150);
		gridLogo.replaceUIObject(imgLogo, newLogo);
		imgLogo = newLogo;
		return screen;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		UIObject screen = super.getScreenForUpdate(object);
		HtmlTemplate htmlTemplate = (HtmlTemplate) object;
		newLogo = LogoHelper.getLogo(htmlTemplate, 150, 150);
		gridLogo.replaceUIObject(imgLogo, newLogo);
		imgLogo = newLogo;
		return screen;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
//		String htmlString = getInputComponent(HtmlTemplate.HTML_STRING).getValue();
//		htmlString = htmlString.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");

		HtmlTemplateDataBean bean = new HtmlTemplateDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		bean.setId(getInputComponent(HtmlTemplate.ID).getValue());
		bean.setOmschrijving(getInputComponent(HtmlTemplate.OMSCHRIJVING).getValue());
		bean.setIsDefault(((CheckBox)getInputComponent(HtmlTemplate.IS_DEFAULT)).getValueAsBoolean());
		bean.setLogoHeight(((NumberField)getInputComponent(HtmlTemplate.LOGO_HEIGHT)).getIntValue());
//		bean.setHtmlString(null);
		byte[] bytes = ((FileSelectionField) getInputComponent(HtmlTemplate.LOGO)).getFileContent();
		if (bytes != null && bytes.length > 0) {
			bean.setLogo(bytes);
		}
		bytes = htmlFile.getFileContent();
		if (bytes != null && bytes.length > 0) {
			String htmlString = new String(bytes);
//			htmlString = htmlString.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");
			bean.setHtmlString(htmlString);
		}

		try {
			HtmlTemplate defaultTemplate = HtmlTemplateHelper.getStdLayout(bedrijf);
			HtmlTemplate htmlTemplate = manager.create(bean);
			if (htmlTemplate.getIsDefault() && defaultTemplate != null) {
				defaultTemplate.setIsDefault(false);
				tableToUpdate.updateRow(defaultTemplate);
			}
			return htmlTemplate;
		} catch (Exception e) {
			getInputComponent(HtmlTemplate.ID).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
//		String htmlString = getInputComponent(HtmlTemplate.HTML_STRING).getValue();
//		htmlString = htmlString.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");

		HtmlTemplate defaultTemplate = HtmlTemplateHelper.getStdLayout(bedrijf);
		HtmlTemplate htmlTemplate = (HtmlTemplate) obj;
		htmlTemplate.setOmschrijving(getInputComponent(HtmlTemplate.OMSCHRIJVING).getValue());
		htmlTemplate.setIsDefault(((CheckBox)getInputComponent(HtmlTemplate.IS_DEFAULT)).getValueAsBoolean());
		htmlTemplate.setLogoHeight(((NumberField)getInputComponent(HtmlTemplate.LOGO_HEIGHT)).getIntValue());
//		htmlTemplate.setHtmlString(htmlString);
		byte[] bytes = ((FileSelectionField) getInputComponent(HtmlTemplate.LOGO)).getFileContent();
		if (bytes != null && bytes.length > 0) {
			htmlTemplate.setLogo(bytes);
		}
		bytes = htmlFile.getFileContent();
		if (bytes != null && bytes.length > 0) {
			htmlTemplate.setHtmlString(new String(bytes));
		}
		if (htmlTemplate.getIsDefault() && !htmlTemplate.equals(defaultTemplate)) {
			defaultTemplate.setIsDefault(false);
			tableToUpdate.updateRow(defaultTemplate);
		}
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (HtmlTemplate.ID.equals(am.getName())) {
			fld = new Field("Layoutnaam");
			fld.setType(Field.TYPE_TEXT);
			fld.setMaxLength(10);
			fld.setLength(10);
			return fld;
		} else if (HtmlTemplate.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (HtmlTemplate.IS_DEFAULT.equals(am.getName())) {
			CheckBox cb = new CheckBox("Standaard");
			return cb;
		} else if (HtmlTemplate.LOGO.equals(am.getName())) {
			fld = new FileSelectionField("Bestandsnaam logo");
			fld.setDiscardLabel(true);
			return fld;
		} else if (HtmlTemplate.LOGO_HEIGHT.equals(am.getName())) {
			NumberField numFld = new NumberField("Logo-hoogte op factuur (px)", 3, false);
			numFld.setMinValue(1, true);
			numFld.setMaxValue(999, true);
			return numFld;
		} else if (HtmlTemplate.HTML_STRING.equals(am.getName())) {
			TextArea editor = new TextArea();
			editor.setDiscardLabel(true);
			editor.setLength(140);
			editor.setHeight(35);
			editor.setReadOnly(true);
			return editor;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}