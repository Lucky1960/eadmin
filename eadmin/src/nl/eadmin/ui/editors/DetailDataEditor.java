package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.HeaderData;
import nl.eadmin.ui.uiobjects.panels.DetailDataPanel;
import nl.eadmin.ui.uiobjects.tables.HeaderDataTable;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.util.DefaultEditor;

public class DetailDataEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private DetailDataPanel detailDataPanel;
	private HeaderData headerData;
	private HeaderDataTable headerDataTable;

	public DetailDataEditor(Bedrijf bedrijf, Dagboek dagboek, HeaderData headerData, HeaderDataTable headerDataTable, DetailDataManager manager) throws Exception {
		super(manager);
		this.headerData = headerData;
		this.headerDataTable = headerDataTable;
		this.detailDataPanel = new DetailDataPanel(bedrijf, dagboek, headerData, false);
		setDefaultScreen(detailDataPanel);
	}

	protected UIObject getScreenForCreate() throws Exception {
		detailDataPanel.load(null);
		return detailDataPanel;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		detailDataPanel.load((DetailData) object);
		return detailDataPanel;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		DetailData detailData = detailDataPanel.save();
		recalculateHeader();
		return detailData;
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		detailDataPanel.save();
		recalculateHeader();
	}

	public void deleteObject(Object object) throws Exception {
		super.deleteObject(object);
		recalculateHeader();
	}

	private void recalculateHeader() throws Exception {
		if (headerData != null) {
			headerData.recalculate();
		}
		if (headerDataTable != null) {
			headerDataTable.updateRow(headerData);
			headerDataTable.reloadPage();
		}
	}
}