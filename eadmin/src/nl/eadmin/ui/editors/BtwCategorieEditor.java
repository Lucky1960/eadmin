package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieDataBean;
import nl.eadmin.db.BtwCategorieManager;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.RekeningHelper;
import nl.eadmin.ui.uiobjects.referencefields.RekeningReferenceField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class BtwCategorieEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private BtwCategorieManager manager;
	private Bedrijf bedrijf;
	private Field fld;
	private RekeningReferenceField refRekening;
	private RekeningHelper rekeningHelper;

	public BtwCategorieEditor(Bedrijf bedrijf, BtwCategorieManager manager) throws Exception {
		super(manager);
		this.bedrijf = bedrijf;
		this.manager = manager;
		this.rekeningHelper = new RekeningHelper(bedrijf.getDBData());
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(BtwCategorie.CODE));
		fg1.add(getInputComponent(BtwCategorie.OMSCHRIJVING));
		FieldGroup fg2 = new FieldGroup();
		fg2.add(getInputComponent(BtwCategorie.REKENING_NR));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		return panel;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		BtwCategorieDataBean bean = new BtwCategorieDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		super.updateObject(bean, createScreen);

		try {
			BtwCategorie btwCat = manager.create(bean);
			Rekening rekening = (Rekening)refRekening.getSelectedObject();
			if (rekening == null) {
				rekeningHelper.resetBtwCategorie(bedrijf, bean.getCode());
			} else {
				rekeningHelper.checkBtwCategorie(rekening, bean.getCode());
				rekeningHelper.setBtwCategorie(rekening, bean.getCode());
			}
			return btwCat;
		} catch (Exception e) {
			getInputComponent(BtwCategorie.CODE).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		BtwCategorie btwCat = (BtwCategorie) obj;
		Rekening rekeningOld = btwCat.getRekeningObject();
		Rekening rekeningNew = (Rekening)refRekening.getSelectedObject();
		if (rekeningNew == null && rekeningOld != null) {
			Bedrijf bedrijf = GeneralHelper.getBedrijf(btwCat.getDBData(), rekeningOld.getBedrijf());
			rekeningHelper.resetBtwCategorie(bedrijf, btwCat.getCode());
		} else {
			if (rekeningNew != null) {
				rekeningHelper.checkBtwCategorie(rekeningNew, btwCat.getCode());
				rekeningHelper.setBtwCategorie(rekeningNew, btwCat.getCode());
			}
		}
		super.updateObject(obj, screen);
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (BtwCategorie.CODE.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(2);
			fld.setLength(2);
			return fld;
		} else if (BtwCategorie.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (BtwCategorie.REKENING_NR.equals(am.getName())) {
			refRekening = new RekeningReferenceField("Gekoppelde rekening", bedrijf, RekeningManagerFactory.getInstance());
			return refRekening;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}