package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.helpers.BoekingHelper;
import nl.eadmin.ui.uiobjects.panels.HeaderDataPanel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.util.DefaultEditor;

public class HeaderDataEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private HeaderDataPanel headerDataPanel;

	public HeaderDataEditor(Bedrijf bedrijf, Dagboek dagboek, HeaderDataManager manager) throws Exception {
		super(manager);
		this.headerDataPanel = new HeaderDataPanel(bedrijf, dagboek);
		setDefaultScreen(headerDataPanel);
	}

	protected UIObject getScreenForCreate() throws Exception {
		headerDataPanel.load(null);
		return headerDataPanel;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		headerDataPanel.load((HeaderData) object);
		return headerDataPanel;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		return headerDataPanel.save();
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		headerDataPanel.save();
	}
	
	public void deleteObject(Object object) throws Exception {
		HeaderData headerData = (HeaderData) object;
		BoekingHelper.delete(headerData);
	}
}