package nl.eadmin.ui.editors;

import nl.eadmin.ApplicationSetup;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Adres;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.Autorisatie;
import nl.eadmin.db.AutorisatieManagerFactory;
import nl.eadmin.db.BankImport;
import nl.eadmin.db.BankImportManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfDataBean;
import nl.eadmin.db.BedrijfManager;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.CodeTabel;
import nl.eadmin.db.CodeTabelElement;
import nl.eadmin.db.CodeTabelElementManagerFactory;
import nl.eadmin.db.CodeTabelManagerFactory;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Hoofdverdichting;
import nl.eadmin.db.HoofdverdichtingManagerFactory;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.db.Instellingen;
import nl.eadmin.db.InstellingenManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.Product;
import nl.eadmin.db.ProductManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.Verdichting;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.db.impl.GebruikerManager_Impl;
import nl.eadmin.enums.AdresTypeEnum;
import nl.eadmin.ui.uiobjects.panels.AdresPanel;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.vegas.meta.AttributeMeta;

public class BedrijfEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private BedrijfManager manager;
	private Field fld;
	private AdresPanel adresPanel1, adresPanel2;
	private CheckBox cbAdres2;
	private EventListener cbAdres2Listener;
	private Field fldDebNrMask = new Field("Opmaak debiteurennummer");
	private Field fldCredNrMask = new Field("Opmaak crediteurennummer");
	private NumberField stdBetaalTrm = new NumberField("Std. betaaltermijn (in dagen)", 14, 2);
	private NumberField rekeningLngt = new NumberField("Lengte grootboekrekeningnrs", 4, 1);
	{
		rekeningLngt.setMinValue(3, true);
		rekeningLngt.setMaxValue(9, true);
	}

	// private boolean suppressEvent;

	public BedrijfEditor(BedrijfManager manager) throws Exception {
		super(manager);
		this.manager = manager;
		this.adresPanel1 = new AdresPanel("", AdresTypeEnum.ADRESTYPE_BEZOEK, 120);
		this.adresPanel2 = new AdresPanel("", AdresTypeEnum.ADRESTYPE_POST, 120);
		this.cbAdres2Listener = new EventListener() {
			private static final long serialVersionUID = 1L;

			public void event(UIObject object, String type) throws Exception {
				adresPanel2.setHidden(!cbAdres2.getValueAsBoolean());
			}
		};
		this.cbAdres2 = new CheckBox("Er is een afwijkend postadres");
		this.cbAdres2.setOrientationCheckBFirst();
		this.cbAdres2.addOnChangeListener(cbAdres2Listener);
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(Bedrijf.BEDRIJFSCODE));
		fg1.add(getInputComponent(Bedrijf.OMSCHRIJVING));
		FieldGroup fg2 = new FieldGroup();
		fg2.add(getInputComponent(Bedrijf.EMAIL));
		fg2.add(getInputComponent(Bedrijf.KVK_NR));
		fg2.add(getInputComponent(Bedrijf.BTW_NR));
		fg2.add(getInputComponent(Bedrijf.IBAN_NR));
		fg2.add(getInputComponent(Bedrijf.BIC));
		FieldGroup fg3 = new FieldGroup();
		fg3.forceBorder(false);
		fg3.setLabelWidth(20);
		fg3.add(cbAdres2);
		FieldGroup fg4 = new FieldGroup();
		fg4.add(stdBetaalTrm);
		fg4.add(rekeningLngt);
		fg4.add(fldDebNrMask);
		fg4.add(fldCredNrMask);
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		panel.addUIObject(fg2);
		panel.addUIObject(fg4);
		panel.addUIObject(adresPanel1);
		panel.addUIObject(fg3);
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(adresPanel2);
		return panel;
	}

	protected UIObject getScreenForCreate() throws Exception {
		adresPanel1.loadFrom(null, "");
		adresPanel2.loadFrom(null, "");
		cbAdres2.setValue(false);
		cbAdres2Listener.event(null, "");
		stdBetaalTrm.setValue(14);
		rekeningLngt.setValue(04);
		fldDebNrMask.setValue("D#####");
		fldCredNrMask.setValue("C#####");
		return super.getScreenForCreate();
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) object;
		adresPanel1.loadFrom(bedrijf, "");
		adresPanel2.loadFrom(bedrijf, "");
		cbAdres2.setValue(!adresPanel2.isEmpty());
		cbAdres2Listener.event(null, "");
		Instellingen settings = InstellingenManagerFactory.getInstance().findOrCreate(bedrijf.getBedrijfscode());
		stdBetaalTrm.setValue(settings.getStdBetaalTermijn());
		rekeningLngt.setValue(settings.getLengteRekening());
		fldDebNrMask.setValue(settings.getMaskDebNr());
		fldCredNrMask.setValue(settings.getMaskCredNr());
		return super.getScreenForUpdate(object);
	}

	protected UIObject getScreenForCopy(Object object) throws Exception {
		adresPanel1.loadFrom((Bedrijf) object, "");
		adresPanel2.loadFrom((Bedrijf) object, "");
		return super.getScreenForCopy(object);
	}

	protected UIObject getScreenForDetails(Object object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) object;
		adresPanel1.loadFrom(bedrijf, "");
		adresPanel2.loadFrom(bedrijf, "");
		return super.getScreenForDetails(object);
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		BedrijfDataBean bean = new BedrijfDataBean();
		super.updateObject(bean, createScreen);
		if (!cbAdres2.getBoolean()) {
			adresPanel2.loadFrom(null, "");// Wis
		}

		try {
			Bedrijf bedrijf = manager.create(bean);
			Gebruiker gebruiker = (Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
			adresPanel1.saveTo(bedrijf, "");
			adresPanel2.saveTo(bedrijf, "");
			Instellingen settings = InstellingenManagerFactory.getInstance().findOrCreate(bedrijf.getBedrijfscode());
			settings.setStdBetaalTermijn(stdBetaalTrm.getIntValue());
			settings.setLengteRekening(rekeningLngt.getIntValue());
			settings.setMaskDebNr(fldDebNrMask.getValue());
			settings.setMaskCredNr(fldCredNrMask.getValue());
			new ApplicationSetup().initBedrijf(bedrijf, gebruiker);
			return bedrijf;
		} catch (Exception e) {
			getInputComponent(Bedrijf.BEDRIJFSCODE).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		super.updateObject(obj, screen);
		Bedrijf bedrijf = (Bedrijf) obj;
		if (!cbAdres2.getBoolean()) {
			adresPanel2.loadFrom(null, "");// Wis
		}
		adresPanel1.saveTo(bedrijf, "");
		adresPanel2.saveTo(bedrijf, "");
		Instellingen settings = InstellingenManagerFactory.getInstance().findOrCreate(bedrijf.getBedrijfscode());
		settings.setStdBetaalTermijn(stdBetaalTrm.getIntValue());
		settings.setLengteRekening(rekeningLngt.getIntValue());
		settings.setMaskDebNr(fldDebNrMask.getValue());
		settings.setMaskCredNr(fldCredNrMask.getValue());
	}

	public void deleteObject(Object object) throws Exception {
		String bedrijfscode = ((Bedrijf) object).getBedrijfscode();
		AdresManagerFactory.getInstance().generalDelete(Adres.BEDRIJF + "='" + bedrijfscode + "'");
		AutorisatieManagerFactory.getInstance().generalDelete(Autorisatie.BEDRIJF + "='" + bedrijfscode + "'");
		BankImportManagerFactory.getInstance().generalDelete(BankImport.BEDRIJF + "='" + bedrijfscode + "'");
		BijlageManagerFactory.getInstance().generalDelete(Bijlage.BEDRIJF + "='" + bedrijfscode + "'");
		BtwCodeManagerFactory.getInstance().generalDelete(BtwCode.BEDRIJF + "='" + bedrijfscode + "'");
		CodeTabelManagerFactory.getInstance().generalDelete(CodeTabel.BEDRIJF + "='" + bedrijfscode + "'");
		CodeTabelElementManagerFactory.getInstance().generalDelete(CodeTabelElement.BEDRIJF + "='" + bedrijfscode + "'");
		CrediteurManagerFactory.getInstance().generalDelete(Crediteur.BEDRIJF + "='" + bedrijfscode + "'");
		DagboekManagerFactory.getInstance().generalDelete(Dagboek.BEDRIJF + "='" + bedrijfscode + "'");
		DebiteurManagerFactory.getInstance().generalDelete(Debiteur.BEDRIJF + "='" + bedrijfscode + "'");
		DetailDataManagerFactory.getInstance().generalDelete(DetailData.BEDRIJF + "='" + bedrijfscode + "'");
		HeaderDataManagerFactory.getInstance().generalDelete(HeaderData.BEDRIJF + "='" + bedrijfscode + "'");
		HoofdverdichtingManagerFactory.getInstance().generalDelete(Hoofdverdichting.BEDRIJF + "='" + bedrijfscode + "'");
		HtmlTemplateManagerFactory.getInstance().generalDelete(HtmlTemplate.BEDRIJF + "='" + bedrijfscode + "'");
		InstellingenManagerFactory.getInstance().generalDelete(Instellingen.BEDRIJF + "='" + bedrijfscode + "'");
		JournaalManagerFactory.getInstance().generalDelete(Journaal.BEDRIJF + "='" + bedrijfscode + "'");
		PeriodeManagerFactory.getInstance().generalDelete(Periode.BEDRIJF + "='" + bedrijfscode + "'");
		ProductManagerFactory.getInstance().generalDelete(Product.BEDRIJF + "='" + bedrijfscode + "'");
		RekeningManagerFactory.getInstance().generalDelete(Rekening.BEDRIJF + "='" + bedrijfscode + "'");
		VerdichtingManagerFactory.getInstance().generalDelete(Verdichting.BEDRIJF + "='" + bedrijfscode + "'");
		super.deleteObject(object);
	}

	public boolean allowDelete(Object object) throws Exception {
		Bedrijf bedrijf = (Bedrijf) object;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Gebruiker WHERE ");
		sb.append(Gebruiker.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		FreeQuery qry = QueryFactory.createFreeQuery((GebruikerManager_Impl) GebruikerManagerFactory.getInstance(), sb.toString());
		return qry.getFirstObject() == null;
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (Bedrijf.BEDRIJFSCODE.equals(am.getName())) {
			fld = new Field("Administratiecode");
			fld.setType(Field.TYPE_UPPER);
			fld.setMaxLength(10);
			fld.setLength(10);
			return fld;
		} else if (Bedrijf.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Bedrijf.EMAIL.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else if (Bedrijf.KVK_NR.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setLabel("KVK-nummer");
			fld.setMaxLength(15);
			fld.setLength(15);
			return fld;
		} else if (Bedrijf.BTW_NR.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setLabel("BTW-nummer");
			fld.setMaxLength(18);
			fld.setLength(18);
			return fld;
		} else if (Bedrijf.IBAN_NR.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setLabel("IBAN-nummer");
			fld.setMaxLength(18);
			fld.setLength(18);
			return fld;
		} else if (Bedrijf.BIC.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setLabel("BIC-code");
			fld.setType(Field.TYPE_UPPER);
			fld.setMaxLength(15);
			fld.setLength(15);
			return fld;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}