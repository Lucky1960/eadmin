package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageDataBean;
import nl.eadmin.db.BijlageManager;
import nl.eadmin.helpers.GeneralHelper;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.FileSelectionField;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;

public class BijlageEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private BijlageManager manager;
	private Bedrijf bedrijf;
	private String dagboekId, boekstuk;
	private FileSelectionField filename;

	public BijlageEditor(DataObject dataObject, BijlageManager manager, Bedrijf bedrijf, String dagboekId, String boekstuk) throws Exception {
		super(manager);
		this.manager = manager;
		this.bedrijf = bedrijf;
		this.dagboekId = dagboekId;
		this.boekstuk = boekstuk;
		this.filename = new FileSelectionField("Bestandsnaam bijlage");
		this.filename.setMandatory(true);
	}

	protected UIObject getScreenForCreate() throws Exception {
		Panel panel = new Panel();
		panel.addUIObject(filename);
		return panel;
	}

	protected UIObject getScreenForUpdate(Object object) throws Exception {
		Bijlage bijlage = (Bijlage) object;
		InputComponent cmp = getInputComponent(Bijlage.OMSCHRIJVING);
		cmp.setValueAsString(bijlage.getOmschrijving());
		cmp.setMandatory(true);
		Panel panel = new Panel();
		panel.addUIObject(cmp);
		return panel;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		BijlageDataBean bean = new BijlageDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		bean.setDagboekId(dagboekId);
		bean.setBoekstuk(boekstuk);
		bean.setVolgNr(GeneralHelper.genereerNieuwBijlageNummer(bedrijf, dagboekId, boekstuk));
		bean.setBestandsnaam(filename.getFileName());
		bean.setOmschrijving(filename.getFileName());
		bean.setData(filename.getFileContent());
		try {
			Bijlage bijlage = manager.create(bean);
			return bijlage;
		} catch (Exception e) {
			filename.setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	public void updateObject(Object obj, UIObject screen) throws Exception {
		Bijlage bijlage = (Bijlage) obj;
		bijlage.setOmschrijving(getInputComponent(Bijlage.OMSCHRIJVING).getValue());
	}
}