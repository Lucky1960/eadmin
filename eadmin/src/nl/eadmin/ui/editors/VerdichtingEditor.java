package nl.eadmin.ui.editors;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Hoofdverdichting;
import nl.eadmin.db.HoofdverdichtingManagerFactory;
import nl.eadmin.db.Verdichting;
import nl.eadmin.db.VerdichtingDataBean;
import nl.eadmin.db.VerdichtingManager;
import nl.eadmin.ui.uiobjects.referencefields.HoofdverdichtingReferenceField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.DefaultEditor;
import nl.ibs.vegas.meta.AttributeMeta;

public class VerdichtingEditor extends DefaultEditor {
	private static final long serialVersionUID = 1L;
	private VerdichtingManager manager;
	private Bedrijf bedrijf;
	private Field fld;

	public VerdichtingEditor(Bedrijf bedrijf, VerdichtingManager manager) throws Exception {
		super(manager);
		this.bedrijf = bedrijf;
		this.manager = manager;
		setDefaultScreen(createDefaultPanel());
	}

	private Panel createDefaultPanel() throws Exception {
		FieldGroup fg1 = new FieldGroup();
		fg1.add(getInputComponent(Verdichting.ID));
		fg1.add(getInputComponent(Verdichting.OMSCHRIJVING));
		fg1.add(getInputComponent(Verdichting.HOOFDVERDICHTING));
		Panel panel = new Panel();
		panel.addUIObject(fg1);
		return panel;
	}

	public Object getCreatedObject(UIObject createScreen) throws Exception {
		VerdichtingDataBean bean = new VerdichtingDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		super.updateObject(bean, createScreen);
		
		try {
			return manager.create(bean);
		} catch (Exception e) {
			getInputComponent(Verdichting.ID).setInvalidTag();
			throw new UserMessageException("object_exists");
		}
	}

	protected InputComponent getNewInputComponent(AttributeMeta am) throws Exception {
		if (Hoofdverdichting.ID.equals(am.getName())) {
			fld = new NumberField("Verdichtingsnummer", 3, false);
			return fld;
		} else if (Verdichting.HOOFDVERDICHTING.equals(am.getName())) {
			return new HoofdverdichtingReferenceField("Hoofdverdichting", bedrijf, HoofdverdichtingManagerFactory.getInstance());
		} else if (Verdichting.OMSCHRIJVING.equals(am.getName())) {
			fld = (Field) super.getNewInputComponent(am);
			fld.setMaxLength(50);
			fld.setLength(50);
			return fld;
		} else {
			return super.getNewInputComponent(am);
		}
	}
}