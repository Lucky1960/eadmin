package nl.eadmin.ui.datareaders;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import nl.eadmin.databeans.VrijeRubriekDefinitie;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.HeaderData;
import nl.eadmin.enums.VrijeRubriekSoortEnum;
import nl.eadmin.helpers.VrijeRubriekHelper;
import nl.ibs.esp.uiobjects.Image;
import nl.ibs.esp.util.CommonObjectDataReader;

public class HeaderDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = -1004651373825835934L;
	public static final String DEBT_NAAM = "debiteurNaam";
	public static final String CRED_NAAM = "crediteurNaam";
	public static final String BIJLAGE = "bijlage";
	// private static HeaderDataReader instance;
	private VrijeRubriekDefinitie[] definitie;

	// public static HeaderDataReader getInstance() throws Exception {
	// if (instance == null)
	// instance = new HeaderDataReader();
	// return instance;
	// }

	public HeaderDataReader(Dagboek dagboek) throws Exception {
		super(HeaderData.class);
		if (dagboek != null) {
			definitie = VrijeRubriekHelper.getVrijeRubriekDefinities(dagboek);
		}
	}

	public Object getValue(Object object, String key) throws Exception {
		HeaderData headerData = (HeaderData) object;
		if (key.equals(DEBT_NAAM)) {
			DebiteurPK debKey = new DebiteurPK();
			debKey.setBedrijf(headerData.getBedrijf());
			debKey.setDebNr(headerData.getDcNummer());
			try {
				return DebiteurManagerFactory.getInstance(headerData.getDBData()).findByPrimaryKey(debKey).getNaam();
			} catch (Exception e) {
				return "???";
			}
		} else if (key.equals(CRED_NAAM)) {
			CrediteurPK credKey = new CrediteurPK();
			credKey.setBedrijf(headerData.getBedrijf());
			credKey.setCredNr(headerData.getDcNummer());
			try {
				return CrediteurManagerFactory.getInstance(headerData.getDBData()).findByPrimaryKey(credKey).getNaam();
			} catch (Exception e) {
				return "???";
			}
		} else if (key.equals(BIJLAGE)) {
			return headerData;
		} else if (key.equals(HeaderData.FAVORIET)) {
			Image img = null;
			if (headerData.getFavoriet()) {
				img = new Image("");
				img.setAlignment("center");
				img.setSource("asterisk_yellow.png");
			}
			return img;
		} else if (key.startsWith("vrijeRub") && definitie != null) {
			int index = Integer.parseInt(key.substring(8));
			String dataType = definitie[index - 1].getDataType();
			Method method = headerData.getClass().getMethod("getVrijeRub" + index, (Class[]) null);
			String val = (String) method.invoke(headerData, (Object[]) null);
			switch (dataType) {
			case VrijeRubriekSoortEnum.BOOL:
				return val.equals(Boolean.TRUE.toString()) ? Boolean.TRUE : Boolean.FALSE;
			case VrijeRubriekSoortEnum.DATUM:
				Date date = null;
				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(val);
				} catch (Exception e) {
				}
				return date;
			case VrijeRubriekSoortEnum.DEC:
				BigDecimal dec = new BigDecimal(0.00);
				try {
					Double d = new Double(val) / 100;
					dec = new BigDecimal(d).setScale(2, BigDecimal.ROUND_HALF_UP);
				} catch (Exception e) {
				}
				return dec;
			case VrijeRubriekSoortEnum.NUM:
				Long nr = 0l;
				try {
					nr = Long.valueOf(val);
				} catch (Exception e) {
				}
				return nr;
			default:
				return val;
			}
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(DEBT_NAAM) || key.equals(CRED_NAAM)) {
			return String.class;
		} else if (key.equals(BIJLAGE)) {
			return HeaderData.class;
		} else if (key.startsWith("vrijeRub") && definitie != null) {
			int index = Integer.parseInt(key.substring(8));
			String dataType = definitie[index - 1].getDataType();
			switch (dataType) {
			case VrijeRubriekSoortEnum.BOOL:
				return Boolean.class;
			case VrijeRubriekSoortEnum.DATUM:
				return Date.class;
			case VrijeRubriekSoortEnum.DEC:
				return BigDecimal.class;
			case VrijeRubriekSoortEnum.NUM:
				return Long.class;
			default:
				return String.class;
			}
		} else if (key.equals(HeaderData.FAVORIET)) {
			return Image.class;
		} else {
			return super.getType(key);
		}
	}
}