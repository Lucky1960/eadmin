package nl.eadmin.ui.datareaders;

import nl.eadmin.db.Journaal;
import nl.ibs.esp.util.CommonObjectDataReader;

public class JournaalDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = 2596856469273754457L;
	public static final String XXXXXX = "xxxxx";
	private static JournaalDataReader instance;

	public static JournaalDataReader getInstance() throws Exception {
		if (instance == null)
			instance = new JournaalDataReader();
		return instance;
	}

	public JournaalDataReader() throws Exception {
		super(Journaal.class);
	}

	public Object getValue(Object object, String key) throws Exception {
		Journaal journaal = (Journaal) object;
		if (key.equals(Journaal.REKENING_NR)) {
			return journaal.getRekeningNr() + " - " + journaal.getRekeningOmschr();
		} else if (key.equals(Journaal.BTW_CATEGORIE)) {
				if (journaal.getBtwCategorie().length() == 0) {
					return "";
				} else {
					return journaal.getBtwCategorie() + " (" + (journaal.getBtwAangifteKolom().equals("O") ? "Omz" : "Btw") + ")";
				}
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(XXXXXX)) {
			return String.class;
		} else {
			return super.getType(key);
		}
	}
}