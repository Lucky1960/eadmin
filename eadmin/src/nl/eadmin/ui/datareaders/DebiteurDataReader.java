package nl.eadmin.ui.datareaders;

import nl.eadmin.db.Debiteur;
import nl.ibs.esp.util.CommonObjectDataReader;

public class DebiteurDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = 8572916440747191271L;
	public static final String XXXXXX = "xxxxx";
	private static DebiteurDataReader instance;

	public static DebiteurDataReader getInstance() throws Exception {
		if (instance == null)
			instance = new DebiteurDataReader();
		return instance;
	}

	public DebiteurDataReader() throws Exception {
		super(Debiteur.class);
	}

	public Object getValue(Object object, String key) throws Exception {
//		Debiteur debiteur = (Debiteur) object;
		if (key.equals(XXXXXX)) {
			return "xxxxxx";
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(XXXXXX)) {
			return String.class;
		} else {
			return super.getType(key);
		}
	}
}