package nl.eadmin.ui.datareaders;

import nl.eadmin.db.Adres;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.AdresPK;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.enums.AdresTypeEnum;
import nl.ibs.esp.util.CommonObjectDataReader;

public class BedrijfDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = 8572916440747191271L;
	public static final String ADRESLINE1 = "Bedrijf_Adres1";
	public static final String ADRESLINE2 = "Bedrijf_Adres2";
	public static final String PLAATS = "Bedrijf_Plaats";
	private static BedrijfDataReader instance;
	private String bedrijfsCodeOLD = null;
	private Adres myAddress = null;

	public static BedrijfDataReader getInstance() throws Exception {
		if (instance == null)
			instance = new BedrijfDataReader();
		return instance;
	}

	public BedrijfDataReader() throws Exception {
		super(Bedrijf.class);
	}

	public Object getValue(Object object, String key) throws Exception {
		Bedrijf bedrijf = (Bedrijf) object;
		if (!bedrijf.getBedrijfscode().equals(bedrijfsCodeOLD)) {
			bedrijfsCodeOLD = bedrijf.getBedrijfscode();
			myAddress = getAddress(bedrijf);
		}
		if (key.equals(PLAATS)) {
			return myAddress.getPlaats();
		} else if (key.equals(ADRESLINE1)) {
			return myAddress.getAdresRegel1();
		} else if (key.equals(ADRESLINE2)) {
			return myAddress.getAdresRegel2();
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(PLAATS) || key.equals(ADRESLINE1) || key.equals(ADRESLINE2)) {
			return String.class;
		} else {
			return super.getType(key);
		}
	}

	private Adres getAddress(Bedrijf bedrijf) throws Exception {
		AdresPK key = new AdresPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setDc("");
		key.setDcNr("");
		key.setAdresType(AdresTypeEnum.ADRESTYPE_BEZOEK);
		return AdresManagerFactory.getInstance(bedrijf.getDBData()).findOrCreate(key);
	}
}