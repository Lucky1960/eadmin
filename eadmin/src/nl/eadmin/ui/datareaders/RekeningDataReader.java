package nl.eadmin.ui.datareaders;

import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.impl.BtwCategorieManager_Impl;
import nl.ibs.esp.util.CommonObjectDataReader;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.QueryFactory;

public class RekeningDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = 6512804612215386507L;
	public static final String BTW_CAT = "BtwCat";
	private static RekeningDataReader instance;

	public static RekeningDataReader getInstance() throws Exception {
		if (instance == null)
			instance = new RekeningDataReader();
		return instance;
	}

	public RekeningDataReader() throws Exception {
		super(Rekening.class);
	}

	public Object getValue(Object object, String key) throws Exception {
		Rekening rekening = (Rekening) object;
		if (key.equals(BTW_CAT)) {
			StringBuilder filter = new StringBuilder();
			filter.append(BtwCategorie.BEDRIJF + "='" + rekening.getBedrijf() + "' AND ");
			filter.append(BtwCategorie.REKENING_NR + "='" + rekening.getRekeningNr() + "'");
			ExecutableQuery qry = QueryFactory.create((BtwCategorieManager_Impl) BtwCategorieManagerFactory.getInstance(rekening.getDBData()), filter.toString());
			try {
				return ((BtwCategorie) qry.getFirstObject()).getCode();
			} catch (Exception e) {
				return "";
			}
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(BTW_CAT)) {
			return String.class;
		} else {
			return super.getType(key);
		}
	}
}