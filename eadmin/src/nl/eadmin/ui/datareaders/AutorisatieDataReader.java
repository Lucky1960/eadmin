package nl.eadmin.ui.datareaders;

import nl.eadmin.db.Autorisatie;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.ibs.esp.util.CommonObjectDataReader;

public class AutorisatieDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = -1787377330623804695L;
	public static final String GEBRUIKER_NAAM = "GebruikersNaam";
	private static AutorisatieDataReader instance;

	public static AutorisatieDataReader getInstance() throws Exception {
		if (instance == null)
			instance = new AutorisatieDataReader();
		return instance;
	}

	public AutorisatieDataReader() throws Exception {
		super(Autorisatie.class);
	}

	public Object getValue(Object object, String key) throws Exception {
		Autorisatie aut = (Autorisatie) object;
		if (key.equals(GEBRUIKER_NAAM)) {
			try {
				return GebruikerManagerFactory.getInstance().findByPrimaryKey(aut.getGebruikerId()).getNaam();
			} catch (Exception e) {
				return aut.getGebruikerId() + " - ???";
			}
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(GEBRUIKER_NAAM)) {
			return String.class;
		} else {
			return super.getType(key);
		}
	}
}