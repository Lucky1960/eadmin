package nl.eadmin.ui.datareaders;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.util.CommonObjectDataReader;
import nl.sf.api.Environment;
import nl.sf.api.InitializationRequest;
import nl.sf.api.UserData;

public class GebruikerDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = -1787377330623804695L;
	public static final String XXXXXX = "xxxxxx";
	private static GebruikerDataReader instance;
	private Environment environment;
	private UserData userData = null;
	private String userId_OLD = null;

	public static GebruikerDataReader getInstance() throws Exception {
		if (instance == null)
			instance = new GebruikerDataReader();
		return instance;
	}

	public GebruikerDataReader() throws Exception {
		super(Gebruiker.class);
		InitializationRequest request = (InitializationRequest) ESPSessionContext.getSessionAttribute(SessionKeys.INIT_REQUEST);
		if (request == null) {
			environment = null;
		} else {
			environment = request.getEnvironment();
		}
	}

	public Object getValue(Object object, String key) throws Exception {
		Gebruiker gebruiker = (Gebruiker) object;
		String userId_NEW = gebruiker.getId();
		if (environment != null && !userId_NEW.equals(userId_OLD)) {
			userData = environment.getUserData(userId_NEW);
			userId_OLD = userId_NEW;
		}
		if (key.equals(Gebruiker.BEDRIJF)) {
			try {
				return BedrijfManagerFactory.getInstance(gebruiker.determineDBData()).findByPrimaryKey(gebruiker.getBedrijf()).getOmschrijvingLang();
			} catch (Exception e) {
				return gebruiker.getBedrijf() + " - ???";
			}
		} else if (key.equals(Gebruiker.NAAM)) {
			return userData != null ? userData.getFirstName() + " " + userData.getLastName() : gebruiker.getNaam();
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(Gebruiker.BEDRIJF)) {
			return String.class;
		} else {
			return super.getType(key);
		}
	}
}