package nl.eadmin.ui.datareaders;

import nl.eadmin.db.Crediteur;
import nl.ibs.esp.util.CommonObjectDataReader;

public class CrediteurDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = -1004651373825835934L;
	public static final String XXXXXX = "xxxxx";
	private static CrediteurDataReader instance;

	public static CrediteurDataReader getInstance() throws Exception {
		if (instance == null)
			instance = new CrediteurDataReader();
		return instance;
	}

	public CrediteurDataReader() throws Exception {
		super(Crediteur.class);
	}

	public Object getValue(Object object, String key) throws Exception {
//		Debiteur debiteur = (Debiteur) object;
		if (key.equals(XXXXXX)) {
			return "xxxxxx";
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(XXXXXX)) {
			return String.class;
		} else {
			return super.getType(key);
		}
	}
}