package nl.eadmin.ui.datareaders;

import nl.eadmin.db.Dagboek;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.ibs.esp.util.CommonObjectDataReader;

public class DagboekDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = 8283859071801632678L;
	public static final String XXXXXX = "xxxxx";
	private static DagboekDataReader instance;

	public static DagboekDataReader getInstance() throws Exception {
		if (instance == null)
			instance = new DagboekDataReader();
		return instance;
	}

	public DagboekDataReader() throws Exception {
		super(Dagboek.class);
	}

	public Object getValue(Object object, String key) throws Exception {
		Dagboek gebruiker = (Dagboek) object;
		if (key.equals(Dagboek.SOORT)) {
			return DagboekSoortEnum.getValue(gebruiker.getSoort());
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(Dagboek.SOORT)) {
			return String.class;
		} else {
			return super.getType(key);
		}
	}
}