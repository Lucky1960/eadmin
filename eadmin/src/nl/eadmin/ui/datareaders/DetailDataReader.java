package nl.eadmin.ui.datareaders;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import nl.eadmin.databeans.VrijeRubriekDefinitie;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.Rekening;
import nl.eadmin.enums.VrijeRubriekSoortEnum;
import nl.eadmin.helpers.VrijeRubriekHelper;
import nl.ibs.esp.util.CommonObjectDataReader;
import nl.ibs.jsql.DBData;

public class DetailDataReader extends CommonObjectDataReader {
	private static final long serialVersionUID = -5641159351327712848L;
	public static final String DC_NAAM = "debCredNaam";
	public static final String DEBT_NAAM = "debiteurNaam";
	public static final String CRED_NAAM = "crediteurNaam";
	private VrijeRubriekDefinitie[] definitie;
	private DBData dbd;

	public DetailDataReader(Dagboek dagboek) throws Exception {
		super(DetailData.class);
		this.dbd = dagboek.getDBData();
		if (dagboek != null) {
			definitie = VrijeRubriekHelper.getVrijeRubriekDefinities(dagboek);
		}
	}

	public Object getValue(Object object, String key) throws Exception {
		DetailData detailData = (DetailData) object;
		if (key.equals(DC_NAAM)) {
			Rekening rekening = detailData.getRekeningObject();
			String subAdm = (rekening == null ? "" : rekening.getSubAdministratieType());
			if (rekening == null || subAdm.length() == 0) {
				return "";
			} else if (subAdm.equals("D")) {
				Debiteur debiteur = getDebiteur(detailData.getBedrijf(), detailData.getDcNummer());
				return debiteur == null ? "???" : debiteur.getNaam();
			} else {
				Crediteur crediteur = getCrediteur(detailData.getBedrijf(), detailData.getDcNummer());
				return crediteur == null ? "???" : crediteur.getNaam();
			}
		} else if (key.equals(DEBT_NAAM)) {
			Debiteur debiteur = getDebiteur(detailData.getBedrijf(), detailData.getDcNummer());
			return debiteur == null ? "???" : debiteur.getNaam();
		} else if (key.equals(CRED_NAAM)) {
			Crediteur crediteur = getCrediteur(detailData.getBedrijf(), detailData.getDcNummer());
			return crediteur == null ? "???" : crediteur.getNaam();
		} else if (key.startsWith("vrijeRub") && definitie != null) {
			int index = Integer.parseInt(key.substring(8));
			String dataType = definitie[index - 1].getDataType();
			Method method = detailData.getClass().getMethod("getVrijeRub" + index, (Class[]) null);
			String val = (String) method.invoke(detailData, (Object[]) null);
			switch (dataType) {
			case VrijeRubriekSoortEnum.BOOL:
				return val.equals(Boolean.TRUE.toString()) ? Boolean.TRUE : Boolean.FALSE;
			case VrijeRubriekSoortEnum.DATUM:
				Date date = null;
				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(val);
				} catch (Exception e) {
				}
				return date;
			case VrijeRubriekSoortEnum.DEC:
				BigDecimal dec = new BigDecimal(0.00);
				try {
					Double d = new Double(val) / 100;
					dec = new BigDecimal(d).setScale(2, BigDecimal.ROUND_HALF_UP);
				} catch (Exception e) {
				}
				return dec;
			case VrijeRubriekSoortEnum.NUM:
				Long nr = 0l;
				try {
					nr = Long.valueOf(val);
				} catch (Exception e) {
				}
				return nr;
			default:
				return val;
			}
		} else {
			return super.getValue(object, key);
		}
	}

	public Class<?> getType(String key) throws Exception {
		if (key.equals(DC_NAAM) || key.equals(DEBT_NAAM) || key.equals(CRED_NAAM)) {
			return String.class;
		} else if (key.startsWith("vrijeRub") && definitie != null) {
			int index = Integer.parseInt(key.substring(8));
			String dataType = definitie[index - 1].getDataType();
			switch (dataType) {
			case VrijeRubriekSoortEnum.BOOL:
				return Boolean.class;
			case VrijeRubriekSoortEnum.DATUM:
				return Date.class;
			case VrijeRubriekSoortEnum.DEC:
				return BigDecimal.class;
			case VrijeRubriekSoortEnum.NUM:
				return Long.class;
			default:
				return String.class;
			}
		} else {
			return super.getType(key);
		}
	}

	private Debiteur getDebiteur(String bedrijf, String dcNr) throws Exception {
		DebiteurPK debKey = new DebiteurPK();
		debKey.setBedrijf(bedrijf);
		debKey.setDebNr(dcNr);
		try {
			return DebiteurManagerFactory.getInstance(dbd).findByPrimaryKey(debKey);
		} catch (Exception e) {
			return null;
		}
	}

	private Crediteur getCrediteur(String bedrijf, String dcNr) throws Exception {
		CrediteurPK key = new CrediteurPK();
		key.setBedrijf(bedrijf);
		key.setCredNr(dcNr);
		try {
			return CrediteurManagerFactory.getInstance(dbd).findByPrimaryKey(key);
		} catch (Exception e) {
			return null;
		}
	}
}