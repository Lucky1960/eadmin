package nl.eadmin.ui.uiobjects;

import java.util.Iterator;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.impl.PeriodeManager_Impl;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class ComboBoekjaar extends ComboBox {
	private static final long serialVersionUID = 6734186481147617534L;

	public ComboBoekjaar(Bedrijf bedrijf) throws Exception {
		super("Boekjaar");
		setWidth("60");

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Periode");
		sb.append(" WHERE BDR = '" + bedrijf.getBedrijfscode() + "'");
		sb.append(" GROUP BY BOEKJAAR");

		FreeQuery qry = QueryFactory.createFreeQuery((PeriodeManager_Impl) PeriodeManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		Iterator<?> jaren = qry.getCollection().iterator();
		while (jaren.hasNext()) {
			String jaar = "" + ((Periode) jaren.next()).getBoekjaar();
			addOption(jaar, jaar);
		}

		Integer huidigJaar = (Integer)ESPSessionContext.getSessionAttribute(SessionKeys.BOEKJAAR);
		if (huidigJaar != null) {
			setSelectedOptionValue(String.valueOf(huidigJaar));
		}
	}
}