package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.uiobjects.referencefields.HtmlTemplateReferenceField;
import nl.eadmin.ui.uiobjects.tables.HeaderDataTable;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Window;

public class ChangeLayoutAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;
	private Window window;
	private HeaderData headerData;
	private HeaderDataTable headerDataTable;
	private Bedrijf bedrijf;

	public ChangeLayoutAction(Bedrijf bedrijf, HeaderDataTable headerDataTable) {
		super("Wijzig layout");
		setIcon("html.png");
		this.bedrijf = bedrijf;
		this.headerDataTable = headerDataTable;
	}

	public boolean execute(DataObject object) throws Exception {
		headerData = (HeaderData) GeneralHelper.getSelectedItem(headerDataTable, object);
		headerDataTable.unSelect(headerData);
		final HtmlTemplateReferenceField refLayout = new HtmlTemplateReferenceField("Factuurlayout", bedrijf, HtmlTemplateManagerFactory.getInstance(bedrijf.getDBData()));
		refLayout.setMandatory(true);
		refLayout.setValue(headerData.getFactuurLayout());

		FloatBar fb = new FloatBar();
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				headerData.setFactuurLayout(refLayout.getValue());
				window.closeWindow(object);
				return true;
			}
		};
		Action cancel = new Action("Terug") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				window.closeWindow(object);
				return true;
			}
		};
		fb.addAction(ok);
		fb.addAction(cancel);

		Panel panel = new Panel();
		panel.addUIObject(refLayout);
		window = new Window();
		window.setLabel("Wijzigen layout");
		window.add(panel);
		window.add(fb);

		object.addUIObject(window);
		return true;
	}
}