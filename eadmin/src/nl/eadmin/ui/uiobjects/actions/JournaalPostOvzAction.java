package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.ui.uiobjects.windows.JournaalPostOvzWindow;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;

public class JournaalPostOvzAction extends Action {
	private static final long serialVersionUID = 1102088107981020738L;

	public JournaalPostOvzAction() {
		this("Journaalposten");
	}

	public JournaalPostOvzAction(String label) {
		super(label);
		setIcon("report.png");
	}

	public boolean execute(DataObject object) throws Exception {
		object.addUIObject(new JournaalPostOvzWindow());
		return true;
	}
}