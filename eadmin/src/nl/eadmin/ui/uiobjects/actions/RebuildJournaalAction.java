package nl.eadmin.ui.uiobjects.actions;

import java.util.Iterator;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.impl.HeaderDataManager_Impl;
import nl.eadmin.helpers.JournaalHelper;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class RebuildJournaalAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;

	public RebuildJournaalAction() {
		this("Reset journaalposten");
	}

	public RebuildJournaalAction(String label) {
		super(label);
		setIcon("report.png");
	}

	public boolean execute(DataObject object) throws Exception {
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		JournaalManagerFactory.getInstance(dbd).generalDelete(Journaal.BEDRIJF + "<>''");
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM HeaderData");
		FreeQuery qry = QueryFactory.createFreeQuery((HeaderDataManager_Impl) HeaderDataManagerFactory.getInstance(dbd), sb.toString());
		Iterator<?> iter = qry.getCollection().iterator();
		while (iter.hasNext()) {
			new JournaalHelper(dbd).createJournals((HeaderData) iter.next(), false);
		}
		return super.execute(object);
	}
}