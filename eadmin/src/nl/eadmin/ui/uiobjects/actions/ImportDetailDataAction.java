package nl.eadmin.ui.uiobjects.actions;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataDataBean;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.jsql.DBData;

public class ImportDetailDataAction extends ImportAction {
	private static final long serialVersionUID = -9051528883630614051L;
	private DetailDataManager manager;
	private DBData dbd;

	public ImportDetailDataAction() {
		super("Boekingen-Detailgegevens", "page_edit.png");
		this.dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		this.manager = DetailDataManagerFactory.getInstance(dbd);
	}

	protected String[] getVoorloopRecord() throws Exception {
		return new String[] { "Dagboek", "Boekstuk", "DebCredNr", "FactuurNr", "LeverDtm", "Omschr1", "Omschr2", "Omschr3", "Aantal", "Eenheid", "Prijs", "BtwCode", "BedragExcl", "BedragBtw",
				"BedragIncl", "DC", "GrootboekRek" };
	}

	protected String[] getRecordMask() throws Exception {
		return new String[] { "A(10)", "A(15)", "A(10)", "A(15)", "D(10)", "A(50)", "A(50)", "A(50)", "D(5,2)", "A(10)", "D(15,2)", "A(3)", "D(15,2)", "D(15,2)", "D(15,2)", "A(1)", "A(9)" };
	}

	protected void deleteCurrentData() throws Exception {
		manager.generalDelete(DetailData.BEDRIJF + "='" + bedrijfsCode + "'");
	}

	protected StringBuilder importeer(String[] record, StringBuilder report) throws Exception {
		DetailDataDataBean bean = new DetailDataDataBean();
		bean.setBedrijf(bedrijfsCode);
		bean.setVrijeRub1("");
		bean.setVrijeRub2("");
		bean.setVrijeRub3("");
		bean.setVrijeRub4("");
		bean.setVrijeRub5("");

		int x = 0;
		Date f_leverdatum;
		BigDecimal f_aantal, f_prijs, f_bedragExcl, f_bedragBtw, f_bedragIncl;
		String f_dagboekId, f_boekstuk, f_eenheid, f_btwCode, f_dcNr, f_factNr, f_dc, f_rekening, f_omschr1, f_omschr2, f_omschr3;
		String[] splitValues = null;
		for (int r = 1; r < record.length; r++) {
			x = 0;
			splitValues = fill(record[r].split(separator.getValue()));
			f_dagboekId = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_boekstuk = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_dcNr = splitValues[x++].replace('"', ' ').trim();
			f_factNr = splitValues[x++].replace('"', ' ').trim();
			f_leverdatum = dateValue(splitValues[x++].replace('"', ' ').trim());
			f_omschr1 = splitValues[x++].replace('"', ' ').trim();
			f_omschr2 = splitValues[x++].replace('"', ' ').trim();
			f_omschr3 = splitValues[x++].replace('"', ' ').trim();
			f_aantal = decValue(splitValues[x++].replace('"', ' ').trim());
			f_eenheid = splitValues[x++].replace('"', ' ').trim();
			f_prijs = decValue(splitValues[x++].replace('"', ' ').trim());
			f_btwCode = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_bedragExcl = decValue(splitValues[x++].replace('"', ' ').trim());
			f_bedragBtw = decValue(splitValues[x++].replace('"', ' ').trim());
			f_bedragIncl = decValue(splitValues[x++].replace('"', ' ').trim());
			f_dc = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_rekening = splitValues[x++].replace('"', ' ').trim().toUpperCase();

			bean.setAantal(f_aantal);
			bean.setBedragBtw(f_bedragBtw);
			bean.setBedragExcl(f_bedragExcl);
			bean.setBedragIncl(f_bedragIncl);
			bean.setBedragDebet(f_dc.equals("D") ? f_bedragIncl : ZERO);
			bean.setBedragCredit(f_dc.equals("C") ? f_bedragIncl : ZERO);
			bean.setBoekstuk(f_boekstuk);
			bean.setBoekstukRegel(GeneralHelper.genereerNieuwBoekstukRegelNummer(dbd, bedrijfsCode, f_dagboekId, f_boekstuk));
			bean.setBtwCode(f_btwCode);
			bean.setDagboekId(f_dagboekId);
			bean.setDatumLevering(f_leverdatum);
			bean.setDC(f_dc);
			bean.setDcNummer(f_dcNr);
			bean.setEenheid(f_eenheid);
			bean.setFactuurNummer(f_factNr);
			bean.setOmschr1(truncate(f_omschr1, 50));
			bean.setOmschr2(truncate(f_omschr2, 50));
			bean.setOmschr3(truncate(f_omschr3, 50));
			bean.setPrijs(f_prijs);
			bean.setRekening(f_rekening);
			try {
				manager.create(bean);
			} catch (Exception e) {
				report.append("Fout in gegevens van boekstuk " + bean.getBoekstuk() + "/" + bean.getBoekstukRegel() + ": " + e.getMessage() + EOL);
				error = true;
			}
		}
		return report;
	}
}