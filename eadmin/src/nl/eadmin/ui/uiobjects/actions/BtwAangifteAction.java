package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.ui.uiobjects.windows.BtwAangifteWindow;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;

public class BtwAangifteAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;
	
	public BtwAangifteAction() {
		this("BTW Aangifte");
	}

	public BtwAangifteAction(String label) {
		super(label);
		setIcon("report.png");
	}

	public boolean execute(DataObject object) throws Exception {
		object.addUIObject(new  BtwAangifteWindow());
		return true;
	}
	
}