package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.ui.uiobjects.windows.BalansWindow;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;

public class BalansAction extends Action {
	private static final long serialVersionUID = 2620212891344141355L;

	public BalansAction() {
		this("Balans");
	}

	public BalansAction(String label) {
		super(label);
		setIcon("report.png");
	}

	public boolean execute(DataObject object) throws Exception {
		object.addUIObject(new BalansWindow());
		return true;
	}
}