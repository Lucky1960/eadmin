package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.SessionKeys;
import nl.eadmin.ui.adapters.BoekingProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.UserMessageException;

public class SelectDagboekAction extends Action {
	private static final long serialVersionUID = 4956233244124296605L;
	private String dagboekSoort;

	public SelectDagboekAction(String label, String dagboekSoort) {
		super(label);
		setAdapter(BoekingProcess.getStartPage());
		setMethod(BoekingProcess.START_ACTION);
		this.dagboekSoort = dagboekSoort;
		setIcon("report.png");
	}

	public boolean execute(DataObject object) throws Exception {
		if (ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF) == null) {
			throw new UserMessageException("Er is nog geen actief bedrijf");
		}
		if (ESPSessionContext.getSessionAttribute(SessionKeys.BOEKJAAR) == null) {
			throw new UserMessageException("Er is nog geen actief boekjaar");
		}
		ESPSessionContext.setSessionAttribute(SessionKeys.DAGBOEKSOORT, dagboekSoort);
		return super.execute(object);
	}
}