package nl.eadmin.ui.uiobjects.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Product;
import nl.eadmin.db.ProductManager;
import nl.eadmin.db.ProductManagerFactory;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CommonTable;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.esp.util.Transformer;

public class SelecteerProductAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;
	public static final String MESSAGE = "MESSAGE_FROM_LOGIN";
	private Bedrijf bedrijf;
	private Window window;
	private Collection<SelectProductListener> myListeners = new ArrayList<SelectProductListener>();

	public SelecteerProductAction(String label, Bedrijf bedrijf) {
		super(label == null ? "Product" : label);
		this.bedrijf = bedrijf;
		setIcon("cart.png");
	}

	public boolean execute(DataObject object) throws Exception {
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				Product product = (Product) getObject(CommonTable.OBJECT);
				if (product != null) {
					Iterator<SelectProductListener> iter = myListeners.iterator();
					while (iter.hasNext()) {
						iter.next().setProduct(product);
					}
				}
				window.closeWindow(object);
				return true;
			}
		};

		ProductManager manager = ProductManagerFactory.getInstance(bedrijf.getDBData());
		ODBTable table = new ODBTable(manager, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setFilter(Product.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		table.setName("ProductAdapter");
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(new String[]{Product.PRODUCT, Product.OMSCHR1, Product.PRIJS});
		table.setColumnLabels(new String[]{"Product", "Omschrijving", "Prijs"});
		table.setColumnSortable(new boolean[]{true, true, true});
		table.setColumnSizes(new short[]{100, 400, 80});
		table.setDisplayTransformers(new Transformer[]{null, null, new DecimalToAmountTransformer()});
		table.setSingleSelectable();
		table.addDownloadCSVContextAction("Producten");
		table.addDownloadPDFContextAction("Producten");
		table.setRowAction(ok);
		table.reload();
		window = new Window();
		window.setLabel("Selecteer product");
		window.add(table);

		object.addUIObject(window);
		return true;
	}

	public void addSelectProductListener(SelectProductListener listener) throws Exception {
		myListeners.add(listener);
	}

	public interface SelectProductListener {
		public void setProduct(Product product) throws Exception ;
	}
}