package nl.eadmin.ui.uiobjects.actions;

import java.util.Date;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.bankfilereaders.BankfileReader;
import nl.eadmin.db.BankImport;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.helpers.PeriodeHelper;
import nl.eadmin.ui.uiobjects.referencefields.DagboekReferenceField;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.FileSelectionField;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.TextArea;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.Window;

public class ImportBankAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;
	private ESPGridLayout grid;
	private FloatBar fb;
	private Bedrijf bedrijf;

	public ImportBankAction(String label) {
		super(label == null?"Importeer bankbestand":label);
		setIcon("application_get.png");
	}

	public boolean execute(DataObject object) throws Exception {
		bedrijf = (Bedrijf)ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		DagboekReferenceField bankboek = new DagboekReferenceField("Bankboek", bedrijf, DagboekManagerFactory.getInstance(bedrijf.getDBData()), DagboekSoortEnum.BANK);
		bankboek.setDiscardLabel(true);
		bankboek.setMandatory(true);
		bankboek.hideIfOnlyOneOption(Dagboek.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Dagboek.SOORT + "='" + DagboekSoortEnum.BANK + "'");
		FileSelectionField filename = new FileSelectionField("Te importeren bestand");
		filename.setDiscardLabel(true);
		filename.setMandatory(true);
		DateSelectionField datumVan = new DateSelectionField("Datum VAN", (Date)null, "dd-MM-yyyy");
		datumVan.setDiscardLabel(true);
		datumVan.setMandatory(true);
		DateSelectionField datumTem = new DateSelectionField("Datum T/M", (Date)null, "dd-MM-yyyy");
		datumTem.setDiscardLabel(true);
		datumTem.setMandatory(true);
		final Window window = new Window();
		window.setLabel("Importeren bankgegevens");
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				byte[] fileData = filename.getFileContent();
				validateInput(fileData, bankboek.getMyObject(), datumVan.getValueAsDate(), datumTem.getValueAsDate());
				String importString = new String(fileData);
				String[] record = importString.split(ApplicationConstants.EOL);
				BankfileReader reader = BankfileReader.getInstance(bankboek.getMyObject(), record, datumVan.getValueAsDate(), datumTem.getValueAsDate());
				String result = reader.importeer();
				BinaryObject bobj = new BinaryObject(result.getBytes(), BinaryObject.TYPE_TEXT, object);
				bobj.openSaveAsDialog("Import_Bankmutaties.txt");
//				window.closeWindow(object);
				window.removeUIObject(grid, true);
				window.removeUIObject(fb, true);
				TextArea txt = new TextArea();
				txt.setValueAsString(result);
				txt.setDiscardLabel(true);
				txt.setLength(80);
				txt.setHeight(15);
				FieldGroup fg = new FieldGroup();
				fg.add(txt);
				window.setLabel("Importverslag");
				window.add(fg);
				window.setModified();
				return true;
			}
		}.setValidationEnabled(true)/*.setIsDownloadAction(true)*/;
		
		grid = new ESPGridLayout();
		grid.setColumnWidths(new short[]{200, 200});
		grid.add(new Label("Te importeren bestand"), 0, 0);
		grid.add(filename, 0, 1);
		grid.add(new Label("Bankboek"), 1, 0);
		grid.add(bankboek, 1, 1);
		grid.add(new Label("Datum VAN"), 2, 0);
		grid.add(datumVan, 2, 1);
		grid.add(new Label("Datum T/M"), 3, 0);
		grid.add(datumTem, 3, 1);
		fb = new FloatBar();
		fb.addAction(ok);
		window.add(grid);
		window.add(fb);
		object.addUIObject(window);
		return true;
	}

	public void validateInput(byte[] fileData, Dagboek dagboek, Date datumVan, Date datumTem) throws Exception {
		if (fileData == null || fileData.length == 0) {
			throw new UserMessageException("Geen gegevens in dit bestand gevonden");
		}
		if (dagboek.getBoekstukMask().trim().length() == 0) {
			throw new UserMessageException("Er is bij geen boekstuk-opmaak opgegeven in het bankboek");
		}
		if (dagboek.getRekening().trim().length() == 0 || dagboek.getTegenRekening().trim().length() == 0) {
			throw new UserMessageException("Er moet een grootboekrekening en tegenrekening worden opgegeven in het bankboek");
		}
		BankImport bankImport = dagboek.getBankImport();
		if (bankImport.getIbanNr() == null || bankImport.getIbanNr().trim().length() == 0) {
			throw new UserMessageException("Er is bij geen bankrekeningnummer opgegeven in het bankboek");
		}
		if (bankImport.getFormat() == null || bankImport.getFormat().trim().length() == 0) {
			throw new UserMessageException("Er is geen import-formaat opgegeven in het bankboek");
		}
		if (datumVan.after(datumTem)) {
			throw new UserMessageException("Datum VAN > Datum T/M");
		}
		if (PeriodeHelper.isClosed(bedrijf, DateHelper.bepaalBoekperiode(bedrijf, datumVan)[0]) || PeriodeHelper.isClosed(bedrijf, DateHelper.bepaalBoekperiode(bedrijf, datumTem)[0])) {
			throw new UserMessageException("De opgegeven periode valt geheel of gedeeltelijk in een afgesloten boekjaar");
		}
	}
}