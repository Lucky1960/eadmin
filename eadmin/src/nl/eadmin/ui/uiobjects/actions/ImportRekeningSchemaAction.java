package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningDataBean;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.jsql.DBData;

public class ImportRekeningSchemaAction extends ImportAction {
	private static final long serialVersionUID = 3936712749083153241L;
	private RekeningManager manager;

	public ImportRekeningSchemaAction() {
		super("Rekeningschema", "application_get.png");
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		manager = RekeningManagerFactory.getInstance(dbd);
	}

	protected String[] getVoorloopRecord() throws Exception {
		return new String[] { "Reknr", "Omschrijving", "OmschrKort", "BtwCode", "BtwSoort", "BtwRekening", "ICPsoort", "Subadmin", "ToonDC", "Verdichting", "Geblokkeerd" };
	}

	protected String[] getRecordMask() throws Exception {
		return new String[] { "A(9)", "A(50)", "A(10)", "A(3)", "I/V", "J/N", "D/L", "D/C", "D/C/S", "N(3)", "J/N" };
	}

	protected void deleteCurrentData() throws Exception {
		manager.generalDelete(Rekening.BEDRIJF + "='" + bedrijfsCode + "'");
	}

	protected StringBuilder importeer(String[] record, StringBuilder report) throws Exception {
		int rekLength = bedrijf.getInstellingen().getLengteRekening();
		RekeningDataBean bean = new RekeningDataBean();
		bean.setBedrijf(bedrijfsCode);

		int x = 0;
		Integer f_verdicht;
		String f_rekeningNr, f_oms1, f_oms2, f_btwCode, f_btwSoort, f_btwRek, f_iCPSoort, f_toonKolom, f_subAdm, f_block;
		String[] splitValues = null;
		for (int r = 1; r < record.length; r++) {
			x = 0;
			splitValues = fill(record[r].split(separator.getValue()));
			f_rekeningNr = splitValues[x++].replace('"', ' ').trim();
			f_rekeningNr = "000000000" + f_rekeningNr;
			f_rekeningNr = f_rekeningNr.substring(f_rekeningNr.length() - rekLength);
			f_oms1 = splitValues[x++].replace('"', ' ').trim();
			f_oms2 = splitValues[x++].replace('"', ' ').trim();
			f_btwCode = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_btwSoort = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_btwRek = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_iCPSoort = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_subAdm = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_toonKolom = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			try {
				f_verdicht = Integer.parseInt(splitValues[x++].replace('"', ' ').trim());
			} catch (Exception e) {
				f_verdicht = 0;			}
			f_block = splitValues[x++].replace('"', ' ').trim().toUpperCase();

			bean.setBtwCode(f_btwCode);
			bean.setBtwSoort(f_btwSoort);
			bean.setBtwRekening(f_btwRek.equals("J"));
			bean.setICPSoort(f_iCPSoort);
			bean.setOmschrijving(f_oms1);
			bean.setOmschrijvingKort(f_oms2);
			bean.setRekeningNr(f_rekeningNr);
			bean.setSubAdministratieType(f_subAdm);
			bean.setToonKolom(f_toonKolom);
			bean.setVerdichting(f_verdicht);
			bean.setBlocked(f_block.equals("J"));
			try {
				manager.create(bean);
			} catch (Exception e) {
				report.append("Fout in rekening " + f_rekeningNr + ": " + e.getMessage() + EOL);
				error = true;
			}
		}
		return report;
	}
}