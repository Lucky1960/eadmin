package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Hoofdverdichting;
import nl.eadmin.db.HoofdverdichtingDataBean;
import nl.eadmin.db.HoofdverdichtingManager;
import nl.eadmin.db.HoofdverdichtingManagerFactory;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.jsql.DBData;

public class ImportHoofdverdichtingenAction extends ImportAction {
	private static final long serialVersionUID = -9051528883630614051L;
	private HoofdverdichtingManager manager;


	public ImportHoofdverdichtingenAction() {
		super("Hoofdverdichtingen", "arrow_in.png");
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		manager = HoofdverdichtingManagerFactory.getInstance(dbd);
	}

	protected String[] getVoorloopRecord() throws Exception {
		return new String[] { "Id", "Omschrijving", "Rekeningsoort" };
	}

	protected String[] getRecordMask() throws Exception {
		return new String[] { "N(3)", "A(50)", "A(1)" };
	}

	protected void deleteCurrentData() throws Exception {
		manager.generalDelete(Hoofdverdichting.BEDRIJF + "='" + bedrijfsCode + "'");
	}

	protected StringBuilder importeer(String[] record, StringBuilder report) throws Exception {
		HoofdverdichtingDataBean bean = new HoofdverdichtingDataBean();
		bean.setBedrijf(bedrijfsCode);

		int x = 0;
		Integer f_id;
		String f_Omschr, f_rekeningsoort;
		String[] splitValues = null;
		for (int r = 1; r < record.length; r++) {
			x = 0;
			splitValues = fill(record[r].split(separator.getValue()));
			f_id = intValue(splitValues[x++]);
			f_Omschr = splitValues[x++].replace('"', ' ').trim();
			f_rekeningsoort = splitValues[x++].replace('"', ' ').trim();

			bean.setId(f_id);
			bean.setOmschrijving(truncate(f_Omschr, 50));
			bean.setRekeningsoort(truncate(f_rekeningsoort, 1));
			try {
				manager.create(bean);
			} catch (Exception e) {
				report.append("Fout in gegevens van hoofdverdichting " + f_id + ": " + e.getMessage() + EOL);
				error = true;
			}
		}
		return report;
	}
}