package nl.eadmin.ui.uiobjects.actions;

import java.util.Date;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.HeaderDataPK;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.datareaders.HeaderDataReader;
import nl.eadmin.ui.uiobjects.referencefields.CrediteurReferenceField;
import nl.eadmin.ui.uiobjects.tables.HeaderDataTable;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Message;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.Screen;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.exception.FinderException;

public class CopyFavoriteAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;
	public static final String MESSAGE = "MESSAGE_FROM_LOGIN";
	private Window window;
	private Bedrijf bedrijf;
	private Dagboek dagboek;
	private DBData dbd;
	private HeaderDataTable table;
	private Field fldBoekstuk = new Field("Boekstuk", Field.TYPE_TEXT, "", "", 15);
	private BoekdatumListener boekdatumListener = new BoekdatumListener();
	private DateSelectionField fldBoekdatum;
	private CrediteurReferenceField fldCredNr;
	private int newBoekjaar, newBoekperiode;
	private boolean manualBoekstuk, suppressEvent;

	public CopyFavoriteAction(String label, Bedrijf bedrijf, Dagboek dagboek) throws Exception {
		super(label == null ? "Kopieer favoriet" : label);
		this.bedrijf = bedrijf;
		this.dagboek = dagboek;
		this.dbd = bedrijf.getDBData();
		setIcon("application_double.png");
		initialize();
	}

	private void initialize() throws Exception {
		manualBoekstuk = (dagboek.getBoekstukMask().trim().length() == 0);
		fldBoekstuk = new Field("Boekstuk", Field.TYPE_TEXT, "", "", 15);
		fldBoekstuk.setReadonly(!manualBoekstuk);
		fldBoekdatum = new DateSelectionField("Boekdatum", (Date) null, "dd-MM-yyyy");
		fldBoekdatum.addOnChangeListener(boekdatumListener);
		fldBoekdatum.setValue(new Date());
		fldCredNr = new CrediteurReferenceField("Evt. afwijkend relatienummer", bedrijf, CrediteurManagerFactory.getInstance(dbd));
		fldBoekstuk.setMandatory(true);
		fldBoekdatum.setMandatory(true);
	}

	private void validateNewFields() throws Exception {
		int currentBoekjaar = ((Integer) ESPSessionContext.getSessionAttribute(SessionKeys.BOEKJAAR)).intValue();
		if (currentBoekjaar != newBoekjaar) {
			fldBoekdatum.setInvalidTag();
			throw new UserMessageException("Deze boekdatum valt buiten het actieve boekjaar");
		}
		HeaderDataPK headerKey = new HeaderDataPK();
		headerKey.setBedrijf(bedrijf.getBedrijfscode());
		headerKey.setDagboekId(dagboek.getId());
		headerKey.setBoekstuk(fldBoekstuk.getValue());
		try {
			HeaderDataManagerFactory.getInstance(dbd).findByPrimaryKey(headerKey);
			fldBoekstuk.setInvalidTag();
			throw new UserMessageException("Dit boekstuknummer bestaat reeds!");
		} catch (FinderException e) {
		}
	}

	public boolean execute(DataObject object) throws Exception {
		FloatBar fb = new FloatBar();
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				HeaderData headerData = (HeaderData) GeneralHelper.getSelectedItem(table, object);
				table.unSelect(headerData);
				String newBoekstuk = fldBoekstuk.getValue();
				Date newBoekdatum = fldBoekdatum.getValueAsDate();
				String newDcNr = fldCredNr.getValue();
				validateNewFields();
				HeaderData newHeaderData = GeneralHelper.copyFavorite(bedrijf, dagboek, headerData, newBoekstuk, newBoekdatum, newBoekjaar, newBoekperiode, newDcNr);
				window.closeWindow(object);
				Screen screen = object.getScreen();
				screen.removeUIObjects(Window.class, false);
				screen.removeUIObjects(Message.class, false);
				screen.addObject(new Message("Boekstuk " + newHeaderData.getBoekstuk() + " is toegevoegd"));
				return true;
			}
		};
		Action cancel = new Action("Terug") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				window.closeWindow(object);
				return true;
			}
		};
		fb.addAction(ok);
		fb.addAction(cancel);

		Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		int boekjaar = ((Integer) ESPSessionContext.getSessionAttribute(SessionKeys.BOEKJAAR)).intValue();
		table = new HeaderDataTable(new HeaderDataReader(dagboek), bedrijf, dagboek, boekjaar, true);
		table.setRowAction(ok);
		table.reload();
		Panel panel = new Panel();
		panel.addUIObject(table);
		panel.addUIObject(new Paragraph(""));
		Panel panel2 = new Panel("Nieuwe gegevens");
		panel2.addUIObject(fldBoekstuk);
		panel2.addUIObject(fldBoekdatum);
		panel2.addUIObject(fldCredNr);
		window = new Window();
		window.setLabel("Kopieer favoriet");
		window.add(panel);
		window.add(panel2);
		window.add(fb);

		object.addUIObject(window);
		return true;
	}

	private class BoekdatumListener implements EventListener {
		private static final long serialVersionUID = 2680984705342842641L;

		public void event(UIObject object, String eventType) throws Exception {
			if (suppressEvent)
				return;
			Date boekdatum = fldBoekdatum.getValueAsDate();
			if (boekdatum == null) {
				newBoekjaar = 0;
				newBoekperiode = 0;
				throw new UserMessageException("De periodetabel is niet ingericht voor deze boekdatum");
			} else {
				int[] tmp = DateHelper.bepaalBoekperiode(bedrijf, boekdatum);
				newBoekjaar = tmp[0];
				newBoekperiode = tmp[1];
				fldBoekstuk.setValue(GeneralHelper.genereerNieuwBoekstukNummer(dagboek, newBoekjaar, newBoekperiode));
			}
		}
	}
}