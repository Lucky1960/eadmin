package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.AdresDataBean;
import nl.eadmin.db.AdresManager;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurDataBean;
import nl.eadmin.db.CrediteurManager;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.enums.AdresTypeEnum;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.jsql.DBData;

public class ImportCrediteurenAction extends ImportAction {
	private static final long serialVersionUID = 3131121935958203738L;
	private CrediteurManager manager;
	private AdresManager adresMgr;

	public ImportCrediteurenAction() {
		super("Crediteuren", "group_error.png");
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		manager = CrediteurManagerFactory.getInstance(dbd);
		adresMgr = AdresManagerFactory.getInstance(dbd);
	}

	protected String[] getVoorloopRecord() throws Exception {
		return new String[] { "CredNr", "Naam", "ContactPersoon", "Straat", "HuisNr", "HuisNrToev", "Postcode", "Woonplaats", "Telefoon", "Mobiel", "Email", "KvkNr", "BtwNr" };
	}

	protected String[] getRecordMask() throws Exception {
		return new String[] { "A(10)", "A(50)", "A(50)", "A(50)", "N(5)", "A(5)", "A(8)", "A(50)", "A(15)", "A(15)", "A(50)", "A(8)", "A(18)" };
	}

	protected void deleteCurrentData() throws Exception {
		manager.generalDelete(Crediteur.BEDRIJF + "='" + bedrijfsCode + "'");
	}

	protected StringBuilder importeer(String[] record, StringBuilder report) throws Exception {
		CrediteurDataBean bean = new CrediteurDataBean();
		bean.setBedrijf(bedrijfsCode);
		AdresDataBean adresBean = new AdresDataBean();
		adresBean.setBedrijf(bedrijfsCode);
		adresBean.setDc("C");
		adresBean.setAdresType(AdresTypeEnum.ADRESTYPE_BEZOEK);
		adresBean.setExtraAdresRegel("");

		int x = 0;
		Integer f_huisNr;
		String f_dcNr, f_naam, f_contactPersoon, f_straat, f_huisNrToev, f_postcode, f_plaats, f_telefoon, f_mobiel, f_email, f_kvkNr, f_btwNr;
		String[] splitValues = null;
		for (int r = 1; r < record.length; r++) {
			x = 0;
			splitValues = fill(record[r].split(separator.getValue()));
			f_dcNr = splitValues[x++].replace('"', ' ').trim();
			f_naam = splitValues[x++].replace('"', ' ').trim();
			f_contactPersoon = splitValues[x++].replace('"', ' ').trim();
			f_straat = splitValues[x++].replace('"', ' ').trim();
			f_huisNr = intValue(splitValues[x++]);
			f_huisNrToev = splitValues[x++].replace('"', ' ').trim();
			f_postcode = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_plaats = splitValues[x++].replace('"', ' ').trim();
			f_telefoon = splitValues[x++].replace('"', ' ').trim();
			f_mobiel = splitValues[x++].replace('"', ' ').trim();
			f_email = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_kvkNr = splitValues[x++].replace('"', ' ').trim();
			f_btwNr = splitValues[x++].replace('"', ' ').trim();

			if (f_dcNr.trim().length() > 0) {
				bean.setBtwNr(f_btwNr);
				bean.setContactPersoon(f_contactPersoon);
				bean.setCredNr(f_dcNr);
				bean.setEmail(f_email);
				bean.setKvkNr(f_kvkNr);
				bean.setNaam(f_naam);
				try {
					manager.create(bean);
				} catch (Exception e) {
					report.append("Fout in gegevens van crediteur " + f_dcNr + ": " + e.getMessage() + EOL);
					error = true;
				}

				adresBean.setDcNr(f_dcNr);
				adresBean.setMobiel(f_mobiel);
				adresBean.setPlaats(f_plaats);
				adresBean.setPostcode(f_postcode);
				adresBean.setStraat(f_straat);
				adresBean.setHuisNr(f_huisNr);
				adresBean.setHuisNrToev(f_huisNrToev);
				adresBean.setTelefoon(f_telefoon);
				try {
					adresMgr.create(adresBean);
				} catch (Exception e) {
					report.append("Fout in adres van crediteur " + f_dcNr + ": " + e.getMessage() + EOL);
					error = true;
				}
			}
		}
		return report;
	}
}