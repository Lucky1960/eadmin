package nl.eadmin.ui.uiobjects.actions;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.DagboekManager;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.DagboekPK;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataDataBean;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.helpers.DateHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.jsql.DBData;

public class ImportHeaderDataAction extends ImportAction {
	private static final long serialVersionUID = -9051528883630614051L;
	private HeaderDataManager manager;
	private DagboekManager dagbMgr;

	public ImportHeaderDataAction() {
		super("Boekingen-Kopgegevens", "page_edit.png");
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		manager = HeaderDataManagerFactory.getInstance(dbd);
		dagbMgr = DagboekManagerFactory.getInstance(dbd);
	}

	protected String[] getVoorloopRecord() throws Exception {
		return new String[] { "Dagboek", "Boekstuk", "Boekdatum", "DebCredNr", "FactuurNr", "ReferentieNr", "FactuurDtm", "VervalDtm", "DC", "FactuurBedrag", "Omschr1", "Omschr2" };
	}

	protected String[] getRecordMask() throws Exception {
		return new String[] { "A(10)", "A(15)", "D(10)", "A(10)", "A(15)", "A(15)", "D(10)", "D(10)", "A(1)", "D(15,2)", "A(50)", "A(50)" };
	}

	protected void deleteCurrentData() throws Exception {
		manager.generalDelete(HeaderData.BEDRIJF + "='" + bedrijfsCode + "'");
	}

	protected StringBuilder importeer(String[] record, StringBuilder report) throws Exception {
		HeaderDataDataBean bean = new HeaderDataDataBean();
		bean.setBedrijf(bedrijfsCode);
		bean.setTotaalBetaald(ZERO);
		bean.setTotaalBtw(ZERO);
		bean.setTotaalExcl(ZERO);
		bean.setTotaalIncl(ZERO);
		bean.setVrijeRub1("");
		bean.setVrijeRub2("");
		bean.setVrijeRub3("");
		bean.setVrijeRub4("");
		bean.setVrijeRub5("");
		bean.setAantalAanmaningen(0);
		bean.setStatus(10);
		bean.setFavoriet(false);
		DagboekPK dagboekPK = new DagboekPK();
		dagboekPK.setBedrijf(bedrijf.getBedrijfscode());

		int x = 0;
		Date f_boekdatum, f_factuurdatum, f_vervaldatum;
		BigDecimal f_factBedrag;
		String f_dagboekId, f_dagboekId_OLD = "", f_dagboekSoort = "", f_boekstuk, f_dcNr, f_factNr, f_refNr, f_dc, f_omschr1, f_omschr2;
		String[] splitValues = null;
		for (int r = 1; r < record.length; r++) {
			x = 0;
			splitValues = fill(record[r].split(separator.getValue()));
			f_dagboekId = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_boekstuk = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_boekdatum = dateValue(splitValues[x++].replace('"', ' ').trim());
			f_dcNr = splitValues[x++].replace('"', ' ').trim();
			f_factNr = splitValues[x++].replace('"', ' ').trim();
			f_refNr = splitValues[x++].replace('"', ' ').trim();
			f_factuurdatum = dateValue(splitValues[x++].replace('"', ' ').trim());
			f_vervaldatum = dateValue(splitValues[x++].replace('"', ' ').trim());
			f_dc = splitValues[x++].replace('"', ' ').trim().toUpperCase();
			f_factBedrag = decValue(splitValues[x++].replace('"', ' ').trim());
			f_omschr1 = splitValues[x++].replace('"', ' ').trim();
			f_omschr2 = splitValues[x++].replace('"', ' ').trim();

			if (!f_dagboekId_OLD.equals(f_dagboekId)) {
				f_dagboekId_OLD = f_dagboekId;
				dagboekPK.setId(f_dagboekId);
				f_dagboekSoort = dagbMgr.findOrCreate(dagboekPK).getSoort();
			}
			int[] boekPer = DateHelper.bepaalBoekperiode(bedrijf, f_boekdatum);
			bean.setBoekdatum(f_boekdatum);
			bean.setBoekjaar(boekPer[0]);
			bean.setBoekperiode(boekPer[1]);
			bean.setBoekstuk(f_boekstuk);
			bean.setDagboekId(f_dagboekId);
			bean.setDagboekSoort(f_dagboekSoort);
			bean.setDC(f_dc);
			bean.setDcNummer(f_dcNr);
			bean.setFactuurdatum(f_factuurdatum);
			bean.setFactuurNummer(f_factNr);
			bean.setOmschr1(truncate(f_omschr1, 50));
			bean.setOmschr2(truncate(f_omschr2, 50));
			bean.setReferentieNummer(f_refNr);
			bean.setTotaalGeboekt(f_factBedrag);
			bean.setTotaalOpenstaand(f_factBedrag);
			bean.setVervaldatum(f_vervaldatum);
			try {
				manager.create(bean);
			} catch (Exception e) {
				report.append("Fout in gegevens van boekstuk " + f_boekstuk + ": " + e.getMessage() + EOL);
				error = true;
			}
		}
		return report;
	}
}