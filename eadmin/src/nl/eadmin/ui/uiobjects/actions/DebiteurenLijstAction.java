package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.ui.uiobjects.windows.DebCredLijstWindow;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;

public class DebiteurenLijstAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;

	public DebiteurenLijstAction() {
		this("Debiteurenoverzicht");
	}
	public DebiteurenLijstAction(String label) {
		super(label);
		setIcon("report.png");
	}

	public boolean execute(DataObject object) throws Exception {
		object.addUIObject(new DebCredLijstWindow("D"));
		return true;
	}
}