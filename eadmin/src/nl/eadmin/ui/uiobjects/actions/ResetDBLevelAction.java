package nl.eadmin.ui.uiobjects.actions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import nl.eadmin.ApplicationConstants;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.jsql.DBConfig;

public class ResetDBLevelAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;
	public static final String MESSAGE = "MESSAGE_FROM_LOGIN";

	public ResetDBLevelAction() {
		super("Reset DB-level");
		setIcon("wand.png");
	}

	public boolean execute(DataObject object) throws Exception {
		String schemaUrl = DBConfig.getURL(ApplicationConstants.APPLICATION);
		String schemaUser = DBConfig.getUser(ApplicationConstants.APPLICATION);
		String schemaPwd = DBConfig.getPassword(ApplicationConstants.APPLICATION);
		Connection con = DriverManager.getConnection(schemaUrl, schemaUser, schemaPwd);
		Statement stmt = con.createStatement();
		stmt.execute("UPDATE JSQLVRSN SET VERSION_NUMBER = '0' WHERE VERSION_TYPE = 'APPLICATION_VERSION'");
		stmt.close();
		con.close();
		System.out.println("Databaselevel teruggezet");
		return true;
	}
}