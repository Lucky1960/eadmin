package nl.eadmin.ui.uiobjects.actions;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.FileSelectionField;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.Window;

public abstract class ImportAction extends Action {
	private static final long serialVersionUID = -6544807684110000806L;
	protected ComboBox separator;
	protected String[] voorloopRecord;
	protected String EOL = ApplicationConstants.EOL;
	protected BigDecimal ZERO = ApplicationConstants.ZERO;
	protected Bedrijf bedrijf;
	protected String bedrijfsCode;
	protected boolean error;
	private CheckBox overWrite;
	private FileSelectionField filename;
	private Window window;
	private String windowLabel;

	public ImportAction(String buttonlabel, String icon) {
		super(buttonlabel);
		this.windowLabel = "Importeren " + buttonlabel.toLowerCase();
		setIcon(icon);
	}

	public boolean execute(DataObject object) throws Exception {
		bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		bedrijfsCode = bedrijf.getBedrijfscode();
		voorloopRecord = getVoorloopRecord();
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				String importString = new String(filename.getFileContent());
				String[] record = importString.split(ApplicationConstants.EOL);
				String[] splitValues = record[0].split(separator.getValue());
				boolean validFile = (splitValues.length == voorloopRecord.length);
				int ix = 0;
				while (validFile && ix < splitValues.length) {
					validFile = (splitValues[ix].replace('"', ' ').trim().toLowerCase().equals(voorloopRecord[ix].trim().toLowerCase()));
					ix++;
				}
				if (validFile) {
					error = false;
					if (overWrite.getBoolean()) {
						deleteCurrentData();
					}
					StringBuilder report = new StringBuilder();
					report.append("Importverslag van bestand " + filename.getFileName() + EOL);
					report.append("==================================================" + EOL);
					importeer(record, report);
					if (!error) {
						report.append(" (Geen fouten)");
					}
					report.append("*** Einde verslag ***");
					System.out.println(report);
					// BinaryObject bobj = new
					// BinaryObject(report.toString().getBytes(),
					// BinaryObject.TYPE_TEXT, object);
					// bobj.openSaveAsDialog("Importverslag.txt");
				} else {
					throw new UserMessageException("Dit is geen geldig importbestand");
				}
				window.closeWindow(object);
				return true;
			}
		}/* .setIsDownloadAction(true) */;

		filename = new FileSelectionField("");
		filename.setMandatory(true);
		filename.setDiscardLabel(true);
		overWrite = new CheckBox("");
		overWrite.setDiscardLabel(true);
		separator = new ComboBox("");
		separator.setDiscardLabel(true);
		separator.setWidth("150");
		separator.addOption("Puntkomma (;)", ";");
		separator.addOption("Komma (,)", ",");

		ESPGridLayout grid = new ESPGridLayout();
		grid.setColumnWidths(new short[] { 250, 200 });
		grid.add(new Label("Te importeren bestand"), 0, 0);
		grid.add(filename, 0, 1);
		grid.add(new Label("Lijstscheidingsteken"), 1, 0);
		grid.add(separator, 1, 1);
		grid.add(new Label("Aanwezige gegevens overschrijven"), 2, 0);
		grid.add(overWrite, 2, 1);
		FloatBar fb = new FloatBar();
		fb.addAction(ok);
		window = new Window();
		window.setLabel(windowLabel);
		window.add(grid);
		window.add(new Label("Het aangeboden bestand moet de volgende layout hebben: (inclusief kopregel)"));
		window.add(getPreviewPanel());
		window.add(fb);

		object.addUIObject(window);
		return true;
	}

	protected Panel getPreviewPanel() throws Exception {
		String[] koprecord = getVoorloopRecord();
		String[] recordMask = getRecordMask();
//		String[] mask = new String[recordMask.length];
//		for (int i = 0; i < recordMask.length; i++) {
//			String type = recordMask[i].substring(0, 1);
//			int len = Integer.parseInt(recordMask[i].substring(recordMask[i].indexOf("(") + 1, recordMask[i].indexOf(")")));
//			switch (type) {
//			case "A":
//				mask[i] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX".substring(0, len);
//				break;
//			case "I":
//				mask[i] = "999999999999999999999999999999999999999999999999999".substring(0, len);
//				break;
//			default:
//				mask[i] = recordMask[i];
//			}
//		}

		ESPGridLayout grid = new ESPGridLayout();
		int row = 0, col = 0;
		for (col = 0; col < koprecord.length; col++) {
			grid.add(new Label(koprecord[col]), row, col);
		}
		for (row = 1; row < 10; row++) {
			for (col = 0; col < recordMask.length; col++) {
				grid.add(new Label(recordMask[col]), row, col);
			}
		}
		Panel pnl = new Panel("");
		pnl.addUIObject(grid);
		return pnl;
	}

	protected String[] fill(String[] splitValues) throws Exception {
		if (splitValues.length < voorloopRecord.length) {
			String[] tmpValues = new String[voorloopRecord.length];
			for (int t = 0; t < tmpValues.length; t++) {
				tmpValues[t] = (t < splitValues.length ? splitValues[t] : "");
			}
			splitValues = tmpValues;
		}
		return splitValues;
	}

	protected String truncate(String value, int len) throws Exception {
		if (value.length() <= len) {
			return value;
		} else {
			return value.substring(0, len);
		}
	}

	protected int intValue(String value) throws Exception {
		try {
			return Integer.parseInt(value.replace('"', ' ').trim());
		} catch (Exception e) {
			return 0;
		}
	}

	protected BigDecimal decValue(String value) throws Exception {
		try {
			return new BigDecimal(value.replaceAll(",", "."));
		} catch (Exception e) {
			return ZERO;
		}
	}

	@SuppressWarnings("deprecation")
	protected Date dateValue(String value) throws Exception {
		if (value == null || value.length() == 0) {
			return null;
		}
		Date date = null;
		try {
			date = new Date(Integer.parseInt(value.substring(0, 4)) - 1900, Integer.parseInt(value.substring(4, 6)) - 1, Integer.parseInt(value.substring(6)));
		} catch (Exception e) {
		}
		return date;
	}

	protected abstract String[] getVoorloopRecord() throws Exception;

	protected abstract String[] getRecordMask() throws Exception;

	protected abstract void deleteCurrentData() throws Exception;

	protected abstract StringBuilder importeer(String[] record, StringBuilder sb) throws Exception;
}