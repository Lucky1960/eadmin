package nl.eadmin.ui.uiobjects.actions;

import java.util.Collection;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.helpers.BijlageHelper;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.editors.BijlageEditor;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.BinaryAction;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.CommonTable;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.esp.util.Editor;

public class BijlageAction extends Action {
	private static final long serialVersionUID = 4235134258254635235L;
	private HeaderData headerData = null;
	private CollectionTable table;

	public BijlageAction(HeaderData headerData) throws Exception {
		this.headerData = headerData;
	}

	public boolean execute(DataObject object) throws Exception {
		Collection<?> bijlagen = headerData.getBijlagen();
		Action showAction = new BinaryAction("show") {
			private static final long serialVersionUID = 1L;

			public BinaryObject getBinaryObject(DataObject object) throws Exception {
				Bijlage bijlage = (Bijlage)GeneralHelper.getSelectedItem(table, object);
				if (bijlage != null) {
					table.unSelect(bijlage);
					return BijlageHelper.getBinaryObject(object, bijlage);
				} else {
					return null;
				}
			}
		}.setIcon("page_white_magnify.png");
//		int aantal = bijlagen.size();
		table = new CollectionTable(Bijlage.class, bijlagen, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setName("Bijlagen");
		table.setColumnNames(new String[]{Bijlage.OMSCHRIJVING});
		table.setColumnLabels(new String[]{"Omschrijving"});
		table.setColumnSizes(new short[]{300});
		table.setSelectable(true);
		table.setRowAction(showAction);

		Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		BijlageEditor editor = new BijlageEditor(object, BijlageManagerFactory.getInstance(bedrijf.getDBData()), bedrijf, headerData.getDagboekId(), headerData.getBoekstuk());
		
		MyMaintenancePanel maintenancePanel = new MyMaintenancePanel(Bijlage.class, "Bijlage", "Bijlagen", table, editor, null);
//		if (aantal == 0) {
//			maintenancePanel.showCreateScreen(object);
//		} else if (aantal == 1) {
//			maintenancePanel.showEditScreen(object);
//		}
		maintenancePanel.setUseWindows(true);
		maintenancePanel.setShowHeaderForMainScreen(false);
		maintenancePanel.addActionForMainButtonBar(showAction);
		Window window = new Window();
		window.setLabel("Bijlage(n) bij boekstuk " + headerData.getBoekstuk());
		Panel pnl = new Panel();
		pnl.addUIObject(maintenancePanel);
		window.add(pnl);
		object.addUIObject(window);
		//GeneralHelper.displayDocument(documentId, object);
		return true;
	}

	public class MyMaintenancePanel extends MaintenancePanel {
		private static final long serialVersionUID = -2443316475223906188L;

		@SuppressWarnings("rawtypes")
		public MyMaintenancePanel(Class clss, String _textCodeSingleObject, String _textCodeMultipleObjects, CommonTable _table, Editor _editor, Action close) throws Exception {
			super(clss, _textCodeSingleObject, _textCodeMultipleObjects, _table, _editor, close);
		}

		public void showCreateScreen(DataObject object) throws Exception {
			handleAction(object, ACTION_ADD);
		}

		public void showEditScreen(DataObject object) throws Exception {
			handleAction(object, ACTION_UPDATE);
		}
	}
}