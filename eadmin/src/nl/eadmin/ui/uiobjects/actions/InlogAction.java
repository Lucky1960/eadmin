package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerManager;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.datareaders.GebruikerDataReader;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.util.Scrambler;

public class InlogAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;
	public static final String MESSAGE = "MESSAGE_FROM_LOGIN";
	private Window window;
	private ODBTable table;
	private InputComponent userId, password;

	public InlogAction(InputComponent userId, InputComponent password) {
		super("");
		this.userId = userId;
		this.password = password;
		setIcon("key.png");
		setWidth(4);
	}

	public boolean execute(DataObject object) throws Exception {
		FloatBar fb = new FloatBar();
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				Gebruiker gebruiker = (Gebruiker) GeneralHelper.getSelectedItem(table, object);
				userId.setValueAsString(gebruiker.getId());
				password.setValueAsString(Scrambler.unscramble(gebruiker.getWachtwoord()));
				window.closeWindow(object);
				return true;
			}
		};
		Action cancel = new Action("Terug") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				window.closeWindow(object);
				return true;
			}
		};
		fb.addAction(ok);
		fb.addAction(cancel);

		GebruikerManager manager = GebruikerManagerFactory.getInstance();
		table = new ODBTable(manager, new GebruikerDataReader(), ApplicationConstants.NUMBEROFTABLEROWS);
		table.setWidth(ApplicationConstants.STD_TABLEWIDTH_1);
		table.setColumnNames(new String[]{Gebruiker.ID, Gebruiker.NAAM});
		table.setColumnLabels(new String[]{"User-id", "Naam"});
		table.setColumnSizes(new short[]{100, 300});
		table.setSingleSelectable();
		table.setRowAction(ok);
		table.reload();

		Panel panel = new Panel();
		panel.addUIObject(table);
		window = new Window();
		window.setLabel("Selecteer gebruiker");
		window.add(panel);
		window.add(fb);

		object.addUIObject(window);
		return true;
	}
}