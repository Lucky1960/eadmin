package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.ui.uiobjects.windows.IcpOverzichtWindow;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;

public class IcpOverzichtAction extends Action {
	private static final long serialVersionUID = 5826060731742600834L;

	public IcpOverzichtAction() {
		this("ICP Overzicht");
	}

	public IcpOverzichtAction(String label) {
		super(label);
		setIcon("report.png");
	}

	public boolean execute(DataObject object) throws Exception {
		object.addUIObject(new  IcpOverzichtWindow());
		return true;
	}
}