package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Verdichting;
import nl.eadmin.db.VerdichtingDataBean;
import nl.eadmin.db.VerdichtingManager;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.jsql.DBData;

public class ImportVerdichtingenAction extends ImportAction {
	private static final long serialVersionUID = 7266385331735423546L;
	private VerdichtingManager manager;


	public ImportVerdichtingenAction() {
		super("Verdichtingen", "arrow_join.png");
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		manager = VerdichtingManagerFactory.getInstance(dbd);
	}

	protected String[] getVoorloopRecord() throws Exception {
		return new String[] { "Id", "Omschrijving", "HoofdverdichtingsNr" };
	}

	protected String[] getRecordMask() throws Exception {
		return new String[] { "N(3)", "A(50)", "N(3)" };
	}

	protected void deleteCurrentData() throws Exception {
		manager.generalDelete(Verdichting.BEDRIJF + "='" + bedrijfsCode + "'");
	}

	protected StringBuilder importeer(String[] record, StringBuilder report) throws Exception {
		VerdichtingDataBean bean = new VerdichtingDataBean();
		bean.setBedrijf(bedrijfsCode);

		int x = 0;
		Integer f_id, f_hfdVerdichting;
		String f_Omschr;
		String[] splitValues = null;
		for (int r = 1; r < record.length; r++) {
			x = 0;
			splitValues = fill(record[r].split(separator.getValue()));
			f_id = intValue(splitValues[x++]);
			f_Omschr = splitValues[x++].replace('"', ' ').trim();
			f_hfdVerdichting = intValue(splitValues[x++]);

			bean.setId(f_id);
			bean.setOmschrijving(truncate(f_Omschr, 50));
			bean.setHoofdverdichting(f_hfdVerdichting);
			try {
				manager.create(bean);
			} catch (Exception e) {
				report.append("Fout in gegevens van verdichting " + f_id + ": " + e.getMessage() + EOL);
				error = true;
			}
		}
		return report;
	}
}