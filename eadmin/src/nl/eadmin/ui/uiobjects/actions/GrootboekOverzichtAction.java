package nl.eadmin.ui.uiobjects.actions;

import nl.eadmin.ui.uiobjects.windows.GrootboekOverzichtWindow;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;

public class GrootboekOverzichtAction extends Action {
	private static final long serialVersionUID = 6126054346069737846L;

	public GrootboekOverzichtAction(String label) {
		super(label);
		setIcon("report.png");
	}

	public boolean execute(DataObject object) throws Exception {
		object.addUIObject(new GrootboekOverzichtWindow());
		return true;
	}
}