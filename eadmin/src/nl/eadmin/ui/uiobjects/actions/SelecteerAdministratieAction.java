package nl.eadmin.ui.uiobjects.actions;

import java.util.Date;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.impl.PeriodeManager_Impl;
import nl.eadmin.helpers.PeriodeHelper;
import nl.eadmin.ui.adapters.MainAdapter;
import nl.eadmin.ui.adapters.MainProcess;
import nl.eadmin.ui.uiobjects.referencefields.BedrijfReferenceField;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.RedirectAction;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class SelecteerAdministratieAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;
	public static final String MESSAGE = "MESSAGE_FROM_LOGIN";
	private Window window;
	private BedrijfReferenceField refBedrijf;
	private ComboBox cbBoekjaar;
	private Label lblBedrijf = new Label("Administratie");
	private Label lblBoekjaar = new Label("Boekjaar");
	private Label lblClosed = new Label("Afgesloten boekjaar");
	private ESPGridLayout grid = new ESPGridLayout();;

	public SelecteerAdministratieAction(String label) {
		super(label == null ? "Selecteer administratie/boekjaar" : label);
		setIcon("date.png");
		setMandatory(true);
	}

	@SuppressWarnings("deprecation")
	public boolean execute(DataObject object) throws Exception {
		Gebruiker gebruiker = (Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		refBedrijf = new BedrijfReferenceField("Administratie", dbd, BedrijfManagerFactory.getInstance(dbd), gebruiker);
		refBedrijf.setMandatory(true);
		refBedrijf.setDiscardLabel(true);
		refBedrijf.setValue(gebruiker.getBedrijf());

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT BOEKJAAR FROM Periode");
		sb.append(" WHERE BDR = '" + gebruiker.getBedrijf() + "'");
		sb.append(" GROUP BY BOEKJAAR");
		FreeQuery qry = QueryFactory.createFreeQuery((PeriodeManager_Impl) PeriodeManagerFactory.getInstance(dbd), sb.toString());
		Object[][] jaren = qry.getResultArray();
		if (jaren.length == 0) {
			throw new UserMessageException("Geen periodedefinities gevonden voor bedrijf '" + gebruiker.getBedrijf() + "'");
		}
		cbBoekjaar = new ComboBox("Boekjaar");
		for (int i = 0; i < jaren.length; i++) {
			String jaar = String.valueOf((Integer) jaren[i][0]);
			cbBoekjaar.addOption(jaar, jaar);
		}
		cbBoekjaar.setWidth("70");
		cbBoekjaar.setSelectedOptionName(gebruiker.getBoekjaar() == 0 ? "" + (new Date(System.currentTimeMillis()).getYear() + 1900) : "" + gebruiker.getBoekjaar());
		cbBoekjaar.setMandatory(true);
		cbBoekjaar.setDiscardLabel(true);

		grid = new ESPGridLayout();
		grid.setColumnWidths(new short[] { 120, 60 });
		grid.add(lblBedrijf, 0, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
		grid.add(refBedrijf, 0, 1, 1, 2, ESPGridLayoutConstraints.GRID_FILL_NONE);
		grid.add(lblBoekjaar, 1, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
		grid.add(cbBoekjaar, 1, 1, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
		grid.add(lblClosed, 1, 2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
		MyListener myListener = new MyListener();
		cbBoekjaar.addOnChangeListener(myListener);
		myListener.event(null, "");

		FloatBar fb = new FloatBar();
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				Bedrijf bedrijf = refBedrijf.getBedrijf();
				Integer boekjaar = Integer.valueOf(cbBoekjaar.getValue());

				gebruiker.setBedrijf(bedrijf.getBedrijfscode());
				gebruiker.setBoekjaar(boekjaar);
				ESPSessionContext.setSessionAttribute(SessionKeys.BEDRIJF, bedrijf);
				ESPSessionContext.setSessionAttribute(SessionKeys.BEDRIJF_ID, bedrijf.getBedrijfscode());
				ESPSessionContext.setSessionAttribute(SessionKeys.BOEKJAAR, boekjaar);
				object.setApplicationIdentifyingString("Bedrijf : " + bedrijf.getOmschrijving() + " - " + "Boekjr: " + boekjaar);
				object.addUIObject(new RedirectAction(MainProcess.getStartPage(), MainAdapter.START_ACTION));
				window.closeWindow(object);
				return true;
			}
		};
		Action cancel = new Action("Terug") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				window.closeWindow(object);
				return true;
			}
		};
		fb.addAction(ok);
		fb.addAction(cancel);

		window = new Window();
		window.setLabel("Selecteer administratie/boekjaar");
		window.add(grid);
		window.add(fb);

		object.addUIObject(window);
		return true;
	}

	private class MyListener implements EventListener {
		private static final long serialVersionUID = 1L;

		public void event(UIObject object, String type) throws Exception {
			Bedrijf bedrijf = refBedrijf.getBedrijf();
			int boekjaar = Integer.parseInt(cbBoekjaar.getValue());
			lblClosed.setHidden(!PeriodeHelper.isClosed(bedrijf, boekjaar));
			lblClosed.setModified();
			grid.setModified();
		}
	}
}