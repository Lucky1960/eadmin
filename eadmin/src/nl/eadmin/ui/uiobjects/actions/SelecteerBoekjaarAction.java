package nl.eadmin.ui.uiobjects.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.impl.PeriodeManager_Impl;
import nl.eadmin.helpers.PeriodeHelper;
import nl.eadmin.ui.adapters.MainAdapter;
import nl.eadmin.ui.adapters.MainProcess;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.CommonTable;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.RedirectAction;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class SelecteerBoekjaarAction extends Action {
	private static final long serialVersionUID = -4609066373385936583L;
	public static final String MESSAGE = "MESSAGE_FROM_LOGIN";
	private Gebruiker gebruiker;
	private Bedrijf bedrijf;
	private Window window;

	public SelecteerBoekjaarAction(String label) {
		super(label == null?"Selecteer boekjaar":label);
		setIcon("date.png");
	}

	public boolean execute(DataObject object) throws Exception {
		gebruiker = (Gebruiker)ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
		bedrijf = (Bedrijf)ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				Periode periode = (Periode) getObject(CommonTable.OBJECT);
				if (periode != null) {
					Integer boekjaar = Integer.valueOf(periode.getBoekjaar());
					gebruiker.setBoekjaar(boekjaar);
					ESPSessionContext.setSessionAttribute(SessionKeys.BOEKJAAR, boekjaar);
					object.setApplicationIdentifyingString("Bedrijf : " + bedrijf.getOmschrijving() + " - " + "Boekjr: " + boekjaar);
					object.addUIObject(new RedirectAction(MainProcess.getStartPage(), MainAdapter.START_ACTION));
				}
				window.closeWindow(object);
				return true;
			}
		};

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Periode");
		sb.append(" WHERE BDR = '" + bedrijf.getBedrijfscode() + "'");
		sb.append(" GROUP BY BOEKJAAR");

		Collection<Boekjaar> boekjaren = new ArrayList<Boekjaar>();
		FreeQuery qry = QueryFactory.createFreeQuery((PeriodeManager_Impl) PeriodeManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		Iterator<?> iter = qry.getCollection().iterator();
		while (iter.hasNext()) {
			int jaar = ((Periode) iter.next()).getBoekjaar();
			boekjaren.add(new Boekjaar(jaar, PeriodeHelper.isClosed(bedrijf, jaar)));
		}
		CollectionTable table = new CollectionTable(Boekjaar.class, boekjaren, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setName("SelectBoekjaar");
		table.setColumnNames(new String[] { "Boekjaar", "isClosed" });
		table.setColumnLabels(new String[] { "Boekjaar", "Afgesloten" });
		table.setColumnTypes(new String[] {Field.TYPE_NUMBER, Field.TYPE_BOOLEAN });
		table.setColumnSizes(new short[] { 60, 60 });
		table.setColumnSortable(new boolean[] { false, false });
		table.setSelectable(false);
		table.setRowAction(ok);
		table.reload();

		window = new Window();
		window.setLabel("Selecteer boekjaar");
		window.add(table);

		object.addUIObject(window);
		return true;
	}

	public class Boekjaar  {
		private int boekjaar;
		private boolean isClosed;

		private Boekjaar(int boekjaar, boolean isClosed) throws Exception {
			this.boekjaar = boekjaar;
			this.isClosed = isClosed;
		}

		public int getBoekjaar() throws Exception {
			return boekjaar;
		}

		public boolean getIsClosed() throws Exception {
			return isClosed;
		}
	}
}