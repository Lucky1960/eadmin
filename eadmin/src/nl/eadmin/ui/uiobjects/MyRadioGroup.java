package nl.eadmin.ui.uiobjects;

import java.util.Iterator;

import nl.ibs.esp.uiobjects.RadioButton;
import nl.ibs.esp.uiobjects.RadioGroup;

public class MyRadioGroup extends RadioGroup {
	private static final long serialVersionUID = -1896392587333668301L;

	public MyRadioGroup(String label) throws Exception {
		super(label);
	}

	public String getSelectedValue() {
		String val = "";
		Iterator<?> objects = getUIObjects(RadioButton.class, false).iterator();
		while (objects.hasNext()) {
			RadioButton rb = (RadioButton) objects.next();
			if (rb.getChecked()) {
				val = rb.getValue();
				break;
			}
		}
		return val;
	}
}