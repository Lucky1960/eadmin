package nl.eadmin.ui.uiobjects;

import java.util.Iterator;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.impl.PeriodeManager_Impl;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class ComboBoekperiode extends ComboBox implements EventListener{
	private static final long serialVersionUID = -4720989231092052985L;
	private Bedrijf bedrijf;

	public ComboBoekperiode(Bedrijf bedrijf) throws Exception {
		super("Boekperiode");
		this.bedrijf = bedrijf;
		setWidth("40");
		setSortThreshold(999);
	}

	public void event(UIObject object, String type) throws Exception {
		if (object instanceof ComboBoekjaar) {
			int boekjr = Integer.parseInt(((ComboBoekjaar)object).getSelectedOptionValue());
			clearOptions();
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM Periode");
			sb.append(" WHERE BDR = '" + bedrijf.getBedrijfscode() + "' AND BOEKJAAR = " + boekjr);
			sb.append(" ORDER BY PERIODE");

			FreeQuery qry = QueryFactory.createFreeQuery((PeriodeManager_Impl) PeriodeManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
			Iterator<?> iter = qry.getCollection().iterator();
			while (iter.hasNext()) {
				String periode = "" + ((Periode) iter.next()).getBoekperiode();
				addOption(periode, periode);
			}
		}
	}
}