package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Hoofdverdichting;
import nl.eadmin.db.HoofdverdichtingManager;
import nl.eadmin.db.HoofdverdichtingPK;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;

public class HoofdverdichtingReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = -8983331851067991530L;
	private static final String[] NAMES = new String[] { Hoofdverdichting.ID, Hoofdverdichting.OMSCHRIJVING, Hoofdverdichting.REKENINGSOORT_FULL };
	private static final String[] LABELS = new String[] { "Hoofdverdichting", "Omschrijving", "Soort" };
	private static final short[] SIZES = new short[] { 90, 250, 150 };
	private HoofdverdichtingManager myMgr;
	private HoofdverdichtingPK myPK;
	private Hoofdverdichting myObject;

	public HoofdverdichtingReferenceField(String label, Bedrijf bedrijf, HoofdverdichtingManager mgr) throws Exception {
		this(label, bedrijf, mgr, null);
	}

	public HoofdverdichtingReferenceField(String label, Bedrijf bedrijf, HoofdverdichtingManager mgr, String filterHoofdverdichtingSoort) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new HoofdverdichtingPK();
		this.myPK.setBedrijf(bedrijf.getBedrijfscode());
		setFilter(Hoofdverdichting.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(50);
		if (filterHoofdverdichtingSoort != null && filterHoofdverdichtingSoort.trim().length() > 0) {
			setFilter(Hoofdverdichting.REKENINGSOORT + "='" + filterHoofdverdichtingSoort + "'");
		}
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				String myValue = getValue().trim();
				if(myValue.length()==0){
					setDescription("");
				} else {
					try {
						myPK.setId(Integer.parseInt(myValue));
						setDescription(myMgr.findByPrimaryKey(myPK).getOmschrijving());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	public void setValue(Hoofdverdichting hfdVerdichting) throws Exception {
		if (hfdVerdichting == null) {
			setValue();
			setDescription("");
		} else {
			setValue("" + hfdVerdichting.getId());
			setDescription(hfdVerdichting.getOmschrijving());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (Hoofdverdichting) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getOmschrijving());
	}

	public Hoofdverdichting getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setId(Integer.parseInt(getValue().trim()));
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}