package nl.eadmin.ui.uiobjects.referencefields;

import java.util.ArrayList;
import java.util.HashMap;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Rol;
import nl.eadmin.db.RolDataBean;
import nl.eadmin.db.RolManager;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;
import nl.sf.api.InitializationRequest;
import nl.sf.api.RoleData;

public class RolReferenceField extends ODBQueryReferenceField {
	private static final String ROLLEN_GESYNCHRONIZEERD = "eaRolsGesynchronizeerd";
	private static final long serialVersionUID = -8983331851067991530L;
	private static final String[] NAMES = new String[] { Rol.NAAM, Rol.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Naam", "Omschrijving" };
	private static final short[] SIZES = new short[] { 120, 300 };
	private RolManager manager;
	private Rol myObject;

	public RolReferenceField(String label, RolManager mgr) throws Exception {
		super(mgr);
		this.manager = mgr;
		init();
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(15);
		setMaxLength(15);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;

			public void event(UIObject object, String type) throws Exception {
				String gbrId = getValue();
				if (gbrId.trim().length() == 0) {
					setDescription("");
				} else {
					try {
						setDescription(manager.findByPrimaryKey(gbrId).getNaam());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	private void init() throws Exception {
		InitializationRequest request = (InitializationRequest) ESPSessionContext.getSessionAttribute(SessionKeys.INIT_REQUEST);
		if (request != null && "true".equals(ESPSessionContext.getSessionAttribute(ROLLEN_GESYNCHRONIZEERD)) == false) {
			RoleData[] rols = request.getEnvironment().getRolesCurrentlyDefinedByCustomer();
			ArrayList<Rol> rollen = (ArrayList<Rol>) manager.getCollection(null);
			HashMap<String, Rol> rollenMap = new HashMap<String, Rol>(rollen.size());
			for (int i = 0; i < rollen.size(); i++) {
				Rol rol = rollen.get(i);
				rollenMap.put(rol.getNaam(), rol);
			}
			for (int i = 0; i < rols.length; i++) {
				Rol rol = rollenMap.get(rols[i].getName());
				if (rol == null) {
					RolDataBean bean = new RolDataBean();
					bean.setNaam(rols[i].getName());
					bean.setOmschrijving(rols[i].getDescription());
					manager.add(bean);
				} else {
					rol.setOmschrijving(rols[i].getDescription());
				}
			}
			ESPSessionContext.setSessionAttribute(ROLLEN_GESYNCHRONIZEERD, "true");
		}
	}

	public void setRolId(String id) throws Exception {
		setValue(id);
		try {
			setDescription(manager.findByPrimaryKey(id.trim()).getNaam());
		} catch (FinderException e) {
			setDescription("???");
			;
		}
	}

	public void setValue(Rol rol) throws Exception {
		if (rol == null) {
			setDescription("");
			setValue();
			return;
		}
		setValue(rol.getNaam());
		setDescription(rol.getOmschrijving());
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (Rol) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getRol();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \"" + getLabel() + "\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getNaam());
	}

	public Rol getRol() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			return manager.findByPrimaryKey(getValue().trim());
		} catch (FinderException e) {
			return null;
		}
	}
}