package nl.eadmin.ui.uiobjects.referencefields;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerDataBean;
import nl.eadmin.db.GebruikerManager;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.exception.FinderException;
import nl.sf.api.InitializationRequest;
import nl.sf.api.UserData;

public class GebruikerReferenceField extends ODBQueryReferenceField {
	private static final String GEBRUIKERS_GESYNCHRONIZEERD = "eaGebruikersGesynchronizeerd";
	private static final long serialVersionUID = -8983331851067991530L;
	private static final String[] NAMES = new String[] { Gebruiker.ID, Gebruiker.NAAM };
	private static final String[] LABELS = new String[] { "Gebruiker", "Naam" };
	private static final short[] SIZES = new short[] { 120, 300 };
	private GebruikerManager manager;
	private Gebruiker myObject;

	public GebruikerReferenceField(String label, GebruikerManager mgr) throws Exception {
		super(mgr);
		this.manager = mgr;
		init();
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(15);
		setMaxLength(15);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;

			public void event(UIObject object, String type) throws Exception {
				String gbrId = getValue();
				if (gbrId.trim().length() == 0) {
					setDescription("");
				} else {
					try {
						setDescription(manager.findByPrimaryKey(gbrId).getNaam());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	private void init() throws Exception {
		InitializationRequest request = (InitializationRequest) ESPSessionContext.getSessionAttribute(SessionKeys.INIT_REQUEST);
		if (request != null && "true".equals(ESPSessionContext.getSessionAttribute(GEBRUIKERS_GESYNCHRONIZEERD)) == false) {
			UserData[] users = request.getEnvironment().getCurrentAndFutureUsersForCurrentProduct();
			ArrayList<Gebruiker> gebruikers = (ArrayList<Gebruiker>) manager.getCollection(null);
			HashMap<String, Gebruiker> gebruikersMap = new HashMap<String, Gebruiker>(gebruikers.size());
			for (int i = 0; i < gebruikers.size(); i++) {
				Gebruiker gebruiker = gebruikers.get(i);
				gebruikersMap.put(gebruiker.getId(), gebruiker);
			}
			for (int i = 0; i < users.length; i++) {
				Bedrijf bedrijf = null;
				Gebruiker gebruiker = gebruikersMap.get(users[i].getUserId());
				if (gebruiker == null) {
					GebruikerDataBean bean = new GebruikerDataBean();
					bean.setId(users[i].getUserId());
					bean.setNaam(users[i].getFirstName() + " " + users[i].getLastName());
					if (bedrijf == null) {
						DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
						bedrijf = BedrijfManagerFactory.getInstance(dbd).getFirstObject(null);
					}
					if (bedrijf != null) {
						bean.setBedrijf(bedrijf.getBedrijfscode());
						bean.setBoekjaar(Calendar.getInstance().get(Calendar.YEAR));
					}
					manager.add(bean);
				} else {
					gebruiker.setNaam(users[i].getFirstName() + " " + users[i].getLastName());
				}
			}
			ESPSessionContext.setSessionAttribute(GEBRUIKERS_GESYNCHRONIZEERD, "true");
		}
	}

	public void setGebruikerId(String id) throws Exception {
		setValue(id);
		try {
			setDescription(manager.findByPrimaryKey(id.trim()).getNaam());
		} catch (FinderException e) {
			setDescription("???");
			;
		}
	}

	public void setValue(Gebruiker gebruiker) throws Exception {
		if (gebruiker == null) {
			setDescription("");
			setValue();
			return;
		}
		setValue(gebruiker.getId());
		setDescription(gebruiker.getNaam());
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (Gebruiker) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getGebruiker();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \"" + getLabel() + "\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getNaam());
	}

	public Gebruiker getGebruiker() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			return manager.findByPrimaryKey(getValue().trim());
		} catch (FinderException e) {
			return null;
		}
	}
}