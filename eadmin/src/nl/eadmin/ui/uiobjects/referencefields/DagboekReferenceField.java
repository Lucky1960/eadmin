package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekManager;
import nl.eadmin.db.DagboekPK;
import nl.eadmin.ui.uiobjects.MyODBQueryReferenceField;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;

public class DagboekReferenceField extends MyODBQueryReferenceField {
	private static final long serialVersionUID = -8983331851067991530L;
	private static final String[] NAMES = new String[] { Dagboek.ID, Dagboek.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Dagboek", "Omschrijving" };
	private static final short[] SIZES = new short[] { 90, 250 };
	private DagboekManager myMgr;
	private DagboekPK myPK;
	private Dagboek myObject;

	public DagboekReferenceField(String label, Bedrijf bedrijf, DagboekManager mgr) throws Exception {
		this(label, bedrijf, mgr, null);
	}

	public DagboekReferenceField(String label, Bedrijf bedrijf, DagboekManager mgr, String filterDagboekSoort) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new DagboekPK();
		this.myPK.setBedrijf(bedrijf.getBedrijfscode());
		StringBuilder filter = new StringBuilder(Dagboek.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		if (filterDagboekSoort != null && filterDagboekSoort.trim().length() > 0) {
			filter.append(" AND " + Dagboek.SOORT + "='" + filterDagboekSoort + "'");
		}
		setFilter(filter.toString());
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				String myValue = getValue().trim();
				if(myValue.length()==0){
					setDescription("");
				} else {
					try {
						myPK.setId(myValue);
						setDescription(myMgr.findByPrimaryKey(myPK).getOmschrijving());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	public void setValue(Dagboek dagboek) throws Exception {
		if (dagboek == null) {
			setValue();
			setDescription("");
		} else {
			setValue(dagboek.getId());
			setDescription(dagboek.getOmschrijving());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (Dagboek) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getOmschrijving());
	}

	public Dagboek getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setId(getValue().trim());
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}