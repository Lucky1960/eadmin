package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.CodeTabelElement;
import nl.eadmin.db.CodeTabelElementManager;
import nl.eadmin.db.CodeTabelElementPK;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;

public class CodeTabelElementReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = -6373463490741907661L;
	private static final String[] NAMES = new String[] { CodeTabelElement.ID, CodeTabelElement.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Code", "Omschrijving" };
	private static final short[] SIZES = new short[] { 90, 250 };
	private CodeTabelElementManager myMgr;
	private CodeTabelElementPK myPK;
	private CodeTabelElement myObject;

	public CodeTabelElementReferenceField(String label, String bedrijf, String tabelNaam, CodeTabelElementManager mgr) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new CodeTabelElementPK();
		this.myPK.setBedrijf(bedrijf);
		this.myPK.setTabelNaam(tabelNaam);
		StringBuilder filter = new StringBuilder(CodeTabelElement.BEDRIJF + "='" + bedrijf + "' AND " + CodeTabelElement.TABEL_NAAM + "='" + tabelNaam + "'");
		setFilter(filter.toString());
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;

			public void event(UIObject object, String type) throws Exception {
				String myValue = getValue().trim();
				if (myValue.length() == 0) {
					setDescription("");
				} else {
					try {
						myPK.setId(myValue);
						setDescription(myMgr.findByPrimaryKey(myPK).getOmschrijving());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	public void setValue(CodeTabelElement codeTabel) throws Exception {
		if (codeTabel == null) {
			setValue();
			setDescription("");
		} else {
			setValue(codeTabel.getId());
			setDescription(codeTabel.getOmschrijving());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (CodeTabelElement) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \"" + getLabel() + "\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getOmschrijving());
	}

	public CodeTabelElement getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setId(getValue().trim());
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}