package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManager;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.ui.editors.CrediteurenEditor;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;

public class CrediteurReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = 2554251509959897619L;
	private static final String[] NAMES = new String[] { Crediteur.CRED_NR, Crediteur.NAAM, Crediteur.ADRESREGEL1, Crediteur.ADRESREGEL2  };
	private static final String[] LABELS = new String[] { "Crediteur", "Naam", "Adres", "Plaats" };
	private static final short[] SIZES = new short[] { 50, 150, 200, 200 };
	private CrediteurManager myMgr;
	private CrediteurPK myPK;
	private Crediteur myObject;

	public CrediteurReferenceField(String label, Bedrijf bedrijf, CrediteurManager mgr) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new CrediteurPK();
		this.myPK.setBedrijf(bedrijf.getBedrijfscode());
		setEditor(new CrediteurenEditor(mgr));
		setFilter(Crediteur.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				String myValue = getValue().trim();
				if(myValue.length()==0){
					setDescription("");
				} else {
					try {
						myPK.setCredNr(myValue);
						setDescription(myMgr.findByPrimaryKey(myPK).getNaam());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	public void setValue(Crediteur crediteur) throws Exception {
		if (crediteur == null) {
			setValue();
			setDescription("");
		} else {
			setValue(crediteur.getCredNr());
			setDescription(crediteur.getNaam());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (Crediteur) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getNaam());
	}

	public Crediteur getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setCredNr(getValue().trim());
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}