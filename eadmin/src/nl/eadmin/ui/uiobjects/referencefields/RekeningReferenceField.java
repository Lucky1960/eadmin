package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningPK;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.esp.util.ODBAutocompleteDataSource;
import nl.ibs.jsql.exception.FinderException;

public class RekeningReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = 9079118214686073332L;
	private static final String[] NAMES = new String[] { Rekening.REKENING_NR, Rekening.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Rekening", "Omschrijving" };
	private static final short[] SIZES = new short[] { 50, 250 };
	private RekeningManager myMgr;
	private RekeningPK myPK;
	private Rekening myObject;
	private int stdRekeningLengte;

	public RekeningReferenceField(String label, Bedrijf bedrijf, RekeningManager mgr) throws Exception {
		this(label, bedrijf, mgr, false);
	}

	public RekeningReferenceField(String label, Bedrijf bedrijf, RekeningManager mgr, boolean inclBlocked) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new RekeningPK();
		this.myPK.setBedrijf(bedrijf.getBedrijfscode());
		this.stdRekeningLengte = bedrijf.getInstellingen().getLengteRekening();
		StringBuilder sb = new StringBuilder(Rekening.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		if (!inclBlocked) {
			sb.append(" AND " + Rekening.BLOCKED + " = 'false'");
		}
		setFilter(sb.toString());
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(stdRekeningLengte);
		setMaxLength(stdRekeningLengte);
		setDescriptionLength(50);
		ODBAutocompleteDataSource naamADS = new ODBAutocompleteDataSource(Rekening.class,Rekening.REKENING_NR, new String[]{Rekening.OMSCHRIJVING});
		naamADS.setPrefilter(Rekening.BEDRIJF + "='"+bedrijf.getBedrijfscode()+"' ");
		setAutocomplete(naamADS, 2);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				Rekening rekening = null;
				String myValue = getValue().trim();
				if(myValue.length()==0){
					setDescription("");
				} else {
					if (myValue.length() != stdRekeningLengte) {
						myValue = ("000000000" + myValue);
						myValue = myValue.substring(myValue.length() - stdRekeningLengte);
						setValue(myValue);
					}
					try {
						myPK.setRekeningNr(myValue);
						rekening = myMgr.findByPrimaryKey(myPK);
						setDescription(rekening.getOmschrijving());
					} catch (Exception e) {
						setDescription("???");
					}
				}
				setSelectedObject(rekening);
			}
		});
	}

	public void setValue(Rekening rekening) throws Exception {
		if (rekening == null) {
			setValue();
			setDescription("");
		} else {
			setValue(rekening.getRekeningNr());
			setDescription(rekening.getOmschrijving());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (Rekening) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getOmschrijving());
	}

	public Rekening getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setRekeningNr(getValue().trim());
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}