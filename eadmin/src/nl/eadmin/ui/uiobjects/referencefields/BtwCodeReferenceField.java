package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeManager;
import nl.eadmin.db.BtwCodePK;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;

public class BtwCodeReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = 7112639900370887196L;
	private static final String[] NAMES = new String[] { BtwCode.CODE, BtwCode.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Code", "Omschrijving" };
	private static final short[] SIZES = new short[] { 50, 250 };
	private BtwCodeManager myMgr;
	private BtwCodePK myPK;
	private BtwCode myObject;

	public BtwCodeReferenceField(String label, Bedrijf bedrijf, BtwCodeManager mgr) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new BtwCodePK();
		this.myPK.setBedrijf(bedrijf.getBedrijfscode());
		setFilter(BtwCode.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				String myValue = getValue().trim();
				if(myValue.length()==0){
					setDescription("");
				} else {
					try {
						myPK.setCode(myValue);
						setDescription(myMgr.findByPrimaryKey(myPK).getOmschrijving());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	public void setValue(BtwCode btwCode) throws Exception {
		if (btwCode == null) {
			setValue();
			setDescription("");
		} else {
			setValue(btwCode.getCode());
			setDescription(btwCode.getOmschrijving());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (BtwCode) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getOmschrijving());
	}

	public BtwCode getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setCode(getValue().trim());
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}