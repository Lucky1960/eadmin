package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieManager;
import nl.eadmin.db.BtwCategoriePK;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;

public class BtwCategorieReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = -4882459450553772816L;
	private static final String[] NAMES = new String[] { BtwCategorie.CODE, BtwCategorie.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Categorie", "Omschrijving" };
	private static final short[] SIZES = new short[] { 50, 250 };
	private BtwCategorieManager myMgr;
	private BtwCategoriePK myPK;
	private BtwCategorie myObject;

	public BtwCategorieReferenceField(String label, Bedrijf bedrijf, BtwCategorieManager mgr) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new BtwCategoriePK();
		this.myPK.setBedrijf(bedrijf.getBedrijfscode());
		setFilter(BtwCategorie.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				BtwCategorie btwCat = null;
				String myValue = getValue().trim();
				if(myValue.length()==0){
					setDescription("");
				} else {
					try {
						myPK.setCode(myValue);
						btwCat = myMgr.findByPrimaryKey(myPK);
						setDescription(btwCat.getOmschrijving());
					} catch (Exception e) {
						setDescription("???");
					}
				}
				setSelectedObject(btwCat);
			}
		});
	}

	public void setValue(BtwCategorie btwCat) throws Exception {
		if (btwCat == null) {
			setValue();
			setDescription("");
		} else {
			setValue(btwCat.getCode());
			setDescription(btwCat.getOmschrijving());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (BtwCategorie) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getOmschrijving());
	}

	public BtwCategorie getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setCode(getValue().trim());
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}