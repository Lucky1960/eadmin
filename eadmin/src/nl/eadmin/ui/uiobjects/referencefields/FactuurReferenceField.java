package nl.eadmin.ui.uiobjects.referencefields;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;

public class FactuurReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = 4314584620337896703L;
	private static final String[] NAMES = new String[] { HeaderData.FACTUUR_NUMMER, HeaderData.OMSCHR1, HeaderData.TOTAAL_OPENSTAAND, HeaderData.DAGBOEK_ID };
	private static final String[] LABELS = new String[] { "Factuur", "Omschrijving", "Openstaand bedrag", "Dagboek" };
	private static final short[] SIZES = new short[] { 100, 250, 100, 100 };
	private Bedrijf bedrijf;
	private ArrayList<Dagboek> inkoopBoeken, verkoopBoeken, memoriaalBoeken;
	private boolean doValidation = true;

	@SuppressWarnings("unchecked")
	public FactuurReferenceField(String label, Bedrijf bedrijf, HeaderDataManager mgr) throws Exception {
		super(mgr);
		this.bedrijf = bedrijf;
		setFilter(HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setSelectableOnly(true);
		setLength(15);
		setMaxLength(15);
		setDescriptionLength(50);
		inkoopBoeken = (ArrayList<Dagboek>)GeneralHelper.getDagboeken(bedrijf, DagboekSoortEnum.INKOOP);
		verkoopBoeken = (ArrayList<Dagboek>)GeneralHelper.getDagboeken(bedrijf, DagboekSoortEnum.VERKOOP);
		memoriaalBoeken = (ArrayList<Dagboek>)GeneralHelper.getDagboeken(bedrijf, DagboekSoortEnum.MEMO);
	}

	public void setDcNummer(String dc, String dcNr) throws Exception {
		Collection<Dagboek> db = dc.equals("C") ? inkoopBoeken : verkoopBoeken;
		db.addAll(memoriaalBoeken);
		Iterator<?> dagboeken = db.iterator();
		StringBuilder sb = new StringBuilder();
		sb.append(HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'AND (");
		sb.append("DAGBOEK = '" + ((Dagboek) dagboeken.next()).getId() + "' ");
		while (dagboeken.hasNext()) {
			sb.append(" OR DAGBOEK = '" + ((Dagboek) dagboeken.next()).getId() + "' ");
		}
		sb.append(") AND " + HeaderData.DC_NUMMER + " = '" + (dcNr.length() > 0 ? dcNr : ApplicationConstants.DUMMYSTRING) + "'");
		sb.append(" AND " + HeaderData.TOTAAL_OPENSTAAND + " <> 0");
		setFilter(sb.toString());
	}

	public void setValue(HeaderData headerData) throws Exception {
		if (headerData == null) {
			setValue();
			setDescription("");
		} else {
			setValue(headerData.getFactuurNummer());
			setDescription(headerData.getOmschr1());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			setValue((HeaderData) object);
		} catch (Exception e) {
		}
		return true;
	}

	public void setValidation(boolean b) throws Exception {
		doValidation = b;
	}

	public void validate() throws Exception {
		if (doValidation) {
			super.validate();
		}
	}
}