package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfManager;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.helpers.UserAutHelper;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.exception.FinderException;

public class BedrijfReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = -8983331851067991530L;
	private static final String[] NAMES = new String[] { Bedrijf.BEDRIJFSCODE, Bedrijf.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Bedrijf", "Omschrijving" };
	private static final short[] SIZES = new short[] { 90, 290 };
	private BedrijfManager bedrijfManager;
	private Bedrijf bedrijf;

	public BedrijfReferenceField(String label, DBData dbd, BedrijfManager mgr, Gebruiker gebruiker) throws Exception {
		super(mgr);
		this.bedrijfManager = mgr;
		setFilter(UserAutHelper.getAdminFilter(dbd, gebruiker));
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(30);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				String bdrCode = getValue();
				if(bdrCode.trim().length()==0){
					setDescription("");
				} else {
					try {
						setDescription(bedrijfManager.findByPrimaryKey(bdrCode).getOmschrijving());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	public void setValue(Bedrijf bedrijf) throws Exception {
		if (bedrijf == null) {
			setDescription("");
			setValue();
			return;
		}
		setValue(bedrijf.getBedrijfscode());
		setDescription(bedrijf.getOmschrijving());
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			bedrijf = (Bedrijf) getSelectedObject();
			setValue(bedrijf);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		bedrijf = getBedrijf();
		if (bedrijf == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(bedrijf.getOmschrijving());
	}

	public Bedrijf getBedrijf() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			return bedrijfManager.findByPrimaryKey(getValue().trim());
		} catch (FinderException e) {
			return null;
		}
	}
}