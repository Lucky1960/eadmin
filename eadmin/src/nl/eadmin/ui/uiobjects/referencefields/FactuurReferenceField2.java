package nl.eadmin.ui.uiobjects.referencefields;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManager;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.helpers.RekeningHelper;
import nl.eadmin.ui.transformer.DatumTransformer;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.esp.uiobjects.CollectionReferenceField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.util.Transformer;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class FactuurReferenceField2 extends CollectionReferenceField {
	private static final long serialVersionUID = 4314584620337896703L;
	private static final String[] NAMES_D = new String[] { MyFactuur.FACTUUR_NR, MyFactuur.FACTUUR_DATUM, MyFactuur.OMSCHR1, MyFactuur.FACTUUR_BEDRAG, MyFactuur.SALDO, MyFactuur.DAGBOEK_ID, MyFactuur.BOEKSTUK };
	private static final String[] NAMES_C = new String[] { MyFactuur.FACTUUR_NR, MyFactuur.FACTUUR_DATUM, MyFactuur.OMSCHR1, MyFactuur.FACTUUR_BEDRAG, MyFactuur.SALDO, MyFactuur.DAGBOEK_ID, MyFactuur.BOEKSTUK };
	private static final String[] LABELS = new String[] { "Factuur", "Datum", "Omschrijving", "Fact.bedrag", "Openst.bedrag", "Dagboek", "Boekstuk" };
	private static final short[] SIZES = new short[] { 100, 80, 250, 100, 100, 100, 100 };
	private Bedrijf bedrijf;

	public FactuurReferenceField2(String label, Bedrijf bedrijf, JournaalManager mgr) throws Exception {
		super(null, MyFactuur.class);
		this.bedrijf = bedrijf;
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableHeaderLabels(LABELS);
		setTableHeaderSizes(SIZES);
		setSelectableOnly(true);
		setLength(15);
		setMaxLength(15);
		setDescriptionLength(50);
	}

	public void setDcNummer(String dc, String dcNr) throws Exception {
		if (dc.equals("C")) {
			setTableFields(NAMES_C);
			setValueField(NAMES_C[0]);
			setDescriptionField(NAMES_C[1]);
			setTableKeys(new String[] { NAMES_C[0] });
			setTableTransformers(new Transformer[] { null, new DatumTransformer(), null, new DecimalToAmountTransformer(), new DecimalToAmountTransformer(), null, null });
		} else {
			setTableFields(NAMES_D);
			setValueField(NAMES_D[0]);
			setDescriptionField(NAMES_D[1]);
			setTableKeys(new String[] { NAMES_D[0] });
			setTableTransformers(new Transformer[] { null, new DatumTransformer(), null, new DecimalToAmountTransformer(), new DecimalToAmountTransformer(), null, null });
		}
		Iterator<?> subAdmRek = new RekeningHelper(bedrijf.getDBData()).getSubAdmRekeningen(bedrijf, dc).iterator();
		StringBuilder sb = new StringBuilder();
//		sb.append("SELECT DAGBOEK, BOEKSTUK, BOEKDTM, FACTUUR, ABS(SUM(BEDRAGDEB)- SUM(BEDRAGCRED)) AS SALDO, OMSCHR1 FROM JOURNAAL");
		sb.append("SELECT FACTUUR, ABS(SUM(BEDRAGDEB)- SUM(BEDRAGCRED)) AS SALDO FROM JOURNAAL");
		sb.append(" WHERE BDR = '" + bedrijf.getBedrijfscode() + "'");
		sb.append("   AND (REKNR = '" + ((Rekening) subAdmRek.next()).getRekeningNr() + "' ");
		while (subAdmRek.hasNext()) {
			sb.append(" OR REKNR = '" + ((Rekening) subAdmRek.next()).getRekeningNr() + "' ");
		}
		sb.append(")  AND DCNR = '" + dcNr + "'");
		sb.append("   AND FACTUUR <> ''");
		sb.append(" GROUP BY FACTUUR");
		sb.append(" HAVING SALDO <> 0");
		sb.append(" ORDER BY FACTUUR");
		Collection<MyFactuur> beans = new ArrayList<MyFactuur>();

		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) JournaalManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		Object[][] rslt1 = qry.getResultArray();
		Object[][] rslt2 = null;
		String dagboek, boekstuk, factNr, omschr1;
		BigDecimal factBedrag, saldo;
		Date boekDatum;
		int ix;
		if (rslt1.length > 0 && rslt1[0].length > 0) {
			for (int i = 0; i < rslt1.length; i++) {
				factNr = (rslt1[i][0] == null) ? "" : ((String) rslt1[i][0]);
				saldo = (rslt1[i][1] == null) ? BigDecimal.ZERO : ((BigDecimal) rslt1[i][1]);
				sb = new StringBuilder();
				sb.append("SELECT DAGBOEK, BOEKSTUK, BOEKDTM, ABS(BEDRAGDEB + BEDRAGCRED), OMSCHR1 FROM JOURNAAL");
				sb.append(" WHERE BDR = '" + bedrijf.getBedrijfscode() + "'");
				sb.append("   AND (DAGBOEKSRT = '" + (dc.equals("D") ? "V" : "I") + "' OR DAGBOEKSRT = 'M')");
				sb.append("   AND DCNR = '" + dcNr + "'");
				sb.append("   AND FACTUUR = '" + factNr +"'");
				sb.append(" ORDER BY BOEKDTM, SEQNR");
				qry.setQuery(sb.toString());
				rslt2 = qry.getResultArray();
				dagboek = (rslt2[0][0] == null) ? "" : ((String) rslt2[0][0]);
				boekstuk = (rslt2[0][1] == null) ? "" : ((String) rslt2[0][1]);
				boekDatum = (rslt2[0][2] == null) ? null : ((Date) rslt2[0][2]);
				factBedrag = (rslt2[0][3] == null) ? BigDecimal.ZERO : ((BigDecimal) rslt2[0][3]);
				omschr1 = "";ix = -1; //Neem eerste omschrijving die gevuld is
				while (omschr1.length() == 0 && ++ix < rslt2.length) {
					omschr1 = (rslt2[ix][4] == null) ? "" : ((String) rslt2[ix][4]);
				}
				beans.add(new MyFactuur(dagboek, boekstuk, factNr, boekDatum, factBedrag, saldo, omschr1));
			}
		}
		setNewCollection(beans);
	}

	public void setValue(Journaal journaal) throws Exception {
		if (journaal == null) {
			setValue();
			setDescription("");
		} else {
			setValue(journaal.getFactuurNr());
			setDescription(journaal.getOmschr1());
		}
	}

	public class MyFactuur {
		public static final String DAGBOEK_ID = Journaal.DAGBOEK_ID;
		public static final String BOEKSTUK = Journaal.BOEKSTUK;
		public static final String FACTUUR_NR = Journaal.FACTUUR_NR;
		public static final String FACTUUR_DATUM = "factuurDatum";
		public static final String FACTUUR_BEDRAG = "factuurBedrag";
		public static final String SALDO = "Saldo";
		public static final String OMSCHR1 = Journaal.OMSCHR1;
		private String dagboekId, boekstuk, factuurNr, omschr1;
		private BigDecimal factuurBedrag, saldo;
		private Date factuurDatum;

		public MyFactuur(String dagboek, String boekstuk, String factNr, Date factuurDatum, BigDecimal factuurBedrag, BigDecimal saldo, String omschr) throws Exception {
			this.dagboekId = dagboek;
			this.boekstuk = boekstuk;
			this.factuurNr = factNr;
			this.factuurDatum = factuurDatum;
			this.factuurBedrag = factuurBedrag;
			this.saldo = saldo;
			this.omschr1 = omschr;
		}

		public String getDagboekId() throws Exception {
			return dagboekId;
		}

		public String getBoekstuk() throws Exception {
			return boekstuk;
		}

		public String getFactuurNr() throws Exception {
			return factuurNr;
		}

		public Date getFactuurDatum() throws Exception {
			return factuurDatum;
		}

		public BigDecimal getFactuurBedrag() throws Exception {
			return factuurBedrag;
		}

		public BigDecimal getSaldo() throws Exception {
			return saldo;
		}

		public String getOmschr1() throws Exception {
			return omschr1;
		}
	}
}