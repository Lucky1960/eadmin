package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.CodeTabel;
import nl.eadmin.db.CodeTabelManager;
import nl.eadmin.db.CodeTabelPK;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;

public class CodeTabelReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = -6373463490741907661L;
	private static final String[] NAMES = new String[] { CodeTabel.TABEL_NAAM, CodeTabel.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "CodeTabel", "Omschrijving" };
	private static final short[] SIZES = new short[] { 90, 250 };
	private CodeTabelManager myMgr;
	private CodeTabelPK myPK;
	private CodeTabel myObject;

	public CodeTabelReferenceField(String label, Bedrijf bedrijf, CodeTabelManager mgr) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new CodeTabelPK();
		this.myPK.setBedrijf(bedrijf.getBedrijfscode());
		StringBuilder filter = new StringBuilder(CodeTabel.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		setFilter(filter.toString());
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				String myValue = getValue().trim();
				if(myValue.length()==0){
					setDescription("");
				} else {
					try {
						myPK.setTabelNaam(myValue);
						setDescription(myMgr.findByPrimaryKey(myPK).getOmschrijving());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	public void setValue(CodeTabel codeTabel) throws Exception {
		if (codeTabel == null) {
			setValue();
			setDescription("");
		} else {
			setValue(codeTabel.getTabelNaam());
			setDescription(codeTabel.getOmschrijving());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (CodeTabel) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getOmschrijving());
	}

	public CodeTabel getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setTabelNaam(getValue().trim());
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}