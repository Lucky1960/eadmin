package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateManager;
import nl.eadmin.db.HtmlTemplatePK;
import nl.eadmin.ui.uiobjects.MyODBQueryReferenceField;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;

public class HtmlTemplateReferenceField extends MyODBQueryReferenceField {
	private static final long serialVersionUID = 7112639900370887196L;
	private static final String[] NAMES = new String[] { HtmlTemplate.ID, HtmlTemplate.OMSCHRIJVING };
	private static final String[] LABELS = new String[] { "Layoutnaam", "Omschrijving" };
	private static final short[] SIZES = new short[] { 100, 250 };
	private HtmlTemplateManager myMgr;
	private HtmlTemplatePK myPK;
	private HtmlTemplate myObject;

	public HtmlTemplateReferenceField(String label, Bedrijf bedrijf, HtmlTemplateManager mgr) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new HtmlTemplatePK();
		this.myPK.setBedrijf(bedrijf.getBedrijfscode());
		setFilter(HtmlTemplate.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				String myValue = getValue().trim();
				if(myValue.length()==0){
					setDescription("");
				} else {
					try {
						myPK.setId(myValue);
						setDescription(myMgr.findByPrimaryKey(myPK).getOmschrijving());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	public void setValue(HtmlTemplate htmlTemplate) throws Exception {
		if (htmlTemplate == null) {
			setValue();
			setDescription("");
		} else {
			setValue(htmlTemplate.getId());
			setDescription(htmlTemplate.getOmschrijving());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (HtmlTemplate) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getOmschrijving());
	}

	public HtmlTemplate getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setId(getValue().trim());
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}