package nl.eadmin.ui.uiobjects.referencefields;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManager;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.ui.editors.DebiteurenEditor;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserErrorMessage;
import nl.ibs.jsql.exception.FinderException;

public class DebiteurReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = 4314584620337896703L;
	private static final String[] NAMES = new String[] { Debiteur.DEB_NR, Debiteur.NAAM, Debiteur.ADRESREGEL1, Debiteur.ADRESREGEL2  };
	private static final String[] LABELS = new String[] { "Debiteur", "Naam", "Adres", "Plaats" };
	private static final short[] SIZES = new short[] { 80, 250, 200, 200 };
	private DebiteurManager myMgr;
	private DebiteurPK myPK;
	private Debiteur myObject;

	public DebiteurReferenceField(String label, Bedrijf bedrijf, DebiteurManager mgr) throws Exception {
		super(mgr);
		this.myMgr = mgr;
		this.myPK = new DebiteurPK();
		this.myPK.setBedrijf(bedrijf.getBedrijfscode());
		setEditor(new DebiteurenEditor(mgr, null));
		setFilter(Debiteur.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'");
		setLabel(label);
		setName(this.getClass().getName());
		setType(Field.TYPE_UPPER);
		setTableFields(NAMES);
		setTableHeaderLabels(LABELS);
		setValueField(NAMES[0]);
		setDescriptionField(NAMES[1]);
		setTableKeys(new String[] { NAMES[0] });
		setTableHeaderSizes(SIZES);
		setLength(10);
		setMaxLength(10);
		setDescriptionLength(50);
		addOnChangeListener(new EventListener() {
			private static final long serialVersionUID = 1L;
			public void event(UIObject object, String type) throws Exception {
				String myValue = getValue().trim();
				if(myValue.length()==0){
					setDescription("");
				} else {
					try {
						myPK.setDebNr(myValue);
						setDescription(myMgr.findByPrimaryKey(myPK).getNaam());
					} catch (Exception e) {
						setDescription("???");
					}
				}
			}
		});
	}

	public void setValue(Debiteur debiteur) throws Exception {
		if (debiteur == null) {
			setValue();
			setDescription("");
		} else {
			setValue(debiteur.getDebNr());
			setDescription(debiteur.getNaam());
		}
	}

	protected boolean selectedValueExecuteHook(DataObject object) {
		try {
			myObject = (Debiteur) getSelectedObject();
			setValue(myObject);
		} catch (Exception e) {
		}
		return true;
	}

	public void validate() throws Exception {
		super.validate();
		if (getValue().trim().length() == 0)
			return;
		myObject = getMyObject();
		if (myObject == null) {
			setInvalidTag();
			throw new UserErrorMessage("Veld \""+getLabel() +"\" bevat geen geldige waarde.");
		}
		setDescription(myObject.getNaam());
	}

	public Debiteur getMyObject() throws Exception {
		if (getValue().trim().length() == 0) {
			return null;
		}
		try {
			myPK.setDebNr(getValue().trim());
			return myMgr.findByPrimaryKey(myPK);
		} catch (FinderException e) {
			return null;
		}
	}
}