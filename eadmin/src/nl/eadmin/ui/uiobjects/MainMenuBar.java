package nl.eadmin.ui.uiobjects;

import java.util.Calendar;
import java.util.Date;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.AutorisatieManagerFactory;
import nl.eadmin.db.AutorisatiePK;
import nl.eadmin.db.Gebruiker;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.ui.adapters.AutorisatieProcess;
import nl.eadmin.ui.adapters.BedrijfProcess;
import nl.eadmin.ui.adapters.BtwCategorieProcess;
import nl.eadmin.ui.adapters.BtwCodeProcess;
import nl.eadmin.ui.adapters.CodeTabelProcess;
import nl.eadmin.ui.adapters.CrediteurenProcess;
import nl.eadmin.ui.adapters.DagboekProcess;
import nl.eadmin.ui.adapters.DebiteurenProcess;
import nl.eadmin.ui.adapters.EnvironmentProcess;
import nl.eadmin.ui.adapters.ExportSystemFileProcess;
import nl.eadmin.ui.adapters.FactureerProcess;
import nl.eadmin.ui.adapters.FactuurRegelProcess;
import nl.eadmin.ui.adapters.GebruikerProcess;
import nl.eadmin.ui.adapters.HoofdverdichtingProcess;
import nl.eadmin.ui.adapters.ImportFactuurRegelProcess;
import nl.eadmin.ui.adapters.ImportSystemFileProcess;
import nl.eadmin.ui.adapters.ProductProcess;
import nl.eadmin.ui.adapters.ProjectActiveUserAdapter;
import nl.eadmin.ui.adapters.RekeningProcess;
import nl.eadmin.ui.adapters.SluitBoekjaarProcess;
import nl.eadmin.ui.adapters.VerdichtingProcess;
import nl.eadmin.ui.uiobjects.actions.BalansAction;
import nl.eadmin.ui.uiobjects.actions.BtwAangifteAction;
import nl.eadmin.ui.uiobjects.actions.CrediteurenLijstAction;
import nl.eadmin.ui.uiobjects.actions.DebiteurenLijstAction;
import nl.eadmin.ui.uiobjects.actions.GrootboekOverzichtAction;
import nl.eadmin.ui.uiobjects.actions.IcpOverzichtAction;
import nl.eadmin.ui.uiobjects.actions.ImportBankAction;
import nl.eadmin.ui.uiobjects.actions.ImportCrediteurenAction;
import nl.eadmin.ui.uiobjects.actions.ImportDebiteurenAction;
import nl.eadmin.ui.uiobjects.actions.ImportDetailDataAction;
import nl.eadmin.ui.uiobjects.actions.ImportHeaderDataAction;
import nl.eadmin.ui.uiobjects.actions.ImportHoofdverdichtingenAction;
import nl.eadmin.ui.uiobjects.actions.ImportRekeningSchemaAction;
import nl.eadmin.ui.uiobjects.actions.ImportVerdichtingenAction;
import nl.eadmin.ui.uiobjects.actions.JournaalPostOvzAction;
import nl.eadmin.ui.uiobjects.actions.RebuildJournaalAction;
import nl.eadmin.ui.uiobjects.actions.ResetDBLevelAction;
import nl.eadmin.ui.uiobjects.actions.SelectDagboekAction;
import nl.eadmin.ui.uiobjects.actions.SelecteerAdministratieAction;
import nl.eadmin.usertool.UserToolProcess;
import nl.ibs.esp.helpers.DateHelper;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.ApplicationUpdateAction;
import nl.ibs.esp.uiobjects.LogoffAction;
import nl.ibs.esp.uiobjects.Menu;
import nl.ibs.esp.uiobjects.MenuBar;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.ViewAction;
import nl.ibs.jsql.DBData;
import nl.sf.api.InitializationRequest;
import nl.sf.api.ProductData;

public class MainMenuBar extends MenuBar {
	private static final long serialVersionUID = 6460507538801786943L;
	private Gebruiker gebruiker = (Gebruiker) ESPSessionContext.getSessionAttribute(SessionKeys.GEBRUIKER);
	private ProductData productData = (ProductData) ESPSessionContext.getSessionAttribute(SessionKeys.PRODUCT_DATA);
	// private boolean moduleInvoicing=productData ==
	// null?true:productData.getModule01();
	// private boolean moduleBookkeeping=productData ==
	// null?true:productData.getModule02();
	private boolean isAdmin;
	private String autMenu;

	
	public MainMenuBar() throws Exception {
		super();
		Action backOrLogoffAction=null;
		Menu settingsMenu=null;
		InitializationRequest request =(InitializationRequest)ESPSessionContext.getSessionAttribute(SessionKeys.INIT_REQUEST);
		if (request != null){
			backOrLogoffAction=request.getBackOrCloseAction();
			settingsMenu=request.getSettingsMenu();
		}
		if (backOrLogoffAction==null)	
			backOrLogoffAction = new LogoffAction().setIcon("door_out.png");
		
		checkLicense();
		isAdmin = gebruiker.isAdmin();
		AutorisatiePK key = new AutorisatiePK();
		key.setBedrijf(gebruiker.getBedrijf());
		key.setGebruikerId(gebruiker.getId());
		try {
			DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
			autMenu = AutorisatieManagerFactory.getInstance(dbd).findByPrimaryKey(key).getAutMenu();
		} catch (Exception e) {
			autMenu = "";
		}

		if (settingsMenu != null)
			addMenu(settingsMenu);
		if (request==null && isAdmin) {
			Menu menu = new Menu("Beheer applicatie");
			menu.setIcon("computer_edit.png");
			menu.addMenuItem(new Action("Gebruikers").setAdapter(GebruikerProcess.getStartPage()).setMethod(GebruikerProcess.START_ACTION).setIcon("user.png"));
			if (productData == null)
				menu.addMenuItem(new Action("Actieve gebruikers", ProjectActiveUserAdapter.class, ProjectActiveUserAdapter.SHOW).setIcon("group.png"));
			menu.addMenuItem(new ApplicationUpdateAction().setIcon("bug_go.png"));
			menu.addMenuItem(new Action("Hulpprogrammas").setAdapter(UserToolProcess.getStartPage()).setMethod(UserToolProcess.START_ACTION).setIcon("application_edit.png"));
			menu.addMenuItem(new RebuildJournaalAction());
			menu.addMenuItem(new ViewAction().setIcon("images.png"));
			menu.addMenuItem(new ResetDBLevelAction());
			menu.addMenuItem(new Action("Omgevingen").setAdapter(EnvironmentProcess.getStartPage()).setMethod(EnvironmentProcess.START_ACTION).setIcon("database_gear.png"));
			addMenu(menu);
		}

		// @formatter:off
		int menuNr = 0;
		boolean b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11;
		b1 = isAuthorized(menuNr, "Administraties");
		b2 = isAuthorized(menuNr, "Autorisaties");
		b3 = isAuthorized(menuNr, "Hoofdverdichtingen");
		b4 = isAuthorized(menuNr, "Verdichtingen");
		b5 = isAuthorized(menuNr, "Grootboekrekeningen");
		b6 = isAuthorized(menuNr, "Dagboeken");
		b7 = isAuthorized(menuNr, "Codetabellen");
		b8 = isAuthorized(menuNr, "BTW-categorieŽn");
		b9 = isAuthorized(menuNr, "BTW-codes");
		b10 = isAuthorized(menuNr, "Producten");
		b11 = isAuthorized(menuNr, "Afsluiten boekjaar");
		if (b1 || b2 || b3 || b4 || b5 || b6 || b7 || b8 || b9 || b10 || b11) {
			Menu menu = new Menu("Beheer administratie");
			menu.setIcon("computer_edit.png");
			if (b1) menu.addMenuItem(new Action("Administraties").setAdapter(BedrijfProcess.getStartPage()).setMethod(BedrijfProcess.START_ACTION).setIcon("building.png"));
			if (b2) menu.addMenuItem(new Action("Autorisaties").setAdapter(AutorisatieProcess.getStartPage()).setMethod(AutorisatieProcess.START_ACTION).setIcon("key.png"));
			if (b3) menu.addMenuItem(new Action("Hoofdverdichtingen").setAdapter(HoofdverdichtingProcess.getStartPage()).setMethod(HoofdverdichtingProcess.START_ACTION).setIcon("arrow_in.png"));
			if (b4) menu.addMenuItem(new Action("Verdichtingen").setAdapter(VerdichtingProcess.getStartPage()).setMethod(VerdichtingProcess.START_ACTION).setIcon("arrow_join.png"));
			if (b5) menu.addMenuItem(new Action("Grootboekrekeningen").setAdapter(RekeningProcess.getStartPage()).setMethod(RekeningProcess.START_ACTION).setIcon("text_list_numbers.png"));
			if (b6) menu.addMenuItem(new Action("Dagboeken").setAdapter(DagboekProcess.getStartPage()).setMethod(DagboekProcess.START_ACTION).setIcon("application_cascade.png"));
			if (b7) menu.addMenuItem(new Action("CodeTabellen").setAdapter(CodeTabelProcess.getStartPage()).setMethod(CodeTabelProcess.START_ACTION).setIcon("application_view_columns.png"));
			if (b8) menu.addMenuItem(new Action("BTW-categorieŽn").setAdapter(BtwCategorieProcess.getStartPage()).setMethod(BtwCategorieProcess.START_ACTION).setIcon("sum.png"));
			if (b9) menu.addMenuItem(new Action("BTW-codes").setAdapter(BtwCodeProcess.getStartPage()).setMethod(BtwCodeProcess.START_ACTION).setIcon("sum.png"));
			if (b10) menu.addMenuItem(new Action("Producten").setAdapter(ProductProcess.getStartPage()).setMethod(ProductProcess.START_ACTION).setIcon("cart.png"));
			if (b11) menu.addMenuItem(new Action("Afsluiten boekjaar").setAdapter(SluitBoekjaarProcess.getStartPage()).setMethod(SluitBoekjaarProcess.START_ACTION).setIcon("date.png"));
			addMenu(menu);
		}

		menuNr++;
		b1 = isAuthorized(menuNr, "Debiteuren");
		b2 = isAuthorized(menuNr, "Crediteuren");
		if (b1 || b2) {
			Menu menu = new Menu("Relaties");
			menu.setIcon("pencil.png");
			if (b1) menu.addMenuItem(new Action("Debiteuren").setAdapter(DebiteurenProcess.getStartPage()).setMethod(DebiteurenProcess.START_ACTION).setIcon("group.png"));
			if (b2) menu.addMenuItem(new Action("Crediteuren").setAdapter(CrediteurenProcess.getStartPage()).setMethod(CrediteurenProcess.START_ACTION).setIcon("group_error.png"));
			addMenu(menu);
		}

		menuNr++;
		b1 = isAuthorized(menuNr, "Bankboek");
		b2 = isAuthorized(menuNr, "Kasboek");
		b3 = isAuthorized(menuNr, "Inkoopboek");
		b4 = isAuthorized(menuNr, "Verkoopboek");
		b5 = isAuthorized(menuNr, "Memoriaal");
		b6 = isAuthorized(menuNr, "Importeren bankbestand");
		if (b1 || b2 || b3 || b4 || b5 || b6) {
			Menu menu = new Menu("Boekingen");
			menu.setIcon("money.png");
			if (b1) menu.addMenuItem(new SelectDagboekAction("Bankboek", DagboekSoortEnum.BANK).setIcon("page_edit.png"));
			if (b2) menu.addMenuItem(new SelectDagboekAction("Kasboek", DagboekSoortEnum.KAS).setIcon("page_edit.png"));
			if (b3) menu.addMenuItem(new SelectDagboekAction("Inkoopboek", DagboekSoortEnum.INKOOP).setIcon("page_edit.png"));
			if (b4) menu.addMenuItem(new SelectDagboekAction("Verkoopboek", DagboekSoortEnum.VERKOOP).setIcon("page_edit.png"));
			if (b5) menu.addMenuItem(new SelectDagboekAction("Memoriaal", DagboekSoortEnum.MEMO).setIcon("page_edit.png"));
			if (b6) menu.addMenuItem(new ImportBankAction("Importeren bankbestand"));
			addMenu(menu);
		}

		menuNr++;
		b1 = isAuthorized(menuNr, "Factuurregels invoeren");
		b2 = isAuthorized(menuNr, "Facturen maken");
		b3 = isAuthorized(menuNr, "Factuurregels importeren");
		if (b1 || b2 || b3) {
			Menu menu = new Menu("Factureren");
			menu.setIcon("application_view_columns.png");
			if (b1) menu.addMenuItem(new Action("Factuurregels invoeren").setAdapter(FactuurRegelProcess.getStartPage()).setMethod(FactuurRegelProcess.START_ACTION).setIcon("application_view_columns.png"));
			if (b2) menu.addMenuItem(new Action("Facturen maken").setAdapter(FactureerProcess.getStartPage()).setMethod(FactureerProcess.START_ACTION).setIcon("application_view_columns.png"));
			if (b3) menu.addMenuItem(new Action("Factuurregels importeren").setAdapter(ImportFactuurRegelProcess.getStartPage()).setMethod(ImportFactuurRegelProcess.START_ACTION).setIcon("database_download.png"));
			addMenu(menu);
		}

		menuNr++;
		b1 = isAuthorized(menuNr, "Grootboek");
		b2 = isAuthorized(menuNr, "Debiteuren");
		b3 = isAuthorized(menuNr, "Crediteuren");
		b4 = isAuthorized(menuNr, "BTW-aangifte");
		b5 = isAuthorized(menuNr, "ICP-overzicht");
		b6 = isAuthorized(menuNr, "Balans");
		b7 = isAuthorized(menuNr, "Journaalposten");
		if (b1 || b2 || b3 || b4 || b5 || b6 || b7) {
			Menu menu = new Menu("Overzichten");
			menu.setIcon("report.png");
			if (b1) menu.addMenuItem(new GrootboekOverzichtAction("Grootboek"));
			if (b2) menu.addMenuItem(new DebiteurenLijstAction("Debiteuren"));
			if (b3) menu.addMenuItem(new CrediteurenLijstAction("Crediteuren"));
			if (b4) menu.addMenuItem(new BtwAangifteAction("BTW-aangifte"));
			if (b5) menu.addMenuItem(new IcpOverzichtAction("ICP-overzicht"));
			if (b6) menu.addMenuItem(new BalansAction("Balans"));
			if (b7) menu.addMenuItem(new JournaalPostOvzAction("Journaalposten"));
			addMenu(menu);
		}

		menuNr++;
		b1 = isAdmin; //isAuthorized(menuNr, "Exporteren systeembestand");
		b2 = isAdmin; //isAuthorized(menuNr, "Importeren systeembestand");
		if (b2) {
			Menu menu = new Menu("Import/Export");
			menu.setIcon("arrow_in.png");
			menu.addMenuItem(new ImportDebiteurenAction());
			menu.addMenuItem(new ImportCrediteurenAction());
			menu.addMenuItem(new ImportHoofdverdichtingenAction());
			menu.addMenuItem(new ImportVerdichtingenAction());
			menu.addMenuItem(new ImportRekeningSchemaAction());
			menu.addMenuItem(new ImportHeaderDataAction());
			menu.addMenuItem(new ImportDetailDataAction());
			if (b1) menu.addMenuItem(new Action("Exporteren systeembestand").setAdapter(ExportSystemFileProcess.getStartPage()).setMethod(ExportSystemFileProcess.START_ACTION).setIcon("database_upload.png"));
			if (b2) menu.addMenuItem(new Action("Importeren systeembestand").setAdapter(ImportSystemFileProcess.getStartPage()).setMethod(ImportSystemFileProcess.START_ACTION).setIcon("database_download.png"));
			addMenu(menu);
		}
		// @formatter:on

		addAction(new SelecteerAdministratieAction((String) null));
		addAction(backOrLogoffAction);
	}

	private void checkLicense() throws Exception {
		// productData = new MyProductData();
		if (productData == null) {
			return;
		}
		Date toDay = Calendar.getInstance().getTime();
		long remainingDays = (productData.getEndDate().getTime()-toDay.getTime())/(1000*60*60*24)+1;
		if (toDay.before(productData.getStartDate()) || remainingDays<0) {
			throw new UserMessageException("Inloggen niet mogelijk. De licentie voor het gebruik van dit product loopt van " + DateHelper.getDDMMJJJJFormat(productData.getStartDate()) + " t/m "
					+ DateHelper.getDDMMJJJJFormat(productData.getEndDate()));
		} else if (remainingDays <= 7) {
			ESPSessionContext.setSessionAttribute(SessionKeys.LICENSE_MSG, "De licentie voor het gebruik van dit product verloopt over " + remainingDays + " dag(en)!");
		} else {
			ESPSessionContext.removeSessionAttribute(SessionKeys.LICENSE_MSG);
		}
	}

	private boolean isAuthorized(int menuNr, String menuOption) {
		try {
			return isAdmin || autMenu.indexOf(menuNr + menuOption) >= 0;
		} catch (Exception e) {
			return false;
		}
	}
}