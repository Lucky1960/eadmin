package nl.eadmin.ui.uiobjects.panels;

import java.math.BigDecimal;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.HeaderPanel;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.jsql.DBData;

public class DebCredHeaderPanel extends HeaderPanel {
	private static final long serialVersionUID = 1L;
	private DecimalToAmountTransformer amtTransformer = new DecimalToAmountTransformer();

	public DebCredHeaderPanel(String bedrijfsCode, String dcNr, String dc, boolean showContact, BigDecimal saldoDebet, BigDecimal saldoCredit) throws Exception {
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		String naam, contactpersoon, adresRgl1, adresRgl2, adresRgl3, telefoon;
		if (dc.equals("D")) {
			DebiteurPK key = new DebiteurPK(); 
			key.setBedrijf(bedrijfsCode);
			key.setDebNr(dcNr);
			Debiteur debiteur = DebiteurManagerFactory.getInstance(dbd).findOrCreate(key);
			naam = debiteur.getDebNr() + " - " + debiteur.getNaam();
			contactpersoon = debiteur.getContactPersoon().trim();
			adresRgl1 = debiteur.getAdresRegel1().trim();
			adresRgl2 = debiteur.getAdresRegel2().trim();
			adresRgl3 = debiteur.getAdresRegel3().trim();
			telefoon = debiteur.getAdres(false).getTelefoon();
		} else {
			CrediteurPK key = new CrediteurPK(); 
			key.setBedrijf(bedrijfsCode);
			key.setCredNr(dcNr);
			Crediteur crediteur = CrediteurManagerFactory.getInstance(dbd).findOrCreate(key);
			naam = crediteur.getCredNr() + " - " + crediteur.getNaam();
			contactpersoon = crediteur.getContactPersoon().trim();
			adresRgl1 = crediteur.getAdresRegel1().trim();
			adresRgl2 = crediteur.getAdresRegel2().trim();
			adresRgl3 = crediteur.getAdresRegel3().trim();
			telefoon = crediteur.getAdres(false).getTelefoon();
		}
		int row = 0;
		ESPGridLayout grid = new ESPGridLayout(); 
		grid.setColumnWidths(new short[]{100, 300});
		grid.setCellPadding(0);
		grid.setCellSpacing(0);
		grid.add(new Label(dc.equals("D") ? "Debiteur: " : "Crediteur: "), row, 0);
		grid.add(new Label(naam), row++, 1);
		if (showContact && contactpersoon.length() > 0) {
			grid.add(new Label(" (Contactpersoon: " + contactpersoon + ")"), row++, 1);
		}
		grid.add(new Label("Adres:"), row, 0);
		grid.add(new Label(adresRgl1), row++, 1);
		if (adresRgl2.length() > 0) {
			grid.add(new Label(adresRgl2), row++, 1);
		}
		grid.add(new Label(adresRgl3), row++, 1);
		if (telefoon.length() > 0) {
			grid.add(new Label("Telefoon:"), row, 0);
			grid.add(new Label(telefoon), row++, 1);
		}
		if (saldoDebet != null && saldoDebet.doubleValue() != 0d) {
			grid.add(new Label("Saldo (D):"), row, 0);
			grid.add(new Label((String)amtTransformer.transform(saldoDebet)), row++, 1);
		}
		if (saldoCredit != null && saldoCredit.doubleValue() != 0d) {
			grid.add(new Label("Saldo (C):"), row, 0);
			grid.add(new Label((String)amtTransformer.transform(saldoCredit)), row++, 1);
		}
		addUIObject(grid);
	}
}