package nl.eadmin.ui.uiobjects.panels;

import java.math.BigDecimal;
import java.sql.Date;

import nl.eadmin.databeans.BTWAangifteDataBean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.HTMLHelper;
import nl.eadmin.helpers.PDFHelper;
import nl.eadmin.util.PrintWebExtension;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.BinaryAction;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class BtwAangiftePanel extends Panel {
	private static final long serialVersionUID = 1L;
	private Label lbl01 = new Label("1. Door u verrichte binnenlandse leveringen/diensten");
	private Label lbl1a = new Label("1a Leveringen/diensten belast met 21%");
	private Label lbl1b = new Label("1b Leveringen/diensten belast met 6%");
	private Label lbl1c = new Label("1c Leveringen/diensten belast met overige tarieven behalve 0%");
	private Label lbl1d = new Label("1d Priv� gebruik");
	private Label lbl1e = new Label("1e Leveringen/diensten belast met 0% of niet door u belast");
	private Label lbl02 = new Label("2. Aan u verrichte binnenlandse leveringen/diensten");
	private Label lbl2a = new Label("2a Leveringen/diensten waarbij de heffing van omzetbelasting naar u is verlegd");
	private Label lbl03 = new Label("3. Door u verrichte leveringen aan het buitenland");
	private Label lbl3a = new Label("3a Leveringen naar landen buiten de EU (uitvoer)");
	private Label lbl3b = new Label("3b Leveringen naar landen binnen de EU");
	private Label lbl3c = new Label("3c Installatie/afstandverkopen binnen de EU");
	private Label lbl04 = new Label("4. Aan u verrichte leveringen vanuit het buitenland");
	private Label lbl4a = new Label("4a Leveringen uit landen buiten de EU (invoer)");
	private Label lbl4b = new Label("4b Verwervingen van goederen uit landen binnen de EU");
	private Label lbl05 = new Label("5. Berekening van de omzetbelasting");
	private Label lbl5a = new Label("5a Verschuldigde omzetbelasting (rubrieken 1a t/m 4b)");
	private Label lbl5b = new Label("5b Voorbelasting");
	private Label lbl5c = new Label("5c Subtotaal (rubriek 5a min 5b)");
	private Label lbl5d = new Label("5d Vermindering volgens de kleine ondernemersregeling");
	private Label lbl5e = new Label("5e Schatting vorige aangifte(n)");
	private Label lbl5f = new Label("5f Schatting deze aangifte");
	private Label lbl5g = new Label("   Totaal");
	private Label lbl06 = new Label("6. Bijzondere teruggaven");
	private Label lbl6a = new Label("6a Verzoek om teruggaaf wegens oninbare vorderingen");
	private Label lbl6b = new Label("6b Verzoek om teruggaaf wegens herrekening en andere correcties");
	private Label lblOmz = new Label("Omzet");
	private Label lblBtw = new Label("BTW");
	private Label lbl_5g = new Label("X");
	private Label lbl_5h = new Label("X");

	private DecimalField fldOmz_1a = new MyField();
	private DecimalField fldOmz_1b = new MyField();
	private DecimalField fldOmz_1c = new MyField();
	private DecimalField fldOmz_1d = new MyField();
	private DecimalField fldOmz_1e = new MyField();
	private DecimalField fldOmz_2a = new MyField();
	private DecimalField fldOmz_3a = new MyField();
	private DecimalField fldOmz_3b = new MyField();
	private DecimalField fldOmz_3c = new MyField();
	private DecimalField fldOmz_4a = new MyField();
	private DecimalField fldOmz_4b = new MyField();
	private DecimalField fldBtw_1a = new MyField();
	private DecimalField fldBtw_1b = new MyField();
	private DecimalField fldBtw_1c = new MyField();
	private DecimalField fldBtw_1d = new MyField();
	private DecimalField fldBtw_2a = new MyField();
	private DecimalField fldBtw_4a = new MyField();
	private DecimalField fldBtw_4b = new MyField();
	private DecimalField fldBtw_5a = new MyField();
	private DecimalField fldBtw_5b = new MyField();
	private DecimalField fldBtw_5c = new MyField();
	private DecimalField fldBtw_5d = new MyField();
	private DecimalField fldBtw_5e = new MyField();
	private DecimalField fldBtw_5f = new MyField();
	private DecimalField fldBtw_5g = new MyField();
	private DecimalField fldBtw_5h = new MyField();
	private DecimalField fldBtw_6a = new MyField();
	private DecimalField fldBtw_6b = new MyField();

	private BigDecimal ZERO = BigDecimal.ZERO.setScale(2);
	private Bedrijf bedrijf;
	private int boekjr1, boekjr2, period1, period2;
	private String title;

	// private boolean isMobileDevice;

	public BtwAangiftePanel(Bedrijf bedrijf, int boekjr1, int period1, int boekjr2, int period2) throws Exception {
		super();
		this.title = "BTW Aangifte periode " + boekjr1 + "-" + period1 + " t/m " + boekjr2 + "-" + period2;
		this.bedrijf = bedrijf;
		this.boekjr1 = boekjr1;
		this.boekjr2 = boekjr2;
		this.period1 = period1;
		this.period2 = period2;
		lblOmz.setCSSClass("btwAangifteOmzetHeader");
		lblBtw.setCSSClass("btwAangifteBTWHeader");

		int col0 = 0, col1 = 1, col2 = 2, col3 = 3, row = 0;
		int row1 = 0, row2 = 8, row3 = 12, row4 = 17, row5 = 21, row6 = 31;
		ESPGridLayout gridLbl = new ESPGridLayout();
		gridLbl.setColumnWidths(new short[] { 400, 100, 80, 80, 80 });
		gridLbl.setRowHeights(new short[] { 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25 });
		row = row1;
		gridLbl.add(lblOmz, row, col2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
		gridLbl.add(lblBtw, row++, col3, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
		gridLbl.add(lbl01, row++, col0);
		gridLbl.add(lbl1a, row, col0);
		gridLbl.add(fldOmz_1a, row, col2);
		gridLbl.add(fldBtw_1a, row++, col3);
		gridLbl.add(lbl1b, row, col0);
		gridLbl.add(fldOmz_1b, row, col2);
		gridLbl.add(fldBtw_1b, row++, col3);
		gridLbl.add(lbl1c, row, col0);
		gridLbl.add(fldOmz_1c, row, col2);
		gridLbl.add(fldBtw_1c, row++, col3);
		gridLbl.add(lbl1d, row, col0);
		gridLbl.add(fldOmz_1d, row, col2);
		gridLbl.add(fldBtw_1d, row++, col3);
		gridLbl.add(lbl1e, row, col0);
		gridLbl.add(fldOmz_1e, row++, col2);
		gridLbl.add(new Paragraph(""), row++, col0);
		row = row2;
		gridLbl.add(lbl02, row++, col0);
		gridLbl.add(lbl2a, row, col0);
		gridLbl.add(fldOmz_2a, row, col2);
		gridLbl.add(fldBtw_2a, row++, col3);
		gridLbl.add(new Paragraph(""), row++, col0);
		row = row3;
		gridLbl.add(lbl03, row++, col0);
		gridLbl.add(lbl3a, row, col0);
		gridLbl.add(fldOmz_3a, row++, col2);
		gridLbl.add(lbl3b, row, col0);
		gridLbl.add(fldOmz_3b, row++, col2);
		gridLbl.add(lbl3c, row, col0);
		gridLbl.add(fldOmz_3c, row++, col2);
		gridLbl.add(new Paragraph(""), row++, col0);
		row = row4;
		gridLbl.add(lbl04, row++, col0);
		gridLbl.add(lbl4a, row, col0);
		gridLbl.add(fldOmz_4a, row, col2);
		gridLbl.add(fldBtw_4a, row++, col3);
		gridLbl.add(lbl4b, row, col0);
		gridLbl.add(fldOmz_4b, row, col2);
		gridLbl.add(fldBtw_4b, row++, col3);
		gridLbl.add(new Paragraph(""), row++, col0);
		row = row5;
		gridLbl.add(lbl05, row++, col0);
		gridLbl.add(lbl5a, row, col0);
		gridLbl.add(fldBtw_5a, row++, col3);
		gridLbl.add(lbl5b, row, col0);
		gridLbl.add(fldBtw_5b, row++, col3);
		gridLbl.add(lbl5c, row, col0);
		gridLbl.add(fldBtw_5c, row++, col3);
		gridLbl.add(lbl5d, row, col0);
		gridLbl.add(fldBtw_5d, row++, col3);
		gridLbl.add(lbl5e, row, col0);
		gridLbl.add(fldBtw_5e, row++, col3);
		gridLbl.add(lbl5f, row, col0);
		gridLbl.add(fldBtw_5f, row++, col3);
		gridLbl.add(lbl5g, row, col0);
		gridLbl.add(new Label("Te betalen"), row, col1);
		gridLbl.add(lbl_5g, row, col2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
		gridLbl.add(fldBtw_5g, row++, col3);
		gridLbl.add(new Label("Terug te vragen"), row, col1);
		gridLbl.add(lbl_5h, row, col2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
		gridLbl.add(fldBtw_5h, row++, col3);
		row = row6;
		gridLbl.add(lbl06, row++, col0);
		gridLbl.add(lbl6a, row, col0);
		gridLbl.add(fldBtw_6a, row++, col3);
		gridLbl.add(lbl6b, row, col0);
		gridLbl.add(fldBtw_6b, row++, col3);
		// gridLbl.add(createPrintAction(1), row++, col3, 1, 1,
		// ESPGridLayoutConstraints.GRID_FILL_NONE,
		// ESPGridLayoutConstraints.GRID_ANCHOR_SOUTHEAST);
		gridLbl.add(createFloatBar(), row++, col0);
		Panel pnlGrid = new Panel();
		pnlGrid.addUIObject(gridLbl);
		addUIObject(pnlGrid);
		recalculate();
	}

	private void recalculate() throws Exception {
		// @formatter:off
		BigDecimal tmpAmt = ZERO;
		fldOmz_1a.setValue(getValues("1A", "O"));
		fldBtw_1a.setValue(getValues("1A", "B"));
		fldOmz_1b.setValue(getValues("1B", "O"));
		fldBtw_1b.setValue(getValues("1B", "B"));
		fldOmz_1e.setValue(getValues("1E", "O"));
		fldOmz_3b.setValue(getValues("3B", "O"));
		fldBtw_5b.setValue(getValues("5B", "B"));
		fldBtw_5a.setValue(fldBtw_1a.getBigDecimal().add(fldBtw_1b.getBigDecimal()).add(fldBtw_1c.getBigDecimal()).add(fldBtw_1d.getBigDecimal()).add(fldBtw_2a.getBigDecimal()).add(fldBtw_4a.getBigDecimal()).add(fldBtw_4b.getBigDecimal()));
		fldBtw_5c.setValue(fldBtw_5a.getBigDecimal().subtract(fldBtw_5b.getBigDecimal()));
		tmpAmt = ZERO.add(fldBtw_5c.getBigDecimal().add(fldBtw_5d.getBigDecimal()).add(fldBtw_5e.getBigDecimal().add(fldBtw_5f.getBigDecimal())));
		if (tmpAmt.doubleValue() >= 0d) {
			lbl_5g.setHidden(false);
			lbl_5h.setHidden(true);
			fldBtw_5g.setValue(tmpAmt.abs());
			fldBtw_5h.setValue(ZERO);
		} else {
			lbl_5g.setHidden(true);
			lbl_5h.setHidden(false);
			fldBtw_5f.setValue(ZERO);
			fldBtw_5h.setValue(tmpAmt.abs());
		}
		// @formatter:on
	}

	private BigDecimal getValues(String btwCat, String btw) throws Exception {
		String schema = GeneralHelper.getSchemaName();
		BigDecimal result = ZERO;
		StringBuilder sb = new StringBuilder();
		if (btwCat.equals("5B")) {
			sb.append("SELECT SUM(BEDRAGDEB) - SUM(BEDRAGCRED)");
		} else {
			sb.append("SELECT SUM(BEDRAGCRED) - SUM(BEDRAGDEB)");
		}
		sb.append(" FROM " + schema + ".JOURNAAL ");
		sb.append("WHERE BDR = '" + bedrijf.getBedrijfscode() + "'");
		sb.append("  AND BTWCAT = '" + btwCat + "'");
		sb.append("  AND BTWAANGKOL = '" + btw + "'");
		sb.append(" AND (BOEKJAAR > " + boekjr1 + " OR (BOEKJAAR = " + boekjr1 + " AND PERIODE >= " + period1 + "))");
		sb.append(" AND (BOEKJAAR < " + boekjr2 + " OR (BOEKJAAR = " + boekjr2 + " AND PERIODE <= " + period2 + "))");
		FreeQuery freeq = QueryFactory.createFreeQuery((JournaalManager_Impl) JournaalManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		Object[][] maxRslt = freeq.getResultArray();
		if (maxRslt.length > 0 && maxRslt[0].length > 0) {
			result = (maxRslt[0][0] == null) ? ZERO : ((BigDecimal) maxRslt[0][0]);
		}
		return result;
	}

	private FloatBar createFloatBar() throws Exception {
		FloatBar floatBar = new FloatBar();
		floatBar.addAction(new PrintAction());
		floatBar.addAction(new BinaryPrintAction());
		return floatBar;
	}

	protected BinaryObject downloadPDF(DataObject object) throws Exception {
		byte[] bytes = PDFHelper.getBTWAangiftePDF(getBTWAangifteDataBean());
		BinaryObject obj = new BinaryObject(bytes, BinaryObject.TYPE_PDF, object);
		obj.setName(title + ".pdf");
		return obj;
	}

	public DataObject openPrintDialog(DataObject object) throws Exception {
		BTWAangifteDataBean btwAangifteDataBean = getBTWAangifteDataBean();
		String html = HTMLHelper.getBTWAangifteHTML(btwAangifteDataBean);
		String name = "BTW_Aangifte_" + new Date(System.currentTimeMillis());
		object.addUIObject(new PrintWebExtension(object.getScreen(), name, html));
		return object;
	}

	private BTWAangifteDataBean getBTWAangifteDataBean() {
		BTWAangifteDataBean bean = new BTWAangifteDataBean();
		bean.setTitle(title);
		bean.setRubriek_1a_Omzet(fldOmz_1a.getValue());
		bean.setRubriek_1b_Omzet(fldOmz_1b.getValue());
		bean.setRubriek_1c_Omzet(fldOmz_1c.getValue());
		bean.setRubriek_1d_Omzet(fldOmz_1d.getValue());
		bean.setRubriek_1e_Omzet(fldOmz_1e.getValue());
		bean.setRubriek_1a_BTW(fldBtw_1a.getValue());
		bean.setRubriek_1b_BTW(fldBtw_1b.getValue());
		bean.setRubriek_1c_BTW(fldBtw_1c.getValue());
		bean.setRubriek_1d_BTW(fldBtw_1d.getValue());
		bean.setRubriek_2a_Omzet(fldOmz_2a.getValue());
		bean.setRubriek_2a_BTW(fldBtw_2a.getValue());
		bean.setRubriek_3a_Omzet(fldOmz_3a.getValue());
		bean.setRubriek_3b_Omzet(fldOmz_3b.getValue());
		bean.setRubriek_3c_Omzet(fldOmz_3c.getValue());
		bean.setRubriek_4a_Omzet(fldOmz_4a.getValue());
		bean.setRubriek_4b_Omzet(fldOmz_4b.getValue());
		bean.setRubriek_4a_BTW(fldBtw_4a.getValue());
		bean.setRubriek_4b_BTW(fldBtw_4b.getValue());
		bean.setRubriek_5a_BTW(fldBtw_5a.getValue());
		bean.setRubriek_5b_BTW(fldBtw_5b.getValue());
		bean.setRubriek_5c_BTW(fldBtw_5c.getValue());
		bean.setRubriek_5d_BTW(fldBtw_5d.getValue());
		bean.setRubriek_5e_BTW(fldBtw_5e.getValue());
		bean.setRubriek_5f_BTW(fldBtw_5f.getValue());
		bean.setRubriek_Totaal_Te_betalen_BTW(fldBtw_5g.getValue());
		bean.setRubriek_Totaal_Terug_te_vragen_BTW(fldBtw_5h.getValue());
		bean.setRubriek_6a_BTW(fldBtw_6a.getValue());
		bean.setRubriek_6b_BTW(fldBtw_6b.getValue());
		return bean;
	}

	private class MyField extends DecimalField {
		private static final long serialVersionUID = 5346490986158082382L;

		public MyField() {
			super("", 15, 2, false);
			setReadonly(true);
		}

		public BigDecimal getBigDecimal() throws Exception {
			return isEmpty() ? new BigDecimal(parse("0").toString()) : new BigDecimal(parse(getValue()).toString());
		}
	}

	private class BinaryPrintAction extends BinaryAction {
		private static final long serialVersionUID = 1L;

		public BinaryPrintAction() throws Exception {
			super("Opslaan als PDF");
			setIcon("page_white_acrobat.png");
		}

		@Override
		public BinaryObject getBinaryObject(DataObject object) throws Exception {
			// return downloadHTML(object,periode);//Werk voor printen zelf,
			// maar niet handig mbt opslaan of versturen op mobile devices;
			return downloadPDF(object);
		}
	}

	private class PrintAction extends Action {
		private static final long serialVersionUID = 1L;

		public PrintAction() {
			super("Print");
			setIcon("printer.png");
		}

		@Override
		public boolean execute(DataObject object) throws Exception {
			openPrintDialog(object);
			return true;
		};
	}

}
