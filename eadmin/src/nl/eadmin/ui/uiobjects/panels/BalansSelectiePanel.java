package nl.eadmin.ui.uiobjects.panels;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.eadmin.ui.uiobjects.ComboBoekjaar;
import nl.eadmin.ui.uiobjects.MyRadioGroup;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.RadioButton;
import nl.ibs.esp.uiobjects.Window;

public class BalansSelectiePanel extends Panel {
	private static final long serialVersionUID = 1L;
	private Bedrijf bedrijf;
	private ComboBoekjaar cbBoekjr;
	private MyRadioGroup rgSoort, rgVolgorde;
	private CheckBox cbExcludeEmpty;

	public BalansSelectiePanel(CloseWindowAction closeAction) throws Exception {
		// this.isMobileDevice = ESPSessionContext.isMobileDevice();
		this.bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		this.cbBoekjr = new ComboBoekjaar(bedrijf);
		addUIObject(createSelectionPanel());
		addUIObject(createFloatBar(closeAction));
	}

	private Panel createSelectionPanel() throws Exception {
		RadioButton srt1 = new RadioButton("Balansrekeningen", RekeningSoortEnum.BALANS);
		RadioButton srt2 = new RadioButton("Resultatenrekeningen", RekeningSoortEnum.RESULT);
		RadioButton srt3 = new RadioButton("Beide", "*").setChecked(true);
		rgSoort = new MyRadioGroup("Soort overzicht");
		rgSoort.add(srt1);
		rgSoort.add(srt2);
		rgSoort.add(srt3);

		RadioButton vlg1 = new RadioButton("Rekeningnummer", "1").setChecked(true);
		RadioButton vlg2 = new RadioButton("Rekeningsoort", "2");
		rgVolgorde = new MyRadioGroup("Volgorde overzicht");
		rgVolgorde.add(vlg1);
		rgVolgorde.add(vlg2);
		cbExcludeEmpty = new CheckBox("Toon alleen rekeningen waarop geboekt is");
		cbExcludeEmpty.setValue(true);

		FieldGroup fg = new FieldGroup();
		fg.forceBorder(false);
		fg.add(rgSoort);
		// fg.add(rgVolgorde);
		fg.add(cbExcludeEmpty);
		Panel panel = new Panel();
		panel.addUIObject(cbBoekjr);
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(fg);
		return panel;
	}

	private FloatBar createFloatBar(CloseWindowAction closeAction) throws Exception {
		FloatBar fb = new FloatBar();
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				Window window = new Window();
				window.setLabel("Balansoverzicht");
				window.add(new BalansOverzichtPanel(rgSoort.getSelectedValue(), Integer.parseInt(cbBoekjr.getSelectedOptionValue()), cbExcludeEmpty.getValueAsBoolean(), new CloseWindowAction(window)));
				object.addUIObject(window);
				return true;
			};
		};
		fb.addAction(ok);
		fb.addAction(closeAction);
		return fb;
	}
}