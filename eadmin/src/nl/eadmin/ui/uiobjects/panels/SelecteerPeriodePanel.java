package nl.eadmin.ui.uiobjects.panels;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Periode;
import nl.eadmin.db.PeriodeManager;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.PeriodePK;
import nl.eadmin.ui.uiobjects.ComboBoekjaar;
import nl.eadmin.ui.uiobjects.ComboBoekperiode;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;

public class SelecteerPeriodePanel extends Panel {
	private static final long serialVersionUID = 1L;
	private ComboBoekjaar cbBoekjr1, cbBoekjr2;
	private ComboBox cbPeriod1, cbPeriod2;
	private Bedrijf bedrijf;
	private PeriodeManager perMgr;

	public SelecteerPeriodePanel(Bedrijf bedrijf, boolean showPeriodTO) throws Exception {
		this.bedrijf = bedrijf;
		this.perMgr = PeriodeManagerFactory.getInstance(bedrijf.getDBData());
		cbBoekjr1 = new ComboBoekjaar(bedrijf);
		cbBoekjr2 = new ComboBoekjaar(bedrijf);
		cbPeriod1 = new  ComboBoekperiode(bedrijf);
		cbPeriod2 = new  ComboBoekperiode(bedrijf);
		cbBoekjr1.addOnChangeListener((EventListener)cbPeriod1);
		cbBoekjr2.addOnChangeListener((EventListener)cbPeriod2);
		cbBoekjr1.setDiscardLabel(true);
		cbBoekjr2.setDiscardLabel(true);
		cbPeriod1.setDiscardLabel(true);
		cbPeriod2.setDiscardLabel(true);
		((EventListener)cbPeriod1).event(cbBoekjr1, "");
		((EventListener)cbPeriod2).event(cbBoekjr2, "");
		ESPGridLayout grid = new ESPGridLayout();
		grid.setColumnWidths(new short[]{150, 50, 60, 50, 50, 50});
		grid.add(new Label(showPeriodTO ? "Periode VAN" : "Periode"), 0, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_CENTER);
		grid.add(cbBoekjr1, 0, 1);
		grid.add(cbPeriod1, 0, 2);
		if (showPeriodTO) {
			grid.add(new Label("T/M"), 0, 3, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_CENTER);
			grid.add(cbBoekjr2, 0, 4);
			grid.add(cbPeriod2, 0, 5);
		}
		addUIObject(grid);
	}

	public Periode getPeriodFrom() throws Exception {
		PeriodePK key = new PeriodePK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setBoekjaar(Integer.parseInt(cbBoekjr1.getSelectedOptionValue()));
		key.setBoekperiode(Integer.parseInt(cbPeriod1.getSelectedOptionValue()));
		try {
			return perMgr.findByPrimaryKey(key);
		} catch (Exception e) {
			return null;
		}
	}

	public Periode getPeriodTo() throws Exception {
		PeriodePK key = new PeriodePK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setBoekjaar(Integer.parseInt(cbBoekjr2.getSelectedOptionValue()));
		key.setBoekperiode(Integer.parseInt(cbPeriod2.getSelectedOptionValue()));
		try {
			return perMgr.findByPrimaryKey(key);
		} catch (Exception e) {
			return null;
		}
	}

	public int getBoekjaar1() throws Exception {
		try {
			return Integer.parseInt(cbBoekjr1.getSelectedOptionValue());
		} catch (Exception e) {
			return 0;
		}
	}

	public int getPeriode1() throws Exception {
		try {
			return Integer.parseInt(cbPeriod1.getSelectedOptionValue());
		} catch (Exception e) {
			return 0;
		}
	}

	public int getBoekjaar2() throws Exception {
		try {
			return Integer.parseInt(cbBoekjr2.getSelectedOptionValue());
		} catch (Exception e) {
			return 0;
		}
	}

	public int getPeriode2() throws Exception {
		try {
			return Integer.parseInt(cbPeriod2.getSelectedOptionValue());
		} catch (Exception e) {
			return 0;
		}
	}
}