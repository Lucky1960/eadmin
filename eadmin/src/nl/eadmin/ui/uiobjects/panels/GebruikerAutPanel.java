package nl.eadmin.ui.uiobjects.panels;

import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Autorisatie;
import nl.eadmin.enums.RoleEnum;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Tab;
import nl.ibs.esp.uiobjects.TabbedPanel;

public class GebruikerAutPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private static final String SEPARATOR = ApplicationConstants.SEPARATOR;
	private ESPGridLayout optionGrid;

	// @formatter:off
	private static final String[][] menuOpt = new String[][] {
			{"111_Beheer administratie", "100_Administraties", "100_Autorisaties", "110_Hoofdverdichtingen", "110_Verdichtingen", "111_Grootboekrekeningen", "110_Dagboeken", "110_Codetabellen", "110_BTW-categorieŽn", "100_BTW-codes", "100_Producten", "100_Afsluiten boekjaar"},
			{"111_Onderhoud", "111_Debiteuren", "111_Crediteuren"},
			{"111_Boekingen", "111_Bankboek", "111_Kasboek", "111_Inkoopboek", "111_Verkoopboek", "111_Memoriaal", "111_Importeren bankbestand"},
			{"111_Factureren", "111_Factuurregels invoeren", "111_Facturen maken", "111_Factuurregels importeren"},
			{"111_Overzichten", "111_Grootboek", "111_Debiteuren", "111_Crediteuren", "111_BTW-aangifte", "111_ICP-overzicht", "111_Balans", "111_Journaalposten"},
			{"110_Import/Export", "110_Exporteren systeembestand", "110_Importeren systeembestand"},
	};
	// @formatter:on

	public GebruikerAutPanel() throws Exception {
		addUIObject(createTabbedPanel());
	}

	private TabbedPanel createTabbedPanel() throws Exception {
		TabbedPanel tabbedPanel = new TabbedPanel();
		tabbedPanel.add(createMenuTab());
		return tabbedPanel;
	}

	private Tab createMenuTab() throws Exception {
		Tab tab = new Tab("Menu-opties");
		int row = 0, col = 0;
		optionGrid = new ESPGridLayout();

		for (col = 0; col < menuOpt.length; col++) {
			String[] opt = menuOpt[col];
			row = 0;
			if (opt.length == 1) {
				optionGrid.add(createCheckBox(-999, name(menuOpt[col][row])), row, col);
			} else {
				optionGrid.add(createLabel((name(menuOpt[col][row]))), row, col);
				for (row = 1; row < opt.length; row++) {
					optionGrid.add(createCheckBox(col, name(menuOpt[col][row])), row, col);
				}
			}
		}
		FloatBar fb = new FloatBar();
		fb.addAction(createDefaultsAction(RoleEnum.ROL_ACCOUNTANT));
		fb.addAction(createDefaultsAction(RoleEnum.ROL_BEHEERDER));
		fb.addAction(createDefaultsAction(RoleEnum.ROL_GEBRUIKER));
		Panel pnl = new Panel();
		pnl.setCSSClass("menuAutorisationPanel");
		pnl.addUIObject(optionGrid);
		pnl.addUIObject(fb);
		tab.add(pnl);
		return tab;
	}

	private Action createDefaultsAction(final String rol) throws Exception {
		Action action = new Action("Std.waardes " + RoleEnum.getValue(rol)) {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				setDefaults(rol);
				return true;
			}
		}.setIcon("tick.png");
		return action;
	}

	private void setDefaults(String rol) throws Exception {
		if (rol.equals(RoleEnum.ROL_ADMIN)) {
			Iterator<?> checkBoxes = optionGrid.getUIObjects(CheckBox.class, false).iterator();
			while (checkBoxes.hasNext()) {
				((CheckBox) checkBoxes.next()).setValue(true);
			}
		} else {
			StringBuilder sb = new StringBuilder();
			int p = "CBG".indexOf(rol), col, row;
			for (col = 0; col < menuOpt.length; col++) {
				for (row = 0; row < menuOpt[col].length; row++) {
					String name = col + name(menuOpt[col][row]);
					boolean authorized = (menuOpt[col][row]).substring(p, p + 1).equals("1");
					CheckBox cb = (CheckBox) optionGrid.getUIOBjectByName(CheckBox.class, name);
					if (cb != null) {
						cb.setValue(authorized);
						if (authorized) {
							sb.append(name + SEPARATOR);
						}
					}
				}
			}
			load(sb.toString());
		}
	}

	public static String getAllAuthorisations() throws Exception {
		StringBuilder sb = new StringBuilder();
		for (int col = 0; col < menuOpt.length; col++) {
			for (int row = 1; row < menuOpt[col].length; row++) {
				sb.append(col + name(menuOpt[col][row]));
				sb.append(SEPARATOR);
			}
		}
		return sb.toString();
	}

	private static String name(String option) throws Exception {
		return option.substring(4);
	}

	private CheckBox createCheckBox(int menuNr, String name) throws Exception {
		CheckBox cb = new CheckBox(name);
		cb.setOrientationCheckBFirst();
		cb.setName(menuNr + name);
		return cb;
	}

	private Label createLabel(String opt) throws Exception {
		Label lbl = new Label(opt);
		lbl.setCSSClass("menuAutorisationHeaderLabel");
		return lbl;
	}

	//
	// public void setGebruiker(Gebruiker gebruiker, boolean reload) throws
	// Exception {
	// this.gebruiker = gebruiker;
	// if (reload) {
	// loadAutMenu(gebruiker.getAutMenu() == null ? "" :
	// gebruiker.getAutMenu());
	// }
	// }

	public void load(String autMenu) throws Exception {
		Iterator<?> checkBoxes = optionGrid.getUIObjects(CheckBox.class, false).iterator();
		while (checkBoxes.hasNext()) {
			CheckBox cb = ((CheckBox) checkBoxes.next());
			cb.setValue(autMenu.indexOf(cb.getName() + SEPARATOR) >= 0);
		}
		optionGrid.setModified();
	}

	public void save(Autorisatie autorisatie) throws Exception {
		StringBuilder sb = new StringBuilder();
		Iterator<?> checkBoxes = optionGrid.getUIObjects(CheckBox.class, false).iterator();
		while (checkBoxes.hasNext()) {
			CheckBox cb = ((CheckBox) checkBoxes.next());
			if (cb.getValueAsBoolean()) {
				sb.append(cb.getName());
				sb.append(SEPARATOR);
			}
		}
		autorisatie.setAutMenu(sb.toString());
	}
}