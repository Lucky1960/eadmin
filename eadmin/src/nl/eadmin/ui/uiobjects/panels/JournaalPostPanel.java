package nl.eadmin.ui.uiobjects.panels;

import java.math.BigDecimal;
import java.util.Iterator;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.Rekening;
import nl.eadmin.helpers.JournaalHelper;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.OutputField;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.jsql.DBData;

public class JournaalPostPanel extends Panel {
	private static final long serialVersionUID = 1L;

	public JournaalPostPanel(String bedrijf, String dagboek, String boekstuk) throws Exception {
		DecimalToAmountTransformer trf = new DecimalToAmountTransformer();
		ESPGridLayout grid = new ESPGridLayout();
		grid.setColumnWidths(new short[] { 80, 300 });
		grid.setPaddingTop(0);
		grid.setPaddingBottom(0);
		Label lbl1 = new Label("Rek.nr.");
		Label lbl2 = new Label("Omschrijving");
		Label lbl3 = new Label("Bedrag Debet");
		Label lbl4 = new Label("Bedrag Credit");
		lbl1.setCSSClass("journaalPost");
		lbl2.setCSSClass("journaalPost");
		lbl3.setCSSClass("journaalPost");
		lbl4.setCSSClass("journaalPost");
		grid.add(lbl1, 0, 0);
		grid.add(lbl2, 0, 1);
		grid.add(lbl3, 0, 2);
		grid.add(lbl4, 0, 3);
		int row = 1;
		OutputField outFld = null;
		Journaal journaal = null;
		Rekening rekening = null;
		BigDecimal amtDebt, amtCred;
		BigDecimal totalD = new BigDecimal(0.00), totalC = new BigDecimal(0.00);
		DBData dbd = (DBData)ESPSessionContext.getSessionAttribute(SessionKeys.ACTIVE_DB_DATA);
		Iterator<?> journals = new JournaalHelper(dbd).getJournals(bedrijf, dagboek, boekstuk).iterator();
		while (journals.hasNext()) {
			journaal = (Journaal) journals.next();
			rekening = journaal.getRekeningObject();
			amtDebt = journaal.getBedragDebet();
			amtCred = journaal.getBedragCredit();
			totalD = totalD.add(amtDebt);
			totalC = totalC.add(amtCred);
			grid.add(new Label(journaal.getRekeningNr()), row, 0);
			grid.add(new Label(rekening == null ? "???" : rekening.getOmschrijving()), row, 1);
			if (amtDebt.doubleValue() != 0d) {
				outFld = new OutputField(trf.transform(amtDebt), "80");
				grid.add(outFld, row++, 2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
			} else {
				outFld = new OutputField(trf.transform(amtCred), "80");
				grid.add(outFld, row++, 3, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
			}
		}
		addUIObject(grid);
		if (totalD.doubleValue() != totalC.doubleValue()) {
			addUIObject(new Paragraph(""));
			addUIObject(new Label(" *** JOURNAALPOST IS NIET SLUITEND! (SALDO = " + trf.transform(totalD.subtract(totalC)) + ")"));
			addUIObject(new Paragraph(""));
		}
	}
}