package nl.eadmin.ui.uiobjects.panels;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.databeans.GrootboekoverzichtDatabean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Hoofdverdichting;
import nl.eadmin.db.HoofdverdichtingManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.Verdichting;
import nl.eadmin.db.VerdichtingManager;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.db.impl.RekeningManager_Impl;
import nl.eadmin.db.impl.VerdichtingManager_Impl;
import nl.eadmin.helpers.HTMLHelper;
import nl.eadmin.helpers.PDFHelper;
import nl.eadmin.ui.uiobjects.MyRadioGroup;
import nl.eadmin.ui.uiobjects.referencefields.HoofdverdichtingReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.VerdichtingReferenceField;
import nl.eadmin.util.PrintWebExtension;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.BinaryAction;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.RadioButton;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.QueryFactory;

public class GrootboekOverzichtPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private boolean isMobileDevice;
	private Bedrijf bedrijf;
	private DBData dbd;
	private HoofdverdichtingReferenceField refHoofdverdichting;
	private VerdichtingReferenceField refVerdichting;
	private ODBTable tblRekeningen;
	private DateSelectionField selDateFrom, selDateTo;
	private MyRadioGroup rgSoort;
	private String overzichtTitle;
	private CheckBox cbExcludeEmpty;

	public GrootboekOverzichtPanel(CloseWindowAction closeAction) throws Exception {
		this.isMobileDevice = ESPSessionContext.isMobileDevice();
		this.bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		this.dbd = bedrijf.getDBData();
		addUIObject(createSelectionPanel());
		addUIObject(createFloatBar(closeAction));
	}

	private Panel createSelectionPanel() throws Exception {
		Calendar c = Calendar.getInstance();
		c.setLenient(true);
		selDateTo = new DateSelectionField("Datum t/m", c.getTime(), "dd-MM-yyyy");
		selDateTo.setMandatory(true);
		c.set(Calendar.MONTH, 0);
		c.set(Calendar.DAY_OF_MONTH, 1);
		selDateFrom = new DateSelectionField("Datum vanaf", c.getTime(), "dd-MM-yyyy");
		selDateFrom.setMandatory(true);
		refHoofdverdichting = new HoofdverdichtingReferenceField("Hoofdverdichting", bedrijf, HoofdverdichtingManagerFactory.getInstance());
		refVerdichting = new VerdichtingReferenceField("Verdichting", bedrijf, VerdichtingManagerFactory.getInstance(dbd));
		tblRekeningen = new ODBTable(RekeningManagerFactory.getInstance(dbd), ApplicationConstants.NUMBEROFTABLEROWS);
		tblRekeningen.setName("GrootboekOvzSel");
		tblRekeningen.setFilter(Rekening.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'", null);
		tblRekeningen.setColumnNames(new String[] { Rekening.REKENING_NR, Rekening.OMSCHRIJVING });
		tblRekeningen.setColumnLabels(new String[] { "Rekening", "Omschrijving" });
		tblRekeningen.setColumnSizes(new short[] { 50, 250 });
		tblRekeningen.setMultipleSelectable();
		tblRekeningen.setAddSelectAllRowsContextMenuItem(true);
		tblRekeningen.setAddUnSelectAllRowsContextMenuItem(true);
		tblRekeningen.addDownloadCSVContextAction("Rekeningen");
		tblRekeningen.addDownloadPDFContextAction("Rekeningen");
		tblRekeningen.reload();
		final Panel pnlRekeningen = new Panel();
		pnlRekeningen.addUIObject(new Label("Selecteer de gewenste rekening(en)"));
		pnlRekeningen.addUIObject(tblRekeningen);

		final RadioButton btn1 = new RadioButton("Een of meerdere grootboekrekeningen", "1").setChecked(true);
		final RadioButton btn2 = new RadioButton("Verdichting", "2");
		final RadioButton btn3 = new RadioButton("Hoofdverdichting", "3");
		rgSoort = new MyRadioGroup("Soort overzicht");
		rgSoort.add(btn1);
		rgSoort.add(btn2);
		rgSoort.add(btn3);
		EventListener rgListener = new EventListener() {
			private static final long serialVersionUID = 1L;

			public void event(UIObject object, String type) throws Exception {
				if (btn1.getChecked()) {
					pnlRekeningen.setHidden(false);
					refVerdichting.setHidden(true);
					refVerdichting.setMandatory(false);
					refHoofdverdichting.setHidden(true);
					refHoofdverdichting.setMandatory(false);
				} else if (btn2.getChecked()) {
					pnlRekeningen.setHidden(true);
					refVerdichting.setHidden(false);
					refVerdichting.setMandatory(true);
					refHoofdverdichting.setHidden(true);
					refHoofdverdichting.setMandatory(false);
				} else if (btn3.getChecked()) {
					pnlRekeningen.setHidden(true);
					refVerdichting.setHidden(true);
					refVerdichting.setMandatory(false);
					refHoofdverdichting.setHidden(false);
					refHoofdverdichting.setMandatory(true);
				}
			}
		};

		rgSoort.addOnChangeListener(rgListener);
		rgListener.event(rgSoort, "");
		cbExcludeEmpty = new CheckBox("Toon alleen rekeningen waarop geboekt is");

		FieldGroup fg = new FieldGroup();
		fg.forceBorder(false);
		fg.add(rgSoort);
		fg.add(refVerdichting);
		fg.add(refHoofdverdichting);
		fg.add(selDateFrom);
		fg.add(selDateTo);
		fg.add(cbExcludeEmpty);
		Panel panel = new Panel();
		panel.addUIObject(fg);
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(pnlRekeningen);
		return panel;
	}

	private FloatBar createFloatBar(CloseWindowAction closeAction) throws Exception {
		FloatBar fb = new FloatBar();
		fb.addAction(isMobileDevice ? new BinaryPrintAction() : new PrintAction());
		fb.addAction(closeAction);
		return fb;
	}

	/*
	 * private BinaryObject downloadHTML(DataObject object, int periode) throws
	 * Exception { GrootboekoverzichtDatabean grootboekoverzichtDatabean =
	 * getGrootboekoverzichtDatabean(); final String html =
	 * HTMLHelper.getGrootboekOverzichtHTML(grootboekoverzichtDatabean); byte[]
	 * bytes = html.getBytes(); String contentType = "text/html"; BinaryObject
	 * obj = new BinaryObject(bytes, contentType, object);
	 * //obj.setName("BTW_Aangifte"
	 * +(periode==5?"_":("_"+periode+(periode==1?"st"
	 * :"e")+"_kwartaal_"))+boekjaar+".html");
	 * obj.setDescription(obj.getName());
	 * obj.openSaveAsDialog(obj.getDescription()); return obj; }
	 */

	private BinaryObject downloadPDF(DataObject object) throws Exception {
		GrootboekoverzichtDatabean grootboekoverzichtDatabean = getGrootboekoverzichtDatabean();
		byte[] bytes = PDFHelper.getGrootboekOverzichtPDF(grootboekoverzichtDatabean);
		BinaryObject obj = new BinaryObject(bytes, BinaryObject.TYPE_PDF, object);
		// obj.setName("BTW_Aangifte"+(periode==5?"_":("_"+periode+(periode==1?"st":"e")+"_kwartaal_"))+boekjaar+".pdf");
		obj.noSaveAsDialog();
		return obj;
	}

	private Collection<?> getRekeningen() throws Exception {
		Collection<?> rekeningen = new ArrayList<Rekening>();
		RekeningManager rekMgr = RekeningManagerFactory.getInstance(dbd);
		VerdichtingManager vrdMgr = VerdichtingManagerFactory.getInstance(dbd);
		StringBuilder filter = new StringBuilder();

		switch (rgSoort.getSelectedValue()) {
		case "1":
			rekeningen = tblRekeningen.getSelectedObjects();
			tblRekeningen.unSelect();
			overzichtTitle = "Grootboekrekeningen-overzicht";
			break;
		case "2":
			Verdichting verdichting = (Verdichting) refVerdichting.getSelectedObject();
			filter.append(Rekening.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Rekening.VERDICHTING + "=" + verdichting.getId());
			rekeningen = rekMgr.getCollection(QueryFactory.create((RekeningManager_Impl) rekMgr, filter.toString()));
			overzichtTitle = "Grootboekrekeningen-overzicht voor verdichting '" + verdichting.getOmschrijving() + "'";
			break;
		case "3":
			Hoofdverdichting hfdVerdichting = (Hoofdverdichting) refHoofdverdichting.getSelectedObject();
			filter.append(Verdichting.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Verdichting.HOOFDVERDICHTING + "=" + hfdVerdichting.getId());
			Iterator<?> iter = vrdMgr.getCollection(QueryFactory.create((VerdichtingManager_Impl) vrdMgr, filter.toString())).iterator();
			if (iter.hasNext()) {
				boolean first = true;
				filter = new StringBuilder();
				filter.append(Rekening.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND (");
				while (iter.hasNext()) {
					if (first) {
						first = false;
					} else {
						filter.append(" OR ");
					}
					filter.append(Rekening.VERDICHTING + "=" + ((Verdichting) iter.next()).getId());
				}
				filter.append(")");
				rekeningen = rekMgr.getCollection(QueryFactory.create((RekeningManager_Impl) rekMgr, filter.toString()));
				overzichtTitle = "Grootboekrekeningen-overzicht voor hoofdverdichting '" + hfdVerdichting.getOmschrijving() + "'";
				break;
			}
		}
		return rekeningen;
	}

	private GrootboekoverzichtDatabean getGrootboekoverzichtDatabean() throws Exception {
		GrootboekoverzichtDatabean grootboekOverzicht = new GrootboekoverzichtDatabean(overzichtTitle, bedrijf, getRekeningen(), selDateFrom.getValueAsDate(), selDateTo.getValueAsDate(), cbExcludeEmpty.getValueAsBoolean());
		return grootboekOverzicht;
	}

	public DataObject openPrintDialog(DataObject object) throws Exception {
		GrootboekoverzichtDatabean grootboekoverzichtDatabean = getGrootboekoverzichtDatabean();
		String html = HTMLHelper.getGrootboekOverzichtHTML(grootboekoverzichtDatabean);
		String name = "Grootboekoverzicht_" + new Date(System.currentTimeMillis());
		object.addUIObject(new PrintWebExtension(object.getScreen(), name, html));
		return object;
	}

	private class BinaryPrintAction extends BinaryAction {
		private static final long serialVersionUID = 1L;

		public BinaryPrintAction() throws Exception {
			super("Print");
			setIcon("print.png");
		}

		@Override
		public BinaryObject getBinaryObject(DataObject object) throws Exception {
			// return downloadHTML(object,periode);//Werk voor printen zelf,
			// maar niet handig mbt opslaan of versturen op mobile devices;
			return downloadPDF(object);
		}
	}

	private class PrintAction extends Action {
		private static final long serialVersionUID = 1L;

		public PrintAction() {
			super("Print");
			setIcon("printer.png");
		}

		public boolean execute(DataObject object) throws Exception {
			openPrintDialog(object);
			return true;
		};
	}

}