package nl.eadmin.ui.uiobjects.panels;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataDataBean;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataDataBean;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.HtmlTemplate;
import nl.eadmin.db.HtmlTemplateManagerFactory;
import nl.eadmin.enums.HeaderDataStatusEnum;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.HtmlTemplateHelper;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.eadmin.ui.uiobjects.referencefields.HtmlTemplateReferenceField;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.OutputField;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.util.Transformer;
import nl.ibs.jsql.DBData;

public class FactureerPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private Field fldOms1, fldOms2;
	private DateSelectionField fldFactuurDatum;
	private NumberField fldBetTermijn;
	private Bedrijf bedrijf;
	private Dagboek dagboek;
	private Collection<?> details;
	private Debiteur debiteur;
	private DBData dbd;
	private BigDecimal ZERO = new BigDecimal(0.00).setScale(2);
	private DecimalToAmountTransformer decTrans = new DecimalToAmountTransformer(); 
	private HtmlTemplateReferenceField layout;

	public FactureerPanel(Bedrijf bedrijf, Dagboek dagboek, Collection<?> details) throws Exception {
		DetailData detailData = (DetailData) details.iterator().next();
		this.bedrijf = bedrijf;
		this.dagboek = dagboek;
		this.details = details;
		this.dbd = bedrijf.getDBData();
		DebiteurPK debKey = new DebiteurPK();
		debKey.setBedrijf(detailData.getBedrijf());
		debKey.setDebNr(detailData.getDcNummer());
		debiteur = DebiteurManagerFactory.getInstance(dbd).findByPrimaryKey(debKey);
		ESPGridLayout gridDebiteur = new ESPGridLayout();
		gridDebiteur.setColumnWidths(new short[] { 200, 300 });
		gridDebiteur.add(new Label("Debiteur"), 0, 0);
		gridDebiteur.add(new OutputField(debiteur.getNaam()), 0, 1);
		gridDebiteur.add(new OutputField(debiteur.getAdresRegel1()), 1, 1);
		gridDebiteur.add(new OutputField(debiteur.getAdresRegel2()), 2, 1);
		addUIObject(gridDebiteur);

		fldFactuurDatum = new DateSelectionField("", (Date)null, "dd-MM-yyyy");
		fldFactuurDatum.setDiscardLabel(true);
		fldFactuurDatum.setMandatory(true);
		fldFactuurDatum.setValue(new Date());
		fldBetTermijn = new NumberField("", 3, false);
		fldBetTermijn.setDiscardLabel(true);
		fldBetTermijn.setValue(debiteur.getBetaalTermijn() == 0 ? bedrijf.getInstellingen().getStdBetaalTermijn() : debiteur.getBetaalTermijn());
		fldOms1 = new Field("");
		fldOms1.setType(Field.TYPE_TEXT);
		fldOms1.setMaxLength(50);
		fldOms1.setLength(50);
		fldOms1.setDiscardLabel(true);
		fldOms2 = new Field("");
		fldOms2.setType(Field.TYPE_TEXT);
		fldOms2.setMaxLength(50);
		fldOms2.setLength(50);
		fldOms2.setDiscardLabel(true);
		layout = new HtmlTemplateReferenceField("Factuurlayout", bedrijf, HtmlTemplateManagerFactory.getInstance(dbd));
		layout.setDiscardLabel(true);
		layout.setSelectableOnly(true);
		layout.setMandatory(true);
		layout.setSelectedObject(HtmlTemplateHelper.getStdLayout(bedrijf));
		layout.hideIfOnlyOneOption(HtmlTemplate.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' ");
		ESPGridLayout gridMisc = new ESPGridLayout();
		gridMisc.setColumnWidths(new short[] { 200, 300 });
		gridMisc.add(new Label("Factuurdatum"), 0, 0);
		gridMisc.add(fldFactuurDatum, 0, 1);
		gridMisc.add(new Label("Betalingstermijn in dagen"), 1, 0);
		gridMisc.add(fldBetTermijn, 1, 1);
		gridMisc.add(new Paragraph(""), 2, 0);
		gridMisc.add(new Label("Alg. factuuromschrijving"), 3, 0);
		gridMisc.add(fldOms1, 3, 1);
		gridMisc.add(fldOms2, 4, 1);
		gridMisc.add(new Label("Factuurlayout"), 5, 0);
		gridMisc.add(layout, 5, 1);
		addUIObject(gridMisc);

		if (details.size() == 1) {
			DetailDataPanel detailPanel = new DetailDataPanel(bedrijf, dagboek, null, true);
			detailPanel.load((DetailData)details.toArray()[0]);
			addUIObject(detailPanel);
		} else {
			CollectionTable table = new CollectionTable(DetailData.class, details, ApplicationConstants.NUMBEROFTABLEROWS);
			table.setName("FactuurRegelTable");
			table.setColumnNames(new String[] { DetailData.AANTAL, DetailData.EENHEID, DetailData.OMSCHR1, DetailData.OMSCHR2, DetailData.OMSCHR3, DetailData.PRIJS, DetailData.BEDRAG_EXCL, DetailData.BEDRAG_BTW, DetailData.BEDRAG_INCL });
			table.setColumnLabels(new String[] { "Aantal", "Eenh.", "Omschrijving(1)", "Omschrijving(2)", "Omschrijving(3)", "Prijs", "Bedrag excl.", "Bedrag BTW", "Bedrag incl."});
			table.setColumnTypes(new String[] { Field.TYPE_DECIMAL, Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_DECIMAL, Field.TYPE_DECIMAL, Field.TYPE_DECIMAL, Field.TYPE_DECIMAL, Field.TYPE_DECIMAL });
			table.setColumnSizes(new short[] { 80, 80, 250, 100, 100, 80, 80, 80, 80 });
			table.setDisplayTransformers(new Transformer[] { null, null, null, null, null, decTrans, decTrans, decTrans, decTrans });
			table.setSelectable(false);
			table.reload();
			addUIObject(table);
			addUIObject(getTotaalPanel(details));
		}
	}

	private Panel getTotaalPanel(Collection<?> details) throws Exception {
		BigDecimal ZERO = new BigDecimal(0.00);
		BigDecimal totlExcl = ZERO;
		BigDecimal totlBtw = ZERO;
		BigDecimal totlIncl = ZERO;
		DetailData detail;
		Iterator<?> iter = details.iterator();
		while (iter.hasNext()) {
			detail = (DetailData) iter.next();
			totlExcl = totlExcl.add(detail.getBedragExcl());
			totlBtw = totlBtw.add(detail.getBedragBtw());
			totlIncl = totlIncl.add(detail.getBedragIncl());
		}
		Field fldExc = new Field("Totaal Excl. BTW", Field.TYPE_DECIMAL, "", (String)decTrans.transform(totlExcl), 15);
		Field fldBtw = new Field("Totaal BTW", Field.TYPE_DECIMAL, "", (String)decTrans.transform(totlBtw), 15);
		Field fldInc = new Field("Totaal Incl. BTW", Field.TYPE_DECIMAL, "", (String)decTrans.transform(totlIncl), 15);
		fldExc.setReadonly(true);
		fldBtw.setReadonly(true);
		fldInc.setReadonly(true);
		Panel panel = new Panel("");
		panel.addUIObject(fldExc);
		panel.addUIObject(fldBtw);
		panel.addUIObject(fldInc);
		return panel;
	}

	public HeaderData factureer() throws Exception {
		Date date = fldFactuurDatum.getValueAsDate();
		Calendar cal = Calendar.getInstance();
		cal.setLenient(true);
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, fldBetTermijn.getIntValue());
		Date vervalDatum = cal.getTime();
		int[] p = DateHelper.bepaalBoekperiode(bedrijf, date);
		int boekjaar = p[0];
		int periode = p[1];
		String boekstuk = GeneralHelper.genereerNieuwBoekstukNummer(dagboek, boekjaar, periode);
		if (boekstuk.length() == 0) {
			throw new UserMessageException("Er kon geen boekstuknummer worden vastgesteld. Mogelijk is er een onjuist masker bij het dagboek opgegeven.");
		}

		HeaderDataDataBean bean = new HeaderDataDataBean();
		bean.setBedrijf(bedrijf.getBedrijfscode());
		bean.setBoekdatum(date);
		bean.setBoekjaar(boekjaar);
		bean.setBoekperiode(periode);
		bean.setBoekstuk(boekstuk);
		bean.setDagboekId(dagboek.getId());
		bean.setDagboekSoort(dagboek.getSoort());
		bean.setDC("D");
		bean.setDcNummer(debiteur.getDebNr());
		bean.setFactuurdatum(date);
		bean.setFactuurNummer(boekstuk);
		bean.setFactuurLayout(layout.getValue());
		bean.setOmschr1(fldOms1.getValue());
		bean.setOmschr2(fldOms2.getValue());
		bean.setReferentieNummer("");
		bean.setStatus(HeaderDataStatusEnum.VERKOOP_CLOSED);
		bean.setVervaldatum(vervalDatum);
		HeaderData headerData = HeaderDataManagerFactory.getInstance(dbd).create(bean);

		int boekstukRegel = 0;
		BigDecimal totlInc = ZERO;
		Iterator<?> iter = details.iterator();
		DetailData detailData = null;
		DetailDataDataBean detailBean = null;
		while (iter.hasNext()) {
			detailData = (DetailData) iter.next();
			detailBean = detailData.getDetailDataDataBean();
			detailBean.setFactuurNummer(boekstuk);
			detailBean.setBoekstuk(boekstuk);
			detailBean.setBoekstukRegel(++boekstukRegel);
			detailData.update(detailBean);
			if (headerData.getDC().equalsIgnoreCase(detailData.getDC())) {
				totlInc = totlInc.subtract(detailData.getBedragIncl());
			} else {
				totlInc = totlInc.add(detailData.getBedragIncl());
			}
		}
		headerData.setTotaalGeboekt(totlInc);
		headerData.setTotaalOpenstaand(totlInc);
		headerData.recalculate();
		return headerData;
	}

	public HtmlTemplate getHtmlTemplate() throws Exception {
		return (HtmlTemplate)layout.getSelectedObject();
	}
}