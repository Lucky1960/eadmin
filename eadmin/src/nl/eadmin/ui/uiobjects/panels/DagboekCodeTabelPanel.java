package nl.eadmin.ui.uiobjects.panels;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.databeans.VrijeRubriekDefinitie;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.CodeTabelManager;
import nl.eadmin.db.CodeTabelManagerFactory;
import nl.eadmin.db.CodeTabelPK;
import nl.eadmin.db.Dagboek;
import nl.eadmin.enums.VrijeRubriekSoortEnum;
import nl.eadmin.helpers.VrijeRubriekHelper;
import nl.eadmin.ui.uiobjects.referencefields.CodeTabelReferenceField;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;

public class DagboekCodeTabelPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private static int numberOfFields = 5;
	private String SEPARATOR = ApplicationConstants.SEPARATOR;
	private ComboBox[] cbUse = new ComboBox[numberOfFields];
	private Field[] fldPrompt = new Field[numberOfFields];
	private Label[] stdPrompt = new Label[numberOfFields];
	private CodeTabelReferenceField[] refTabel = new CodeTabelReferenceField[numberOfFields];
	private DataTypeListener[] dataTypeListener = new DataTypeListener[numberOfFields];
	private Label lblUse = new Label("Soort rubriek");
	private Label lblPrompt = new Label("Schermaanduiding");
	private Label lblTabel = new Label("Tabel");
	private Bedrijf bedrijf;
	private CodeTabelManager manager;
	private CodeTabelPK key;

	public DagboekCodeTabelPanel(Bedrijf bedrijf) throws Exception {
		this.bedrijf = bedrijf;
		this.manager = CodeTabelManagerFactory.getInstance(bedrijf.getDBData());
		this.key = new CodeTabelPK();
		this.key.setBedrijf(bedrijf.getBedrijfscode());
		setLabel("Vrije rubrieken");
		ESPGridLayout grid = new ESPGridLayout();
		grid.setColumnWidths(new short[] { 250, 100, 400 });
		grid.add(lblPrompt, 0, 0);
		grid.add(lblUse, 0, 1);
		grid.add(lblTabel, 0, 2);
		for (int i = 0; i < numberOfFields; i++) {
			int row = i + 1;
			setUse(i);
			setPrompt(i);
			setRefTabel(i);
			grid.add(stdPrompt[i], row, 0);
			grid.add(fldPrompt[i], row, 0);
			grid.add(cbUse[i], row, 1);
			grid.add(refTabel[i], row, 2);
			dataTypeListener[i].event(cbUse[i], "");
		}
		addUIObject(grid);
	}

	private void setUse(int x) throws Exception {
		dataTypeListener[x] = new DataTypeListener(x);
		cbUse[x] = VrijeRubriekSoortEnum.createComboBox("", VrijeRubriekSoortEnum.getCollection(), false);
		cbUse[x].setDiscardLabel(true);
		cbUse[x].setSortThreshold(999);
		cbUse[x].addOnChangeListener(dataTypeListener[x]);
	}

	private void setPrompt(int x) throws Exception {
		stdPrompt[x] = new Label("Vrije rubriek " + (x + 1) + ".");
		fldPrompt[x] = new Field("Schermaanduiding");
		fldPrompt[x].setDiscardLabel(true);
		fldPrompt[x].setLength(30);
		fldPrompt[x].setMaxLength(30);
	}

	private void setRefTabel(int x) throws Exception {
		refTabel[x] = new CodeTabelReferenceField("Tabel", bedrijf, manager);
		refTabel[x].setDiscardLabel(true);
		refTabel[x].setSelectableOnly(true);
	}

	public void loadFrom(Dagboek dagboek) throws Exception {
		VrijeRubriekDefinitie[] definitie = VrijeRubriekHelper.getVrijeRubriekDefinities(dagboek);
		for (int i = 0; i < definitie.length; i++) {
			cbUse[i].setSelectedOptionValue(definitie[i].getDataType());
			fldPrompt[i].setValue(definitie[i].getPrompt());
			key.setTabelNaam(definitie[i].getTabelNaam());
			refTabel[i].setValue(manager.findOrCreate(key));
		}
	}

	public void saveTo(Dagboek dagboek) throws Exception {
		String[] dbField = new String[numberOfFields];
		String use;
		for (int i = 0; i < numberOfFields; i++) {
			use = cbUse[i].getSelectedOptionValue();
			switch (use) {
			case VrijeRubriekSoortEnum.NOT_IN_USE:
				dbField[i] = use;
				break;
			case VrijeRubriekSoortEnum.TABELWAARDE:
				dbField[i] = use + SEPARATOR + fldPrompt[i].getValue() + SEPARATOR + refTabel[i].getValue();
				break;
			default:
				dbField[i] = use + SEPARATOR + fldPrompt[i].getValue();
				break;
			}
		}
		dagboek.setVrijeRub1(dbField[0]);
		dagboek.setVrijeRub2(dbField[1]);
		dagboek.setVrijeRub3(dbField[2]);
		dagboek.setVrijeRub4(dbField[3]);
		dagboek.setVrijeRub5(dbField[4]);
	}

	private class DataTypeListener implements EventListener {
		private static final long serialVersionUID = 1L;
		private int index;

		public DataTypeListener(int x) throws Exception {
			this.index = x;
		}

		public void event(UIObject object, String type) throws Exception {
			switch (((ComboBox) object).getSelectedOptionValue()) {
			case VrijeRubriekSoortEnum.NOT_IN_USE:
				stdPrompt[index].setHidden(false);
				fldPrompt[index].setHidden(true);
				fldPrompt[index].setMandatory(false);
				refTabel[index].setHidden(true);
				refTabel[index].setMandatory(false);
				break;
			case VrijeRubriekSoortEnum.TABELWAARDE:
				stdPrompt[index].setHidden(true);
				fldPrompt[index].setHidden(false);
				fldPrompt[index].setMandatory(true);
				refTabel[index].setHidden(false);
				refTabel[index].setMandatory(true);
				break;
			default:
				stdPrompt[index].setHidden(true);
				fldPrompt[index].setHidden(false);
				fldPrompt[index].setMandatory(true);
				refTabel[index].setHidden(true);
				refTabel[index].setMandatory(false);
				break;
			}
		};
	}
}