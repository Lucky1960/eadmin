package nl.eadmin.ui.uiobjects.panels;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.eadmin.ui.uiobjects.referencefields.RekeningReferenceField;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UserMessageException;

public class RekeningSpeciaalPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private Bedrijf bedrijf;
	private Label lblRekResltVrgJr = new Label("Rekening winst-saldo");
	private RekeningReferenceField fldRekResltVrgJr;

	public RekeningSpeciaalPanel(Bedrijf bedrijf, int labelWidth) throws Exception {
		this.bedrijf = bedrijf;
		this.fldRekResltVrgJr = new RekeningReferenceField("", bedrijf, RekeningManagerFactory.getInstance(bedrijf.getDBData()));
		this.fldRekResltVrgJr.setDiscardLabel(true);
		setLabel("Speciale rekeningnummers");
		setForceBorder(true);
		ESPGridLayout grid1 = new ESPGridLayout();
		grid1.setColumnWidths(new short[] { (short) labelWidth });
		grid1.setPaddingTop(0);
		grid1.setPaddingBottom(0);
		grid1.add(lblRekResltVrgJr, 0, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
		grid1.add(fldRekResltVrgJr, 0, 1, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE);
		ESPGridLayout mainGrid = new ESPGridLayout();
		mainGrid.add(grid1, 0, 0);
		FloatBar fb = new FloatBar();
		fb.addAction(new Action("Opslaan") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				save();
				return true;
			}
		}.setIcon("disk.png").setAlertMessage("Gegevens opgeslagen"));
		addUIObject(mainGrid);
		addUIObject(fb);
		load();
	}

	public void load() throws Exception {
		fldRekResltVrgJr.setValue(bedrijf.getDagboek("SYS1").getRekening());
	}

	public void save() throws Exception {
		validate();
		bedrijf.getDagboek("SYS1").setRekening(fldRekResltVrgJr.getValue());
	}

	private void validate() throws Exception {
		Rekening rekNr = (Rekening) fldRekResltVrgJr.getSelectedObject();
		if (rekNr != null && !rekNr.getRekeningSoort().equals(RekeningSoortEnum.BALANS)) {
			fldRekResltVrgJr.setInvalidTag();
			throw new UserMessageException("Dit is geen Balans-rekening!");
		}
	}
}