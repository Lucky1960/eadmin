package nl.eadmin.ui.uiobjects.panels;

import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.BinaryAction;
import nl.ibs.esp.uiobjects.Panel;

public abstract class PrintablePanel extends Panel {
	private static final long serialVersionUID = 1L;

	public Action getDownloadAction() throws Exception {
		return new BinaryDownloadAction();
	}

	public Action getPrintAction() throws Exception {
		return new PrintAction();
	}

	protected BinaryObject downloadPDF(DataObject object) throws Exception {
		return null;
	}

	public DataObject openPrintDialog(DataObject object) throws Exception {
		return object;
	}

	private class BinaryDownloadAction extends BinaryAction {
		private static final long serialVersionUID = 1L;

		public BinaryDownloadAction() throws Exception {
			super("Opslaan als PDF");
			setIcon("page_white_acrobat.png");
		}

		public BinaryObject getBinaryObject(DataObject object) throws Exception {
			return downloadPDF(object);
		}
	}

	private class PrintAction extends Action {
		private static final long serialVersionUID = 1L;

		public PrintAction() {
			super("Print");
			setIcon("printer.png");
		}

		public boolean execute(DataObject object) throws Exception {
			openPrintDialog(object);
			return true;
		};
	}

}