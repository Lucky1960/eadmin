package nl.eadmin.ui.uiobjects.panels;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.databeans.DebCredKaartMutatieDatabean;
import nl.eadmin.databeans.DebCredLijstDataBean;
import nl.eadmin.databeans.DebCredOverzichtDataBean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.HTMLHelper;
import nl.eadmin.helpers.PDFHelper;
import nl.eadmin.selection.DebCredLijstSelection;
import nl.eadmin.selection.SelectionChangeListener;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.eadmin.ui.uiobjects.referencefields.CrediteurReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.DebiteurReferenceField;
import nl.eadmin.ui.uiobjects.tables.HeaderDataKeyedTransformer;
import nl.eadmin.ui.uiobjects.windows.FacturenLijstWindow;
import nl.eadmin.util.PrintWebExtension;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.CommonTable.SearchPanel;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.util.Transformer;
import nl.ibs.jsql.DBData;

public class DebCredLijstPanel extends PrintablePanel implements SelectionChangeListener {
	private static final long serialVersionUID = 1L;
	private DebCredOverzichtDataBean debCredOverzichtDataBean;
	private DebCredLijstSelection selection;
	private static DecimalToAmountTransformer trf = new DecimalToAmountTransformer();
	private static final String[] NAMES = new String[] { DebCredLijstDataBean.DCNR, DebCredLijstDataBean.DCNAAM, DebCredLijstDataBean.BEDRAG_D, DebCredLijstDataBean.BEDRAG_C,
			DebCredLijstDataBean.SALDO };
	private static final String[] LABELS = new String[] { "Relatie", "Naam", "Bedrag D", "Bedrag C", "Saldo" };
	private static final String[] TYPES = new String[] { Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_DECIMAL, Field.TYPE_DECIMAL, Field.TYPE_DECIMAL };
	private static final short[] SIZES = new short[] { 100, 300, 80, 80, 80 };
	private static final Transformer[] TRANS = new Transformer[] { null, null, trf, trf, trf };
	private Bedrijf bedrijf;
	private DBData dbd;
	private String title, dc;
	private CollectionTable table;

	public DebCredLijstPanel(Bedrijf bedrijf, String dc, Action close) throws Exception {
		this.bedrijf = bedrijf;
		this.dc = dc;
		this.dbd = bedrijf.getDBData();
		this.title = (dc.equals("D") ? "Debiteuren " : "Crediteuren ") + new Date(System.currentTimeMillis());
		this.selection = new DebCredLijstSelection(bedrijf);
		this.selection.addSelectionChangeListener(this);
		this.debCredOverzichtDataBean = new DebCredOverzichtDataBean(bedrijf, dc, title);
		updateTableAfterSelectionChange();
		FloatBar fb = new FloatBar();
		fb.addAction(getPrintAction());
		fb.addAction(getDownloadAction());
		fb.addAction(close);
		SearchPanel searchPanel = table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true);
		searchPanel.setDefaultInputTransformer(new HeaderDataKeyedTransformer());
		addUIObject(new Paragraph(""));
		addUIObject(new Label("Klik op ��n van onderstaande relaties om de bijbehorende facturen te tonen"));
		addUIObject(new Paragraph(""));
		addUIObject(searchPanel);
		addUIObject(selection.getPanel());
		addUIObject(table);
		addUIObject(fb);
	}

	protected BinaryObject downloadPDF(DataObject object) throws Exception {
		byte[] bytes = PDFHelper.getDebiteurenOverzichtPDF(debCredOverzichtDataBean);
		BinaryObject obj = new BinaryObject(bytes, BinaryObject.TYPE_PDF, object);
		obj.setName(title + ".pdf");
		obj.noSaveAsDialog();
		return obj;
	}

	public DataObject openPrintDialog(DataObject object) throws Exception {
		String html = HTMLHelper.getDebiteurenOverzichtHTML(debCredOverzichtDataBean);
		object.addUIObject(new PrintWebExtension(object.getScreen(), title, html));
		return object;
	}

	public void updateTableAfterSelectionChange() throws Exception {
		if (table == null) {
			table = new CollectionTable(DebCredLijstDataBean.class, debCredOverzichtDataBean.getDebCredBeans(selection), ApplicationConstants.NUMBEROFTABLEROWS);
			table.setName("DebCredLijst");
			table.setColumnNames(NAMES);
			table.setColumnLabels(LABELS);
			table.setColumnSizes(SIZES);
			table.setColumnTypes(TYPES);
			table.setDisplayTransformers(TRANS);
			table.setSortable(true);
			table.setSelectable(false);
			table.addDownloadCSVContextAction(title);
			table.addDownloadPDFContextAction(title);
			Map<String, InputComponent> myInputComponents = new HashMap<String, InputComponent>();
			ODBQueryReferenceField debCredRef = dc.equals("D") ? new DebiteurReferenceField("", bedrijf, DebiteurManagerFactory.getInstance(dbd)) : new CrediteurReferenceField("", bedrijf,
				CrediteurManagerFactory.getInstance(dbd));
			myInputComponents.put(DebCredLijstDataBean.DCNR, debCredRef);
			myInputComponents.put(DebCredLijstDataBean.DCNAAM, new Field(""));
			myInputComponents.put(DebCredLijstDataBean.BEDRAG_D, new DecimalField(DebCredLijstDataBean.BEDRAG_D, 15, 2, false));
			myInputComponents.put(DebCredLijstDataBean.BEDRAG_C, new DecimalField(DebCredLijstDataBean.BEDRAG_C, 15, 2, false));
			myInputComponents.put(DebCredLijstDataBean.SALDO, new DecimalField(DebCredKaartMutatieDatabean._CREDIT, 15, 2, false));
			table.setInputComponents(myInputComponents);
			Action showAction = new Action("OK") {
				private static final long serialVersionUID = 1L;

				public boolean execute(DataObject object) throws Exception {
					DebCredLijstDataBean bean = (DebCredLijstDataBean) GeneralHelper.getSelectedItem(table, object);
					object.addUIObject(new FacturenLijstWindow(bedrijf, dc, bean.getDcNr(), selection));
					return true;
				}
			};
			table.setRowAction(showAction);
		} else {
			table.setNewCollection(DebCredLijstDataBean.class, debCredOverzichtDataBean.getDebCredBeans(selection), ApplicationConstants.NUMBEROFTABLEROWS);
		}
		table.reload();
	}
}