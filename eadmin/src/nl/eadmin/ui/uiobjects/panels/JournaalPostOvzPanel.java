package nl.eadmin.ui.uiobjects.panels;

import java.sql.Date;

import nl.eadmin.SessionKeys;
import nl.eadmin.databeans.GrootboekoverzichtDatabean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.helpers.HTMLHelper;
import nl.eadmin.helpers.PDFHelper;
import nl.eadmin.ui.datareaders.JournaalDataReader;
import nl.eadmin.ui.uiobjects.tables.JournaalTable;
import nl.eadmin.util.PrintWebExtension;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.BinaryAction;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Panel;

public class JournaalPostOvzPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private boolean isMobileDevice;
	private int boekjaar;
	private Bedrijf bedrijf;
	private String overzichtTitle;
	private JournaalTable table;

	public JournaalPostOvzPanel(CloseWindowAction closeAction) throws Exception {
		this.isMobileDevice = ESPSessionContext.isMobileDevice();
		this.bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		this.boekjaar = ((Integer) ESPSessionContext.getSessionAttribute(SessionKeys.BOEKJAAR)).intValue();
		table = new JournaalTable(new JournaalDataReader(), bedrijf, boekjaar);
		table.setSelectable(false);
		table.reload();
		addUIObject(table.getSearchPanel());
		addUIObject(table);
		addUIObject(createFloatBar(closeAction));
	}

	private FloatBar createFloatBar(CloseWindowAction closeAction) throws Exception {
		FloatBar fb = new FloatBar();
		fb.addAction(isMobileDevice ? new BinaryPrintAction() : new PrintAction());
		fb.addAction(closeAction);
		return fb;
	}

	/*
	 * private BinaryObject downloadHTML(DataObject object, int periode) throws
	 * Exception { GrootboekoverzichtDatabean grootboekoverzichtDatabean =
	 * getGrootboekoverzichtDatabean(); final String html =
	 * HTMLHelper.getGrootboekOverzichtHTML(grootboekoverzichtDatabean); byte[]
	 * bytes = html.getBytes(); String contentType = "text/html"; BinaryObject
	 * obj = new BinaryObject(bytes, contentType, object);
	 * //obj.setName("BTW_Aangifte"
	 * +(periode==5?"_":("_"+periode+(periode==1?"st"
	 * :"e")+"_kwartaal_"))+boekjaar+".html");
	 * obj.setDescription(obj.getName());
	 * obj.openSaveAsDialog(obj.getDescription()); return obj; }
	 */

	private BinaryObject downloadPDF(DataObject object) throws Exception {
		GrootboekoverzichtDatabean grootboekoverzichtDatabean = getGrootboekoverzichtDatabean();
		byte[] bytes = PDFHelper.getGrootboekOverzichtPDF(grootboekoverzichtDatabean);
		BinaryObject obj = new BinaryObject(bytes, BinaryObject.TYPE_PDF, object);
		obj.setName(overzichtTitle+".pdf");
		obj.noSaveAsDialog();
		return obj;
	}

	private GrootboekoverzichtDatabean getGrootboekoverzichtDatabean() throws Exception {
//		GrootboekoverzichtDatabean grootboekOverzicht = new GrootboekoverzichtDatabean(overzichtTitle, bedrijf, getJournaalPostBeans(), selDateFrom.getValueAsDate(), selDateTo.getValueAsDate());
		return null;
	}

	public DataObject openPrintDialog(DataObject object) throws Exception {
		GrootboekoverzichtDatabean grootboekoverzichtDatabean = getGrootboekoverzichtDatabean();
		String html = HTMLHelper.getGrootboekOverzichtHTML(grootboekoverzichtDatabean);
		String name = "Grootboekoverzicht_" + new Date(System.currentTimeMillis());
		object.addUIObject(new PrintWebExtension(object.getScreen(), name, html));
		return object;
	}

	private class BinaryPrintAction extends BinaryAction {
		private static final long serialVersionUID = 1L;

		public BinaryPrintAction() throws Exception {
			super("Print");
			setIcon("print.png");
		}

		@Override
		public BinaryObject getBinaryObject(DataObject object) throws Exception {
			// return downloadHTML(object,periode);//Werk voor printen zelf,
			// maar niet handig mbt opslaan of versturen op mobile devices;
			return downloadPDF(object);
		}
	}

	private class PrintAction extends Action {
		private static final long serialVersionUID = 1L;

		public PrintAction() {
			super("Print");
			setIcon("printer.png");
		}

		public boolean execute(DataObject object) throws Exception {
			openPrintDialog(object);
			return true;
		};
	}

}