package nl.eadmin.ui.uiobjects.panels;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataDataBean;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.DetailDataPK;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataDataBean;
import nl.eadmin.db.HeaderDataManager;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.HeaderDataPK;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.helpers.DateHelper;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.PeriodeHelper;
import nl.eadmin.ui.uiobjects.ComboBtwCodes;
import nl.eadmin.ui.uiobjects.ComboDC;
import nl.eadmin.ui.uiobjects.referencefields.CrediteurReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.DebiteurReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.FactuurReferenceField2;
import nl.eadmin.ui.uiobjects.referencefields.RekeningReferenceField;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.exception.FinderException;

public class HeaderDataPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private HeaderData headerData;
	private HeaderDataPK headerKey;
	private HeaderDataDataBean headerDataBean;
	private HeaderDataManager hdrMgr;
	private Bedrijf bedrijf;
	private Dagboek dagboek;
	private DBData dbd;
//	private ESPGridLayout gridMain  = new ESPGridLayout();
	private Label lblBoekstuk = new Label("Boekstuknr.");
	private Label lblBoekdatum = new Label("Boekdatum");
	private Label lblDc = new Label("");
	private Label lblDcNummer = new Label("Relatienr.");
	private Label lblRekening = new Label("GrootboekRek.");
	private Label lblFactuur = new Label("Factuurnr.");
	private Label lblReferentie = new Label("Referentienr.");
	private Label lblOmschr = new Label("Omschrijving");
	private Label lblSaldoBegin = new Label("Beginsaldo");
	private Label lblSaldoEind = new Label("Eindsaldo");
//	private Label lblTotlExcl = new Label("Totaal Excl.");
//	private Label lblTotlBtw = new Label("Totaal BTW");
	private Label lblTotlIncl = new Label("Totaal Incl.");
	private Label lblTotlGbkt = new Label("Boekingsbedrag");
	private Label lblDetlAmt = new Label("Bedrag/Prijs");
	private Label lblDetlExcl = new Label("Bedrag Excl.");
	private Label lblDetlBtw = new Label("Bedrag BTW");
	private Label lblDetlIncl = new Label("Bedrag Incl.");
	private Field fldBoekstuk = new Field("Boekstuk", Field.TYPE_TEXT, "", "", 15);
	private Field fldFactuur = new Field("Factuurnr.", Field.TYPE_TEXT, "", "", 15);
	private Field fldReferentie = new Field("Referentienr.", Field.TYPE_TEXT, "", "", 15);
	private Field fldOmschr1 = new Field("Omschrijving", Field.TYPE_TEXT, "", "", 50);
	private Field fldOmschr2 = new Field("Omschrijving", Field.TYPE_TEXT, "", "", 50);
	private DecimalField fldTotlExcl = new DecimalField("Totaal Excl.", 15, 2, true);
	private DecimalField fldTotlBtw = new DecimalField("Totaal BTW", 15, 2, true);
	private DecimalField fldTotlIncl = new DecimalField("Totaal Incl.", 15, 2, true);
	private DecimalField fldTotlGbkt = new DecimalField("Boekingsbedrag", 15, 2, true);
	private DecimalField fldDetlAmt = new DecimalField("Bedrag/Prijs", 15, 2, true);
	private DecimalField fldDetlExcl = new DecimalField("Bedrag Excl.", 15, 2, true);
	private DecimalField fldDetlBtw = new DecimalField("Bedrag BTW", 15, 2, true);
	private DecimalField fldDetlIncl = new DecimalField("Bedrag Incl.", 15, 2, true);
	private DecimalField fldSaldoBegin = new DecimalField("Beginsaldo", 15, 2, true);
	private DecimalField fldSaldoEind = new DecimalField("Eindsaldo", 15, 2, true);
	private DateSelectionField fldBoekdatum = new DateSelectionField("", (Date)null, "dd-MM-yyyy");
	private DateSelectionField fldFactuurdatum = new DateSelectionField("", (Date)null, "dd-MM-yyyy");
	private DateSelectionField fldVervaldatum = new DateSelectionField("", (Date)null, "dd-MM-yyyy");
	private BoekdatumListener boekdatumListener = new BoekdatumListener(); 
	private DebiteurReferenceField fldDebiteur;
	private CrediteurReferenceField fldCrediteur;
	private RekeningReferenceField fldRekening;
	private FactuurReferenceField2 fldDebiteurFact, fldCrediteurFact;
	private ComboBtwCodes fldBtwCode;
	private ComboDC fldDc;
	private int tmpBoekjaar, tmpBoekperiode;
	private boolean manualBoekstuk, suppressEvent;
	private RekeningListener rekeningListener = new RekeningListener();
	private DcNummerListener dcNummerListener = new DcNummerListener();
	private Panel boekstukPanel = new Panel(""); 
	private Panel miscPanel = new Panel(""); 
	private VrijeRubriekPanel vrijeRubriekPanel;
	private BigDecimal ZERO = ApplicationConstants.ZERO;

	public HeaderDataPanel(Bedrijf bedrijf, Dagboek dagboek) throws Exception {
		this.bedrijf = bedrijf;
		this.dagboek = dagboek;
		this.dbd = bedrijf.getDBData();
		this.vrijeRubriekPanel = new VrijeRubriekPanel(dagboek);
		this.manualBoekstuk = (dagboek.getBoekstukMask().trim().length() == 0);
		this.hdrMgr = HeaderDataManagerFactory.getInstance(dbd);
		this.fldBoekdatum.addOnChangeListener(boekdatumListener);
		this.headerKey = new HeaderDataPK();
		this.headerKey.setBedrijf(dagboek.getBedrijf());
		this.headerKey.setDagboekId(dagboek.getId());
		fldBoekstuk.setDiscardLabel(true);
		fldBoekstuk.setMandatory(true);
		fldBoekdatum.setDiscardLabel(true);
		fldBoekdatum.setMandatory(true);
		fldDc = new ComboDC(dagboek, true);
		fldDc.setDiscardLabel(true);
		fldBtwCode = new ComboBtwCodes(bedrijf);
		fldBtwCode.setDiscardLabel(true);
		fldFactuur.setDiscardLabel(true);
		fldFactuurdatum.setDiscardLabel(true);
		fldOmschr1.setDiscardLabel(true);
		fldOmschr2.setDiscardLabel(true);
		fldReferentie.setDiscardLabel(true);
		fldVervaldatum.setDiscardLabel(true);
		fldSaldoBegin.setDiscardLabel(true);
		fldSaldoEind.setDiscardLabel(true);
		fldTotlBtw.setDiscardLabel(true);
		fldTotlExcl.setDiscardLabel(true);
		fldTotlIncl.setDiscardLabel(true);
		fldTotlGbkt.setDiscardLabel(true);
		fldDetlAmt.setDiscardLabel(true);
		fldDetlAmt.setMandatory(true);
		fldDetlExcl.setDiscardLabel(true);
		fldDetlExcl.setReadonly(true);
		fldDetlBtw.setDiscardLabel(true);
		fldDetlBtw.setReadonly(true);
		fldDetlIncl.setDiscardLabel(true);
		fldDetlIncl.setReadonly(true);
		fldDebiteur = new DebiteurReferenceField("Debiteur", bedrijf, DebiteurManagerFactory.getInstance(dbd));
		fldDebiteur.setDiscardLabel(true);
		fldDebiteur.setLength(10);
		fldDebiteur.setDescriptionLength(50);
		fldDebiteur.setDiscardLabel(true);
		fldDebiteur.setHidden(true);
		fldDebiteur.addOnChangeListener(dcNummerListener);
		fldCrediteur = new CrediteurReferenceField("Crediteur", bedrijf, CrediteurManagerFactory.getInstance(dbd));
		fldCrediteur.setDiscardLabel(true);
		fldCrediteur.setDescriptionLength(50);
		fldCrediteur.setDiscardLabel(true);
		fldCrediteur.setHidden(true);
		fldCrediteur.addOnChangeListener(dcNummerListener);
		fldRekening = new RekeningReferenceField("Grootboekrekening", bedrijf, RekeningManagerFactory.getInstance(dbd), false);
		fldRekening.setDiscardLabel(true);
		fldRekening.setLength(10);
		fldRekening.setDescriptionLength(30);
		fldRekening.addOnChangeListener(rekeningListener);
		fldRekening.setMandatory(true);
		fldDebiteurFact = new FactuurReferenceField2("Factuurnr.", bedrijf, JournaalManagerFactory.getInstance(dbd));
		fldDebiteurFact.setDiscardLabel(true);
		fldDebiteurFact.setLength(10);
		fldDebiteurFact.setDescriptionLength(30);
		fldDebiteurFact.setHidden(true);
		fldCrediteurFact = new FactuurReferenceField2("Factuurnr.", bedrijf, JournaalManagerFactory.getInstance(dbd));
		fldCrediteurFact.setDiscardLabel(true);
		fldCrediteurFact.setLength(10);
		fldCrediteurFact.setDescriptionLength(30);
		fldCrediteurFact.setHidden(true);
		lblDcNummer.setHidden(true);
		lblFactuur.setHidden(true);

		addUIObject(boekstukPanel);
		addUIObject(miscPanel);
		addUIObject(vrijeRubriekPanel);
	}

	private void setLayout() throws Exception {
		ESPGridLayout gridBoekstuk = new ESPGridLayout();
		gridBoekstuk.setColumnWidths(new short[] { ApplicationConstants.STD_LABELWIDTH, 300 });
		gridBoekstuk.add(lblBoekstuk, 0, 0);
		gridBoekstuk.add(fldBoekstuk, 0, 1);
		gridBoekstuk.add(lblBoekdatum, 1, 0);
		gridBoekstuk.add(fldBoekdatum, 1, 1);
		boekstukPanel.removeAllUIObjects();
		boekstukPanel.addUIObject(gridBoekstuk);
		fldSaldoBegin.setMandatory(false);
		fldSaldoEind.setMandatory(false);

		ESPGridLayout gridMisc  = new ESPGridLayout();
		gridMisc.setColumnWidths(new short[] { ApplicationConstants.STD_LABELWIDTH, 300, 90, 90 });
		switch (dagboek.getSoort()) {
		case DagboekSoortEnum.BANK:
			if (headerData == null || headerData.getBeginsaldo().doubleValue() != 0d) {
				fldSaldoBegin.setMandatory(true);
				fldSaldoEind.setMandatory(true);
				gridMisc.add(lblSaldoBegin, 0, 0);
				gridMisc.add(fldSaldoBegin, 0, 1);
				gridMisc.add(lblSaldoEind, 1, 0);
				gridMisc.add(fldSaldoEind, 1, 1);
			} else {
				fldTotlGbkt.setReadonly(true);
				fldOmschr1.setReadonly(true);
				lblDc.setLabel("Af/Bij");
				gridMisc.add(lblTotlGbkt, 0, 0);
				gridMisc.add(fldTotlGbkt, 0, 1);
				gridMisc.add(lblDc, 1, 0);
				gridMisc.add(fldDc, 1, 1);
			}
			gridMisc.add(lblOmschr, 2, 0);
			gridMisc.add(fldOmschr1, 2, 1);
			gridMisc.add(fldOmschr2, 3, 1);
			break;
		case DagboekSoortEnum.KAS:
			fldDetlAmt.addOnChangeListener(new RecalcListener());
			fldBtwCode.addOnChangeListener(new RecalcListener());
			lblDc.setLabel("Ontvangst/Uitgave");
			gridMisc.add(lblDc, 3, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_BOTH);
			gridMisc.add(fldDc, 3, 1);
			gridMisc.add(lblDetlAmt, 4, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_BOTH);
			gridMisc.add(fldDetlAmt, 4, 1);
			gridMisc.add(fldBtwCode, 4, 2);
			gridMisc.add(lblDetlExcl, 5, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_BOTH);
			gridMisc.add(fldDetlExcl, 5, 1);
			gridMisc.add(lblDetlBtw, 6, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_BOTH);
			gridMisc.add(fldDetlBtw, 6, 1);
			gridMisc.add(lblDetlIncl, 7, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_BOTH);
			gridMisc.add(fldDetlIncl, 7, 1);
			gridMisc.add(lblOmschr, 8, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_BOTH);
			gridMisc.add(fldOmschr1, 8, 1, 1, 3, ESPGridLayoutConstraints.GRID_FILL_BOTH);
			gridMisc.add(fldOmschr2, 9, 1, 1, 3, ESPGridLayoutConstraints.GRID_FILL_BOTH);
			gridMisc.add(lblRekening, 10, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMisc.add(fldRekening, 10, 1, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMisc.add(lblDcNummer, 11, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMisc.add(fldDebiteur, 11, 1, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMisc.add(fldCrediteur, 11, 1, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMisc.add(lblFactuur, 12, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMisc.add(fldDebiteurFact, 12, 1, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMisc.add(fldCrediteurFact, 12, 1, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			break;
		case DagboekSoortEnum.MEMO:
			lblTotlGbkt.setLabel("Bedrag(Debet)");
			fldTotlGbkt.setReadonly(true);
			lblTotlIncl.setLabel("Bedrag(Credit)");
			fldTotlIncl.setReadonly(true);
			fldOmschr1.setMandatory(true);
			if (headerData == null) {
				gridMisc.add(lblOmschr, 0, 0);
				gridMisc.add(fldOmschr1, 0, 1);
				gridMisc.add(fldOmschr2, 1, 1);
			} else {
				gridMisc.add(lblTotlGbkt, 0, 0);
				gridMisc.add(fldTotlGbkt, 0, 1);
				gridMisc.add(lblTotlIncl, 1, 0);
				gridMisc.add(fldTotlIncl, 1, 1);
				gridMisc.add(lblOmschr, 2, 0);
				gridMisc.add(fldOmschr1, 2, 1);
				gridMisc.add(fldOmschr2, 3, 1);
			}
			break;
		case DagboekSoortEnum.INKOOP:
			lblDcNummer.setLabel("Crediteur");
			lblDcNummer.setHidden(false);
			fldCrediteur.setHidden(false);
			fldCrediteur.setMandatory(true);
			fldFactuur.setMandatory(true);
			fldTotlGbkt.setMandatory(true);
			gridMisc.add(lblDcNummer, 0, 0);
			gridMisc.add(fldCrediteur, 0, 1);
			gridMisc.add(lblTotlGbkt, 1, 0);
			gridMisc.add(fldTotlGbkt, 1, 1);
			gridMisc.add(lblReferentie, 2, 0);
			gridMisc.add(fldReferentie, 2, 1);
			gridMisc.add(lblOmschr, 3, 0);
			gridMisc.add(fldOmschr1, 3, 1);
			gridMisc.add(fldOmschr2, 4, 1);
			break;
		case DagboekSoortEnum.VERKOOP:
			lblDcNummer.setLabel("Debiteur");
			lblDcNummer.setHidden(false);
			fldDebiteur.setHidden(false);
			fldDebiteur.setMandatory(true);
			fldFactuur.setMandatory(true);
			fldTotlGbkt.setMandatory(true);
			gridMisc.add(lblDcNummer, 0, 0);
			gridMisc.add(fldDebiteur, 0, 1);
			gridMisc.add(lblTotlGbkt, 1, 0);
			gridMisc.add(fldTotlGbkt, 1, 1);
			gridMisc.add(lblReferentie, 2, 0);
			gridMisc.add(fldReferentie, 2, 1);
			gridMisc.add(lblOmschr, 3, 0);
			gridMisc.add(fldOmschr1, 3, 1);
			gridMisc.add(fldOmschr2, 4, 1);
			break;
		}
		miscPanel.removeAllUIObjects();
		miscPanel.addUIObject(gridMisc);
	}

	public void load(HeaderData headerData) throws Exception {
		this.headerData = headerData;
		suppressEvent = true;
		Debiteur debiteur = null;
		Crediteur crediteur = null;
		if (headerData == null) {
			resetFields();
			Date boekdatum = new Date();
			int[] tmp = DateHelper.bepaalBoekperiode(bedrijf, boekdatum);
			tmpBoekjaar = tmp[0];
			tmpBoekperiode = tmp[1];

			headerDataBean = new HeaderDataDataBean();
			headerDataBean.setBedrijf(dagboek.getBedrijf());
			headerDataBean.setDagboekId(dagboek.getId());
			headerDataBean.setDagboekSoort(dagboek.getSoort());
			headerDataBean.setBoekstuk(GeneralHelper.genereerNieuwBoekstukNummer(dagboek, tmpBoekjaar, tmpBoekperiode));
			headerDataBean.setBoekdatum(boekdatum);
			headerDataBean.setBoekjaar(tmpBoekjaar);
			headerDataBean.setBoekperiode(tmpBoekperiode);
			headerDataBean.setDC(dagboek.isInkoopboek() ? "C":"D");
			headerDataBean.setStatus(GeneralHelper.getOpenStatus(dagboek));
			fldBoekstuk.setReadonly(!manualBoekstuk);
			fldBoekdatum.setReadonly(false);
			fldBtwCode.setSelectedOptionValue("X");
		} else {
			headerDataBean = headerData.getHeaderDataDataBean();
			fldBoekstuk.setReadonly(true);
			fldBoekdatum.setReadonly(true);
			if (dagboek.isKasboek()) {
				DetailData detailData = getDetail(headerData.getBedrijf(), headerData.getDagboekId(), headerData.getBoekstuk());
				fldDetlAmt.setValue(detailData.getPrijs());
				fldDetlBtw.setValue(detailData.getBedragBtw());
				fldDetlExcl.setValue(detailData.getBedragExcl());
				fldDetlIncl.setValue(detailData.getBedragIncl());
				fldBtwCode.setSelectedOptionValue(detailData.getBtwCode());
			}
			debiteur = GeneralHelper.getDebiteur(bedrijf, headerDataBean.getDcNummer());
			crediteur = GeneralHelper.getCrediteur(bedrijf, headerDataBean.getDcNummer());
		}

		fldBoekstuk.setValue(headerDataBean.getBoekstuk());
		fldBoekdatum.setValue(headerDataBean.getBoekdatum());
		fldDebiteur.setValue(debiteur);
		fldCrediteur.setValue(crediteur);
		fldDebiteurFact.setValue(headerDataBean.getFactuurNummer());
		fldCrediteurFact.setValue(headerDataBean.getFactuurNummer());
		fldFactuur.setValue(headerDataBean.getFactuurNummer());
		fldFactuurdatum.setValue(headerDataBean.getFactuurdatum());
		fldVervaldatum.setValue(headerDataBean.getVervaldatum());
		fldOmschr1.setValue(headerDataBean.getOmschr1());
		fldOmschr2.setValue(headerDataBean.getOmschr2());
		fldReferentie.setValue(headerDataBean.getReferentieNummer());
		fldSaldoBegin.setValue(headerDataBean.getBeginsaldo());
		fldSaldoEind.setValue(headerDataBean.getEindsaldo());
		fldTotlBtw.setValue(headerDataBean.getTotaalBtw());
		fldTotlExcl.setValue(headerDataBean.getTotaalExcl());
		fldTotlGbkt.setValue(headerDataBean.getTotaalGeboekt());
		fldTotlIncl.setValue(headerDataBean.getTotaalIncl());
		fldDc.setSelectedOptionValue(headerDataBean.getDC());
		tmpBoekjaar = headerDataBean.getBoekjaar();
		tmpBoekperiode = headerDataBean.getBoekperiode();
		if (dagboek.isKasboek()) {
			lblDc.setLabel(headerDataBean.getDC().equals("D") ? "Kasontvangst" : headerDataBean.getDC().equals("C") ? "Kasuitgave" : "");
		} else if(dagboek.isBankboek()) {
			lblDc.setLabel(headerDataBean.getDC().equals("D") ? "Bij" : headerDataBean.getDC().equals("C") ? "Af" : "");
		} else {
			lblDc.setLabel(headerDataBean.getDC().equals("D") ? "Debet" : headerDataBean.getDC().equals("C") ? "Credit" : "");
		}
		if (dagboek.isKasboek()) {
			loadDetail();
		}
		setLayout();
		suppressEvent = false;
		vrijeRubriekPanel.loadFrom(headerData, null);
	}

	private void loadDetail() throws Exception {
		fldDebiteur.setHidden(true);
		fldDebiteur.setMandatory(false);
		fldDebiteurFact.setValue("");
		fldDebiteurFact.setHidden(true);
		fldDebiteurFact.setMandatory(false);
		fldCrediteur.setHidden(true);
		fldCrediteur.setMandatory(false);
		fldCrediteurFact.setValue("");
		fldCrediteurFact.setHidden(true);
		fldCrediteurFact.setMandatory(false);
		lblDcNummer.setHidden(true);
		lblFactuur.setHidden(true);
		DetailData detailData = getDetail(headerDataBean.getBedrijf(), headerDataBean.getDagboekId(), headerDataBean.getBoekstuk());
		Rekening rekening = detailData.getRekeningObject();
		if (rekening != null) {
			fldRekening.setValue(rekening);
			if (rekening.getSubAdministratieType().trim().equals("D")) {
				lblDcNummer.setLabel("Debiteur");
				lblDcNummer.setTranslatedLabel("Debiteur");
				fldDebiteur.setHidden(false);
				fldDebiteur.setMandatory(true);
				fldDebiteurFact.setHidden(false);
				fldDebiteurFact.setDcNummer("D", detailData.getDcNummer());
				HeaderData factuur = GeneralHelper.getFactuurRecord(bedrijf, detailData.getDcNummer(), detailData.getFactuurNummer(), DagboekSoortEnum.VERKOOP);
				if (factuur != null) {
					fldDebiteurFact.setValue(factuur.getFactuurNummer());
				}
			} else if (rekening.getSubAdministratieType().trim().equals("C")) {
				lblDcNummer.setLabel("Crediteur");
				lblDcNummer.setTranslatedLabel("Crediteur");
				fldCrediteur.setHidden(false);
				fldCrediteur.setMandatory(true);
				fldCrediteurFact.setHidden(false);
				fldCrediteurFact.setDcNummer("C", detailData.getDcNummer());
				HeaderData factuur = GeneralHelper.getFactuurRecord(bedrijf, detailData.getDcNummer(), detailData.getFactuurNummer(), DagboekSoortEnum.INKOOP);
				if (factuur != null) {
					fldCrediteurFact.setValue(factuur.getFactuurNummer());
				}
			}
		}
	}

	public HeaderData save() throws Exception {
		if (fldSaldoBegin.isMandatory()) {
			BigDecimal value = fldSaldoEind.getBigDecimal().subtract(fldSaldoBegin.getBigDecimal()).abs().setScale(2, BigDecimal.ROUND_HALF_UP);
			fldTotlBtw.setValue(ZERO);
			fldTotlExcl.setValue(value);
			fldTotlGbkt.setValue(value);
			fldTotlIncl.setValue(value);
			fldDc.setSelectedOptionValue(fldSaldoEind.getBigDecimal().doubleValue() > fldSaldoBegin.getBigDecimal().doubleValue() ? "D" : "C");
		}
		boekdatumListener.event(null, "");
		headerDataBean.setBoekdatum(fldBoekdatum.getValueAsDate());
		headerDataBean.setBoekjaar(tmpBoekjaar);
		headerDataBean.setBoekperiode(tmpBoekperiode);
		headerDataBean.setDC(fldDc.getSelectedOptionValue());
		headerDataBean.setDcNummer(!fldDebiteur.isHidden() ? fldDebiteur.getValue() : !fldCrediteur.isHidden() ? fldCrediteur.getValue() : "");
		headerDataBean.setOmschr1(fldOmschr1.getValue());
		headerDataBean.setOmschr2(fldOmschr2.getValue());
		headerDataBean.setReferentieNummer(fldReferentie.getValue());
		headerDataBean.setBeginsaldo(fldSaldoBegin.getBigDecimal());
		headerDataBean.setEindsaldo(fldSaldoEind.getBigDecimal());
		headerDataBean.setTotaalBtw(fldTotlBtw.getBigDecimal());
		headerDataBean.setTotaalExcl(fldTotlExcl.getBigDecimal());
		headerDataBean.setTotaalGeboekt(fldTotlGbkt.getBigDecimal());
		headerDataBean.setTotaalIncl(fldTotlIncl.getBigDecimal());
		headerDataBean.setVervaldatum(fldVervaldatum.getValueAsDate());
		validateBean();
		if (headerData == null) {
			headerDataBean.setTotaalBetaald(ZERO);
			headerData = HeaderDataManagerFactory.getInstance(dbd).create(headerDataBean);
			dagboek.setLaatsteBoekstuk(headerData.getBoekstuk());
		} else {
			headerData.update(headerDataBean);
		}
		if (dagboek.isKasboek()) {
			saveDetail();
			headerData.setTotaalGeboekt(fldDetlIncl.getBigDecimal());
			headerData.setTotaalBetaald(fldDetlIncl.getBigDecimal());
		}
		headerData.recalculate();
		vrijeRubriekPanel.saveTo(headerData);
		return headerData;
	}

	private void saveDetail() throws Exception {
		DetailData detailData = getDetail(headerDataBean.getBedrijf(), headerDataBean.getDagboekId(), headerDataBean.getBoekstuk()); 
		DetailDataDataBean detailDataBean = detailData.getDetailDataDataBean();
		detailDataBean.setAantal(BigDecimal.ONE);
		detailDataBean.setBedragBtw(fldDetlBtw.getBigDecimal());
		detailDataBean.setBedragExcl(fldDetlExcl.getBigDecimal());
		detailDataBean.setBedragIncl(fldDetlIncl.getBigDecimal());
		detailDataBean.setBtwCode(fldBtwCode.getSelectedOptionValue());
		detailDataBean.setDatumLevering(null);
		detailDataBean.setDC(headerDataBean.getDC().equals("C") ? "D" : "C");
		detailDataBean.setDcNummer(!fldDebiteur.isHidden() ? fldDebiteur.getValue() : !fldCrediteur.isHidden() ? fldCrediteur.getValue() : "");
		detailDataBean.setFactuurNummer(!fldDebiteurFact.isHidden() ? fldDebiteurFact.getValue() : !fldCrediteurFact.isHidden() ? fldCrediteurFact.getValue() : "");
		detailDataBean.setEenheid("");
		detailDataBean.setOmschr1(headerDataBean.getOmschr1());
		detailDataBean.setOmschr2(headerDataBean.getOmschr2());
		detailDataBean.setOmschr3("");
		detailDataBean.setRekening(fldRekening.getValue());
		detailDataBean.setPrijs(fldDetlAmt.getBigDecimal());
		detailData.update(detailDataBean);
	}

	private DetailData getDetail(String bedrijf, String dagboekId, String boekstuk) throws Exception {
		DetailDataPK key = new DetailDataPK();
		key.setBedrijf(bedrijf);
		key.setDagboekId(dagboekId);
		key.setBoekstuk(boekstuk);
		key.setBoekstukRegel(1);
		return DetailDataManagerFactory.getInstance(dbd).findOrCreate(key); 
	}

	private void validateBean() throws Exception {
		// Check boekdatum
		int currentBoekjaar = ((Integer)ESPSessionContext.getSessionAttribute(SessionKeys.BOEKJAAR)).intValue();
		if (currentBoekjaar != headerDataBean.getBoekjaar()) {
			fldBoekdatum.setInvalidTag();
			throw new UserMessageException("Deze boekdatum valt buiten het actieve boekjaar");
		}
		// Check nieuw boekstuknummer
		if (headerData == null) {
			if (manualBoekstuk) {
				headerKey.setBoekstuk(fldBoekstuk.getValue());
				try {
					hdrMgr.findByPrimaryKey(headerKey);
					fldBoekstuk.setInvalidTag();
					throw new UserMessageException("Dit boekstuknummer bestaat reeds!");
				} catch (FinderException e) {
				}
			}
			headerDataBean.setBoekstuk(fldBoekstuk.getValue());
		}
		if (dagboek.isInkoopboek() || dagboek.isVerkoopboek()) {
			headerDataBean.setFactuurdatum(headerDataBean.getBoekdatum());
			headerDataBean.setFactuurNummer(headerDataBean.getBoekstuk());
		} else {
			headerDataBean.setFactuurdatum(null);
			headerDataBean.setFactuurNummer("");
		}
	}

	private void resetFields() throws Exception {
		fldBoekstuk.setValue();
		fldFactuur.setValue();
		fldReferentie.setValue();
		fldOmschr1.setValue();
		fldOmschr2.setValue();
		fldTotlExcl.setValue(ZERO);
		fldTotlBtw.setValue(ZERO);
		fldTotlIncl.setValue(ZERO);
		fldTotlGbkt.setValue(ZERO);
		fldDetlAmt.setValue(ZERO);
		fldDetlExcl.setValue(ZERO);
		fldDetlBtw.setValue(ZERO);
		fldDetlIncl.setValue(ZERO);
		fldSaldoBegin.setValue(ZERO);
		fldSaldoEind.setValue(ZERO);
		fldBoekdatum.setValue((Date)null);
		fldFactuurdatum.setValue((Date)null);
		fldVervaldatum.setValue((Date)null);
		fldDebiteur.setValue();
		fldCrediteur.setValue();
		fldRekening.setValue();
		fldDebiteurFact.setValue();
		fldCrediteurFact.setValue();
	}

	private class BoekdatumListener implements EventListener {
		private static final long serialVersionUID = 2680984705342842641L;

		public void event(UIObject object, String eventType) throws Exception {
			if (suppressEvent)
				return;
			Date boekdatum = fldBoekdatum.getValueAsDate();
			if (boekdatum == null) {
				tmpBoekjaar = 0;
				tmpBoekperiode = 0;
				fldBoekdatum.setInvalidTag();
				throw new UserMessageException("De periodetabel is niet ingericht voor deze boekdatum");
			} else {
				int[] tmp = DateHelper.bepaalBoekperiode(bedrijf, boekdatum);
				tmpBoekjaar = tmp[0];
				tmpBoekperiode = tmp[1];
				fldBoekstuk.setValue(GeneralHelper.genereerNieuwBoekstukNummer(dagboek, tmpBoekjaar, tmpBoekperiode));
				if (PeriodeHelper.isClosed(bedrijf, tmpBoekjaar)) {
					fldBoekdatum.setInvalidTag();
					throw new UserMessageException("In dit boekjaar kunnen geen boekingen meer worden gemaakt");
				}
			}
		}
	}
	private class RecalcListener implements EventListener {
		private static final long serialVersionUID = 7163982728963290595L;

		public void event(UIObject object, String type) throws Exception {
			if (suppressEvent)
				return;
			BigDecimal bedrag = fldDetlAmt.getBigDecimal().setScale(2);
			BtwCode btwCode = fldBtwCode.getMyObject();
			double percentage = btwCode == null ? 0d : btwCode.getPercentage().doubleValue();
			if (percentage == 0d) {
				fldDetlBtw.setValue(ZERO);
				fldDetlExcl.setValue(bedrag);
				fldDetlIncl.setValue(bedrag);
			} else {
				if (btwCode.getInEx().equals("E")) {
					fldDetlExcl.setValue(bedrag);
					fldDetlBtw.setValue(bedrag.multiply(new BigDecimal(percentage / 100)).setScale(2, BigDecimal.ROUND_HALF_UP));
					fldDetlIncl.setValue(bedrag.add(fldDetlBtw.getBigDecimal()));
				} else {
					fldDetlIncl.setValue(bedrag);
					fldDetlExcl.setValue((bedrag.multiply(new BigDecimal(100.00)).divide(new BigDecimal(100 + percentage), BigDecimal.ROUND_HALF_UP)));
					fldDetlBtw.setValue(fldDetlIncl.getBigDecimal().subtract(fldDetlExcl.getBigDecimal()));
				}
			}
		}
	}
	private class RekeningListener implements EventListener {
		private static final long serialVersionUID = 5346490986158082382L;

		public void event(UIObject object, String type) throws Exception {
			if (suppressEvent)
				return;
			fldDebiteur.setHidden(true);
			fldDebiteur.setMandatory(false);
			fldDebiteurFact.setValue("");
			fldDebiteurFact.setHidden(true);
			fldDebiteurFact.setMandatory(false);
			fldCrediteur.setHidden(true);
			fldCrediteur.setMandatory(false);
			fldCrediteurFact.setValue("");
			fldCrediteurFact.setHidden(true);
			fldCrediteurFact.setMandatory(false);
			lblDcNummer.setHidden(true);
			lblFactuur.setHidden(true);
			Rekening rekening = fldRekening.getMyObject();
			if (rekening != null && rekening.getSubAdministratieType().trim().length() > 0) {
				lblDcNummer.setHidden(false);
				lblFactuur.setHidden(dagboek.isVerkoopboek() ? true : false);
				if (rekening.getSubAdministratieType().trim().equals("D")) {
					lblDcNummer.setLabel("Debiteur");
					lblDcNummer.setTranslatedLabel("Debiteur");
					fldDebiteur.setHidden(false);
					fldDebiteur.setMandatory(true);
					fldDebiteurFact.setHidden(false);
					fldDebiteurFact.setDcNummer("D", "*NONE");
				} else if (rekening.getSubAdministratieType().trim().equals("C")) {
					lblDcNummer.setLabel("Crediteur");
					lblDcNummer.setTranslatedLabel("Crediteur");
					fldCrediteur.setHidden(false);
					fldCrediteur.setMandatory(true);
					fldCrediteurFact.setHidden(false);
					fldCrediteurFact.setDcNummer("C", "*NONE");
				}
			}
			if (miscPanel != null) {
				miscPanel.setModified();
			}
		}
	}

	private class DcNummerListener implements EventListener {
		private static final long serialVersionUID = 5346490986158082382L;

		public void event(UIObject object, String type) throws Exception {
			if (suppressEvent)
				return;
			String dcNr = ((Field) object).getValue();
			if (object instanceof DebiteurReferenceField) {
				fldDebiteurFact.setDcNummer("D", dcNr);
			} else {
				fldCrediteurFact.setDcNummer("C", dcNr);
			}
		}
	}
}