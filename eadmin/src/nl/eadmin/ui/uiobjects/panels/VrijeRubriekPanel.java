package nl.eadmin.ui.uiobjects.panels;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.HeaderData;
import nl.eadmin.helpers.VrijeRubriekHelper;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.Panel;

public class VrijeRubriekPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private InputComponent[] field;
	private SimpleDateFormat yyyymmddFormat = new SimpleDateFormat("yyyy-MM-dd");

	public VrijeRubriekPanel(Dagboek dagboek) throws Exception {
		this.field = VrijeRubriekHelper.getInputComponenten(dagboek);
		this.setLabel("Vrije rubriek(en)");
		ESPGridLayout grid = new ESPGridLayout();
		grid.setColumnWidths(new short[] { ApplicationConstants.STD_LABELWIDTH, 300 });
		boolean hidden = true;
		int row = 0;
		for (int f = 0; f < 5; f++) {
			if (field[f] != null) {
				hidden = false;
				field[f].setDiscardLabel(true);
				grid.add(new Label(field[f].getLanguageCode()), row, 0);
				grid.add(field[f], row++, 1);
			}
		}
		addUIObject(grid);
		setHidden(hidden);
	}

	public void loadFrom(HeaderData headerData, DetailData detailData) throws Exception {
		if (isHidden())
			return;
		String[] value = new String[5];
		if (detailData == null) {
			if (headerData == null) {
				value[0] = "";
				value[1] = "";
				value[2] = "";
				value[3] = "";
				value[4] = "";
			} else {
				value[0] = headerData.getVrijeRub1();
				value[1] = headerData.getVrijeRub2();
				value[2] = headerData.getVrijeRub3();
				value[3] = headerData.getVrijeRub4();
				value[4] = headerData.getVrijeRub5();
			}
		} else {
			value[0] = detailData.getVrijeRub1();
			value[1] = detailData.getVrijeRub2();
			value[2] = detailData.getVrijeRub3();
			value[3] = detailData.getVrijeRub4();
			value[4] = detailData.getVrijeRub5();
		}
		for (int f = 0; f < 5; f++) {
			if (field[f] != null) {
				if (field[f] instanceof CheckBox) {
					((CheckBox) field[f]).setValue(value[f].trim().equals(Boolean.TRUE.toString()));
				} else if (field[f] instanceof NumberField) {
					Long nr = 0l;
					try {
						nr = Long.valueOf(value[f]);
					} catch (Exception e) {
					}
					((NumberField) field[f]).setValue(nr);
				} else if (field[f] instanceof DecimalField) {
					BigDecimal dec = new BigDecimal(0.00);
					try {
						dec = new BigDecimal(Double.parseDouble(value[f]) / 100).setScale(2, BigDecimal.ROUND_HALF_UP);
					} catch (Exception e) {
					}
					((DecimalField) field[f]).setValue(dec);
				} else if (field[f] instanceof DateSelectionField) {
					Date date = null;
					try {
						date = yyyymmddFormat.parse(value[f]);
					} catch (Exception e) {
					}
					((DateSelectionField) field[f]).setValue(date);
				} else {
					field[f].setValueAsString(value[f]);
				}
			}
		}
	}

	public void saveTo(Object objectToSave) throws Exception {
		if (isHidden())
			return;
		String[] value = new String[] { "", "", "", "", "" };
		for (int f = 0; f < 5; f++) {
			if (field[f] != null) {
				if (field[f] instanceof CheckBox) {
					value[f] = String.valueOf(((CheckBox) field[f]).getValueAsBoolean());
				} else if (field[f] instanceof NumberField) {
					value[f] = "0000000000" + ((NumberField) field[f]).getLong();
					value[f] = value[f].substring(value[f].length() - 10);
				} else if (field[f] instanceof DecimalField) {
					value[f] = "0000000000" + (((DecimalField) field[f]).getBigDecimal()).toString().replaceAll("\\.", "");
					value[f] = value[f].substring(value[f].length() - 10);
				} else if (field[f] instanceof DateSelectionField) {
					value[f] = yyyymmddFormat.format(((DateSelectionField) field[f]).getValueAsDate());
				} else {
					value[f] = field[f].getValue();
				}
			}
		}
		if (objectToSave instanceof HeaderData) {
			HeaderData headerData = (HeaderData) objectToSave;
			headerData.setVrijeRub1(value[0]);
			headerData.setVrijeRub2(value[1]);
			headerData.setVrijeRub3(value[2]);
			headerData.setVrijeRub4(value[3]);
			headerData.setVrijeRub5(value[4]);
		} else {
			DetailData detailData = (DetailData) objectToSave;
			detailData.setVrijeRub1(value[0]);
			detailData.setVrijeRub2(value[1]);
			detailData.setVrijeRub3(value[2]);
			detailData.setVrijeRub4(value[3]);
			detailData.setVrijeRub5(value[4]);
		}
	}
}