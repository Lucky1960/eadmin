package nl.eadmin.ui.uiobjects.panels;

import java.math.BigDecimal;
import java.util.Date;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataDataBean;
import nl.eadmin.db.DetailDataManager;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.DetailDataPK;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.Product;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.RekeningPK;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.uiobjects.ComboBtwCodes;
import nl.eadmin.ui.uiobjects.ComboDC;
import nl.eadmin.ui.uiobjects.actions.SelecteerProductAction;
import nl.eadmin.ui.uiobjects.actions.SelecteerProductAction.SelectProductListener;
import nl.eadmin.ui.uiobjects.referencefields.CrediteurReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.DebiteurReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.FactuurReferenceField2;
import nl.eadmin.ui.uiobjects.referencefields.RekeningReferenceField;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.Image;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.jsql.DBData;

public class DetailDataPanel extends Panel implements SelectProductListener {
	private static final long serialVersionUID = 1L;
	private DetailDataPK detailKey;
	private DetailDataDataBean detailDataBean;
	private DetailDataManager detailMgr;
	private DetailData detailData;
	private HeaderData headerData;
	private Bedrijf bedrijf;
	private Dagboek dagboek;
	private ESPGridLayout gridMain;
	private Label lblAantal = new Label("Aantal");
	private Label lblEenheid = new Label("Eenheid");
	private Label lblPrijs = new Label("Prijs");
	private Label lblBtwCode = new Label("BTW-code");
	private Label lblAflDatum = new Label("Transactiedatum");
	private Label lblDc = new Label("D/C");
	private Label lblDcNummer = new Label("Relatienr.");
	private Label lblFactuur = new Label("Factuurnr.");
	private Label lblOmschr = new Label("Omschrijving");
	private Label lblRekening = new Label("Grootboekrekening");
	private Label lblAmtExcl = new Label("Bedrag Excl.");
	private Label lblAmtBtw = new Label("Bedrag BTW");
	private Label lblAmtIncl = new Label("Bedrag Incl.");
	private Field fldEenheid, fldOms1, fldOms2, fldOms3;
	private DecimalField fldAantal, fldPrijs, fldAmtExcl, fldAmtBtw, fldAmtIncl;
	private DebiteurReferenceField fldDebiteur;
	private CrediteurReferenceField fldCrediteur;
	private ComboBtwCodes fldBtwCode;
	private RekeningReferenceField fldRekening;
	private FactuurReferenceField2 fldDebiteurFact, fldCrediteurFact;
	private DateSelectionField fldAflDatum;
	private ComboDC fldDc;
	private BigDecimal ZERO = new BigDecimal(0.00).setScale(2);
	private DcNummerListener dcNummerListener = new DcNummerListener();
	private RecalcListener recalcListener = new RecalcListener();
	private RekeningListener rekeningListener = new RekeningListener();
	private boolean suppressEvent, readOnly;
	private String factuurNr_OUD, dcNr_OUD;
	private BigDecimal bedragIncl_OUD;
	private VrijeRubriekPanel vrijeRubriekPanel;
	private Image imgProduct;
	private DBData dbd;

	public DetailDataPanel(Bedrijf bedrijf, Dagboek dagboek, HeaderData headerData, boolean readOnly) throws Exception {
		this.bedrijf = bedrijf;
		this.dagboek = dagboek;
		this.headerData = headerData;
		this.readOnly = readOnly;
		this.dbd = bedrijf.getDBData();
		this.vrijeRubriekPanel = new VrijeRubriekPanel(dagboek);
		SelecteerProductAction selectProduct = new SelecteerProductAction(null, bedrijf);
		selectProduct.addSelectProductListener(this);
		imgProduct = new Image("");
		imgProduct.setSource("cart.png");
		imgProduct.setAlignment("left");
		imgProduct.addAction(selectProduct);

		suppressEvent = true;
		fldAantal = new DecimalField("", 10, 2, true);
		fldAantal.setDiscardLabel(true);
		fldAantal.setValue(ZERO);
		fldAantal.setReadonly(readOnly);
		fldAantal.setMandatory(true);
		fldAantal.addOnChangeListener(recalcListener);
		fldPrijs = new DecimalField("", 15, 2, true);
		fldPrijs.setDiscardLabel(true);
		fldPrijs.setValue(ZERO);
		fldPrijs.setReadonly(readOnly);
		fldPrijs.setMandatory(true);
		fldPrijs.addOnChangeListener(recalcListener);
		fldAmtBtw = new DecimalField("", 15, 2, true);
		fldAmtBtw.setDiscardLabel(true);
		fldAmtBtw.setReadonly(true);
		fldAmtBtw.setValue(ZERO);
		fldAmtExcl = new DecimalField("", 15, 2, true);
		fldAmtExcl.setDiscardLabel(true);
		fldAmtExcl.setReadonly(true);
		fldAmtExcl.setValue(ZERO);
		fldAmtIncl = new DecimalField("", 15, 2, true);
		fldAmtIncl.setDiscardLabel(true);
		fldAmtIncl.setReadonly(true);
		fldAmtIncl.setValue(ZERO);
		fldDebiteur = new DebiteurReferenceField("Debiteur", bedrijf, DebiteurManagerFactory.getInstance(dbd));
		fldDebiteur.setDiscardLabel(true);
		fldDebiteur.setLength(10);
		fldDebiteur.setDescriptionLength(30);
		fldDebiteur.addOnChangeListener(dcNummerListener);
		fldCrediteur = new CrediteurReferenceField("Crediteur", bedrijf, CrediteurManagerFactory.getInstance(dbd));
		fldCrediteur.setDiscardLabel(true);
		fldCrediteur.setLength(10);
		fldCrediteur.setDescriptionLength(30);
		fldCrediteur.addOnChangeListener(dcNummerListener);
		fldBtwCode = new ComboBtwCodes(bedrijf);
		fldBtwCode.setDiscardLabel(true);
		fldBtwCode.setWidth("120");
		fldBtwCode.setReadonly(readOnly);
		fldBtwCode.addOnChangeListener(recalcListener);
		fldRekening = new RekeningReferenceField("GrootboekRek.", bedrijf, RekeningManagerFactory.getInstance(dbd), false);
		fldRekening.setDiscardLabel(true);
		fldRekening.setLength(10);
		fldRekening.setDescriptionLength(30);
		fldRekening.addOnChangeListener(rekeningListener);
		fldRekening.setMandatory(true);
		fldAflDatum = new DateSelectionField("", (Date) null, "dd-MM-yyyy");
		fldAflDatum.setDiscardLabel(true);
		fldAflDatum.setReadonly(readOnly);
		fldDc = new ComboDC(dagboek, false);
		fldDc.setDiscardLabel(true);
		fldDc.setReadonly(readOnly);
		lblDc.setLabel(fldDc.getLabel());
		fldDebiteurFact = new FactuurReferenceField2("Factuurnr.", bedrijf, JournaalManagerFactory.getInstance(dbd));
		fldDebiteurFact.setDiscardLabel(true);
		fldDebiteurFact.setLength(10);
		fldDebiteurFact.setDescriptionLength(30);
		fldCrediteurFact = new FactuurReferenceField2("Factuurnr.", bedrijf, JournaalManagerFactory.getInstance(dbd));
		fldCrediteurFact.setDiscardLabel(true);
		fldCrediteurFact.setLength(10);
		fldCrediteurFact.setDescriptionLength(30);
		fldEenheid = new Field("");
		fldEenheid.setType(Field.TYPE_TEXT);
		fldEenheid.setMaxLength(10);
		fldEenheid.setLength(10);
		fldEenheid.setDiscardLabel(true);
		fldEenheid.setReadonly(readOnly);
		fldOms1 = new Field("");
		fldOms1.setType(Field.TYPE_TEXT);
		fldOms1.setMaxLength(50);
		fldOms1.setLength(50);
		fldOms1.setDiscardLabel(true);
		fldOms1.setReadonly(readOnly);
		fldOms2 = new Field("");
		fldOms2.setType(Field.TYPE_TEXT);
		fldOms2.setMaxLength(50);
		fldOms2.setLength(50);
		fldOms2.setDiscardLabel(true);
		fldOms2.setReadonly(readOnly);
		fldOms3 = new Field("");
		fldOms3.setType(Field.TYPE_TEXT);
		fldOms3.setMaxLength(50);
		fldOms3.setLength(50);
		fldOms3.setDiscardLabel(true);
		fldOms3.setReadonly(readOnly);
		setLayout();
		suppressEvent = false;
		addUIObject(gridMain);
		addUIObject(vrijeRubriekPanel);
	}

	private void setLayout() throws Exception {
		gridMain = new ESPGridLayout();
		gridMain.setColumnWidths(new short[] { 80, 80, 80, 80, 80, 80, 80 });
		switch (dagboek.getSoort()) {
		case DagboekSoortEnum.BANK:
			lblPrijs.setLabel("Bedrag");
			lblPrijs.setTranslatedLabel("Bedrag");
			fldPrijs.setLabel("Bedrag");
			fldPrijs.setTranslatedLabel("Bedrag");
			gridMain.add(lblOmschr, 0, 0);
			gridMain.add(fldOms1, 1, 0, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldOms2, 2, 0, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldOms3, 3, 0, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(lblPrijs, 0, 2);
			gridMain.add(fldPrijs, 1, 2);
			gridMain.add(lblBtwCode, 0, 3);
			gridMain.add(fldBtwCode, 1, 3);
			if (headerData.isManualBank()) {
				gridMain.add(lblDc, 0, 4);
				gridMain.add(fldDc, 1, 4);
			}
			gridMain.add(lblAmtExcl, 3, 2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldAmtExcl, 4, 2);
			gridMain.add(lblAmtBtw, 3, 3, 1, 1, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldAmtBtw, 4, 3);
			gridMain.add(lblAmtIncl, 3, 4, 1, 1, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldAmtIncl, 4, 4);
			if (!readOnly) {
				gridMain.add(new Paragraph(""), 5, 0);
				gridMain.add(lblRekening, 6, 0);
				gridMain.add(fldRekening, 7, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(lblDcNummer, 8, 0);
				gridMain.add(fldDebiteur, 9, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(fldCrediteur, 10, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(lblFactuur, 11, 0);
				gridMain.add(fldDebiteurFact, 12, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(fldCrediteurFact, 13, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			}
			break;
		case DagboekSoortEnum.KAS:
			// Dummy
			break;
		case DagboekSoortEnum.MEMO:
			// fldDebiteurFact.setValidation(false);
			// fldCrediteurFact.setValidation(false);
			fldDebiteurFact.setSelectableOnly(false);
			fldCrediteurFact.setSelectableOnly(false);
			lblPrijs.setLabel("Bedrag");
			lblPrijs.setTranslatedLabel("Bedrag");
			fldPrijs.setLabel("Bedrag");
			fldPrijs.setTranslatedLabel("Bedrag");
			gridMain.add(lblOmschr, 0, 0);
			gridMain.add(fldOms1, 1, 0, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldOms2, 2, 0, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldOms3, 3, 0, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(lblDc, 0, 2);
			gridMain.add(fldDc, 1, 2);
			gridMain.add(lblPrijs, 0, 3);
			gridMain.add(fldPrijs, 1, 3);
			gridMain.add(lblBtwCode, 0, 4);
			gridMain.add(fldBtwCode, 1, 4, 1, 3, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(lblAmtExcl, 3, 2, 1, 1, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldAmtExcl, 4, 2);
			gridMain.add(lblAmtBtw, 3, 3, 1, 1, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldAmtBtw, 4, 3);
			gridMain.add(lblAmtIncl, 3, 4, 1, 1, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldAmtIncl, 4, 4);
			if (!readOnly) {
				gridMain.add(new Paragraph(""), 5, 0);
				gridMain.add(lblRekening, 6, 0);
				gridMain.add(fldRekening, 7, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(lblDcNummer, 8, 0);
				gridMain.add(fldDebiteur, 9, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(fldCrediteur, 10, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(lblFactuur, 11, 0);
				gridMain.add(fldDebiteurFact, 12, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(fldCrediteurFact, 13, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			}
			break;
		case DagboekSoortEnum.INKOOP:
			fldDc.setSelectedOptionValue("D");
			lblDcNummer.setLabel("Crediteur");
			lblDcNummer.setTranslatedLabel("Crediteur");
			lblPrijs.setLabel("Prijs");
			lblPrijs.setTranslatedLabel("Prijs");
			fldPrijs.setLabel("Prijs");
			fldPrijs.setTranslatedLabel("Prijs");
			gridMain.add(lblAflDatum, 0, 0);
			gridMain.add(fldAflDatum, 1, 0);
			gridMain.add(lblAantal, 0, 1);
			gridMain.add(fldAantal, 1, 1);
			gridMain.add(lblEenheid, 0, 2);
			gridMain.add(fldEenheid, 1, 2);
			gridMain.add(lblOmschr, 0, 3);
			gridMain.add(fldOms1, 1, 3, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldOms2, 2, 3, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldOms3, 3, 3, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(lblPrijs, 0, 5);
			gridMain.add(fldPrijs, 1, 5);
			gridMain.add(lblBtwCode, 0, 6);
			gridMain.add(fldBtwCode, 1, 6, 1, 3, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(lblAmtExcl, 2, 5);
			gridMain.add(fldAmtExcl, 3, 5);
			gridMain.add(lblAmtBtw, 2, 6);
			gridMain.add(fldAmtBtw, 3, 6);
			gridMain.add(lblAmtIncl, 2, 7);
			gridMain.add(fldAmtIncl, 3, 7);
			if (!readOnly) {
				gridMain.add(new Paragraph(""), 5, 0);
				gridMain.add(lblRekening, 6, 0);
				gridMain.add(fldRekening, 7, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(lblDcNummer, 8, 0);
				gridMain.add(fldCrediteur, 9, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(lblFactuur, 10, 0);
				gridMain.add(fldCrediteurFact, 11, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			}
			break;
		case DagboekSoortEnum.VERKOOP:
			fldDc.setSelectedOptionValue("C");
			lblDcNummer.setLabel("Debiteur");
			lblDcNummer.setTranslatedLabel("Debiteur");
			if (!readOnly) {
				gridMain.add(lblDcNummer, 0, 0);
				gridMain.add(fldDebiteur, 1, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(lblFactuur, 2, 0);
				gridMain.add(fldDebiteurFact, 3, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				//gridMain.add(new Paragraph(""), 4, 0);
				gridMain.add(imgProduct, 4, 0);
			}

			gridMain.add(lblAflDatum, 5, 0);
			gridMain.add(fldAflDatum, 6, 0);
			gridMain.add(lblAantal, 5, 1);
			gridMain.add(fldAantal, 6, 1);
			gridMain.add(lblEenheid, 5, 2);
			gridMain.add(fldEenheid, 6, 2);
			gridMain.add(lblOmschr, 5, 3);
			gridMain.add(fldOms1, 6, 3, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldOms2, 7, 3, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			gridMain.add(fldOms3, 8, 3, 1, 2, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
			if (!readOnly) {
				gridMain.add(lblRekening, 9, 0);
				gridMain.add(fldRekening, 10, 0, 1, 4, ESPGridLayoutConstraints.GRID_FILL_HORIZONTAL);
				gridMain.add(new Paragraph(""), 11, 0);
			}

			gridMain.add(lblPrijs, 12, 0);
			gridMain.add(fldPrijs, 13, 0);
			gridMain.add(lblBtwCode, 12, 1);
			gridMain.add(fldBtwCode, 13, 1);
			gridMain.add(lblAmtExcl, 14, 0);
			gridMain.add(fldAmtExcl, 15, 0);
			gridMain.add(lblAmtBtw, 14, 1);
			gridMain.add(fldAmtBtw, 15, 1);
			gridMain.add(lblAmtIncl, 14, 2);
			gridMain.add(fldAmtIncl, 15, 2);
			break;
		}
	}

	public void load(DetailData detailData) throws Exception {
		this.detailData = detailData;
		this.detailMgr = DetailDataManagerFactory.getInstance(dbd);
		this.detailKey = new DetailDataPK();
		this.detailKey.setBedrijf(bedrijf.getBedrijfscode());
		this.detailKey.setDagboekId(dagboek.getId());
		this.detailKey.setBoekstuk(headerData == null ? "" : headerData.getBoekstuk());
		Debiteur debiteur = null;
		Crediteur crediteur = null;
		if (detailData == null) {
			BigDecimal dfltBoekingsBedrag = headerData == null ? ZERO : headerData.getTotaalGeboekt().subtract(headerData.getTotaalIncl());
			detailDataBean = new DetailDataDataBean();
			detailDataBean.setBedrijf(bedrijf.getBedrijfscode());
			detailDataBean.setDagboekId(dagboek.getId());
			detailDataBean.setBoekstuk(headerData == null ? "" : headerData.getBoekstuk());
			detailDataBean.setBoekstukRegel(GeneralHelper.genereerNieuwBoekstukRegelNummer(dagboek, headerData));
			detailDataBean.setAantal(new BigDecimal(1.00));
			detailDataBean.setPrijs(dfltBoekingsBedrag);
			detailDataBean.setOmschr1(headerData != null && (dagboek.isInkoopboek() || dagboek.isMemoriaal()) ? headerData.getOmschr1() : "");
			detailDataBean.setOmschr2(headerData != null && (dagboek.isInkoopboek() || dagboek.isMemoriaal()) ? headerData.getOmschr2() : "");
			detailDataBean.setBedragBtw(dfltBoekingsBedrag);
			detailDataBean.setBedragExcl(ZERO);
			detailDataBean.setBedragIncl(dfltBoekingsBedrag);
			detailDataBean.setBtwCode("L");
			detailDataBean.setDC(headerData != null && headerData.getDC().equals("C") ? "D" : "C");
			detailDataBean.setRekening("");
		} else {
			detailDataBean = detailData.getDetailDataDataBean();
			DebiteurPK debKey = new DebiteurPK();
			debKey.setBedrijf(bedrijf.getBedrijfscode());
			debKey.setDebNr(detailDataBean.getDcNummer());
			try {
				debiteur = DebiteurManagerFactory.getInstance(dbd).findByPrimaryKey(debKey);
			} catch (Exception e) {
			}
			CrediteurPK credKey = new CrediteurPK();
			credKey.setBedrijf(bedrijf.getBedrijfscode());
			credKey.setCredNr(detailDataBean.getDcNummer());
			try {
				crediteur = CrediteurManagerFactory.getInstance(dbd).findByPrimaryKey(credKey);
			} catch (Exception e) {
			}
		}

		suppressEvent = true;
		fldAantal.setValue(detailDataBean.getAantal());
		fldAmtBtw.setValue(detailDataBean.getBedragBtw());
		fldAmtExcl.setValue(detailDataBean.getBedragExcl());
		fldAmtIncl.setValue(detailDataBean.getBedragIncl());
		fldBtwCode.setSelectedOptionValue(detailDataBean.getBtwCode());
		fldAflDatum.setValue(detailDataBean.getDatumLevering());
		fldDc.setValueAsString(detailDataBean.getDC());
		fldDebiteur.setValue(debiteur);
		fldCrediteur.setValue(crediteur);
		fldEenheid.setValue(detailDataBean.getEenheid());
		fldDebiteurFact.setValue(detailDataBean.getFactuurNummer());
		fldCrediteurFact.setValue(detailDataBean.getFactuurNummer());
		fldOms1.setValue(detailDataBean.getOmschr1());
		fldOms2.setValue(detailDataBean.getOmschr2());
		fldOms3.setValue(detailDataBean.getOmschr3());
		fldPrijs.setValue(detailDataBean.getPrijs());
		Rekening rekening = null;
		if (detailDataBean.getRekening().length() != 0) {
			RekeningPK key = new RekeningPK();
			key.setBedrijf(bedrijf.getBedrijfscode());
			key.setRekeningNr(detailDataBean.getRekening());
			try {
				rekening = RekeningManagerFactory.getInstance(dbd).findByPrimaryKey(key);
			} catch (Exception e) {
			}
		}
		fldRekening.setValue(rekening);
		factuurNr_OUD = detailDataBean.getFactuurNummer();
		dcNr_OUD = detailDataBean.getDcNummer();
		bedragIncl_OUD = detailDataBean.getBedragIncl();
		suppressEvent = false;
		rekeningListener.event(null, "");
		dcNummerListener.event(fldDebiteur, null);
		dcNummerListener.event(fldCrediteur, null);
		recalcListener.event(null, "");
		vrijeRubriekPanel.loadFrom(headerData, detailData);
	}

	public DetailData save() throws Exception {
		String dc = fldDc.getValue();
		detailDataBean.setAantal(fldAantal.getBigDecimal());
		detailDataBean.setBedragBtw(fldAmtBtw.getBigDecimal());
		detailDataBean.setBedragExcl(fldAmtExcl.getBigDecimal());
		detailDataBean.setBedragIncl(fldAmtIncl.getBigDecimal());
		detailDataBean.setBedragDebet(dc.equals("D") ? fldAmtIncl.getBigDecimal() : ZERO);
		detailDataBean.setBedragCredit(dc.equals("C") ? fldAmtIncl.getBigDecimal() : ZERO);
		detailDataBean.setBtwCode(fldBtwCode.getValue());
		detailDataBean.setDatumLevering(fldAflDatum.getValueAsDate());
		detailDataBean.setDC(dc);
		detailDataBean.setDcNummer(!fldDebiteur.isHidden() ? fldDebiteur.getValue() : !fldCrediteur.isHidden() ? fldCrediteur.getValue() : "");
		detailDataBean.setEenheid(fldEenheid.getValue());
		detailDataBean.setFactuurNummer(!fldDebiteurFact.isHidden() ? fldDebiteurFact.getValue() : !fldCrediteurFact.isHidden() ? fldCrediteurFact.getValue() : detailDataBean.getFactuurNummer());
		detailDataBean.setOmschr1(fldOms1.getValue());
		detailDataBean.setOmschr2(fldOms2.getValue());
		detailDataBean.setOmschr3(fldOms3.getValue());
		detailDataBean.setPrijs(fldPrijs.getBigDecimal());
		Rekening rekening = fldRekening.getMyObject();
		if (rekening.getBlocked()) {
			fldRekening.setInvalidTag();
			throw new UserMessageException("Deze rekening is geblokkeerd.");
		}
		detailDataBean.setRekening(rekening == null ? "" : rekening.getRekeningNr());
		validateBean();
		if (detailData == null) {
			detailData = detailMgr.create(detailDataBean);
		} else {
			detailData.update(detailDataBean);
		}
		if (headerData != null) {
			Dagboek dagboek = headerData.getDagboekObject();
			if (dagboek.isBankboek() || dagboek.isKasboek()) {
				recalculateBedragBetaald();
			}
		}
		vrijeRubriekPanel.saveTo(detailData);
		return detailData;
	}

	private void recalculateBedragBetaald() throws Exception {
		if (factuurNr_OUD.equals(detailData.getFactuurNummer()) && dcNr_OUD.equals(detailData.getDcNummer()) && bedragIncl_OUD.doubleValue() == detailData.getBedragIncl().doubleValue()) {
			return; // Niks gewijzigd
		}

		String dagboekSoort = headerData.getDC().equals("C") ? DagboekSoortEnum.INKOOP : DagboekSoortEnum.VERKOOP;
		HeaderData tmpHeaderData = GeneralHelper.getFactuurRecord(bedrijf, dcNr_OUD, factuurNr_OUD, dagboekSoort);
		if (tmpHeaderData != null) {
			tmpHeaderData.recalculateBedragBetaald();
		}
		if (!factuurNr_OUD.equals(detailData.getFactuurNummer()) || !dcNr_OUD.equals(detailData.getDcNummer())) {
			tmpHeaderData = GeneralHelper.getFactuurRecord(bedrijf, detailData.getDcNummer(), detailData.getFactuurNummer(), dagboekSoort);
			if (tmpHeaderData != null) {
				tmpHeaderData.recalculateBedragBetaald();
			}
			return; // Alleen bedrag gewijzigd
		}
	}

	private void validateBean() throws Exception {
	}

	public void setProduct(Product product) throws Exception {
		fldEenheid.setValue(product.getEenheid());	
		fldPrijs.setValue(product.getPrijs());	
		fldOms1.setValue(product.getOmschr1());	
		fldOms2.setValue(product.getOmschr2());	
		fldOms3.setValue(product.getOmschr3());	
		fldRekening.setValue(product.getRekeningNr());	
	}

	private class RecalcListener implements EventListener {
		private static final long serialVersionUID = 7163982728963290595L;

		public void event(UIObject object, String type) throws Exception {
			if (suppressEvent)
				return;
			BigDecimal aantal = fldAantal.getBigDecimal() == null ? ZERO : fldAantal.getBigDecimal().setScale(2);
			BigDecimal prijs = fldPrijs.getBigDecimal() == null ? ZERO : fldPrijs.getBigDecimal().setScale(2);
			BigDecimal bedrag = aantal.multiply(prijs).setScale(2);
			BtwCode btwCode = fldBtwCode.getMyObject();
			double percentage = btwCode == null ? 0d : btwCode.getPercentage().doubleValue();
			if (percentage == 0d) {
				fldAmtBtw.setValue(ZERO);
				fldAmtExcl.setValue(bedrag);
				fldAmtIncl.setValue(bedrag);
			} else {
				if (btwCode.getInEx().equals("E")) {
					fldAmtExcl.setValue(bedrag);
					fldAmtBtw.setValue(bedrag.multiply(new BigDecimal(percentage / 100)).setScale(2, BigDecimal.ROUND_HALF_UP));
					fldAmtIncl.setValue(new BigDecimal(fldAmtExcl.getDoubleValue() + fldAmtBtw.getDoubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
				} else {
					fldAmtIncl.setValue(bedrag);
					fldAmtExcl.setValue(bedrag.divide(new BigDecimal(100 + percentage), BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100.00)));
					fldAmtExcl.setValue((bedrag.multiply(new BigDecimal(100.00)).divide(new BigDecimal(100 + percentage), BigDecimal.ROUND_HALF_UP)));
					fldAmtBtw.setValue(new BigDecimal(fldAmtIncl.getDoubleValue() - fldAmtExcl.getDoubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
				}
			}
		}
	}

	private class RekeningListener implements EventListener {
		private static final long serialVersionUID = 5346490986158082382L;

		public void event(UIObject object, String type) throws Exception {
			if (suppressEvent)
				return;
			fldDebiteur.setHidden(true);
			fldDebiteur.setMandatory(false);
			fldDebiteurFact.setHidden(true);
			fldDebiteurFact.setMandatory(false);
			fldCrediteur.setHidden(true);
			fldCrediteur.setMandatory(false);
			fldCrediteurFact.setHidden(true);
			fldCrediteurFact.setMandatory(false);
			lblDcNummer.setHidden(true);
			lblFactuur.setHidden(true);
			Rekening rekening = fldRekening.getMyObject();
			if (rekening != null && rekening.getBtwCode().trim().length() > 0) {
				fldBtwCode.setValueAsString(rekening.getBtwCode());
				fldBtwCode.setReadonly(true);
			} else {
				fldBtwCode.setReadonly(readOnly ? true : false);
			}
			if (dagboek.isVerkoopboek()) {
				lblDcNummer.setLabel("Debiteur");
				lblDcNummer.setTranslatedLabel("Debiteur");
				lblDcNummer.setHidden(false);
				fldDebiteur.setHidden(false);
				fldDebiteur.setMandatory(true);
			} else {
				if (rekening != null && rekening.getSubAdministratieType().trim().length() > 0) {
					lblDcNummer.setHidden(false);
					lblFactuur.setHidden(dagboek.isVerkoopboek() ? true : false);
					if (rekening.getSubAdministratieType().trim().equals("D")) {
						lblDcNummer.setLabel("Debiteur");
						lblDcNummer.setTranslatedLabel("Debiteur");
						fldDebiteur.setHidden(false);
						fldDebiteur.setMandatory(true);
						fldDebiteurFact.setHidden(dagboek.isVerkoopboek() ? true : false);
					} else {
						lblDcNummer.setLabel("Crediteur");
						lblDcNummer.setTranslatedLabel("Crediteur");
						fldCrediteur.setHidden(false);
						fldCrediteur.setMandatory(true);
						fldCrediteurFact.setHidden(false);
					}
					lblBtwCode.setHidden(true);
					fldBtwCode.setHidden(true);
					fldBtwCode.setSelectedOptionValue("X");
				} else {
					fldDebiteurFact.setValue("");
					fldCrediteurFact.setValue("");
					lblBtwCode.setHidden(false);
					fldBtwCode.setHidden(false);
				}
			}
			if (gridMain != null) {
				gridMain.setModified();
			}
		}
	}

	private class DcNummerListener implements EventListener {
		private static final long serialVersionUID = 5346490986158082382L;

		public void event(UIObject object, String type) throws Exception {
			if (suppressEvent)
				return;
			String dcNr = ((Field) object).getValue();
//			String dagboekToExclude = (headerData == null ? "" : headerData.getDagboekId());
//			String boekstkToExclude = (headerData == null ? "" : headerData.getBoekstuk());
			if (object instanceof DebiteurReferenceField) {
//				fldDebiteurFact.setDcNummer("D", dcNr, dagboekToExclude, boekstkToExclude);
				fldDebiteurFact.setDcNummer("D", dcNr);
			} else {
//				fldCrediteurFact.setDcNummer("C", dcNr, dagboekToExclude, boekstkToExclude);
				fldCrediteurFact.setDcNummer("C", dcNr);
			}
		}
	}
}