package nl.eadmin.ui.uiobjects.panels;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Collection;
import java.util.Iterator;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.databeans.BalansOverzichtDataBean;
import nl.eadmin.databeans.BalansRekeningDataBean;
import nl.eadmin.databeans.BalansRekeningSoortDataBean;
import nl.eadmin.databeans.GrootboekmutatieDatabean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.impl.RekeningManager_Impl;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.HTMLHelper;
import nl.eadmin.helpers.PDFHelper;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.eadmin.ui.uiobjects.windows.JournaalPostWindow;
import nl.eadmin.util.PrintWebExtension;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.BinaryAction;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.HeaderPanel;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.uiobjects.Tab;
import nl.ibs.esp.uiobjects.TabbedPanel;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.esp.util.Transformer;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class BalansOverzichtPanel extends Panel {
	private static final long serialVersionUID = 1L;
	// private boolean isMobileDevice;
	private Bedrijf bedrijf;
	private String overzichtTitle;
	private BalansOverzichtDataBean balansOverzicht;
	private DecimalToAmountTransformer dectrf = new DecimalToAmountTransformer();

	public BalansOverzichtPanel(String soort, int boekjaar, boolean excludeEmpty, CloseWindowAction closeAction) throws Exception {
		this.bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		this.balansOverzicht = new BalansOverzichtDataBean(overzichtTitle, bedrijf, getRekeningen(soort), boekjaar, excludeEmpty);
		this.overzichtTitle = "Balans_" + boekjaar;
		addUIObject(createTabbedPanel());
		addUIObject(createFloatBar(closeAction));
	}

	private Panel createTabbedPanel() throws Exception {
		Object[] rekeningSoorten = balansOverzicht.getRekeningSoort();
		TabbedPanel tabbedPnl = new TabbedPanel();
		for (int i = 0; i < rekeningSoorten.length; i++) {
			BalansRekeningSoortDataBean bean = (BalansRekeningSoortDataBean) rekeningSoorten[i];
			if (bean.getRekSoort() != null && bean.getRekSoort().trim().length() > 0) {
				tabbedPnl.add(createTab(bean));
			}
		}
		Panel panel = new Panel();
		panel.addUIObject(tabbedPnl);
		return panel;
	}

	private Tab createTab(BalansRekeningSoortDataBean bean) throws Exception {
		Tab tab = new Tab(bean.getRekSoort());
		CollectionTable tblRek = new CollectionTable(BalansRekeningDataBean.class, bean.getRekening(), ApplicationConstants.NUMBEROFTABLEROWS);
		tblRek.setName("BalansRekeningTabel");
		tblRek.setColumnNames(new String[] { BalansRekeningDataBean.REKNR, BalansRekeningDataBean.OMSCHR, BalansRekeningDataBean.DEBET, BalansRekeningDataBean.CREDIT });
		tblRek.setColumnLabels(new String[] { "Rekening", "Omschrijving", "Debet", "Credit" });
		tblRek.setColumnSizes(new short[] { 80, 300, 100, 100 });
		tblRek.setColumnTypes(new String[] { Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_DECIMAL, Field.TYPE_DECIMAL });
		tblRek.setDisplayTransformers(new Transformer[] { null, null, dectrf, dectrf });
		tblRek.setSingleSelectable();
		tblRek.addDownloadCSVContextAction("BalansRekeningen");
		tblRek.addDownloadPDFContextAction("BalansRekeningen");
		Action showMutaties = new Action("Toon mutaties") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				BalansRekeningDataBean bean = (BalansRekeningDataBean) GeneralHelper.getSelectedItem(tblRek, object);
				CollectionTable tblMut = new CollectionTable(GrootboekmutatieDatabean.class, bean.getGrootboekmutatieBeans(), ApplicationConstants.NUMBEROFTABLEROWS);
				tblMut.setName("BalansRekeningMutatieTabel");
				tblMut.setColumnNames(new String[] { GrootboekmutatieDatabean.BOEKDTM, GrootboekmutatieDatabean.DAGBOEK, GrootboekmutatieDatabean.BOEKSTUK, GrootboekmutatieDatabean.OMSCHR,
						GrootboekmutatieDatabean.DEBET, GrootboekmutatieDatabean.CREDIT });
				tblMut.setColumnLabels(new String[] { "Boekdatum", "Dagboek", "Boekstuk", "Omschrijving", "Debet", "Credit" });
				tblMut.setColumnSizes(new short[] { 80, 80, 100, 300, 100, 100 });
				tblMut.setColumnTypes(new String[] { Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_DECIMAL, Field.TYPE_DECIMAL });
				// tblMut.setDisplayTransformers(new Transformer[] { null, null,
				// null, null, null, dectrf });
				tblMut.setSingleSelectable();
				tblMut.addDownloadCSVContextAction("Rekeningmutaties");
				tblMut.addDownloadPDFContextAction("Rekeningmutaties");
				Action showJournaal = new Action("Toon journaalpost") {
					private static final long serialVersionUID = 1L;

					public boolean execute(DataObject object) throws Exception {
						GrootboekmutatieDatabean bean = (GrootboekmutatieDatabean) GeneralHelper.getSelectedItem(tblMut, object);
						Journaal journaal = bean.getJournaal();
						object.addUIObject(new JournaalPostWindow(journaal.getBedrijf(), journaal.getDagboekId(), journaal.getBoekstuk()));
						return true;
					}
				};
				tblMut.setRowAction(showJournaal);
				tblMut.reload();
				Window window = new Window();
				window.setLabel("Mutatieoverzicht rekening " + bean.getRekNr() + " - " + bean.getRekOmschrijving());
				window.add(tblMut);
				CloseWindowAction closeAction = new CloseWindowAction(window);
				closeAction.setLabel("Terug");
				closeAction.setIcon("arrow_left.png");
				FloatBar fb = new FloatBar();
				fb.addAction(closeAction);
				window.add(fb);
				object.addUIObject(window);
				return true;
			}
		};
		tblRek.setRowAction(showMutaties);
		tblRek.reload();
		ESPGridLayout totaalGrid = new ESPGridLayout();
		totaalGrid.setColumnWidths(new short[] { 200, 200 });
		totaalGrid.add(new Label("Totaal Debet: " + bean.getRekSoortTotaalDebet$()), 0, 0);
		totaalGrid.add(new Label("Totaal Credit: " + bean.getRekSoortTotaalCredit$()), 0, 1);
		if (bean.getRekSoort().startsWith(RekeningSoortEnum.RESULT)) {
			BigDecimal amt = bean.getRekSoortTotaalDebet().subtract(bean.getRekSoortTotaalCredit());
			String vw = amt.doubleValue() > 0d ? "Verlies" : "Winst";
			totaalGrid.add(new Label("Resultaat boekjaar: " + vw + " " + dectrf.transform(amt.abs())), 0, 2);
		}
		HeaderPanel hdrPanel = new HeaderPanel();
		hdrPanel.addUIObject(totaalGrid);
		Panel panel = new Panel();
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(new Label("Klik op ��n van onderstaande rekeningen om de bijbehorende mutaties te tonen"));
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(tblRek);
		panel.addUIObject(new Paragraph(""));
		panel.addUIObject(hdrPanel);
		tab.add(panel);
		return tab;
	}

	private FloatBar createFloatBar(CloseWindowAction closeAction) throws Exception {
		createActions();
		FloatBar fb = new FloatBar();
		fb.addAction(new PrintAction());
		fb.addAction(new BinaryPrintAction());
		fb.addAction(closeAction);
		return fb;
	}

	private void createActions() throws Exception {
	}

	private BinaryObject downloadPDF(DataObject object) throws Exception {
		byte[] bytes = PDFHelper.getBalansOverzichtPDF(balansOverzicht);
		BinaryObject obj = new BinaryObject(bytes, BinaryObject.TYPE_PDF, object);
		obj.setName(overzichtTitle + ".pdf");
		return obj;
	}

	private Collection<?> getRekeningen(String soort) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Rekening WHERE ");
		sb.append(Rekening.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' ");
		sb.append("ORDER BY " + Rekening.REKENING_NR);
		FreeQuery qry = QueryFactory.createFreeQuery((RekeningManager_Impl) RekeningManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		if (soort.equals("*")) { // Beide
			return qry.getCollection();
		} else {
			Collection<?> col = qry.getCollection();
			Iterator<?> iter = col.iterator();
			while (iter.hasNext()) {
				if (!((Rekening) iter.next()).getRekeningSoort().equals(soort)) {
					iter.remove();
				}
			}
			return col;
		}
	}

	public DataObject openPrintDialog(DataObject object) throws Exception {
		String html = HTMLHelper.getBalansOverzichtHTML(balansOverzicht);
		String name = "Balansoverzicht_" + new Date(System.currentTimeMillis());
		object.addUIObject(new PrintWebExtension(object.getScreen(), name, html));
		return object;
	}

	private class BinaryPrintAction extends BinaryAction {
		private static final long serialVersionUID = 1L;

		public BinaryPrintAction() throws Exception {
			super("Opslaan als PDF");
			setIcon("page_white_acrobat.png");
		}

		@Override
		public BinaryObject getBinaryObject(DataObject object) throws Exception {
			// return downloadHTML(object,periode);//Werk voor printen zelf,
			// maar niet handig mbt opslaan of versturen op mobile devices;
			return downloadPDF(object);
		}
	}

	private class PrintAction extends Action {
		private static final long serialVersionUID = 1L;

		public PrintAction() {
			super("Print");
			setIcon("printer.png");
		}

		public boolean execute(DataObject object) throws Exception {
			openPrintDialog(object);
			return true;
		};
	}

}