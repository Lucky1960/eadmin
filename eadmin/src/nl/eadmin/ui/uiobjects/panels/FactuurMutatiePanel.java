package nl.eadmin.ui.uiobjects.panels;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.ApplicationConstants;
//import nl.eadmin.databeans.DebCredLijstDataBean;
import nl.eadmin.databeans.DebCredOverzichtDataBean;
//import nl.eadmin.databeans.FacturenLijstDataBean;
import nl.eadmin.databeans.FactuurMutatieDataBean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.HTMLHelper;
import nl.eadmin.helpers.JournaalHelper;
import nl.eadmin.helpers.PDFHelper;
import nl.eadmin.ui.transformer.DatumTransformer;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.eadmin.ui.uiobjects.tables.HeaderDataKeyedTransformer;
import nl.eadmin.ui.uiobjects.windows.JournaalPostWindow;
import nl.eadmin.util.PrintWebExtension;
import nl.ibs.esp.dataobject.BinaryObject;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.CommonTable.SearchPanel;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Paragraph;
import nl.ibs.esp.util.Transformer;

public class FactuurMutatiePanel extends PrintablePanel {
	private static final long serialVersionUID = 1L;
	private DebCredOverzichtDataBean debCredOverzichtDataBean;
	private static DatumTransformer dateTrf = new DatumTransformer();
	private static DecimalToAmountTransformer decTrf = new DecimalToAmountTransformer();
	private static final String[] NAMES = new String[] { FactuurMutatieDataBean.BOEKDATUM, FactuurMutatieDataBean.DAGBOEK, FactuurMutatieDataBean.BOEKSTUK, FactuurMutatieDataBean.BEDRAG_D,
		FactuurMutatieDataBean.BEDRAG_C, FactuurMutatieDataBean.OMSCHR1, FactuurMutatieDataBean.OMSCHR2, FactuurMutatieDataBean.OMSCHR3 };
	private static final String[] LABELS = new String[] { "Boekdatum", "Dagboek", "Boekstuk", "Bedrag D", "Bedrag C", "Omschr.1", "Omschr.2", "Omschr.3" };
	private static final String[] TYPES = new String[] { Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_DECIMAL, Field.TYPE_DECIMAL, Field.TYPE_TEXT, Field.TYPE_TEXT, Field.TYPE_TEXT };
	private static final short[] SIZES = new short[] { 100, 100, 100, 80, 80, 100, 100, 100 };
	private static final Transformer[] TRANS = new Transformer[] { dateTrf, null, null, decTrf, decTrf, null, null, null };
	private String title;

	public FactuurMutatiePanel(Bedrijf bedrijf, String dc, String dcNr, String factuurNr, Action close) throws Exception {
		this.title = (dc.equals("D") ? "Debiteuren " : "Crediteuren ") + new Date(System.currentTimeMillis());
		this.debCredOverzichtDataBean = new DebCredOverzichtDataBean(bedrijf, dc, title);
		Collection<FactuurMutatieDataBean> col = new JournaalHelper(bedrijf.getDBData()).getSubBeans(bedrijf.getBedrijfscode(), dc, dcNr, factuurNr, null);
		CollectionTable table = new CollectionTable(FactuurMutatieDataBean.class, col, ApplicationConstants.NUMBEROFTABLEROWS);
		table.setName("FactuurMutatiePanel");
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnSizes(SIZES);
		table.setColumnTypes(TYPES);
		table.setDisplayTransformers(TRANS);
		table.setSortable(true);
		table.setSelectable(false);
		table.addDownloadCSVContextAction(title);
		table.addDownloadPDFContextAction(title);
		Map<String, InputComponent> myInputComponents = new HashMap<String, InputComponent>();
		myInputComponents.put(FactuurMutatieDataBean.BOEKDATUM, new DateSelectionField("Boekdatum", new Date(), "dd-MM-yyyy"));
		myInputComponents.put(FactuurMutatieDataBean.BEDRAG_D, new DecimalField(FactuurMutatieDataBean.BEDRAG_D, 15, 2, false));
		myInputComponents.put(FactuurMutatieDataBean.BEDRAG_C, new DecimalField(FactuurMutatieDataBean.BEDRAG_C, 15, 2, false));
		table.setInputComponents(myInputComponents);
		Action showAction = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				FactuurMutatieDataBean bean = (FactuurMutatieDataBean) GeneralHelper.getSelectedItem(table, object);
				object.addUIObject(new JournaalPostWindow(bedrijf.getBedrijfscode(), bean.getDagboek(), bean.getBoekstuk()));
				return true;
			}
		};
		table.setRowAction(showAction);
		table.reload();
		FloatBar fb = new FloatBar();
		fb.addAction(getPrintAction());
		fb.addAction(getDownloadAction());
		fb.addAction(close);
		SearchPanel searchPanel = table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true);
		searchPanel.setDefaultInputTransformer(new HeaderDataKeyedTransformer());
		addUIObject(new Paragraph(""));
		addUIObject(new Label("Klik op ��n van onderstaande mutaties om de bijbehorende journaalpost te tonen"));
		addUIObject(new Paragraph(""));
		addUIObject(searchPanel);
		addUIObject(table);
		addUIObject(fb);
	}

	protected BinaryObject downloadPDF(DataObject object) throws Exception {
		byte[] bytes = PDFHelper.getDebiteurenOverzichtPDF(debCredOverzichtDataBean);
		BinaryObject obj = new BinaryObject(bytes, BinaryObject.TYPE_PDF, object);
		obj.setName(title + ".pdf");
		obj.noSaveAsDialog();
		return obj;
	}

	public DataObject openPrintDialog(DataObject object) throws Exception {
		String html = HTMLHelper.getDebiteurenOverzichtHTML(debCredOverzichtDataBean);
		object.addUIObject(new PrintWebExtension(object.getScreen(), title, html));
		return object;
	}

	// private class BinaryPrintAction extends BinaryAction {
	// private static final long serialVersionUID = 1L;
	//
	// public BinaryPrintAction() throws Exception {
	// super("Print");
	// setIcon("print.png");
	// }
	//
	// @Override
	// public BinaryObject getBinaryObject(DataObject object) throws Exception {
	// // return downloadHTML(object,periode);//Werk voor printen zelf,
	// // maar niet handig mbt opslaan of versturen op mobile devices;
	// return downloadPDF(object);
	// }
	// }
	//
	// private class PrintAction extends Action {
	// private static final long serialVersionUID = 1L;
	//
	// public PrintAction() {
	// super("Print");
	// setIcon("printer.png");
	// }
	//
	// @Override
	// public boolean execute(DataObject object) throws Exception {
	// openPrintDialog(object);
	// return true;
	// };
	// }
	//
}
