package nl.eadmin.ui.uiobjects.panels;

import nl.eadmin.db.Adres;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.AdresPK;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.enums.AdresTypeEnum;
import nl.eadmin.ui.uiobjects.ComboLand;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FieldGroup;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.Panel;

public class AdresPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private Field fldStraat = new Field("", Field.TYPE_TEXT, "", "", 50);
	private Field fldHsnrToev = new Field("", Field.TYPE_TEXT, "", "", 2);
	private Field fldPostCode = new Field("", Field.TYPE_TEXT, "", "", 8);
	private Field fldPlaats = new Field("", Field.TYPE_TEXT, "", "", 50);
	private Field fldExtra = new Field("", Field.TYPE_TEXT, "", "", 50);
	private Field fldTelefoon = new Field("", Field.TYPE_TEXT, "", "", 15);
	private Field fldMobiel = new Field("", Field.TYPE_TEXT, "", "", 15);
	private NumberField fldHsnr = new NumberField("", 5, true);
	private ComboLand cbLand = (ComboLand)new ComboLand().setDiscardLabel(true);
	private Label lblStr = new Label("Straat/Huisnr.");
	private Label lblPlt = new Label("Postcode/Plaats");
	private Label lblTel1 = new Label("Telefoon");
	private Label lblTel2 = new Label("Mobiel");
	private Label lblExtra = new Label("  2e adresregel");
	private String dc;
	private int addressType;

	public AdresPanel(String dc, int addressType, int labelWidth) throws Exception {
		this.dc = dc;
		this.addressType = addressType;
		ESPGridLayout adresRegel1 = new ESPGridLayout();
		ESPGridLayout adresRegel2 = new ESPGridLayout();
		ESPGridLayout adresRegel3 = new ESPGridLayout();
		ESPGridLayout adresRegel4 = new ESPGridLayout();
		adresRegel1.setColumnWidths(new short[] { (short) labelWidth });
		adresRegel2.setColumnWidths(new short[] { (short) labelWidth });
		adresRegel3.setColumnWidths(new short[] { (short) labelWidth });
		adresRegel4.setColumnWidths(new short[] { (short) labelWidth });
		adresRegel1.setPaddingTop(0);
		adresRegel1.setPaddingBottom(0);
		adresRegel2.setPaddingTop(0);
		adresRegel2.setPaddingBottom(0);
		adresRegel3.setPaddingTop(0);
		adresRegel3.setPaddingBottom(0);
		adresRegel4.setPaddingTop(0);
		adresRegel4.setPaddingBottom(0);
		adresRegel1.add(lblStr, 0, 0);
		adresRegel1.add(fldStraat, 0, 1);
		adresRegel1.add(fldHsnr, 0, 2);
		adresRegel1.add(fldHsnrToev, 0, 3);
		adresRegel2.add(lblExtra, 0, 0);
		adresRegel2.add(fldExtra, 0, 1);
		adresRegel3.add(lblPlt, 0, 0);
		adresRegel3.add(fldPostCode, 0, 1);
		adresRegel3.add(fldPlaats, 0, 2);
		adresRegel3.add(cbLand, 0, 3);
		adresRegel4.add(lblTel1, 0, 0);
		adresRegel4.add(fldTelefoon, 0, 1);
		adresRegel4.add(lblTel2, 0, 2);
		adresRegel4.add(fldMobiel, 0, 3);
		FieldGroup adres1 = new FieldGroup(AdresTypeEnum.getValue(addressType));
		adres1.add(adresRegel1);
		adres1.add(adresRegel2);
		adres1.add(adresRegel3);
		if (addressType != AdresTypeEnum.ADRESTYPE_POST) {
			adres1.add(adresRegel4);
		}
		ESPGridLayout mainGrid = new ESPGridLayout();
		mainGrid.add(adres1, 0, 0);
		addUIObject(mainGrid);
	}

	public boolean isEmpty() throws Exception {
		boolean empty = true;
		// @formatter:off
		if (empty) empty = fldStraat.getValue().trim().length() == 0;
		if (empty) empty = fldHsnr.getIntValue() == 0;
		if (empty) empty = fldHsnrToev.getValue().trim().length() == 0;
		if (empty) empty = fldPostCode.getValue().trim().length() == 0;
		if (empty) empty = fldPlaats.getValue().trim().length() == 0;
		if (empty) empty = fldExtra.getValue().trim().length() == 0;
		if (empty) empty = fldTelefoon.getValue().trim().length() == 0;
		if (empty) empty = fldMobiel.getValue().trim().length() == 0;
		// @formatter:on
		return empty;
	}

	public void loadFrom(Bedrijf bedrijf, String dcNr) throws Exception {
		if (bedrijf == null) {
			fldStraat.setValue();
			fldHsnr.setValue();
			fldHsnrToev.setValue();
			fldPostCode.setValue();
			fldPlaats.setValue();
			fldExtra.setValue();
			fldTelefoon.setValue();
			fldMobiel.setValue();
			cbLand.setSelectedOptionValue("NL");
		} else {
			Adres adres = getAddress(bedrijf, dcNr);
			fldStraat.setValue(adres.getStraat());
			fldHsnr.setValue(adres.getHuisNr());
			fldHsnrToev.setValue(adres.getHuisNrToev());
			fldPostCode.setValue(adres.getPostcode());
			fldPlaats.setValue(adres.getPlaats());
			fldExtra.setValue(adres.getExtraAdresRegel());
			fldTelefoon.setValue(adres.getTelefoon());
			fldMobiel.setValue(adres.getMobiel());
			cbLand.setSelectedOptionValue(adres.getLand() == null || adres.getLand().length() == 0 ? "NL" : adres.getLand());
		}
	}

	private Adres getAddress(Bedrijf bedrijf, String dcNr) throws Exception {
		AdresPK key = new AdresPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setDc(dc);
		key.setDcNr(dcNr);
		key.setAdresType(addressType);
		return AdresManagerFactory.getInstance(bedrijf.getDBData()).findOrCreate(key);
	}

	public void saveTo(Bedrijf bedrijf, String dcNr) throws Exception {
		Adres adres = getAddress(bedrijf, dcNr);
		adres.setStraat(fldStraat.getValue());
		adres.setHuisNr(fldHsnr.getIntValue());
		adres.setHuisNrToev(fldHsnrToev.getValue());
		adres.setPostcode(fldPostCode.getValue());
		adres.setPlaats(fldPlaats.getValue());
		adres.setExtraAdresRegel(fldExtra.getValue());
		adres.setTelefoon(fldTelefoon.getValue());
		adres.setMobiel(fldMobiel.getValue());
		adres.setLand(cbLand.getSelectedOptionValue());
	}
}