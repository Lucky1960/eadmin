package nl.eadmin.ui.uiobjects;

import java.util.Locale;

import nl.ibs.esp.uiobjects.ComboBox;

public class ComboLand extends ComboBox {
	private static final long serialVersionUID = -2648663664722388837L;

	public ComboLand() throws Exception {
		super("Land");
		setWidth("100");

		@SuppressWarnings("static-access")
		Locale[] locales = new Locale("nl").getAvailableLocales();
		for (int i = 0; i < locales.length; i++) {
			addOption(locales[i].getDisplayCountry(new Locale("nl")), locales[i].getCountry());
		}
		setSelectedOptionValue("NL");
	}
}