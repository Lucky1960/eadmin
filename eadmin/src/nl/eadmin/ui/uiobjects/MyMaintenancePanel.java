package nl.eadmin.ui.uiobjects;

import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CommonTable;
import nl.ibs.esp.uiobjects.MaintenancePanel;
import nl.ibs.esp.uiobjects.UIObject;
import nl.ibs.esp.util.Editor;

@SuppressWarnings("rawtypes")
public class MyMaintenancePanel extends MaintenancePanel {
	private static final long serialVersionUID = -1896392587333668301L;
	private String scrollLeft, scrollTop;

	public MyMaintenancePanel(Class clss, String _textCodeSingleObject, String _textCodeMultipleObjects, CommonTable _table, Editor _editor, Action close) throws Exception {
		super(clss, _textCodeSingleObject, _textCodeMultipleObjects, _table, _editor, close);
		initialize();
	}

	public MyMaintenancePanel(Class clss, String textCodeSingleObject, String textCodeMultipleObjects, CommonTable table, UIObject mainTopPanel, Editor editor, Action close) throws Exception {
		super(clss, textCodeSingleObject, textCodeMultipleObjects, table, mainTopPanel, editor, close);
		initialize();
	}

	public MyMaintenancePanel(Class clss, String textCodeSingleObject, String textCodeMultipleObjects, Object object, boolean edit, Editor editor, Action close) throws Exception {
		super(clss, textCodeSingleObject, textCodeMultipleObjects, object, edit, editor, close);
		initialize();
	}

	private void initialize() throws Exception {
		scrollLeft = null;
		scrollTop = null;
	}

	protected void displayMainScreen() throws Exception {
		super.displayMainScreen();
		if (scrollTop != null) {
			getTable().setScrollLeft(scrollLeft);
			getTable().setScrollTop(scrollTop);
			scrollLeft = null;
			scrollTop = null;
		}
	}

	protected void displayEditOrDetailsScreen(boolean edit) throws Exception {
		super.displayEditOrDetailsScreen(edit);
		scrollLeft = getTable().getScrollLeft();
		scrollTop = getTable().getScrollTop();
	}
}