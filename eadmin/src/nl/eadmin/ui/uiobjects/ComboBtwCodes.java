package nl.eadmin.ui.uiobjects;

import java.util.Iterator;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCode;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.BtwCodePK;
import nl.eadmin.db.impl.BtwCodeManager_Impl;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.QueryFactory;

public class ComboBtwCodes extends ComboBox {
	private static final long serialVersionUID = 7515377577107856800L;
	private Bedrijf bedrijf;
	private DBData dbd;

	public ComboBtwCodes(Bedrijf bedrijf) throws Exception {
		super("BTW-code");
		this.bedrijf = bedrijf;
		this.dbd = bedrijf.getDBData();
		setWidth("120");
		boolean first = true;
		String firstOption = null;
		String filter = BtwCode.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "'";
		ExecutableQuery qry = QueryFactory.create((BtwCodeManager_Impl) BtwCodeManagerFactory.getInstance(dbd), filter);
		Iterator<?> iter = qry.getCollection().iterator();
		while (iter.hasNext()) {
			BtwCode btwCode = (BtwCode) iter.next();
			addOption(btwCode.getOmschrijving(), btwCode.getCode());
			if (first) {
				firstOption = btwCode.getCode();
				first = false;
			}
		}
		if (firstOption != null) {
			setSelectedOptionValue(firstOption);
		}
	}
	
	public BtwCode getMyObject() throws Exception {
		BtwCodePK key = new BtwCodePK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key.setCode(getSelectedOptionValue());
		try {
			return BtwCodeManagerFactory.getInstance(dbd).findByPrimaryKey(key);
		} catch (Exception e) {
			return null;
		}
	}
}