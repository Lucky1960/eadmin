package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.db.Adres;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Crediteur;
import nl.eadmin.db.CrediteurManager;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.Debiteur;
import nl.eadmin.db.DebiteurManager;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.ui.uiobjects.panels.DebCredHeaderPanel;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.RedirectAction;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.jsql.DBData;

public class RenumberDCWindow extends Window {
	private static final long serialVersionUID = 1L;

	public RenumberDCWindow(Bedrijf bedrijf, String dc, String oldDcNr, String returnPage, String returnMethod) throws Exception {
		final Window thisWdw = this;
		boolean isDeb = dc.equals("D");
		setLabel("Hernummeren " + (isDeb ? "Debiteur " : "Crediteur "));
		Field fldDcNr = new Field("Nieuw nummer");
		fldDcNr.setLength(10);
		fldDcNr.setMaxLength(10);
		fldDcNr.setValue(oldDcNr);
		Panel pnl = new Panel();
		pnl.addUIObject(fldDcNr);
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				String newDcNr = fldDcNr.getValue();
				if (oldDcNr.equals(newDcNr)) {
					thisWdw.closeWindow(object);
					return true;
				}
				DBData dbd = bedrijf.getDBData();
				String set, where;
				DebiteurManager debMgr = DebiteurManagerFactory.getInstance(dbd);
				CrediteurManager credMgr = CrediteurManagerFactory.getInstance(dbd);
				boolean exists = false;
				if (isDeb) {
					DebiteurPK key = new DebiteurPK();
					key.setBedrijf(bedrijf.getBedrijfscode());
					key.setDebNr(newDcNr);
					try {
						debMgr.findByPrimaryKey(key);
						exists = true;
					} catch (Exception e) {
					}
				} else {
					CrediteurPK key = new CrediteurPK();
					key.setBedrijf(bedrijf.getBedrijfscode());
					key.setCredNr(newDcNr);
					try {
						credMgr.findByPrimaryKey(key);
						exists = true;
					} catch (Exception e) {
					}
				}
				if (exists) {
					throw new UserMessageException("Dit nummer bestaat reeds!");
				}

				set = Adres.DC_NR + "='" + newDcNr + "'";
				where = Adres.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Adres.DC + "='" + dc + "' AND " + Adres.DC_NR + "='" + oldDcNr + "'";
				AdresManagerFactory.getInstance(dbd).generalUpdate(set, where);

				set = DetailData.DC_NUMMER + "='" + newDcNr + "'";
				where = DetailData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + DetailData.DC_NUMMER + "='" + oldDcNr + "'";
				DetailDataManagerFactory.getInstance(dbd).generalUpdate(set, where);

				set = HeaderData.DC_NUMMER + "='" + newDcNr + "'";
				where = HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + HeaderData.DC_NUMMER + "='" + oldDcNr + "'";
				HeaderDataManagerFactory.getInstance(dbd).generalUpdate(set, where);

				set = Journaal.DC_NUMMER + "='" + newDcNr + "'";
				where = Journaal.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Journaal.DC_NUMMER + "='" + oldDcNr + "'";
				JournaalManagerFactory.getInstance(dbd).generalUpdate(set, where);

				if (isDeb) {
					set = Debiteur.DEB_NR + "='" + newDcNr + "'";
					where = Debiteur.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Debiteur.DEB_NR + "='" + oldDcNr + "'";
					debMgr.generalUpdate(set, where);
				} else {
					set = Crediteur.CRED_NR + "='" + newDcNr + "'";
					where = Crediteur.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Crediteur.CRED_NR + "='" + oldDcNr + "'";
					credMgr.generalUpdate(set, where);
				}
				thisWdw.closeWindow(object);
				object.addUIObject(new RedirectAction(returnPage, returnMethod));
				return super.execute(object);
			}
		}.setIcon("tick.png");
		FloatBar fb = new FloatBar();
		fb.addAction(ok);
		fb.addAction(new CloseWindowAction(this));
		add(new DebCredHeaderPanel(bedrijf.getBedrijfscode(), oldDcNr, dc, true, null, null));
		add(pnl);
		add(fb);
	}
}
