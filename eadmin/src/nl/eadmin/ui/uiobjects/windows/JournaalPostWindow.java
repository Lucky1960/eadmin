package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.ui.uiobjects.panels.JournaalPostPanel;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Window;

public class JournaalPostWindow extends Window {
	private static final long serialVersionUID = 1L;

	public JournaalPostWindow(String bedrijf, String dagboek, String boekstuk) throws Exception {
		setLabel("Journaalpost van boekstuk " + boekstuk + " uit dagboek '" + dagboek + "'");
		Action back = new Action("Terug") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				closeWindow(object);
				return true;
			}
		}.setIcon("arrow_left.png");
		FloatBar floatbar = new FloatBar();
		floatbar.addAction(back);
		add(new JournaalPostPanel(bedrijf, dagboek, boekstuk));
		add(floatbar);
	}
}