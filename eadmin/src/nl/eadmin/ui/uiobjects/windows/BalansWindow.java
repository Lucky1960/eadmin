package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.ui.uiobjects.panels.BalansSelectiePanel;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.Window;


public class BalansWindow extends Window {
	private static final long serialVersionUID = 1L;

	public BalansWindow() throws Exception {
		setLabel("Balansoverzicht");
		add(new BalansSelectiePanel(new CloseWindowAction(this)));
	}
}
