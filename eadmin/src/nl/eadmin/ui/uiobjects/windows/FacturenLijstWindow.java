package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.selection.DebCredLijstSelection;
import nl.eadmin.ui.uiobjects.panels.DebCredHeaderPanel;
import nl.eadmin.ui.uiobjects.panels.FacturenLijstPanel;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.Window;


public class FacturenLijstWindow extends Window {
	private static final long serialVersionUID = 1L;

	public FacturenLijstWindow(Bedrijf bedrijf, String dc, String dcNr, DebCredLijstSelection selection) throws Exception {
		setLabel("Facturenlijst");
		add(new DebCredHeaderPanel(bedrijf.getBedrijfscode(), dcNr, dc, true, null, null));
		add(new FacturenLijstPanel(bedrijf, dc, dcNr, selection, new CloseWindowAction(this)));
	}
}