package nl.eadmin.ui.uiobjects.windows;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.eadmin.databeans.IcpOverzichtDataBean;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.impl.JournaalManager_Impl;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.enums.RekeningIcpSoortEnum;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.eadmin.ui.uiobjects.panels.SelecteerPeriodePanel;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.esp.util.Transformer;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class IcpOverzichtWindow extends Window {
	private static final long serialVersionUID = 1L;
	private SelecteerPeriodePanel selectPnl;
	private CollectionTable table;

	public IcpOverzichtWindow() throws Exception {
		final Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		table = new CollectionTable(IcpOverzichtDataBean.class, getCollection(bedrijf, 0, 0, 0, 0), ApplicationConstants.NUMBEROFTABLEROWS);
		table.setName("IcpOverzicht");
		table.setColumnNames(new String[] { IcpOverzichtDataBean.LEV_DIENST, IcpOverzichtDataBean.FACTNR, IcpOverzichtDataBean.DEBNR, IcpOverzichtDataBean.BTWNR, IcpOverzichtDataBean.DEBNAAM,
				IcpOverzichtDataBean.ADRES_RGL1, IcpOverzichtDataBean.ADRES_RGL2, IcpOverzichtDataBean.LAND, IcpOverzichtDataBean.BEDRAG_EXCL });
		table.setColumnLabels(new String[] { "Lev/Dienst", "FactuurNr", "DebiteurNr", "BtwNr", "Naam", "Adresregel-1", "Adresregel-2", "Land", "Bedrag" });
		table.setColumnSizes(new short[] { 70, 80, 80, 100, 200, 200, 200, 50, 80 });
		table.setDisplayTransformers(new Transformer[]{null, null, null, null, null, null, null, null, new DecimalToAmountTransformer()});
		table.addDownloadCSVContextAction("IcpOverzicht");
		table.addDownloadPDFContextAction("IcpOverzicht");
		table.setSelectable(false);
		table.setRowAction(null);
		table.reload();
		FloatBar floatbar = new FloatBar();
		floatbar.addAction(new Action("Toon overzicht") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				selectPnl.getBoekjaar1();
				table.setNewCollection(IcpOverzichtDataBean.class, getCollection(bedrijf, selectPnl.getBoekjaar1(), selectPnl.getPeriode1(), selectPnl.getBoekjaar2(), selectPnl.getPeriode2()),
					ApplicationConstants.NUMBEROFTABLEROWS);
				table.reload();
				return true;
			}
		});
		floatbar.addAction(new CloseWindowAction(this));
		setLabel("ICP overzicht");
		ESPGridLayout grid = new ESPGridLayout();
		grid.add(selectPnl = new SelecteerPeriodePanel(bedrijf, true), 0, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_CENTER);
		grid.add(floatbar, 0, 1, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_CENTER);
		add(grid);
		add(table);
	}

	private Collection<IcpOverzichtDataBean> getCollection(Bedrijf bedrijf, int boekjaar1, int periode1, int boekjaar2, int periode2) throws Exception {
		//JournaalHelper.createJournals(bedrijf, boekjaar1, periode1, boekjaar2, periode2);
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ICPSOORT, FACTUUR, DCNR, SUM(BEDRAGDEB), SUM(BEDRAGCRED) FROM JOURNAAL");
		sb.append(" WHERE BDR = '" + bedrijf.getBedrijfscode() + "'");
		sb.append("   AND ICPSOORT <> ''");
		sb.append("   AND DAGBOEKSRT = '" + DagboekSoortEnum.VERKOOP + "'");
		sb.append("   AND ((BOEKJAAR = " + boekjaar1 + " AND PERIODE >= " + periode1 + ") OR BOEKJAAR > " + boekjaar1 + ")");
		sb.append("   AND ((BOEKJAAR = " + boekjaar2 + " AND PERIODE <= " + periode2 + ") OR BOEKJAAR < " + boekjaar2 + ")");
		sb.append(" GROUP BY ICPSOORT, FACTUUR, DCNR");
		sb.append(" ORDER BY ICPSOORT, FACTUUR, DCNR");
		Collection<IcpOverzichtDataBean> beans = new ArrayList<IcpOverzichtDataBean>();

		FreeQuery qry = QueryFactory.createFreeQuery((JournaalManager_Impl) JournaalManagerFactory.getInstance(bedrijf.getDBData()), sb.toString());
		Object[][] maxRslt = qry.getResultArray();
		String debNr, factNr, levDienst;
		BigDecimal amount1, amount2;
		if (maxRslt.length > 0 && maxRslt[0].length > 0) {
			for (int i = 0; i < maxRslt.length; i++) {
				levDienst = (maxRslt[i][0] == null) ? "" : ((String) maxRslt[i][0]);
				factNr = (maxRslt[i][1] == null) ? "" : ((String) maxRslt[i][1]);
				debNr = (maxRslt[i][2] == null) ? "" : ((String) maxRslt[i][2]);
				amount1 = (maxRslt[i][3] == null) ? BigDecimal.ZERO : ((BigDecimal) maxRslt[i][3]);
				amount2 = (maxRslt[i][4] == null) ? BigDecimal.ZERO : ((BigDecimal) maxRslt[i][4]);
				beans.add(new IcpOverzichtDataBean(bedrijf.getBedrijfscode(), debNr, factNr, amount1.add(amount2), RekeningIcpSoortEnum.getValue(levDienst)));
			}
		}
		return beans;
	}
}