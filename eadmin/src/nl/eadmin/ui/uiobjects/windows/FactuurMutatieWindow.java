package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.ui.uiobjects.panels.DebCredHeaderPanel;
import nl.eadmin.ui.uiobjects.panels.FactuurMutatiePanel;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.Window;


public class FactuurMutatieWindow extends Window {
	private static final long serialVersionUID = 1L;

	public FactuurMutatieWindow(Bedrijf bedrijf, String dc, String dcNr, String factuurNr) throws Exception {
		setLabel("FactuurMutaties");
		add(new DebCredHeaderPanel(bedrijf.getBedrijfscode(), dcNr, dc, true, null, null));
		add(new FactuurMutatiePanel(bedrijf, dc, dcNr, factuurNr, new CloseWindowAction(this)));
	}
}