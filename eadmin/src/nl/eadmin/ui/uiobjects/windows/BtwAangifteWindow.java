package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.ui.uiobjects.panels.BtwAangiftePanel;
import nl.eadmin.ui.uiobjects.panels.SelecteerPeriodePanel;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Window;

public class BtwAangifteWindow extends Window {
	private static final long serialVersionUID = 1L;
	private BtwAangiftePanel btwAangiftePanel;
	private SelecteerPeriodePanel selectPnl;

	public BtwAangifteWindow() throws Exception {
		final Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		FloatBar floatbar = new FloatBar();
		floatbar.addAction(new Action("Toon overzicht") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				BtwAangiftePanel newBtwAangiftePanel = new BtwAangiftePanel(bedrijf, selectPnl.getBoekjaar1(), selectPnl.getPeriode1(), selectPnl.getBoekjaar2(), selectPnl.getPeriode2());
				object.getScreen().replaceUIObject(btwAangiftePanel, newBtwAangiftePanel);
				btwAangiftePanel = newBtwAangiftePanel;
				return true;
			}
		});
		floatbar.addAction(new CloseWindowAction(this));
		setLabel("BTW aangifte");
		ESPGridLayout grid = new ESPGridLayout();
		grid.add(selectPnl = new SelecteerPeriodePanel(bedrijf, true), 0, 0, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_CENTER);
		grid.add(floatbar, 0, 1, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_CENTER);
		add(grid);
//		add(selectPnl = new SelecteerPeriodePanel(bedrijf, true));
//		add(floatbar);
		add(btwAangiftePanel = new BtwAangiftePanel(bedrijf, selectPnl.getBoekjaar1(), selectPnl.getPeriode1(), selectPnl.getBoekjaar2(), selectPnl.getPeriode2()));
	}

}
