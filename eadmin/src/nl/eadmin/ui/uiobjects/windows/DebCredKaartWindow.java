package nl.eadmin.ui.uiobjects.windows;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.databeans.DebCredKaartDataBean;
import nl.eadmin.databeans.DebCredKaartMutatieDatabean;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.ui.transformer.DatumTransformer;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.eadmin.ui.uiobjects.panels.DebCredHeaderPanel;
import nl.eadmin.ui.uiobjects.tables.HeaderDataKeyedTransformer;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.layout.ESPGridLayout;
import nl.ibs.esp.layout.ESPGridLayoutConstraints;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.CollectionTable;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.HeaderPanel;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.Label;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.esp.uiobjects.CommonTable.SearchPanel;
import nl.ibs.esp.util.Transformer;

public class DebCredKaartWindow extends Window {
	private static final long serialVersionUID = 1L;
	private static DecimalToAmountTransformer trf = new DecimalToAmountTransformer();
	private static final String[] NAMES = new String[] { DebCredKaartMutatieDatabean._BOEKDATUM, DebCredKaartMutatieDatabean._DAGBOEK, DebCredKaartMutatieDatabean._BOEKSTUK, DebCredKaartMutatieDatabean._FACTUUR,
		DebCredKaartMutatieDatabean._OMSCHR, DebCredKaartMutatieDatabean._DEBET, DebCredKaartMutatieDatabean._CREDIT };
	private static final String[] LABELS = new String[] { "Datum", "Dagboek", "Boekstuk", "Factuur", "Omschrijving", "D", "C" };
	private static final short[] SIZES = new short[] { 80, 80, 100, 100, 250, 80, 80 };
	private static final Transformer[] TRANS = new Transformer[] { new DatumTransformer(), null, null, null, null, trf, trf };
	private CollectionTable table;

	public DebCredKaartWindow(String bedrijfsCode, String dcNr, String dc) throws Exception {
		Action journaalPost = new Action("Journaalpost") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				DebCredKaartMutatieDatabean bean = (DebCredKaartMutatieDatabean) GeneralHelper.getSelectedItem(table, object);
				table.unSelect(bean);
				object.addUIObject(new JournaalPostWindow(bedrijfsCode, bean.getDagboek(), bean.getBoekstuk()));
				return true;
			}
		}.setIcon("sum.png");
		DebCredKaartDataBean bean = new DebCredKaartDataBean(bedrijfsCode, dcNr, dc);
		String title = (dc.equals("D") ? "Debiteurenkaart: " : "Crediteurenkaart: ") + bean.getDcNr() + " - " + bean.getDcNaam();
		table = new CollectionTable(DebCredKaartMutatieDatabean.class, bean.getMutaties(), ApplicationConstants.NUMBEROFTABLEROWS);
		table.setName(dc.equals("D") ? "DebiteurenKaart" : "CrediteurenKaart");
		table.setColumnNames(NAMES);
		table.setColumnLabels(LABELS);
		table.setColumnSizes(SIZES);
		table.setDisplayTransformers(TRANS);
		table.setSortable(true);
		table.setSingleSelect(true);
		table.setSortable(false);
		table.addDownloadCSVContextAction(title);
		table.addDownloadPDFContextAction(title);
		Map<String, InputComponent> myInputComponents = new HashMap<String, InputComponent>();
		myInputComponents.put(DebCredKaartMutatieDatabean._BOEKDATUM, new DateSelectionField("Boekdatum", new Date(), "dd-MM-yyyy"));
		myInputComponents.put(DebCredKaartMutatieDatabean._DEBET, new DecimalField(DebCredKaartMutatieDatabean._DEBET, 15, 2, false));
		myInputComponents.put(DebCredKaartMutatieDatabean._CREDIT, new DecimalField(DebCredKaartMutatieDatabean._CREDIT, 15, 2, false));
		table.setInputComponents(myInputComponents);
		table.setRowAction(journaalPost);
		table.reload();
		FloatBar floatbar = new FloatBar();
		floatbar.addAction(journaalPost);
		floatbar.addAction(new CloseWindowAction(this));
		setLabel(dc.equals("D") ? "DebiteurenKaart" : "CrediteurenKaart");
		ESPGridLayout saldoGrid = new ESPGridLayout();
		saldoGrid.setColumnWidths(new short[] { 100, 80, 100, 80 });
		saldoGrid.add(new Label("Totaal Debet: "), 0, 0);
		saldoGrid.add(new Label((String) trf.transform(bean.getTotaalDebet())), 0, 1, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
		saldoGrid.add(new Label("Totaal Credit: "), 0, 2);
		saldoGrid.add(new Label((String) trf.transform(bean.getTotaalCredit())), 0, 3, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
		if (bean.getSaldoDebet().doubleValue() != 0d) {
			saldoGrid.add(new Label("Saldo: "), 1, 0);
			saldoGrid.add(new Label((String) trf.transform(bean.getSaldoDebet())), 1, 1, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
		} else {
			saldoGrid.add(new Label("Saldo: "), 1, 2);
			saldoGrid.add(new Label((String) trf.transform(bean.getSaldoCredit())), 1, 3, 1, 1, ESPGridLayoutConstraints.GRID_FILL_NONE, ESPGridLayoutConstraints.GRID_ANCHOR_EAST);
		}
		HeaderPanel saldoPanel = new HeaderPanel();
		saldoPanel.addUIObject(saldoGrid);
		Panel myPanel = new Panel();
		myPanel.addUIObject(new DebCredHeaderPanel(bedrijfsCode, dcNr, dc, true, bean.getSaldoDebet(), bean.getSaldoCredit()));
		SearchPanel searchPanel = table.createSearchWithFilterPanel(NAMES, LABELS, NAMES, LABELS, true);
		searchPanel.setDefaultInputTransformer(new HeaderDataKeyedTransformer());
		myPanel.addUIObject(searchPanel);
		myPanel.addUIObject(table);
		myPanel.addUIObject(saldoPanel);
		myPanel.addUIObject(floatbar);
		add(myPanel);
	}
}