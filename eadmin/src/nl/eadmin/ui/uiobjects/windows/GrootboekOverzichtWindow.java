package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.ui.uiobjects.panels.GrootboekOverzichtPanel;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.Window;


public class GrootboekOverzichtWindow extends Window {
	private static final long serialVersionUID = 1L;
	
	public GrootboekOverzichtWindow() throws Exception {
		setLabel("Grootboekoverzicht");
		add(new GrootboekOverzichtPanel(new CloseWindowAction(this)));
	}
}
