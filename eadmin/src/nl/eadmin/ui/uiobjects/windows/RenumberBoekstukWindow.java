package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.JournaalManagerFactory;
import nl.eadmin.db.impl.HeaderDataManager_Impl;
import nl.ibs.esp.dataobject.DataObject;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FloatBar;
import nl.ibs.esp.uiobjects.Panel;
import nl.ibs.esp.uiobjects.RedirectAction;
import nl.ibs.esp.uiobjects.UserMessageException;
import nl.ibs.esp.uiobjects.Window;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;

public class RenumberBoekstukWindow extends Window {
	private static final long serialVersionUID = 1L;

	public RenumberBoekstukWindow(Bedrijf bedrijf, String dagboek, String boekstuk, String returnPage, String returnMethod) throws Exception {
		final Window thisWdw = this;
		setLabel("Hernummeren boekstuk '" + boekstuk + "'");
		Field fldBoekstuk = new Field("Nieuw boekstuknummer");
		fldBoekstuk.setLength(15);
		fldBoekstuk.setMaxLength(15);
		fldBoekstuk.setValue(boekstuk);
		Panel pnl = new Panel();
		pnl.addUIObject(fldBoekstuk);
		Action ok = new Action("OK") {
			private static final long serialVersionUID = 1L;

			public boolean execute(DataObject object) throws Exception {
				String newBoekstuk = fldBoekstuk.getValue();
				if (boekstuk.equals(newBoekstuk)) {
					thisWdw.closeWindow(object);
					return true;
				}
				DBData dbd = bedrijf.getDBData();
				String set, where;
				StringBuffer sb = new StringBuffer();
				sb.append("SELECT * FROM HeaderData WHERE ");
				sb.append(HeaderData.BEDRIJF + "='" + bedrijf + "' AND ");
				sb.append(HeaderData.DAGBOEK_ID + "='" + dagboek + "' AND ");
				sb.append(HeaderData.BOEKSTUK + "='" + boekstuk + "' ");
				FreeQuery qry = QueryFactory.createFreeQuery((HeaderDataManager_Impl) HeaderDataManagerFactory.getInstance(dbd), sb.toString());
				if ((HeaderData) qry.getFirstObject() != null) {
					throw new UserMessageException("Dit nummer bestaat reeds!");
				}

				set = Bijlage.BOEKSTUK + "='" + newBoekstuk + "'";
				where = Bijlage.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Bijlage.DAGBOEK_ID + "='" + dagboek + "' AND " + Bijlage.BOEKSTUK + "='" + boekstuk + "'";
				BijlageManagerFactory.getInstance(dbd).generalUpdate(set, where);

				set = DetailData.BOEKSTUK + "='" + newBoekstuk + "'";
				where = DetailData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + DetailData.DAGBOEK_ID + "='" + dagboek + "' AND " + DetailData.BOEKSTUK + "='" + boekstuk + "'";
				DetailDataManagerFactory.getInstance(dbd).generalUpdate(set, where);

				set = HeaderData.BOEKSTUK + "='" + newBoekstuk + "'";
				where = HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + HeaderData.DAGBOEK_ID + "='" + dagboek + "' AND " + HeaderData.BOEKSTUK + "='" + boekstuk + "'";
				HeaderDataManagerFactory.getInstance(dbd).generalUpdate(set, where);

				set = Journaal.BOEKSTUK + "='" + newBoekstuk + "'";
				where = Journaal.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + Journaal.DAGBOEK_ID + "='" + dagboek + "' AND " + Journaal.BOEKSTUK + "='" + boekstuk + "'";
				JournaalManagerFactory.getInstance(dbd).generalUpdate(set, where);

				thisWdw.closeWindow(object);
				object.addUIObject(new RedirectAction(returnPage, returnMethod));
				return super.execute(object);
			}
		}.setIcon("tick.png");
		FloatBar fb = new FloatBar();
		fb.addAction(ok);
		fb.addAction(new CloseWindowAction(this));
		add(pnl);
		add(fb);
	}
}
