package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.ui.uiobjects.panels.JournaalPostOvzPanel;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.Window;


public class JournaalPostOvzWindow extends Window {
	private static final long serialVersionUID = 1L;

	public JournaalPostOvzWindow() throws Exception {
		setLabel("Overzicht journaalposten");
		add(new JournaalPostOvzPanel(new CloseWindowAction(this)));
	}
}