package nl.eadmin.ui.uiobjects.windows;

import nl.eadmin.SessionKeys;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.ui.uiobjects.panels.DebCredLijstPanel;
import nl.ibs.esp.servlet.ESPSessionContext;
import nl.ibs.esp.uiobjects.CloseWindowAction;
import nl.ibs.esp.uiobjects.Window;

public class DebCredLijstWindow extends Window {
	private static final long serialVersionUID = 1L;

	public DebCredLijstWindow(String dc) throws Exception {
		Bedrijf bedrijf = (Bedrijf)ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
		setLabel(dc.equals("D") ? "Debiteurenoverzicht" : "Crediteurenoverzicht");
		add(new DebCredLijstPanel(bedrijf, dc, new CloseWindowAction(this)));
	}
}