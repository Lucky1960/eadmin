package nl.eadmin.ui.uiobjects;

import nl.eadmin.db.Gebruiker;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.ibs.esp.event.EventListener;
import nl.ibs.esp.uiobjects.ReferenceField;
import nl.ibs.esp.uiobjects.UIObject;
import nl.sf.api.UserData;

public class MyUserReferenceField extends ReferenceField {
	private static final long serialVersionUID = -4879970420451150684L;
	private MyListener myListener = new MyListener();

	public MyUserReferenceField(String label) throws Exception {
		super(label);
		setLength(40);
		setDescriptionLength(60);
		setPlaceholder("<gebruikerscode of email-adres>");
		addOnChangeListener(myListener);
	}

	private class MyListener implements EventListener {
		private static final long serialVersionUID = 1L;

		public void event(UIObject object, String type) throws Exception {
			removeInvalidTag();
			String userIdOrEmail = getValue();
			if (userIdOrEmail == null || userIdOrEmail.trim().length() == 0) {
				setDescription("");
				return;
			}
			try {
				Gebruiker gebruiker = GebruikerManagerFactory.getInstance().findByPrimaryKey(userIdOrEmail);
				setDescription(gebruiker.getNaam());
				return;
			} catch (Exception e) {
			}
			UserData userData = GeneralHelper.getUserData(userIdOrEmail);
			if (userData != null) {
				setDescription(userData.getFirstName().trim() + " " + userData.getLastName().trim());
			} else {
				setInvalidTag();
				setDescription("<Dit is geen geregistreerde gebruiker>");
			}
		}
	}
}