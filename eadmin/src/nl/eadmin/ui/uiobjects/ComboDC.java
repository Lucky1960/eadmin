package nl.eadmin.ui.uiobjects;

import nl.eadmin.db.Dagboek;
import nl.ibs.esp.uiobjects.ComboBox;

public class ComboDC extends ComboBox {
	private static final long serialVersionUID = -5791132662173639797L;

	public ComboDC(Dagboek dagboek, boolean isHeader) throws Exception {
		super("");
		setWidth("100");
		if (dagboek.isBankboek() && isHeader) {
			setTranslatedLabel("Af/Bij");
			addOption("C (Af)", "C");
			addOption("D (Bij)", "D");
		} else if (dagboek.isBankboek() && !isHeader) {
			setTranslatedLabel("Af/Bij");
			addOption("D (Af)", "D");
			addOption("C (Bij)", "C");
		} else if (dagboek.isKasboek()) {
			setTranslatedLabel("Uitg/Ontv");
			addOption("Uitgave", "C");
			addOption("Ontvangst", "D");
		} else {
			setTranslatedLabel("D/C");
			addOption("Debet", "D");
			addOption("Credit", "C");
		}
	}
}