package nl.eadmin.ui.uiobjects;

import java.awt.event.KeyEvent;

import nl.ibs.esp.adapter.Adapter;
import nl.ibs.esp.uiobjects.Action;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.FloatBar;

/**
 * @author c_hc01
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LoginScreen extends nl.ibs.esp.uiobjects.LoginPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8998685243762574460L;
	private Field fldUser = new Field("username", Field.TYPE_TEXT, getClass().getName()+"."+"username");
	private Field fldPassword = new Field("password", Field.TYPE_PASSWORD, getClass().getName()+"."+"password");

	public LoginScreen(Adapter adapter) {
		setHeader("login-caption");
		addField(fldUser);
		addField(fldPassword);
		setVersionInfo("test test test");
		FloatBar bar = new FloatBar();
		Action login = new Action("login-login", adapter, "login");
		login.setMnemonic(KeyEvent.VK_L);
		login.setDefault(true);
		bar.addAction(login);
		addFloatBar(bar);
	}

	public String getUser() {
		return fldUser.getValue().trim();
	}

	public String getPassword() {
		return fldPassword.getValue().trim();
	}

	public void setUser(String user)throws Exception {
		fldUser.setValue(user);
	}

	public void setPassword(String password) throws Exception{
		fldPassword.setValue(password);
	}

	public Field getPasswordField() {
		return fldPassword;
	}

	public Field getUserField() {
		return fldUser;
	}

}
