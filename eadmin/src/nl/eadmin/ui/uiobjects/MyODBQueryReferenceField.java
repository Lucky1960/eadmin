package nl.eadmin.ui.uiobjects;

import java.util.Collection;

import nl.ibs.esp.uiobjects.ODBQueryReferenceField;
import nl.ibs.jsql.ExecutableQuery;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.jsql.sql.BusinessObjectManager;
import nl.ibs.vegas.persistence.Manager;

public class MyODBQueryReferenceField extends ODBQueryReferenceField {
	private static final long serialVersionUID = -8983331851067991530L;

	public MyODBQueryReferenceField(Manager mgr) throws Exception {
		super(mgr);
	}

	public void hideIfOnlyOneOption(String filter) throws Exception {
		ExecutableQuery qry = QueryFactory.create((BusinessObjectManager) getRelationManager(), filter);
		qry.setMaxObjects(2);
		Collection<?> col = qry.getCollection();
		if (col.size() == 1) {
			setSelectedObject(col.iterator().next());
			setReadonly(true);
		} else {
			setReadonly(false);
		}
	}
}