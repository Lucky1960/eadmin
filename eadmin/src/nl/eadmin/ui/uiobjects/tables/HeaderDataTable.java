package nl.eadmin.ui.uiobjects.tables;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.databeans.VrijeRubriekDefinitie;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.HeaderData;
import nl.eadmin.enums.EnumHelper;
import nl.eadmin.enums.HeaderDataStatusEnum;
import nl.eadmin.enums.VrijeRubriekSoortEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.VrijeRubriekHelper;
import nl.eadmin.query.HeaderDataQuery;
import nl.eadmin.ui.datareaders.HeaderDataReader;
import nl.eadmin.ui.transformer.BijlageTransformer;
import nl.eadmin.ui.transformer.DatumTransformer;
import nl.eadmin.ui.transformer.DebCredTransformer;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.eadmin.ui.transformer.HeaderDataStatusTransformer;
import nl.eadmin.ui.transformer.VrijeRubriekTransformer;
import nl.eadmin.ui.uiobjects.ComboDC;
import nl.ibs.esp.uiobjects.CheckBox;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.CommonObjectDataReader;
import nl.ibs.esp.util.ODBCalculatorImpl;
import nl.ibs.esp.util.Transformer;

public class HeaderDataTable extends ODBTable {
	private static final long serialVersionUID = -7711629080534758729L;
	private static String[] keys = new String[] { HeaderData.BEDRIJF, HeaderData.DAGBOEK_ID, HeaderData.BOEKSTUK };
	private Dagboek dagboek;
	private HeaderDataQuery query;
	private String[] filterNames;
	private String[] filterLabels;
	private DecimalToAmountTransformer amtTransformer;
	private DatumTransformer dtmTransformer;
	private DebCredTransformer dcTransformer;
	private BijlageTransformer bijlageTransformer;
	private HeaderDataStatusTransformer stsTransformer;
	private boolean favoriteOnly;
	private int decAmount;

	public HeaderDataTable(CommonObjectDataReader reader, Bedrijf bedrijf, Dagboek dagboek, int boekjaar) throws Exception {
		this(reader, new HeaderDataQuery(dagboek), bedrijf, dagboek, boekjaar, false);
	}

	public HeaderDataTable(CommonObjectDataReader reader, Bedrijf bedrijf, Dagboek dagboek, int boekjaar, boolean favoriteOnly) throws Exception {
		this(reader, new HeaderDataQuery(dagboek), bedrijf, dagboek, boekjaar, favoriteOnly);
	}

	private HeaderDataTable(CommonObjectDataReader reader, HeaderDataQuery query, Bedrijf bedrijf, Dagboek dagboek, int boekjaar, boolean favoriteOnly) throws Exception {
		super(reader, query, keys, ApplicationConstants.NUMBEROFTABLEROWS);
		this.favoriteOnly = favoriteOnly;
		StringBuilder filter = new StringBuilder();
		filter.append(HeaderData.BEDRIJF + "='" + bedrijf.getBedrijfscode() + "' AND " + HeaderData.BOEKJAAR + "=" + boekjaar);
		if (favoriteOnly) {
			filter.append(" AND " + HeaderData.FAVORIET + "='true'");
		}
		setFilter(filter.toString(), null);
		if (!favoriteOnly) {
			setCounter(query);
			this.query = query;
		}
		this.dagboek = dagboek;
		this.decAmount = 2;
		this.amtTransformer = new DecimalToAmountTransformer(decAmount);
		this.dtmTransformer = new DatumTransformer();
		this.stsTransformer = new HeaderDataStatusTransformer();
		this.dcTransformer = new DebCredTransformer(dagboek, true);
		this.bijlageTransformer = new BijlageTransformer();
		initialize();
	}

	private void initialize() throws Exception {
		setName("HeaderDataTable/" + dagboek.getId() + (favoriteOnly ? "/Fav" : ""));
		setAddSelectAllRowsContextMenuItem(true);
		setAddUnSelectAllRowsContextMenuItem(true);
		addDownloadCSVContextAction("Boekingen " + dagboek.getOmschrijving());
		addDownloadPDFContextAction("Boekingen " + dagboek.getOmschrijving());
		Map<String, InputComponent> myInputComponents = new HashMap<String, InputComponent>();
		myInputComponents.put(HeaderData.BOEKDATUM, new DateSelectionField("Boekdatum", new Date(), "dd-MM-yyyy"));
		myInputComponents.put(HeaderData.BOEKPERIODE, new NumberField("Boekperiode", 2, true));
		// myInputComponents.put(HeaderData.BOEKSTUK, new
		// DecimalField(MatchDtaHor.AMT_ACCEPTEDF, 15, decAmount, false));
		// myInputComponents.put(HeaderData.DC_NUMMER, new
		// DecimalField(MatchDtaHor.AMT_MATCHABLE, 15, decAmount, false));
		// myInputComponents.put(HeaderData.FACTUUR_NUMMER, new
		// DecimalField(MatchDtaHor.AMT_MATCHABLEF, 15, decAmount, false));
		myInputComponents.put(HeaderData.BOEKDATUM, new DateSelectionField("Boekdatum", new Date(), "dd-MM-yyyy"));
		myInputComponents.put(HeaderData.FACTUURDATUM, new DateSelectionField("Factuurdatum", new Date(), "dd-MM-yyyy"));
		// myInputComponents.put(HeaderData.OMSCHR1, new
		// DecimalField(MatchDtaHor.AMT_MATCHEDF, 15, decAmount, false));
		// myInputComponents.put(HeaderData.OMSCHR2, new
		// DecimalField(MatchDtaHor.AMT_SETTLED, 15, decAmount, false));
		// myInputComponents.put(HeaderData.REFERENTIE_NUMMER, new
		// Field("Referentienummer", 15, decAmount, false));
		myInputComponents.put(HeaderData.DC, new ComboDC(dagboek, true));
		myInputComponents.put(HeaderData.VERVALDATUM, new DateSelectionField("Vervaldatum", new Date(), "dd-MM-yyyy"));
		myInputComponents.put(HeaderData.STATUS, EnumHelper.createComboBox("Status", HeaderDataStatusEnum.getCollection(dagboek), false));
		myInputComponents.put(HeaderData.TOTAAL_BTW, new DecimalField(HeaderData.TOTAAL_BTW, 15, decAmount, false));
		myInputComponents.put(HeaderData.TOTAAL_EXCL, new DecimalField(HeaderData.TOTAAL_EXCL, 15, decAmount, false));
		myInputComponents.put(HeaderData.TOTAAL_GEBOEKT, new DecimalField(HeaderData.TOTAAL_GEBOEKT, 15, decAmount, false));
		myInputComponents.put(HeaderData.TOTAAL_INCL, new DecimalField(HeaderData.TOTAAL_INCL, 15, decAmount, false));
		myInputComponents.put(HeaderData.FAVORIET, new CheckBox());
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> labels = new ArrayList<String>();
		ArrayList<String> sizes = new ArrayList<String>();
		ArrayList<String> types = new ArrayList<String>();
		ArrayList<Boolean> sort = new ArrayList<Boolean>();
		ArrayList<Transformer> transformers = new ArrayList<Transformer>();
		ArrayList<String> namesForFilter = new ArrayList<String>();
		ArrayList<String> labelsForFilter = new ArrayList<String>();

		String tmpString;
		String[] colName = GeneralHelper.getColumnNamesForHeaderData(dagboek);
		for (int c = 0; c < colName.length; c++) {
			switch (colName[c]) {
			case HeaderData.BOEKSTUK:
				names.add(HeaderData.BOEKSTUK);
				namesForFilter.add(HeaderData.BOEKSTUK);
				labels.add("Boekstuk");
				labelsForFilter.add("Boekstuk");
				sizes.add("100");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case HeaderData.BOEKDATUM:
				if (!favoriteOnly) {
					names.add(HeaderData.BOEKDATUM);
					namesForFilter.add(HeaderData.BOEKDATUM);
					labels.add("Boekdatum");
					labelsForFilter.add("Boekdatum");
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_TEXT);
					transformers.add(dtmTransformer);
				}
				break;
			case HeaderData.DC:
				names.add(HeaderData.DC);
				namesForFilter.add(HeaderData.DC);
				tmpString = (dagboek.isBankboek() ? "Bij/Af" : dagboek.isKasboek() ? "Ontv/Uitg" : "Deb/Cr");
				labels.add(tmpString);
				labelsForFilter.add(tmpString);
				sizes.add("50");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(dcTransformer);
				break;
			case HeaderData.DC_NUMMER:
				names.add(HeaderData.DC_NUMMER);
				namesForFilter.add(HeaderData.DC_NUMMER);
				String relatie = dagboek.isInkoopboek() ? "Crediteur" : dagboek.isVerkoopboek() ? "Debiteur" : "Relatie";
				labels.add(relatie);
				labelsForFilter.add(relatie);
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case HeaderDataReader.DEBT_NAAM:
				names.add(HeaderDataReader.DEBT_NAAM);
				// namesForFilter.add(HeaderDataReader.DEBT_NAAM);
				labels.add("Naam");
				// labelsForFilter.add("Naam");
				sizes.add("150");
				sort.add(Boolean.FALSE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case HeaderDataReader.CRED_NAAM:
				names.add(HeaderDataReader.CRED_NAAM);
				// namesForFilter.add(HeaderDataReader.CRED_NAAM);
				labels.add("Naam");
				// labelsForFilter.add("Naam");
				sizes.add("150");
				sort.add(Boolean.FALSE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case HeaderData.FACTUUR_NUMMER:
				names.add(HeaderData.FACTUUR_NUMMER);
				namesForFilter.add(HeaderData.FACTUUR_NUMMER);
				labels.add("Factuur");
				labelsForFilter.add("Factuur");
				sizes.add("100");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case HeaderData.FACTUURDATUM:
				if (!favoriteOnly) {
					names.add(HeaderData.FACTUURDATUM);
					namesForFilter.add(HeaderData.FACTUURDATUM);
					labels.add("Factuurdatum");
					labelsForFilter.add("Factuurdatum");
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_TEXT);
					transformers.add(dtmTransformer);
				}
				break;
			case HeaderData.VERVALDATUM:
				if (!favoriteOnly) {
					names.add(HeaderData.VERVALDATUM);
					namesForFilter.add(HeaderData.VERVALDATUM);
					labels.add("Vervaldatum");
					labelsForFilter.add("Vervaldatum");
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_TEXT);
					transformers.add(dtmTransformer);
				}
				break;
			case HeaderData.REFERENTIE_NUMMER:
				if (!favoriteOnly) {
					names.add(HeaderData.REFERENTIE_NUMMER);
					namesForFilter.add(HeaderData.REFERENTIE_NUMMER);
					labels.add("Referentienr.");
					labelsForFilter.add("Referentienr.");
					sizes.add("100");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_TEXT);
					transformers.add(null);
				}
				break;
			case HeaderData.TOTAAL_BTW:
				if (!favoriteOnly) {
					names.add(HeaderData.TOTAAL_BTW);
					namesForFilter.add(HeaderData.TOTAAL_BTW);
					labels.add("Bedrag BTW");
					labelsForFilter.add("Bedrag BTW");
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_DECIMAL);
					transformers.add(amtTransformer);
				}
				break;
			case HeaderData.TOTAAL_EXCL:
				if (!favoriteOnly) {
					names.add(HeaderData.TOTAAL_EXCL);
					namesForFilter.add(HeaderData.TOTAAL_EXCL);
					labels.add("Bedrag Excl.");
					labelsForFilter.add("Bedrag Excl.");
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_DECIMAL);
					transformers.add(amtTransformer);
				}
				break;
			case HeaderData.TOTAAL_INCL:
				if (!favoriteOnly) {
					names.add(HeaderData.TOTAAL_INCL);
					namesForFilter.add(HeaderData.TOTAAL_INCL);
					labels.add("Bedrag Incl.");
					labelsForFilter.add("Bedrag Incl.");
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_DECIMAL);
					transformers.add(amtTransformer);
				}
				break;
			case HeaderData.TOTAAL_DEBET:
				if (!favoriteOnly) {
					names.add(HeaderData.TOTAAL_DEBET);
					namesForFilter.add(HeaderData.TOTAAL_DEBET);
					tmpString = GeneralHelper.getColumnLabelDC(dagboek, "D", true);
					labels.add(tmpString);
					labelsForFilter.add(tmpString);
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_DECIMAL);
					transformers.add(amtTransformer);
				}
				break;
			case HeaderData.TOTAAL_CREDIT:
				if (!favoriteOnly) {
					names.add(HeaderData.TOTAAL_CREDIT);
					namesForFilter.add(HeaderData.TOTAAL_CREDIT);
					tmpString = GeneralHelper.getColumnLabelDC(dagboek, "C", true);
					labels.add(tmpString);
					labelsForFilter.add(tmpString);
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_DECIMAL);
					transformers.add(amtTransformer);
				}
				break;
			case HeaderData.TOTAAL_GEBOEKT:
				names.add(HeaderData.TOTAAL_GEBOEKT);
				namesForFilter.add(HeaderData.TOTAAL_GEBOEKT);
				tmpString = (dagboek.isInkoopboek() || dagboek.isVerkoopboek() ? "Factuurbedrag" : dagboek.isMemoriaal() ? "Bedrag(Debet)" : "Geboekt bedrag");
				labels.add(tmpString);
				labelsForFilter.add(tmpString);
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case HeaderData.TOTAAL_BETAALD:
				if (!favoriteOnly) {
					names.add(HeaderData.TOTAAL_BETAALD);
					namesForFilter.add(HeaderData.TOTAAL_BETAALD);
					labels.add("Betaald");
					labelsForFilter.add("Betaald");
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_DECIMAL);
					transformers.add(amtTransformer);
				}
				break;
			case HeaderData.OMSCHR1:
				names.add(HeaderData.OMSCHR1);
				namesForFilter.add(HeaderData.OMSCHR1);
				labels.add("Omschrijving(1)");
				labelsForFilter.add("Omschrijving(1)");
				sizes.add("250");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case HeaderData.OMSCHR2:
				names.add(HeaderData.OMSCHR2);
				namesForFilter.add(HeaderData.OMSCHR2);
				labels.add("Omschrijving(2)");
				labelsForFilter.add("Omschrijving(2)");
				sizes.add("250");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case HeaderData.STATUS:
				if (!favoriteOnly) {
					names.add(HeaderData.STATUS);
					namesForFilter.add(HeaderData.STATUS);
					labels.add("Status");
					labelsForFilter.add("Status");
					sizes.add("80");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_TEXT);
					transformers.add(stsTransformer);
				}
				break;
			case HeaderDataReader.BIJLAGE:
				if (!favoriteOnly) {
					names.add(HeaderDataReader.BIJLAGE);
					// namesForFilter.add(HeaderDataReader.BIJLAGE);
					labels.add("Bijlage");
					// labelsForFilter.add("Bijlage");
					sizes.add("40");
					sort.add(Boolean.FALSE);
					types.add(Field.TYPE_TEXT);
					transformers.add(bijlageTransformer);
				}
				break;
			case HeaderData.FAVORIET:
				if (!favoriteOnly) {
					names.add(HeaderData.FAVORIET);
					namesForFilter.add(HeaderData.FAVORIET);
					labels.add("Fav.");
					labelsForFilter.add("Favoriet");
					sizes.add("30");
					sort.add(Boolean.TRUE);
					types.add(Field.TYPE_BOOLEAN);
					transformers.add(null);
				}
				break;
			}
		}

		String[] vrijeRubriekNaam = new String[] { HeaderData.VRIJE_RUB1, HeaderData.VRIJE_RUB2, HeaderData.VRIJE_RUB3, HeaderData.VRIJE_RUB4, HeaderData.VRIJE_RUB5 };
		VrijeRubriekDefinitie[] definitie = VrijeRubriekHelper.getVrijeRubriekDefinities(dagboek);
		for (int f = 0; f < 5; f++) {
			String dataType = definitie[f].getDataType();
			if (!dataType.equals(VrijeRubriekSoortEnum.NOT_IN_USE)) {
				names.add(vrijeRubriekNaam[f]);
				namesForFilter.add(vrijeRubriekNaam[f]);
				labels.add(definitie[f].getPrompt());
				labelsForFilter.add(definitie[f].getPrompt());
				sizes.add("100");
				sort.add(Boolean.TRUE);
				switch (dataType) {
				case VrijeRubriekSoortEnum.NUM:
					types.add(Field.TYPE_NUMBER);
					break;
				case VrijeRubriekSoortEnum.DEC:
					types.add(Field.TYPE_DECIMAL);
					break;
				default:
					types.add(Field.TYPE_TEXT);
				}
				transformers.add(new VrijeRubriekTransformer());
				myInputComponents.put(vrijeRubriekNaam[f], VrijeRubriekHelper.getInputComponent(dagboek, f, true));
			}
		}

		int numberOfColumns = names.size();
		String[] columnNames = new String[numberOfColumns];
		String[] columnLabels = new String[numberOfColumns];
		String[] columnTypes = new String[numberOfColumns];
		short[] columnSizes = new short[numberOfColumns];
		boolean[] columnSort = new boolean[numberOfColumns];
		Transformer[] displayTransformers = new Transformer[numberOfColumns];
		for (int i = 0; i < numberOfColumns; i++) {
			columnNames[i] = (String) names.get(i);
			columnLabels[i] = (String) labels.get(i);
			columnTypes[i] = (String) types.get(i);
			columnSizes[i] = Short.parseShort((String) sizes.get(i));
			displayTransformers[i] = (Transformer) transformers.get(i);
			columnSort[i] = ((Boolean) sort.get(i)).booleanValue();
		}
		setColumnNames(columnNames);
		setColumnLabels(columnLabels);
		setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		setColumnTypes(columnTypes);
		setColumnSizes(columnSizes);
		setColumnSortable(columnSort);
		setInputComponents(myInputComponents);
		setDisplayTransformers(displayTransformers);
		setMultipleSelect(true);

		numberOfColumns = namesForFilter.size();
		filterNames = new String[numberOfColumns];
		filterLabels = new String[numberOfColumns];
		for (int i = 0; i < numberOfColumns; i++) {
			filterNames[i] = (String) namesForFilter.get(i);
			filterLabels[i] = (String) labelsForFilter.get(i);
		}
		HashMap<String, String> columnsToCalculate = new HashMap<String, String>();
		columnsToCalculate.put(HeaderData.TOTAAL_EXCL, (String) ODBCalculatorImpl.SUM);
		columnsToCalculate.put(HeaderData.TOTAAL_BTW, (String) ODBCalculatorImpl.SUM);
		columnsToCalculate.put(HeaderData.TOTAAL_INCL, (String) ODBCalculatorImpl.SUM);
		columnsToCalculate.put(HeaderData.TOTAAL_DEBET, (String) ODBCalculatorImpl.SUM);
		columnsToCalculate.put(HeaderData.TOTAAL_CREDIT, (String) ODBCalculatorImpl.SUM);
		if (!favoriteOnly) {
			query.setColumnsToCalculate(columnsToCalculate);
			setCalculator(query);
		}
	}

	public SearchPanel getSearchPanel() throws Exception {
		SearchPanel searchPanel = createSearchWithFilterPanel(filterNames, filterLabels, filterNames, filterLabels, true);
		searchPanel.setDefaultInputTransformer(new MyKeyedTransformer(dagboek));
		return searchPanel;
	}

	public void setColumnsToCalculate(HashMap<String, String> columnsToCalculate) throws Exception {
		query.setColumnsToCalculate(columnsToCalculate);
		setCalculator(query);
	}
}