package nl.eadmin.ui.uiobjects.tables;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.Journaal;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.query.JournaalQuery;
import nl.eadmin.ui.transformer.DatumTransformer;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.eadmin.ui.uiobjects.referencefields.BtwCodeReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.DagboekReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.RekeningReferenceField;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.NumberField;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.CommonObjectDataReader;
import nl.ibs.esp.util.ODBCalculatorImpl;
import nl.ibs.esp.util.Transformer;
import nl.ibs.jsql.DBData;

public class JournaalTable extends ODBTable {
	private static final long serialVersionUID = -7711629080534758729L;
	private static String[] keys = new String[] { Journaal.BEDRIJF, Journaal.DAGBOEK_ID, Journaal.BOEKSTUK, Journaal.SEQ_NR };
	private Bedrijf bedrijf;
	private JournaalQuery query;
	private String[] filterNames;
	private String[] filterLabels;
	private DecimalToAmountTransformer amtTransformer;
	private DatumTransformer dtmTransformer;
	private int decAmount;

	public JournaalTable(CommonObjectDataReader reader, Bedrijf bedrijf, int boekjaar) throws Exception {
		this(reader, new JournaalQuery(bedrijf, boekjaar), bedrijf, boekjaar);
	}

	private JournaalTable(CommonObjectDataReader reader, JournaalQuery query, Bedrijf bedrijf, int boekjaar) throws Exception {
		super(reader, query, keys, ApplicationConstants.NUMBEROFTABLEROWS);
		setCounter(query);
		this.query = query;
		this.bedrijf = bedrijf;
		this.decAmount = 2;
		this.amtTransformer = new DecimalToAmountTransformer(decAmount);
		this.dtmTransformer = new DatumTransformer();
		initialize();
	}

	private void initialize() throws Exception {
		DBData dbd = bedrijf.getDBData();
		setName("JournaalTable");
		setAddSelectAllRowsContextMenuItem(true);
		setAddUnSelectAllRowsContextMenuItem(true);
		addDownloadCSVContextAction("Journaalposten");
		addDownloadPDFContextAction("Journaalposten");
		Map<String, InputComponent> myInputComponents = new HashMap<String, InputComponent>();
		myInputComponents.put(Journaal.DAGBOEK_ID, new DagboekReferenceField("Dagboek", bedrijf, DagboekManagerFactory.getInstance(dbd)));
		myInputComponents.put(Journaal.BOEKDATUM, new DateSelectionField("Boekdatum", new Date(), "dd-MM-yyyy"));
		myInputComponents.put(Journaal.BOEKPERIODE, new NumberField("Boekperiode", 2, true));
		myInputComponents.put(Journaal.REKENING_NR, new RekeningReferenceField("Rekening", bedrijf, RekeningManagerFactory.getInstance(dbd)));
		myInputComponents.put(Journaal.BTW_CODE, new BtwCodeReferenceField("Btw-code", bedrijf, BtwCodeManagerFactory.getInstance(dbd)));
		myInputComponents.put(Journaal.BEDRAG_DEBET, new DecimalField("Bedrag(d)", 15, decAmount, false));
		myInputComponents.put(Journaal.BEDRAG_CREDIT, new DecimalField("Bedrag(c)", 15, decAmount, false));
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> labels = new ArrayList<String>();
		ArrayList<String> sizes = new ArrayList<String>();
		ArrayList<String> types = new ArrayList<String>();
		ArrayList<Boolean> sort = new ArrayList<Boolean>();
		ArrayList<Transformer> transformers = new ArrayList<Transformer>();
		ArrayList<String> namesForFilter = new ArrayList<String>();
		ArrayList<String> labelsForFilter = new ArrayList<String>();

		String[] colName = GeneralHelper.getColumnNamesForJournals();
		for (int c = 0; c < colName.length; c++) {
			switch (colName[c]) {
			case Journaal.DAGBOEK_ID:
				names.add(Journaal.DAGBOEK_ID);
				namesForFilter.add(Journaal.DAGBOEK_ID);
				labels.add("Dagboek");
				labelsForFilter.add("Dagboek");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case Journaal.BOEKSTUK:
				names.add(Journaal.BOEKSTUK);
				namesForFilter.add(Journaal.BOEKSTUK);
				labels.add("Boekstuk");
				labelsForFilter.add("Boekstuk");
				sizes.add("100");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case Journaal.BOEKDATUM:
				names.add(Journaal.BOEKDATUM);
				namesForFilter.add(Journaal.BOEKDATUM);
				labels.add("Boekdatum");
				labelsForFilter.add("Boekdatum");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(dtmTransformer);
				break;
			case Journaal.BOEKPERIODE:
				names.add(Journaal.BOEKPERIODE);
				namesForFilter.add(Journaal.BOEKPERIODE);
				labels.add("Periode");
				labelsForFilter.add("Periode");
				sizes.add("30");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_NUMBER);
				transformers.add(null);
				break;
			case Journaal.DC_NUMMER:
				names.add(Journaal.DC_NUMMER);
				namesForFilter.add(Journaal.DC_NUMMER);
				labels.add("Relatie");
				labelsForFilter.add("Relatie");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case Journaal.FACTUUR_NR:
				names.add(Journaal.FACTUUR_NR);
				namesForFilter.add(Journaal.FACTUUR_NR);
				labels.add("Factnr");
				labelsForFilter.add("Factnr");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case Journaal.BTW_CODE:
				names.add(Journaal.BTW_CODE);
				namesForFilter.add(Journaal.BTW_CODE);
				labels.add("Btw-code");
				labelsForFilter.add("Btw-code");
				sizes.add("40");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case Journaal.BTW_CATEGORIE:
				names.add(Journaal.BTW_CATEGORIE);
				namesForFilter.add(Journaal.BTW_CATEGORIE);
				labels.add("Btw-cat");
				labelsForFilter.add("Btw-cat");
				sizes.add("40");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case Journaal.ICP_SOORT:
				names.add(Journaal.ICP_SOORT);
				namesForFilter.add(Journaal.ICP_SOORT);
				labels.add("Icp-type");
				labelsForFilter.add("Icp-type");
				sizes.add("40");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			// case JournaalReader.DEBT_NAAM:
			// names.add(JournaalReader.DEBT_NAAM);
			// // namesForFilter.add(JournaalReader.DEBT_NAAM);
			// labels.add("Naam");
			// // labelsForFilter.add("Naam");
			// sizes.add("150");
			// sort.add(Boolean.FALSE);
			// types.add(Field.TYPE_TEXT);
			// transformers.add(null);
			// break;
			case Journaal.REKENING_NR:
				names.add(Journaal.REKENING_NR);
				namesForFilter.add(Journaal.REKENING_NR);
				labels.add("Rekening");
				labelsForFilter.add("Rekening");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case Journaal.BEDRAG_CREDIT:
				names.add(Journaal.BEDRAG_CREDIT);
				namesForFilter.add(Journaal.BEDRAG_CREDIT);
				labels.add("Bedrag(c)");
				labelsForFilter.add("Bedrag(c)");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case Journaal.BEDRAG_DEBET:
				names.add(Journaal.BEDRAG_DEBET);
				namesForFilter.add(Journaal.BEDRAG_DEBET);
				labels.add("Bedrag(d)");
				labelsForFilter.add("Bedrag(d)");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case Journaal.OMSCHR1:
				names.add(Journaal.OMSCHR1);
				namesForFilter.add(Journaal.OMSCHR1);
				labels.add("Omschrijving(1)");
				labelsForFilter.add("Omschrijving(1)");
				sizes.add("250");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case Journaal.OMSCHR2:
				names.add(Journaal.OMSCHR2);
				namesForFilter.add(Journaal.OMSCHR2);
				labels.add("Omschrijving(2)");
				labelsForFilter.add("Omschrijving(2)");
				sizes.add("250");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case Journaal.OMSCHR3:
				names.add(Journaal.OMSCHR3);
				namesForFilter.add(Journaal.OMSCHR3);
				labels.add("Omschrijving(3)");
				labelsForFilter.add("Omschrijving(3)");
				sizes.add("250");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			}
		}

		int numberOfColumns = names.size();
		String[] columnNames = new String[numberOfColumns];
		String[] columnLabels = new String[numberOfColumns];
		String[] columnTypes = new String[numberOfColumns];
		short[] columnSizes = new short[numberOfColumns];
		boolean[] columnSort = new boolean[numberOfColumns];
		Transformer[] displayTransformers = new Transformer[numberOfColumns];
		for (int i = 0; i < numberOfColumns; i++) {
			columnNames[i] = (String) names.get(i);
			columnLabels[i] = (String) labels.get(i);
			columnTypes[i] = (String) types.get(i);
			columnSizes[i] = Short.parseShort((String) sizes.get(i));
			displayTransformers[i] = (Transformer) transformers.get(i);
			columnSort[i] = ((Boolean) sort.get(i)).booleanValue();
		}
		setColumnNames(columnNames);
		setColumnLabels(columnLabels);
		setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		setColumnTypes(columnTypes);
		setColumnSizes(columnSizes);
		setColumnSortable(columnSort);
		setInputComponents(myInputComponents);
		setDisplayTransformers(displayTransformers);
		setMultipleSelect(true);

		numberOfColumns = namesForFilter.size();
		filterNames = new String[numberOfColumns];
		filterLabels = new String[numberOfColumns];
		for (int i = 0; i < numberOfColumns; i++) {
			filterNames[i] = (String) namesForFilter.get(i);
			filterLabels[i] = (String) labelsForFilter.get(i);
		}
		HashMap<String, String> columnsToCalculate = new HashMap<String, String>();
		columnsToCalculate.put(Journaal.BEDRAG_CREDIT, (String) ODBCalculatorImpl.SUM);
		columnsToCalculate.put(Journaal.BEDRAG_DEBET, (String) ODBCalculatorImpl.SUM);
		query.setColumnsToCalculate(columnsToCalculate);
		setCalculator(query);
	}

	public SearchPanel getSearchPanel() throws Exception {
		SearchPanel searchPanel = createSearchWithFilterPanel(filterNames, filterLabels, filterNames, filterLabels, true);
//		searchPanel.setDefaultInputTransformer(new MyKeyedTransformer(dagboek));
		return searchPanel;
	}

	public void setColumnsToCalculate(HashMap<String, String> columnsToCalculate) throws Exception {
		query.setColumnsToCalculate(columnsToCalculate);
		setCalculator(query);
	}
}