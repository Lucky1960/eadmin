package nl.eadmin.ui.uiobjects.tables;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import nl.eadmin.databeans.DebCredKaartMutatieDatabean;
import nl.eadmin.databeans.DebCredLijstDataBean;
import nl.eadmin.db.HeaderData;
import nl.ibs.esp.util.KeyedTransformer;

public class HeaderDataKeyedTransformer implements KeyedTransformer {
	private static final long serialVersionUID = -4537604508840660105L;
	private ArrayList<String> decimalFields;

	public HeaderDataKeyedTransformer() throws Exception {
		this.decimalFields = new ArrayList<String>();
		this.decimalFields.add(HeaderData.TOTAAL_BETAALD);
		this.decimalFields.add(HeaderData.TOTAAL_DEBET);
		this.decimalFields.add(HeaderData.TOTAAL_CREDIT);
		this.decimalFields.add(HeaderData.TOTAAL_BTW);
		this.decimalFields.add(HeaderData.TOTAAL_EXCL);
		this.decimalFields.add(HeaderData.TOTAAL_GEBOEKT);
		this.decimalFields.add(HeaderData.TOTAAL_INCL);
		this.decimalFields.add(HeaderData.TOTAAL_OPENSTAAND);
		this.decimalFields.add(HeaderData.TOTAAL_OPENSTAAND);
		this.decimalFields.add(DebCredLijstDataBean.BEDRAG_D);
		this.decimalFields.add(DebCredLijstDataBean.BEDRAG_C);
		this.decimalFields.add(DebCredLijstDataBean.SALDO);
		this.decimalFields.add(DebCredKaartMutatieDatabean._DEBET);
		this.decimalFields.add(DebCredKaartMutatieDatabean._CREDIT);
	}

	public Object transform(Object object, Object key) {
		if (object instanceof String && decimalFields.contains(key)) {
			String amt = ((String) object).trim();
			if (amt == null || amt.length() == 0) {
				return "0.00";
			}
			int i1 = amt.lastIndexOf(".");
			int i2 = amt.lastIndexOf(",");
			if (i1 > i2) {
				amt = amt.replaceAll("\\,", "");
			} else {
				if (i1 < 0) {
					amt = amt.replaceAll("\\,", "\\.");
				} else {
					amt = amt.replaceAll("\\.", "");
					amt = amt.replaceAll("\\,", "\\.");
				}
			}
			return amt;
		} else {
			String result = null;
			if (object instanceof Date) {
				Calendar cal = Calendar.getInstance();
				cal.setTime((Date) object);
				int y = cal.get(Calendar.YEAR);
				int m = cal.get(Calendar.MONTH) + 1;
				int d = cal.get(Calendar.DAY_OF_MONTH);
				String year = "" + y;
				String month = (m <= 9 ? "0" : "") + m;
				String day = (d <= 9 ? "0" : "") + d;
				result = year + month + day;
			}
			// if (object instanceof String) {
			// if (fieldType.equals(FieldTypeEnum.ALFA) ||
			// fieldType.equals(FieldTypeEnum.BOOLEAN)) {
			// result = (String) object;
			// } else if (fieldType.equals(FieldTypeEnum.DATE)) {
			// result = ((String) object).replaceAll("-", "");
			// } else if (fieldType.equals(FieldTypeEnum.DECIMAL) ||
			// fieldType.equals(FieldTypeEnum.NUM)) {
			// result = ((String) object).replaceAll("\\.",
			// "").replaceAll("\\,", "");
			// result = "000000000000000000000000000000" + result;
			// result = result.substring(result.length() - fieldLength);
			// }
			// }
			if (result == null) {
				return object;
			} else {
				return result;
			}
		}
	}
}