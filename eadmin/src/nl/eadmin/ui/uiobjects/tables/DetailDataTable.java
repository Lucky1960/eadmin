package nl.eadmin.ui.uiobjects.tables;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nl.eadmin.databeans.VrijeRubriekDefinitie;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.enums.VrijeRubriekSoortEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.VrijeRubriekHelper;
import nl.eadmin.query.DetailDataQuery;
import nl.eadmin.ui.datareaders.DetailDataReader;
import nl.eadmin.ui.transformer.BtwCodeTransformer;
import nl.eadmin.ui.transformer.DatumTransformer;
import nl.eadmin.ui.transformer.DebCredTransformer;
import nl.eadmin.ui.transformer.DecimalToAmountTransformer;
import nl.eadmin.ui.transformer.RekeningTransformer;
import nl.eadmin.ui.transformer.VrijeRubriekTransformer;
import nl.eadmin.ui.uiobjects.referencefields.BtwCodeReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.DebiteurReferenceField;
import nl.eadmin.ui.uiobjects.referencefields.RekeningReferenceField;
import nl.ibs.esp.uiobjects.DateSelectionField;
import nl.ibs.esp.uiobjects.DecimalField;
import nl.ibs.esp.uiobjects.Field;
import nl.ibs.esp.uiobjects.InputComponent;
import nl.ibs.esp.uiobjects.ODBTable;
import nl.ibs.esp.util.CommonObjectDataReader;
import nl.ibs.esp.util.ODBCalculatorImpl;
import nl.ibs.esp.util.Transformer;
import nl.ibs.jsql.DBData;

public class DetailDataTable extends ODBTable {
	private static final long serialVersionUID = -7711629080534758729L;
	private static String[] keys = new String[] { DetailData.BEDRIJF, DetailData.DAGBOEK_ID, DetailData.BOEKSTUK };
	private Bedrijf bedrijf;
	private Dagboek dagboek;
	private DetailDataQuery query;
	private String[] filterNames;
	private String[] filterLabels;
	private DecimalToAmountTransformer amtTransformer;
	private DatumTransformer dtmTransformer;
	private DebCredTransformer dcTransformer;
	private int decAmount;

	public DetailDataTable(CommonObjectDataReader reader, Bedrijf bedrijf, Dagboek dagboek, HeaderData headerData) throws Exception {
		this(reader, new DetailDataQuery(dagboek, headerData), bedrijf, dagboek);
	}

	private DetailDataTable(CommonObjectDataReader reader, DetailDataQuery query, Bedrijf bedrijf, Dagboek dagboek) throws Exception {
		super(reader, query, keys, 5);
		setCounter(query);
		this.query = query;
		this.bedrijf = bedrijf;
		this.dagboek = dagboek;
		this.decAmount = 2;
		this.amtTransformer = new DecimalToAmountTransformer(decAmount);
		this.dtmTransformer = new DatumTransformer();
		this.dcTransformer = new DebCredTransformer(dagboek, false);
		initialize();
	}

	private void initialize() throws Exception {
		DBData dbd = bedrijf.getDBData();
		setName("DetailDataTable/" + dagboek.getId());
		setAddSelectAllRowsContextMenuItem(true);
		setAddUnSelectAllRowsContextMenuItem(true);
		addDownloadCSVContextAction("Boekingen " + dagboek.getOmschrijving());
		addDownloadPDFContextAction("Boekingen " + dagboek.getOmschrijving());
		Map<String, InputComponent> myInputComponents = new HashMap<String, InputComponent>();
		myInputComponents.put(DetailData.AANTAL, new DecimalField(DetailData.BEDRAG_BTW, 5, decAmount, false));
		myInputComponents.put(DetailData.BEDRAG_BTW, new DecimalField(DetailData.BEDRAG_BTW, 15, decAmount, false));
		myInputComponents.put(DetailData.BEDRAG_EXCL, new DecimalField(DetailData.BEDRAG_EXCL, 15, decAmount, false));
		myInputComponents.put(DetailData.BEDRAG_INCL, new DecimalField(DetailData.BEDRAG_INCL, 15, decAmount, false));
		myInputComponents.put(DetailData.BTW_CODE, new BtwCodeReferenceField("Btw-code", bedrijf, BtwCodeManagerFactory.getInstance(dbd)));
		myInputComponents.put(DetailData.DATUM_LEVERING, new DateSelectionField("Factuurdatum", new Date(), "dd-MM-yyyy"));
		myInputComponents.put(DetailData.DC_NUMMER, new DebiteurReferenceField("Debiteur/klant", bedrijf, DebiteurManagerFactory.getInstance(dbd)));
		myInputComponents.put(DetailData.EENHEID, new Field("Eenheid", Field.TYPE_TEXT, DetailData.EENHEID, "", 10));
		myInputComponents.put(DetailData.FACTUUR_NUMMER, new Field("Eenheid", Field.TYPE_TEXT, DetailData.EENHEID, "", 10));
		myInputComponents.put(DetailData.OMSCHR1, new Field("Omschrijving(1)", Field.TYPE_TEXT, DetailData.OMSCHR1, "", 50));
		myInputComponents.put(DetailData.OMSCHR2, new Field("Omschrijving(2)", Field.TYPE_TEXT, DetailData.OMSCHR2, "", 50));
		myInputComponents.put(DetailData.OMSCHR3, new Field("Omschrijving(3)", Field.TYPE_TEXT, DetailData.OMSCHR3, "", 50));
		myInputComponents.put(DetailData.PRIJS, new DecimalField(DetailData.PRIJS, 15, decAmount, false));
		myInputComponents.put(DetailData.REKENING, new RekeningReferenceField("Rekeningnr.", bedrijf, RekeningManagerFactory.getInstance(dbd)));
//		myInputComponents.put(DetailData.VRIJE_RUB1, VrijeRubriekHelper.getInputComponent(dagboek, 0, false));
//		myInputComponents.put(DetailData.VRIJE_RUB2, VrijeRubriekHelper.getInputComponent(dagboek, 1, false));
//		myInputComponents.put(DetailData.VRIJE_RUB3, VrijeRubriekHelper.getInputComponent(dagboek, 2, false));
//		myInputComponents.put(DetailData.VRIJE_RUB4, VrijeRubriekHelper.getInputComponent(dagboek, 3, false));
//		myInputComponents.put(DetailData.VRIJE_RUB5, VrijeRubriekHelper.getInputComponent(dagboek, 4, false));

		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> labels = new ArrayList<String>();
		ArrayList<String> sizes = new ArrayList<String>();
		ArrayList<String> types = new ArrayList<String>();
		ArrayList<Boolean> sort = new ArrayList<Boolean>();
		ArrayList<Transformer> transformers = new ArrayList<Transformer>();
		ArrayList<String> namesForFilter = new ArrayList<String>();
		ArrayList<String> labelsForFilter = new ArrayList<String>();

		String tmpString;
		String[] colName = GeneralHelper.getColumnNamesForDetailData(dagboek);
		for (int c = 0; c < colName.length; c++) {
			switch (colName[c]) {
			case DetailData.AANTAL:
				names.add(DetailData.AANTAL);
				namesForFilter.add(DetailData.AANTAL);
				labels.add("Aantal");
				labelsForFilter.add("Aantal");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case DetailData.BEDRAG_BTW:
				names.add(DetailData.BEDRAG_BTW);
				namesForFilter.add(DetailData.BEDRAG_BTW);
				labels.add("Bedrag BTW");
				labelsForFilter.add("Bedrag BTW");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case DetailData.BEDRAG_EXCL:
				names.add(DetailData.BEDRAG_EXCL);
				namesForFilter.add(DetailData.BEDRAG_EXCL);
				labels.add("Bedrag Excl.");
				labelsForFilter.add("Bedrag Excl.");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case DetailData.BEDRAG_INCL:
				names.add(DetailData.BEDRAG_INCL);
				namesForFilter.add(DetailData.BEDRAG_INCL);
				labels.add("Bedrag Incl.");
				labelsForFilter.add("Bedrag Incl.");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case DetailData.BEDRAG_DEBET:
				names.add(DetailData.BEDRAG_DEBET);
				namesForFilter.add(DetailData.BEDRAG_DEBET);
				tmpString = GeneralHelper.getColumnLabelDC(dagboek, "D", false);
				labels.add(tmpString);
				labelsForFilter.add(tmpString);
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case DetailData.BEDRAG_CREDIT:
				names.add(DetailData.BEDRAG_CREDIT);
				namesForFilter.add(DetailData.BEDRAG_CREDIT);
				tmpString = GeneralHelper.getColumnLabelDC(dagboek, "C", false);
				labels.add(tmpString);
				labelsForFilter.add(tmpString);
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case DetailData.BTW_CODE:
				names.add(DetailData.BTW_CODE);
				namesForFilter.add(DetailData.BTW_CODE);
				labels.add("BTW-code");
				labelsForFilter.add("BTW-code");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(new BtwCodeTransformer(bedrijf));
				break;
			case DetailData.DATUM_LEVERING:
				names.add(DetailData.DATUM_LEVERING);
				namesForFilter.add(DetailData.DATUM_LEVERING);
				labels.add("Trans.datum");
				labelsForFilter.add("Trans.datum");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(dtmTransformer);
				break;
			case DetailData.DC:
				names.add(DetailData.DC);
				namesForFilter.add(DetailData.DC);
				labels.add("D/C");
				labelsForFilter.add("D/C");
				sizes.add("40");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(dcTransformer);
				break;
			case DetailData.DC_NUMMER:
				names.add(DetailData.DC_NUMMER);
				namesForFilter.add(DetailData.DC_NUMMER);
				String relatie = dagboek.isInkoopboek() ? "Crediteur" : dagboek.isVerkoopboek() ? "Debiteur" : "Relatie";
				labels.add(relatie);
				labelsForFilter.add(relatie);
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case DetailDataReader.DC_NAAM:
				names.add(DetailDataReader.DC_NAAM);
				// namesForFilter.add(DetailDataReader.DEBT_NAAM);
				labels.add("Naam");
				// labelsForFilter.add("Naam");
				sizes.add("150");
				sort.add(Boolean.FALSE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case DetailDataReader.DEBT_NAAM:
				names.add(DetailDataReader.DEBT_NAAM);
				// namesForFilter.add(DetailDataReader.DEBT_NAAM);
				labels.add("Naam");
				// labelsForFilter.add("Naam");
				sizes.add("150");
				sort.add(Boolean.FALSE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case DetailDataReader.CRED_NAAM:
				names.add(DetailDataReader.CRED_NAAM);
				// namesForFilter.add(HeaderDataReader.CRED_NAAM);
				labels.add("Naam");
				// labelsForFilter.add("Naam");
				sizes.add("150");
				sort.add(Boolean.FALSE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case DetailData.EENHEID:
				names.add(DetailData.EENHEID);
				namesForFilter.add(DetailData.EENHEID);
				labels.add("Eenheid");
				labelsForFilter.add("Eenheid");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case DetailData.FACTUUR_NUMMER:
				names.add(DetailData.FACTUUR_NUMMER);
				namesForFilter.add(DetailData.FACTUUR_NUMMER);
				labels.add("Factuur");
				labelsForFilter.add("Factuur");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case DetailData.OMSCHR1:
				names.add(DetailData.OMSCHR1);
				namesForFilter.add(DetailData.OMSCHR1);
				labels.add("Omschrijving(1)");
				labelsForFilter.add("Omschrijving(1)");
				sizes.add("150");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case DetailData.OMSCHR2:
				names.add(DetailData.OMSCHR2);
				namesForFilter.add(DetailData.OMSCHR2);
				labels.add("Omschrijving(2)");
				labelsForFilter.add("Omschrijving(2)");
				sizes.add("150");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case DetailData.OMSCHR3:
				names.add(DetailData.OMSCHR3);
				namesForFilter.add(DetailData.OMSCHR3);
				labels.add("Omschrijving(3)");
				labelsForFilter.add("Omschrijving(3)");
				sizes.add("150");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(null);
				break;
			case DetailData.PRIJS:
				names.add(DetailData.PRIJS);
				namesForFilter.add(DetailData.PRIJS);
				labels.add("Prijs");
				labelsForFilter.add("Prijs");
				sizes.add("80");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_DECIMAL);
				transformers.add(amtTransformer);
				break;
			case DetailData.REKENING:
				names.add(DetailData.REKENING);
				namesForFilter.add(DetailData.REKENING);
				labels.add("Rekening");
				labelsForFilter.add("Rekening");
				sizes.add("200");
				sort.add(Boolean.TRUE);
				types.add(Field.TYPE_TEXT);
				transformers.add(new RekeningTransformer(bedrijf));
				break;
			}
		}

		String[] vrijeRubriekNaam = new String[] { DetailData.VRIJE_RUB1, DetailData.VRIJE_RUB2, DetailData.VRIJE_RUB3, DetailData.VRIJE_RUB4, DetailData.VRIJE_RUB5 };
		VrijeRubriekDefinitie[] definitie = VrijeRubriekHelper.getVrijeRubriekDefinities(dagboek);
		for (int f = 0; f < 5; f++) {
			String dataType = definitie[f].getDataType();
			if (!dataType.equals(VrijeRubriekSoortEnum.NOT_IN_USE)) {
				names.add(vrijeRubriekNaam[f]);
				namesForFilter.add(vrijeRubriekNaam[f]);
				labels.add(definitie[f].getPrompt());
				labelsForFilter.add(definitie[f].getPrompt());
				sizes.add("100");
				sort.add(Boolean.TRUE);
				switch(dataType) {
				case VrijeRubriekSoortEnum.NUM:
					types.add(Field.TYPE_NUMBER);
					break;
				case VrijeRubriekSoortEnum.DEC:
					types.add(Field.TYPE_DECIMAL);
					break;
				default:
					types.add(Field.TYPE_TEXT);
				}
				transformers.add(new VrijeRubriekTransformer());
				myInputComponents.put(vrijeRubriekNaam[f], VrijeRubriekHelper.getInputComponent(dagboek, f, true));
			}
		}

		int numberOfColumns = names.size();
		String[] columnNames = new String[numberOfColumns];
		String[] columnLabels = new String[numberOfColumns];
		String[] columnTypes = new String[numberOfColumns];
		short[] columnSizes = new short[numberOfColumns];
		boolean[] columnSort = new boolean[numberOfColumns];
		Transformer[] displayTransformers = new Transformer[numberOfColumns];
		for (int i = 0; i < numberOfColumns; i++) {
			columnNames[i] = (String) names.get(i);
			columnLabels[i] = (String) labels.get(i);
			columnTypes[i] = (String) types.get(i);
			columnSizes[i] = Short.parseShort((String) sizes.get(i));
			displayTransformers[i] = (Transformer) transformers.get(i);
			columnSort[i] = ((Boolean) sort.get(i)).booleanValue();
		}
		setColumnNames(columnNames);
		setColumnLabels(columnLabels);
		setColumnLabelsEditable(GeneralHelper.isColumnLabelsEditable());
		setColumnTypes(columnTypes);
		setColumnSizes(columnSizes);
		setColumnSortable(columnSort);
		setInputComponents(myInputComponents);
		setDisplayTransformers(displayTransformers);
		setSelectable(true);

		numberOfColumns = namesForFilter.size();
		filterNames = new String[numberOfColumns];
		filterLabels = new String[numberOfColumns];
		for (int i = 0; i < numberOfColumns; i++) {
			filterNames[i] = (String) namesForFilter.get(i);
			filterLabels[i] = (String) labelsForFilter.get(i);
		}
		HashMap<String, String> columnsToCalculate = new HashMap<String, String>();
		columnsToCalculate.put(DetailData.BEDRAG_BTW, (String) ODBCalculatorImpl.SUM);
		columnsToCalculate.put(DetailData.BEDRAG_EXCL, (String) ODBCalculatorImpl.SUM);
		columnsToCalculate.put(DetailData.BEDRAG_INCL, (String) ODBCalculatorImpl.SUM);
		columnsToCalculate.put(DetailData.BEDRAG_DEBET, (String) ODBCalculatorImpl.SUM);
		columnsToCalculate.put(DetailData.BEDRAG_CREDIT, (String) ODBCalculatorImpl.SUM);
		query.setColumnsToCalculate(columnsToCalculate);
		setCalculator(query);
	}

	public SearchPanel getSearchPanel() throws Exception {
		SearchPanel searchPanel = createSearchWithFilterPanel(filterNames, filterLabels, filterNames, filterLabels, true);
		searchPanel.setDefaultInputTransformer(new MyKeyedTransformer(dagboek));
		return searchPanel;
	}

	public void setColumnsToCalculate(HashMap<String, String> columnsToCalculate) throws Exception {
		query.setColumnsToCalculate(columnsToCalculate);
		setCalculator(query);
	}
}