package nl.eadmin.ui.uiobjects.tables;

import nl.eadmin.databeans.VrijeRubriekDefinitie;
import nl.eadmin.db.Dagboek;
import nl.eadmin.enums.VrijeRubriekSoortEnum;
import nl.eadmin.helpers.VrijeRubriekHelper;
import nl.ibs.esp.util.KeyedTransformer;

public class MyKeyedTransformer implements KeyedTransformer {
	private static final long serialVersionUID = -4537604508840660105L;
	private VrijeRubriekDefinitie[] definitie;
	public MyKeyedTransformer(Dagboek dagboek) throws Exception {
		this.definitie = VrijeRubriekHelper.getVrijeRubriekDefinities(dagboek);
	}

	public Object transform(Object object, Object key) {
		try {
			String s = (String)key;
			String n = "0000000000" + (String) object;
			if (s.startsWith("vrijeRub")) {
				int index = Integer.parseInt(s.substring(s.length() - 1)) - 1;
				switch (definitie[index].getDataType()) {
				case VrijeRubriekSoortEnum.TABELWAARDE:
					break;
				case VrijeRubriekSoortEnum.ALFA:
					break;
				case VrijeRubriekSoortEnum.NUM:
					n = n.substring(n.length() - 10);
					object = n;
					break;
				case VrijeRubriekSoortEnum.DEC:
					n = n.replaceAll("\\.", "");
					n = n.replaceAll("\\,", "");
					n = n.substring(n.length() - 10);
					object = n;
					break;
				case VrijeRubriekSoortEnum.DATUM:
					object = ((String)object).replaceAll("-", "");
					break;
				case VrijeRubriekSoortEnum.BOOL:
					object = Boolean.valueOf((String) object).toString();
					break;
				}
			}
		} catch (Exception e) {
		}
		return object;
	}
}