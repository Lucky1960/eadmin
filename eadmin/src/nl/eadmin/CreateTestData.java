package nl.eadmin;

import java.math.BigDecimal;
import java.util.Calendar;

import nl.eadmin.db.AdresDataBean;
import nl.eadmin.db.AdresManager;
import nl.eadmin.db.AdresManagerFactory;
import nl.eadmin.db.AdresPK;
import nl.eadmin.db.Bedrijf;
import nl.eadmin.db.BedrijfDataBean;
import nl.eadmin.db.BedrijfManager;
import nl.eadmin.db.BedrijfManagerFactory;
import nl.eadmin.db.BtwCodeDataBean;
import nl.eadmin.db.BtwCodeManager;
import nl.eadmin.db.BtwCodeManagerFactory;
import nl.eadmin.db.BtwCodePK;
import nl.eadmin.db.CrediteurDataBean;
import nl.eadmin.db.CrediteurManager;
import nl.eadmin.db.CrediteurManagerFactory;
import nl.eadmin.db.CrediteurPK;
import nl.eadmin.db.DagboekDataBean;
import nl.eadmin.db.DagboekManager;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.DagboekPK;
import nl.eadmin.db.DebiteurDataBean;
import nl.eadmin.db.DebiteurManager;
import nl.eadmin.db.DebiteurManagerFactory;
import nl.eadmin.db.DebiteurPK;
import nl.eadmin.db.GebruikerDataBean;
import nl.eadmin.db.GebruikerManager;
import nl.eadmin.db.GebruikerManagerFactory;
import nl.eadmin.db.HoofdverdichtingDataBean;
import nl.eadmin.db.HoofdverdichtingManager;
import nl.eadmin.db.HoofdverdichtingManagerFactory;
import nl.eadmin.db.HoofdverdichtingPK;
import nl.eadmin.db.InstellingenManagerFactory;
import nl.eadmin.db.PeriodeDataBean;
import nl.eadmin.db.PeriodeManager;
import nl.eadmin.db.PeriodeManagerFactory;
import nl.eadmin.db.PeriodePK;
import nl.eadmin.db.RekeningDataBean;
import nl.eadmin.db.RekeningManager;
import nl.eadmin.db.RekeningManagerFactory;
import nl.eadmin.db.RekeningPK;
import nl.eadmin.db.VerdichtingDataBean;
import nl.eadmin.db.VerdichtingManager;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.db.VerdichtingPK;
import nl.eadmin.enums.AdresTypeEnum;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.eadmin.enums.RekeningToonKolomEnum;
import nl.ibs.jsql.exception.FinderException;
import nl.ibs.util.Scrambler;

public class CreateTestData {

	public static void main(String[] args) throws Exception {
		CreateTestData pgm = new CreateTestData();
		pgm.setup();
		System.exit(0);
	}

	public void setup() throws Exception {
		createUsers();
		createBedrijf();
	}

	private void createUsers() throws Exception {
		// @formatter:off
		String[][] data = new String[][] {
		{ ApplicationConstants.DEFAULT_ADMIN_NAME, "Administrator EAdmin", "HPL", "2015" },
		{ "BEHEERDER", "Beheerder EAdmin", "HPL", "2015" },
		{ "USER", "Beheerder EAdmin", "HPL", "2015" } };
		// @formatter:on
		GebruikerManager manager = GebruikerManagerFactory.getInstance();
		for (int d = 0; d < data.length; d++) {
			try {
				manager.findByPrimaryKey(data[d][0]);
			} catch (FinderException e) {
				GebruikerDataBean bean = new GebruikerDataBean();
				bean.setId(data[d][0]);
				bean.setNaam(data[d][1]);
				bean.setWachtwoord(Scrambler.scramble("eadmin"));
				bean.setBedrijf(data[d][2]);
				bean.setBoekjaar(Integer.parseInt(data[d][3]));
				manager.create(bean);
			}
		}
	}

	private void createBedrijf() throws Exception {
		// @formatter:off
		String[][] data = new String[][] {
		{ "001", "HPVL Consult" },
		{ "002", "Beheerder EAdmin" } };
		// @formatter:on
		Bedrijf bedrijf = null;
		BedrijfManager manager = BedrijfManagerFactory.getInstance();
		BedrijfDataBean bean = new BedrijfDataBean();
		for (int d = 0; d < data.length; d++) {
			try {
				bedrijf = manager.findByPrimaryKey(data[d][0]);
			} catch (FinderException e) {
				bean.setBedrijfscode(data[d][0]);
				bean.setOmschrijving(data[d][1]);
				bedrijf = manager.create(bean);
			}
			InstellingenManagerFactory.getInstance().findOrCreate(bedrijf.getBedrijfscode());
			createPeriodes(bedrijf);
			createDagboeken(bedrijf);
			createBtwCodes(bedrijf);
			createHoofdverdichtingen(bedrijf);
			createVerdichtingen(bedrijf);
			createRekeningen(bedrijf);
			createDebiteuren(bedrijf);
			createCrediteuren(bedrijf);
		}
	}

	private void createPeriodes(Bedrijf bedrijf) throws Exception {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setLenient(true);
		cal2.setLenient(true);
		PeriodeManager manager = PeriodeManagerFactory.getInstance();
		PeriodePK key = new PeriodePK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		for (int jaar = 2010; jaar <= 2025; jaar++) {
			for (int per = 1; per <= 12; per++) {
				key.setBoekjaar(jaar);
				key.setBoekperiode(per);
				try {
					manager.findByPrimaryKey(key);
				} catch (FinderException e) {
					cal1.set(Calendar.YEAR, jaar);
					cal1.set(Calendar.MONTH, per - 1);
					cal1.set(Calendar.DAY_OF_MONTH, 1);
					cal2.set(Calendar.YEAR, jaar);
					cal2.set(Calendar.MONTH, per - 1);
					cal2.set(Calendar.DAY_OF_MONTH, cal1.getActualMaximum(Calendar.DAY_OF_MONTH));
					PeriodeDataBean bean = new PeriodeDataBean();
					bean.setBedrijf(key.getBedrijf());
					bean.setBoekjaar(key.getBoekjaar());
					bean.setBoekperiode(key.getBoekperiode());
					bean.setStartdatum(cal1.getTime());
					bean.setEinddatum(cal2.getTime());
					manager.create(bean);
				}
			}
		}
	}

	private void createDagboeken(Bedrijf bedrijf) throws Exception {
		// @formatter:off
		String[][] data = new String[][]{
		{"BNK1", "Bankboek", DagboekSoortEnum.BANK, "1100", "B@@@@&&#####"},
		{"KAS1", "Kasboek", DagboekSoortEnum.KAS, "1000", "K@@@@&&#####"},
		{"INK1", "Inkoopboek", DagboekSoortEnum.INKOOP, "1600", "I@@@@&&#####"},
		{"VRK1", "Verkoopboek", DagboekSoortEnum.VERKOOP, "1300", "@@@@&&#####"},
		{"MEM1", "Memoriaal", DagboekSoortEnum.MEMO, "", "@@@@&&#####"}	};
		// @formatter:on
		DagboekManager manager = DagboekManagerFactory.getInstance();
		DagboekDataBean bean = new DagboekDataBean();
		DagboekPK key = new DagboekPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		for (int d = 0; d < data.length; d++) {
			try {
				key.setId(data[d][0]);
				manager.findByPrimaryKey(key);
			} catch (FinderException e) {
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setId(data[d][0]);
				bean.setOmschrijving(data[d][1]);
				bean.setSoort(data[d][2]);
				bean.setRekening(data[d][3]);
				bean.setBoekstukMask(data[d][4]);
				bean.setLaatsteBoekstuk("");
				manager.create(bean);
			}
		}
	}

	private void createBtwCodes(Bedrijf bedrijf) throws Exception {
		// @formatter:off
		String[][] data = new String[][] {
		{ "L", "Geen BTW", "E", "0.00", "", "" },
		{ "X", "0%", "E", "0.00", "5B", "1E" },
		{ "V", "BTW verlegd", "I", "0.00", "5B", "3B" },
		{ "E06", "6% Excl.", "E", "6.00", "5B", "1B" },
		{ "E21", "21% Excl.", "E", "21.00", "5B", "1A" },
		{ "I06", "6% Incl.", "I", "6.00", "5B", "1B" },
		{ "I21", "21% Incl.", "I", "21.00", "5B", "1A" } };
		// @formatter:on
		BtwCodeManager manager = BtwCodeManagerFactory.getInstance();
		BtwCodeDataBean bean = new BtwCodeDataBean();
		BtwCodePK key = new BtwCodePK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		for (int d = 0; d < data.length; d++) {
			try {
				key.setCode(data[d][0]);
				manager.findByPrimaryKey(key);
			} catch (FinderException e) {
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setCode(data[d][0]);
				bean.setOmschrijving(data[d][1]);
				bean.setInEx(data[d][2]);
				bean.setPercentage(new BigDecimal(data[d][3]));
				bean.setBtwCatInkoop(data[d][4]);
				bean.setBtwCatVerkoop(data[d][5]);
				manager.create(bean);
			}
		}
	}

	private void createHoofdverdichtingen(Bedrijf bedrijf) throws Exception {
		// @formatter:off
		String[][] data = new String[][] {
		{ "1", "Vaste Activa", RekeningSoortEnum.BALANS },
		{ "2", "Vlottende Activa", RekeningSoortEnum.BALANS },
		{ "3", "Eigen vermogen", RekeningSoortEnum.BALANS },
		{ "4", "Vreemd vermogen", RekeningSoortEnum.BALANS },
		{ "5", "Bedrijfsopbrengsten", RekeningSoortEnum.RESULT },
		{ "6", "Personeelslasten", RekeningSoortEnum.RESULT },
		{ "7", "Afschrijvingen", RekeningSoortEnum.RESULT },
		{ "8", "Overige bedrijfslasten", RekeningSoortEnum.RESULT },
		{ "9", "Financi�le baten en lasten", RekeningSoortEnum.RESULT },
		{ "10", "Belastingen", RekeningSoortEnum.RESULT } };
		// @formatter:on
		HoofdverdichtingManager manager = HoofdverdichtingManagerFactory.getInstance();
		HoofdverdichtingDataBean bean = new HoofdverdichtingDataBean();
		HoofdverdichtingPK key = new HoofdverdichtingPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		for (int d = 0; d < data.length; d++) {
			try {
				key.setId(Integer.parseInt(data[d][0]));
				manager.findByPrimaryKey(key);
			} catch (FinderException e) {
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setId(Integer.parseInt(data[d][0]));
				bean.setOmschrijving(data[d][1]);
				bean.setRekeningsoort(data[d][2]);
				manager.create(bean);
			}
		}
	}

	private void createVerdichtingen(Bedrijf bedrijf) throws Exception {
		// @formatter:off
		String[][] data = new String[][] {
		{ "11", "Immateri�le vaste activa", "1" },
		{ "12", "Materi�le vaste activa", "1" },
		{ "13", "Financi�le vaste activa", "1" },
		{ "21", "Handelsvoorraden", "2" },
		{ "22", "Vorderingen", "2" },
		{ "23", "Effecten", "2" },
		{ "24", "Liquide middelen", "2" },
		{ "25", "Tussenrekeningen", "2" },
		{ "31", "Kapitaal", "3" },
		{ "41", "Kredietinstellingen lang", "4" },
		{ "44", "Overige schulden lang", "4" },
		{ "48", "Belastingen/Sociale lasten", "4" },
		{ "49", "Overige schulden kort", "4" },
		{ "51", "Netto omzet", "5" },
		{ "52", "Kostprijs van de omzet", "5" },
		{ "53", "Overige bedrijfsopbrengst", "5" },
		{ "60", "Lonen en salarissen", "6" },
		{ "61", "Sociale lasten", "6" },
		{ "62", "Overige personeelskosten", "6" },
		{ "70", "Afschrijving materi�le vasste activa", "7" },
		{ "71", "Afschrijving immateri�le vaste activa", "7" },
		{ "80", "Huisvestingskosten", "8" },
		{ "81", "Autokosten", "8" },
		{ "82", "Verkoopkosten", "8" },
		{ "83", "Distributiekosten", "8" },
		{ "90", "Rente baten", "9" },
		{ "91", "Rente- en overige financi�le lasten", "9" },
		{ "92", "Opbrengst overige activa", "9" },
		{ "98", "Belastingen", "10" } };
		// @formatter:on
		VerdichtingManager manager = VerdichtingManagerFactory.getInstance();
		VerdichtingDataBean bean = new VerdichtingDataBean();
		VerdichtingPK key = new VerdichtingPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		for (int d = 0; d < data.length; d++) {
			try {
				key.setId(Integer.parseInt(data[d][0]));
				manager.findByPrimaryKey(key);
			} catch (FinderException e) {
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setId(Integer.parseInt(data[d][0]));
				bean.setOmschrijving(data[d][1]);
				bean.setHoofdverdichting(Integer.parseInt(data[d][2]));
				manager.create(bean);
			}
		}
	}

	private void createRekeningen(Bedrijf bedrijf) throws Exception {
		// @formatter:off
		String[][] data = new String[][] {
		{ "0100", "12", "   ", "I", " ", " ", "Gebouwen" },
		{ "0150", "12", "   ", "I", " ", " ", "Inventaris" },
		{ "0300", "12", "   ", "I", " ", " ", "Transportmiddelen" },
		{ "0500", "13", "   ", "I", " ", " ", "Leningen" },
		{ "0600", "31", "   ", "I", " ", " ", "Eigen vermogen" },
		{ "0650", "31", "   ", "I", " ", " ", "FOR firmant" },
		{ "0700", "21", "   ", "I", " ", " ", "Voorraad" },
		{ "1000", "24", "   ", "I", " ", " ", "Kas" },
		{ "1100", "24", "   ", "I", " ", " ", "ING 4260302" },
		{ "1300", "22", "   ", "I", " ", "D", "Debiteuren" },
		{ "1305", "22", "   ", "I", " ", "D", "Debiteuren oude jaren" },
		{ "1310", "22", "   ", "I", " ", " ", "Te veel/te weinig betaald" },
		{ "1320", "22", "   ", "I", " ", " ", "Voorziening dubieuze debiteuren" },
		{ "1360", "22", "   ", "I", " ", " ", "Voorgeschoten kosten" },
		{ "1380", "22", "   ", "I", " ", " ", "Rek CRT ZeeuwFM" },
		{ "1382", "22", "   ", "I", " ", " ", "Rek CRT Vrienden van ZeeuwFM" },
		{ "1600", "49", "   ", "I", " ", "C", "Crediteuren" },
		{ "1605", "49", "   ", "I", " ", "C", "Crediteuren voorgaande jaren" },
		{ "1610", "49", "   ", "I", " ", " ", "Nog te betalen kosten" },
		{ "1710", "48", "   ", "I", " ", " ", "Voorbelasting" },
		{ "1770", "48", "   ", "I", " ", " ", "BTW verkoop hoog" },
		{ "1772", "48", "   ", "I", " ", " ", "BTW verkoop laag" },
		{ "1780", "48", "   ", "I", " ", " ", "Te betalen BTW priv� gebruik" },
		{ "1790", "48", "   ", "I", " ", " ", "Afdracht Omzetbelasting" },
		{ "1800", "31", "   ", "I", " ", " ", "Priv� belastingen IB" },
		{ "1802", "31", "   ", "I", " ", " ", "Priv� belastingen Zorgverz. Wet" },
		{ "1804", "31", "   ", "I", " ", " ", "Priv� belastingen Huurtoeslag" },
		{ "1806", "31", "   ", "I", " ", " ", "Priv� belastingen Zorgtoeslag" },
		{ "1808", "31", "   ", "I", " ", " ", "Priv� belastingen Motorrijtuigen belasting" },
		{ "1850", "31", "   ", "I", " ", " ", "Priv� opnamen algemeen" },
		{ "1852", "31", "   ", "I", " ", " ", "Priv� zorgkosten" },
		{ "1854", "31", "   ", "I", " ", " ", "Priv� studiekosten - Yor en Jad" },
		{ "1860", "31", "   ", "I", " ", " ", "Priv� brandstof" },
		{ "1862", "31", "   ", "I", " ", " ", "Priv� overige autokosten" },
		{ "1870", "31", "   ", "I", " ", " ", "Priv� gebruik telefoon" },
		{ "2000", "25", "   ", "I", " ", " ", "Kruisposten" },
		{ "2050", "25", "   ", "I", " ", " ", "Tussenrekening Visa" },
		{ "2060", "25", "   ", "I", " ", " ", "Tussenrekening import bankmutaties" },
		{ "2070", "25", "   ", "I", " ", " ", "Voorschotten" },
		{ "2090", "25", "   ", "I", " ", " ", "Vraagposten" },
		{ "4000", "60", "   ", "I", " ", " ", "Loonkosten" },
		{ "4050", "60", "   ", "I", " ", " ", "Inleen- en Uitzendkrachten" },
		{ "4100", "70", "   ", "I", " ", " ", "Afschrijvingen" },
		{ "4300", "91", "   ", "I", " ", " ", "Hypotheekrente" },
		{ "4310", "91", "   ", "I", " ", " ", "Rente lening o/g" },
		{ "4320", "91", "   ", "I", " ", " ", "Bankrente" },
		{ "4330", "91", "   ", "I", " ", " ", "Bankkosten" },
		{ "4400", "81", "   ", "I", " ", " ", "Brandstofkosten auto" },
		{ "4410", "81", "   ", "I", " ", " ", "Verzekering auto" },
		{ "4420", "81", "   ", "I", " ", " ", "Wegenbelasting" },
		{ "4430", "81", "   ", "I", " ", " ", "Reparatie en onderhoud auto" },
		{ "4450", "81", "   ", "I", " ", " ", "Overige autokosten" },
		{ "4500", "82", "   ", "I", " ", " ", "Reclamekosten" },
		{ "4510", "82", "   ", "I", " ", " ", "Representatiekosten" },
		{ "4520", "84", "   ", "I", " ", " ", "Kantoorkosten en Porti" },
		{ "4525", "84", "   ", "I", " ", " ", "Computerbenodigdheden en licenties" },
		{ "4530", "84", "   ", "I", " ", " ", "Contributies en abonnementen" },
		{ "4540", "84", "   ", "I", " ", " ", "Verzekering algemeen" },
		{ "4600", "84", "   ", "I", " ", " ", "Telefoon en internet" },
		{ "4700", "82", "   ", "I", " ", " ", "Reiskosten" },
		{ "4710", "84", "   ", "I", " ", " ", "Tunnelkosten" },
		{ "4720", "84", "   ", "I", " ", " ", "Verblijfkosten" },
		{ "4730", "84", "   ", "I", " ", " ", "Verteer" },
		{ "4800", "84", "   ", "I", " ", " ", "Administratie- en Accountantskosten" },
		{ "4900", "84", "   ", "I", " ", " ", "Overige algemene kosten" },
		{ "7000", "52", "   ", "I", " ", " ", "Inkoop Excel consult" },
		{ "7005", "52", "   ", "I", " ", " ", "Inkoop Internet/Websites" },
		{ "7010", "52", "   ", "I", " ", " ", "Inkoop Reclame/Media" },
		{ "7015", "52", "   ", "I", " ", " ", "Inkoop Overige Diensten" },
		{ "8000", "51", "E21", "V", " ", " ", "Omzet Excel consult BTW hoog" },
		{ "8005", "51", "E21", "V", " ", " ", "Omzet Internet/Websites BTW hoog" },
		{ "8010", "51", "E21", "V", " ", " ", "Omzet Reclame/Media BTW hoog" },
		{ "8015", "51", "E21", "V", " ", " ", "Omzet Overige Diensten BTW hoog" },
		{ "8020", "51", "E21", "V", " ", " ", "Omzet Training" },
		{ "8040", "51", "E21", "V", " ", " ", "Reiskosten vergoeding" },
		{ "8500", "51", "X  ", "V", " ", " ", "Omzet 0% BTW" },
		{ "8600", "51", "V  ", "V", " ", " ", "Omzet BTW verlegd" },
		{ "9000", "90", "   ", "I", " ", " ", "Rente baten" } };
		// @formatter:on
		RekeningManager manager = RekeningManagerFactory.getInstance();
		RekeningDataBean bean = new RekeningDataBean();
		RekeningPK key = new RekeningPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		for (int d = 0; d < data.length; d++) {
			try {
				key.setRekeningNr(data[d][0]);
				manager.findByPrimaryKey(key);
			} catch (FinderException e) {
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setRekeningNr(data[d][0]);
				bean.setVerdichting(Integer.parseInt(data[d][1]));
				bean.setBtwCode(data[d][2]);
				bean.setBtwSoort(data[d][3]);
				bean.setICPSoort(data[d][4]);
				bean.setSubAdministratieType(data[d][5]);
				bean.setOmschrijving(data[d][6]);
				bean.setOmschrijvingKort("");
				bean.setToonKolom(RekeningToonKolomEnum.SALDO);
				bean.setBlocked(false);
				manager.create(bean);
			}
		}
	}

	private void createDebiteuren(Bedrijf bedrijf) throws Exception {
		// @formatter:off
		String[][] data = new String[][] {
		{ "D1801", "Promo products     ", "Verl. van Steenbergenlaan", "29", "4537 BB", "Terneuzen " },
		{ "D1802", "NHI-opleidingen Tnz", "Verl. van Steenbergenlaan", "29", "4537 BB", "Terneuzen " },
		{ "D1803", "BCNed bv           ", "Industrieweg             ", "01", "4382 NA", "Vlissingen" },
		{ "D1804", "NHI-opleidingen Vls", "Industrieweg             ", "01", "4382 NA", "Vlissingen" },
		{ "D1805", "A & L Techniek     ", "Dijkwelseweg             ", "38", "4421 PP", "Kapelle   " },
		{ "D1806", "Provincie Zeeland  ", "                         ", "00", "       ", "Middelburg" },
		{ "D9999", "Diverse debiteuren ", "                         ", "00", "       ", "          " } };
		// @formatter:on
		DebiteurManager manager = DebiteurManagerFactory.getInstance();
		DebiteurDataBean bean = new DebiteurDataBean();
		DebiteurPK key = new DebiteurPK();
		AdresManager manager2 = AdresManagerFactory.getInstance();
		AdresDataBean bean2 = new AdresDataBean();
		AdresPK key2 = new AdresPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key2.setBedrijf(bedrijf.getBedrijfscode());
		key2.setDc("D");
		key2.setAdresType(AdresTypeEnum.ADRESTYPE_BEZOEK);
		for (int d = 0; d < data.length; d++) {
			try {
				key.setDebNr(data[d][0]);
				manager.findByPrimaryKey(key);
			} catch (FinderException e) {
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setDebNr(data[d][0]);
				bean.setNaam(data[d][1]);
				bean.setBetaalTermijn(14);
				bean.setBtwNr("BTW123456789");
				bean.setContactPersoon("T.a.v. Dhr. L. Rozewater");
				bean.setEmail("debiteur@eadmin.nl");
				bean.setKvkNr("12345678");
				manager.create(bean);
			}
			try {
				key2.setDcNr(data[d][0]);
				manager2.findByPrimaryKey(key2);
			} catch (FinderException e) {
				bean2.setBedrijf(bedrijf.getBedrijfscode());
				bean2.setDcNr(data[d][0]);
				bean2.setAdresType(AdresTypeEnum.ADRESTYPE_BEZOEK);
				bean2.setStraat(data[d][2]);
				bean2.setHuisNr(Integer.parseInt(data[d][3]));
				bean2.setPostcode(data[d][4]);
				bean2.setPlaats(data[d][5]);
				manager2.create(bean2);
			}
		}
	}

	private void createCrediteuren(Bedrijf bedrijf) throws Exception {
		// @formatter:off
		String[][] data = new String[][] {
		{ "C16000", "Diverse crediteuren", "                         ", "00", "       ", "         " },
		{ "C16001", "Promo Products     ", "Verl. van Steenbergenlaan", "29", "4537 BB", "Terneuzen" },
		{ "C16002", "Telfort            ", "                         ", "00", "       ", "         " },
		{ "C16003", "KPN                ", "                         ", "00", "       ", "         " },
		{ "C16004", "Ziggo              ", "                         ", "00", "       ", "         " },
		{ "C16005", "Media Markt        ", "                         ", "00", "       ", "         " },
		{ "C16006", "Saturn             ", "                         ", "00", "       ", "         " },
		{ "C16007", "PostNL             ", "                         ", "00", "       ", "         " },
		{ "C16008", "V & D              ", "                         ", "00", "       ", "         " },
		{ "C16009", "Wennekes           ", "                         ", "00", "       ", "         " },
		{ "C16010", "Maasen Motors      ", "                         ", "00", "       ", "         " },
		{ "C16011", "Ikea               ", "                         ", "00", "       ", "         " } };
		// @formatter:on
		CrediteurManager manager = CrediteurManagerFactory.getInstance();
		CrediteurDataBean bean = new CrediteurDataBean();
		CrediteurPK key = new CrediteurPK();
		AdresManager manager2 = AdresManagerFactory.getInstance();
		AdresDataBean bean2 = new AdresDataBean();
		AdresPK key2 = new AdresPK();
		key.setBedrijf(bedrijf.getBedrijfscode());
		key2.setBedrijf(bedrijf.getBedrijfscode());
		key2.setDc("C");
		key2.setAdresType(AdresTypeEnum.ADRESTYPE_BEZOEK);
		for (int d = 0; d < data.length; d++) {
			try {
				key.setCredNr(data[d][0]);
				manager.findByPrimaryKey(key);
			} catch (FinderException e) {
				bean.setBedrijf(bedrijf.getBedrijfscode());
				bean.setCredNr(data[d][0]);
				bean.setNaam(data[d][1]);
				bean.setBtwNr("BTW123456789");
				bean.setContactPersoon("T.a.v. Dhr. L. Rozewater");
				bean.setEmail("crediteur@eadmin.nl");
				bean.setKvkNr("12345678");
				manager.create(bean);
			}
			try {
				key2.setDcNr(data[d][0]);
				manager2.findByPrimaryKey(key2);
			} catch (FinderException e) {
				bean2.setBedrijf(bedrijf.getBedrijfscode());
				bean2.setDcNr(data[d][0]);
				bean2.setAdresType(AdresTypeEnum.ADRESTYPE_BEZOEK);
				bean2.setStraat(data[d][2]);
				bean2.setHuisNr(Integer.parseInt(data[d][3]));
				bean2.setPostcode(data[d][4]);
				bean2.setPlaats(data[d][5]);
				manager2.create(bean2);
			}
		}
	}
}