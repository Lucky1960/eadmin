package nl.eadmin;

public class SessionKeys {
	public static final String ACTIVE_DB_DATA = "eaActiveDBData";
	public static final String INIT_REQUEST = "eaInitializationRequest";
	public static final String GEBRUIKER = "eaGebruiker";
	public static final String GEBRUIKER_ID = "eaGebruikerId";
	public static final String BEDRIJF = "eaBedrijf";
	public static final String BEDRIJF_ID = "eaBedrijfId";
	public static final String BOEKJAAR = "eaBoekjaar";
	public static final String DAGBOEK = "eaDagboek";
	public static final String DAGBOEKSOORT = "eaDagboekSoort";
	public static final String BOEKING = "eaBoeking";
	public static final String BOEKINGSREGEL = "eaBoekingsRegel";
	public static final String PRODUCT_DATA = "eaProductData";
	public static final String LICENSE_MSG = "eaLicenceMessage";
	public static final String USER_IS_ADMIN = "eaUserIsAdmin";
	public static final String ROLES = "eaRoles";
}
