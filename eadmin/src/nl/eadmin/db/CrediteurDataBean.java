package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class CrediteurDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String credNr =  "";
    protected String naam =  "";
    protected String contactPersoon =  "";
    protected String email =  "";
    protected String kvkNr =  "";
    protected String btwNr =  "";
    protected String ibanNr =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public CrediteurDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getCredNr (){
      return credNr;
    }

    public String getNaam (){
      return naam;
    }

    public String getContactPersoon (){
      return contactPersoon;
    }

    public String getEmail (){
      return email;
    }

    public String getKvkNr (){
      return kvkNr;
    }

    public String getBtwNr (){
      return btwNr;
    }

    public String getIbanNr (){
      return ibanNr;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setCredNr (String _credNr ){
      credNr = _credNr;
    }

    public void setNaam (String _naam ){
      naam = _naam;
    }

    public void setContactPersoon (String _contactPersoon ){
      contactPersoon = _contactPersoon;
    }

    public void setEmail (String _email ){
      email = _email;
    }

    public void setKvkNr (String _kvkNr ){
      kvkNr = _kvkNr;
    }

    public void setBtwNr (String _btwNr ){
      btwNr = _btwNr;
    }

    public void setIbanNr (String _ibanNr ){
      ibanNr = _ibanNr;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
