package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class RekeningPK implements java.io.Serializable {


    private String bedrijf;
    private String rekeningNr;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setRekeningNr (String _rekeningNr ) {
      rekeningNr = _rekeningNr;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getRekeningNr () {
       return rekeningNr;
    }

}
