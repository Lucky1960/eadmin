package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class CrediteurPK implements java.io.Serializable {


    private String bedrijf;
    private String credNr;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setCredNr (String _credNr ) {
      credNr = _credNr;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getCredNr () {
       return credNr;
    }

}
