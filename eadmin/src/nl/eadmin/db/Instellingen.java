package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Instellingen extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String LENGTE_REKENING = "lengteRekening";
    public final String MASK_CRED_NR = "maskCredNr";
    public final String MASK_DEB_NR = "maskDebNr";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String STD_BETAAL_TERMIJN = "stdBetaalTermijn";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public int getStdBetaalTermijn()throws Exception;
    public int getLengteRekening()throws Exception;
    public String getMaskCredNr()throws Exception;
    public String getMaskDebNr()throws Exception;

    public void setStdBetaalTermijn(int _stdBetaalTermijn )throws Exception;
    public void setLengteRekening(int _lengteRekening )throws Exception;
    public void setMaskCredNr(String _maskCredNr )throws Exception;
    public void setMaskDebNr(String _maskDebNr )throws Exception;

    /**
    * @deprecated  replaced by getInstellingenDataBean()!
    */
    public InstellingenDataBean getDataBean()throws Exception;
    public InstellingenDataBean getInstellingenDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(InstellingenDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
