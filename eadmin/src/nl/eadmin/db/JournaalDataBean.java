package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class JournaalDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String dagboekId =  "";
    protected String boekstuk =  "";
    protected int seqNr = 0;
    protected String dagboekSoort =  "";
    protected String rekeningNr =  "";
    protected String rekeningsoort =  "";
    protected String rekeningOmschr =  "";
    protected String dcNummer =  "";
    protected String factuurNr =  "";
    protected Date boekdatum;
    protected int boekjaar = 0;
    protected int boekperiode = 0;
    protected String btwCode =  "";
    protected String iCPSoort =  "";
    protected String btwCategorie =  "";
    protected String btwAangifteKolom =  "";
    protected String dC =  "";
    protected BigDecimal bedragDebet =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal bedragCredit =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected String omschr1 =  "";
    protected String omschr2 =  "";
    protected String omschr3 =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public JournaalDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getDagboekId (){
      return dagboekId;
    }

    public String getBoekstuk (){
      return boekstuk;
    }

    public int getSeqNr (){
      return seqNr;
    }

    public String getDagboekSoort (){
      return dagboekSoort;
    }

    public String getRekeningNr (){
      return rekeningNr;
    }

    public String getRekeningsoort (){
      return rekeningsoort;
    }

    public String getRekeningOmschr (){
      return rekeningOmschr;
    }

    public String getDcNummer (){
      return dcNummer;
    }

    public String getFactuurNr (){
      return factuurNr;
    }

    public Date getBoekdatum (){
      return boekdatum;
    }

    public int getBoekjaar (){
      return boekjaar;
    }

    public int getBoekperiode (){
      return boekperiode;
    }

    public String getBtwCode (){
      return btwCode;
    }

    public String getICPSoort (){
      return iCPSoort;
    }

    public String getBtwCategorie (){
      return btwCategorie;
    }

    public String getBtwAangifteKolom (){
      return btwAangifteKolom;
    }

    public String getDC (){
      return dC;
    }

    public BigDecimal getBedragDebet (){
      return bedragDebet;
    }

    public BigDecimal getBedragCredit (){
      return bedragCredit;
    }

    public String getOmschr1 (){
      return omschr1;
    }

    public String getOmschr2 (){
      return omschr2;
    }

    public String getOmschr3 (){
      return omschr3;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ){
      dagboekId = _dagboekId;
    }

    public void setBoekstuk (String _boekstuk ){
      boekstuk = _boekstuk;
    }

    public void setSeqNr (int _seqNr ){
      seqNr = _seqNr;
    }

    public void setDagboekSoort (String _dagboekSoort ){
      dagboekSoort = _dagboekSoort;
    }

    public void setRekeningNr (String _rekeningNr ){
      rekeningNr = _rekeningNr;
    }

    public void setRekeningsoort (String _rekeningsoort ){
      rekeningsoort = _rekeningsoort;
    }

    public void setRekeningOmschr (String _rekeningOmschr ){
      rekeningOmschr = _rekeningOmschr;
    }

    public void setDcNummer (String _dcNummer ){
      dcNummer = _dcNummer;
    }

    public void setFactuurNr (String _factuurNr ){
      factuurNr = _factuurNr;
    }

    public void setBoekdatum (Date _boekdatum ){
      boekdatum = _boekdatum;
    }

    public void setBoekjaar (int _boekjaar ){
      boekjaar = _boekjaar;
    }

    public void setBoekperiode (int _boekperiode ){
      boekperiode = _boekperiode;
    }

    public void setBtwCode (String _btwCode ){
      btwCode = _btwCode;
    }

    public void setICPSoort (String _iCPSoort ){
      iCPSoort = _iCPSoort;
    }

    public void setBtwCategorie (String _btwCategorie ){
      btwCategorie = _btwCategorie;
    }

    public void setBtwAangifteKolom (String _btwAangifteKolom ){
      btwAangifteKolom = _btwAangifteKolom;
    }

    public void setDC (String _dC ){
      dC = _dC;
    }

    public void setBedragDebet (BigDecimal _bedragDebet ){
      bedragDebet = _bedragDebet;
    }

    public void setBedragCredit (BigDecimal _bedragCredit ){
      bedragCredit = _bedragCredit;
    }

    public void setOmschr1 (String _omschr1 ){
      omschr1 = _omschr1;
    }

    public void setOmschr2 (String _omschr2 ){
      omschr2 = _omschr2;
    }

    public void setOmschr3 (String _omschr3 ){
      omschr3 = _omschr3;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
