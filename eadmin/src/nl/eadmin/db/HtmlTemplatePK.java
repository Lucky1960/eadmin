package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class HtmlTemplatePK implements java.io.Serializable {


    private String bedrijf;
    private String id;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setId (String _id ) {
      id = _id;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getId () {
       return id;
    }

}
