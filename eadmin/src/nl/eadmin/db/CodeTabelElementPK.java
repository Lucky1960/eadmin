package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class CodeTabelElementPK implements java.io.Serializable {


    private String bedrijf;
    private String tabelNaam;
    private String id;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setTabelNaam (String _tabelNaam ) {
      tabelNaam = _tabelNaam;
    }

    public void setId (String _id ) {
      id = _id;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getTabelNaam () {
       return tabelNaam;
    }

    public String getId () {
       return id;
    }

}
