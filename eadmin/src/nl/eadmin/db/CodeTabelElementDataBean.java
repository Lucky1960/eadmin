package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class CodeTabelElementDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String tabelNaam =  "";
    protected String id =  "";
    protected String omschrijving =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public CodeTabelElementDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getTabelNaam (){
      return tabelNaam;
    }

    public String getId (){
      return id;
    }

    public String getOmschrijving (){
      return omschrijving;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setTabelNaam (String _tabelNaam ){
      tabelNaam = _tabelNaam;
    }

    public void setId (String _id ){
      id = _id;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
