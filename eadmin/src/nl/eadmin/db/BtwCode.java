package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface BtwCode extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String BTW_CAT_INKOOP = "btwCatInkoop";
    public final String BTW_CAT_VERKOOP = "btwCatVerkoop";
    public final String CODE = "code";
    public final String IN_EX = "inEx";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";
    public final String PERCENTAGE = "percentage";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getCode()throws Exception;
    public String getOmschrijving()throws Exception;
    public BigDecimal getPercentage()throws Exception;
    public String getInEx()throws Exception;
    public String getBtwCatInkoop()throws Exception;
    public String getBtwCatVerkoop()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setPercentage(BigDecimal _percentage )throws Exception;
    public void setInEx(String _inEx )throws Exception;
    public void setBtwCatInkoop(String _btwCatInkoop )throws Exception;
    public void setBtwCatVerkoop(String _btwCatVerkoop )throws Exception;

    /**
    * @deprecated  replaced by getBtwCodeDataBean()!
    */
    public BtwCodeDataBean getDataBean()throws Exception;
    public BtwCodeDataBean getBtwCodeDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(BtwCodeDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
