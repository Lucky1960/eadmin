package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class BtwCategorieDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String code =  "";
    protected String omschrijving =  "";
    protected String rekeningNr =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public BtwCategorieDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getCode (){
      return code;
    }

    public String getOmschrijving (){
      return omschrijving;
    }

    public String getRekeningNr (){
      return rekeningNr;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setCode (String _code ){
      code = _code;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

    public void setRekeningNr (String _rekeningNr ){
      rekeningNr = _rekeningNr;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
