package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Product extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String EENHEID = "eenheid";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHR1 = "omschr1";
    public final String OMSCHR2 = "omschr2";
    public final String OMSCHR3 = "omschr3";
    public final String PRIJS = "prijs";
    public final String PRIJS_INKOOP = "prijsInkoop";
    public final String PRODUCT = "product";
    public final String REKENING_NR = "rekeningNr";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getProduct()throws Exception;
    public String getOmschr1()throws Exception;
    public String getOmschr2()throws Exception;
    public String getOmschr3()throws Exception;
    public String getEenheid()throws Exception;
    public BigDecimal getPrijs()throws Exception;
    public BigDecimal getPrijsInkoop()throws Exception;
    public String getRekeningNr()throws Exception;

    public void setOmschr1(String _omschr1 )throws Exception;
    public void setOmschr2(String _omschr2 )throws Exception;
    public void setOmschr3(String _omschr3 )throws Exception;
    public void setEenheid(String _eenheid )throws Exception;
    public void setPrijs(BigDecimal _prijs )throws Exception;
    public void setPrijsInkoop(BigDecimal _prijsInkoop )throws Exception;
    public void setRekeningNr(String _rekeningNr )throws Exception;

    /**
    * @deprecated  replaced by getProductDataBean()!
    */
    public ProductDataBean getDataBean()throws Exception;
    public ProductDataBean getProductDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(ProductDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
