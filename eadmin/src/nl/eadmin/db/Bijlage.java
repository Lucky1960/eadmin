package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Bijlage extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String BOEKSTUK = "boekstuk";
    public final String DAGBOEK_ID = "dagboekId";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";
    public final String VOLG_NR = "volgNr";

//{CODE-INSERT-BEGIN:-1274708295}
    public final String BESTANDS_NAAM = "bestandsnaam";
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getDagboekId()throws Exception;
    public String getBoekstuk()throws Exception;
    public int getVolgNr()throws Exception;
    public String getBestandsnaam()throws Exception;
    public String getOmschrijving()throws Exception;
    public byte[] getData()throws Exception;

    public void setBestandsnaam(String _bestandsnaam )throws Exception;
    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setData(byte[] _data )throws Exception;

    /**
    * @deprecated  replaced by getBijlageDataBean()!
    */
    public BijlageDataBean getDataBean()throws Exception;
    public BijlageDataBean getBijlageDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(BijlageDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
