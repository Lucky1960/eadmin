package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class BankImportDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String dagboekId =  "";
    protected String ibanNr =  "";
    protected String format =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public BankImportDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getDagboekId (){
      return dagboekId;
    }

    public String getIbanNr (){
      return ibanNr;
    }

    public String getFormat (){
      return format;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ){
      dagboekId = _dagboekId;
    }

    public void setIbanNr (String _ibanNr ){
      ibanNr = _ibanNr;
    }

    public void setFormat (String _format ){
      format = _format;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
