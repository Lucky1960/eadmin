package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface BankAfschrift extends  BusinessObject{

    public final String AFSCHRIFT_NR = "afschriftNr";
    public final String BEDRAG = "bedrag";
    public final String BEDRIJF = "bedrijf";
    public final String BLAD_NR = "bladNr";
    public final String BOEKDATUM = "boekdatum";
    public final String DC = "dC";
    public final String DTL_OMSCHR1 = "dtlOmschr1";
    public final String DTL_OMSCHR2 = "dtlOmschr2";
    public final String DTL_OMSCHR3 = "dtlOmschr3";
    public final String HDR_OMSCHR1 = "hdrOmschr1";
    public final String HDR_OMSCHR2 = "hdrOmschr2";
    public final String MUNTSOORT = "muntsoort";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String NAAM = "naam";
    public final String REKENING = "rekening";
    public final String RUN_NR = "runNr";
    public final String SEQ_NR = "seqNr";
    public final String TEGENREKENING = "tegenrekening";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public int getRunNr()throws Exception;
    public int getSeqNr()throws Exception;
    public String getAfschriftNr()throws Exception;
    public String getBladNr()throws Exception;
    public Date getBoekdatum()throws Exception;
    public String getRekening()throws Exception;
    public String getTegenrekening()throws Exception;
    public String getNaam()throws Exception;
    public String getDC()throws Exception;
    public BigDecimal getBedrag()throws Exception;
    public String getMuntsoort()throws Exception;
    public String getHdrOmschr1()throws Exception;
    public String getHdrOmschr2()throws Exception;
    public String getDtlOmschr1()throws Exception;
    public String getDtlOmschr2()throws Exception;
    public String getDtlOmschr3()throws Exception;
    public String getRemark()throws Exception;
    public String getInputRecord()throws Exception;

    public void setAfschriftNr(String _afschriftNr )throws Exception;
    public void setBladNr(String _bladNr )throws Exception;
    public void setBoekdatum(Date _boekdatum )throws Exception;
    public void setRekening(String _rekening )throws Exception;
    public void setTegenrekening(String _tegenrekening )throws Exception;
    public void setNaam(String _naam )throws Exception;
    public void setDC(String _dC )throws Exception;
    public void setBedrag(BigDecimal _bedrag )throws Exception;
    public void setMuntsoort(String _muntsoort )throws Exception;
    public void setHdrOmschr1(String _hdrOmschr1 )throws Exception;
    public void setHdrOmschr2(String _hdrOmschr2 )throws Exception;
    public void setDtlOmschr1(String _dtlOmschr1 )throws Exception;
    public void setDtlOmschr2(String _dtlOmschr2 )throws Exception;
    public void setDtlOmschr3(String _dtlOmschr3 )throws Exception;
    public void setRemark(String _remark )throws Exception;
    public void setInputRecord(String _inputRecord )throws Exception;

    /**
    * @deprecated  replaced by getBankAfschriftDataBean()!
    */
    public BankAfschriftDataBean getDataBean()throws Exception;
    public BankAfschriftDataBean getBankAfschriftDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(BankAfschriftDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
