package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class BankAfschriftPK implements java.io.Serializable {


    private String bedrijf;
    private int runNr;
    private int seqNr;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setRunNr (int _runNr ) {
      runNr = _runNr;
    }

    public void setSeqNr (int _seqNr ) {
      seqNr = _seqNr;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public int getRunNr () {
       return runNr;
    }

    public int getSeqNr () {
       return seqNr;
    }

}
