package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class PeriodePK implements java.io.Serializable {


    private String bedrijf;
    private int boekjaar;
    private int boekperiode;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setBoekjaar (int _boekjaar ) {
      boekjaar = _boekjaar;
    }

    public void setBoekperiode (int _boekperiode ) {
      boekperiode = _boekperiode;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public int getBoekjaar () {
       return boekjaar;
    }

    public int getBoekperiode () {
       return boekperiode;
    }

}
