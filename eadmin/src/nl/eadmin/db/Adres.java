package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Adres extends  BusinessObject{

    public final String ADRES_TYPE = "adresType";
    public final String BEDRIJF = "bedrijf";
    public final String DC = "dc";
    public final String DC_NR = "dcNr";
    public final String EXTRA_ADRES_REGEL = "extraAdresRegel";
    public final String HUIS_NR = "huisNr";
    public final String HUIS_NR_TOEV = "huisNrToev";
    public final String LAND = "land";
    public final String MOBIEL = "mobiel";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String PLAATS = "plaats";
    public final String POSTCODE = "postcode";
    public final String STRAAT = "straat";
    public final String TELEFOON = "telefoon";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getDc()throws Exception;
    public String getDcNr()throws Exception;
    public int getAdresType()throws Exception;
    public String getStraat()throws Exception;
    public int getHuisNr()throws Exception;
    public String getHuisNrToev()throws Exception;
    public String getExtraAdresRegel()throws Exception;
    public String getPostcode()throws Exception;
    public String getPlaats()throws Exception;
    public String getLand()throws Exception;
    public String getTelefoon()throws Exception;
    public String getMobiel()throws Exception;

    public void setStraat(String _straat )throws Exception;
    public void setHuisNr(int _huisNr )throws Exception;
    public void setHuisNrToev(String _huisNrToev )throws Exception;
    public void setExtraAdresRegel(String _extraAdresRegel )throws Exception;
    public void setPostcode(String _postcode )throws Exception;
    public void setPlaats(String _plaats )throws Exception;
    public void setLand(String _land )throws Exception;
    public void setTelefoon(String _telefoon )throws Exception;
    public void setMobiel(String _mobiel )throws Exception;

    /**
    * @deprecated  replaced by getAdresDataBean()!
    */
    public AdresDataBean getDataBean()throws Exception;
    public AdresDataBean getAdresDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(AdresDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public String getAdresRegel1() throws Exception;
    public String getAdresRegel2() throws Exception;
    public String getAdresRegel3() throws Exception;
    public boolean isEmpty() throws Exception;
//{CODE-INSERT-END:955534258}
}
