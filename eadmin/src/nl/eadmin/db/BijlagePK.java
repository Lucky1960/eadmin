package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class BijlagePK implements java.io.Serializable {


    private String bedrijf;
    private String dagboekId;
    private String boekstuk;
    private int volgNr;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ) {
      dagboekId = _dagboekId;
    }

    public void setBoekstuk (String _boekstuk ) {
      boekstuk = _boekstuk;
    }

    public void setVolgNr (int _volgNr ) {
      volgNr = _volgNr;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getDagboekId () {
       return dagboekId;
    }

    public String getBoekstuk () {
       return boekstuk;
    }

    public int getVolgNr () {
       return volgNr;
    }

}
