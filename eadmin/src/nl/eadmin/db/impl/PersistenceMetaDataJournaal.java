package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataJournaal implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataJournaal();

      persistables.add("nl.eadmin.db.impl.Journaal_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("dagboekId");
      javaVariableNames.add("boekstuk");
      javaVariableNames.add("seqNr");
      javaVariableNames.add("dagboekSoort");
      javaVariableNames.add("rekeningNr");
      javaVariableNames.add("rekeningsoort");
      javaVariableNames.add("rekeningOmschr");
      javaVariableNames.add("dcNummer");
      javaVariableNames.add("factuurNr");
      javaVariableNames.add("boekdatum");
      javaVariableNames.add("boekjaar");
      javaVariableNames.add("boekperiode");
      javaVariableNames.add("btwCode");
      javaVariableNames.add("iCPSoort");
      javaVariableNames.add("btwCategorie");
      javaVariableNames.add("btwAangifteKolom");
      javaVariableNames.add("dC");
      javaVariableNames.add("bedragDebet");
      javaVariableNames.add("bedragCredit");
      javaVariableNames.add("omschr1");
      javaVariableNames.add("omschr2");
      javaVariableNames.add("omschr3");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("dagboekId");
      javaPKVariableNames.add("boekstuk");
      javaPKVariableNames.add("seqNr");

      fieldNames.put("mutationTimeStamp","JOURNAAL_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("DagboekId","DAGBOEK");
      fieldNames.put("Boekstuk","BOEKSTUK");
      fieldNames.put("SeqNr","SEQNR");
      fieldNames.put("DagboekSoort","DAGBOEKSRT");
      fieldNames.put("RekeningNr","REKNR");
      fieldNames.put("Rekeningsoort","REKSOORT");
      fieldNames.put("RekeningOmschr","REKOMSCHR");
      fieldNames.put("DcNummer","DCNR");
      fieldNames.put("FactuurNr","FACTUUR");
      fieldNames.put("Boekdatum","BOEKDTM");
      fieldNames.put("Boekjaar","BOEKJAAR");
      fieldNames.put("Boekperiode","PERIODE");
      fieldNames.put("BtwCode","BTWCODE");
      fieldNames.put("ICPSoort","ICPSOORT");
      fieldNames.put("BtwCategorie","BTWCAT");
      fieldNames.put("BtwAangifteKolom","BTWAANGKOL");
      fieldNames.put("DC","DC");
      fieldNames.put("BedragDebet","BEDRAGDEB");
      fieldNames.put("BedragCredit","BEDRAGCRED");
      fieldNames.put("Omschr1","OMSCHR1");
      fieldNames.put("Omschr2","OMSCHR2");
      fieldNames.put("Omschr3","OMSCHR3");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("DagboekId","DagboekId");
      fieldDescriptions.put("Boekstuk","Boekstuknummer");
      fieldDescriptions.put("SeqNr","Sequence nr binnen boekstuk");
      fieldDescriptions.put("DagboekSoort","DagboekSoort");
      fieldDescriptions.put("RekeningNr","RekeningNummer");
      fieldDescriptions.put("Rekeningsoort","Rekeningsoort Balans/Resultaat");
      fieldDescriptions.put("RekeningOmschr","Omschrijving rekening");
      fieldDescriptions.put("DcNummer","DebCredNummer");
      fieldDescriptions.put("FactuurNr","Factuurnummer");
      fieldDescriptions.put("Boekdatum","Boekdatum");
      fieldDescriptions.put("Boekjaar","Boekjaar");
      fieldDescriptions.put("Boekperiode","Boekperiode");
      fieldDescriptions.put("BtwCode","BTW code");
      fieldDescriptions.put("ICPSoort","ICP soort");
      fieldDescriptions.put("BtwCategorie","BTW type");
      fieldDescriptions.put("BtwAangifteKolom","BTW aangifte kolom O=Omzet, B=Btw");
      fieldDescriptions.put("DC","Debet of Credit");
      fieldDescriptions.put("BedragDebet","Bedrag(D)");
      fieldDescriptions.put("BedragCredit","Bedrag(C)");
      fieldDescriptions.put("Omschr1","Omschrijving 1");
      fieldDescriptions.put("Omschr2","Omschrijving 2");
      fieldDescriptions.put("Omschr3","Omschrijving 3");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("dagboekId","DagboekId");
      javaToBONameMapping.put("boekstuk","Boekstuk");
      javaToBONameMapping.put("seqNr","SeqNr");
      javaToBONameMapping.put("dagboekSoort","DagboekSoort");
      javaToBONameMapping.put("rekeningNr","RekeningNr");
      javaToBONameMapping.put("rekeningsoort","Rekeningsoort");
      javaToBONameMapping.put("rekeningOmschr","RekeningOmschr");
      javaToBONameMapping.put("dcNummer","DcNummer");
      javaToBONameMapping.put("factuurNr","FactuurNr");
      javaToBONameMapping.put("boekdatum","Boekdatum");
      javaToBONameMapping.put("boekjaar","Boekjaar");
      javaToBONameMapping.put("boekperiode","Boekperiode");
      javaToBONameMapping.put("btwCode","BtwCode");
      javaToBONameMapping.put("iCPSoort","ICPSoort");
      javaToBONameMapping.put("btwCategorie","BtwCategorie");
      javaToBONameMapping.put("btwAangifteKolom","BtwAangifteKolom");
      javaToBONameMapping.put("dC","DC");
      javaToBONameMapping.put("bedragDebet","BedragDebet");
      javaToBONameMapping.put("bedragCredit","BedragCredit");
      javaToBONameMapping.put("omschr1","Omschr1");
      javaToBONameMapping.put("omschr2","Omschr2");
      javaToBONameMapping.put("omschr3","Omschr3");

    }

    private PersistenceMetaDataJournaal(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Journaal";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Journaal" , "JOURNAAL");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Journaal" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Journaalposten";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DagboekId", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Boekstuk", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("SeqNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 5, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("DagboekSoort", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("RekeningNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 9, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Rekeningsoort", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("RekeningOmschr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DcNummer", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("FactuurNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Boekdatum", provider)	 + " " + provider.getDBMapping().getSQLDefinition("Date", 0, 0) + "	," + 
				getFieldName("Boekjaar", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 4, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Boekperiode", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 3, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("BtwCode", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 3, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("ICPSoort", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BtwCategorie", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 2, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BtwAangifteKolom", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DC", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BedragDebet", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("BedragCredit", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("Omschr1", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschr2", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschr3", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[24][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("DagboekId", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "DagboekId"};
      columnDefinitions[3]=new String[]{getFieldName("Boekstuk", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Boekstuknummer"};
      columnDefinitions[4]=new String[]{getFieldName("SeqNr", provider), provider.getDBMapping().getSQLDefinition("int", 5, 0) , "0"  , " NOT NULL", "Sequence nr binnen boekstuk"};
      columnDefinitions[5]=new String[]{getFieldName("DagboekSoort", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "DagboekSoort"};
      columnDefinitions[6]=new String[]{getFieldName("RekeningNr", provider), provider.getDBMapping().getSQLDefinition("String", 9, 0) , "''"  , " NOT NULL", "RekeningNummer"};
      columnDefinitions[7]=new String[]{getFieldName("Rekeningsoort", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "Rekeningsoort Balans/Resultaat"};
      columnDefinitions[8]=new String[]{getFieldName("RekeningOmschr", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving rekening"};
      columnDefinitions[9]=new String[]{getFieldName("DcNummer", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "DebCredNummer"};
      columnDefinitions[10]=new String[]{getFieldName("FactuurNr", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Factuurnummer"};
      columnDefinitions[11]=new String[]{getFieldName("Boekdatum", provider), provider.getDBMapping().getSQLDefinition("Date", 0, 0) ,  null  , "", "Boekdatum"};
      columnDefinitions[12]=new String[]{getFieldName("Boekjaar", provider), provider.getDBMapping().getSQLDefinition("int", 4, 0) , "0"  , " NOT NULL", "Boekjaar"};
      columnDefinitions[13]=new String[]{getFieldName("Boekperiode", provider), provider.getDBMapping().getSQLDefinition("int", 3, 0) , "0"  , " NOT NULL", "Boekperiode"};
      columnDefinitions[14]=new String[]{getFieldName("BtwCode", provider), provider.getDBMapping().getSQLDefinition("String", 3, 0) , "''"  , " NOT NULL", "BTW code"};
      columnDefinitions[15]=new String[]{getFieldName("ICPSoort", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "ICP soort"};
      columnDefinitions[16]=new String[]{getFieldName("BtwCategorie", provider), provider.getDBMapping().getSQLDefinition("String", 2, 0) , "''"  , " NOT NULL", "BTW type"};
      columnDefinitions[17]=new String[]{getFieldName("BtwAangifteKolom", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "BTW aangifte kolom O=Omzet, B=Btw"};
      columnDefinitions[18]=new String[]{getFieldName("DC", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "Debet of Credit"};
      columnDefinitions[19]=new String[]{getFieldName("BedragDebet", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Bedrag(D)"};
      columnDefinitions[20]=new String[]{getFieldName("BedragCredit", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Bedrag(C)"};
      columnDefinitions[21]=new String[]{getFieldName("Omschr1", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 1"};
      columnDefinitions[22]=new String[]{getFieldName("Omschr2", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 2"};
      columnDefinitions[23]=new String[]{getFieldName("Omschr3", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 3"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_JOURNAAL_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_JOURNAAL_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DagboekId", provider) + " , "  +  getFieldName("Boekstuk", provider) + " , "  +  getFieldName("SeqNr", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_JOURNAAL", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_JOURNAAL PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DagboekId", provider) + " , "  +  getFieldName("Boekstuk", provider) + " , "  +  getFieldName("SeqNr", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
