package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import nl.eadmin.db.*;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


  public class HeaderDataManager_Impl extends BusinessObjectManager implements HeaderDataManager{


    

    private final static String APPLICATION ="eadmin";
    private static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    private static AttributeAccessor generalAccessor = new AttributeAccessor();
    private static boolean verbose = Log.debug();
    private static PersistenceMetaData pmd = PersistenceMetaDataHeaderData.getInstance();
    private static Hashtable instances = new Hashtable();

    private DBData  dbd;

    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    private HeaderDataManager_Impl(DBData dbData){
       dbd = dbData;
    }


    public static HeaderDataManager getInstance(){
		return getInstance(null);
    }


    public static HeaderDataManager getInstance(DBData dbd){
		HeaderDataManager_Impl inst = (HeaderDataManager_Impl)instances.get(dbd==null?"":dbd);
		if (inst == null){
		  inst = new HeaderDataManager_Impl(dbd);
		  instances.put(dbd==null?"":dbd,inst);
		}
		return inst;
    }


    public DBData getDBData(){
       return dbd==null?DBData.getDefaultDBData(APPLICATION):dbd;
    }


    public HeaderData create(HeaderDataDataBean bean)  throws Exception {
      HeaderData_Impl impl = new HeaderData_Impl(getDBData(),  bean);
        impl.assureStorage();
      return (HeaderData)impl;
    }


    public HeaderData create(String _bedrijf, String _dagboekId, String _boekstuk) throws Exception {
      HeaderData_Impl obj = new HeaderData_Impl(getDBData(), _bedrijf, _dagboekId, _boekstuk);
      obj.assureStorage();
      return obj;
    }


    private HeaderDataPK getPK(ResultSet rs) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      HeaderDataPK inst =  new HeaderDataPK();
      inst.setBedrijf(rs.getString(pmd.getFieldName("Bedrijf",provider)));
      inst.setDagboekId(rs.getString(pmd.getFieldName("DagboekId",provider)));
      inst.setBoekstuk(rs.getString(pmd.getFieldName("Boekstuk",provider)));
      return inst; 
    }


    public HeaderData findOrCreate (HeaderDataPK key) throws Exception {

      HeaderData obj;
      try{
         obj = findByPrimaryKey( key);
      }catch (FinderException e){
         obj = create( key.getBedrijf (),  key.getDagboekId (),  key.getBoekstuk ());
      }

      return obj;
    }


    public void add(HeaderDataDataBean inst)  throws Exception {
        HeaderData_Impl impl = new HeaderData_Impl(getDBData(),  inst);
        impl.assureStorage();
    }


    public void removeByPrimaryKey (HeaderDataPK key) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      HeaderData cachedObject = (HeaderData)getCachedObjectByPrimaryKey(key);
      if (provider instanceof AtomicDBTransaction)
        dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate((BusinessObject_Impl)cachedObject);
      else
        dbc = provider.getConnection();
      String  pskey = "nl.eadmin.db.HeaderData.removeByPrimaryKey";
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider) + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("DagboekId",provider) + "=?" + " AND "  + pmd.getFieldName("Boekstuk",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDagboekId());
          prep.setString(3, key.getBoekstuk());
          int x = prep.executeUpdate();

          if (x == 0) throw new RemoveException("Key not found");
          if (cachedObject != null){
          		DBPersistenceManager.removeFromCache(cachedObject);
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
    }


    public HeaderData findByPrimaryKey (HeaderDataPK key) throws Exception {
      //Custom code
//{CODE-INSERT-BEGIN:-1586979883}
//{CODE-INSERT-END:-1586979883}

      HeaderData headerData = getCachedObjectByPrimaryKey(key);
       if (headerData != null)
      	  return headerData;
       ResultSet rs = null;
       try{
           rs = getResultSet(key);
           return (HeaderData)getBusinessObject(rs);
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }      //Custom code
//{CODE-INSERT-BEGIN:-933211338}
//{CODE-INSERT-END:-933211338}

    }

    public ResultSet getResultSet(HeaderDataPK key) throws Exception{
      ResultSet rs = null;
      String  pskey = "nl.eadmin.db.HeaderData.findByPrimaryKey1";
      Object inst = null;
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " SELECT * " + 
                                           " FROM " + provider.getPrefix() + pmd.getTableName(provider)  + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("DagboekId",provider) + "=?" + " AND "  + pmd.getFieldName("Boekstuk",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDagboekId());
          prep.setString(3, key.getBoekstuk());
          prep.executeQuery();

          rs = prep.getResultSet();

          if (!rs.next()){
                throw new FinderException("Key not found");
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
      return rs;
    }
    public HeaderData getCachedObjectByPrimaryKey (HeaderDataPK key) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache == null)
          return null;
       Object o = cache.get(new String("-657057613_"+ getDBData().getDBId() + key.getBedrijf() + key.getDagboekId() + key.getBoekstuk()));
       return (HeaderData)o;
    }



    public ConnectionProvider getConnectionProvider() throws Exception{
       return DBPersistenceManager.getConnectionProvider(getDBData());
    }


    public PersistenceMetaData getPersistenceMetaData(){
      return pmd;
    }


    public int generalUpdate(String setClause, String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String update = "UPDATE " + provider.getPrefix() + pmd.getTableName(provider)  + " SET " + QueryTranslator.translateObjectQuery(setClause + " WHERE " + whereClause,pmd,provider);
        return dbc.executeUpdate(update);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public int generalDelete(String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String delete = "DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider)  + " WHERE " + QueryTranslator.translateObjectQuery(whereClause,pmd,provider);
        return dbc.executeUpdate(delete);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public HeaderData getFirstObject(Query query) throws Exception{
      if (query == null)
         query = QueryFactory.create();
      int prevMax = query.getMaxObjects();
      query.setMaxObjects(1);
      try{
         return (HeaderData) ((QueryImplementation)query).execute(this, RETURN_FIRST_OBJECT);
      }catch(Exception e){
         throw e;
      }finally{
         query.setMaxObjects(prevMax);
      }
    }

    public Collection getCollection(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Collection) queryImpl.execute(this, RETURN_COLLECTION);
    }

    public ListIterator getListIterator(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ListIterator) queryImpl.execute(this, RETURN_LISTITERATOR);
    }

    public Document getDocument(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Document) queryImpl.execute(this, RETURN_DOCUMENT);
    }

    public DocumentFragment getDocumentFragment(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (DocumentFragment) queryImpl.execute(this, RETURN_DOCUMENT_FRAGMENT);
    }

    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ArrayList) queryImpl.execute(this,RETURN_DOCUMENT_FRAGMENT_ARRAYLIST );
    }



    protected Collection getCollection(ResultSet rs) throws Exception {
      ArrayList result = new ArrayListImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      return (Collection)result;
    }



    protected Document getDocument(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      Element root = document.getDocumentElement();
      collectResultElements(resultSet, root, getConnectionProvider());
      return document;
    };


    protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      DocumentFragment documentFragment = document.createDocumentFragment();
      collectResultElements(resultSet, documentFragment, getConnectionProvider());
      return documentFragment;
    };


    protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      Document document = domImplementation.createDocument("", "data", null);
      ArrayList arrayList = new ArrayListImpl();
      while (resultSet.next()){
        DocumentFragment documentFragment = document.createDocumentFragment();
        Node rec = documentFragment.appendChild(document.createElement("headerdata"));
        addBOFieldsToElement(resultSet, rec, provider);
        arrayList.add(documentFragment);
      }
      return arrayList;
    }


    protected ListIterator getListIterator(ResultSet rs) throws Exception {
      ListIterator result = new ListIteratorImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      ((ListIteratorImpl)result).pointer = 0; 
      return result; 
    }


    protected Object getBusinessObject(ResultSet rs) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache != null){
          ConnectionProvider provider = getConnectionProvider();
          Object o = cache.get(new String("-657057613_"+ getDBData().getDBId()+rs.getObject(pmd.getFieldName("Bedrijf",provider))+rs.getObject(pmd.getFieldName("DagboekId",provider))+rs.getObject(pmd.getFieldName("Boekstuk",provider))));
          if (o != null)
             return o;
       }
    	  return DBPersistenceManager.cache(createBusinessObject(rs));
    }

    private Object createBusinessObject(ResultSet rs) throws Exception {
      return new HeaderData_Impl(getDBData(), rs);
    }


    private void collectResultElements(ResultSet rs, Node root, ConnectionProvider provider) throws Exception {
      Document doc = root.getOwnerDocument();
      while (rs.next()){
        Node rec = root.appendChild(doc.createElement("headerdata"));
        addBOFieldsToElement(rs,rec,provider);
      }
    }


    public static void addBOFieldsToElement(ResultSet rs, Node rec, ConnectionProvider provider) throws Exception {
      DBMapping mapping = provider.getDBMapping();
      Document doc = rec.getOwnerDocument();
      Object o = null;
      String s = null;
      if ((o=rs.getObject(pmd.getFieldName("mutationTimeStamp",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("mutationtimestamp")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Bedrijf",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrijf")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DagboekId",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dagboekid")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DagboekSoort",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dagboeksoort")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Boekstuk",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("boekstuk")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("Status",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("status")).appendChild(doc.createTextNode(s));
      if ((o=rs.getObject(pmd.getFieldName("Favoriet",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("favoriet")).appendChild(doc.createTextNode(s));
      if ((o=rs.getDate(pmd.getFieldName("Boekdatum",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("boekdatum")).appendChild(doc.createTextNode(s));
      if ((o=rs.getObject(pmd.getFieldName("Boekjaar",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("boekjaar")).appendChild(doc.createTextNode(s));
      if ((o=rs.getObject(pmd.getFieldName("Boekperiode",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("boekperiode")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("DcNummer",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dcnummer")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("FactuurNummer",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("factuurnummer")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("ReferentieNummer",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("referentienummer")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getDate(pmd.getFieldName("Factuurdatum",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("factuurdatum")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("FactuurLayout",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("factuurlayout")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getDate(pmd.getFieldName("Vervaldatum",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("vervaldatum")).appendChild(doc.createTextNode(s));
      if ((o=rs.getObject(pmd.getFieldName("AantalAanmaningen",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("aantalaanmaningen")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("DC",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dc")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("TotaalExcl",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("totaalexcl")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("TotaalBtw",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("totaalbtw")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("TotaalIncl",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("totaalincl")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("TotaalGeboekt",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("totaalgeboekt")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("TotaalBetaald",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("totaalbetaald")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("TotaalOpenstaand",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("totaalopenstaand")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("TotaalDebet",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("totaaldebet")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("TotaalCredit",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("totaalcredit")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("Beginsaldo",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("beginsaldo")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("Eindsaldo",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("eindsaldo")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Omschr1",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschr1")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Omschr2",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschr2")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub1",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub1")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub2",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub2")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub3",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub3")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub4",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub4")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub5",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub5")).appendChild(doc.createCDATASection(s));
    }

    public nl.ibs.vegas.ExecutableQuery getExecutableQuery() {
    		return QueryFactory.create(this);
    }

    final static String[] keys = new String[]{ "bedrijf","dagboekId","boekstuk"};
    public String[] getKeyNames() {
    	return keys;
    }
    final static nl.ibs.vegas.meta.AttributeMeta[] attributes = new nl.ibs.vegas.meta.AttributeMeta[]{
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrijf","bedrijf","bedrijf-short","bedrijf-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dagboekId","dagboek-id","dagboek-id-short","dagboek-id-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dagboekSoort","dagboek-soort","dagboek-soort-short","dagboek-soort-description","text",String.class,1,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekstuk","boekstuk","boekstuk-short","boekstuk-description","text",String.class,15,0,null,"false","false","true","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("status","status","status-short","status-description","number",int.class,2,0,"10","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("favoriet","favoriet","favoriet-short","favoriet-description","boolean",boolean.class,0,0,"false","false","false","","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekdatum","boekdatum","boekdatum-short","boekdatum-description","date",Date.class,0,0,"2001-01-01","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekjaar","boekjaar","boekjaar-short","boekjaar-description","number",int.class,4,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekperiode","boekperiode","boekperiode-short","boekperiode-description","number",int.class,3,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dcNummer","dc-nummer","dc-nummer-short","dc-nummer-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("factuurNummer","factuur-nummer","factuur-nummer-short","factuur-nummer-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("referentieNummer","referentie-nummer","referentie-nummer-short","referentie-nummer-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("factuurdatum","factuurdatum","factuurdatum-short","factuurdatum-description","date",Date.class,0,0,"2001-01-01","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("factuurLayout","factuur-layout","factuur-layout-short","factuur-layout-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vervaldatum","vervaldatum","vervaldatum-short","vervaldatum-description","date",Date.class,0,0,"2001-01-01","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("aantalAanmaningen","aantal-aanmaningen","aantal-aanmaningen-short","aantal-aanmaningen-description","number",int.class,2,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dC","dc","dc-short","dc-description","text",String.class,1,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("totaalExcl","totaal-excl","totaal-excl-short","totaal-excl-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("totaalBtw","totaal-btw","totaal-btw-short","totaal-btw-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("totaalIncl","totaal-incl","totaal-incl-short","totaal-incl-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("totaalGeboekt","totaal-geboekt","totaal-geboekt-short","totaal-geboekt-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("totaalBetaald","totaal-betaald","totaal-betaald-short","totaal-betaald-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("totaalOpenstaand","totaal-openstaand","totaal-openstaand-short","totaal-openstaand-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("totaalDebet","totaal-debet","totaal-debet-short","totaal-debet-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("totaalCredit","totaal-credit","totaal-credit-short","totaal-credit-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("beginsaldo","beginsaldo","beginsaldo-short","beginsaldo-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("eindsaldo","eindsaldo","eindsaldo-short","eindsaldo-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschr1","omschr1","omschr1-short","omschr1-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschr2","omschr2","omschr2-short","omschr2-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub1","vrije-rub1","vrije-rub1-short","vrije-rub1-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub2","vrije-rub2","vrije-rub2-short","vrije-rub2-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub3","vrije-rub3","vrije-rub3-short","vrije-rub3-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub4","vrije-rub4","vrije-rub4-short","vrije-rub4-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub5","vrije-rub5","vrije-rub5-short","vrije-rub5-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null)
    };
    public nl.ibs.vegas.meta.AttributeMeta[] getAttributes() {
    	return attributes;
    }
    public Class getClassType() {
    	return nl.eadmin.db.HeaderData.class;
    }
    public String getNameField() {
    	return "boekstuk";
    }
    public String getDescriptionField() {
    	return "boekstuk";
    }
    public String getTextKey() {
    	return "header-data";
    }
    public String getTextKeyShort() {
    	return "header-data-short";
    }
    public String getTextKeyDescription() {
    	return "header-data-description";
    }
    public nl.ibs.vegas.meta.AttributeMeta getAttribute(String name) {
    	if (attributes == null || name == null)
    		return null;
    	for (int i = 0; i < attributes.length; i++)
    		if (name.equals(attributes[i].getName()))
    			return attributes[i];
    	return null;
    }
  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
