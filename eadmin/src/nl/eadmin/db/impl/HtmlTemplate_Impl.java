package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class HtmlTemplate_Impl  implements HtmlTemplate, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataHtmlTemplate.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String id;
    protected String omschrijving;
    protected boolean isDefault;
    protected byte[] logo;
    protected int logoHeight;
    protected byte[] htmlString;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected HtmlTemplate_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected HtmlTemplate_Impl (DBData _dbd ,  String _bedrijf, String _id) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setId(_id);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected HtmlTemplate_Impl (DBData _dbd, HtmlTemplateDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.HtmlTemplateDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate HtmlTemplate with " + bean.getClass().getName() + "!! Use nl.eadmin.db.HtmlTemplateDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setId(bean.getId());
        setOmschrijving(bean.getOmschrijving());
        setIsDefault(bean.getIsDefault());
        setLogo(bean.getLogo());
        setLogoHeight(bean.getLogoHeight());
        setHtmlString(bean.getHtmlString());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public HtmlTemplate_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      id = rs.getString(pmd.getFieldName("Id",provider));
      omschrijving = rs.getString(pmd.getFieldName("Omschrijving",provider));
      isDefault = rs.getBoolean(pmd.getFieldName("IsDefault",provider));
      logo = rs.getBytes(pmd.getFieldName("Logo",provider));
      logoHeight = rs.getInt(pmd.getFieldName("LogoHeight",provider));
      htmlString = rs.getBytes(pmd.getFieldName("HtmlString",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       HtmlTemplatePK key = new HtmlTemplatePK();
       key.setBedrijf(getBedrijf());
       key.setId(getId());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getId()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:349057815}
//{CODE-INSERT-END:349057815}
      return trim(id);
      //Custom code
//{CODE-INSERT-BEGIN:-2064111956}
//{CODE-INSERT-END:-2064111956}
    }


    public String getOmschrijving()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:86851457}
//{CODE-INSERT-END:86851457}
      return omschrijving;
      //Custom code
//{CODE-INSERT-BEGIN:-1602574462}
//{CODE-INSERT-END:-1602574462}
    }


    public boolean getIsDefault()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1377404803}
//{CODE-INSERT-END:1377404803}
      return isDefault;
      //Custom code
//{CODE-INSERT-BEGIN:-250126400}
//{CODE-INSERT-END:-250126400}
    }


    public byte[] getLogo()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-6450769}
//{CODE-INSERT-END:-6450769}

      byte[] obj = null;
	     if (logo != null && logo.length > 0){
	        java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(logo);
	        java.io.ObjectInputStream in = new java.io.ObjectInputStream(bis);
	        obj = (byte[])in.readObject();
	        in.close();
      }

      //Custom code
//{CODE-INSERT-BEGIN:-1524228697}
//{CODE-INSERT-END:-1524228697}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:-6451684}
//{CODE-INSERT-END:-6451684}
    }


    public int getLogoHeight()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1894547758}
//{CODE-INSERT-END:1894547758}
      return logoHeight;
      //Custom code
//{CODE-INSERT-BEGIN:-1398563979}
//{CODE-INSERT-END:-1398563979}
    }


    public String getHtmlString()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:302649438}
//{CODE-INSERT-END:302649438}

      String obj = null;
	     if (htmlString != null && htmlString.length > 0){
	        java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(htmlString);
	        java.io.ObjectInputStream in = new java.io.ObjectInputStream(bis);
	        obj = (String)in.readObject();
	        in.close();
      }

      //Custom code
//{CODE-INSERT-BEGIN:1256688920}
//{CODE-INSERT-END:1256688920}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:302648523}
//{CODE-INSERT-END:302648523}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setId(String _id ) throws Exception {

      if (_id !=  null)
         _id = _id.trim();
      if (_id !=  null)
         _id = _id.toUpperCase();
      if (_id == null)
          _id =  "";
      if (_id.equals(getId()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-2009104245}
//{CODE-INSERT-END:-2009104245}
      id =  _id;

      autoUpdate();

    }

    public void setOmschrijving(String _omschrijving ) throws Exception {

      if (_omschrijving == null)
          _omschrijving =  "";
      if (_omschrijving.equals(getOmschrijving()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-524071947}
//{CODE-INSERT-END:-524071947}
      omschrijving =  _omschrijving;

      autoUpdate();

    }

    public void setIsDefault(boolean _isDefault ) throws Exception {

      if (_isDefault == getIsDefault())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:437251983}
//{CODE-INSERT-END:437251983}
      isDefault =  _isDefault;

      autoUpdate();

    }

    public void setLogo(byte[] _logo ) throws Exception {

      if ((_logo == null ? getLogo() == null : _logo.equals(getLogo())))
         return;
      preSet();

         if ( _logo == null){
            logo = null;
         } else {
            java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
            java.io.ObjectOutputStream out = new java.io.ObjectOutputStream(bos);
            out.writeObject( _logo);
            out.flush();
            out.close();
         logo = bos.toByteArray();
         }

      autoUpdate();

    }

    public void setLogoHeight(int _logoHeight ) throws Exception {

      if (_logoHeight == getLogoHeight())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1480385886}
//{CODE-INSERT-END:-1480385886}
      logoHeight =  _logoHeight;

      autoUpdate();

    }

    public void setHtmlString(String _htmlString ) throws Exception {

      if ((_htmlString == null ? getHtmlString() == null : _htmlString.equals(getHtmlString())))
         return;
      preSet();

         if ( _htmlString == null){
            htmlString = null;
         } else {
            java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
            java.io.ObjectOutputStream out = new java.io.ObjectOutputStream(bos);
            out.writeObject( _htmlString);
            out.flush();
            out.close();
         htmlString = bos.toByteArray();
         }

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      id =  "";
      omschrijving =  "";
      isDefault =  false;
      logoHeight = 60;
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.id = id;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("HtmlTemplate is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         HtmlTemplatePK key = new HtmlTemplatePK();
         key.setBedrijf(getBedrijf());
         key.setId(getId());
         ((nl.eadmin.db.impl.HtmlTemplateManager_Impl)nl.eadmin.db.impl.HtmlTemplateManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((HtmlTemplateManager_Impl)HtmlTemplateManager_Impl.getInstance(dbd)).getResultSet((HtmlTemplatePK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("2077194017_"+ dbd.getDBId() + bedrijf + id).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getHtmlTemplateDataBean()!
    */
    public HtmlTemplateDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public HtmlTemplateDataBean getHtmlTemplateDataBean() throws Exception {
      return getDataBean(null);
    }

    private HtmlTemplateDataBean getDataBean(HtmlTemplateDataBean bean) throws Exception {

      if (bean == null)
          bean = new HtmlTemplateDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setId(getId());
      bean.setOmschrijving(getOmschrijving());
      bean.setIsDefault(getIsDefault());
      bean.setLogo(getLogo());
      bean.setLogoHeight(getLogoHeight());
      bean.setHtmlString(getHtmlString());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.HtmlTemplate_Impl))
             return false;
          nl.eadmin.db.impl.HtmlTemplate_Impl bo = (nl.eadmin.db.impl.HtmlTemplate_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getId() == null){
              if (this.getId() != null)
                  return false;
          }else if (!bo.getId().equals(this.getId())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(HtmlTemplateDataBean bean) throws Exception {
       set(bean);
    }


    public void set(HtmlTemplateDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setId(bean.getId());
           setOmschrijving(bean.getOmschrijving());
           setIsDefault(bean.getIsDefault());
           setLogo(bean.getLogo());
           setLogoHeight(bean.getLogoHeight());
           setHtmlString(bean.getHtmlString());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"HtmlTemplate is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.HtmlTemplate.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("Id",provider) +  " = ? " + ", " + pmd.getFieldName("Omschrijving",provider) +  " = ? " + ", " + pmd.getFieldName("IsDefault",provider) +  " = ? " + ", " + pmd.getFieldName("Logo",provider) +  " = ? " + ", " + pmd.getFieldName("LogoHeight",provider) +  " = ? " + ", " + pmd.getFieldName("HtmlString",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("Id",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  id,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Boolean( isDefault),  mapping.getJDBCTypeFor("boolean"));
        if ( logo == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("byte[]"));
       } else {
          prep.setObject(parmIndex++,  logo,  mapping.getJDBCTypeFor("byte[]"));
}
        prep.setObject(parmIndex++,  new Integer( logoHeight),  mapping.getJDBCTypeFor("int"));
        if ( htmlString == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("byte[]"));
       } else {
          prep.setObject(parmIndex++,  htmlString,  mapping.getJDBCTypeFor("byte[]"));
}

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.id,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("HtmlTemplate modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.HtmlTemplate.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.HtmlTemplate.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("Id",provider) + ", " + pmd.getFieldName("Omschrijving",provider) + ", " + pmd.getFieldName("IsDefault",provider) + ", " + pmd.getFieldName("Logo",provider) + ", " + pmd.getFieldName("LogoHeight",provider) + ", " + pmd.getFieldName("HtmlString",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, id,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5,  new Boolean(isDefault),  mapping.getJDBCTypeFor("boolean"));
        if (logo == null)
          prep.setNull(6,  mapping.getJDBCTypeFor("byte[]"));
        else
          prep.setObject(6, logo,  mapping.getJDBCTypeFor("byte[]"));
        prep.setObject(7,  new Integer(logoHeight),  mapping.getJDBCTypeFor("int"));
        if (htmlString == null)
          prep.setNull(8,  mapping.getJDBCTypeFor("byte[]"));
        else
          prep.setObject(8, htmlString,  mapping.getJDBCTypeFor("byte[]"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.id = currentDBImage.id;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.id = transactionImage.dbImage.id;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        id = transactionImage.dbImage.id;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String id;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
