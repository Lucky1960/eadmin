package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataAdres implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataAdres();

      persistables.add("nl.eadmin.db.impl.Adres_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("dc");
      javaVariableNames.add("dcNr");
      javaVariableNames.add("adresType");
      javaVariableNames.add("straat");
      javaVariableNames.add("huisNr");
      javaVariableNames.add("huisNrToev");
      javaVariableNames.add("extraAdresRegel");
      javaVariableNames.add("postcode");
      javaVariableNames.add("plaats");
      javaVariableNames.add("land");
      javaVariableNames.add("telefoon");
      javaVariableNames.add("mobiel");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("dc");
      javaPKVariableNames.add("dcNr");
      javaPKVariableNames.add("adresType");

      fieldNames.put("mutationTimeStamp","ADRES_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("Dc","DC");
      fieldNames.put("DcNr","DCNR");
      fieldNames.put("AdresType","TYPE");
      fieldNames.put("Straat","STRAAT");
      fieldNames.put("HuisNr","HUISNR");
      fieldNames.put("HuisNrToev","HUISNRT");
      fieldNames.put("ExtraAdresRegel","EXTRAADRES");
      fieldNames.put("Postcode","POSTCODE");
      fieldNames.put("Plaats","PLAATS");
      fieldNames.put("Land","LANDCODE");
      fieldNames.put("Telefoon","TELNR1");
      fieldNames.put("Mobiel","TELNR2");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("Dc","Deb/Cred");
      fieldDescriptions.put("DcNr","DebCredNr");
      fieldDescriptions.put("AdresType","AdresType: 0=Post, 1=Bezoek");
      fieldDescriptions.put("Straat","Straatnaam");
      fieldDescriptions.put("HuisNr","Huisnummer");
      fieldDescriptions.put("HuisNrToev","Huisnummer toevoeging");
      fieldDescriptions.put("ExtraAdresRegel","Extra adresRegel");
      fieldDescriptions.put("Postcode","Postcode");
      fieldDescriptions.put("Plaats","Plaats");
      fieldDescriptions.put("Land","ISO code land");
      fieldDescriptions.put("Telefoon","Telefoon");
      fieldDescriptions.put("Mobiel","Telefoon mobiel");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("dc","Dc");
      javaToBONameMapping.put("dcNr","DcNr");
      javaToBONameMapping.put("adresType","AdresType");
      javaToBONameMapping.put("straat","Straat");
      javaToBONameMapping.put("huisNr","HuisNr");
      javaToBONameMapping.put("huisNrToev","HuisNrToev");
      javaToBONameMapping.put("extraAdresRegel","ExtraAdresRegel");
      javaToBONameMapping.put("postcode","Postcode");
      javaToBONameMapping.put("plaats","Plaats");
      javaToBONameMapping.put("land","Land");
      javaToBONameMapping.put("telefoon","Telefoon");
      javaToBONameMapping.put("mobiel","Mobiel");

    }

    private PersistenceMetaDataAdres(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Adres";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Adres" , "ADRES");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Adres" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Adres";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Dc", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DcNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("AdresType", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 1, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Straat", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("HuisNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 5, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("HuisNrToev", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 5, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("ExtraAdresRegel", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Postcode", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 8, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Plaats", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Land", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 3, 0) + " DEFAULT 'NL'" + " NOT NULL	," + 
				getFieldName("Telefoon", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Mobiel", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[14][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("Dc", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "Deb/Cred"};
      columnDefinitions[3]=new String[]{getFieldName("DcNr", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "DebCredNr"};
      columnDefinitions[4]=new String[]{getFieldName("AdresType", provider), provider.getDBMapping().getSQLDefinition("int", 1, 0) , "0"  , " NOT NULL", "AdresType: 0=Post, 1=Bezoek"};
      columnDefinitions[5]=new String[]{getFieldName("Straat", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Straatnaam"};
      columnDefinitions[6]=new String[]{getFieldName("HuisNr", provider), provider.getDBMapping().getSQLDefinition("int", 5, 0) , "0"  , " NOT NULL", "Huisnummer"};
      columnDefinitions[7]=new String[]{getFieldName("HuisNrToev", provider), provider.getDBMapping().getSQLDefinition("String", 5, 0) , "''"  , " NOT NULL", "Huisnummer toevoeging"};
      columnDefinitions[8]=new String[]{getFieldName("ExtraAdresRegel", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Extra adresRegel"};
      columnDefinitions[9]=new String[]{getFieldName("Postcode", provider), provider.getDBMapping().getSQLDefinition("String", 8, 0) , "''"  , " NOT NULL", "Postcode"};
      columnDefinitions[10]=new String[]{getFieldName("Plaats", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Plaats"};
      columnDefinitions[11]=new String[]{getFieldName("Land", provider), provider.getDBMapping().getSQLDefinition("String", 3, 0) , "'NL'"  , " NOT NULL", "ISO code land"};
      columnDefinitions[12]=new String[]{getFieldName("Telefoon", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Telefoon"};
      columnDefinitions[13]=new String[]{getFieldName("Mobiel", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Telefoon mobiel"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_ADRES_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_ADRES_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Dc", provider) + " , "  +  getFieldName("DcNr", provider) + " , "  +  getFieldName("AdresType", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_ADRES", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_ADRES PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Dc", provider) + " , "  +  getFieldName("DcNr", provider) + " , "  +  getFieldName("AdresType", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
