package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataBtwCode implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataBtwCode();

      persistables.add("nl.eadmin.db.impl.BtwCode_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("code");
      javaVariableNames.add("omschrijving");
      javaVariableNames.add("percentage");
      javaVariableNames.add("inEx");
      javaVariableNames.add("btwCatInkoop");
      javaVariableNames.add("btwCatVerkoop");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("code");

      fieldNames.put("mutationTimeStamp","BTWCODE_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("Code","CODE");
      fieldNames.put("Omschrijving","OMSCHR");
      fieldNames.put("Percentage","PERCENTAGE");
      fieldNames.put("InEx","INEX");
      fieldNames.put("BtwCatInkoop","BTWCATINK");
      fieldNames.put("BtwCatVerkoop","BTWCATVRK");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("Code","BTW code");
      fieldDescriptions.put("Omschrijving","Omschrijving");
      fieldDescriptions.put("Percentage","BTW percentage");
      fieldDescriptions.put("InEx","Inclusief/Exclusief");
      fieldDescriptions.put("BtwCatInkoop","Btw-categorie Inkoop");
      fieldDescriptions.put("BtwCatVerkoop","Btw-categorie Verkoop");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("code","Code");
      javaToBONameMapping.put("omschrijving","Omschrijving");
      javaToBONameMapping.put("percentage","Percentage");
      javaToBONameMapping.put("inEx","InEx");
      javaToBONameMapping.put("btwCatInkoop","BtwCatInkoop");
      javaToBONameMapping.put("btwCatVerkoop","BtwCatVerkoop");

    }

    private PersistenceMetaDataBtwCode(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "BtwCode";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("BtwCode" , "BTWCODE");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "BtwCode" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "BTW codes";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Code", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 3, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschrijving", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 20, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Percentage", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 5, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("InEx", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + "	," + 
				getFieldName("BtwCatInkoop", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 2, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BtwCatVerkoop", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 2, 0) + " DEFAULT ''" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[8][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("Code", provider), provider.getDBMapping().getSQLDefinition("String", 3, 0) , "''"  , " NOT NULL", "BTW code"};
      columnDefinitions[3]=new String[]{getFieldName("Omschrijving", provider), provider.getDBMapping().getSQLDefinition("String", 20, 0) , "''"  , " NOT NULL", "Omschrijving"};
      columnDefinitions[4]=new String[]{getFieldName("Percentage", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 5, 2) , "0.00"  , " NOT NULL", "BTW percentage"};
      columnDefinitions[5]=new String[]{getFieldName("InEx", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) ,  null  , "", "Inclusief/Exclusief"};
      columnDefinitions[6]=new String[]{getFieldName("BtwCatInkoop", provider), provider.getDBMapping().getSQLDefinition("String", 2, 0) , "''"  , " NOT NULL", "Btw-categorie Inkoop"};
      columnDefinitions[7]=new String[]{getFieldName("BtwCatVerkoop", provider), provider.getDBMapping().getSQLDefinition("String", 2, 0) , "''"  , " NOT NULL", "Btw-categorie Verkoop"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_BTWCODE_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_BTWCODE_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Code", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_BTWCODE", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_BTWCODE PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Code", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
