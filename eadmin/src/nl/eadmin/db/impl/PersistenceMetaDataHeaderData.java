package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataHeaderData implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataHeaderData();

      persistables.add("nl.eadmin.db.impl.HeaderData_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("dagboekId");
      javaVariableNames.add("dagboekSoort");
      javaVariableNames.add("boekstuk");
      javaVariableNames.add("status");
      javaVariableNames.add("favoriet");
      javaVariableNames.add("boekdatum");
      javaVariableNames.add("boekjaar");
      javaVariableNames.add("boekperiode");
      javaVariableNames.add("dcNummer");
      javaVariableNames.add("factuurNummer");
      javaVariableNames.add("referentieNummer");
      javaVariableNames.add("factuurdatum");
      javaVariableNames.add("factuurLayout");
      javaVariableNames.add("vervaldatum");
      javaVariableNames.add("aantalAanmaningen");
      javaVariableNames.add("dC");
      javaVariableNames.add("totaalExcl");
      javaVariableNames.add("totaalBtw");
      javaVariableNames.add("totaalIncl");
      javaVariableNames.add("totaalGeboekt");
      javaVariableNames.add("totaalBetaald");
      javaVariableNames.add("totaalOpenstaand");
      javaVariableNames.add("totaalDebet");
      javaVariableNames.add("totaalCredit");
      javaVariableNames.add("beginsaldo");
      javaVariableNames.add("eindsaldo");
      javaVariableNames.add("omschr1");
      javaVariableNames.add("omschr2");
      javaVariableNames.add("vrijeRub1");
      javaVariableNames.add("vrijeRub2");
      javaVariableNames.add("vrijeRub3");
      javaVariableNames.add("vrijeRub4");
      javaVariableNames.add("vrijeRub5");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("dagboekId");
      javaPKVariableNames.add("boekstuk");

      fieldNames.put("mutationTimeStamp","HEADERDATA_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("DagboekId","DAGBOEK");
      fieldNames.put("DagboekSoort","DAGBOEKSRT");
      fieldNames.put("Boekstuk","BOEKSTUK");
      fieldNames.put("Status","STATUS");
      fieldNames.put("Favoriet","FAVORIET");
      fieldNames.put("Boekdatum","BOEKDTM");
      fieldNames.put("Boekjaar","BOEKJAAR");
      fieldNames.put("Boekperiode","PERIODE");
      fieldNames.put("DcNummer","DCNR");
      fieldNames.put("FactuurNummer","FACTUUR");
      fieldNames.put("ReferentieNummer","REFERENTIE");
      fieldNames.put("Factuurdatum","FACTUURDTM");
      fieldNames.put("FactuurLayout","FACTLAYOUT");
      fieldNames.put("Vervaldatum","VERVALDTM");
      fieldNames.put("AantalAanmaningen","AANTLAANM");
      fieldNames.put("DC","DC");
      fieldNames.put("TotaalExcl","TOTLEXCL");
      fieldNames.put("TotaalBtw","TOTLBTW");
      fieldNames.put("TotaalIncl","TOTLINCL");
      fieldNames.put("TotaalGeboekt","TOTLGEBKT");
      fieldNames.put("TotaalBetaald","TOTLBTAALD");
      fieldNames.put("TotaalOpenstaand","TOTLOPEN");
      fieldNames.put("TotaalDebet","TOTLDEB");
      fieldNames.put("TotaalCredit","TOTLCRED");
      fieldNames.put("Beginsaldo","SALDOBGN");
      fieldNames.put("Eindsaldo","SALDOEND");
      fieldNames.put("Omschr1","OMSCHR1");
      fieldNames.put("Omschr2","OMSCHR2");
      fieldNames.put("VrijeRub1","VRIJERUB01");
      fieldNames.put("VrijeRub2","VRIJERUB02");
      fieldNames.put("VrijeRub3","VRIJERUB03");
      fieldNames.put("VrijeRub4","VRIJERUB04");
      fieldNames.put("VrijeRub5","VRIJERUB05");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("DagboekId","DagboekId");
      fieldDescriptions.put("DagboekSoort","DagboekSoort");
      fieldDescriptions.put("Boekstuk","Boekstuknummer");
      fieldDescriptions.put("Status","Status");
      fieldDescriptions.put("Favoriet","Favoriet J/N");
      fieldDescriptions.put("Boekdatum","Boekdatum");
      fieldDescriptions.put("Boekjaar","Boekjaar");
      fieldDescriptions.put("Boekperiode","Boekperiode");
      fieldDescriptions.put("DcNummer","DebCredNummer");
      fieldDescriptions.put("FactuurNummer","Factuurnummer");
      fieldDescriptions.put("ReferentieNummer","Referentienummer");
      fieldDescriptions.put("Factuurdatum","Factuurdatum");
      fieldDescriptions.put("FactuurLayout","Factuur-layout");
      fieldDescriptions.put("Vervaldatum","Vervaldatum");
      fieldDescriptions.put("AantalAanmaningen","Aantal aanmaningen");
      fieldDescriptions.put("DC","Debit of Credit");
      fieldDescriptions.put("TotaalExcl","Totaal Excl. BTW");
      fieldDescriptions.put("TotaalBtw","Totaal BTW");
      fieldDescriptions.put("TotaalIncl","Totaal Incl. BTW");
      fieldDescriptions.put("TotaalGeboekt","Totaal tegengeboekt");
      fieldDescriptions.put("TotaalBetaald","Totaal betaald");
      fieldDescriptions.put("TotaalOpenstaand","Totaal openstaand");
      fieldDescriptions.put("TotaalDebet","Totaal Details Debet");
      fieldDescriptions.put("TotaalCredit","Totaal Details Credit");
      fieldDescriptions.put("Beginsaldo","Beginsaldo bankafschrift");
      fieldDescriptions.put("Eindsaldo","Eindsaldo bankafschrift");
      fieldDescriptions.put("Omschr1","Omschrijving 1");
      fieldDescriptions.put("Omschr2","Omschrijving 2");
      fieldDescriptions.put("VrijeRub1","Vrije rubriek 1");
      fieldDescriptions.put("VrijeRub2","Vrije rubriek 2");
      fieldDescriptions.put("VrijeRub3","Vrije rubriek 3");
      fieldDescriptions.put("VrijeRub4","Vrije rubriek 4");
      fieldDescriptions.put("VrijeRub5","Vrije rubriek 5");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("dagboekId","DagboekId");
      javaToBONameMapping.put("dagboekSoort","DagboekSoort");
      javaToBONameMapping.put("boekstuk","Boekstuk");
      javaToBONameMapping.put("status","Status");
      javaToBONameMapping.put("favoriet","Favoriet");
      javaToBONameMapping.put("boekdatum","Boekdatum");
      javaToBONameMapping.put("boekjaar","Boekjaar");
      javaToBONameMapping.put("boekperiode","Boekperiode");
      javaToBONameMapping.put("dcNummer","DcNummer");
      javaToBONameMapping.put("factuurNummer","FactuurNummer");
      javaToBONameMapping.put("referentieNummer","ReferentieNummer");
      javaToBONameMapping.put("factuurdatum","Factuurdatum");
      javaToBONameMapping.put("factuurLayout","FactuurLayout");
      javaToBONameMapping.put("vervaldatum","Vervaldatum");
      javaToBONameMapping.put("aantalAanmaningen","AantalAanmaningen");
      javaToBONameMapping.put("dC","DC");
      javaToBONameMapping.put("totaalExcl","TotaalExcl");
      javaToBONameMapping.put("totaalBtw","TotaalBtw");
      javaToBONameMapping.put("totaalIncl","TotaalIncl");
      javaToBONameMapping.put("totaalGeboekt","TotaalGeboekt");
      javaToBONameMapping.put("totaalBetaald","TotaalBetaald");
      javaToBONameMapping.put("totaalOpenstaand","TotaalOpenstaand");
      javaToBONameMapping.put("totaalDebet","TotaalDebet");
      javaToBONameMapping.put("totaalCredit","TotaalCredit");
      javaToBONameMapping.put("beginsaldo","Beginsaldo");
      javaToBONameMapping.put("eindsaldo","Eindsaldo");
      javaToBONameMapping.put("omschr1","Omschr1");
      javaToBONameMapping.put("omschr2","Omschr2");
      javaToBONameMapping.put("vrijeRub1","VrijeRub1");
      javaToBONameMapping.put("vrijeRub2","VrijeRub2");
      javaToBONameMapping.put("vrijeRub3","VrijeRub3");
      javaToBONameMapping.put("vrijeRub4","VrijeRub4");
      javaToBONameMapping.put("vrijeRub5","VrijeRub5");

    }

    private PersistenceMetaDataHeaderData(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "HeaderData";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("HeaderData" , "HEADERDATA");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "HeaderData" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Kopgegevens";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DagboekId", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DagboekSoort", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Boekstuk", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Status", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 2, 0) + " DEFAULT 10" + " NOT NULL	," + 
				getFieldName("Favoriet", provider)	 + " " + provider.getDBMapping().getSQLDefinition("boolean", 0, 0) + " DEFAULT 'false'" + " NOT NULL	," + 
				getFieldName("Boekdatum", provider)	 + " " + provider.getDBMapping().getSQLDefinition("Date", 0, 0) + " DEFAULT '2001-01-01'" + " NOT NULL	," + 
				getFieldName("Boekjaar", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 4, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Boekperiode", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 3, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("DcNummer", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("FactuurNummer", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("ReferentieNummer", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Factuurdatum", provider)	 + " " + provider.getDBMapping().getSQLDefinition("Date", 0, 0) + "	," + 
				getFieldName("FactuurLayout", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Vervaldatum", provider)	 + " " + provider.getDBMapping().getSQLDefinition("Date", 0, 0) + "	," + 
				getFieldName("AantalAanmaningen", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 2, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("DC", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("TotaalExcl", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("TotaalBtw", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("TotaalIncl", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("TotaalGeboekt", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("TotaalBetaald", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("TotaalOpenstaand", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("TotaalDebet", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("TotaalCredit", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("Beginsaldo", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("Eindsaldo", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("Omschr1", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschr2", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub1", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub2", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub3", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub4", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub5", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[35][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("DagboekId", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "DagboekId"};
      columnDefinitions[3]=new String[]{getFieldName("DagboekSoort", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "DagboekSoort"};
      columnDefinitions[4]=new String[]{getFieldName("Boekstuk", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Boekstuknummer"};
      columnDefinitions[5]=new String[]{getFieldName("Status", provider), provider.getDBMapping().getSQLDefinition("int", 2, 0) , "10"  , " NOT NULL", "Status"};
      columnDefinitions[6]=new String[]{getFieldName("Favoriet", provider), provider.getDBMapping().getSQLDefinition("boolean", 0, 0) , "'false'"  , " NOT NULL", "Favoriet J/N"};
      columnDefinitions[7]=new String[]{getFieldName("Boekdatum", provider), provider.getDBMapping().getSQLDefinition("Date", 0, 0) , "'2001-01-01'"  , " NOT NULL", "Boekdatum"};
      columnDefinitions[8]=new String[]{getFieldName("Boekjaar", provider), provider.getDBMapping().getSQLDefinition("int", 4, 0) , "0"  , " NOT NULL", "Boekjaar"};
      columnDefinitions[9]=new String[]{getFieldName("Boekperiode", provider), provider.getDBMapping().getSQLDefinition("int", 3, 0) , "0"  , " NOT NULL", "Boekperiode"};
      columnDefinitions[10]=new String[]{getFieldName("DcNummer", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "DebCredNummer"};
      columnDefinitions[11]=new String[]{getFieldName("FactuurNummer", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Factuurnummer"};
      columnDefinitions[12]=new String[]{getFieldName("ReferentieNummer", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Referentienummer"};
      columnDefinitions[13]=new String[]{getFieldName("Factuurdatum", provider), provider.getDBMapping().getSQLDefinition("Date", 0, 0) ,  null  , "", "Factuurdatum"};
      columnDefinitions[14]=new String[]{getFieldName("FactuurLayout", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Factuur-layout"};
      columnDefinitions[15]=new String[]{getFieldName("Vervaldatum", provider), provider.getDBMapping().getSQLDefinition("Date", 0, 0) ,  null  , "", "Vervaldatum"};
      columnDefinitions[16]=new String[]{getFieldName("AantalAanmaningen", provider), provider.getDBMapping().getSQLDefinition("int", 2, 0) , "0"  , " NOT NULL", "Aantal aanmaningen"};
      columnDefinitions[17]=new String[]{getFieldName("DC", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "Debit of Credit"};
      columnDefinitions[18]=new String[]{getFieldName("TotaalExcl", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Totaal Excl. BTW"};
      columnDefinitions[19]=new String[]{getFieldName("TotaalBtw", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Totaal BTW"};
      columnDefinitions[20]=new String[]{getFieldName("TotaalIncl", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Totaal Incl. BTW"};
      columnDefinitions[21]=new String[]{getFieldName("TotaalGeboekt", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Totaal tegengeboekt"};
      columnDefinitions[22]=new String[]{getFieldName("TotaalBetaald", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Totaal betaald"};
      columnDefinitions[23]=new String[]{getFieldName("TotaalOpenstaand", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Totaal openstaand"};
      columnDefinitions[24]=new String[]{getFieldName("TotaalDebet", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Totaal Details Debet"};
      columnDefinitions[25]=new String[]{getFieldName("TotaalCredit", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Totaal Details Credit"};
      columnDefinitions[26]=new String[]{getFieldName("Beginsaldo", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Beginsaldo bankafschrift"};
      columnDefinitions[27]=new String[]{getFieldName("Eindsaldo", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Eindsaldo bankafschrift"};
      columnDefinitions[28]=new String[]{getFieldName("Omschr1", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 1"};
      columnDefinitions[29]=new String[]{getFieldName("Omschr2", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 2"};
      columnDefinitions[30]=new String[]{getFieldName("VrijeRub1", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 1"};
      columnDefinitions[31]=new String[]{getFieldName("VrijeRub2", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 2"};
      columnDefinitions[32]=new String[]{getFieldName("VrijeRub3", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 3"};
      columnDefinitions[33]=new String[]{getFieldName("VrijeRub4", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 4"};
      columnDefinitions[34]=new String[]{getFieldName("VrijeRub5", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 5"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_HEADERDATA_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_HEADERDATA_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DagboekId", provider) + " , "  +  getFieldName("Boekstuk", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_HEADERDATA", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_HEADERDATA PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DagboekId", provider) + " , "  +  getFieldName("Boekstuk", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
