package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import nl.eadmin.db.*;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


  public class RekeningManager_Impl extends BusinessObjectManager implements RekeningManager{


    

    private final static String APPLICATION ="eadmin";
    private static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    private static AttributeAccessor generalAccessor = new AttributeAccessor();
    private static boolean verbose = Log.debug();
    private static PersistenceMetaData pmd = PersistenceMetaDataRekening.getInstance();
    private static Hashtable instances = new Hashtable();

    private DBData  dbd;

    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    private RekeningManager_Impl(DBData dbData){
       dbd = dbData;
    }


    public static RekeningManager getInstance(){
		return getInstance(null);
    }


    public static RekeningManager getInstance(DBData dbd){
		RekeningManager_Impl inst = (RekeningManager_Impl)instances.get(dbd==null?"":dbd);
		if (inst == null){
		  inst = new RekeningManager_Impl(dbd);
		  instances.put(dbd==null?"":dbd,inst);
		}
		return inst;
    }


    public DBData getDBData(){
       return dbd==null?DBData.getDefaultDBData(APPLICATION):dbd;
    }


    public Rekening create(RekeningDataBean bean)  throws Exception {
      Rekening_Impl impl = new Rekening_Impl(getDBData(),  bean);
        impl.assureStorage();
      return (Rekening)impl;
    }


    public Rekening create(String _bedrijf, String _rekeningNr) throws Exception {
      Rekening_Impl obj = new Rekening_Impl(getDBData(), _bedrijf, _rekeningNr);
      obj.assureStorage();
      return obj;
    }


    private RekeningPK getPK(ResultSet rs) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      RekeningPK inst =  new RekeningPK();
      inst.setBedrijf(rs.getString(pmd.getFieldName("Bedrijf",provider)));
      inst.setRekeningNr(rs.getString(pmd.getFieldName("RekeningNr",provider)));
      return inst; 
    }


    public Rekening findOrCreate (RekeningPK key) throws Exception {

      Rekening obj;
      try{
         obj = findByPrimaryKey( key);
      }catch (FinderException e){
         obj = create( key.getBedrijf (),  key.getRekeningNr ());
      }

      return obj;
    }


    public void add(RekeningDataBean inst)  throws Exception {
        Rekening_Impl impl = new Rekening_Impl(getDBData(),  inst);
        impl.assureStorage();
    }


    public void removeByPrimaryKey (RekeningPK key) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      Rekening cachedObject = (Rekening)getCachedObjectByPrimaryKey(key);
      if (provider instanceof AtomicDBTransaction)
        dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate((BusinessObject_Impl)cachedObject);
      else
        dbc = provider.getConnection();
      String  pskey = "nl.eadmin.db.Rekening.removeByPrimaryKey";
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider) + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("RekeningNr",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getRekeningNr());
          int x = prep.executeUpdate();

          if (x == 0) throw new RemoveException("Key not found");
          if (cachedObject != null){
          		DBPersistenceManager.removeFromCache(cachedObject);
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
    }


    public Rekening findByPrimaryKey (RekeningPK key) throws Exception {
      //Custom code
//{CODE-INSERT-BEGIN:-1586979883}
//{CODE-INSERT-END:-1586979883}

      Rekening rekening = getCachedObjectByPrimaryKey(key);
       if (rekening != null)
      	  return rekening;
       ResultSet rs = null;
       try{
           rs = getResultSet(key);
           return (Rekening)getBusinessObject(rs);
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }      //Custom code
//{CODE-INSERT-BEGIN:-933211338}
//{CODE-INSERT-END:-933211338}

    }

    public ResultSet getResultSet(RekeningPK key) throws Exception{
      ResultSet rs = null;
      String  pskey = "nl.eadmin.db.Rekening.findByPrimaryKey1";
      Object inst = null;
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " SELECT * " + 
                                           " FROM " + provider.getPrefix() + pmd.getTableName(provider)  + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("RekeningNr",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getRekeningNr());
          prep.executeQuery();

          rs = prep.getResultSet();

          if (!rs.next()){
                throw new FinderException("Key not found");
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
      return rs;
    }
    public Rekening getCachedObjectByPrimaryKey (RekeningPK key) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache == null)
          return null;
       Object o = cache.get(new String("1227762909_"+ getDBData().getDBId() + key.getBedrijf() + key.getRekeningNr()));
       return (Rekening)o;
    }



    public ConnectionProvider getConnectionProvider() throws Exception{
       return DBPersistenceManager.getConnectionProvider(getDBData());
    }


    public PersistenceMetaData getPersistenceMetaData(){
      return pmd;
    }


    public int generalUpdate(String setClause, String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String update = "UPDATE " + provider.getPrefix() + pmd.getTableName(provider)  + " SET " + QueryTranslator.translateObjectQuery(setClause + " WHERE " + whereClause,pmd,provider);
        return dbc.executeUpdate(update);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public int generalDelete(String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String delete = "DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider)  + " WHERE " + QueryTranslator.translateObjectQuery(whereClause,pmd,provider);
        return dbc.executeUpdate(delete);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public Rekening getFirstObject(Query query) throws Exception{
      if (query == null)
         query = QueryFactory.create();
      int prevMax = query.getMaxObjects();
      query.setMaxObjects(1);
      try{
         return (Rekening) ((QueryImplementation)query).execute(this, RETURN_FIRST_OBJECT);
      }catch(Exception e){
         throw e;
      }finally{
         query.setMaxObjects(prevMax);
      }
    }

    public Collection getCollection(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Collection) queryImpl.execute(this, RETURN_COLLECTION);
    }

    public ListIterator getListIterator(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ListIterator) queryImpl.execute(this, RETURN_LISTITERATOR);
    }

    public Document getDocument(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Document) queryImpl.execute(this, RETURN_DOCUMENT);
    }

    public DocumentFragment getDocumentFragment(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (DocumentFragment) queryImpl.execute(this, RETURN_DOCUMENT_FRAGMENT);
    }

    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ArrayList) queryImpl.execute(this,RETURN_DOCUMENT_FRAGMENT_ARRAYLIST );
    }



    protected Collection getCollection(ResultSet rs) throws Exception {
      ArrayList result = new ArrayListImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      return (Collection)result;
    }



    protected Document getDocument(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      Element root = document.getDocumentElement();
      collectResultElements(resultSet, root, getConnectionProvider());
      return document;
    };


    protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      DocumentFragment documentFragment = document.createDocumentFragment();
      collectResultElements(resultSet, documentFragment, getConnectionProvider());
      return documentFragment;
    };


    protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      Document document = domImplementation.createDocument("", "data", null);
      ArrayList arrayList = new ArrayListImpl();
      while (resultSet.next()){
        DocumentFragment documentFragment = document.createDocumentFragment();
        Node rec = documentFragment.appendChild(document.createElement("rekening"));
        addBOFieldsToElement(resultSet, rec, provider);
        arrayList.add(documentFragment);
      }
      return arrayList;
    }


    protected ListIterator getListIterator(ResultSet rs) throws Exception {
      ListIterator result = new ListIteratorImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      ((ListIteratorImpl)result).pointer = 0; 
      return result; 
    }


    protected Object getBusinessObject(ResultSet rs) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache != null){
          ConnectionProvider provider = getConnectionProvider();
          Object o = cache.get(new String("1227762909_"+ getDBData().getDBId()+rs.getObject(pmd.getFieldName("Bedrijf",provider))+rs.getObject(pmd.getFieldName("RekeningNr",provider))));
          if (o != null)
             return o;
       }
    	  return DBPersistenceManager.cache(createBusinessObject(rs));
    }

    private Object createBusinessObject(ResultSet rs) throws Exception {
      return new Rekening_Impl(getDBData(), rs);
    }


    private void collectResultElements(ResultSet rs, Node root, ConnectionProvider provider) throws Exception {
      Document doc = root.getOwnerDocument();
      while (rs.next()){
        Node rec = root.appendChild(doc.createElement("rekening"));
        addBOFieldsToElement(rs,rec,provider);
      }
    }


    public static void addBOFieldsToElement(ResultSet rs, Node rec, ConnectionProvider provider) throws Exception {
      DBMapping mapping = provider.getDBMapping();
      Document doc = rec.getOwnerDocument();
      Object o = null;
      String s = null;
      if ((o=rs.getObject(pmd.getFieldName("mutationTimeStamp",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("mutationtimestamp")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Bedrijf",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrijf")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("RekeningNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("rekeningnr")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Omschrijving",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschrijving")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("OmschrijvingKort",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschrijvingkort")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("Verdichting",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("verdichting")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("ICPSoort",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("icpsoort")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("BtwCode",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("btwcode")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("BtwSoort",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("btwsoort")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("BtwRekening",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("btwrekening")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("SubAdministratieType",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("subadministratietype")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("ToonKolom",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("toonkolom")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("Blocked",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("blocked")).appendChild(doc.createTextNode(s));
    }

    public nl.ibs.vegas.ExecutableQuery getExecutableQuery() {
    		return QueryFactory.create(this);
    }

    final static String[] keys = new String[]{ "bedrijf","rekeningNr"};
    public String[] getKeyNames() {
    	return keys;
    }
    final static nl.ibs.vegas.meta.AttributeMeta[] attributes = new nl.ibs.vegas.meta.AttributeMeta[]{
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrijf","bedrijf","bedrijf-short","bedrijf-description","text",String.class,10,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("rekeningNr","rekening-nr","rekening-nr-short","rekening-nr-description","text",String.class,9,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschrijving","omschrijving","omschrijving-short","omschrijving-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschrijvingKort","omschrijving-kort","omschrijving-kort-short","omschrijving-kort-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("verdichting","verdichting","verdichting-short","verdichting-description","number",int.class,3,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("iCPSoort","i-c-p-soort","i-c-p-soort-short","i-c-p-soort-description","text",String.class,1,0,null,"false","false","false","mixed", false, false,null,new nl.ibs.vegas.input.InputOption[]{new nl.ibs.vegas.input.impl.InputOptionImpl("","Geen"),new nl.ibs.vegas.input.impl.InputOptionImpl("D","Diensten"),new nl.ibs.vegas.input.impl.InputOptionImpl("L","Leveringen")}),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("btwCode","btw-code","btw-code-short","btw-code-description","text",String.class,3,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("btwSoort","btw-soort","btw-soort-short","btw-soort-description","text",String.class,1,0,"I","false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("btwRekening","btw-rekening","btw-rekening-short","btw-rekening-description","boolean",boolean.class,0,0,"false","false","false","","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("subAdministratieType","sub-administratie-type","sub-administratie-type-short","sub-administratie-type-description","text",String.class,1,0,null,"false","false","false","mixed", false, false,null,new nl.ibs.vegas.input.InputOption[]{new nl.ibs.vegas.input.impl.InputOptionImpl("","Geen"),new nl.ibs.vegas.input.impl.InputOptionImpl("D","Debiteurenrekening"),new nl.ibs.vegas.input.impl.InputOptionImpl("C","Crediteurenrekening")}),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("toonKolom","toon-kolom","toon-kolom-short","toon-kolom-description","text",String.class,1,0,"S","false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("blocked","blocked","blocked-short","blocked-description","boolean",boolean.class,0,0,"false","false","false","","", false, false,null,null)
    };
    public nl.ibs.vegas.meta.AttributeMeta[] getAttributes() {
    	return attributes;
    }
    public Class getClassType() {
    	return nl.eadmin.db.Rekening.class;
    }
    public String getNameField() {
    	return "rekeningNr";
    }
    public String getDescriptionField() {
    	return "rekeningNr";
    }
    public String getTextKey() {
    	return "rekening";
    }
    public String getTextKeyShort() {
    	return "rekening-short";
    }
    public String getTextKeyDescription() {
    	return "rekening-description";
    }
    public nl.ibs.vegas.meta.AttributeMeta getAttribute(String name) {
    	if (attributes == null || name == null)
    		return null;
    	for (int i = 0; i < attributes.length; i++)
    		if (name.equals(attributes[i].getName()))
    			return attributes[i];
    	return null;
    }
  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
