package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import nl.eadmin.db.*;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


  public class BankAfschriftManager_Impl extends BusinessObjectManager implements BankAfschriftManager{


    

    private final static String APPLICATION ="eadmin";
    private static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    private static AttributeAccessor generalAccessor = new AttributeAccessor();
    private static boolean verbose = Log.debug();
    private static PersistenceMetaData pmd = PersistenceMetaDataBankAfschrift.getInstance();
    private static Hashtable instances = new Hashtable();

    private DBData  dbd;

    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    private BankAfschriftManager_Impl(DBData dbData){
       dbd = dbData;
    }


    public static BankAfschriftManager getInstance(){
		return getInstance(null);
    }


    public static BankAfschriftManager getInstance(DBData dbd){
		BankAfschriftManager_Impl inst = (BankAfschriftManager_Impl)instances.get(dbd==null?"":dbd);
		if (inst == null){
		  inst = new BankAfschriftManager_Impl(dbd);
		  instances.put(dbd==null?"":dbd,inst);
		}
		return inst;
    }


    public DBData getDBData(){
       return dbd==null?DBData.getDefaultDBData(APPLICATION):dbd;
    }


    public BankAfschrift create(BankAfschriftDataBean bean)  throws Exception {
      BankAfschrift_Impl impl = new BankAfschrift_Impl(getDBData(),  bean);
        impl.assureStorage();
      return (BankAfschrift)impl;
    }


    public BankAfschrift create(String _bedrijf, int _runNr, int _seqNr) throws Exception {
      BankAfschrift_Impl obj = new BankAfschrift_Impl(getDBData(), _bedrijf, _runNr, _seqNr);
      obj.assureStorage();
      return obj;
    }


    private BankAfschriftPK getPK(ResultSet rs) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      BankAfschriftPK inst =  new BankAfschriftPK();
      inst.setBedrijf(rs.getString(pmd.getFieldName("Bedrijf",provider)));
      inst.setRunNr(rs.getInt(pmd.getFieldName("RunNr",provider)));
      inst.setSeqNr(rs.getInt(pmd.getFieldName("SeqNr",provider)));
      return inst; 
    }


    public BankAfschrift findOrCreate (BankAfschriftPK key) throws Exception {

      BankAfschrift obj;
      try{
         obj = findByPrimaryKey( key);
      }catch (FinderException e){
         obj = create( key.getBedrijf (),  key.getRunNr (),  key.getSeqNr ());
      }

      return obj;
    }


    public void add(BankAfschriftDataBean inst)  throws Exception {
        BankAfschrift_Impl impl = new BankAfschrift_Impl(getDBData(),  inst);
        impl.assureStorage();
    }


    public void removeByPrimaryKey (BankAfschriftPK key) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      BankAfschrift cachedObject = (BankAfschrift)getCachedObjectByPrimaryKey(key);
      if (provider instanceof AtomicDBTransaction)
        dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate((BusinessObject_Impl)cachedObject);
      else
        dbc = provider.getConnection();
      String  pskey = "nl.eadmin.db.BankAfschrift.removeByPrimaryKey";
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider) + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("RunNr",provider) + "=?" + " AND "  + pmd.getFieldName("SeqNr",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setInt(2, key.getRunNr());
          prep.setInt(3, key.getSeqNr());
          int x = prep.executeUpdate();

          if (x == 0) throw new RemoveException("Key not found");
          if (cachedObject != null){
          		DBPersistenceManager.removeFromCache(cachedObject);
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
    }


    public BankAfschrift findByPrimaryKey (BankAfschriftPK key) throws Exception {
      //Custom code
//{CODE-INSERT-BEGIN:-1586979883}
//{CODE-INSERT-END:-1586979883}

      BankAfschrift bankAfschrift = getCachedObjectByPrimaryKey(key);
       if (bankAfschrift != null)
      	  return bankAfschrift;
       ResultSet rs = null;
       try{
           rs = getResultSet(key);
           return (BankAfschrift)getBusinessObject(rs);
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }      //Custom code
//{CODE-INSERT-BEGIN:-933211338}
//{CODE-INSERT-END:-933211338}

    }

    public ResultSet getResultSet(BankAfschriftPK key) throws Exception{
      ResultSet rs = null;
      String  pskey = "nl.eadmin.db.BankAfschrift.findByPrimaryKey1";
      Object inst = null;
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " SELECT * " + 
                                           " FROM " + provider.getPrefix() + pmd.getTableName(provider)  + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("RunNr",provider) + "=?" + " AND "  + pmd.getFieldName("SeqNr",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setInt(2, key.getRunNr());
          prep.setInt(3, key.getSeqNr());
          prep.executeQuery();

          rs = prep.getResultSet();

          if (!rs.next()){
                throw new FinderException("Key not found");
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
      return rs;
    }
    public BankAfschrift getCachedObjectByPrimaryKey (BankAfschriftPK key) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache == null)
          return null;
       Object o = cache.get(new String("55298144_"+ getDBData().getDBId() + key.getBedrijf() + key.getRunNr() + key.getSeqNr()));
       return (BankAfschrift)o;
    }



    public ConnectionProvider getConnectionProvider() throws Exception{
       return DBPersistenceManager.getConnectionProvider(getDBData());
    }


    public PersistenceMetaData getPersistenceMetaData(){
      return pmd;
    }


    public int generalUpdate(String setClause, String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String update = "UPDATE " + provider.getPrefix() + pmd.getTableName(provider)  + " SET " + QueryTranslator.translateObjectQuery(setClause + " WHERE " + whereClause,pmd,provider);
        return dbc.executeUpdate(update);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public int generalDelete(String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String delete = "DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider)  + " WHERE " + QueryTranslator.translateObjectQuery(whereClause,pmd,provider);
        return dbc.executeUpdate(delete);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public BankAfschrift getFirstObject(Query query) throws Exception{
      if (query == null)
         query = QueryFactory.create();
      int prevMax = query.getMaxObjects();
      query.setMaxObjects(1);
      try{
         return (BankAfschrift) ((QueryImplementation)query).execute(this, RETURN_FIRST_OBJECT);
      }catch(Exception e){
         throw e;
      }finally{
         query.setMaxObjects(prevMax);
      }
    }

    public Collection getCollection(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Collection) queryImpl.execute(this, RETURN_COLLECTION);
    }

    public ListIterator getListIterator(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ListIterator) queryImpl.execute(this, RETURN_LISTITERATOR);
    }

    public Document getDocument(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Document) queryImpl.execute(this, RETURN_DOCUMENT);
    }

    public DocumentFragment getDocumentFragment(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (DocumentFragment) queryImpl.execute(this, RETURN_DOCUMENT_FRAGMENT);
    }

    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ArrayList) queryImpl.execute(this,RETURN_DOCUMENT_FRAGMENT_ARRAYLIST );
    }



    protected Collection getCollection(ResultSet rs) throws Exception {
      ArrayList result = new ArrayListImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      return (Collection)result;
    }



    protected Document getDocument(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      Element root = document.getDocumentElement();
      collectResultElements(resultSet, root, getConnectionProvider());
      return document;
    };


    protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      DocumentFragment documentFragment = document.createDocumentFragment();
      collectResultElements(resultSet, documentFragment, getConnectionProvider());
      return documentFragment;
    };


    protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      Document document = domImplementation.createDocument("", "data", null);
      ArrayList arrayList = new ArrayListImpl();
      while (resultSet.next()){
        DocumentFragment documentFragment = document.createDocumentFragment();
        Node rec = documentFragment.appendChild(document.createElement("bankafschrift"));
        addBOFieldsToElement(resultSet, rec, provider);
        arrayList.add(documentFragment);
      }
      return arrayList;
    }


    protected ListIterator getListIterator(ResultSet rs) throws Exception {
      ListIterator result = new ListIteratorImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      ((ListIteratorImpl)result).pointer = 0; 
      return result; 
    }


    protected Object getBusinessObject(ResultSet rs) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache != null){
          ConnectionProvider provider = getConnectionProvider();
          Object o = cache.get(new String("55298144_"+ getDBData().getDBId()+rs.getObject(pmd.getFieldName("Bedrijf",provider))+rs.getObject(pmd.getFieldName("RunNr",provider))+rs.getObject(pmd.getFieldName("SeqNr",provider))));
          if (o != null)
             return o;
       }
    	  return DBPersistenceManager.cache(createBusinessObject(rs));
    }

    private Object createBusinessObject(ResultSet rs) throws Exception {
      return new BankAfschrift_Impl(getDBData(), rs);
    }


    private void collectResultElements(ResultSet rs, Node root, ConnectionProvider provider) throws Exception {
      Document doc = root.getOwnerDocument();
      while (rs.next()){
        Node rec = root.appendChild(doc.createElement("bankafschrift"));
        addBOFieldsToElement(rs,rec,provider);
      }
    }


    public static void addBOFieldsToElement(ResultSet rs, Node rec, ConnectionProvider provider) throws Exception {
      DBMapping mapping = provider.getDBMapping();
      Document doc = rec.getOwnerDocument();
      Object o = null;
      String s = null;
      if ((o=rs.getObject(pmd.getFieldName("mutationTimeStamp",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("mutationtimestamp")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Bedrijf",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrijf")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("RunNr",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("runnr")).appendChild(doc.createTextNode(s));
      if ((o=rs.getObject(pmd.getFieldName("SeqNr",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("seqnr")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("AfschriftNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("afschriftnr")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("BladNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bladnr")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getDate(pmd.getFieldName("Boekdatum",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("boekdatum")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Rekening",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("rekening")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Tegenrekening",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("tegenrekening")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Naam",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("naam")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DC",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dc")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("Bedrag",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrag")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Muntsoort",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("muntsoort")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("HdrOmschr1",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("hdromschr1")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("HdrOmschr2",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("hdromschr2")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DtlOmschr1",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dtlomschr1")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DtlOmschr2",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dtlomschr2")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DtlOmschr3",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dtlomschr3")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getBytes(pmd.getFieldName("Remark",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("remark")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBytes(pmd.getFieldName("InputRecord",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("inputrecord")).appendChild(doc.createTextNode(s));
    }

    public nl.ibs.vegas.ExecutableQuery getExecutableQuery() {
    		return QueryFactory.create(this);
    }

    final static String[] keys = new String[]{ "bedrijf","runNr","seqNr"};
    public String[] getKeyNames() {
    	return keys;
    }
    final static nl.ibs.vegas.meta.AttributeMeta[] attributes = new nl.ibs.vegas.meta.AttributeMeta[]{
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrijf","bedrijf","bedrijf-short","bedrijf-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("runNr","run-nr","run-nr-short","run-nr-description","number",int.class,5,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("seqNr","seq-nr","seq-nr-short","seq-nr-description","number",int.class,5,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("afschriftNr","afschrift-nr","afschrift-nr-short","afschrift-nr-description","text",String.class,5,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bladNr","blad-nr","blad-nr-short","blad-nr-description","text",String.class,5,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekdatum","boekdatum","boekdatum-short","boekdatum-description","date",Date.class,0,0,"2001-01-01","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("rekening","rekening","rekening-short","rekening-description","text",String.class,18,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("tegenrekening","tegenrekening","tegenrekening-short","tegenrekening-description","text",String.class,30,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("naam","naam","naam-short","naam-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dC","dc","dc-short","dc-description","text",String.class,1,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrag","bedrag","bedrag-short","bedrag-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("muntsoort","muntsoort","muntsoort-short","muntsoort-description","text",String.class,3,0,"EUR","false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("hdrOmschr1","hdr-omschr1","hdr-omschr1-short","hdr-omschr1-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("hdrOmschr2","hdr-omschr2","hdr-omschr2-short","hdr-omschr2-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dtlOmschr1","dtl-omschr1","dtl-omschr1-short","dtl-omschr1-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dtlOmschr2","dtl-omschr2","dtl-omschr2-short","dtl-omschr2-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dtlOmschr3","dtl-omschr3","dtl-omschr3-short","dtl-omschr3-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("remark","remark","remark-short","remark-description","object",String.class,0,0,null,"false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("inputRecord","input-record","input-record-short","input-record-description","object",String.class,0,0,null,"false","false","false","", false, false,null,null)
    };
    public nl.ibs.vegas.meta.AttributeMeta[] getAttributes() {
    	return attributes;
    }
    public Class getClassType() {
    	return nl.eadmin.db.BankAfschrift.class;
    }
    public String getNameField() {
    	return "seqNr";
    }
    public String getDescriptionField() {
    	return "seqNr";
    }
    public String getTextKey() {
    	return "bank-afschrift";
    }
    public String getTextKeyShort() {
    	return "bank-afschrift-short";
    }
    public String getTextKeyDescription() {
    	return "bank-afschrift-description";
    }
    public nl.ibs.vegas.meta.AttributeMeta getAttribute(String name) {
    	if (attributes == null || name == null)
    		return null;
    	for (int i = 0; i < attributes.length; i++)
    		if (name.equals(attributes[i].getName()))
    			return attributes[i];
    	return null;
    }
  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
