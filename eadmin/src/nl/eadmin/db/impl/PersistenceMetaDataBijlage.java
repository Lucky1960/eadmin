package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataBijlage implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataBijlage();

      persistables.add("nl.eadmin.db.impl.Bijlage_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("dagboekId");
      javaVariableNames.add("boekstuk");
      javaVariableNames.add("volgNr");
      javaVariableNames.add("omschrijving");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("dagboekId");
      javaPKVariableNames.add("boekstuk");
      javaPKVariableNames.add("volgNr");

      fieldNames.put("mutationTimeStamp","BIJLAGE_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("DagboekId","DAGBOEK");
      fieldNames.put("Boekstuk","BOEKSTUK");
      fieldNames.put("VolgNr","VOLGNR");
      fieldNames.put("Bestandsnaam","NAAM");
      fieldNames.put("Omschrijving","OMSCHR");
      fieldNames.put("Data","DATA");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("DagboekId","DagboekId");
      fieldDescriptions.put("Boekstuk","Boekstuknummer");
      fieldDescriptions.put("VolgNr","VolgNummer");
      fieldDescriptions.put("Bestandsnaam","Bestandsnaam");
      fieldDescriptions.put("Omschrijving","Omschrijving");
      fieldDescriptions.put("Data","Bijlagedata");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("dagboekId","DagboekId");
      javaToBONameMapping.put("boekstuk","Boekstuk");
      javaToBONameMapping.put("volgNr","VolgNr");
      javaToBONameMapping.put("bestandsnaam","Bestandsnaam");
      javaToBONameMapping.put("omschrijving","Omschrijving");
      javaToBONameMapping.put("data","Data");

    }

    private PersistenceMetaDataBijlage(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Bijlage";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Bijlage" , "BIJLAGE");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Bijlage" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Bijlagen bij boekstukken";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DagboekId", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Boekstuk", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VolgNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 3, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bestandsnaam", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	," + 
				getFieldName("Omschrijving", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Data", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[8][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("DagboekId", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "DagboekId"};
      columnDefinitions[3]=new String[]{getFieldName("Boekstuk", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Boekstuknummer"};
      columnDefinitions[4]=new String[]{getFieldName("VolgNr", provider), provider.getDBMapping().getSQLDefinition("int", 3, 0) , "0"  , " NOT NULL", "VolgNummer"};
      columnDefinitions[5]=new String[]{getFieldName("Bestandsnaam", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Bestandsnaam"};
      columnDefinitions[6]=new String[]{getFieldName("Omschrijving", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving"};
      columnDefinitions[7]=new String[]{getFieldName("Data", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Bijlagedata"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_BIJLAGE_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_BIJLAGE_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DagboekId", provider) + " , "  +  getFieldName("Boekstuk", provider) + " , "  +  getFieldName("VolgNr", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_BIJLAGE", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_BIJLAGE PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DagboekId", provider) + " , "  +  getFieldName("Boekstuk", provider) + " , "  +  getFieldName("VolgNr", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
