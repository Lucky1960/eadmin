package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataGebruiker implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataGebruiker();

      persistables.add("nl.eadmin.db.impl.Gebruiker_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("id");
      javaVariableNames.add("naam");
      javaVariableNames.add("wachtwoord");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("boekjaar");
      javaPKVariableNames.add("id");

      fieldNames.put("mutationTimeStamp","GEBRUIKER_RCRDMTTM");
      fieldNames.put("Id","GBRID");
      fieldNames.put("Naam","NAAM");
      fieldNames.put("Wachtwoord","WCHTWRD");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("Boekjaar","BOEKJAAR");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Id","GebruikersId");
      fieldDescriptions.put("Naam","Naam");
      fieldDescriptions.put("Wachtwoord","Wachtwoord");
      fieldDescriptions.put("Bedrijf","Laatst gebruikte administratie");
      fieldDescriptions.put("Boekjaar","Laatst gebruikte boekjaar");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("id","Id");
      javaToBONameMapping.put("naam","Naam");
      javaToBONameMapping.put("wachtwoord","Wachtwoord");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("boekjaar","Boekjaar");

    }

    private PersistenceMetaDataGebruiker(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Gebruiker";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Gebruiker" , "GEBRUIKER");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Gebruiker" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Gebruikers";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Id", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 20, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Naam", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Wachtwoord", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 17, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Boekjaar", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 4, 0) + " DEFAULT 0" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[6][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Id", provider), provider.getDBMapping().getSQLDefinition("String", 20, 0) , "''"  , " NOT NULL", "GebruikersId"};
      columnDefinitions[2]=new String[]{getFieldName("Naam", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Naam"};
      columnDefinitions[3]=new String[]{getFieldName("Wachtwoord", provider), provider.getDBMapping().getSQLDefinition("String", 17, 0) , "''"  , " NOT NULL", "Wachtwoord"};
      columnDefinitions[4]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Laatst gebruikte administratie"};
      columnDefinitions[5]=new String[]{getFieldName("Boekjaar", provider), provider.getDBMapping().getSQLDefinition("int", 4, 0) , "0"  , " NOT NULL", "Laatst gebruikte boekjaar"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_GEBRUIKER_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_GEBRUIKER_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Id", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_GEBRUIKER", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_GEBRUIKER PRIMARY KEY(" +  getFieldName("Id", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
