package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class BtwCode_Impl  implements BtwCode, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataBtwCode.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String code;
    protected String omschrijving;
    protected BigDecimal percentage;
    protected String inEx;
    protected String btwCatInkoop;
    protected String btwCatVerkoop;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected BtwCode_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected BtwCode_Impl (DBData _dbd ,  String _bedrijf, String _code) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setCode(_code);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected BtwCode_Impl (DBData _dbd, BtwCodeDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.BtwCodeDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate BtwCode with " + bean.getClass().getName() + "!! Use nl.eadmin.db.BtwCodeDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setCode(bean.getCode());
        setOmschrijving(bean.getOmschrijving());
        setPercentage(bean.getPercentage());
        setInEx(bean.getInEx());
        setBtwCatInkoop(bean.getBtwCatInkoop());
        setBtwCatVerkoop(bean.getBtwCatVerkoop());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public BtwCode_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      code = rs.getString(pmd.getFieldName("Code",provider));
      omschrijving = rs.getString(pmd.getFieldName("Omschrijving",provider));
      percentage = rs.getBigDecimal(pmd.getFieldName("Percentage",provider));
      inEx = rs.getString(pmd.getFieldName("InEx",provider));
      btwCatInkoop = rs.getString(pmd.getFieldName("BtwCatInkoop",provider));
      btwCatVerkoop = rs.getString(pmd.getFieldName("BtwCatVerkoop",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       BtwCodePK key = new BtwCodePK();
       key.setBedrijf(getBedrijf());
       key.setCode(getCode());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getCode()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-124775191}
//{CODE-INSERT-END:-124775191}
      return trim(code);
      //Custom code
//{CODE-INSERT-BEGIN:426934042}
//{CODE-INSERT-END:426934042}
    }


    public String getOmschrijving()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:86851457}
//{CODE-INSERT-END:86851457}
      return trim(omschrijving);
      //Custom code
//{CODE-INSERT-BEGIN:-1602574462}
//{CODE-INSERT-END:-1602574462}
    }


    public BigDecimal getPercentage()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:447228182}
//{CODE-INSERT-END:447228182}
      return percentage;
      //Custom code
//{CODE-INSERT-BEGIN:979169421}
//{CODE-INSERT-END:979169421}
    }


    public String getInEx()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-15308236}
//{CODE-INSERT-END:-15308236}
      return trim(inEx);
      //Custom code
//{CODE-INSERT-BEGIN:-474557649}
//{CODE-INSERT-END:-474557649}
    }


    public String getBtwCatInkoop()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1100534647}
//{CODE-INSERT-END:1100534647}
      return trim(btwCatInkoop);
      //Custom code
//{CODE-INSERT-BEGIN:-243166644}
//{CODE-INSERT-END:-243166644}
    }


    public String getBtwCatVerkoop()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1477902787}
//{CODE-INSERT-END:1477902787}
      return trim(btwCatVerkoop);
      //Custom code
//{CODE-INSERT-BEGIN:-1429656192}
//{CODE-INSERT-END:-1429656192}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setCode(String _code ) throws Exception {

      if (_code !=  null)
         _code = _code.trim();
      if (_code !=  null)
         _code = _code.toUpperCase();
      if (_code == null)
          _code =  "";
      if (_code.equals(getCode()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1424217437}
//{CODE-INSERT-END:1424217437}
      code =  _code;

      autoUpdate();

    }

    public void setOmschrijving(String _omschrijving ) throws Exception {

      if (_omschrijving !=  null)
         _omschrijving = _omschrijving.trim();
      if (_omschrijving == null)
          _omschrijving =  "";
      if (_omschrijving.equals(getOmschrijving()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-524071947}
//{CODE-INSERT-END:-524071947}
      omschrijving =  _omschrijving;

      autoUpdate();

    }

    public void setPercentage(BigDecimal _percentage ) throws Exception {

      if (_percentage == null)
          _percentage =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_percentage.equals(getPercentage()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1367261834}
//{CODE-INSERT-END:1367261834}
      percentage =  _percentage;

      autoUpdate();

    }

    public void setInEx(String _inEx ) throws Exception {

      if (_inEx !=  null)
         _inEx = _inEx.trim();
      if ((_inEx == null ? getInEx() == null : _inEx.equals(getInEx())))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1533684392}
//{CODE-INSERT-END:1533684392}
      inEx =  _inEx;

      autoUpdate();

    }

    public void setBtwCatInkoop(String _btwCatInkoop ) throws Exception {

      if (_btwCatInkoop !=  null)
         _btwCatInkoop = _btwCatInkoop.trim();
      if (_btwCatInkoop !=  null)
         _btwCatInkoop = _btwCatInkoop.toUpperCase();
      if (_btwCatInkoop == null)
          _btwCatInkoop =  "";
      if (_btwCatInkoop.equals(getBtwCatInkoop()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:489611243}
//{CODE-INSERT-END:489611243}
      btwCatInkoop =  _btwCatInkoop;

      autoUpdate();

    }

    public void setBtwCatVerkoop(String _btwCatVerkoop ) throws Exception {

      if (_btwCatVerkoop !=  null)
         _btwCatVerkoop = _btwCatVerkoop.trim();
      if (_btwCatVerkoop !=  null)
         _btwCatVerkoop = _btwCatVerkoop.toUpperCase();
      if (_btwCatVerkoop == null)
          _btwCatVerkoop =  "";
      if (_btwCatVerkoop.equals(getBtwCatVerkoop()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-280853553}
//{CODE-INSERT-END:-280853553}
      btwCatVerkoop =  _btwCatVerkoop;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      code =  "";
      omschrijving =  "";
      percentage =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      btwCatInkoop =  "";
      btwCatVerkoop =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.code = code;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("BtwCode is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         BtwCodePK key = new BtwCodePK();
         key.setBedrijf(getBedrijf());
         key.setCode(getCode());
         ((nl.eadmin.db.impl.BtwCodeManager_Impl)nl.eadmin.db.impl.BtwCodeManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((BtwCodeManager_Impl)BtwCodeManager_Impl.getInstance(dbd)).getResultSet((BtwCodePK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("1519260310_"+ dbd.getDBId() + bedrijf + code).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getBtwCodeDataBean()!
    */
    public BtwCodeDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public BtwCodeDataBean getBtwCodeDataBean() throws Exception {
      return getDataBean(null);
    }

    private BtwCodeDataBean getDataBean(BtwCodeDataBean bean) throws Exception {

      if (bean == null)
          bean = new BtwCodeDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setCode(getCode());
      bean.setOmschrijving(getOmschrijving());
      bean.setPercentage(getPercentage());
      bean.setInEx(getInEx());
      bean.setBtwCatInkoop(getBtwCatInkoop());
      bean.setBtwCatVerkoop(getBtwCatVerkoop());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.BtwCode_Impl))
             return false;
          nl.eadmin.db.impl.BtwCode_Impl bo = (nl.eadmin.db.impl.BtwCode_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getCode() == null){
              if (this.getCode() != null)
                  return false;
          }else if (!bo.getCode().equals(this.getCode())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(BtwCodeDataBean bean) throws Exception {
       set(bean);
    }


    public void set(BtwCodeDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setCode(bean.getCode());
           setOmschrijving(bean.getOmschrijving());
           setPercentage(bean.getPercentage());
           setInEx(bean.getInEx());
           setBtwCatInkoop(bean.getBtwCatInkoop());
           setBtwCatVerkoop(bean.getBtwCatVerkoop());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"BtwCode is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.BtwCode.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("Code",provider) +  " = ? " + ", " + pmd.getFieldName("Omschrijving",provider) +  " = ? " + ", " + pmd.getFieldName("Percentage",provider) +  " = ? " + ", " + pmd.getFieldName("InEx",provider) +  " = ? " + ", " + pmd.getFieldName("BtwCatInkoop",provider) +  " = ? " + ", " + pmd.getFieldName("BtwCatVerkoop",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("Code",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  code,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  percentage,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        if ( inEx == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("String"));
       } else {
          prep.setObject(parmIndex++,  inEx,  mapping.getJDBCTypeFor("String"));
}
        prep.setObject(parmIndex++,  btwCatInkoop,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  btwCatVerkoop,  mapping.getJDBCTypeFor("String"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.code,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("BtwCode modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.BtwCode.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.BtwCode.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("Code",provider) + ", " + pmd.getFieldName("Omschrijving",provider) + ", " + pmd.getFieldName("Percentage",provider) + ", " + pmd.getFieldName("InEx",provider) + ", " + pmd.getFieldName("BtwCatInkoop",provider) + ", " + pmd.getFieldName("BtwCatVerkoop",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, code,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5, percentage,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        if (inEx == null)
          prep.setNull(6,  mapping.getJDBCTypeFor("String"));
        else
          prep.setObject(6, inEx,  mapping.getJDBCTypeFor("String"));
        prep.setObject(7, btwCatInkoop,  mapping.getJDBCTypeFor("String"));
        prep.setObject(8, btwCatVerkoop,  mapping.getJDBCTypeFor("String"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.code = currentDBImage.code;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.code = transactionImage.dbImage.code;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        code = transactionImage.dbImage.code;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String code;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
