package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Journaal_Impl  implements Journaal, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataJournaal.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String dagboekId;
    protected String boekstuk;
    protected int seqNr;
    protected String dagboekSoort;
    protected String rekeningNr;
    protected String rekeningsoort;
    protected String rekeningOmschr;
    protected String dcNummer;
    protected String factuurNr;
    protected java.sql.Date boekdatum;
    protected int boekjaar;
    protected int boekperiode;
    protected String btwCode;
    protected String iCPSoort;
    protected String btwCategorie;
    protected String btwAangifteKolom;
    protected String dC;
    protected BigDecimal bedragDebet;
    protected BigDecimal bedragCredit;
    protected String omschr1;
    protected String omschr2;
    protected String omschr3;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Journaal_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Journaal_Impl (DBData _dbd ,  String _bedrijf, String _dagboekId, String _boekstuk, int _seqNr) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setDagboekId(_dagboekId);
        setBoekstuk(_boekstuk);
        setSeqNr(_seqNr);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Journaal_Impl (DBData _dbd, JournaalDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.JournaalDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Journaal with " + bean.getClass().getName() + "!! Use nl.eadmin.db.JournaalDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setDagboekId(bean.getDagboekId());
        setBoekstuk(bean.getBoekstuk());
        setSeqNr(bean.getSeqNr());
        setDagboekSoort(bean.getDagboekSoort());
        setRekeningNr(bean.getRekeningNr());
        setRekeningsoort(bean.getRekeningsoort());
        setRekeningOmschr(bean.getRekeningOmschr());
        setDcNummer(bean.getDcNummer());
        setFactuurNr(bean.getFactuurNr());
        setBoekdatum(bean.getBoekdatum());
        setBoekjaar(bean.getBoekjaar());
        setBoekperiode(bean.getBoekperiode());
        setBtwCode(bean.getBtwCode());
        setICPSoort(bean.getICPSoort());
        setBtwCategorie(bean.getBtwCategorie());
        setBtwAangifteKolom(bean.getBtwAangifteKolom());
        setDC(bean.getDC());
        setBedragDebet(bean.getBedragDebet());
        setBedragCredit(bean.getBedragCredit());
        setOmschr1(bean.getOmschr1());
        setOmschr2(bean.getOmschr2());
        setOmschr3(bean.getOmschr3());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Journaal_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      dagboekId = rs.getString(pmd.getFieldName("DagboekId",provider));
      boekstuk = rs.getString(pmd.getFieldName("Boekstuk",provider));
      seqNr = rs.getInt(pmd.getFieldName("SeqNr",provider));
      dagboekSoort = rs.getString(pmd.getFieldName("DagboekSoort",provider));
      rekeningNr = rs.getString(pmd.getFieldName("RekeningNr",provider));
      rekeningsoort = rs.getString(pmd.getFieldName("Rekeningsoort",provider));
      rekeningOmschr = rs.getString(pmd.getFieldName("RekeningOmschr",provider));
      dcNummer = rs.getString(pmd.getFieldName("DcNummer",provider));
      factuurNr = rs.getString(pmd.getFieldName("FactuurNr",provider));
      boekdatum = rs.getDate(pmd.getFieldName("Boekdatum",provider));
      boekjaar = rs.getInt(pmd.getFieldName("Boekjaar",provider));
      boekperiode = rs.getInt(pmd.getFieldName("Boekperiode",provider));
      btwCode = rs.getString(pmd.getFieldName("BtwCode",provider));
      iCPSoort = rs.getString(pmd.getFieldName("ICPSoort",provider));
      btwCategorie = rs.getString(pmd.getFieldName("BtwCategorie",provider));
      btwAangifteKolom = rs.getString(pmd.getFieldName("BtwAangifteKolom",provider));
      dC = rs.getString(pmd.getFieldName("DC",provider));
      bedragDebet = rs.getBigDecimal(pmd.getFieldName("BedragDebet",provider));
      bedragCredit = rs.getBigDecimal(pmd.getFieldName("BedragCredit",provider));
      omschr1 = rs.getString(pmd.getFieldName("Omschr1",provider));
      omschr2 = rs.getString(pmd.getFieldName("Omschr2",provider));
      omschr3 = rs.getString(pmd.getFieldName("Omschr3",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       JournaalPK key = new JournaalPK();
       key.setBedrijf(getBedrijf());
       key.setDagboekId(getDagboekId());
       key.setBoekstuk(getBoekstuk());
       key.setSeqNr(getSeqNr());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getDagboekId()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:633870212}
//{CODE-INSERT-END:633870212}
      return trim(dagboekId);
      //Custom code
//{CODE-INSERT-BEGIN:-1824862241}
//{CODE-INSERT-END:-1824862241}
    }


    public String getBoekstuk()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1271118490}
//{CODE-INSERT-END:-1271118490}
      return boekstuk;
      //Custom code
//{CODE-INSERT-BEGIN:-749969859}
//{CODE-INSERT-END:-749969859}
    }


    public int getSeqNr()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-664583601}
//{CODE-INSERT-END:-664583601}
      return seqNr;
      //Custom code
//{CODE-INSERT-BEGIN:872742516}
//{CODE-INSERT-END:872742516}
    }


    public String getDagboekSoort()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1126849108}
//{CODE-INSERT-END:1126849108}
      return trim(dagboekSoort);
      //Custom code
//{CODE-INSERT-BEGIN:572581647}
//{CODE-INSERT-END:572581647}
    }


    public String getRekeningNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:666684321}
//{CODE-INSERT-END:666684321}
      return rekeningNr;
      //Custom code
//{CODE-INSERT-BEGIN:-807624862}
//{CODE-INSERT-END:-807624862}
    }


    public String getRekeningsoort()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-746744320}
//{CODE-INSERT-END:-746744320}
      return rekeningsoort;
      //Custom code
//{CODE-INSERT-BEGIN:-1674239773}
//{CODE-INSERT-END:-1674239773}
    }


    public String getRekeningOmschr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1947634283}
//{CODE-INSERT-END:-1947634283}
      return rekeningOmschr;
      //Custom code
//{CODE-INSERT-BEGIN:-247122962}
//{CODE-INSERT-END:-247122962}
    }


    public String getDcNummer()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1535325713}
//{CODE-INSERT-END:-1535325713}
      return trim(dcNummer);
      //Custom code
//{CODE-INSERT-BEGIN:-350459180}
//{CODE-INSERT-END:-350459180}
    }


    public String getFactuurNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1336662166}
//{CODE-INSERT-END:1336662166}
      return trim(factuurNr);
      //Custom code
//{CODE-INSERT-BEGIN:-1513148147}
//{CODE-INSERT-END:-1513148147}
    }


    public Date getBoekdatum()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:82796942}
//{CODE-INSERT-END:82796942}

        Date obj = boekdatum == null?null:new Date(boekdatum.getTime());

      //Custom code
//{CODE-INSERT-BEGIN:1249596904}
//{CODE-INSERT-END:1249596904}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:82796027}
//{CODE-INSERT-END:82796027}
    }


    public int getBoekjaar()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-25361449}
//{CODE-INSERT-END:-25361449}
      return boekjaar;
      //Custom code
//{CODE-INSERT-BEGIN:-786207252}
//{CODE-INSERT-END:-786207252}
    }


    public int getBoekperiode()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-814355331}
//{CODE-INSERT-END:-814355331}
      return boekperiode;
      //Custom code
//{CODE-INSERT-BEGIN:524786182}
//{CODE-INSERT-END:524786182}
    }


    public String getBtwCode()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:635008062}
//{CODE-INSERT-END:635008062}
      return trim(btwCode);
      //Custom code
//{CODE-INSERT-BEGIN:-1789588891}
//{CODE-INSERT-END:-1789588891}
    }


    public String getICPSoort()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1246441445}
//{CODE-INSERT-END:-1246441445}
      return trim(iCPSoort);
      //Custom code
//{CODE-INSERT-BEGIN:15018536}
//{CODE-INSERT-END:15018536}
    }


    public String getBtwCategorie()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:522287310}
//{CODE-INSERT-END:522287310}
      return trim(btwCategorie);
      //Custom code
//{CODE-INSERT-BEGIN:-988964907}
//{CODE-INSERT-END:-988964907}
    }


    public String getBtwAangifteKolom()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-609204202}
//{CODE-INSERT-END:-609204202}
      return trim(btwAangifteKolom);
      //Custom code
//{CODE-INSERT-BEGIN:-1705463411}
//{CODE-INSERT-END:-1705463411}
    }


    public String getDC()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:175435867}
//{CODE-INSERT-END:175435867}
      return dC;
      //Custom code
//{CODE-INSERT-BEGIN:1143542248}
//{CODE-INSERT-END:1143542248}
    }


    public BigDecimal getBedragDebet()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2108193403}
//{CODE-INSERT-END:-2108193403}
      return bedragDebet;
      //Custom code
//{CODE-INSERT-BEGIN:-929488386}
//{CODE-INSERT-END:-929488386}
    }


    public BigDecimal getBedragCredit()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1289848652}
//{CODE-INSERT-END:1289848652}
      return bedragCredit;
      //Custom code
//{CODE-INSERT-BEGIN:1330600215}
//{CODE-INSERT-END:1330600215}
    }


    public String getOmschr1()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1430504389}
//{CODE-INSERT-END:1430504389}
      return omschr1;
      //Custom code
//{CODE-INSERT-BEGIN:1395960766}
//{CODE-INSERT-END:1395960766}
    }


    public String getOmschr2()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1431427910}
//{CODE-INSERT-END:1431427910}
      return omschr2;
      //Custom code
//{CODE-INSERT-BEGIN:1424589917}
//{CODE-INSERT-END:1424589917}
    }


    public String getOmschr3()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1432351431}
//{CODE-INSERT-END:1432351431}
      return omschr3;
      //Custom code
//{CODE-INSERT-BEGIN:1453219068}
//{CODE-INSERT-END:1453219068}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setDagboekId(String _dagboekId ) throws Exception {

      if (_dagboekId !=  null)
         _dagboekId = _dagboekId.trim();
      if (_dagboekId !=  null)
         _dagboekId = _dagboekId.toUpperCase();
      if (_dagboekId == null)
          _dagboekId =  "";
      if (_dagboekId.equals(getDagboekId()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-306282608}
//{CODE-INSERT-END:-306282608}
      dagboekId =  _dagboekId;

      autoUpdate();

    }

    public void setBoekstuk(String _boekstuk ) throws Exception {

      if (_boekstuk == null)
          _boekstuk =  "";
      if (_boekstuk.equals(getBoekstuk()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1192405978}
//{CODE-INSERT-END:1192405978}
      boekstuk =  _boekstuk;

      autoUpdate();

    }

    public void setSeqNr(int _seqNr ) throws Exception {

      if (_seqNr == getSeqNr())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:109547611}
//{CODE-INSERT-END:109547611}
      seqNr =  _seqNr;

      autoUpdate();

    }

    public void setDagboekSoort(String _dagboekSoort ) throws Exception {

      if (_dagboekSoort !=  null)
         _dagboekSoort = _dagboekSoort.trim();
      if (_dagboekSoort !=  null)
         _dagboekSoort = _dagboekSoort.toUpperCase();
      if (_dagboekSoort == null)
          _dagboekSoort =  "";
      if (_dagboekSoort.equals(getDagboekSoort()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:515925704}
//{CODE-INSERT-END:515925704}
      dagboekSoort =  _dagboekSoort;

      autoUpdate();

    }

    public void setRekeningNr(String _rekeningNr ) throws Exception {

      if (_rekeningNr == null)
          _rekeningNr =  "";
      if (_rekeningNr.equals(getRekeningNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1586717973}
//{CODE-INSERT-END:1586717973}
      rekeningNr =  _rekeningNr;

      autoUpdate();

    }

    public void setRekeningsoort(String _rekeningsoort ) throws Exception {

      if (_rekeningsoort == null)
          _rekeningsoort =  "";
      if (_rekeningsoort.equals(getRekeningsoort()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1789466636}
//{CODE-INSERT-END:1789466636}
      rekeningsoort =  _rekeningsoort;

      autoUpdate();

    }

    public void setRekeningOmschr(String _rekeningOmschr ) throws Exception {

      if (_rekeningOmschr == null)
          _rekeningOmschr =  "";
      if (_rekeningOmschr.equals(getRekeningOmschr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-634505975}
//{CODE-INSERT-END:-634505975}
      rekeningOmschr =  _rekeningOmschr;

      autoUpdate();

    }

    public void setDcNummer(String _dcNummer ) throws Exception {

      if (_dcNummer !=  null)
         _dcNummer = _dcNummer.trim();
      if (_dcNummer == null)
          _dcNummer =  "";
      if (_dcNummer.equals(getDcNummer()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:928198755}
//{CODE-INSERT-END:928198755}
      dcNummer =  _dcNummer;

      autoUpdate();

    }

    public void setFactuurNr(String _factuurNr ) throws Exception {

      if (_factuurNr !=  null)
         _factuurNr = _factuurNr.trim();
      if (_factuurNr == null)
          _factuurNr =  "";
      if (_factuurNr.equals(getFactuurNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:396509346}
//{CODE-INSERT-END:396509346}
      factuurNr =  _factuurNr;

      autoUpdate();

    }

    public void setBoekdatum(Date _boekdatum ) throws Exception {

      if ((_boekdatum == null ? getBoekdatum() == null : _boekdatum.equals(getBoekdatum())))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:309444084}
//{CODE-INSERT-END:309444084}
      boekdatum =  _boekdatum == null?null: new java.sql.Date( _boekdatum.getTime());

      autoUpdate();

    }

    public void setBoekjaar(int _boekjaar ) throws Exception {

      if (_boekjaar == getBoekjaar())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1856804277}
//{CODE-INSERT-END:-1856804277}
      boekjaar =  _boekjaar;

      autoUpdate();

    }

    public void setBoekperiode(int _boekperiode ) throws Exception {

      if (_boekperiode == getBoekperiode())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1936884105}
//{CODE-INSERT-END:1936884105}
      boekperiode =  _boekperiode;

      autoUpdate();

    }

    public void setBtwCode(String _btwCode ) throws Exception {

      if (_btwCode !=  null)
         _btwCode = _btwCode.trim();
      if (_btwCode == null)
          _btwCode =  "";
      if (_btwCode.equals(getBtwCode()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1545760586}
//{CODE-INSERT-END:1545760586}
      btwCode =  _btwCode;

      autoUpdate();

    }

    public void setICPSoort(String _iCPSoort ) throws Exception {

      if (_iCPSoort !=  null)
         _iCPSoort = _iCPSoort.trim();
      if (_iCPSoort == null)
          _iCPSoort =  "";
      if (_iCPSoort.equals(getICPSoort()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1217083023}
//{CODE-INSERT-END:1217083023}
      iCPSoort =  _iCPSoort;

      autoUpdate();

    }

    public void setBtwCategorie(String _btwCategorie ) throws Exception {

      if (_btwCategorie !=  null)
         _btwCategorie = _btwCategorie.trim();
      if (_btwCategorie !=  null)
         _btwCategorie = _btwCategorie.toUpperCase();
      if (_btwCategorie == null)
          _btwCategorie =  "";
      if (_btwCategorie.equals(getBtwCategorie()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-88636094}
//{CODE-INSERT-END:-88636094}
      btwCategorie =  _btwCategorie;

      autoUpdate();

    }

    public void setBtwAangifteKolom(String _btwAangifteKolom ) throws Exception {

      if (_btwAangifteKolom !=  null)
         _btwAangifteKolom = _btwAangifteKolom.trim();
      if (_btwAangifteKolom !=  null)
         _btwAangifteKolom = _btwAangifteKolom.toUpperCase();
      if (_btwAangifteKolom == null)
          _btwAangifteKolom =  "";
      if (_btwAangifteKolom.equals(getBtwAangifteKolom()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1413285238}
//{CODE-INSERT-END:-1413285238}
      btwAangifteKolom =  _btwAangifteKolom;

      autoUpdate();

    }

    public void setDC(String _dC ) throws Exception {

      if (_dC !=  null)
         _dC = _dC.toUpperCase();
      if (_dC == null)
          _dC =  "";
      if (_dC.equals(getDC()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:2112241103}
//{CODE-INSERT-END:2112241103}
      dC =  _dC;

      autoUpdate();

    }

    public void setBedragDebet(BigDecimal _bedragDebet ) throws Exception {

      if (_bedragDebet == null)
          _bedragDebet =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_bedragDebet.equals(getBedragDebet()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:643046033}
//{CODE-INSERT-END:643046033}
      bedragDebet =  _bedragDebet;

      autoUpdate();

    }

    public void setBedragCredit(BigDecimal _bedragCredit ) throws Exception {

      if (_bedragCredit == null)
          _bedragCredit =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_bedragCredit.equals(getBedragCredit()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:678925248}
//{CODE-INSERT-END:678925248}
      bedragCredit =  _bedragCredit;

      autoUpdate();

    }

    public void setOmschr1(String _omschr1 ) throws Exception {

      if (_omschr1 == null)
          _omschr1 =  "";
      if (_omschr1.equals(getOmschr1()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1953710383}
//{CODE-INSERT-END:-1953710383}
      omschr1 =  _omschr1;

      autoUpdate();

    }

    public void setOmschr2(String _omschr2 ) throws Exception {

      if (_omschr2 == null)
          _omschr2 =  "";
      if (_omschr2.equals(getOmschr2()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1952786862}
//{CODE-INSERT-END:-1952786862}
      omschr2 =  _omschr2;

      autoUpdate();

    }

    public void setOmschr3(String _omschr3 ) throws Exception {

      if (_omschr3 == null)
          _omschr3 =  "";
      if (_omschr3.equals(getOmschr3()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1951863341}
//{CODE-INSERT-END:-1951863341}
      omschr3 =  _omschr3;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      dagboekId =  "";
      boekstuk =  "";
      seqNr = 0;
      dagboekSoort =  "";
      rekeningNr =  "";
      rekeningsoort =  "";
      rekeningOmschr =  "";
      dcNummer =  "";
      factuurNr =  "";
      boekjaar = 0;
      boekperiode = 0;
      btwCode =  "";
      iCPSoort =  "";
      btwCategorie =  "";
      btwAangifteKolom =  "";
      dC =  "";
      bedragDebet =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      bedragCredit =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      omschr1 =  "";
      omschr2 =  "";
      omschr3 =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.dagboekId = dagboekId;
        currentDBImage.boekstuk = boekstuk;
        currentDBImage.seqNr = seqNr;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Journaal is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         JournaalPK key = new JournaalPK();
         key.setBedrijf(getBedrijf());
         key.setDagboekId(getDagboekId());
         key.setBoekstuk(getBoekstuk());
         key.setSeqNr(getSeqNr());
         ((nl.eadmin.db.impl.JournaalManager_Impl)nl.eadmin.db.impl.JournaalManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((JournaalManager_Impl)JournaalManager_Impl.getInstance(dbd)).getResultSet((JournaalPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("753573532_"+ dbd.getDBId() + bedrijf + dagboekId + boekstuk + seqNr).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getJournaalDataBean()!
    */
    public JournaalDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public JournaalDataBean getJournaalDataBean() throws Exception {
      return getDataBean(null);
    }

    private JournaalDataBean getDataBean(JournaalDataBean bean) throws Exception {

      if (bean == null)
          bean = new JournaalDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setDagboekId(getDagboekId());
      bean.setBoekstuk(getBoekstuk());
      bean.setSeqNr(getSeqNr());
      bean.setDagboekSoort(getDagboekSoort());
      bean.setRekeningNr(getRekeningNr());
      bean.setRekeningsoort(getRekeningsoort());
      bean.setRekeningOmschr(getRekeningOmschr());
      bean.setDcNummer(getDcNummer());
      bean.setFactuurNr(getFactuurNr());
      bean.setBoekdatum(getBoekdatum());
      bean.setBoekjaar(getBoekjaar());
      bean.setBoekperiode(getBoekperiode());
      bean.setBtwCode(getBtwCode());
      bean.setICPSoort(getICPSoort());
      bean.setBtwCategorie(getBtwCategorie());
      bean.setBtwAangifteKolom(getBtwAangifteKolom());
      bean.setDC(getDC());
      bean.setBedragDebet(getBedragDebet());
      bean.setBedragCredit(getBedragCredit());
      bean.setOmschr1(getOmschr1());
      bean.setOmschr2(getOmschr2());
      bean.setOmschr3(getOmschr3());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Journaal_Impl))
             return false;
          nl.eadmin.db.impl.Journaal_Impl bo = (nl.eadmin.db.impl.Journaal_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getDagboekId() == null){
              if (this.getDagboekId() != null)
                  return false;
          }else if (!bo.getDagboekId().equals(this.getDagboekId())){
              return false;
          }
          if (bo.getBoekstuk() == null){
              if (this.getBoekstuk() != null)
                  return false;
          }else if (!bo.getBoekstuk().equals(this.getBoekstuk())){
              return false;
          }
          if (bo.getSeqNr() != this.getSeqNr())
              return false;
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(JournaalDataBean bean) throws Exception {
       set(bean);
    }


    public void set(JournaalDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setDagboekId(bean.getDagboekId());
           setBoekstuk(bean.getBoekstuk());
           setSeqNr(bean.getSeqNr());
           setDagboekSoort(bean.getDagboekSoort());
           setRekeningNr(bean.getRekeningNr());
           setRekeningsoort(bean.getRekeningsoort());
           setRekeningOmschr(bean.getRekeningOmschr());
           setDcNummer(bean.getDcNummer());
           setFactuurNr(bean.getFactuurNr());
           setBoekdatum(bean.getBoekdatum());
           setBoekjaar(bean.getBoekjaar());
           setBoekperiode(bean.getBoekperiode());
           setBtwCode(bean.getBtwCode());
           setICPSoort(bean.getICPSoort());
           setBtwCategorie(bean.getBtwCategorie());
           setBtwAangifteKolom(bean.getBtwAangifteKolom());
           setDC(bean.getDC());
           setBedragDebet(bean.getBedragDebet());
           setBedragCredit(bean.getBedragCredit());
           setOmschr1(bean.getOmschr1());
           setOmschr2(bean.getOmschr2());
           setOmschr3(bean.getOmschr3());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Journaal is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Journaal.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("DagboekId",provider) +  " = ? " + ", " + pmd.getFieldName("Boekstuk",provider) +  " = ? " + ", " + pmd.getFieldName("SeqNr",provider) +  " = ? " + ", " + pmd.getFieldName("DagboekSoort",provider) +  " = ? " + ", " + pmd.getFieldName("RekeningNr",provider) +  " = ? " + ", " + pmd.getFieldName("Rekeningsoort",provider) +  " = ? " + ", " + pmd.getFieldName("RekeningOmschr",provider) +  " = ? " + ", " + pmd.getFieldName("DcNummer",provider) +  " = ? " + ", " + pmd.getFieldName("FactuurNr",provider) +  " = ? " + ", " + pmd.getFieldName("Boekdatum",provider) +  " = ? " + ", " + pmd.getFieldName("Boekjaar",provider) +  " = ? " + ", " + pmd.getFieldName("Boekperiode",provider) +  " = ? " + ", " + pmd.getFieldName("BtwCode",provider) +  " = ? " + ", " + pmd.getFieldName("ICPSoort",provider) +  " = ? " + ", " + pmd.getFieldName("BtwCategorie",provider) +  " = ? " + ", " + pmd.getFieldName("BtwAangifteKolom",provider) +  " = ? " + ", " + pmd.getFieldName("DC",provider) +  " = ? " + ", " + pmd.getFieldName("BedragDebet",provider) +  " = ? " + ", " + pmd.getFieldName("BedragCredit",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr1",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr2",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr3",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("DagboekId",provider) + " = ? " + " AND  " +  pmd.getFieldName("Boekstuk",provider) + " = ? " + " AND  " +  pmd.getFieldName("SeqNr",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dagboekId,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  boekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( seqNr),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  dagboekSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  rekeningNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  rekeningsoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  rekeningOmschr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dcNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  factuurNr,  mapping.getJDBCTypeFor("String"));
        if ( boekdatum == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("Date"));
       } else {
          prep.setObject(parmIndex++,  boekdatum,  mapping.getJDBCTypeFor("Date"));
}
        prep.setObject(parmIndex++,  new Integer( boekjaar),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  new Integer( boekperiode),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  btwCode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  iCPSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  btwCategorie,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  btwAangifteKolom,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dC,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  bedragDebet,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  bedragCredit,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  omschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschr3,  mapping.getJDBCTypeFor("String"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.dagboekId,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.boekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer(image.seqNr),  mapping.getJDBCTypeFor("int"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Journaal modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Journaal.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Journaal.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("DagboekId",provider) + ", " + pmd.getFieldName("Boekstuk",provider) + ", " + pmd.getFieldName("SeqNr",provider) + ", " + pmd.getFieldName("DagboekSoort",provider) + ", " + pmd.getFieldName("RekeningNr",provider) + ", " + pmd.getFieldName("Rekeningsoort",provider) + ", " + pmd.getFieldName("RekeningOmschr",provider) + ", " + pmd.getFieldName("DcNummer",provider) + ", " + pmd.getFieldName("FactuurNr",provider) + ", " + pmd.getFieldName("Boekdatum",provider) + ", " + pmd.getFieldName("Boekjaar",provider) + ", " + pmd.getFieldName("Boekperiode",provider) + ", " + pmd.getFieldName("BtwCode",provider) + ", " + pmd.getFieldName("ICPSoort",provider) + ", " + pmd.getFieldName("BtwCategorie",provider) + ", " + pmd.getFieldName("BtwAangifteKolom",provider) + ", " + pmd.getFieldName("DC",provider) + ", " + pmd.getFieldName("BedragDebet",provider) + ", " + pmd.getFieldName("BedragCredit",provider) + ", " + pmd.getFieldName("Omschr1",provider) + ", " + pmd.getFieldName("Omschr2",provider) + ", " + pmd.getFieldName("Omschr3",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, dagboekId,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, boekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5,  new Integer(seqNr),  mapping.getJDBCTypeFor("int"));
        prep.setObject(6, dagboekSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(7, rekeningNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(8, rekeningsoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(9, rekeningOmschr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(10, dcNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(11, factuurNr,  mapping.getJDBCTypeFor("String"));
        if (boekdatum == null)
          prep.setNull(12,  mapping.getJDBCTypeFor("Date"));
        else
          prep.setObject(12, boekdatum,  mapping.getJDBCTypeFor("Date"));
        prep.setObject(13,  new Integer(boekjaar),  mapping.getJDBCTypeFor("int"));
        prep.setObject(14,  new Integer(boekperiode),  mapping.getJDBCTypeFor("int"));
        prep.setObject(15, btwCode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(16, iCPSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(17, btwCategorie,  mapping.getJDBCTypeFor("String"));
        prep.setObject(18, btwAangifteKolom,  mapping.getJDBCTypeFor("String"));
        prep.setObject(19, dC,  mapping.getJDBCTypeFor("String"));
        prep.setObject(20, bedragDebet,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(21, bedragCredit,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(22, omschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(23, omschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(24, omschr3,  mapping.getJDBCTypeFor("String"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.dagboekId = currentDBImage.dagboekId;
        transactionImage.dbImage.boekstuk = currentDBImage.boekstuk;
        transactionImage.dbImage.seqNr = currentDBImage.seqNr;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.dagboekId = transactionImage.dbImage.dagboekId;
      currentDBImage.boekstuk = transactionImage.dbImage.boekstuk;
      currentDBImage.seqNr = transactionImage.dbImage.seqNr;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        dagboekId = transactionImage.dbImage.dagboekId;
        boekstuk = transactionImage.dbImage.boekstuk;
        seqNr = transactionImage.dbImage.seqNr;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String dagboekId;
      private String boekstuk;
      private int seqNr;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
    public Rekening getRekeningObject() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Rekening WHERE ");
		sb.append(Rekening.BEDRIJF + "='" + bedrijf + "' AND ");
		sb.append(Rekening.REKENING_NR + "='" + rekeningNr + "' ");
		FreeQuery qry = QueryFactory.createFreeQuery((RekeningManager_Impl) RekeningManagerFactory.getInstance(dbd), sb.toString());
		return (Rekening) qry.getFirstObject();
    }
//{CODE-INSERT-END:955534258}
}
