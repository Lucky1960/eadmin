package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class BankAfschrift_Impl  implements BankAfschrift, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataBankAfschrift.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected int runNr;
    protected int seqNr;
    protected String afschriftNr;
    protected String bladNr;
    protected java.sql.Date boekdatum;
    protected String rekening;
    protected String tegenrekening;
    protected String naam;
    protected String dC;
    protected BigDecimal bedrag;
    protected String muntsoort;
    protected String hdrOmschr1;
    protected String hdrOmschr2;
    protected String dtlOmschr1;
    protected String dtlOmschr2;
    protected String dtlOmschr3;
    protected byte[] remark;
    protected byte[] inputRecord;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected BankAfschrift_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected BankAfschrift_Impl (DBData _dbd ,  String _bedrijf, int _runNr, int _seqNr) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setRunNr(_runNr);
        setSeqNr(_seqNr);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected BankAfschrift_Impl (DBData _dbd, BankAfschriftDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.BankAfschriftDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate BankAfschrift with " + bean.getClass().getName() + "!! Use nl.eadmin.db.BankAfschriftDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setRunNr(bean.getRunNr());
        setSeqNr(bean.getSeqNr());
        setAfschriftNr(bean.getAfschriftNr());
        setBladNr(bean.getBladNr());
        setBoekdatum(bean.getBoekdatum());
        setRekening(bean.getRekening());
        setTegenrekening(bean.getTegenrekening());
        setNaam(bean.getNaam());
        setDC(bean.getDC());
        setBedrag(bean.getBedrag());
        setMuntsoort(bean.getMuntsoort());
        setHdrOmschr1(bean.getHdrOmschr1());
        setHdrOmschr2(bean.getHdrOmschr2());
        setDtlOmschr1(bean.getDtlOmschr1());
        setDtlOmschr2(bean.getDtlOmschr2());
        setDtlOmschr3(bean.getDtlOmschr3());
        setRemark(bean.getRemark());
        setInputRecord(bean.getInputRecord());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public BankAfschrift_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      runNr = rs.getInt(pmd.getFieldName("RunNr",provider));
      seqNr = rs.getInt(pmd.getFieldName("SeqNr",provider));
      afschriftNr = rs.getString(pmd.getFieldName("AfschriftNr",provider));
      bladNr = rs.getString(pmd.getFieldName("BladNr",provider));
      boekdatum = rs.getDate(pmd.getFieldName("Boekdatum",provider));
      rekening = rs.getString(pmd.getFieldName("Rekening",provider));
      tegenrekening = rs.getString(pmd.getFieldName("Tegenrekening",provider));
      naam = rs.getString(pmd.getFieldName("Naam",provider));
      dC = rs.getString(pmd.getFieldName("DC",provider));
      bedrag = rs.getBigDecimal(pmd.getFieldName("Bedrag",provider));
      muntsoort = rs.getString(pmd.getFieldName("Muntsoort",provider));
      hdrOmschr1 = rs.getString(pmd.getFieldName("HdrOmschr1",provider));
      hdrOmschr2 = rs.getString(pmd.getFieldName("HdrOmschr2",provider));
      dtlOmschr1 = rs.getString(pmd.getFieldName("DtlOmschr1",provider));
      dtlOmschr2 = rs.getString(pmd.getFieldName("DtlOmschr2",provider));
      dtlOmschr3 = rs.getString(pmd.getFieldName("DtlOmschr3",provider));
      remark = rs.getBytes(pmd.getFieldName("Remark",provider));
      inputRecord = rs.getBytes(pmd.getFieldName("InputRecord",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       BankAfschriftPK key = new BankAfschriftPK();
       key.setBedrijf(getBedrijf());
       key.setRunNr(getRunNr());
       key.setSeqNr(getSeqNr());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public int getRunNr()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:595521403}
//{CODE-INSERT-END:595521403}
      return runNr;
      //Custom code
//{CODE-INSERT-BEGIN:1281291976}
//{CODE-INSERT-END:1281291976}
    }


    public int getSeqNr()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-664583601}
//{CODE-INSERT-END:-664583601}
      return seqNr;
      //Custom code
//{CODE-INSERT-BEGIN:872742516}
//{CODE-INSERT-END:872742516}
    }


    public String getAfschriftNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2002709816}
//{CODE-INSERT-END:-2002709816}
      return afschriftNr;
      //Custom code
//{CODE-INSERT-BEGIN:-1954464485}
//{CODE-INSERT-END:-1954464485}
    }


    public String getBladNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-632332403}
//{CODE-INSERT-END:-632332403}
      return bladNr;
      //Custom code
//{CODE-INSERT-BEGIN:1872529654}
//{CODE-INSERT-END:1872529654}
    }


    public Date getBoekdatum()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:82796942}
//{CODE-INSERT-END:82796942}

        Date obj = boekdatum == null?null:new Date(boekdatum.getTime());

      //Custom code
//{CODE-INSERT-BEGIN:1249596904}
//{CODE-INSERT-END:1249596904}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:82796027}
//{CODE-INSERT-END:82796027}
    }


    public String getRekening()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1158739453}
//{CODE-INSERT-END:1158739453}
      return rekening;
      //Custom code
//{CODE-INSERT-BEGIN:1561182342}
//{CODE-INSERT-END:1561182342}
    }


    public String getTegenrekening()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:523157132}
//{CODE-INSERT-END:523157132}
      return tegenrekening;
      //Custom code
//{CODE-INSERT-BEGIN:-962000425}
//{CODE-INSERT-END:-962000425}
    }


    public String getNaam()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2047346917}
//{CODE-INSERT-END:-2047346917}
      return naam;
      //Custom code
//{CODE-INSERT-BEGIN:956752680}
//{CODE-INSERT-END:956752680}
    }


    public String getDC()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:175435867}
//{CODE-INSERT-END:175435867}
      return dC;
      //Custom code
//{CODE-INSERT-BEGIN:1143542248}
//{CODE-INSERT-END:1143542248}
    }


    public BigDecimal getBedrag()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:142355443}
//{CODE-INSERT-END:142355443}
      return bedrag;
      //Custom code
//{CODE-INSERT-BEGIN:118049104}
//{CODE-INSERT-END:118049104}
    }


    public String getMuntsoort()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-604133325}
//{CODE-INSERT-END:-604133325}
      return muntsoort;
      //Custom code
//{CODE-INSERT-BEGIN:-1548266224}
//{CODE-INSERT-END:-1548266224}
    }


    public String getHdrOmschr1()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1623539937}
//{CODE-INSERT-END:-1623539937}
      return hdrOmschr1;
      //Custom code
//{CODE-INSERT-BEGIN:1209867172}
//{CODE-INSERT-END:1209867172}
    }


    public String getHdrOmschr2()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1622616416}
//{CODE-INSERT-END:-1622616416}
      return hdrOmschr2;
      //Custom code
//{CODE-INSERT-BEGIN:1238496323}
//{CODE-INSERT-END:1238496323}
    }


    public String getDtlOmschr1()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-775282567}
//{CODE-INSERT-END:-775282567}
      return dtlOmschr1;
      //Custom code
//{CODE-INSERT-BEGIN:1736041866}
//{CODE-INSERT-END:1736041866}
    }


    public String getDtlOmschr2()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-774359046}
//{CODE-INSERT-END:-774359046}
      return dtlOmschr2;
      //Custom code
//{CODE-INSERT-BEGIN:1764671017}
//{CODE-INSERT-END:1764671017}
    }


    public String getDtlOmschr3()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-773435525}
//{CODE-INSERT-END:-773435525}
      return dtlOmschr3;
      //Custom code
//{CODE-INSERT-BEGIN:1793300168}
//{CODE-INSERT-END:1793300168}
    }


    public String getRemark()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:803583738}
//{CODE-INSERT-END:803583738}

      String obj = null;
	     if (remark != null && remark.length > 0){
	        java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(remark);
	        java.io.ObjectInputStream in = new java.io.ObjectInputStream(bis);
	        obj = (String)in.readObject();
	        in.close();
      }

      //Custom code
//{CODE-INSERT-BEGIN:-1913740548}
//{CODE-INSERT-END:-1913740548}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:803582823}
//{CODE-INSERT-END:803582823}
    }


    public String getInputRecord()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:157912271}
//{CODE-INSERT-END:157912271}

      String obj = null;
	     if (inputRecord != null && inputRecord.length > 0){
	        java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(inputRecord);
	        java.io.ObjectInputStream in = new java.io.ObjectInputStream(bis);
	        obj = (String)in.readObject();
	        in.close();
      }

      //Custom code
//{CODE-INSERT-BEGIN:1113472647}
//{CODE-INSERT-END:1113472647}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:157911356}
//{CODE-INSERT-END:157911356}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setRunNr(int _runNr ) throws Exception {

      if (_runNr == getRunNr())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1369652615}
//{CODE-INSERT-END:1369652615}
      runNr =  _runNr;

      autoUpdate();

    }

    public void setSeqNr(int _seqNr ) throws Exception {

      if (_seqNr == getSeqNr())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:109547611}
//{CODE-INSERT-END:109547611}
      seqNr =  _seqNr;

      autoUpdate();

    }

    public void setAfschriftNr(String _afschriftNr ) throws Exception {

      if (_afschriftNr == null)
          _afschriftNr =  "";
      if (_afschriftNr.equals(getAfschriftNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:748529620}
//{CODE-INSERT-END:748529620}
      afschriftNr =  _afschriftNr;

      autoUpdate();

    }

    public void setBladNr(String _bladNr ) throws Exception {

      if (_bladNr == null)
          _bladNr =  "";
      if (_bladNr.equals(getBladNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1890898689}
//{CODE-INSERT-END:1890898689}
      bladNr =  _bladNr;

      autoUpdate();

    }

    public void setBoekdatum(Date _boekdatum ) throws Exception {

      if (_boekdatum == null)
          _boekdatum =  java.sql.Date.valueOf("2001-01-01");
      if (_boekdatum.equals(getBoekdatum()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:309444084}
//{CODE-INSERT-END:309444084}
      boekdatum =  _boekdatum == null?null: new java.sql.Date( _boekdatum.getTime());

      autoUpdate();

    }

    public void setRekening(String _rekening ) throws Exception {

      if (_rekening == null)
          _rekening =  "";
      if (_rekening.equals(getRekening()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-672703375}
//{CODE-INSERT-END:-672703375}
      rekening =  _rekening;

      autoUpdate();

    }

    public void setTegenrekening(String _tegenrekening ) throws Exception {

      if (_tegenrekening == null)
          _tegenrekening =  "";
      if (_tegenrekening.equals(getTegenrekening()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1235599208}
//{CODE-INSERT-END:-1235599208}
      tegenrekening =  _tegenrekening;

      autoUpdate();

    }

    public void setNaam(String _naam ) throws Exception {

      if (_naam == null)
          _naam =  "";
      if (_naam.equals(getNaam()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-498354289}
//{CODE-INSERT-END:-498354289}
      naam =  _naam;

      autoUpdate();

    }

    public void setDC(String _dC ) throws Exception {

      if (_dC == null)
          _dC =  "";
      if (_dC.equals(getDC()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:2112241103}
//{CODE-INSERT-END:2112241103}
      dC =  _dC;

      autoUpdate();

    }

    public void setBedrag(BigDecimal _bedrag ) throws Exception {

      if (_bedrag == null)
          _bedrag =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_bedrag.equals(getBedrag()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1629380761}
//{CODE-INSERT-END:-1629380761}
      bedrag =  _bedrag;

      autoUpdate();

    }

    public void setMuntsoort(String _muntsoort ) throws Exception {

      if (_muntsoort == null)
          _muntsoort =  "EUR";
      if (_muntsoort.equals(getMuntsoort()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1544286145}
//{CODE-INSERT-END:-1544286145}
      muntsoort =  _muntsoort;

      autoUpdate();

    }

    public void setHdrOmschr1(String _hdrOmschr1 ) throws Exception {

      if (_hdrOmschr1 == null)
          _hdrOmschr1 =  "";
      if (_hdrOmschr1.equals(getHdrOmschr1()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-703506285}
//{CODE-INSERT-END:-703506285}
      hdrOmschr1 =  _hdrOmschr1;

      autoUpdate();

    }

    public void setHdrOmschr2(String _hdrOmschr2 ) throws Exception {

      if (_hdrOmschr2 == null)
          _hdrOmschr2 =  "";
      if (_hdrOmschr2.equals(getHdrOmschr2()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-702582764}
//{CODE-INSERT-END:-702582764}
      hdrOmschr2 =  _hdrOmschr2;

      autoUpdate();

    }

    public void setDtlOmschr1(String _dtlOmschr1 ) throws Exception {

      if (_dtlOmschr1 == null)
          _dtlOmschr1 =  "";
      if (_dtlOmschr1.equals(getDtlOmschr1()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:144751085}
//{CODE-INSERT-END:144751085}
      dtlOmschr1 =  _dtlOmschr1;

      autoUpdate();

    }

    public void setDtlOmschr2(String _dtlOmschr2 ) throws Exception {

      if (_dtlOmschr2 == null)
          _dtlOmschr2 =  "";
      if (_dtlOmschr2.equals(getDtlOmschr2()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:145674606}
//{CODE-INSERT-END:145674606}
      dtlOmschr2 =  _dtlOmschr2;

      autoUpdate();

    }

    public void setDtlOmschr3(String _dtlOmschr3 ) throws Exception {

      if (_dtlOmschr3 == null)
          _dtlOmschr3 =  "";
      if (_dtlOmschr3.equals(getDtlOmschr3()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:146598127}
//{CODE-INSERT-END:146598127}
      dtlOmschr3 =  _dtlOmschr3;

      autoUpdate();

    }

    public void setRemark(String _remark ) throws Exception {

      if ((_remark == null ? getRemark() == null : _remark.equals(getRemark())))
         return;
      preSet();

         if ( _remark == null){
            remark = null;
         } else {
            java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
            java.io.ObjectOutputStream out = new java.io.ObjectOutputStream(bos);
            out.writeObject( _remark);
            out.flush();
            out.close();
         remark = bos.toByteArray();
         }

      autoUpdate();

    }

    public void setInputRecord(String _inputRecord ) throws Exception {

      if ((_inputRecord == null ? getInputRecord() == null : _inputRecord.equals(getInputRecord())))
         return;
      preSet();

         if ( _inputRecord == null){
            inputRecord = null;
         } else {
            java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
            java.io.ObjectOutputStream out = new java.io.ObjectOutputStream(bos);
            out.writeObject( _inputRecord);
            out.flush();
            out.close();
         inputRecord = bos.toByteArray();
         }

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      runNr = 0;
      seqNr = 0;
      afschriftNr =  "";
      bladNr =  "";
      boekdatum =  java.sql.Date.valueOf("2001-01-01");
      rekening =  "";
      tegenrekening =  "";
      naam =  "";
      dC =  "";
      bedrag =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      muntsoort =  "EUR";
      hdrOmschr1 =  "";
      hdrOmschr2 =  "";
      dtlOmschr1 =  "";
      dtlOmschr2 =  "";
      dtlOmschr3 =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.runNr = runNr;
        currentDBImage.seqNr = seqNr;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("BankAfschrift is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         BankAfschriftPK key = new BankAfschriftPK();
         key.setBedrijf(getBedrijf());
         key.setRunNr(getRunNr());
         key.setSeqNr(getSeqNr());
         ((nl.eadmin.db.impl.BankAfschriftManager_Impl)nl.eadmin.db.impl.BankAfschriftManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((BankAfschriftManager_Impl)BankAfschriftManager_Impl.getInstance(dbd)).getResultSet((BankAfschriftPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("55298144_"+ dbd.getDBId() + bedrijf + runNr + seqNr).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getBankAfschriftDataBean()!
    */
    public BankAfschriftDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public BankAfschriftDataBean getBankAfschriftDataBean() throws Exception {
      return getDataBean(null);
    }

    private BankAfschriftDataBean getDataBean(BankAfschriftDataBean bean) throws Exception {

      if (bean == null)
          bean = new BankAfschriftDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setRunNr(getRunNr());
      bean.setSeqNr(getSeqNr());
      bean.setAfschriftNr(getAfschriftNr());
      bean.setBladNr(getBladNr());
      bean.setBoekdatum(getBoekdatum());
      bean.setRekening(getRekening());
      bean.setTegenrekening(getTegenrekening());
      bean.setNaam(getNaam());
      bean.setDC(getDC());
      bean.setBedrag(getBedrag());
      bean.setMuntsoort(getMuntsoort());
      bean.setHdrOmschr1(getHdrOmschr1());
      bean.setHdrOmschr2(getHdrOmschr2());
      bean.setDtlOmschr1(getDtlOmschr1());
      bean.setDtlOmschr2(getDtlOmschr2());
      bean.setDtlOmschr3(getDtlOmschr3());
      bean.setRemark(getRemark());
      bean.setInputRecord(getInputRecord());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.BankAfschrift_Impl))
             return false;
          nl.eadmin.db.impl.BankAfschrift_Impl bo = (nl.eadmin.db.impl.BankAfschrift_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getRunNr() != this.getRunNr())
              return false;
          if (bo.getSeqNr() != this.getSeqNr())
              return false;
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(BankAfschriftDataBean bean) throws Exception {
       set(bean);
    }


    public void set(BankAfschriftDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setRunNr(bean.getRunNr());
           setSeqNr(bean.getSeqNr());
           setAfschriftNr(bean.getAfschriftNr());
           setBladNr(bean.getBladNr());
           setBoekdatum(bean.getBoekdatum());
           setRekening(bean.getRekening());
           setTegenrekening(bean.getTegenrekening());
           setNaam(bean.getNaam());
           setDC(bean.getDC());
           setBedrag(bean.getBedrag());
           setMuntsoort(bean.getMuntsoort());
           setHdrOmschr1(bean.getHdrOmschr1());
           setHdrOmschr2(bean.getHdrOmschr2());
           setDtlOmschr1(bean.getDtlOmschr1());
           setDtlOmschr2(bean.getDtlOmschr2());
           setDtlOmschr3(bean.getDtlOmschr3());
           setRemark(bean.getRemark());
           setInputRecord(bean.getInputRecord());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"BankAfschrift is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.BankAfschrift.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("RunNr",provider) +  " = ? " + ", " + pmd.getFieldName("SeqNr",provider) +  " = ? " + ", " + pmd.getFieldName("AfschriftNr",provider) +  " = ? " + ", " + pmd.getFieldName("BladNr",provider) +  " = ? " + ", " + pmd.getFieldName("Boekdatum",provider) +  " = ? " + ", " + pmd.getFieldName("Rekening",provider) +  " = ? " + ", " + pmd.getFieldName("Tegenrekening",provider) +  " = ? " + ", " + pmd.getFieldName("Naam",provider) +  " = ? " + ", " + pmd.getFieldName("DC",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrag",provider) +  " = ? " + ", " + pmd.getFieldName("Muntsoort",provider) +  " = ? " + ", " + pmd.getFieldName("HdrOmschr1",provider) +  " = ? " + ", " + pmd.getFieldName("HdrOmschr2",provider) +  " = ? " + ", " + pmd.getFieldName("DtlOmschr1",provider) +  " = ? " + ", " + pmd.getFieldName("DtlOmschr2",provider) +  " = ? " + ", " + pmd.getFieldName("DtlOmschr3",provider) +  " = ? " + ", " + pmd.getFieldName("Remark",provider) +  " = ? " + ", " + pmd.getFieldName("InputRecord",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("RunNr",provider) + " = ? " + " AND  " +  pmd.getFieldName("SeqNr",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( runNr),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  new Integer( seqNr),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  afschriftNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  bladNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  boekdatum,  mapping.getJDBCTypeFor("Date"));
        prep.setObject(parmIndex++,  rekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  tegenrekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  naam,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dC,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  bedrag,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  muntsoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  hdrOmschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  hdrOmschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dtlOmschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dtlOmschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dtlOmschr3,  mapping.getJDBCTypeFor("String"));
        if ( remark == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("byte[]"));
       } else {
          prep.setObject(parmIndex++,  remark,  mapping.getJDBCTypeFor("byte[]"));
}
        if ( inputRecord == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("byte[]"));
       } else {
          prep.setObject(parmIndex++,  inputRecord,  mapping.getJDBCTypeFor("byte[]"));
}

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer(image.runNr),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  new Integer(image.seqNr),  mapping.getJDBCTypeFor("int"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("BankAfschrift modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.BankAfschrift.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.BankAfschrift.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("RunNr",provider) + ", " + pmd.getFieldName("SeqNr",provider) + ", " + pmd.getFieldName("AfschriftNr",provider) + ", " + pmd.getFieldName("BladNr",provider) + ", " + pmd.getFieldName("Boekdatum",provider) + ", " + pmd.getFieldName("Rekening",provider) + ", " + pmd.getFieldName("Tegenrekening",provider) + ", " + pmd.getFieldName("Naam",provider) + ", " + pmd.getFieldName("DC",provider) + ", " + pmd.getFieldName("Bedrag",provider) + ", " + pmd.getFieldName("Muntsoort",provider) + ", " + pmd.getFieldName("HdrOmschr1",provider) + ", " + pmd.getFieldName("HdrOmschr2",provider) + ", " + pmd.getFieldName("DtlOmschr1",provider) + ", " + pmd.getFieldName("DtlOmschr2",provider) + ", " + pmd.getFieldName("DtlOmschr3",provider) + ", " + pmd.getFieldName("Remark",provider) + ", " + pmd.getFieldName("InputRecord",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3,  new Integer(runNr),  mapping.getJDBCTypeFor("int"));
        prep.setObject(4,  new Integer(seqNr),  mapping.getJDBCTypeFor("int"));
        prep.setObject(5, afschriftNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6, bladNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(7, boekdatum,  mapping.getJDBCTypeFor("Date"));
        prep.setObject(8, rekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(9, tegenrekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(10, naam,  mapping.getJDBCTypeFor("String"));
        prep.setObject(11, dC,  mapping.getJDBCTypeFor("String"));
        prep.setObject(12, bedrag,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(13, muntsoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(14, hdrOmschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(15, hdrOmschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(16, dtlOmschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(17, dtlOmschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(18, dtlOmschr3,  mapping.getJDBCTypeFor("String"));
        if (remark == null)
          prep.setNull(19,  mapping.getJDBCTypeFor("byte[]"));
        else
          prep.setObject(19, remark,  mapping.getJDBCTypeFor("byte[]"));
        if (inputRecord == null)
          prep.setNull(20,  mapping.getJDBCTypeFor("byte[]"));
        else
          prep.setObject(20, inputRecord,  mapping.getJDBCTypeFor("byte[]"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.runNr = currentDBImage.runNr;
        transactionImage.dbImage.seqNr = currentDBImage.seqNr;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.runNr = transactionImage.dbImage.runNr;
      currentDBImage.seqNr = transactionImage.dbImage.seqNr;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        runNr = transactionImage.dbImage.runNr;
        seqNr = transactionImage.dbImage.seqNr;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private int runNr;
      private int seqNr;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
