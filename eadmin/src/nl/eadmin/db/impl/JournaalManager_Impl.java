package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import nl.eadmin.db.*;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


  public class JournaalManager_Impl extends BusinessObjectManager implements JournaalManager{


    

    private final static String APPLICATION ="eadmin";
    private static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    private static AttributeAccessor generalAccessor = new AttributeAccessor();
    private static boolean verbose = Log.debug();
    private static PersistenceMetaData pmd = PersistenceMetaDataJournaal.getInstance();
    private static Hashtable instances = new Hashtable();

    private DBData  dbd;

    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    private JournaalManager_Impl(DBData dbData){
       dbd = dbData;
    }


    public static JournaalManager getInstance(){
		return getInstance(null);
    }


    public static JournaalManager getInstance(DBData dbd){
		JournaalManager_Impl inst = (JournaalManager_Impl)instances.get(dbd==null?"":dbd);
		if (inst == null){
		  inst = new JournaalManager_Impl(dbd);
		  instances.put(dbd==null?"":dbd,inst);
		}
		return inst;
    }


    public DBData getDBData(){
       return dbd==null?DBData.getDefaultDBData(APPLICATION):dbd;
    }


    public Journaal create(JournaalDataBean bean)  throws Exception {
      Journaal_Impl impl = new Journaal_Impl(getDBData(),  bean);
        impl.assureStorage();
      return (Journaal)impl;
    }


    public Journaal create(String _bedrijf, String _dagboekId, String _boekstuk, int _seqNr) throws Exception {
      Journaal_Impl obj = new Journaal_Impl(getDBData(), _bedrijf, _dagboekId, _boekstuk, _seqNr);
      obj.assureStorage();
      return obj;
    }


    private JournaalPK getPK(ResultSet rs) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      JournaalPK inst =  new JournaalPK();
      inst.setBedrijf(rs.getString(pmd.getFieldName("Bedrijf",provider)));
      inst.setDagboekId(rs.getString(pmd.getFieldName("DagboekId",provider)));
      inst.setBoekstuk(rs.getString(pmd.getFieldName("Boekstuk",provider)));
      inst.setSeqNr(rs.getInt(pmd.getFieldName("SeqNr",provider)));
      return inst; 
    }


    public Journaal findOrCreate (JournaalPK key) throws Exception {

      Journaal obj;
      try{
         obj = findByPrimaryKey( key);
      }catch (FinderException e){
         obj = create( key.getBedrijf (),  key.getDagboekId (),  key.getBoekstuk (),  key.getSeqNr ());
      }

      return obj;
    }


    public void add(JournaalDataBean inst)  throws Exception {
        Journaal_Impl impl = new Journaal_Impl(getDBData(),  inst);
        impl.assureStorage();
    }


    public void removeByPrimaryKey (JournaalPK key) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      Journaal cachedObject = (Journaal)getCachedObjectByPrimaryKey(key);
      if (provider instanceof AtomicDBTransaction)
        dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate((BusinessObject_Impl)cachedObject);
      else
        dbc = provider.getConnection();
      String  pskey = "nl.eadmin.db.Journaal.removeByPrimaryKey";
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider) + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("DagboekId",provider) + "=?" + " AND "  + pmd.getFieldName("Boekstuk",provider) + "=?" + " AND "  + pmd.getFieldName("SeqNr",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDagboekId());
          prep.setString(3, key.getBoekstuk());
          prep.setInt(4, key.getSeqNr());
          int x = prep.executeUpdate();

          if (x == 0) throw new RemoveException("Key not found");
          if (cachedObject != null){
          		DBPersistenceManager.removeFromCache(cachedObject);
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
    }


    public Journaal findByPrimaryKey (JournaalPK key) throws Exception {
      //Custom code
//{CODE-INSERT-BEGIN:-1586979883}
//{CODE-INSERT-END:-1586979883}

      Journaal journaal = getCachedObjectByPrimaryKey(key);
       if (journaal != null)
      	  return journaal;
       ResultSet rs = null;
       try{
           rs = getResultSet(key);
           return (Journaal)getBusinessObject(rs);
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }      //Custom code
//{CODE-INSERT-BEGIN:-933211338}
//{CODE-INSERT-END:-933211338}

    }

    public ResultSet getResultSet(JournaalPK key) throws Exception{
      ResultSet rs = null;
      String  pskey = "nl.eadmin.db.Journaal.findByPrimaryKey1";
      Object inst = null;
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " SELECT * " + 
                                           " FROM " + provider.getPrefix() + pmd.getTableName(provider)  + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("DagboekId",provider) + "=?" + " AND "  + pmd.getFieldName("Boekstuk",provider) + "=?" + " AND "  + pmd.getFieldName("SeqNr",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDagboekId());
          prep.setString(3, key.getBoekstuk());
          prep.setInt(4, key.getSeqNr());
          prep.executeQuery();

          rs = prep.getResultSet();

          if (!rs.next()){
                throw new FinderException("Key not found");
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
      return rs;
    }
    public Journaal getCachedObjectByPrimaryKey (JournaalPK key) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache == null)
          return null;
       Object o = cache.get(new String("753573532_"+ getDBData().getDBId() + key.getBedrijf() + key.getDagboekId() + key.getBoekstuk() + key.getSeqNr()));
       return (Journaal)o;
    }



    public ConnectionProvider getConnectionProvider() throws Exception{
       return DBPersistenceManager.getConnectionProvider(getDBData());
    }


    public PersistenceMetaData getPersistenceMetaData(){
      return pmd;
    }


    public int generalUpdate(String setClause, String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String update = "UPDATE " + provider.getPrefix() + pmd.getTableName(provider)  + " SET " + QueryTranslator.translateObjectQuery(setClause + " WHERE " + whereClause,pmd,provider);
        return dbc.executeUpdate(update);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public int generalDelete(String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String delete = "DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider)  + " WHERE " + QueryTranslator.translateObjectQuery(whereClause,pmd,provider);
        return dbc.executeUpdate(delete);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public Journaal getFirstObject(Query query) throws Exception{
      if (query == null)
         query = QueryFactory.create();
      int prevMax = query.getMaxObjects();
      query.setMaxObjects(1);
      try{
         return (Journaal) ((QueryImplementation)query).execute(this, RETURN_FIRST_OBJECT);
      }catch(Exception e){
         throw e;
      }finally{
         query.setMaxObjects(prevMax);
      }
    }

    public Collection getCollection(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Collection) queryImpl.execute(this, RETURN_COLLECTION);
    }

    public ListIterator getListIterator(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ListIterator) queryImpl.execute(this, RETURN_LISTITERATOR);
    }

    public Document getDocument(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Document) queryImpl.execute(this, RETURN_DOCUMENT);
    }

    public DocumentFragment getDocumentFragment(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (DocumentFragment) queryImpl.execute(this, RETURN_DOCUMENT_FRAGMENT);
    }

    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ArrayList) queryImpl.execute(this,RETURN_DOCUMENT_FRAGMENT_ARRAYLIST );
    }



    protected Collection getCollection(ResultSet rs) throws Exception {
      ArrayList result = new ArrayListImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      return (Collection)result;
    }



    protected Document getDocument(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      Element root = document.getDocumentElement();
      collectResultElements(resultSet, root, getConnectionProvider());
      return document;
    };


    protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      DocumentFragment documentFragment = document.createDocumentFragment();
      collectResultElements(resultSet, documentFragment, getConnectionProvider());
      return documentFragment;
    };


    protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      Document document = domImplementation.createDocument("", "data", null);
      ArrayList arrayList = new ArrayListImpl();
      while (resultSet.next()){
        DocumentFragment documentFragment = document.createDocumentFragment();
        Node rec = documentFragment.appendChild(document.createElement("journaal"));
        addBOFieldsToElement(resultSet, rec, provider);
        arrayList.add(documentFragment);
      }
      return arrayList;
    }


    protected ListIterator getListIterator(ResultSet rs) throws Exception {
      ListIterator result = new ListIteratorImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      ((ListIteratorImpl)result).pointer = 0; 
      return result; 
    }


    protected Object getBusinessObject(ResultSet rs) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache != null){
          ConnectionProvider provider = getConnectionProvider();
          Object o = cache.get(new String("753573532_"+ getDBData().getDBId()+rs.getObject(pmd.getFieldName("Bedrijf",provider))+rs.getObject(pmd.getFieldName("DagboekId",provider))+rs.getObject(pmd.getFieldName("Boekstuk",provider))+rs.getObject(pmd.getFieldName("SeqNr",provider))));
          if (o != null)
             return o;
       }
    	  return DBPersistenceManager.cache(createBusinessObject(rs));
    }

    private Object createBusinessObject(ResultSet rs) throws Exception {
      return new Journaal_Impl(getDBData(), rs);
    }


    private void collectResultElements(ResultSet rs, Node root, ConnectionProvider provider) throws Exception {
      Document doc = root.getOwnerDocument();
      while (rs.next()){
        Node rec = root.appendChild(doc.createElement("journaal"));
        addBOFieldsToElement(rs,rec,provider);
      }
    }


    public static void addBOFieldsToElement(ResultSet rs, Node rec, ConnectionProvider provider) throws Exception {
      DBMapping mapping = provider.getDBMapping();
      Document doc = rec.getOwnerDocument();
      Object o = null;
      String s = null;
      if ((o=rs.getObject(pmd.getFieldName("mutationTimeStamp",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("mutationtimestamp")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Bedrijf",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrijf")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DagboekId",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dagboekid")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Boekstuk",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("boekstuk")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("SeqNr",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("seqnr")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("DagboekSoort",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dagboeksoort")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("RekeningNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("rekeningnr")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Rekeningsoort",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("rekeningsoort")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("RekeningOmschr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("rekeningomschr")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DcNummer",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dcnummer")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("FactuurNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("factuurnr")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getDate(pmd.getFieldName("Boekdatum",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("boekdatum")).appendChild(doc.createTextNode(s));
      if ((o=rs.getObject(pmd.getFieldName("Boekjaar",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("boekjaar")).appendChild(doc.createTextNode(s));
      if ((o=rs.getObject(pmd.getFieldName("Boekperiode",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("boekperiode")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("BtwCode",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("btwcode")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("ICPSoort",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("icpsoort")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("BtwCategorie",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("btwcategorie")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("BtwAangifteKolom",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("btwaangiftekolom")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DC",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dc")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("BedragDebet",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("bedragdebet")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("BedragCredit",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("bedragcredit")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Omschr1",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschr1")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Omschr2",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschr2")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Omschr3",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschr3")).appendChild(doc.createCDATASection(s));
    }

    public nl.ibs.vegas.ExecutableQuery getExecutableQuery() {
    		return QueryFactory.create(this);
    }

    final static String[] keys = new String[]{ "bedrijf","dagboekId","boekstuk","seqNr"};
    public String[] getKeyNames() {
    	return keys;
    }
    final static nl.ibs.vegas.meta.AttributeMeta[] attributes = new nl.ibs.vegas.meta.AttributeMeta[]{
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrijf","bedrijf","bedrijf-short","bedrijf-description","text",String.class,10,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dagboekId","dagboek-id","dagboek-id-short","dagboek-id-description","text",String.class,10,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekstuk","boekstuk","boekstuk-short","boekstuk-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("seqNr","seq-nr","seq-nr-short","seq-nr-description","number",int.class,5,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dagboekSoort","dagboek-soort","dagboek-soort-short","dagboek-soort-description","text",String.class,1,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("rekeningNr","rekening-nr","rekening-nr-short","rekening-nr-description","text",String.class,9,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("rekeningsoort","rekeningsoort","rekeningsoort-short","rekeningsoort-description","text",String.class,1,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("rekeningOmschr","rekening-omschr","rekening-omschr-short","rekening-omschr-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dcNummer","dc-nummer","dc-nummer-short","dc-nummer-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("factuurNr","factuur-nr","factuur-nr-short","factuur-nr-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekdatum","boekdatum","boekdatum-short","boekdatum-description","date",Date.class,0,0,"2001-01-01","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekjaar","boekjaar","boekjaar-short","boekjaar-description","number",int.class,4,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekperiode","boekperiode","boekperiode-short","boekperiode-description","number",int.class,3,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("btwCode","btw-code","btw-code-short","btw-code-description","text",String.class,3,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("iCPSoort","i-c-p-soort","i-c-p-soort-short","i-c-p-soort-description","text",String.class,1,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("btwCategorie","btw-categorie","btw-categorie-short","btw-categorie-description","text",String.class,2,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("btwAangifteKolom","btw-aangifte-kolom","btw-aangifte-kolom-short","btw-aangifte-kolom-description","text",String.class,1,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dC","dc","dc-short","dc-description","text",String.class,1,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedragDebet","bedrag-debet","bedrag-debet-short","bedrag-debet-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedragCredit","bedrag-credit","bedrag-credit-short","bedrag-credit-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschr1","omschr1","omschr1-short","omschr1-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschr2","omschr2","omschr2-short","omschr2-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschr3","omschr3","omschr3-short","omschr3-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null)
    };
    public nl.ibs.vegas.meta.AttributeMeta[] getAttributes() {
    	return attributes;
    }
    public Class getClassType() {
    	return nl.eadmin.db.Journaal.class;
    }
    public String getNameField() {
    	return "seqNr";
    }
    public String getDescriptionField() {
    	return "seqNr";
    }
    public String getTextKey() {
    	return "journaal";
    }
    public String getTextKeyShort() {
    	return "journaal-short";
    }
    public String getTextKeyDescription() {
    	return "journaal-description";
    }
    public nl.ibs.vegas.meta.AttributeMeta getAttribute(String name) {
    	if (attributes == null || name == null)
    		return null;
    	for (int i = 0; i < attributes.length; i++)
    		if (name.equals(attributes[i].getName()))
    			return attributes[i];
    	return null;
    }
  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
