package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import nl.eadmin.db.*;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


  public class DetailDataManager_Impl extends BusinessObjectManager implements DetailDataManager{


    

    private final static String APPLICATION ="eadmin";
    private static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    private static AttributeAccessor generalAccessor = new AttributeAccessor();
    private static boolean verbose = Log.debug();
    private static PersistenceMetaData pmd = PersistenceMetaDataDetailData.getInstance();
    private static Hashtable instances = new Hashtable();

    private DBData  dbd;

    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    private DetailDataManager_Impl(DBData dbData){
       dbd = dbData;
    }


    public static DetailDataManager getInstance(){
		return getInstance(null);
    }


    public static DetailDataManager getInstance(DBData dbd){
		DetailDataManager_Impl inst = (DetailDataManager_Impl)instances.get(dbd==null?"":dbd);
		if (inst == null){
		  inst = new DetailDataManager_Impl(dbd);
		  instances.put(dbd==null?"":dbd,inst);
		}
		return inst;
    }


    public DBData getDBData(){
       return dbd==null?DBData.getDefaultDBData(APPLICATION):dbd;
    }


    public DetailData create(DetailDataDataBean bean)  throws Exception {
      DetailData_Impl impl = new DetailData_Impl(getDBData(),  bean);
        impl.assureStorage();
      return (DetailData)impl;
    }


    public DetailData create(String _bedrijf, String _dagboekId, String _boekstuk, int _boekstukRegel) throws Exception {
      DetailData_Impl obj = new DetailData_Impl(getDBData(), _bedrijf, _dagboekId, _boekstuk, _boekstukRegel);
      obj.assureStorage();
      return obj;
    }


    private DetailDataPK getPK(ResultSet rs) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DetailDataPK inst =  new DetailDataPK();
      inst.setBedrijf(rs.getString(pmd.getFieldName("Bedrijf",provider)));
      inst.setDagboekId(rs.getString(pmd.getFieldName("DagboekId",provider)));
      inst.setBoekstuk(rs.getString(pmd.getFieldName("Boekstuk",provider)));
      inst.setBoekstukRegel(rs.getInt(pmd.getFieldName("BoekstukRegel",provider)));
      return inst; 
    }


    public DetailData findOrCreate (DetailDataPK key) throws Exception {

      DetailData obj;
      try{
         obj = findByPrimaryKey( key);
      }catch (FinderException e){
         obj = create( key.getBedrijf (),  key.getDagboekId (),  key.getBoekstuk (),  key.getBoekstukRegel ());
      }

      return obj;
    }


    public void add(DetailDataDataBean inst)  throws Exception {
        DetailData_Impl impl = new DetailData_Impl(getDBData(),  inst);
        impl.assureStorage();
    }


    public void removeByPrimaryKey (DetailDataPK key) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      DetailData cachedObject = (DetailData)getCachedObjectByPrimaryKey(key);
      if (provider instanceof AtomicDBTransaction)
        dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate((BusinessObject_Impl)cachedObject);
      else
        dbc = provider.getConnection();
      String  pskey = "nl.eadmin.db.DetailData.removeByPrimaryKey";
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider) + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("DagboekId",provider) + "=?" + " AND "  + pmd.getFieldName("Boekstuk",provider) + "=?" + " AND "  + pmd.getFieldName("BoekstukRegel",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDagboekId());
          prep.setString(3, key.getBoekstuk());
          prep.setInt(4, key.getBoekstukRegel());
          int x = prep.executeUpdate();

          if (x == 0) throw new RemoveException("Key not found");
          if (cachedObject != null){
          		DBPersistenceManager.removeFromCache(cachedObject);
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
    }


    public DetailData findByPrimaryKey (DetailDataPK key) throws Exception {
      //Custom code
//{CODE-INSERT-BEGIN:-1586979883}
//{CODE-INSERT-END:-1586979883}

      DetailData detailData = getCachedObjectByPrimaryKey(key);
       if (detailData != null)
      	  return detailData;
       ResultSet rs = null;
       try{
           rs = getResultSet(key);
           return (DetailData)getBusinessObject(rs);
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }      //Custom code
//{CODE-INSERT-BEGIN:-933211338}
//{CODE-INSERT-END:-933211338}

    }

    public ResultSet getResultSet(DetailDataPK key) throws Exception{
      ResultSet rs = null;
      String  pskey = "nl.eadmin.db.DetailData.findByPrimaryKey1";
      Object inst = null;
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " SELECT * " + 
                                           " FROM " + provider.getPrefix() + pmd.getTableName(provider)  + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("DagboekId",provider) + "=?" + " AND "  + pmd.getFieldName("Boekstuk",provider) + "=?" + " AND "  + pmd.getFieldName("BoekstukRegel",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDagboekId());
          prep.setString(3, key.getBoekstuk());
          prep.setInt(4, key.getBoekstukRegel());
          prep.executeQuery();

          rs = prep.getResultSet();

          if (!rs.next()){
                throw new FinderException("Key not found");
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
      return rs;
    }
    public DetailData getCachedObjectByPrimaryKey (DetailDataPK key) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache == null)
          return null;
       Object o = cache.get(new String("624086135_"+ getDBData().getDBId() + key.getBedrijf() + key.getDagboekId() + key.getBoekstuk() + key.getBoekstukRegel()));
       return (DetailData)o;
    }



    public ConnectionProvider getConnectionProvider() throws Exception{
       return DBPersistenceManager.getConnectionProvider(getDBData());
    }


    public PersistenceMetaData getPersistenceMetaData(){
      return pmd;
    }


    public int generalUpdate(String setClause, String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String update = "UPDATE " + provider.getPrefix() + pmd.getTableName(provider)  + " SET " + QueryTranslator.translateObjectQuery(setClause + " WHERE " + whereClause,pmd,provider);
        return dbc.executeUpdate(update);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public int generalDelete(String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String delete = "DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider)  + " WHERE " + QueryTranslator.translateObjectQuery(whereClause,pmd,provider);
        return dbc.executeUpdate(delete);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public DetailData getFirstObject(Query query) throws Exception{
      if (query == null)
         query = QueryFactory.create();
      int prevMax = query.getMaxObjects();
      query.setMaxObjects(1);
      try{
         return (DetailData) ((QueryImplementation)query).execute(this, RETURN_FIRST_OBJECT);
      }catch(Exception e){
         throw e;
      }finally{
         query.setMaxObjects(prevMax);
      }
    }

    public Collection getCollection(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Collection) queryImpl.execute(this, RETURN_COLLECTION);
    }

    public ListIterator getListIterator(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ListIterator) queryImpl.execute(this, RETURN_LISTITERATOR);
    }

    public Document getDocument(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Document) queryImpl.execute(this, RETURN_DOCUMENT);
    }

    public DocumentFragment getDocumentFragment(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (DocumentFragment) queryImpl.execute(this, RETURN_DOCUMENT_FRAGMENT);
    }

    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ArrayList) queryImpl.execute(this,RETURN_DOCUMENT_FRAGMENT_ARRAYLIST );
    }



    protected Collection getCollection(ResultSet rs) throws Exception {
      ArrayList result = new ArrayListImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      return (Collection)result;
    }



    protected Document getDocument(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      Element root = document.getDocumentElement();
      collectResultElements(resultSet, root, getConnectionProvider());
      return document;
    };


    protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      DocumentFragment documentFragment = document.createDocumentFragment();
      collectResultElements(resultSet, documentFragment, getConnectionProvider());
      return documentFragment;
    };


    protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      Document document = domImplementation.createDocument("", "data", null);
      ArrayList arrayList = new ArrayListImpl();
      while (resultSet.next()){
        DocumentFragment documentFragment = document.createDocumentFragment();
        Node rec = documentFragment.appendChild(document.createElement("detaildata"));
        addBOFieldsToElement(resultSet, rec, provider);
        arrayList.add(documentFragment);
      }
      return arrayList;
    }


    protected ListIterator getListIterator(ResultSet rs) throws Exception {
      ListIterator result = new ListIteratorImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      ((ListIteratorImpl)result).pointer = 0; 
      return result; 
    }


    protected Object getBusinessObject(ResultSet rs) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache != null){
          ConnectionProvider provider = getConnectionProvider();
          Object o = cache.get(new String("624086135_"+ getDBData().getDBId()+rs.getObject(pmd.getFieldName("Bedrijf",provider))+rs.getObject(pmd.getFieldName("DagboekId",provider))+rs.getObject(pmd.getFieldName("Boekstuk",provider))+rs.getObject(pmd.getFieldName("BoekstukRegel",provider))));
          if (o != null)
             return o;
       }
    	  return DBPersistenceManager.cache(createBusinessObject(rs));
    }

    private Object createBusinessObject(ResultSet rs) throws Exception {
      return new DetailData_Impl(getDBData(), rs);
    }


    private void collectResultElements(ResultSet rs, Node root, ConnectionProvider provider) throws Exception {
      Document doc = root.getOwnerDocument();
      while (rs.next()){
        Node rec = root.appendChild(doc.createElement("detaildata"));
        addBOFieldsToElement(rs,rec,provider);
      }
    }


    public static void addBOFieldsToElement(ResultSet rs, Node rec, ConnectionProvider provider) throws Exception {
      DBMapping mapping = provider.getDBMapping();
      Document doc = rec.getOwnerDocument();
      Object o = null;
      String s = null;
      if ((o=rs.getObject(pmd.getFieldName("mutationTimeStamp",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("mutationtimestamp")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Bedrijf",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrijf")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DagboekId",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dagboekid")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Boekstuk",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("boekstuk")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("BoekstukRegel",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("boekstukregel")).appendChild(doc.createTextNode(s));
      if ((o=rs.getDate(pmd.getFieldName("DatumLevering",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("datumlevering")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Omschr1",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschr1")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Omschr2",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschr2")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Omschr3",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschr3")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("Aantal",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("aantal")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Eenheid",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("eenheid")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("Prijs",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("prijs")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("BtwCode",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("btwcode")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("BedragExcl",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("bedragexcl")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("BedragBtw",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("bedragbtw")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("BedragIncl",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("bedragincl")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("BedragDebet",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("bedragdebet")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBigDecimal(pmd.getFieldName("BedragCredit",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("bedragcredit")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("DC",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dc")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Rekening",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("rekening")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DcNummer",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dcnummer")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("FactuurNummer",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("factuurnummer")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub1",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub1")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub2",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub2")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub3",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub3")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub4",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub4")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("VrijeRub5",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub5")).appendChild(doc.createCDATASection(s));
    }

    public nl.ibs.vegas.ExecutableQuery getExecutableQuery() {
    		return QueryFactory.create(this);
    }

    final static String[] keys = new String[]{ "bedrijf","dagboekId","boekstuk","boekstukRegel"};
    public String[] getKeyNames() {
    	return keys;
    }
    final static nl.ibs.vegas.meta.AttributeMeta[] attributes = new nl.ibs.vegas.meta.AttributeMeta[]{
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrijf","bedrijf","bedrijf-short","bedrijf-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dagboekId","dagboek-id","dagboek-id-short","dagboek-id-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekstuk","boekstuk","boekstuk-short","boekstuk-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekstukRegel","boekstuk-regel","boekstuk-regel-short","boekstuk-regel-description","number",int.class,3,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("datumLevering","datum-levering","datum-levering-short","datum-levering-description","date",Date.class,0,0,"2001-01-01","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschr1","omschr1","omschr1-short","omschr1-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschr2","omschr2","omschr2-short","omschr2-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschr3","omschr3","omschr3-short","omschr3-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("aantal","aantal","aantal-short","aantal-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("eenheid","eenheid","eenheid-short","eenheid-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("prijs","prijs","prijs-short","prijs-description","decimal",BigDecimal.class,15,2,"0.00","false","false","true","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("btwCode","btw-code","btw-code-short","btw-code-description","text",String.class,3,0,null,"false","false","true","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedragExcl","bedrag-excl","bedrag-excl-short","bedrag-excl-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedragBtw","bedrag-btw","bedrag-btw-short","bedrag-btw-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedragIncl","bedrag-incl","bedrag-incl-short","bedrag-incl-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedragDebet","bedrag-debet","bedrag-debet-short","bedrag-debet-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedragCredit","bedrag-credit","bedrag-credit-short","bedrag-credit-description","decimal",BigDecimal.class,15,2,"0.00","false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dC","dc","dc-short","dc-description","text",String.class,1,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("rekening","rekening","rekening-short","rekening-description","text",String.class,9,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dcNummer","dc-nummer","dc-nummer-short","dc-nummer-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("factuurNummer","factuur-nummer","factuur-nummer-short","factuur-nummer-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub1","vrije-rub1","vrije-rub1-short","vrije-rub1-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub2","vrije-rub2","vrije-rub2-short","vrije-rub2-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub3","vrije-rub3","vrije-rub3-short","vrije-rub3-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub4","vrije-rub4","vrije-rub4-short","vrije-rub4-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub5","vrije-rub5","vrije-rub5-short","vrije-rub5-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null)
    };
    public nl.ibs.vegas.meta.AttributeMeta[] getAttributes() {
    	return attributes;
    }
    public Class getClassType() {
    	return nl.eadmin.db.DetailData.class;
    }
    public String getNameField() {
    	return "boekstukRegel";
    }
    public String getDescriptionField() {
    	return "boekstukRegel";
    }
    public String getTextKey() {
    	return "detail-data";
    }
    public String getTextKeyShort() {
    	return "detail-data-short";
    }
    public String getTextKeyDescription() {
    	return "detail-data-description";
    }
    public nl.ibs.vegas.meta.AttributeMeta getAttribute(String name) {
    	if (attributes == null || name == null)
    		return null;
    	for (int i = 0; i < attributes.length; i++)
    		if (name.equals(attributes[i].getName()))
    			return attributes[i];
    	return null;
    }
  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
