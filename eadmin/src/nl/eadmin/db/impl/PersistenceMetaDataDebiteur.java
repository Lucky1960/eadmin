package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataDebiteur implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataDebiteur();

      persistables.add("nl.eadmin.db.impl.Debiteur_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("debNr");
      javaVariableNames.add("naam");
      javaVariableNames.add("contactPersoon");
      javaVariableNames.add("email");
      javaVariableNames.add("kvkNr");
      javaVariableNames.add("btwNr");
      javaVariableNames.add("ibanNr");
      javaVariableNames.add("betaalTermijn");
      javaVariableNames.add("dtmLaatsteAanmaning");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("debNr");

      fieldNames.put("mutationTimeStamp","DEBITEUR_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("DebNr","DEBNR");
      fieldNames.put("Naam","NAAM");
      fieldNames.put("ContactPersoon","CONTACT");
      fieldNames.put("Email","EMAIL");
      fieldNames.put("KvkNr","KVKNR");
      fieldNames.put("BtwNr","BTWNR");
      fieldNames.put("IbanNr","IBANNR");
      fieldNames.put("BetaalTermijn","BETAALTRM");
      fieldNames.put("DtmLaatsteAanmaning","DTMAANMNG");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("DebNr","Debiteurennummer");
      fieldDescriptions.put("Naam","Naam");
      fieldDescriptions.put("ContactPersoon","Contactpersoon");
      fieldDescriptions.put("Email","Emailadres");
      fieldDescriptions.put("KvkNr","KvK nummer");
      fieldDescriptions.put("BtwNr","BTW nummer");
      fieldDescriptions.put("IbanNr","Iban nummer");
      fieldDescriptions.put("BetaalTermijn","Betaaltermijn in dagen");
      fieldDescriptions.put("DtmLaatsteAanmaning","Datum laatste aanmaning");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("debNr","DebNr");
      javaToBONameMapping.put("naam","Naam");
      javaToBONameMapping.put("contactPersoon","ContactPersoon");
      javaToBONameMapping.put("email","Email");
      javaToBONameMapping.put("kvkNr","KvkNr");
      javaToBONameMapping.put("btwNr","BtwNr");
      javaToBONameMapping.put("ibanNr","IbanNr");
      javaToBONameMapping.put("betaalTermijn","BetaalTermijn");
      javaToBONameMapping.put("dtmLaatsteAanmaning","DtmLaatsteAanmaning");

    }

    private PersistenceMetaDataDebiteur(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Debiteur";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Debiteur" , "DEBITEUR");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Debiteur" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Debiteuren";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DebNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Naam", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("ContactPersoon", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Email", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("KvkNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BtwNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 18, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("IbanNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 18, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BetaalTermijn", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 3, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("DtmLaatsteAanmaning", provider)	 + " " + provider.getDBMapping().getSQLDefinition("Date", 0, 0) + "	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[11][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("DebNr", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Debiteurennummer"};
      columnDefinitions[3]=new String[]{getFieldName("Naam", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Naam"};
      columnDefinitions[4]=new String[]{getFieldName("ContactPersoon", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Contactpersoon"};
      columnDefinitions[5]=new String[]{getFieldName("Email", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Emailadres"};
      columnDefinitions[6]=new String[]{getFieldName("KvkNr", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "KvK nummer"};
      columnDefinitions[7]=new String[]{getFieldName("BtwNr", provider), provider.getDBMapping().getSQLDefinition("String", 18, 0) , "''"  , " NOT NULL", "BTW nummer"};
      columnDefinitions[8]=new String[]{getFieldName("IbanNr", provider), provider.getDBMapping().getSQLDefinition("String", 18, 0) , "''"  , " NOT NULL", "Iban nummer"};
      columnDefinitions[9]=new String[]{getFieldName("BetaalTermijn", provider), provider.getDBMapping().getSQLDefinition("int", 3, 0) , "0"  , " NOT NULL", "Betaaltermijn in dagen"};
      columnDefinitions[10]=new String[]{getFieldName("DtmLaatsteAanmaning", provider), provider.getDBMapping().getSQLDefinition("Date", 0, 0) ,  null  , "", "Datum laatste aanmaning"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_DEBITEUR_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_DEBITEUR_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DebNr", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_DEBITEUR", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_DEBITEUR PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DebNr", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
