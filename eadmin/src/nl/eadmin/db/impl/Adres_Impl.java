package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;

import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;

import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Adres_Impl  implements Adres, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataAdres.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String dc;
    protected String dcNr;
    protected int adresType;
    protected String straat;
    protected int huisNr;
    protected String huisNrToev;
    protected String extraAdresRegel;
    protected String postcode;
    protected String plaats;
    protected String land;
    protected String telefoon;
    protected String mobiel;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Adres_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Adres_Impl (DBData _dbd ,  String _bedrijf, String _dc, String _dcNr, int _adresType) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setDc(_dc);
        setDcNr(_dcNr);
        setAdresType(_adresType);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Adres_Impl (DBData _dbd, AdresDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.AdresDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Adres with " + bean.getClass().getName() + "!! Use nl.eadmin.db.AdresDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setDc(bean.getDc());
        setDcNr(bean.getDcNr());
        setAdresType(bean.getAdresType());
        setStraat(bean.getStraat());
        setHuisNr(bean.getHuisNr());
        setHuisNrToev(bean.getHuisNrToev());
        setExtraAdresRegel(bean.getExtraAdresRegel());
        setPostcode(bean.getPostcode());
        setPlaats(bean.getPlaats());
        setLand(bean.getLand());
        setTelefoon(bean.getTelefoon());
        setMobiel(bean.getMobiel());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Adres_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      dc = rs.getString(pmd.getFieldName("Dc",provider));
      dcNr = rs.getString(pmd.getFieldName("DcNr",provider));
      adresType = rs.getInt(pmd.getFieldName("AdresType",provider));
      straat = rs.getString(pmd.getFieldName("Straat",provider));
      huisNr = rs.getInt(pmd.getFieldName("HuisNr",provider));
      huisNrToev = rs.getString(pmd.getFieldName("HuisNrToev",provider));
      extraAdresRegel = rs.getString(pmd.getFieldName("ExtraAdresRegel",provider));
      postcode = rs.getString(pmd.getFieldName("Postcode",provider));
      plaats = rs.getString(pmd.getFieldName("Plaats",provider));
      land = rs.getString(pmd.getFieldName("Land",provider));
      telefoon = rs.getString(pmd.getFieldName("Telefoon",provider));
      mobiel = rs.getString(pmd.getFieldName("Mobiel",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       AdresPK key = new AdresPK();
       key.setBedrijf(getBedrijf());
       key.setDc(getDc());
       key.setDcNr(getDcNr());
       key.setAdresType(getAdresType());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getDc()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:204988539}
//{CODE-INSERT-END:204988539}
      return dc;
      //Custom code
//{CODE-INSERT-BEGIN:2059675080}
//{CODE-INSERT-END:2059675080}
    }


    public String getDcNr()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1059909985}
//{CODE-INSERT-END:-1059909985}
      return trim(dcNr);
      //Custom code
//{CODE-INSERT-BEGIN:1502526500}
//{CODE-INSERT-END:1502526500}
    }


    public int getAdresType()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:1179913283}
//{CODE-INSERT-END:1179913283}
      return adresType;
      //Custom code
//{CODE-INSERT-BEGIN:-2077396224}
//{CODE-INSERT-END:-2077396224}
    }


    public String getStraat()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:488433279}
//{CODE-INSERT-END:488433279}
      return trim(straat);
      //Custom code
//{CODE-INSERT-BEGIN:-2038439868}
//{CODE-INSERT-END:-2038439868}
    }


    public int getHuisNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:586599703}
//{CODE-INSERT-END:586599703}
      return huisNr;
      //Custom code
//{CODE-INSERT-BEGIN:1004719276}
//{CODE-INSERT-END:1004719276}
    }


    public String getHuisNrToev()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1097249315}
//{CODE-INSERT-END:1097249315}
      return trim(huisNrToev);
      //Custom code
//{CODE-INSERT-BEGIN:-345011936}
//{CODE-INSERT-END:-345011936}
    }


    public String getExtraAdresRegel()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:2004179226}
//{CODE-INSERT-END:2004179226}
      return trim(extraAdresRegel);
      //Custom code
//{CODE-INSERT-BEGIN:2000011529}
//{CODE-INSERT-END:2000011529}
    }


    public String getPostcode()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:242647433}
//{CODE-INSERT-END:242647433}
      return trim(postcode);
      //Custom code
//{CODE-INSERT-BEGIN:-1067866502}
//{CODE-INSERT-END:-1067866502}
    }


    public String getPlaats()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-662233961}
		if (land.trim().length() > 0 && !land.trim().equals("NL")) {
			Locale l = new Locale("NL", land.trim());
			String plaats2 = plaats + " (" + l.getDisplayCountry(l).toUpperCase() + ")";
			if (true) return plaats2;
		}
//{CODE-INSERT-END:-662233961}
      return trim(plaats);
      //Custom code
//{CODE-INSERT-BEGIN:945581356}
//{CODE-INSERT-END:945581356}
    }


    public String getLand()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-874133017}
//{CODE-INSERT-END:-874133017}
      return land;
      //Custom code
//{CODE-INSERT-BEGIN:-1328322084}
//{CODE-INSERT-END:-1328322084}
    }


    public String getTelefoon()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1227959182}
//{CODE-INSERT-END:1227959182}
      return trim(telefoon);
      //Custom code
//{CODE-INSERT-BEGIN:-587973355}
//{CODE-INSERT-END:-587973355}
    }


    public String getMobiel()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1383086260}
//{CODE-INSERT-END:-1383086260}
      return trim(mobiel);
      //Custom code
//{CODE-INSERT-BEGIN:73996567}
//{CODE-INSERT-END:73996567}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setDc(String _dc ) throws Exception {

      if (_dc !=  null && _dc.length() != 1){
         _dc = _dc.trim();
         _dc = " ".substring(_dc.length()) + _dc;
      }
      if (_dc == null)
          _dc =  "";
      if (_dc.equals(getDc()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:2141793775}
//{CODE-INSERT-END:2141793775}
      dc =  _dc;

      autoUpdate();

    }

    public void setDcNr(String _dcNr ) throws Exception {

      if (_dcNr !=  null)
         _dcNr = _dcNr.trim();
      if (_dcNr !=  null)
         _dcNr = _dcNr.toUpperCase();
      if (_dcNr == null)
          _dcNr =  "";
      if (_dcNr.equals(getDcNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:489082643}
//{CODE-INSERT-END:489082643}
      dcNr =  _dcNr;

      autoUpdate();

    }

    public void setAdresType(int _adresType ) throws Exception {

      if (_adresType == getAdresType())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:239760463}
//{CODE-INSERT-END:239760463}
      adresType =  _adresType;

      autoUpdate();

    }

    public void setStraat(String _straat ) throws Exception {

      if (_straat !=  null)
         _straat = _straat.trim();
      if (_straat == null)
          _straat =  "";
      if (_straat.equals(getStraat()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1283302925}
//{CODE-INSERT-END:-1283302925}
      straat =  _straat;

      autoUpdate();

    }

    public void setHuisNr(int _huisNr ) throws Exception {

      if (_huisNr == getHuisNr())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1185136501}
//{CODE-INSERT-END:-1185136501}
      huisNr =  _huisNr;

      autoUpdate();

    }

    public void setHuisNrToev(String _huisNrToev ) throws Exception {

      if (_huisNrToev !=  null)
         _huisNrToev = _huisNrToev.trim();
      if (_huisNrToev == null)
          _huisNrToev =  "";
      if (_huisNrToev.equals(getHuisNrToev()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:2017282967}
//{CODE-INSERT-END:2017282967}
      huisNrToev =  _huisNrToev;

      autoUpdate();

    }

    public void setExtraAdresRegel(String _extraAdresRegel ) throws Exception {

      if (_extraAdresRegel !=  null)
         _extraAdresRegel = _extraAdresRegel.trim();
      if (_extraAdresRegel == null)
          _extraAdresRegel =  "";
      if (_extraAdresRegel.equals(getExtraAdresRegel()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-238516186}
//{CODE-INSERT-END:-238516186}
      extraAdresRegel =  _extraAdresRegel;

      autoUpdate();

    }

    public void setPostcode(String _postcode ) throws Exception {

      if (_postcode !=  null)
         _postcode = _postcode.trim();
      if (_postcode == null)
          _postcode =  "";
      if (_postcode.equals(getPostcode()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1588795395}
//{CODE-INSERT-END:-1588795395}
      postcode =  _postcode;

      autoUpdate();

    }

    public void setPlaats(String _plaats ) throws Exception {

      if (_plaats !=  null)
         _plaats = _plaats.trim();
      if (_plaats == null)
          _plaats =  "";
      if (_plaats.equals(getPlaats()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1860997131}
//{CODE-INSERT-END:1860997131}
      plaats =  _plaats;

      autoUpdate();

    }

    public void setLand(String _land ) throws Exception {

      if (_land == null)
          _land =  "NL";
      if (_land.equals(getLand()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:674859611}
//{CODE-INSERT-END:674859611}
      land =  _land;

      autoUpdate();

    }

    public void setTelefoon(String _telefoon ) throws Exception {

      if (_telefoon !=  null)
         _telefoon = _telefoon.trim();
      if (_telefoon == null)
          _telefoon =  "";
      if (_telefoon.equals(getTelefoon()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-603483646}
//{CODE-INSERT-END:-603483646}
      telefoon =  _telefoon;

      autoUpdate();

    }

    public void setMobiel(String _mobiel ) throws Exception {

      if (_mobiel !=  null)
         _mobiel = _mobiel.trim();
      if (_mobiel == null)
          _mobiel =  "";
      if (_mobiel.equals(getMobiel()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1140144832}
//{CODE-INSERT-END:1140144832}
      mobiel =  _mobiel;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      dc =  "";
      dcNr =  "";
      adresType = 0;
      straat =  "";
      huisNr = 0;
      huisNrToev =  "";
      extraAdresRegel =  "";
      postcode =  "";
      plaats =  "";
      land =  "NL";
      telefoon =  "";
      mobiel =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.dc = dc;
        currentDBImage.dcNr = dcNr;
        currentDBImage.adresType = adresType;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Adres is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         AdresPK key = new AdresPK();
         key.setBedrijf(getBedrijf());
         key.setDc(getDc());
         key.setDcNr(getDcNr());
         key.setAdresType(getAdresType());
         ((nl.eadmin.db.impl.AdresManager_Impl)nl.eadmin.db.impl.AdresManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((AdresManager_Impl)AdresManager_Impl.getInstance(dbd)).getResultSet((AdresPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("1425873729_"+ dbd.getDBId() + bedrijf + dc + dcNr + adresType).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getAdresDataBean()!
    */
    public AdresDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public AdresDataBean getAdresDataBean() throws Exception {
      return getDataBean(null);
    }

    private AdresDataBean getDataBean(AdresDataBean bean) throws Exception {

      if (bean == null)
          bean = new AdresDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setDc(getDc());
      bean.setDcNr(getDcNr());
      bean.setAdresType(getAdresType());
      bean.setStraat(getStraat());
      bean.setHuisNr(getHuisNr());
      bean.setHuisNrToev(getHuisNrToev());
      bean.setExtraAdresRegel(getExtraAdresRegel());
      bean.setPostcode(getPostcode());
      bean.setPlaats(getPlaats());
      bean.setLand(getLand());
      bean.setTelefoon(getTelefoon());
      bean.setMobiel(getMobiel());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Adres_Impl))
             return false;
          nl.eadmin.db.impl.Adres_Impl bo = (nl.eadmin.db.impl.Adres_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getDc() == null){
              if (this.getDc() != null)
                  return false;
          }else if (!bo.getDc().equals(this.getDc())){
              return false;
          }
          if (bo.getDcNr() == null){
              if (this.getDcNr() != null)
                  return false;
          }else if (!bo.getDcNr().equals(this.getDcNr())){
              return false;
          }
          if (bo.getAdresType() != this.getAdresType())
              return false;
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(AdresDataBean bean) throws Exception {
       set(bean);
    }


    public void set(AdresDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setDc(bean.getDc());
           setDcNr(bean.getDcNr());
           setAdresType(bean.getAdresType());
           setStraat(bean.getStraat());
           setHuisNr(bean.getHuisNr());
           setHuisNrToev(bean.getHuisNrToev());
           setExtraAdresRegel(bean.getExtraAdresRegel());
           setPostcode(bean.getPostcode());
           setPlaats(bean.getPlaats());
           setLand(bean.getLand());
           setTelefoon(bean.getTelefoon());
           setMobiel(bean.getMobiel());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Adres is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Adres.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("Dc",provider) +  " = ? " + ", " + pmd.getFieldName("DcNr",provider) +  " = ? " + ", " + pmd.getFieldName("AdresType",provider) +  " = ? " + ", " + pmd.getFieldName("Straat",provider) +  " = ? " + ", " + pmd.getFieldName("HuisNr",provider) +  " = ? " + ", " + pmd.getFieldName("HuisNrToev",provider) +  " = ? " + ", " + pmd.getFieldName("ExtraAdresRegel",provider) +  " = ? " + ", " + pmd.getFieldName("Postcode",provider) +  " = ? " + ", " + pmd.getFieldName("Plaats",provider) +  " = ? " + ", " + pmd.getFieldName("Land",provider) +  " = ? " + ", " + pmd.getFieldName("Telefoon",provider) +  " = ? " + ", " + pmd.getFieldName("Mobiel",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("Dc",provider) + " = ? " + " AND  " +  pmd.getFieldName("DcNr",provider) + " = ? " + " AND  " +  pmd.getFieldName("AdresType",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dc,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dcNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( adresType),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  straat,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( huisNr),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  huisNrToev,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  extraAdresRegel,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  postcode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  plaats,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  land,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  telefoon,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  mobiel,  mapping.getJDBCTypeFor("String"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.dc,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.dcNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer(image.adresType),  mapping.getJDBCTypeFor("int"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Adres modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Adres.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Adres.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("Dc",provider) + ", " + pmd.getFieldName("DcNr",provider) + ", " + pmd.getFieldName("AdresType",provider) + ", " + pmd.getFieldName("Straat",provider) + ", " + pmd.getFieldName("HuisNr",provider) + ", " + pmd.getFieldName("HuisNrToev",provider) + ", " + pmd.getFieldName("ExtraAdresRegel",provider) + ", " + pmd.getFieldName("Postcode",provider) + ", " + pmd.getFieldName("Plaats",provider) + ", " + pmd.getFieldName("Land",provider) + ", " + pmd.getFieldName("Telefoon",provider) + ", " + pmd.getFieldName("Mobiel",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, dc,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, dcNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5,  new Integer(adresType),  mapping.getJDBCTypeFor("int"));
        prep.setObject(6, straat,  mapping.getJDBCTypeFor("String"));
        prep.setObject(7,  new Integer(huisNr),  mapping.getJDBCTypeFor("int"));
        prep.setObject(8, huisNrToev,  mapping.getJDBCTypeFor("String"));
        prep.setObject(9, extraAdresRegel,  mapping.getJDBCTypeFor("String"));
        prep.setObject(10, postcode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(11, plaats,  mapping.getJDBCTypeFor("String"));
        prep.setObject(12, land,  mapping.getJDBCTypeFor("String"));
        prep.setObject(13, telefoon,  mapping.getJDBCTypeFor("String"));
        prep.setObject(14, mobiel,  mapping.getJDBCTypeFor("String"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.dc = currentDBImage.dc;
        transactionImage.dbImage.dcNr = currentDBImage.dcNr;
        transactionImage.dbImage.adresType = currentDBImage.adresType;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.dc = transactionImage.dbImage.dc;
      currentDBImage.dcNr = transactionImage.dbImage.dcNr;
      currentDBImage.adresType = transactionImage.dbImage.adresType;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        dc = transactionImage.dbImage.dc;
        dcNr = transactionImage.dbImage.dcNr;
        adresType = transactionImage.dbImage.adresType;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String dc;
      private String dcNr;
      private int adresType;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
    public String getAdresRegel1() throws Exception {
		StringBuffer sb = new StringBuffer(getStraat());
		if (getHuisNr() != 0)
			sb.append(" " + getHuisNr());
		if (getHuisNrToev().trim().length() > 0)
			sb.append(getHuisNrToev());
		return sb.toString();
    }

    public String getAdresRegel2() throws Exception {
    	if (extraAdresRegel == null || extraAdresRegel.length() == 0) {
    		StringBuffer sb = new StringBuffer();
    		if (getPostcode().trim().length() > 0)
    			sb.append(getPostcode() + " ");
    		if (getPlaats().trim().length() > 0)
    			sb.append(getPlaats().trim());
    		return sb.toString();
    	} else {
    		return extraAdresRegel;
    	}
    }

    public String getAdresRegel3() throws Exception {
    	if (extraAdresRegel == null || extraAdresRegel.length() == 0) {
    		return "";
    	} else {
    		StringBuffer sb = new StringBuffer();
    		if (getPostcode().trim().length() > 0)
    			sb.append(getPostcode() + " ");
    		if (getPlaats().trim().length() > 0)
    			sb.append(getPlaats().trim());
//    		if (getLand().trim().length() > 0 && !getLand().trim().equals("NL")) {
//    			Locale l = new Locale("NL", getLand().trim());
//    			sb.append(" (" + l.getDisplayCountry().toUpperCase() + ")");
//    		}
    		return sb.toString();
    	}
    }

    public boolean isEmpty() throws Exception {
    	return (getAdresRegel1() + getAdresRegel2() + getAdresRegel3()).trim().length() == 0; 
    }
//{CODE-INSERT-END:955534258}
}
