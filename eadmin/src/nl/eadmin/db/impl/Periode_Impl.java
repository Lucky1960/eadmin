package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Periode_Impl  implements Periode, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataPeriode.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected int boekjaar;
    protected int boekperiode;
    protected java.sql.Date startdatum;
    protected java.sql.Date einddatum;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Periode_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Periode_Impl (DBData _dbd ,  String _bedrijf, int _boekjaar, int _boekperiode) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setBoekjaar(_boekjaar);
        setBoekperiode(_boekperiode);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Periode_Impl (DBData _dbd, PeriodeDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.PeriodeDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Periode with " + bean.getClass().getName() + "!! Use nl.eadmin.db.PeriodeDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setBoekjaar(bean.getBoekjaar());
        setBoekperiode(bean.getBoekperiode());
        setStartdatum(bean.getStartdatum());
        setEinddatum(bean.getEinddatum());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Periode_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      boekjaar = rs.getInt(pmd.getFieldName("Boekjaar",provider));
      boekperiode = rs.getInt(pmd.getFieldName("Boekperiode",provider));
      startdatum = rs.getDate(pmd.getFieldName("Startdatum",provider));
      einddatum = rs.getDate(pmd.getFieldName("Einddatum",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       PeriodePK key = new PeriodePK();
       key.setBedrijf(getBedrijf());
       key.setBoekjaar(getBoekjaar());
       key.setBoekperiode(getBoekperiode());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public int getBoekjaar()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-25361449}
//{CODE-INSERT-END:-25361449}
      return boekjaar;
      //Custom code
//{CODE-INSERT-BEGIN:-786207252}
//{CODE-INSERT-END:-786207252}
    }


    public int getBoekperiode()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-814355331}
//{CODE-INSERT-END:-814355331}
      return boekperiode;
      //Custom code
//{CODE-INSERT-BEGIN:524786182}
//{CODE-INSERT-END:524786182}
    }


    public Date getStartdatum()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-167023283}
//{CODE-INSERT-END:-167023283}

        Date obj = startdatum == null?null:new Date(startdatum.getTime());

      //Custom code
//{CODE-INSERT-BEGIN:-975219127}
//{CODE-INSERT-END:-975219127}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:-167024198}
//{CODE-INSERT-END:-167024198}
    }


    public Date getEinddatum()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1182041205}
//{CODE-INSERT-END:1182041205}

        Date obj = einddatum == null?null:new Date(einddatum.getTime());

      //Custom code
//{CODE-INSERT-BEGIN:1977793057}
//{CODE-INSERT-END:1977793057}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:1182040290}
//{CODE-INSERT-END:1182040290}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setBoekjaar(int _boekjaar ) throws Exception {

      if (_boekjaar == getBoekjaar())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1856804277}
//{CODE-INSERT-END:-1856804277}
      boekjaar =  _boekjaar;

      autoUpdate();

    }

    public void setBoekperiode(int _boekperiode ) throws Exception {

      if (_boekperiode == getBoekperiode())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1936884105}
//{CODE-INSERT-END:1936884105}
      boekperiode =  _boekperiode;

      autoUpdate();

    }

    public void setStartdatum(Date _startdatum ) throws Exception {

      if ((_startdatum == null ? getStartdatum() == null : _startdatum.equals(getStartdatum())))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-55185475}
//{CODE-INSERT-END:-55185475}
      startdatum =  _startdatum == null?null: new java.sql.Date( _startdatum.getTime());

      autoUpdate();

    }

    public void setEinddatum(Date _einddatum ) throws Exception {

      if (_einddatum == null)
          _einddatum =  java.sql.Date.valueOf("0001-01-01");
      if (_einddatum.equals(getEinddatum()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1037640237}
//{CODE-INSERT-END:1037640237}
      einddatum =  _einddatum == null?null: new java.sql.Date( _einddatum.getTime());

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      boekjaar = 0;
      boekperiode = 0;
      einddatum =  java.sql.Date.valueOf("0001-01-01");
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.boekjaar = boekjaar;
        currentDBImage.boekperiode = boekperiode;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Periode is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         PeriodePK key = new PeriodePK();
         key.setBedrijf(getBedrijf());
         key.setBoekjaar(getBoekjaar());
         key.setBoekperiode(getBoekperiode());
         ((nl.eadmin.db.impl.PeriodeManager_Impl)nl.eadmin.db.impl.PeriodeManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((PeriodeManager_Impl)PeriodeManager_Impl.getInstance(dbd)).getResultSet((PeriodePK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("626487144_"+ dbd.getDBId() + bedrijf + boekjaar + boekperiode).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getPeriodeDataBean()!
    */
    public PeriodeDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public PeriodeDataBean getPeriodeDataBean() throws Exception {
      return getDataBean(null);
    }

    private PeriodeDataBean getDataBean(PeriodeDataBean bean) throws Exception {

      if (bean == null)
          bean = new PeriodeDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setBoekjaar(getBoekjaar());
      bean.setBoekperiode(getBoekperiode());
      bean.setStartdatum(getStartdatum());
      bean.setEinddatum(getEinddatum());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Periode_Impl))
             return false;
          nl.eadmin.db.impl.Periode_Impl bo = (nl.eadmin.db.impl.Periode_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getBoekjaar() != this.getBoekjaar())
              return false;
          if (bo.getBoekperiode() != this.getBoekperiode())
              return false;
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(PeriodeDataBean bean) throws Exception {
       set(bean);
    }


    public void set(PeriodeDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setBoekjaar(bean.getBoekjaar());
           setBoekperiode(bean.getBoekperiode());
           setStartdatum(bean.getStartdatum());
           setEinddatum(bean.getEinddatum());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Periode is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Periode.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("Boekjaar",provider) +  " = ? " + ", " + pmd.getFieldName("Boekperiode",provider) +  " = ? " + ", " + pmd.getFieldName("Startdatum",provider) +  " = ? " + ", " + pmd.getFieldName("Einddatum",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("Boekjaar",provider) + " = ? " + " AND  " +  pmd.getFieldName("Boekperiode",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( boekjaar),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  new Integer( boekperiode),  mapping.getJDBCTypeFor("int"));
        if ( startdatum == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("Date"));
       } else {
          prep.setObject(parmIndex++,  startdatum,  mapping.getJDBCTypeFor("Date"));
}
        prep.setObject(parmIndex++,  einddatum,  mapping.getJDBCTypeFor("Date"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer(image.boekjaar),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  new Integer(image.boekperiode),  mapping.getJDBCTypeFor("int"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Periode modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Periode.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Periode.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("Boekjaar",provider) + ", " + pmd.getFieldName("Boekperiode",provider) + ", " + pmd.getFieldName("Startdatum",provider) + ", " + pmd.getFieldName("Einddatum",provider)+ 
                                           ") VALUES (?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3,  new Integer(boekjaar),  mapping.getJDBCTypeFor("int"));
        prep.setObject(4,  new Integer(boekperiode),  mapping.getJDBCTypeFor("int"));
        if (startdatum == null)
          prep.setNull(5,  mapping.getJDBCTypeFor("Date"));
        else
          prep.setObject(5, startdatum,  mapping.getJDBCTypeFor("Date"));
        prep.setObject(6, einddatum,  mapping.getJDBCTypeFor("Date"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.boekjaar = currentDBImage.boekjaar;
        transactionImage.dbImage.boekperiode = currentDBImage.boekperiode;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.boekjaar = transactionImage.dbImage.boekjaar;
      currentDBImage.boekperiode = transactionImage.dbImage.boekperiode;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        boekjaar = transactionImage.dbImage.boekjaar;
        boekperiode = transactionImage.dbImage.boekperiode;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private int boekjaar;
      private int boekperiode;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
