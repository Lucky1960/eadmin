package nl.eadmin.db.impl;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import nl.eadmin.db.BtwCategorie;
import nl.eadmin.db.BtwCategorieManager;
import nl.eadmin.db.BtwCategorieManagerFactory;
import nl.eadmin.db.HoofdverdichtingManagerFactory;
import nl.eadmin.db.HoofdverdichtingPK;
import nl.eadmin.db.Rekening;
import nl.eadmin.db.RekeningDataBean;
import nl.eadmin.db.RekeningPK;
import nl.eadmin.db.VerdichtingManagerFactory;
import nl.eadmin.db.VerdichtingPK;
import nl.eadmin.enums.RekeningSoortEnum;
import nl.ibs.jeelog.Log;
import nl.ibs.jsql.AtomicDBTransaction;
import nl.ibs.jsql.BusinessObject_Impl;
import nl.ibs.jsql.DBConfig;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.DBPersistenceManager;
import nl.ibs.jsql.DBTransactionListener;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.jsql.exception.CreateException;
import nl.ibs.jsql.exception.DeletedException;
import nl.ibs.jsql.exception.FinderException;
import nl.ibs.jsql.exception.InvalidStateException;
import nl.ibs.jsql.exception.JSQLException;
import nl.ibs.jsql.exception.UpdateException;
import nl.ibs.jsql.impl.ArrayListImpl;
import nl.ibs.jsql.sql.AttributeAccessor;
import nl.ibs.jsql.sql.ConnectionProvider;
import nl.ibs.jsql.sql.DBConnection;
import nl.ibs.jsql.sql.DBMapping;
import nl.ibs.jsql.sql.PersistenceMetaData;

import org.w3c.dom.DOMImplementation;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Rekening_Impl  implements Rekening, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataRekening.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String rekeningNr;
    protected String omschrijving;
    protected String omschrijvingKort;
    protected int verdichting;
    protected String iCPSoort;
    protected String btwCode;
    protected String btwSoort;
    protected boolean btwRekening;
    protected String subAdministratieType;
    protected String toonKolom;
    protected boolean blocked;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Rekening_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Rekening_Impl (DBData _dbd ,  String _bedrijf, String _rekeningNr) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setRekeningNr(_rekeningNr);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Rekening_Impl (DBData _dbd, RekeningDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.RekeningDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Rekening with " + bean.getClass().getName() + "!! Use nl.eadmin.db.RekeningDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setRekeningNr(bean.getRekeningNr());
        setOmschrijving(bean.getOmschrijving());
        setOmschrijvingKort(bean.getOmschrijvingKort());
        setVerdichting(bean.getVerdichting());
        setICPSoort(bean.getICPSoort());
        setBtwCode(bean.getBtwCode());
        setBtwSoort(bean.getBtwSoort());
        setBtwRekening(bean.getBtwRekening());
        setSubAdministratieType(bean.getSubAdministratieType());
        setToonKolom(bean.getToonKolom());
        setBlocked(bean.getBlocked());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Rekening_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      rekeningNr = rs.getString(pmd.getFieldName("RekeningNr",provider));
      omschrijving = rs.getString(pmd.getFieldName("Omschrijving",provider));
      omschrijvingKort = rs.getString(pmd.getFieldName("OmschrijvingKort",provider));
      verdichting = rs.getInt(pmd.getFieldName("Verdichting",provider));
      iCPSoort = rs.getString(pmd.getFieldName("ICPSoort",provider));
      btwCode = rs.getString(pmd.getFieldName("BtwCode",provider));
      btwSoort = rs.getString(pmd.getFieldName("BtwSoort",provider));
      btwRekening = rs.getBoolean(pmd.getFieldName("BtwRekening",provider));
      subAdministratieType = rs.getString(pmd.getFieldName("SubAdministratieType",provider));
      toonKolom = rs.getString(pmd.getFieldName("ToonKolom",provider));
      blocked = rs.getBoolean(pmd.getFieldName("Blocked",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       RekeningPK key = new RekeningPK();
       key.setBedrijf(getBedrijf());
       key.setRekeningNr(getRekeningNr());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getRekeningNr()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:666684321}
//{CODE-INSERT-END:666684321}
      return trim(rekeningNr);
      //Custom code
//{CODE-INSERT-BEGIN:-807624862}
//{CODE-INSERT-END:-807624862}
    }


    public String getOmschrijving()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:86851457}
//{CODE-INSERT-END:86851457}
      return trim(omschrijving);
      //Custom code
//{CODE-INSERT-BEGIN:-1602574462}
//{CODE-INSERT-END:-1602574462}
    }


    public String getOmschrijvingKort()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-737010489}
//{CODE-INSERT-END:-737010489}
      return trim(omschrijvingKort);
      //Custom code
//{CODE-INSERT-BEGIN:-1372491012}
//{CODE-INSERT-END:-1372491012}
    }


    public int getVerdichting()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1840266247}
//{CODE-INSERT-END:1840266247}
      return verdichting;
      //Custom code
//{CODE-INSERT-BEGIN:1213676476}
//{CODE-INSERT-END:1213676476}
    }


    public String getICPSoort()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1246441445}
//{CODE-INSERT-END:-1246441445}
      return trim(iCPSoort);
      //Custom code
//{CODE-INSERT-BEGIN:15018536}
//{CODE-INSERT-END:15018536}
    }


    public String getBtwCode()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:635008062}
//{CODE-INSERT-END:635008062}
      return trim(btwCode);
      //Custom code
//{CODE-INSERT-BEGIN:-1789588891}
//{CODE-INSERT-END:-1789588891}
    }


    public String getBtwSoort()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:919584044}
//{CODE-INSERT-END:919584044}
      return trim(btwSoort);
      //Custom code
//{CODE-INSERT-BEGIN:-1557668041}
//{CODE-INSERT-END:-1557668041}
    }


    public boolean getBtwRekening()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-448748846}
//{CODE-INSERT-END:-448748846}
      return btwRekening;
      //Custom code
//{CODE-INSERT-BEGIN:-1026314671}
//{CODE-INSERT-END:-1026314671}
    }


    public String getSubAdministratieType()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1659498116}
//{CODE-INSERT-END:-1659498116}
      return trim(subAdministratieType);
      //Custom code
//{CODE-INSERT-BEGIN:95163623}
//{CODE-INSERT-END:95163623}
    }


    public String getToonKolom()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1068484920}
//{CODE-INSERT-END:1068484920}
      return toonKolom;
      //Custom code
//{CODE-INSERT-BEGIN:-1236708181}
//{CODE-INSERT-END:-1236708181}
    }


    public boolean getBlocked()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:194896696}
//{CODE-INSERT-END:194896696}
      return blocked;
      //Custom code
//{CODE-INSERT-BEGIN:1746827947}
//{CODE-INSERT-END:1746827947}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setRekeningNr(String _rekeningNr ) throws Exception {

      if (_rekeningNr !=  null)
         _rekeningNr = _rekeningNr.trim();
      if (_rekeningNr !=  null)
         _rekeningNr = _rekeningNr.toUpperCase();
      if (_rekeningNr == null)
          _rekeningNr =  "";
      if (_rekeningNr.equals(getRekeningNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1586717973}
//{CODE-INSERT-END:1586717973}
      rekeningNr =  _rekeningNr;

      autoUpdate();

    }

    public void setOmschrijving(String _omschrijving ) throws Exception {

      if (_omschrijving !=  null)
         _omschrijving = _omschrijving.trim();
      if (_omschrijving == null)
          _omschrijving =  "";
      if (_omschrijving.equals(getOmschrijving()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-524071947}
//{CODE-INSERT-END:-524071947}
      omschrijving =  _omschrijving;

      autoUpdate();

    }

    public void setOmschrijvingKort(String _omschrijvingKort ) throws Exception {

      if (_omschrijvingKort !=  null)
         _omschrijvingKort = _omschrijvingKort.trim();
      if (_omschrijvingKort == null)
          _omschrijvingKort =  "";
      if (_omschrijvingKort.equals(getOmschrijvingKort()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1541091525}
//{CODE-INSERT-END:-1541091525}
      omschrijvingKort =  _omschrijvingKort;

      autoUpdate();

    }

    public void setVerdichting(int _verdichting ) throws Exception {

      if (_verdichting == getVerdichting())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:296538387}
//{CODE-INSERT-END:296538387}
      verdichting =  _verdichting;

      autoUpdate();

    }

    public void setICPSoort(String _iCPSoort ) throws Exception {

      if (_iCPSoort !=  null)
         _iCPSoort = _iCPSoort.trim();
      if (_iCPSoort == null)
          _iCPSoort =  "";
      if (_iCPSoort.equals(getICPSoort()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1217083023}
//{CODE-INSERT-END:1217083023}
      iCPSoort =  _iCPSoort;

      autoUpdate();

    }

    public void setBtwCode(String _btwCode ) throws Exception {

      if (_btwCode !=  null)
         _btwCode = _btwCode.trim();
      if (_btwCode !=  null)
         _btwCode = _btwCode.toUpperCase();
      if (_btwCode == null)
          _btwCode =  "";
      if (_btwCode.equals(getBtwCode()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1545760586}
//{CODE-INSERT-END:1545760586}
      btwCode =  _btwCode;

      autoUpdate();

    }

    public void setBtwSoort(String _btwSoort ) throws Exception {

      if (_btwSoort !=  null)
         _btwSoort = _btwSoort.trim();
      if (_btwSoort !=  null)
         _btwSoort = _btwSoort.toUpperCase();
      if (_btwSoort == null)
          _btwSoort =  "I";
      if (_btwSoort.equals(getBtwSoort()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-911858784}
//{CODE-INSERT-END:-911858784}
      btwSoort =  _btwSoort;

      autoUpdate();

    }

    public void setBtwRekening(boolean _btwRekening ) throws Exception {

      if (_btwRekening == getBtwRekening())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1992476706}
//{CODE-INSERT-END:-1992476706}
      btwRekening =  _btwRekening;

      autoUpdate();

    }

    public void setSubAdministratieType(String _subAdministratieType ) throws Exception {

      if (_subAdministratieType !=  null)
         _subAdministratieType = _subAdministratieType.trim();
      if (_subAdministratieType == null)
          _subAdministratieType =  "";
      if (_subAdministratieType.equals(getSubAdministratieType()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-421369360}
//{CODE-INSERT-END:-421369360}
      subAdministratieType =  _subAdministratieType;

      autoUpdate();

    }

    public void setToonKolom(String _toonKolom ) throws Exception {

      if (_toonKolom !=  null)
         _toonKolom = _toonKolom.toUpperCase();
      if (_toonKolom == null)
          _toonKolom =  "S";
      if (_toonKolom.equals(getToonKolom()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:128332100}
//{CODE-INSERT-END:128332100}
      toonKolom =  _toonKolom;

      autoUpdate();

    }

    public void setBlocked(boolean _blocked ) throws Exception {

      if (_blocked == getBlocked())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1105649220}
//{CODE-INSERT-END:1105649220}
      blocked =  _blocked;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      rekeningNr =  "";
      omschrijving =  "";
      omschrijvingKort =  "";
      verdichting = 0;
      iCPSoort =  "";
      btwCode =  "";
      btwSoort =  "I";
      btwRekening =  false;
      subAdministratieType =  "";
      toonKolom =  "S";
      blocked =  false;
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.rekeningNr = rekeningNr;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Rekening is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         RekeningPK key = new RekeningPK();
         key.setBedrijf(getBedrijf());
         key.setRekeningNr(getRekeningNr());
         ((nl.eadmin.db.impl.RekeningManager_Impl)nl.eadmin.db.impl.RekeningManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((RekeningManager_Impl)RekeningManager_Impl.getInstance(dbd)).getResultSet((RekeningPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("1227762909_"+ dbd.getDBId() + bedrijf + rekeningNr).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getRekeningDataBean()!
    */
    public RekeningDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public RekeningDataBean getRekeningDataBean() throws Exception {
      return getDataBean(null);
    }

    private RekeningDataBean getDataBean(RekeningDataBean bean) throws Exception {

      if (bean == null)
          bean = new RekeningDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setRekeningNr(getRekeningNr());
      bean.setOmschrijving(getOmschrijving());
      bean.setOmschrijvingKort(getOmschrijvingKort());
      bean.setVerdichting(getVerdichting());
      bean.setICPSoort(getICPSoort());
      bean.setBtwCode(getBtwCode());
      bean.setBtwSoort(getBtwSoort());
      bean.setBtwRekening(getBtwRekening());
      bean.setSubAdministratieType(getSubAdministratieType());
      bean.setToonKolom(getToonKolom());
      bean.setBlocked(getBlocked());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Rekening_Impl))
             return false;
          nl.eadmin.db.impl.Rekening_Impl bo = (nl.eadmin.db.impl.Rekening_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getRekeningNr() == null){
              if (this.getRekeningNr() != null)
                  return false;
          }else if (!bo.getRekeningNr().equals(this.getRekeningNr())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(RekeningDataBean bean) throws Exception {
       set(bean);
    }


    public void set(RekeningDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setRekeningNr(bean.getRekeningNr());
           setOmschrijving(bean.getOmschrijving());
           setOmschrijvingKort(bean.getOmschrijvingKort());
           setVerdichting(bean.getVerdichting());
           setICPSoort(bean.getICPSoort());
           setBtwCode(bean.getBtwCode());
           setBtwSoort(bean.getBtwSoort());
           setBtwRekening(bean.getBtwRekening());
           setSubAdministratieType(bean.getSubAdministratieType());
           setToonKolom(bean.getToonKolom());
           setBlocked(bean.getBlocked());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Rekening is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Rekening.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("RekeningNr",provider) +  " = ? " + ", " + pmd.getFieldName("Omschrijving",provider) +  " = ? " + ", " + pmd.getFieldName("OmschrijvingKort",provider) +  " = ? " + ", " + pmd.getFieldName("Verdichting",provider) +  " = ? " + ", " + pmd.getFieldName("ICPSoort",provider) +  " = ? " + ", " + pmd.getFieldName("BtwCode",provider) +  " = ? " + ", " + pmd.getFieldName("BtwSoort",provider) +  " = ? " + ", " + pmd.getFieldName("BtwRekening",provider) +  " = ? " + ", " + pmd.getFieldName("SubAdministratieType",provider) +  " = ? " + ", " + pmd.getFieldName("ToonKolom",provider) +  " = ? " + ", " + pmd.getFieldName("Blocked",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("RekeningNr",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  rekeningNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschrijvingKort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( verdichting),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  iCPSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  btwCode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  btwSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Boolean( btwRekening),  mapping.getJDBCTypeFor("boolean"));
        prep.setObject(parmIndex++,  subAdministratieType,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  toonKolom,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Boolean( blocked),  mapping.getJDBCTypeFor("boolean"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.rekeningNr,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Rekening modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Rekening.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Rekening.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("RekeningNr",provider) + ", " + pmd.getFieldName("Omschrijving",provider) + ", " + pmd.getFieldName("OmschrijvingKort",provider) + ", " + pmd.getFieldName("Verdichting",provider) + ", " + pmd.getFieldName("ICPSoort",provider) + ", " + pmd.getFieldName("BtwCode",provider) + ", " + pmd.getFieldName("BtwSoort",provider) + ", " + pmd.getFieldName("BtwRekening",provider) + ", " + pmd.getFieldName("SubAdministratieType",provider) + ", " + pmd.getFieldName("ToonKolom",provider) + ", " + pmd.getFieldName("Blocked",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, rekeningNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5, omschrijvingKort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6,  new Integer(verdichting),  mapping.getJDBCTypeFor("int"));
        prep.setObject(7, iCPSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(8, btwCode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(9, btwSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(10,  new Boolean(btwRekening),  mapping.getJDBCTypeFor("boolean"));
        prep.setObject(11, subAdministratieType,  mapping.getJDBCTypeFor("String"));
        prep.setObject(12, toonKolom,  mapping.getJDBCTypeFor("String"));
        prep.setObject(13,  new Boolean(blocked),  mapping.getJDBCTypeFor("boolean"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.rekeningNr = currentDBImage.rekeningNr;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.rekeningNr = transactionImage.dbImage.rekeningNr;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        rekeningNr = transactionImage.dbImage.rekeningNr;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String rekeningNr;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
    public String getRekeningSoort() throws Exception {
    	try {
			VerdichtingPK vrdPK = new VerdichtingPK();
			vrdPK.setBedrijf(bedrijf);
			vrdPK.setId(verdichting);
			HoofdverdichtingPK hfvPK = new HoofdverdichtingPK();
			hfvPK.setBedrijf(bedrijf);
			hfvPK.setId(VerdichtingManagerFactory.getInstance(dbd).findByPrimaryKey(vrdPK).getHoofdverdichting());
	    	return HoofdverdichtingManagerFactory.getInstance(dbd).findByPrimaryKey(hfvPK).getRekeningsoort();
		} catch (Exception e) {
	    	return RekeningSoortEnum.BALANS;
		}
    }
    public BtwCategorie getBtwCategorie() throws Exception {
		StringBuilder filter = new StringBuilder();
		filter.append("SELECT * FROM BtwCategorie");
		filter.append(" WHERE " + BtwCategorie.BEDRIJF + "='" + bedrijf + "'");
		filter.append("   AND " + BtwCategorie.REKENING_NR + "='" + rekeningNr + "'");
		FreeQuery qry = QueryFactory.createFreeQuery((BtwCategorieManager_Impl) BtwCategorieManagerFactory.getInstance(dbd), filter.toString());
		return (BtwCategorie)qry.getFirstObject();
    }

//{CODE-INSERT-END:955534258}
}
