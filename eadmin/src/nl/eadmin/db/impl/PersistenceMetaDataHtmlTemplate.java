package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataHtmlTemplate implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataHtmlTemplate();

      persistables.add("nl.eadmin.db.impl.HtmlTemplate_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("id");
      javaVariableNames.add("omschrijving");
      javaVariableNames.add("isDefault");
      javaVariableNames.add("logoHeight");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("id");

      fieldNames.put("mutationTimeStamp","HTMLTMPLT_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("Id","ID");
      fieldNames.put("Omschrijving","OMSCHR");
      fieldNames.put("IsDefault","DEFLT");
      fieldNames.put("Logo","LOGO");
      fieldNames.put("LogoHeight","LOGOHGT");
      fieldNames.put("HtmlString","HTMLSTR");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("Id","Template Id");
      fieldDescriptions.put("Omschrijving","Omschrijving");
      fieldDescriptions.put("IsDefault","Is default template");
      fieldDescriptions.put("Logo","Logo op factuur");
      fieldDescriptions.put("LogoHeight","Logo hoogte");
      fieldDescriptions.put("HtmlString","Html code");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("id","Id");
      javaToBONameMapping.put("omschrijving","Omschrijving");
      javaToBONameMapping.put("isDefault","IsDefault");
      javaToBONameMapping.put("logo","Logo");
      javaToBONameMapping.put("logoHeight","LogoHeight");
      javaToBONameMapping.put("htmlString","HtmlString");

    }

    private PersistenceMetaDataHtmlTemplate(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "HtmlTemplate";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("HtmlTemplate" , "HTMLTMPLT");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "HtmlTemplate" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Html templates";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Id", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschrijving", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("IsDefault", provider)	 + " " + provider.getDBMapping().getSQLDefinition("boolean", 0, 0) + " DEFAULT 'false'" + " NOT NULL	," + 
				getFieldName("Logo", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	," + 
				getFieldName("LogoHeight", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 3, 0) + " DEFAULT 60" + " NOT NULL	," + 
				getFieldName("HtmlString", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[8][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("Id", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Template Id"};
      columnDefinitions[3]=new String[]{getFieldName("Omschrijving", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving"};
      columnDefinitions[4]=new String[]{getFieldName("IsDefault", provider), provider.getDBMapping().getSQLDefinition("boolean", 0, 0) , "'false'"  , " NOT NULL", "Is default template"};
      columnDefinitions[5]=new String[]{getFieldName("Logo", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Logo op factuur"};
      columnDefinitions[6]=new String[]{getFieldName("LogoHeight", provider), provider.getDBMapping().getSQLDefinition("int", 3, 0) , "60"  , " NOT NULL", "Logo hoogte"};
      columnDefinitions[7]=new String[]{getFieldName("HtmlString", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Html code"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_HT_UPDTS_N1647553201", " INDEX  " + provider.getPrefix() + "JSQL_INDX_HT_UPDTS_N1647553201" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Id", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_HTMLTEMPLATE", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_HTMLTEMPLATE PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Id", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
