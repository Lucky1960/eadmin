package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataBedrijf implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataBedrijf();

      persistables.add("nl.eadmin.db.impl.Bedrijf_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijfscode");
      javaVariableNames.add("omschrijving");
      javaVariableNames.add("email");
      javaVariableNames.add("kvkNr");
      javaVariableNames.add("btwNr");
      javaVariableNames.add("ibanNr");
      javaVariableNames.add("bic");
      javaPKVariableNames.add("bedrijfscode");

      fieldNames.put("mutationTimeStamp","BEDRIJF_RCRDMTTM");
      fieldNames.put("Bedrijfscode","BDR");
      fieldNames.put("Omschrijving","OMSCHR");
      fieldNames.put("Email","EMAIL");
      fieldNames.put("KvkNr","KVKNR");
      fieldNames.put("BtwNr","BTWNR");
      fieldNames.put("IbanNr","IBANNR");
      fieldNames.put("Bic","BIC");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijfscode","Bedrijfscode");
      fieldDescriptions.put("Omschrijving","Omschrijving");
      fieldDescriptions.put("Email","Emailadres");
      fieldDescriptions.put("KvkNr","KvK nummer");
      fieldDescriptions.put("BtwNr","BTW nummer");
      fieldDescriptions.put("IbanNr","Iban nummer");
      fieldDescriptions.put("Bic","BIC code");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijfscode","Bedrijfscode");
      javaToBONameMapping.put("omschrijving","Omschrijving");
      javaToBONameMapping.put("email","Email");
      javaToBONameMapping.put("kvkNr","KvkNr");
      javaToBONameMapping.put("btwNr","BtwNr");
      javaToBONameMapping.put("ibanNr","IbanNr");
      javaToBONameMapping.put("bic","Bic");

    }

    private PersistenceMetaDataBedrijf(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Bedrijf";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Bedrijf" , "BEDRIJF");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Bedrijf" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Bedrijfsgegevens";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijfscode", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschrijving", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Email", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("KvkNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BtwNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 18, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("IbanNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 18, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Bic", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[8][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijfscode", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("Omschrijving", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving"};
      columnDefinitions[3]=new String[]{getFieldName("Email", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Emailadres"};
      columnDefinitions[4]=new String[]{getFieldName("KvkNr", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "KvK nummer"};
      columnDefinitions[5]=new String[]{getFieldName("BtwNr", provider), provider.getDBMapping().getSQLDefinition("String", 18, 0) , "''"  , " NOT NULL", "BTW nummer"};
      columnDefinitions[6]=new String[]{getFieldName("IbanNr", provider), provider.getDBMapping().getSQLDefinition("String", 18, 0) , "''"  , " NOT NULL", "Iban nummer"};
      columnDefinitions[7]=new String[]{getFieldName("Bic", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "BIC code"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_BEDRIJF_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_BEDRIJF_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijfscode", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_BEDRIJF", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_BEDRIJF PRIMARY KEY(" +  getFieldName("Bedrijfscode", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
