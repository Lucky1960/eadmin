package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import nl.eadmin.db.*;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


  public class AdresManager_Impl extends BusinessObjectManager implements AdresManager{


    

    private final static String APPLICATION ="eadmin";
    private static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    private static AttributeAccessor generalAccessor = new AttributeAccessor();
    private static boolean verbose = Log.debug();
    private static PersistenceMetaData pmd = PersistenceMetaDataAdres.getInstance();
    private static Hashtable instances = new Hashtable();

    private DBData  dbd;

    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    private AdresManager_Impl(DBData dbData){
       dbd = dbData;
    }


    public static AdresManager getInstance(){
		return getInstance(null);
    }


    public static AdresManager getInstance(DBData dbd){
		AdresManager_Impl inst = (AdresManager_Impl)instances.get(dbd==null?"":dbd);
		if (inst == null){
		  inst = new AdresManager_Impl(dbd);
		  instances.put(dbd==null?"":dbd,inst);
		}
		return inst;
    }


    public DBData getDBData(){
       return dbd==null?DBData.getDefaultDBData(APPLICATION):dbd;
    }


    public Adres create(AdresDataBean bean)  throws Exception {
      Adres_Impl impl = new Adres_Impl(getDBData(),  bean);
        impl.assureStorage();
      return (Adres)impl;
    }


    public Adres create(String _bedrijf, String _dc, String _dcNr, int _adresType) throws Exception {
      Adres_Impl obj = new Adres_Impl(getDBData(), _bedrijf, _dc, _dcNr, _adresType);
      obj.assureStorage();
      return obj;
    }


    private AdresPK getPK(ResultSet rs) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      AdresPK inst =  new AdresPK();
      inst.setBedrijf(rs.getString(pmd.getFieldName("Bedrijf",provider)));
      inst.setDc(rs.getString(pmd.getFieldName("Dc",provider)));
      inst.setDcNr(rs.getString(pmd.getFieldName("DcNr",provider)));
      inst.setAdresType(rs.getInt(pmd.getFieldName("AdresType",provider)));
      return inst; 
    }


    public Adres findOrCreate (AdresPK key) throws Exception {

      Adres obj;
      try{
         obj = findByPrimaryKey( key);
      }catch (FinderException e){
         obj = create( key.getBedrijf (),  key.getDc (),  key.getDcNr (),  key.getAdresType ());
      }

      return obj;
    }


    public void add(AdresDataBean inst)  throws Exception {
        Adres_Impl impl = new Adres_Impl(getDBData(),  inst);
        impl.assureStorage();
    }


    public void removeByPrimaryKey (AdresPK key) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      Adres cachedObject = (Adres)getCachedObjectByPrimaryKey(key);
      if (provider instanceof AtomicDBTransaction)
        dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate((BusinessObject_Impl)cachedObject);
      else
        dbc = provider.getConnection();
      String  pskey = "nl.eadmin.db.Adres.removeByPrimaryKey";
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider) + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("Dc",provider) + "=?" + " AND "  + pmd.getFieldName("DcNr",provider) + "=?" + " AND "  + pmd.getFieldName("AdresType",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDc());
          prep.setString(3, key.getDcNr());
          prep.setInt(4, key.getAdresType());
          int x = prep.executeUpdate();

          if (x == 0) throw new RemoveException("Key not found");
          if (cachedObject != null){
          		DBPersistenceManager.removeFromCache(cachedObject);
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
    }


    public Adres findByPrimaryKey (AdresPK key) throws Exception {
      //Custom code
//{CODE-INSERT-BEGIN:-1586979883}
//{CODE-INSERT-END:-1586979883}

      Adres adres = getCachedObjectByPrimaryKey(key);
       if (adres != null)
      	  return adres;
       ResultSet rs = null;
       try{
           rs = getResultSet(key);
           return (Adres)getBusinessObject(rs);
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }      //Custom code
//{CODE-INSERT-BEGIN:-933211338}
//{CODE-INSERT-END:-933211338}

    }

    public ResultSet getResultSet(AdresPK key) throws Exception{
      ResultSet rs = null;
      String  pskey = "nl.eadmin.db.Adres.findByPrimaryKey1";
      Object inst = null;
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " SELECT * " + 
                                           " FROM " + provider.getPrefix() + pmd.getTableName(provider)  + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("Dc",provider) + "=?" + " AND "  + pmd.getFieldName("DcNr",provider) + "=?" + " AND "  + pmd.getFieldName("AdresType",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDc());
          prep.setString(3, key.getDcNr());
          prep.setInt(4, key.getAdresType());
          prep.executeQuery();

          rs = prep.getResultSet();

          if (!rs.next()){
                throw new FinderException("Key not found");
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
      return rs;
    }
    public Adres getCachedObjectByPrimaryKey (AdresPK key) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache == null)
          return null;
       Object o = cache.get(new String("1425873729_"+ getDBData().getDBId() + key.getBedrijf() + key.getDc() + key.getDcNr() + key.getAdresType()));
       return (Adres)o;
    }



    public ConnectionProvider getConnectionProvider() throws Exception{
       return DBPersistenceManager.getConnectionProvider(getDBData());
    }


    public PersistenceMetaData getPersistenceMetaData(){
      return pmd;
    }


    public int generalUpdate(String setClause, String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String update = "UPDATE " + provider.getPrefix() + pmd.getTableName(provider)  + " SET " + QueryTranslator.translateObjectQuery(setClause + " WHERE " + whereClause,pmd,provider);
        return dbc.executeUpdate(update);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public int generalDelete(String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String delete = "DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider)  + " WHERE " + QueryTranslator.translateObjectQuery(whereClause,pmd,provider);
        return dbc.executeUpdate(delete);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public Adres getFirstObject(Query query) throws Exception{
      if (query == null)
         query = QueryFactory.create();
      int prevMax = query.getMaxObjects();
      query.setMaxObjects(1);
      try{
         return (Adres) ((QueryImplementation)query).execute(this, RETURN_FIRST_OBJECT);
      }catch(Exception e){
         throw e;
      }finally{
         query.setMaxObjects(prevMax);
      }
    }

    public Collection getCollection(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Collection) queryImpl.execute(this, RETURN_COLLECTION);
    }

    public ListIterator getListIterator(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ListIterator) queryImpl.execute(this, RETURN_LISTITERATOR);
    }

    public Document getDocument(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Document) queryImpl.execute(this, RETURN_DOCUMENT);
    }

    public DocumentFragment getDocumentFragment(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (DocumentFragment) queryImpl.execute(this, RETURN_DOCUMENT_FRAGMENT);
    }

    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ArrayList) queryImpl.execute(this,RETURN_DOCUMENT_FRAGMENT_ARRAYLIST );
    }



    protected Collection getCollection(ResultSet rs) throws Exception {
      ArrayList result = new ArrayListImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      return (Collection)result;
    }



    protected Document getDocument(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      Element root = document.getDocumentElement();
      collectResultElements(resultSet, root, getConnectionProvider());
      return document;
    };


    protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      DocumentFragment documentFragment = document.createDocumentFragment();
      collectResultElements(resultSet, documentFragment, getConnectionProvider());
      return documentFragment;
    };


    protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      Document document = domImplementation.createDocument("", "data", null);
      ArrayList arrayList = new ArrayListImpl();
      while (resultSet.next()){
        DocumentFragment documentFragment = document.createDocumentFragment();
        Node rec = documentFragment.appendChild(document.createElement("adres"));
        addBOFieldsToElement(resultSet, rec, provider);
        arrayList.add(documentFragment);
      }
      return arrayList;
    }


    protected ListIterator getListIterator(ResultSet rs) throws Exception {
      ListIterator result = new ListIteratorImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      ((ListIteratorImpl)result).pointer = 0; 
      return result; 
    }


    protected Object getBusinessObject(ResultSet rs) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache != null){
          ConnectionProvider provider = getConnectionProvider();
          Object o = cache.get(new String("1425873729_"+ getDBData().getDBId()+rs.getObject(pmd.getFieldName("Bedrijf",provider))+rs.getObject(pmd.getFieldName("Dc",provider))+rs.getObject(pmd.getFieldName("DcNr",provider))+rs.getObject(pmd.getFieldName("AdresType",provider))));
          if (o != null)
             return o;
       }
    	  return DBPersistenceManager.cache(createBusinessObject(rs));
    }

    private Object createBusinessObject(ResultSet rs) throws Exception {
      return new Adres_Impl(getDBData(), rs);
    }


    private void collectResultElements(ResultSet rs, Node root, ConnectionProvider provider) throws Exception {
      Document doc = root.getOwnerDocument();
      while (rs.next()){
        Node rec = root.appendChild(doc.createElement("adres"));
        addBOFieldsToElement(rs,rec,provider);
      }
    }


    public static void addBOFieldsToElement(ResultSet rs, Node rec, ConnectionProvider provider) throws Exception {
      DBMapping mapping = provider.getDBMapping();
      Document doc = rec.getOwnerDocument();
      Object o = null;
      String s = null;
      if ((o=rs.getObject(pmd.getFieldName("mutationTimeStamp",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("mutationtimestamp")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Bedrijf",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrijf")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Dc",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dc")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DcNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dcnr")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("AdresType",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("adrestype")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Straat",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("straat")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("HuisNr",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("huisnr")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("HuisNrToev",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("huisnrtoev")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("ExtraAdresRegel",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("extraadresregel")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Postcode",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("postcode")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Plaats",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("plaats")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Land",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("land")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Telefoon",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("telefoon")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Mobiel",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("mobiel")).appendChild(doc.createCDATASection(s));
    }

    public nl.ibs.vegas.ExecutableQuery getExecutableQuery() {
    		return QueryFactory.create(this);
    }

    final static String[] keys = new String[]{ "bedrijf","dc","dcNr","adresType"};
    public String[] getKeyNames() {
    	return keys;
    }
    final static nl.ibs.vegas.meta.AttributeMeta[] attributes = new nl.ibs.vegas.meta.AttributeMeta[]{
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrijf","bedrijf","bedrijf-short","bedrijf-description","text",String.class,10,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dc","dc","dc-short","dc-description","text",String.class,1,0,null,"false","false","true","upper", false, true,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dcNr","dc-nr","dc-nr-short","dc-nr-description","text",String.class,10,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("adresType","adres-type","adres-type-short","adres-type-description","number",int.class,1,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("straat","straat","straat-short","straat-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("huisNr","huis-nr","huis-nr-short","huis-nr-description","number",int.class,5,0,"0","false","false","false","", true, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("huisNrToev","huis-nr-toev","huis-nr-toev-short","huis-nr-toev-description","text",String.class,5,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("extraAdresRegel","extra-adres-regel","extra-adres-regel-short","extra-adres-regel-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("postcode","postcode","postcode-short","postcode-description","text",String.class,8,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("plaats","plaats","plaats-short","plaats-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("land","land","land-short","land-description","text",String.class,3,0,"NL","false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("telefoon","telefoon","telefoon-short","telefoon-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("mobiel","mobiel","mobiel-short","mobiel-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null)
    };
    public nl.ibs.vegas.meta.AttributeMeta[] getAttributes() {
    	return attributes;
    }
    public Class getClassType() {
    	return nl.eadmin.db.Adres.class;
    }
    public String getNameField() {
    	return "adresType";
    }
    public String getDescriptionField() {
    	return "adresType";
    }
    public String getTextKey() {
    	return "adres";
    }
    public String getTextKeyShort() {
    	return "adres-short";
    }
    public String getTextKeyDescription() {
    	return "adres-description";
    }
    public nl.ibs.vegas.meta.AttributeMeta getAttribute(String name) {
    	if (attributes == null || name == null)
    		return null;
    	for (int i = 0; i < attributes.length; i++)
    		if (name.equals(attributes[i].getName()))
    			return attributes[i];
    	return null;
    }
  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
