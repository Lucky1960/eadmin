package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataDagboek implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataDagboek();

      persistables.add("nl.eadmin.db.impl.Dagboek_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("id");
      javaVariableNames.add("soort");
      javaVariableNames.add("omschrijving");
      javaVariableNames.add("boekstukMask");
      javaVariableNames.add("laatsteBoekstuk");
      javaVariableNames.add("rekening");
      javaVariableNames.add("tegenRekening");
      javaVariableNames.add("systemValue");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("id");

      fieldNames.put("mutationTimeStamp","DAGBOEK_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("Id","ID");
      fieldNames.put("Soort","SOORT");
      fieldNames.put("Omschrijving","OMSCHR");
      fieldNames.put("BoekstukMask","MASK");
      fieldNames.put("LaatsteBoekstuk","BOEKSTUK");
      fieldNames.put("Rekening","REKENING");
      fieldNames.put("TegenRekening","TEGENREK");
      fieldNames.put("SystemValue","SYSVAL");
      fieldNames.put("VrijeRub1","VRUB1");
      fieldNames.put("VrijeRub2","VRUB2");
      fieldNames.put("VrijeRub3","VRUB3");
      fieldNames.put("VrijeRub4","VRUB4");
      fieldNames.put("VrijeRub5","VRUB5");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("Id","DagboekId");
      fieldDescriptions.put("Soort","DagboekSoort");
      fieldDescriptions.put("Omschrijving","Omschrijving");
      fieldDescriptions.put("BoekstukMask","Boekstukmasker");
      fieldDescriptions.put("LaatsteBoekstuk","Laatste boekstuk");
      fieldDescriptions.put("Rekening","Grootboekrekening");
      fieldDescriptions.put("TegenRekening","Tegenrekening");
      fieldDescriptions.put("SystemValue","Systeemwaarde");
      fieldDescriptions.put("VrijeRub1","Vrije rubriek definitie 1");
      fieldDescriptions.put("VrijeRub2","Vrije rubriek definitie 2");
      fieldDescriptions.put("VrijeRub3","Vrije rubriek definitie 3");
      fieldDescriptions.put("VrijeRub4","Vrije rubriek definitie 4");
      fieldDescriptions.put("VrijeRub5","Vrije rubriek definitie 5");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("id","Id");
      javaToBONameMapping.put("soort","Soort");
      javaToBONameMapping.put("omschrijving","Omschrijving");
      javaToBONameMapping.put("boekstukMask","BoekstukMask");
      javaToBONameMapping.put("laatsteBoekstuk","LaatsteBoekstuk");
      javaToBONameMapping.put("rekening","Rekening");
      javaToBONameMapping.put("tegenRekening","TegenRekening");
      javaToBONameMapping.put("systemValue","SystemValue");
      javaToBONameMapping.put("vrijeRub1","VrijeRub1");
      javaToBONameMapping.put("vrijeRub2","VrijeRub2");
      javaToBONameMapping.put("vrijeRub3","VrijeRub3");
      javaToBONameMapping.put("vrijeRub4","VrijeRub4");
      javaToBONameMapping.put("vrijeRub5","VrijeRub5");

    }

    private PersistenceMetaDataDagboek(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Dagboek";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Dagboek" , "DAGBOEK");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Dagboek" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Dagboeken";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Id", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Soort", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschrijving", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BoekstukMask", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("LaatsteBoekstuk", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Rekening", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 9, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("TegenRekening", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 9, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("SystemValue", provider)	 + " " + provider.getDBMapping().getSQLDefinition("boolean", 0, 0) + " DEFAULT 'false'" + " NOT NULL	," + 
				getFieldName("VrijeRub1", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	," + 
				getFieldName("VrijeRub2", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	," + 
				getFieldName("VrijeRub3", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	," + 
				getFieldName("VrijeRub4", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	," + 
				getFieldName("VrijeRub5", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[15][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("Id", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "DagboekId"};
      columnDefinitions[3]=new String[]{getFieldName("Soort", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "DagboekSoort"};
      columnDefinitions[4]=new String[]{getFieldName("Omschrijving", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving"};
      columnDefinitions[5]=new String[]{getFieldName("BoekstukMask", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Boekstukmasker"};
      columnDefinitions[6]=new String[]{getFieldName("LaatsteBoekstuk", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Laatste boekstuk"};
      columnDefinitions[7]=new String[]{getFieldName("Rekening", provider), provider.getDBMapping().getSQLDefinition("String", 9, 0) , "''"  , " NOT NULL", "Grootboekrekening"};
      columnDefinitions[8]=new String[]{getFieldName("TegenRekening", provider), provider.getDBMapping().getSQLDefinition("String", 9, 0) , "''"  , " NOT NULL", "Tegenrekening"};
      columnDefinitions[9]=new String[]{getFieldName("SystemValue", provider), provider.getDBMapping().getSQLDefinition("boolean", 0, 0) , "'false'"  , " NOT NULL", "Systeemwaarde"};
      columnDefinitions[10]=new String[]{getFieldName("VrijeRub1", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Vrije rubriek definitie 1"};
      columnDefinitions[11]=new String[]{getFieldName("VrijeRub2", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Vrije rubriek definitie 2"};
      columnDefinitions[12]=new String[]{getFieldName("VrijeRub3", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Vrije rubriek definitie 3"};
      columnDefinitions[13]=new String[]{getFieldName("VrijeRub4", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Vrije rubriek definitie 4"};
      columnDefinitions[14]=new String[]{getFieldName("VrijeRub5", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Vrije rubriek definitie 5"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_DAGBOEK_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_DAGBOEK_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Id", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_DAGBOEK", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_DAGBOEK PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Id", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
