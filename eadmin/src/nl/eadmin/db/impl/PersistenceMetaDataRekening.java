package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataRekening implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataRekening();

      persistables.add("nl.eadmin.db.impl.Rekening_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("rekeningNr");
      javaVariableNames.add("omschrijving");
      javaVariableNames.add("omschrijvingKort");
      javaVariableNames.add("verdichting");
      javaVariableNames.add("iCPSoort");
      javaVariableNames.add("btwCode");
      javaVariableNames.add("btwSoort");
      javaVariableNames.add("btwRekening");
      javaVariableNames.add("subAdministratieType");
      javaVariableNames.add("toonKolom");
      javaVariableNames.add("blocked");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("rekeningNr");

      fieldNames.put("mutationTimeStamp","REKENING_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("RekeningNr","REKNR");
      fieldNames.put("Omschrijving","OMSCHR");
      fieldNames.put("OmschrijvingKort","OMSCHRKORT");
      fieldNames.put("Verdichting","VERDICHTNR");
      fieldNames.put("ICPSoort","ICPSOORT");
      fieldNames.put("BtwCode","BTWCODE");
      fieldNames.put("BtwSoort","BTWSOORT");
      fieldNames.put("BtwRekening","BTWREK");
      fieldNames.put("SubAdministratieType","SUBADMTYPE");
      fieldNames.put("ToonKolom","SHOWCOL");
      fieldNames.put("Blocked","BLOCKED");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("RekeningNr","RekeningNummer");
      fieldDescriptions.put("Omschrijving","Omschrijving");
      fieldDescriptions.put("OmschrijvingKort","Korte omschrijving");
      fieldDescriptions.put("Verdichting","Verdichtingsnummer");
      fieldDescriptions.put("ICPSoort","ICP soort");
      fieldDescriptions.put("BtwCode","BTW code");
      fieldDescriptions.put("BtwSoort","BTW soort  I=Inkoop V=Verkoop");
      fieldDescriptions.put("BtwRekening","Btw-rekening J/N");
      fieldDescriptions.put("SubAdministratieType","Subadministratie type");
      fieldDescriptions.put("ToonKolom","Presentation");
      fieldDescriptions.put("Blocked","Geblokkeerd");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("rekeningNr","RekeningNr");
      javaToBONameMapping.put("omschrijving","Omschrijving");
      javaToBONameMapping.put("omschrijvingKort","OmschrijvingKort");
      javaToBONameMapping.put("verdichting","Verdichting");
      javaToBONameMapping.put("iCPSoort","ICPSoort");
      javaToBONameMapping.put("btwCode","BtwCode");
      javaToBONameMapping.put("btwSoort","BtwSoort");
      javaToBONameMapping.put("btwRekening","BtwRekening");
      javaToBONameMapping.put("subAdministratieType","SubAdministratieType");
      javaToBONameMapping.put("toonKolom","ToonKolom");
      javaToBONameMapping.put("blocked","Blocked");

    }

    private PersistenceMetaDataRekening(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Rekening";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Rekening" , "REKENING");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Rekening" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Grootboekrekeningen";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("RekeningNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 9, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschrijving", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("OmschrijvingKort", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Verdichting", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 3, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("ICPSoort", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BtwCode", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 3, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BtwSoort", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT 'I'" + " NOT NULL	," + 
				getFieldName("BtwRekening", provider)	 + " " + provider.getDBMapping().getSQLDefinition("boolean", 0, 0) + " DEFAULT 'false'" + " NOT NULL	," + 
				getFieldName("SubAdministratieType", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("ToonKolom", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT 'S'" + " NOT NULL	," + 
				getFieldName("Blocked", provider)	 + " " + provider.getDBMapping().getSQLDefinition("boolean", 0, 0) + " DEFAULT 'false'" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[13][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("RekeningNr", provider), provider.getDBMapping().getSQLDefinition("String", 9, 0) , "''"  , " NOT NULL", "RekeningNummer"};
      columnDefinitions[3]=new String[]{getFieldName("Omschrijving", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving"};
      columnDefinitions[4]=new String[]{getFieldName("OmschrijvingKort", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Korte omschrijving"};
      columnDefinitions[5]=new String[]{getFieldName("Verdichting", provider), provider.getDBMapping().getSQLDefinition("int", 3, 0) , "0"  , " NOT NULL", "Verdichtingsnummer"};
      columnDefinitions[6]=new String[]{getFieldName("ICPSoort", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "ICP soort"};
      columnDefinitions[7]=new String[]{getFieldName("BtwCode", provider), provider.getDBMapping().getSQLDefinition("String", 3, 0) , "''"  , " NOT NULL", "BTW code"};
      columnDefinitions[8]=new String[]{getFieldName("BtwSoort", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "'I'"  , " NOT NULL", "BTW soort  I=Inkoop V=Verkoop"};
      columnDefinitions[9]=new String[]{getFieldName("BtwRekening", provider), provider.getDBMapping().getSQLDefinition("boolean", 0, 0) , "'false'"  , " NOT NULL", "Btw-rekening J/N"};
      columnDefinitions[10]=new String[]{getFieldName("SubAdministratieType", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "Subadministratie type"};
      columnDefinitions[11]=new String[]{getFieldName("ToonKolom", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "'S'"  , " NOT NULL", "Presentation"};
      columnDefinitions[12]=new String[]{getFieldName("Blocked", provider), provider.getDBMapping().getSQLDefinition("boolean", 0, 0) , "'false'"  , " NOT NULL", "Geblokkeerd"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_REKENING_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_REKENING_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("RekeningNr", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_REKENING", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_REKENING PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("RekeningNr", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
