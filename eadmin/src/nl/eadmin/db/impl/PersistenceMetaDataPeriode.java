package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataPeriode implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataPeriode();

      persistables.add("nl.eadmin.db.impl.Periode_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("boekjaar");
      javaVariableNames.add("boekperiode");
      javaVariableNames.add("startdatum");
      javaVariableNames.add("einddatum");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("boekjaar");
      javaPKVariableNames.add("boekperiode");

      fieldNames.put("mutationTimeStamp","PERIODE_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("Boekjaar","BOEKJAAR");
      fieldNames.put("Boekperiode","PERIODE");
      fieldNames.put("Startdatum","STARTDATUM");
      fieldNames.put("Einddatum","EINDDATUM");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("Boekjaar","Boekjaar");
      fieldDescriptions.put("Boekperiode","Boekperiode");
      fieldDescriptions.put("Startdatum","Startdatum");
      fieldDescriptions.put("Einddatum","Einddatum");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("boekjaar","Boekjaar");
      javaToBONameMapping.put("boekperiode","Boekperiode");
      javaToBONameMapping.put("startdatum","Startdatum");
      javaToBONameMapping.put("einddatum","Einddatum");

    }

    private PersistenceMetaDataPeriode(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Periode";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Periode" , "PERIODE");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Periode" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Boekperiode";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Boekjaar", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 4, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Boekperiode", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 3, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Startdatum", provider)	 + " " + provider.getDBMapping().getSQLDefinition("Date", 0, 0) + "	," + 
				getFieldName("Einddatum", provider)	 + " " + provider.getDBMapping().getSQLDefinition("Date", 0, 0) + " DEFAULT '0001-01-01'" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[6][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("Boekjaar", provider), provider.getDBMapping().getSQLDefinition("int", 4, 0) , "0"  , " NOT NULL", "Boekjaar"};
      columnDefinitions[3]=new String[]{getFieldName("Boekperiode", provider), provider.getDBMapping().getSQLDefinition("int", 3, 0) , "0"  , " NOT NULL", "Boekperiode"};
      columnDefinitions[4]=new String[]{getFieldName("Startdatum", provider), provider.getDBMapping().getSQLDefinition("Date", 0, 0) ,  null  , "", "Startdatum"};
      columnDefinitions[5]=new String[]{getFieldName("Einddatum", provider), provider.getDBMapping().getSQLDefinition("Date", 0, 0) , "'0001-01-01'"  , " NOT NULL", "Einddatum"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_PERIODE_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_PERIODE_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Boekjaar", provider) + " , "  +  getFieldName("Boekperiode", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_PERIODE", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_PERIODE PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Boekjaar", provider) + " , "  +  getFieldName("Boekperiode", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
