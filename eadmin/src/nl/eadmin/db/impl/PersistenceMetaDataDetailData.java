package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataDetailData implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataDetailData();

      persistables.add("nl.eadmin.db.impl.DetailData_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("dagboekId");
      javaVariableNames.add("boekstuk");
      javaVariableNames.add("boekstukRegel");
      javaVariableNames.add("datumLevering");
      javaVariableNames.add("omschr1");
      javaVariableNames.add("omschr2");
      javaVariableNames.add("omschr3");
      javaVariableNames.add("aantal");
      javaVariableNames.add("eenheid");
      javaVariableNames.add("prijs");
      javaVariableNames.add("btwCode");
      javaVariableNames.add("bedragExcl");
      javaVariableNames.add("bedragBtw");
      javaVariableNames.add("bedragIncl");
      javaVariableNames.add("bedragDebet");
      javaVariableNames.add("bedragCredit");
      javaVariableNames.add("dC");
      javaVariableNames.add("rekening");
      javaVariableNames.add("dcNummer");
      javaVariableNames.add("factuurNummer");
      javaVariableNames.add("vrijeRub1");
      javaVariableNames.add("vrijeRub2");
      javaVariableNames.add("vrijeRub3");
      javaVariableNames.add("vrijeRub4");
      javaVariableNames.add("vrijeRub5");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("dagboekId");
      javaPKVariableNames.add("boekstuk");
      javaPKVariableNames.add("boekstukRegel");

      fieldNames.put("mutationTimeStamp","DETAILDATA_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("DagboekId","DAGBOEK");
      fieldNames.put("Boekstuk","BOEKSTUK");
      fieldNames.put("BoekstukRegel","BOEKSTKRGL");
      fieldNames.put("DatumLevering","LEVERDTM");
      fieldNames.put("Omschr1","OMSCHR1");
      fieldNames.put("Omschr2","OMSCHR2");
      fieldNames.put("Omschr3","OMSCHR3");
      fieldNames.put("Aantal","AANTAL");
      fieldNames.put("Eenheid","EENHEID");
      fieldNames.put("Prijs","PRIJS");
      fieldNames.put("BtwCode","BTWCODE");
      fieldNames.put("BedragExcl","BEDRAGEXCL");
      fieldNames.put("BedragBtw","BEDRAGBTW");
      fieldNames.put("BedragIncl","BEDRAGINCL");
      fieldNames.put("BedragDebet","BEDRAGDEB");
      fieldNames.put("BedragCredit","BEDRAGCRED");
      fieldNames.put("DC","DC");
      fieldNames.put("Rekening","REKENING");
      fieldNames.put("DcNummer","DCNR");
      fieldNames.put("FactuurNummer","FACTUUR");
      fieldNames.put("VrijeRub1","VRIJERUB01");
      fieldNames.put("VrijeRub2","VRIJERUB02");
      fieldNames.put("VrijeRub3","VRIJERUB03");
      fieldNames.put("VrijeRub4","VRIJERUB04");
      fieldNames.put("VrijeRub5","VRIJERUB05");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("DagboekId","DagboekId");
      fieldDescriptions.put("Boekstuk","Boekstuknummer");
      fieldDescriptions.put("BoekstukRegel","Boekstukregel");
      fieldDescriptions.put("DatumLevering","Datum levering/dienst");
      fieldDescriptions.put("Omschr1","Omschrijving 1");
      fieldDescriptions.put("Omschr2","Omschrijving 2");
      fieldDescriptions.put("Omschr3","Omschrijving 3");
      fieldDescriptions.put("Aantal","Aantal");
      fieldDescriptions.put("Eenheid","Eenheid");
      fieldDescriptions.put("Prijs","Prijs per eenheid");
      fieldDescriptions.put("BtwCode","BTW code");
      fieldDescriptions.put("BedragExcl","Bedrag Excl.");
      fieldDescriptions.put("BedragBtw","Bedrag BTW");
      fieldDescriptions.put("BedragIncl","Bedrag Incl. BTW");
      fieldDescriptions.put("BedragDebet","Bedrag Debet");
      fieldDescriptions.put("BedragCredit","Bedrag Credit");
      fieldDescriptions.put("DC","Debit of Credit");
      fieldDescriptions.put("Rekening","Grootboekrekening");
      fieldDescriptions.put("DcNummer","DebCredNummer");
      fieldDescriptions.put("FactuurNummer","Factuurnummer");
      fieldDescriptions.put("VrijeRub1","Vrije rubriek 1");
      fieldDescriptions.put("VrijeRub2","Vrije rubriek 2");
      fieldDescriptions.put("VrijeRub3","Vrije rubriek 3");
      fieldDescriptions.put("VrijeRub4","Vrije rubriek 4");
      fieldDescriptions.put("VrijeRub5","Vrije rubriek 5");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("dagboekId","DagboekId");
      javaToBONameMapping.put("boekstuk","Boekstuk");
      javaToBONameMapping.put("boekstukRegel","BoekstukRegel");
      javaToBONameMapping.put("datumLevering","DatumLevering");
      javaToBONameMapping.put("omschr1","Omschr1");
      javaToBONameMapping.put("omschr2","Omschr2");
      javaToBONameMapping.put("omschr3","Omschr3");
      javaToBONameMapping.put("aantal","Aantal");
      javaToBONameMapping.put("eenheid","Eenheid");
      javaToBONameMapping.put("prijs","Prijs");
      javaToBONameMapping.put("btwCode","BtwCode");
      javaToBONameMapping.put("bedragExcl","BedragExcl");
      javaToBONameMapping.put("bedragBtw","BedragBtw");
      javaToBONameMapping.put("bedragIncl","BedragIncl");
      javaToBONameMapping.put("bedragDebet","BedragDebet");
      javaToBONameMapping.put("bedragCredit","BedragCredit");
      javaToBONameMapping.put("dC","DC");
      javaToBONameMapping.put("rekening","Rekening");
      javaToBONameMapping.put("dcNummer","DcNummer");
      javaToBONameMapping.put("factuurNummer","FactuurNummer");
      javaToBONameMapping.put("vrijeRub1","VrijeRub1");
      javaToBONameMapping.put("vrijeRub2","VrijeRub2");
      javaToBONameMapping.put("vrijeRub3","VrijeRub3");
      javaToBONameMapping.put("vrijeRub4","VrijeRub4");
      javaToBONameMapping.put("vrijeRub5","VrijeRub5");

    }

    private PersistenceMetaDataDetailData(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "DetailData";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("DetailData" , "DETAILDATA");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "DetailData" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Detailgegevens";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DagboekId", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Boekstuk", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BoekstukRegel", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 3, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("DatumLevering", provider)	 + " " + provider.getDBMapping().getSQLDefinition("Date", 0, 0) + "	," + 
				getFieldName("Omschr1", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschr2", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschr3", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Aantal", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("Eenheid", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Prijs", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("BtwCode", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 3, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BedragExcl", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("BedragBtw", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("BedragIncl", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("BedragDebet", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("BedragCredit", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("DC", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Rekening", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 9, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DcNummer", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("FactuurNummer", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 15, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub1", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub2", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub3", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub4", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("VrijeRub5", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[27][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("DagboekId", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "DagboekId"};
      columnDefinitions[3]=new String[]{getFieldName("Boekstuk", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Boekstuknummer"};
      columnDefinitions[4]=new String[]{getFieldName("BoekstukRegel", provider), provider.getDBMapping().getSQLDefinition("int", 3, 0) , "0"  , " NOT NULL", "Boekstukregel"};
      columnDefinitions[5]=new String[]{getFieldName("DatumLevering", provider), provider.getDBMapping().getSQLDefinition("Date", 0, 0) ,  null  , "", "Datum levering/dienst"};
      columnDefinitions[6]=new String[]{getFieldName("Omschr1", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 1"};
      columnDefinitions[7]=new String[]{getFieldName("Omschr2", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 2"};
      columnDefinitions[8]=new String[]{getFieldName("Omschr3", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 3"};
      columnDefinitions[9]=new String[]{getFieldName("Aantal", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Aantal"};
      columnDefinitions[10]=new String[]{getFieldName("Eenheid", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Eenheid"};
      columnDefinitions[11]=new String[]{getFieldName("Prijs", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Prijs per eenheid"};
      columnDefinitions[12]=new String[]{getFieldName("BtwCode", provider), provider.getDBMapping().getSQLDefinition("String", 3, 0) , "''"  , " NOT NULL", "BTW code"};
      columnDefinitions[13]=new String[]{getFieldName("BedragExcl", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Bedrag Excl."};
      columnDefinitions[14]=new String[]{getFieldName("BedragBtw", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Bedrag BTW"};
      columnDefinitions[15]=new String[]{getFieldName("BedragIncl", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Bedrag Incl. BTW"};
      columnDefinitions[16]=new String[]{getFieldName("BedragDebet", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Bedrag Debet"};
      columnDefinitions[17]=new String[]{getFieldName("BedragCredit", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Bedrag Credit"};
      columnDefinitions[18]=new String[]{getFieldName("DC", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "Debit of Credit"};
      columnDefinitions[19]=new String[]{getFieldName("Rekening", provider), provider.getDBMapping().getSQLDefinition("String", 9, 0) , "''"  , " NOT NULL", "Grootboekrekening"};
      columnDefinitions[20]=new String[]{getFieldName("DcNummer", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "DebCredNummer"};
      columnDefinitions[21]=new String[]{getFieldName("FactuurNummer", provider), provider.getDBMapping().getSQLDefinition("String", 15, 0) , "''"  , " NOT NULL", "Factuurnummer"};
      columnDefinitions[22]=new String[]{getFieldName("VrijeRub1", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 1"};
      columnDefinitions[23]=new String[]{getFieldName("VrijeRub2", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 2"};
      columnDefinitions[24]=new String[]{getFieldName("VrijeRub3", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 3"};
      columnDefinitions[25]=new String[]{getFieldName("VrijeRub4", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 4"};
      columnDefinitions[26]=new String[]{getFieldName("VrijeRub5", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Vrije rubriek 5"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_DETAILDATA_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_DETAILDATA_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DagboekId", provider) + " , "  +  getFieldName("Boekstuk", provider) + " , "  +  getFieldName("BoekstukRegel", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_DETAILDATA", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_DETAILDATA PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("DagboekId", provider) + " , "  +  getFieldName("Boekstuk", provider) + " , "  +  getFieldName("BoekstukRegel", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
