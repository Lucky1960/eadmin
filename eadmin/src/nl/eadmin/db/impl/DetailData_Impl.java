package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}

//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class DetailData_Impl  implements DetailData, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataDetailData.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String dagboekId;
    protected String boekstuk;
    protected int boekstukRegel;
    protected java.sql.Date datumLevering;
    protected String omschr1;
    protected String omschr2;
    protected String omschr3;
    protected BigDecimal aantal;
    protected String eenheid;
    protected BigDecimal prijs;
    protected String btwCode;
    protected BigDecimal bedragExcl;
    protected BigDecimal bedragBtw;
    protected BigDecimal bedragIncl;
    protected BigDecimal bedragDebet;
    protected BigDecimal bedragCredit;
    protected String dC;
    protected String rekening;
    protected String dcNummer;
    protected String factuurNummer;
    protected String vrijeRub1;
    protected String vrijeRub2;
    protected String vrijeRub3;
    protected String vrijeRub4;
    protected String vrijeRub5;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected DetailData_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected DetailData_Impl (DBData _dbd ,  String _bedrijf, String _dagboekId, String _boekstuk, int _boekstukRegel) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setDagboekId(_dagboekId);
        setBoekstuk(_boekstuk);
        setBoekstukRegel(_boekstukRegel);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected DetailData_Impl (DBData _dbd, DetailDataDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.DetailDataDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate DetailData with " + bean.getClass().getName() + "!! Use nl.eadmin.db.DetailDataDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setDagboekId(bean.getDagboekId());
        setBoekstuk(bean.getBoekstuk());
        setBoekstukRegel(bean.getBoekstukRegel());
        setDatumLevering(bean.getDatumLevering());
        setOmschr1(bean.getOmschr1());
        setOmschr2(bean.getOmschr2());
        setOmschr3(bean.getOmschr3());
        setAantal(bean.getAantal());
        setEenheid(bean.getEenheid());
        setPrijs(bean.getPrijs());
        setBtwCode(bean.getBtwCode());
        setBedragExcl(bean.getBedragExcl());
        setBedragBtw(bean.getBedragBtw());
        setBedragIncl(bean.getBedragIncl());
        setBedragDebet(bean.getBedragDebet());
        setBedragCredit(bean.getBedragCredit());
        setDC(bean.getDC());
        setRekening(bean.getRekening());
        setDcNummer(bean.getDcNummer());
        setFactuurNummer(bean.getFactuurNummer());
        setVrijeRub1(bean.getVrijeRub1());
        setVrijeRub2(bean.getVrijeRub2());
        setVrijeRub3(bean.getVrijeRub3());
        setVrijeRub4(bean.getVrijeRub4());
        setVrijeRub5(bean.getVrijeRub5());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public DetailData_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      dagboekId = rs.getString(pmd.getFieldName("DagboekId",provider));
      boekstuk = rs.getString(pmd.getFieldName("Boekstuk",provider));
      boekstukRegel = rs.getInt(pmd.getFieldName("BoekstukRegel",provider));
      datumLevering = rs.getDate(pmd.getFieldName("DatumLevering",provider));
      omschr1 = rs.getString(pmd.getFieldName("Omschr1",provider));
      omschr2 = rs.getString(pmd.getFieldName("Omschr2",provider));
      omschr3 = rs.getString(pmd.getFieldName("Omschr3",provider));
      aantal = rs.getBigDecimal(pmd.getFieldName("Aantal",provider));
      eenheid = rs.getString(pmd.getFieldName("Eenheid",provider));
      prijs = rs.getBigDecimal(pmd.getFieldName("Prijs",provider));
      btwCode = rs.getString(pmd.getFieldName("BtwCode",provider));
      bedragExcl = rs.getBigDecimal(pmd.getFieldName("BedragExcl",provider));
      bedragBtw = rs.getBigDecimal(pmd.getFieldName("BedragBtw",provider));
      bedragIncl = rs.getBigDecimal(pmd.getFieldName("BedragIncl",provider));
      bedragDebet = rs.getBigDecimal(pmd.getFieldName("BedragDebet",provider));
      bedragCredit = rs.getBigDecimal(pmd.getFieldName("BedragCredit",provider));
      dC = rs.getString(pmd.getFieldName("DC",provider));
      rekening = rs.getString(pmd.getFieldName("Rekening",provider));
      dcNummer = rs.getString(pmd.getFieldName("DcNummer",provider));
      factuurNummer = rs.getString(pmd.getFieldName("FactuurNummer",provider));
      vrijeRub1 = rs.getString(pmd.getFieldName("VrijeRub1",provider));
      vrijeRub2 = rs.getString(pmd.getFieldName("VrijeRub2",provider));
      vrijeRub3 = rs.getString(pmd.getFieldName("VrijeRub3",provider));
      vrijeRub4 = rs.getString(pmd.getFieldName("VrijeRub4",provider));
      vrijeRub5 = rs.getString(pmd.getFieldName("VrijeRub5",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       DetailDataPK key = new DetailDataPK();
       key.setBedrijf(getBedrijf());
       key.setDagboekId(getDagboekId());
       key.setBoekstuk(getBoekstuk());
       key.setBoekstukRegel(getBoekstukRegel());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getDagboekId()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:633870212}
//{CODE-INSERT-END:633870212}
      return trim(dagboekId);
      //Custom code
//{CODE-INSERT-BEGIN:-1824862241}
//{CODE-INSERT-END:-1824862241}
    }


    public String getBoekstuk()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1271118490}
//{CODE-INSERT-END:-1271118490}
      return trim(boekstuk);
      //Custom code
//{CODE-INSERT-BEGIN:-749969859}
//{CODE-INSERT-END:-749969859}
    }


    public int getBoekstukRegel()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1077355971}
//{CODE-INSERT-END:-1077355971}
      return boekstukRegel;
      //Custom code
//{CODE-INSERT-BEGIN:961700934}
//{CODE-INSERT-END:961700934}
    }


    public Date getDatumLevering()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1164507805}
//{CODE-INSERT-END:-1164507805}

        Date obj = datumLevering == null?null:new Date(datumLevering.getTime());

      //Custom code
//{CODE-INSERT-BEGIN:1209361267}
//{CODE-INSERT-END:1209361267}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:-1164508720}
//{CODE-INSERT-END:-1164508720}
    }


    public String getOmschr1()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1430504389}
//{CODE-INSERT-END:1430504389}
      return trim(omschr1);
      //Custom code
//{CODE-INSERT-BEGIN:1395960766}
//{CODE-INSERT-END:1395960766}
    }


    public String getOmschr2()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1431427910}
//{CODE-INSERT-END:1431427910}
      return trim(omschr2);
      //Custom code
//{CODE-INSERT-BEGIN:1424589917}
//{CODE-INSERT-END:1424589917}
    }


    public String getOmschr3()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1432351431}
//{CODE-INSERT-END:1432351431}
      return trim(omschr3);
      //Custom code
//{CODE-INSERT-BEGIN:1453219068}
//{CODE-INSERT-END:1453219068}
    }


    public BigDecimal getAantal()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1006611341}
//{CODE-INSERT-END:1006611341}
      return aantal;
      //Custom code
//{CODE-INSERT-BEGIN:1140178166}
//{CODE-INSERT-END:1140178166}
    }


    public String getEenheid()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1490637810}
//{CODE-INSERT-END:1490637810}
      return trim(eenheid);
      //Custom code
//{CODE-INSERT-BEGIN:-1034870479}
//{CODE-INSERT-END:-1034870479}
    }


    public BigDecimal getPrijs()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-358012036}
//{CODE-INSERT-END:-358012036}
      return prijs;
      //Custom code
//{CODE-INSERT-BEGIN:1786526439}
//{CODE-INSERT-END:1786526439}
    }


    public String getBtwCode()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:635008062}
//{CODE-INSERT-END:635008062}
      return trim(btwCode);
      //Custom code
//{CODE-INSERT-BEGIN:-1789588891}
//{CODE-INSERT-END:-1789588891}
    }


    public BigDecimal getBedragExcl()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-686220561}
//{CODE-INSERT-END:-686220561}
      return bedragExcl;
      //Custom code
//{CODE-INSERT-BEGIN:201996756}
//{CODE-INSERT-END:201996756}
    }


    public BigDecimal getBedragBtw()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1929545018}
//{CODE-INSERT-END:1929545018}
      return bedragBtw;
      //Custom code
//{CODE-INSERT-BEGIN:-313648919}
//{CODE-INSERT-END:-313648919}
    }


    public BigDecimal getBedragIncl()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1704951265}
//{CODE-INSERT-END:1704951265}
      return bedragIncl;
      //Custom code
//{CODE-INSERT-BEGIN:1313879330}
//{CODE-INSERT-END:1313879330}
    }


    public BigDecimal getBedragDebet()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2108193403}
//{CODE-INSERT-END:-2108193403}
      return bedragDebet;
      //Custom code
//{CODE-INSERT-BEGIN:-929488386}
//{CODE-INSERT-END:-929488386}
    }


    public BigDecimal getBedragCredit()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1289848652}
//{CODE-INSERT-END:1289848652}
      return bedragCredit;
      //Custom code
//{CODE-INSERT-BEGIN:1330600215}
//{CODE-INSERT-END:1330600215}
    }


    public String getDC()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:175435867}
//{CODE-INSERT-END:175435867}
      return trim(dC);
      //Custom code
//{CODE-INSERT-BEGIN:1143542248}
//{CODE-INSERT-END:1143542248}
    }


    public String getRekening()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1158739453}
//{CODE-INSERT-END:1158739453}
      return trim(rekening);
      //Custom code
//{CODE-INSERT-BEGIN:1561182342}
//{CODE-INSERT-END:1561182342}
    }


    public String getDcNummer()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1535325713}
//{CODE-INSERT-END:-1535325713}
      return trim(dcNummer);
      //Custom code
//{CODE-INSERT-BEGIN:-350459180}
//{CODE-INSERT-END:-350459180}
    }


    public String getFactuurNummer()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:627163238}
//{CODE-INSERT-END:627163238}
      return trim(factuurNummer);
      //Custom code
//{CODE-INSERT-BEGIN:-2032778435}
//{CODE-INSERT-END:-2032778435}
    }


    public String getVrijeRub1()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-229272282}
//{CODE-INSERT-END:-229272282}
      return vrijeRub1;
      //Custom code
//{CODE-INSERT-BEGIN:1482491517}
//{CODE-INSERT-END:1482491517}
    }


    public String getVrijeRub2()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-228348761}
//{CODE-INSERT-END:-228348761}
      return vrijeRub2;
      //Custom code
//{CODE-INSERT-BEGIN:1511120668}
//{CODE-INSERT-END:1511120668}
    }


    public String getVrijeRub3()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-227425240}
//{CODE-INSERT-END:-227425240}
      return vrijeRub3;
      //Custom code
//{CODE-INSERT-BEGIN:1539749819}
//{CODE-INSERT-END:1539749819}
    }


    public String getVrijeRub4()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-226501719}
//{CODE-INSERT-END:-226501719}
      return vrijeRub4;
      //Custom code
//{CODE-INSERT-BEGIN:1568378970}
//{CODE-INSERT-END:1568378970}
    }


    public String getVrijeRub5()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-225578198}
//{CODE-INSERT-END:-225578198}
      return vrijeRub5;
      //Custom code
//{CODE-INSERT-BEGIN:1597008121}
//{CODE-INSERT-END:1597008121}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setDagboekId(String _dagboekId ) throws Exception {

      if (_dagboekId !=  null)
         _dagboekId = _dagboekId.trim();
      if (_dagboekId !=  null)
         _dagboekId = _dagboekId.toUpperCase();
      if (_dagboekId == null)
          _dagboekId =  "";
      if (_dagboekId.equals(getDagboekId()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-306282608}
//{CODE-INSERT-END:-306282608}
      dagboekId =  _dagboekId;

      autoUpdate();

    }

    public void setBoekstuk(String _boekstuk ) throws Exception {

      if (_boekstuk !=  null)
         _boekstuk = _boekstuk.trim();
      if (_boekstuk == null)
          _boekstuk =  "";
      if (_boekstuk.equals(getBoekstuk()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1192405978}
//{CODE-INSERT-END:1192405978}
      boekstuk =  _boekstuk;

      autoUpdate();

    }

    public void setBoekstukRegel(int _boekstukRegel ) throws Exception {

      if (_boekstukRegel == getBoekstukRegel())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1458854985}
//{CODE-INSERT-END:1458854985}
      boekstukRegel =  _boekstukRegel;

      autoUpdate();

    }

    public void setDatumLevering(Date _datumLevering ) throws Exception {

      if ((_datumLevering == null ? getDatumLevering() == null : _datumLevering.equals(getDatumLevering())))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-549395073}
//{CODE-INSERT-END:-549395073}
      datumLevering =  _datumLevering == null?null: new java.sql.Date( _datumLevering.getTime());

      autoUpdate();

    }

    public void setOmschr1(String _omschr1 ) throws Exception {

      if (_omschr1 !=  null)
         _omschr1 = _omschr1.trim();
      if (_omschr1 == null)
          _omschr1 =  "";
      if (_omschr1.equals(getOmschr1()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1953710383}
//{CODE-INSERT-END:-1953710383}
      omschr1 =  _omschr1;

      autoUpdate();

    }

    public void setOmschr2(String _omschr2 ) throws Exception {

      if (_omschr2 !=  null)
         _omschr2 = _omschr2.trim();
      if (_omschr2 == null)
          _omschr2 =  "";
      if (_omschr2.equals(getOmschr2()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1952786862}
//{CODE-INSERT-END:-1952786862}
      omschr2 =  _omschr2;

      autoUpdate();

    }

    public void setOmschr3(String _omschr3 ) throws Exception {

      if (_omschr3 !=  null)
         _omschr3 = _omschr3.trim();
      if (_omschr3 == null)
          _omschr3 =  "";
      if (_omschr3.equals(getOmschr3()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1951863341}
//{CODE-INSERT-END:-1951863341}
      omschr3 =  _omschr3;

      autoUpdate();

    }

    public void setAantal(BigDecimal _aantal ) throws Exception {

      if (_aantal == null)
          _aantal =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_aantal.equals(getAantal()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-765124863}
//{CODE-INSERT-END:-765124863}
      aantal =  _aantal;

      autoUpdate();

    }

    public void setEenheid(String _eenheid ) throws Exception {

      if (_eenheid !=  null)
         _eenheid = _eenheid.trim();
      if (_eenheid == null)
          _eenheid =  "";
      if (_eenheid.equals(getEenheid()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1893576962}
//{CODE-INSERT-END:-1893576962}
      eenheid =  _eenheid;

      autoUpdate();

    }

    public void setPrijs(BigDecimal _prijs ) throws Exception {

      if (_prijs == null)
          _prijs =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_prijs.equals(getPrijs()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:416119176}
//{CODE-INSERT-END:416119176}
      prijs =  _prijs;

      autoUpdate();

    }

    public void setBtwCode(String _btwCode ) throws Exception {

      if (_btwCode !=  null)
         _btwCode = _btwCode.trim();
      if (_btwCode == null)
          _btwCode =  "";
      if (_btwCode.equals(getBtwCode()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1545760586}
//{CODE-INSERT-END:1545760586}
      btwCode =  _btwCode;

      autoUpdate();

    }

    public void setBedragExcl(BigDecimal _bedragExcl ) throws Exception {

      if (_bedragExcl == null)
          _bedragExcl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_bedragExcl.equals(getBedragExcl()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:233813091}
//{CODE-INSERT-END:233813091}
      bedragExcl =  _bedragExcl;

      autoUpdate();

    }

    public void setBedragBtw(BigDecimal _bedragBtw ) throws Exception {

      if (_bedragBtw == null)
          _bedragBtw =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_bedragBtw.equals(getBedragBtw()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:989392198}
//{CODE-INSERT-END:989392198}
      bedragBtw =  _bedragBtw;

      autoUpdate();

    }

    public void setBedragIncl(BigDecimal _bedragIncl ) throws Exception {

      if (_bedragIncl == null)
          _bedragIncl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_bedragIncl.equals(getBedragIncl()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1669982379}
//{CODE-INSERT-END:-1669982379}
      bedragIncl =  _bedragIncl;

      autoUpdate();

    }

    public void setBedragDebet(BigDecimal _bedragDebet ) throws Exception {

      if (_bedragDebet == null)
          _bedragDebet =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_bedragDebet.equals(getBedragDebet()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:643046033}
//{CODE-INSERT-END:643046033}
      bedragDebet =  _bedragDebet;

      autoUpdate();

    }

    public void setBedragCredit(BigDecimal _bedragCredit ) throws Exception {

      if (_bedragCredit == null)
          _bedragCredit =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_bedragCredit.equals(getBedragCredit()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:678925248}
//{CODE-INSERT-END:678925248}
      bedragCredit =  _bedragCredit;

      autoUpdate();

    }

    public void setDC(String _dC ) throws Exception {

      if (_dC !=  null)
         _dC = _dC.trim();
      if (_dC !=  null)
         _dC = _dC.toUpperCase();
      if (_dC == null)
          _dC =  "";
      if (_dC.equals(getDC()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:2112241103}
//{CODE-INSERT-END:2112241103}
      dC =  _dC;

      autoUpdate();

    }

    public void setRekening(String _rekening ) throws Exception {

      if (_rekening !=  null)
         _rekening = _rekening.trim();
      if (_rekening !=  null)
         _rekening = _rekening.toUpperCase();
      if (_rekening == null)
          _rekening =  "";
      if (_rekening.equals(getRekening()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-672703375}
//{CODE-INSERT-END:-672703375}
      rekening =  _rekening;

      autoUpdate();

    }

    public void setDcNummer(String _dcNummer ) throws Exception {

      if (_dcNummer !=  null)
         _dcNummer = _dcNummer.trim();
      if (_dcNummer == null)
          _dcNummer =  "";
      if (_dcNummer.equals(getDcNummer()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:928198755}
//{CODE-INSERT-END:928198755}
      dcNummer =  _dcNummer;

      autoUpdate();

    }

    public void setFactuurNummer(String _factuurNummer ) throws Exception {

      if (_factuurNummer !=  null)
         _factuurNummer = _factuurNummer.trim();
      if (_factuurNummer == null)
          _factuurNummer =  "";
      if (_factuurNummer.equals(getFactuurNummer()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1131593102}
//{CODE-INSERT-END:-1131593102}
      factuurNummer =  _factuurNummer;

      autoUpdate();

    }

    public void setVrijeRub1(String _vrijeRub1 ) throws Exception {

      if (_vrijeRub1 == null)
          _vrijeRub1 =  "";
      if (_vrijeRub1.equals(getVrijeRub1()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1169425102}
//{CODE-INSERT-END:-1169425102}
      vrijeRub1 =  _vrijeRub1;

      autoUpdate();

    }

    public void setVrijeRub2(String _vrijeRub2 ) throws Exception {

      if (_vrijeRub2 == null)
          _vrijeRub2 =  "";
      if (_vrijeRub2.equals(getVrijeRub2()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1168501581}
//{CODE-INSERT-END:-1168501581}
      vrijeRub2 =  _vrijeRub2;

      autoUpdate();

    }

    public void setVrijeRub3(String _vrijeRub3 ) throws Exception {

      if (_vrijeRub3 == null)
          _vrijeRub3 =  "";
      if (_vrijeRub3.equals(getVrijeRub3()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1167578060}
//{CODE-INSERT-END:-1167578060}
      vrijeRub3 =  _vrijeRub3;

      autoUpdate();

    }

    public void setVrijeRub4(String _vrijeRub4 ) throws Exception {

      if (_vrijeRub4 == null)
          _vrijeRub4 =  "";
      if (_vrijeRub4.equals(getVrijeRub4()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1166654539}
//{CODE-INSERT-END:-1166654539}
      vrijeRub4 =  _vrijeRub4;

      autoUpdate();

    }

    public void setVrijeRub5(String _vrijeRub5 ) throws Exception {

      if (_vrijeRub5 == null)
          _vrijeRub5 =  "";
      if (_vrijeRub5.equals(getVrijeRub5()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1165731018}
//{CODE-INSERT-END:-1165731018}
      vrijeRub5 =  _vrijeRub5;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      dagboekId =  "";
      boekstuk =  "";
      boekstukRegel = 0;
      omschr1 =  "";
      omschr2 =  "";
      omschr3 =  "";
      aantal =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      eenheid =  "";
      prijs =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      btwCode =  "";
      bedragExcl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      bedragBtw =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      bedragIncl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      bedragDebet =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      bedragCredit =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      dC =  "";
      rekening =  "";
      dcNummer =  "";
      factuurNummer =  "";
      vrijeRub1 =  "";
      vrijeRub2 =  "";
      vrijeRub3 =  "";
      vrijeRub4 =  "";
      vrijeRub5 =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.dagboekId = dagboekId;
        currentDBImage.boekstuk = boekstuk;
        currentDBImage.boekstukRegel = boekstukRegel;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("DetailData is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         DetailDataPK key = new DetailDataPK();
         key.setBedrijf(getBedrijf());
         key.setDagboekId(getDagboekId());
         key.setBoekstuk(getBoekstuk());
         key.setBoekstukRegel(getBoekstukRegel());
         ((nl.eadmin.db.impl.DetailDataManager_Impl)nl.eadmin.db.impl.DetailDataManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((DetailDataManager_Impl)DetailDataManager_Impl.getInstance(dbd)).getResultSet((DetailDataPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("624086135_"+ dbd.getDBId() + bedrijf + dagboekId + boekstuk + boekstukRegel).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getDetailDataDataBean()!
    */
    public DetailDataDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public DetailDataDataBean getDetailDataDataBean() throws Exception {
      return getDataBean(null);
    }

    private DetailDataDataBean getDataBean(DetailDataDataBean bean) throws Exception {

      if (bean == null)
          bean = new DetailDataDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setDagboekId(getDagboekId());
      bean.setBoekstuk(getBoekstuk());
      bean.setBoekstukRegel(getBoekstukRegel());
      bean.setDatumLevering(getDatumLevering());
      bean.setOmschr1(getOmschr1());
      bean.setOmschr2(getOmschr2());
      bean.setOmschr3(getOmschr3());
      bean.setAantal(getAantal());
      bean.setEenheid(getEenheid());
      bean.setPrijs(getPrijs());
      bean.setBtwCode(getBtwCode());
      bean.setBedragExcl(getBedragExcl());
      bean.setBedragBtw(getBedragBtw());
      bean.setBedragIncl(getBedragIncl());
      bean.setBedragDebet(getBedragDebet());
      bean.setBedragCredit(getBedragCredit());
      bean.setDC(getDC());
      bean.setRekening(getRekening());
      bean.setDcNummer(getDcNummer());
      bean.setFactuurNummer(getFactuurNummer());
      bean.setVrijeRub1(getVrijeRub1());
      bean.setVrijeRub2(getVrijeRub2());
      bean.setVrijeRub3(getVrijeRub3());
      bean.setVrijeRub4(getVrijeRub4());
      bean.setVrijeRub5(getVrijeRub5());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.DetailData_Impl))
             return false;
          nl.eadmin.db.impl.DetailData_Impl bo = (nl.eadmin.db.impl.DetailData_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getDagboekId() == null){
              if (this.getDagboekId() != null)
                  return false;
          }else if (!bo.getDagboekId().equals(this.getDagboekId())){
              return false;
          }
          if (bo.getBoekstuk() == null){
              if (this.getBoekstuk() != null)
                  return false;
          }else if (!bo.getBoekstuk().equals(this.getBoekstuk())){
              return false;
          }
          if (bo.getBoekstukRegel() != this.getBoekstukRegel())
              return false;
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(DetailDataDataBean bean) throws Exception {
       set(bean);
    }


    public void set(DetailDataDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setDagboekId(bean.getDagboekId());
           setBoekstuk(bean.getBoekstuk());
           setBoekstukRegel(bean.getBoekstukRegel());
           setDatumLevering(bean.getDatumLevering());
           setOmschr1(bean.getOmschr1());
           setOmschr2(bean.getOmschr2());
           setOmschr3(bean.getOmschr3());
           setAantal(bean.getAantal());
           setEenheid(bean.getEenheid());
           setPrijs(bean.getPrijs());
           setBtwCode(bean.getBtwCode());
           setBedragExcl(bean.getBedragExcl());
           setBedragBtw(bean.getBedragBtw());
           setBedragIncl(bean.getBedragIncl());
           setBedragDebet(bean.getBedragDebet());
           setBedragCredit(bean.getBedragCredit());
           setDC(bean.getDC());
           setRekening(bean.getRekening());
           setDcNummer(bean.getDcNummer());
           setFactuurNummer(bean.getFactuurNummer());
           setVrijeRub1(bean.getVrijeRub1());
           setVrijeRub2(bean.getVrijeRub2());
           setVrijeRub3(bean.getVrijeRub3());
           setVrijeRub4(bean.getVrijeRub4());
           setVrijeRub5(bean.getVrijeRub5());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"DetailData is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.DetailData.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("DagboekId",provider) +  " = ? " + ", " + pmd.getFieldName("Boekstuk",provider) +  " = ? " + ", " + pmd.getFieldName("BoekstukRegel",provider) +  " = ? " + ", " + pmd.getFieldName("DatumLevering",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr1",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr2",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr3",provider) +  " = ? " + ", " + pmd.getFieldName("Aantal",provider) +  " = ? " + ", " + pmd.getFieldName("Eenheid",provider) +  " = ? " + ", " + pmd.getFieldName("Prijs",provider) +  " = ? " + ", " + pmd.getFieldName("BtwCode",provider) +  " = ? " + ", " + pmd.getFieldName("BedragExcl",provider) +  " = ? " + ", " + pmd.getFieldName("BedragBtw",provider) +  " = ? " + ", " + pmd.getFieldName("BedragIncl",provider) +  " = ? " + ", " + pmd.getFieldName("BedragDebet",provider) +  " = ? " + ", " + pmd.getFieldName("BedragCredit",provider) +  " = ? " + ", " + pmd.getFieldName("DC",provider) +  " = ? " + ", " + pmd.getFieldName("Rekening",provider) +  " = ? " + ", " + pmd.getFieldName("DcNummer",provider) +  " = ? " + ", " + pmd.getFieldName("FactuurNummer",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub1",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub2",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub3",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub4",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub5",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("DagboekId",provider) + " = ? " + " AND  " +  pmd.getFieldName("Boekstuk",provider) + " = ? " + " AND  " +  pmd.getFieldName("BoekstukRegel",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dagboekId,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  boekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( boekstukRegel),  mapping.getJDBCTypeFor("int"));
        if ( datumLevering == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("Date"));
       } else {
          prep.setObject(parmIndex++,  datumLevering,  mapping.getJDBCTypeFor("Date"));
}
        prep.setObject(parmIndex++,  omschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschr3,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  aantal,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  eenheid,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  prijs,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  btwCode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  bedragExcl,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  bedragBtw,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  bedragIncl,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  bedragDebet,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  bedragCredit,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  dC,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  rekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dcNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  factuurNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub3,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub4,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub5,  mapping.getJDBCTypeFor("String"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.dagboekId,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.boekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer(image.boekstukRegel),  mapping.getJDBCTypeFor("int"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("DetailData modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.DetailData.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.DetailData.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("DagboekId",provider) + ", " + pmd.getFieldName("Boekstuk",provider) + ", " + pmd.getFieldName("BoekstukRegel",provider) + ", " + pmd.getFieldName("DatumLevering",provider) + ", " + pmd.getFieldName("Omschr1",provider) + ", " + pmd.getFieldName("Omschr2",provider) + ", " + pmd.getFieldName("Omschr3",provider) + ", " + pmd.getFieldName("Aantal",provider) + ", " + pmd.getFieldName("Eenheid",provider) + ", " + pmd.getFieldName("Prijs",provider) + ", " + pmd.getFieldName("BtwCode",provider) + ", " + pmd.getFieldName("BedragExcl",provider) + ", " + pmd.getFieldName("BedragBtw",provider) + ", " + pmd.getFieldName("BedragIncl",provider) + ", " + pmd.getFieldName("BedragDebet",provider) + ", " + pmd.getFieldName("BedragCredit",provider) + ", " + pmd.getFieldName("DC",provider) + ", " + pmd.getFieldName("Rekening",provider) + ", " + pmd.getFieldName("DcNummer",provider) + ", " + pmd.getFieldName("FactuurNummer",provider) + ", " + pmd.getFieldName("VrijeRub1",provider) + ", " + pmd.getFieldName("VrijeRub2",provider) + ", " + pmd.getFieldName("VrijeRub3",provider) + ", " + pmd.getFieldName("VrijeRub4",provider) + ", " + pmd.getFieldName("VrijeRub5",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, dagboekId,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, boekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5,  new Integer(boekstukRegel),  mapping.getJDBCTypeFor("int"));
        if (datumLevering == null)
          prep.setNull(6,  mapping.getJDBCTypeFor("Date"));
        else
          prep.setObject(6, datumLevering,  mapping.getJDBCTypeFor("Date"));
        prep.setObject(7, omschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(8, omschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(9, omschr3,  mapping.getJDBCTypeFor("String"));
        prep.setObject(10, aantal,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(11, eenheid,  mapping.getJDBCTypeFor("String"));
        prep.setObject(12, prijs,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(13, btwCode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(14, bedragExcl,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(15, bedragBtw,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(16, bedragIncl,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(17, bedragDebet,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(18, bedragCredit,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(19, dC,  mapping.getJDBCTypeFor("String"));
        prep.setObject(20, rekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(21, dcNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(22, factuurNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(23, vrijeRub1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(24, vrijeRub2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(25, vrijeRub3,  mapping.getJDBCTypeFor("String"));
        prep.setObject(26, vrijeRub4,  mapping.getJDBCTypeFor("String"));
        prep.setObject(27, vrijeRub5,  mapping.getJDBCTypeFor("String"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.dagboekId = currentDBImage.dagboekId;
        transactionImage.dbImage.boekstuk = currentDBImage.boekstuk;
        transactionImage.dbImage.boekstukRegel = currentDBImage.boekstukRegel;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.dagboekId = transactionImage.dbImage.dagboekId;
      currentDBImage.boekstuk = transactionImage.dbImage.boekstuk;
      currentDBImage.boekstukRegel = transactionImage.dbImage.boekstukRegel;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        dagboekId = transactionImage.dbImage.dagboekId;
        boekstuk = transactionImage.dbImage.boekstuk;
        boekstukRegel = transactionImage.dbImage.boekstukRegel;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String dagboekId;
      private String boekstuk;
      private int boekstukRegel;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
    public HeaderData getHeaderData() throws Exception {
    	HeaderDataPK key = new HeaderDataPK();
    	key.setBedrijf(bedrijf);
    	key.setDagboekId(dagboekId);
    	key.setBoekstuk(boekstuk);
    	return HeaderDataManager_Impl.getInstance(dbd).findOrCreate(key);
    }

    public Rekening getRekeningObject() throws Exception {
    	RekeningPK key = new RekeningPK();
    	key.setBedrijf(bedrijf);
    	key.setRekeningNr(rekening);
    	try {
			return RekeningManagerFactory.getInstance(dbd).findByPrimaryKey(key);
		} catch (Exception e) {
	    	return null;
		}
    }

    public BtwCode getBtwCodeObject() throws Exception {
		BtwCodePK btwKey = new BtwCodePK();
    	btwKey.setBedrijf(bedrijf);
    	btwKey.setCode(btwCode);
    	try {
    		return BtwCodeManagerFactory.getInstance(dbd).findByPrimaryKey(btwKey);
		} catch (Exception e) {
	    	return null;
		}
    }
//{CODE-INSERT-END:955534258}
}
