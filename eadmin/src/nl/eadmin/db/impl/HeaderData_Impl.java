package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;

import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import java.util.concurrent.TimeUnit;

import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;




/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
import nl.eadmin.ApplicationConstants;
import nl.eadmin.db.Bijlage;
import nl.eadmin.db.BijlageManagerFactory;
import nl.eadmin.db.Dagboek;
import nl.eadmin.db.DagboekManagerFactory;
import nl.eadmin.db.DagboekPK;
import nl.eadmin.db.DetailData;
import nl.eadmin.db.DetailDataManagerFactory;
import nl.eadmin.db.HeaderData;
import nl.eadmin.db.HeaderDataDataBean;
import nl.eadmin.db.HeaderDataPK;
import nl.eadmin.enums.DagboekSoortEnum;
import nl.eadmin.helpers.GeneralHelper;
import nl.eadmin.helpers.JournaalHelper;
import nl.ibs.jeelog.Log;
import nl.ibs.jsql.AtomicDBTransaction;
import nl.ibs.jsql.BusinessObject_Impl;
import nl.ibs.jsql.DBConfig;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.DBPersistenceManager;
import nl.ibs.jsql.DBTransactionListener;
import nl.ibs.jsql.FreeQuery;
import nl.ibs.jsql.QueryFactory;
import nl.ibs.jsql.exception.CreateException;
import nl.ibs.jsql.exception.DeletedException;
import nl.ibs.jsql.exception.FinderException;
import nl.ibs.jsql.exception.InvalidStateException;
import nl.ibs.jsql.exception.JSQLException;
import nl.ibs.jsql.exception.UpdateException;
import nl.ibs.jsql.impl.ArrayListImpl;
import nl.ibs.jsql.sql.AttributeAccessor;
import nl.ibs.jsql.sql.ConnectionProvider;
import nl.ibs.jsql.sql.DBConnection;
import nl.ibs.jsql.sql.DBMapping;
import nl.ibs.jsql.sql.PersistenceMetaData;

import org.w3c.dom.DOMImplementation;
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class HeaderData_Impl  implements HeaderData, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataHeaderData.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String dagboekId;
    protected String dagboekSoort;
    protected String boekstuk;
    protected int status;
    protected boolean favoriet;
    protected java.sql.Date boekdatum;
    protected int boekjaar;
    protected int boekperiode;
    protected String dcNummer;
    protected String factuurNummer;
    protected String referentieNummer;
    protected java.sql.Date factuurdatum;
    protected String factuurLayout;
    protected java.sql.Date vervaldatum;
    protected int aantalAanmaningen;
    protected String dC;
    protected BigDecimal totaalExcl;
    protected BigDecimal totaalBtw;
    protected BigDecimal totaalIncl;
    protected BigDecimal totaalGeboekt;
    protected BigDecimal totaalBetaald;
    protected BigDecimal totaalOpenstaand;
    protected BigDecimal totaalDebet;
    protected BigDecimal totaalCredit;
    protected BigDecimal beginsaldo;
    protected BigDecimal eindsaldo;
    protected String omschr1;
    protected String omschr2;
    protected String vrijeRub1;
    protected String vrijeRub2;
    protected String vrijeRub3;
    protected String vrijeRub4;
    protected String vrijeRub5;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
    protected Dagboek dagboek;
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected HeaderData_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected HeaderData_Impl (DBData _dbd ,  String _bedrijf, String _dagboekId, String _boekstuk) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setDagboekId(_dagboekId);
        setBoekstuk(_boekstuk);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected HeaderData_Impl (DBData _dbd, HeaderDataDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.HeaderDataDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate HeaderData with " + bean.getClass().getName() + "!! Use nl.eadmin.db.HeaderDataDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setDagboekId(bean.getDagboekId());
        setDagboekSoort(bean.getDagboekSoort());
        setBoekstuk(bean.getBoekstuk());
        setStatus(bean.getStatus());
        setFavoriet(bean.getFavoriet());
        setBoekdatum(bean.getBoekdatum());
        setBoekjaar(bean.getBoekjaar());
        setBoekperiode(bean.getBoekperiode());
        setDcNummer(bean.getDcNummer());
        setFactuurNummer(bean.getFactuurNummer());
        setReferentieNummer(bean.getReferentieNummer());
        setFactuurdatum(bean.getFactuurdatum());
        setFactuurLayout(bean.getFactuurLayout());
        setVervaldatum(bean.getVervaldatum());
        setAantalAanmaningen(bean.getAantalAanmaningen());
        setDC(bean.getDC());
        setTotaalExcl(bean.getTotaalExcl());
        setTotaalBtw(bean.getTotaalBtw());
        setTotaalIncl(bean.getTotaalIncl());
        setTotaalGeboekt(bean.getTotaalGeboekt());
        setTotaalBetaald(bean.getTotaalBetaald());
        setTotaalOpenstaand(bean.getTotaalOpenstaand());
        setTotaalDebet(bean.getTotaalDebet());
        setTotaalCredit(bean.getTotaalCredit());
        setBeginsaldo(bean.getBeginsaldo());
        setEindsaldo(bean.getEindsaldo());
        setOmschr1(bean.getOmschr1());
        setOmschr2(bean.getOmschr2());
        setVrijeRub1(bean.getVrijeRub1());
        setVrijeRub2(bean.getVrijeRub2());
        setVrijeRub3(bean.getVrijeRub3());
        setVrijeRub4(bean.getVrijeRub4());
        setVrijeRub5(bean.getVrijeRub5());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public HeaderData_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      dagboekId = rs.getString(pmd.getFieldName("DagboekId",provider));
      dagboekSoort = rs.getString(pmd.getFieldName("DagboekSoort",provider));
      boekstuk = rs.getString(pmd.getFieldName("Boekstuk",provider));
      status = rs.getInt(pmd.getFieldName("Status",provider));
      favoriet = rs.getBoolean(pmd.getFieldName("Favoriet",provider));
      boekdatum = rs.getDate(pmd.getFieldName("Boekdatum",provider));
      boekjaar = rs.getInt(pmd.getFieldName("Boekjaar",provider));
      boekperiode = rs.getInt(pmd.getFieldName("Boekperiode",provider));
      dcNummer = rs.getString(pmd.getFieldName("DcNummer",provider));
      factuurNummer = rs.getString(pmd.getFieldName("FactuurNummer",provider));
      referentieNummer = rs.getString(pmd.getFieldName("ReferentieNummer",provider));
      factuurdatum = rs.getDate(pmd.getFieldName("Factuurdatum",provider));
      factuurLayout = rs.getString(pmd.getFieldName("FactuurLayout",provider));
      vervaldatum = rs.getDate(pmd.getFieldName("Vervaldatum",provider));
      aantalAanmaningen = rs.getInt(pmd.getFieldName("AantalAanmaningen",provider));
      dC = rs.getString(pmd.getFieldName("DC",provider));
      totaalExcl = rs.getBigDecimal(pmd.getFieldName("TotaalExcl",provider));
      totaalBtw = rs.getBigDecimal(pmd.getFieldName("TotaalBtw",provider));
      totaalIncl = rs.getBigDecimal(pmd.getFieldName("TotaalIncl",provider));
      totaalGeboekt = rs.getBigDecimal(pmd.getFieldName("TotaalGeboekt",provider));
      totaalBetaald = rs.getBigDecimal(pmd.getFieldName("TotaalBetaald",provider));
      totaalOpenstaand = rs.getBigDecimal(pmd.getFieldName("TotaalOpenstaand",provider));
      totaalDebet = rs.getBigDecimal(pmd.getFieldName("TotaalDebet",provider));
      totaalCredit = rs.getBigDecimal(pmd.getFieldName("TotaalCredit",provider));
      beginsaldo = rs.getBigDecimal(pmd.getFieldName("Beginsaldo",provider));
      eindsaldo = rs.getBigDecimal(pmd.getFieldName("Eindsaldo",provider));
      omschr1 = rs.getString(pmd.getFieldName("Omschr1",provider));
      omschr2 = rs.getString(pmd.getFieldName("Omschr2",provider));
      vrijeRub1 = rs.getString(pmd.getFieldName("VrijeRub1",provider));
      vrijeRub2 = rs.getString(pmd.getFieldName("VrijeRub2",provider));
      vrijeRub3 = rs.getString(pmd.getFieldName("VrijeRub3",provider));
      vrijeRub4 = rs.getString(pmd.getFieldName("VrijeRub4",provider));
      vrijeRub5 = rs.getString(pmd.getFieldName("VrijeRub5",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       HeaderDataPK key = new HeaderDataPK();
       key.setBedrijf(getBedrijf());
       key.setDagboekId(getDagboekId());
       key.setBoekstuk(getBoekstuk());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getDagboekId()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:633870212}
//{CODE-INSERT-END:633870212}
      return trim(dagboekId);
      //Custom code
//{CODE-INSERT-BEGIN:-1824862241}
//{CODE-INSERT-END:-1824862241}
    }


    public String getDagboekSoort()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1126849108}
//{CODE-INSERT-END:1126849108}
      return trim(dagboekSoort);
      //Custom code
//{CODE-INSERT-BEGIN:572581647}
//{CODE-INSERT-END:572581647}
    }


    public String getBoekstuk()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1271118490}
//{CODE-INSERT-END:-1271118490}
      return trim(boekstuk);
      //Custom code
//{CODE-INSERT-BEGIN:-749969859}
//{CODE-INSERT-END:-749969859}
    }


    public int getStatus()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1179788910}
//{CODE-INSERT-END:1179788910}
      return status;
      //Custom code
//{CODE-INSERT-BEGIN:-2081251787}
//{CODE-INSERT-END:-2081251787}
    }


    public boolean getFavoriet()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1876878422}
//{CODE-INSERT-END:1876878422}
      return favoriet;
      //Custom code
//{CODE-INSERT-BEGIN:-1946313395}
//{CODE-INSERT-END:-1946313395}
    }


    public Date getBoekdatum()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:82796942}
//{CODE-INSERT-END:82796942}

        Date obj = boekdatum == null?null:new Date(boekdatum.getTime());

      //Custom code
//{CODE-INSERT-BEGIN:1249596904}
//{CODE-INSERT-END:1249596904}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:82796027}
//{CODE-INSERT-END:82796027}
    }


    public int getBoekjaar()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-25361449}
//{CODE-INSERT-END:-25361449}
      return boekjaar;
      //Custom code
//{CODE-INSERT-BEGIN:-786207252}
//{CODE-INSERT-END:-786207252}
    }


    public int getBoekperiode()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-814355331}
//{CODE-INSERT-END:-814355331}
      return boekperiode;
      //Custom code
//{CODE-INSERT-BEGIN:524786182}
//{CODE-INSERT-END:524786182}
    }


    public String getDcNummer()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1535325713}
//{CODE-INSERT-END:-1535325713}
      return trim(dcNummer);
      //Custom code
//{CODE-INSERT-BEGIN:-350459180}
//{CODE-INSERT-END:-350459180}
    }


    public String getFactuurNummer()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:627163238}
//{CODE-INSERT-END:627163238}
      return trim(factuurNummer);
      //Custom code
//{CODE-INSERT-BEGIN:-2032778435}
//{CODE-INSERT-END:-2032778435}
    }


    public String getReferentieNummer()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-463907273}
//{CODE-INSERT-END:-463907273}
      return trim(referentieNummer);
      //Custom code
//{CODE-INSERT-BEGIN:-1496225908}
//{CODE-INSERT-END:-1496225908}
    }


    public Date getFactuurdatum()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:425042385}
//{CODE-INSERT-END:425042385}

        Date obj = factuurdatum == null?null:new Date(factuurdatum.getTime());

      //Custom code
//{CODE-INSERT-BEGIN:-1510309563}
//{CODE-INSERT-END:-1510309563}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:425041470}
//{CODE-INSERT-END:425041470}
    }


    public String getFactuurLayout()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:188382204}
//{CODE-INSERT-END:188382204}
      return factuurLayout;
      //Custom code
//{CODE-INSERT-BEGIN:1544878695}
//{CODE-INSERT-END:1544878695}
    }


    public Date getVervaldatum()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:755396793}
//{CODE-INSERT-END:755396793}

        Date obj = vervaldatum == null?null:new Date(vervaldatum.getTime());

      //Custom code
//{CODE-INSERT-BEGIN:855651677}
//{CODE-INSERT-END:855651677}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:755395878}
//{CODE-INSERT-END:755395878}
    }


    public int getAantalAanmaningen()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:351979066}
//{CODE-INSERT-END:351979066}
      return aantalAanmaningen;
      //Custom code
//{CODE-INSERT-BEGIN:-1973553175}
//{CODE-INSERT-END:-1973553175}
    }


    public String getDC()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:175435867}
//{CODE-INSERT-END:175435867}
      return trim(dC);
      //Custom code
//{CODE-INSERT-BEGIN:1143542248}
//{CODE-INSERT-END:1143542248}
    }


    public BigDecimal getTotaalExcl()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2028983477}
//{CODE-INSERT-END:-2028983477}
      return totaalExcl;
      //Custom code
//{CODE-INSERT-BEGIN:1526019320}
//{CODE-INSERT-END:1526019320}
    }


    public BigDecimal getTotaalBtw()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1577453218}
//{CODE-INSERT-END:-1577453218}
      return totaalBtw;
      //Custom code
//{CODE-INSERT-BEGIN:-1656411835}
//{CODE-INSERT-END:-1656411835}
    }


    public BigDecimal getTotaalIncl()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:362188349}
//{CODE-INSERT-END:362188349}
      return totaalIncl;
      //Custom code
//{CODE-INSERT-BEGIN:-1657065402}
//{CODE-INSERT-END:-1657065402}
    }


    public BigDecimal getTotaalGeboekt()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1397453948}
//{CODE-INSERT-END:1397453948}
      return totaalGeboekt;
      //Custom code
//{CODE-INSERT-BEGIN:371397095}
//{CODE-INSERT-END:371397095}
    }


    public BigDecimal getTotaalBetaald()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1452131362}
//{CODE-INSERT-END:1452131362}
      return totaalBetaald;
      //Custom code
//{CODE-INSERT-BEGIN:2066396929}
//{CODE-INSERT-END:2066396929}
    }


    public BigDecimal getTotaalOpenstaand()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:752513712}
//{CODE-INSERT-END:752513712}
      return totaalOpenstaand;
      //Custom code
//{CODE-INSERT-BEGIN:1853086259}
//{CODE-INSERT-END:1853086259}
    }


    public BigDecimal getTotaalDebet()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-784170839}
//{CODE-INSERT-END:-784170839}
      return totaalDebet;
      //Custom code
//{CODE-INSERT-BEGIN:1460505434}
//{CODE-INSERT-END:1460505434}
    }


    public BigDecimal getTotaalCredit()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-615124824}
//{CODE-INSERT-END:-615124824}
      return totaalCredit;
      //Custom code
//{CODE-INSERT-BEGIN:-1889002693}
//{CODE-INSERT-END:-1889002693}
    }


    public BigDecimal getBeginsaldo()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-255265444}
//{CODE-INSERT-END:-255265444}
      return beginsaldo;
      //Custom code
//{CODE-INSERT-BEGIN:676703495}
//{CODE-INSERT-END:676703495}
    }


    public BigDecimal getEindsaldo()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1640836507}
//{CODE-INSERT-END:1640836507}
      return eindsaldo;
      //Custom code
//{CODE-INSERT-BEGIN:-673678168}
//{CODE-INSERT-END:-673678168}
    }


    public String getOmschr1()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1430504389}
//{CODE-INSERT-END:1430504389}
      return trim(omschr1);
      //Custom code
//{CODE-INSERT-BEGIN:1395960766}
//{CODE-INSERT-END:1395960766}
    }


    public String getOmschr2()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1431427910}
//{CODE-INSERT-END:1431427910}
      return trim(omschr2);
      //Custom code
//{CODE-INSERT-BEGIN:1424589917}
//{CODE-INSERT-END:1424589917}
    }


    public String getVrijeRub1()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-229272282}
//{CODE-INSERT-END:-229272282}
      return vrijeRub1;
      //Custom code
//{CODE-INSERT-BEGIN:1482491517}
//{CODE-INSERT-END:1482491517}
    }


    public String getVrijeRub2()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-228348761}
//{CODE-INSERT-END:-228348761}
      return vrijeRub2;
      //Custom code
//{CODE-INSERT-BEGIN:1511120668}
//{CODE-INSERT-END:1511120668}
    }


    public String getVrijeRub3()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-227425240}
//{CODE-INSERT-END:-227425240}
      return vrijeRub3;
      //Custom code
//{CODE-INSERT-BEGIN:1539749819}
//{CODE-INSERT-END:1539749819}
    }


    public String getVrijeRub4()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-226501719}
//{CODE-INSERT-END:-226501719}
      return vrijeRub4;
      //Custom code
//{CODE-INSERT-BEGIN:1568378970}
//{CODE-INSERT-END:1568378970}
    }


    public String getVrijeRub5()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-225578198}
//{CODE-INSERT-END:-225578198}
      return vrijeRub5;
      //Custom code
//{CODE-INSERT-BEGIN:1597008121}
//{CODE-INSERT-END:1597008121}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setDagboekId(String _dagboekId ) throws Exception {

      if (_dagboekId !=  null)
         _dagboekId = _dagboekId.trim();
      if (_dagboekId !=  null)
         _dagboekId = _dagboekId.toUpperCase();
      if (_dagboekId == null)
          _dagboekId =  "";
      if (_dagboekId.equals(getDagboekId()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-306282608}
//{CODE-INSERT-END:-306282608}
      dagboekId =  _dagboekId;

      autoUpdate();

    }

    public void setDagboekSoort(String _dagboekSoort ) throws Exception {

      if (_dagboekSoort !=  null)
         _dagboekSoort = _dagboekSoort.trim();
      if (_dagboekSoort !=  null)
         _dagboekSoort = _dagboekSoort.toUpperCase();
      if (_dagboekSoort == null)
          _dagboekSoort =  "";
      if (_dagboekSoort.equals(getDagboekSoort()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:515925704}
//{CODE-INSERT-END:515925704}
      dagboekSoort =  _dagboekSoort;

      autoUpdate();

    }

    public void setBoekstuk(String _boekstuk ) throws Exception {

      if (_boekstuk !=  null)
         _boekstuk = _boekstuk.trim();
      if (_boekstuk == null)
          _boekstuk =  "";
      if (_boekstuk.equals(getBoekstuk()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1192405978}
//{CODE-INSERT-END:1192405978}
      boekstuk =  _boekstuk;

      autoUpdate();

    }

    public void setStatus(int _status ) throws Exception {

      if (_status == getStatus())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-591947294}
//{CODE-INSERT-END:-591947294}
      status =  _status;

      autoUpdate();

    }

    public void setFavoriet(boolean _favoriet ) throws Exception {

      if (_favoriet == getFavoriet())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:45435594}
//{CODE-INSERT-END:45435594}
      favoriet =  _favoriet;

      autoUpdate();

    }

    public void setBoekdatum(Date _boekdatum ) throws Exception {

      if (_boekdatum == null)
          _boekdatum =  java.sql.Date.valueOf("2001-01-01");
      if (_boekdatum.equals(getBoekdatum()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:309444084}
//{CODE-INSERT-END:309444084}
      boekdatum =  _boekdatum == null?null: new java.sql.Date( _boekdatum.getTime());

      autoUpdate();

    }

    public void setBoekjaar(int _boekjaar ) throws Exception {

      if (_boekjaar == getBoekjaar())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1856804277}
//{CODE-INSERT-END:-1856804277}
      boekjaar =  _boekjaar;

      autoUpdate();

    }

    public void setBoekperiode(int _boekperiode ) throws Exception {

      if (_boekperiode == getBoekperiode())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1936884105}
//{CODE-INSERT-END:1936884105}
      boekperiode =  _boekperiode;

      autoUpdate();

    }

    public void setDcNummer(String _dcNummer ) throws Exception {

      if (_dcNummer !=  null)
         _dcNummer = _dcNummer.trim();
      if (_dcNummer == null)
          _dcNummer =  "";
      if (_dcNummer.equals(getDcNummer()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:928198755}
//{CODE-INSERT-END:928198755}
      dcNummer =  _dcNummer;

      autoUpdate();

    }

    public void setFactuurNummer(String _factuurNummer ) throws Exception {

      if (_factuurNummer !=  null)
         _factuurNummer = _factuurNummer.trim();
      if (_factuurNummer == null)
          _factuurNummer =  "";
      if (_factuurNummer.equals(getFactuurNummer()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1131593102}
//{CODE-INSERT-END:-1131593102}
      factuurNummer =  _factuurNummer;

      autoUpdate();

    }

    public void setReferentieNummer(String _referentieNummer ) throws Exception {

      if (_referentieNummer !=  null)
         _referentieNummer = _referentieNummer.trim();
      if (_referentieNummer == null)
          _referentieNummer =  "";
      if (_referentieNummer.equals(getReferentieNummer()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1267988309}
//{CODE-INSERT-END:-1267988309}
      referentieNummer =  _referentieNummer;

      autoUpdate();

    }

    public void setFactuurdatum(Date _factuurdatum ) throws Exception {

      if ((_factuurdatum == null ? getFactuurdatum() == null : _factuurdatum.equals(getFactuurdatum())))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-2121232967}
//{CODE-INSERT-END:-2121232967}
      factuurdatum =  _factuurdatum == null?null: new java.sql.Date( _factuurdatum.getTime());

      autoUpdate();

    }

    public void setFactuurLayout(String _factuurLayout ) throws Exception {

      if (_factuurLayout == null)
          _factuurLayout =  "";
      if (_factuurLayout.equals(getFactuurLayout()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1570374136}
//{CODE-INSERT-END:-1570374136}
      factuurLayout =  _factuurLayout;

      autoUpdate();

    }

    public void setVervaldatum(Date _vervaldatum ) throws Exception {

      if ((_vervaldatum == null ? getVervaldatum() == null : _vervaldatum.equals(getVervaldatum())))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-688076183}
//{CODE-INSERT-END:-688076183}
      vervaldatum =  _vervaldatum == null?null: new java.sql.Date( _vervaldatum.getTime());

      autoUpdate();

    }

    public void setAantalAanmaningen(int _aantalAanmaningen ) throws Exception {

      if (_aantalAanmaningen == getAantalAanmaningen())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1195270726}
//{CODE-INSERT-END:1195270726}
      aantalAanmaningen =  _aantalAanmaningen;

      autoUpdate();

    }

    public void setDC(String _dC ) throws Exception {

      if (_dC !=  null)
         _dC = _dC.trim();
      if (_dC !=  null)
         _dC = _dC.toUpperCase();
      if (_dC == null)
          _dC =  "";
      if (_dC.equals(getDC()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:2112241103}
//{CODE-INSERT-END:2112241103}
      dC =  _dC;

      autoUpdate();

    }

    public void setTotaalExcl(BigDecimal _totaalExcl ) throws Exception {

      if (_totaalExcl == null)
          _totaalExcl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_totaalExcl.equals(getTotaalExcl()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1108949825}
//{CODE-INSERT-END:-1108949825}
      totaalExcl =  _totaalExcl;

      autoUpdate();

    }

    public void setTotaalBtw(BigDecimal _totaalBtw ) throws Exception {

      if (_totaalBtw == null)
          _totaalBtw =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_totaalBtw.equals(getTotaalBtw()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1777361258}
//{CODE-INSERT-END:1777361258}
      totaalBtw =  _totaalBtw;

      autoUpdate();

    }

    public void setTotaalIncl(BigDecimal _totaalIncl ) throws Exception {

      if (_totaalIncl == null)
          _totaalIncl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_totaalIncl.equals(getTotaalIncl()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1282222001}
//{CODE-INSERT-END:1282222001}
      totaalIncl =  _totaalIncl;

      autoUpdate();

    }

    public void setTotaalGeboekt(BigDecimal _totaalGeboekt ) throws Exception {

      if (_totaalGeboekt == null)
          _totaalGeboekt =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_totaalGeboekt.equals(getTotaalGeboekt()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-361302392}
//{CODE-INSERT-END:-361302392}
      totaalGeboekt =  _totaalGeboekt;

      autoUpdate();

    }

    public void setTotaalBetaald(BigDecimal _totaalBetaald ) throws Exception {

      if (_totaalBetaald == null)
          _totaalBetaald =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_totaalBetaald.equals(getTotaalBetaald()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-306624978}
//{CODE-INSERT-END:-306624978}
      totaalBetaald =  _totaalBetaald;

      autoUpdate();

    }

    public void setTotaalOpenstaand(BigDecimal _totaalOpenstaand ) throws Exception {

      if (_totaalOpenstaand == null)
          _totaalOpenstaand =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_totaalOpenstaand.equals(getTotaalOpenstaand()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-51567324}
//{CODE-INSERT-END:-51567324}
      totaalOpenstaand =  _totaalOpenstaand;

      autoUpdate();

    }

    public void setTotaalDebet(BigDecimal _totaalDebet ) throws Exception {

      if (_totaalDebet == null)
          _totaalDebet =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_totaalDebet.equals(getTotaalDebet()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1967068597}
//{CODE-INSERT-END:1967068597}
      totaalDebet =  _totaalDebet;

      autoUpdate();

    }

    public void setTotaalCredit(BigDecimal _totaalCredit ) throws Exception {

      if (_totaalCredit == null)
          _totaalCredit =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_totaalCredit.equals(getTotaalCredit()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1226048228}
//{CODE-INSERT-END:-1226048228}
      totaalCredit =  _totaalCredit;

      autoUpdate();

    }

    public void setBeginsaldo(BigDecimal _beginsaldo ) throws Exception {

      if (_beginsaldo == null)
          _beginsaldo =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_beginsaldo.equals(getBeginsaldo()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:664768208}
//{CODE-INSERT-END:664768208}
      beginsaldo =  _beginsaldo;

      autoUpdate();

    }

    public void setEindsaldo(BigDecimal _eindsaldo ) throws Exception {

      if (_eindsaldo == null)
          _eindsaldo =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_eindsaldo.equals(getEindsaldo()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:700683687}
//{CODE-INSERT-END:700683687}
      eindsaldo =  _eindsaldo;

      autoUpdate();

    }

    public void setOmschr1(String _omschr1 ) throws Exception {

      if (_omschr1 !=  null)
         _omschr1 = _omschr1.trim();
      if (_omschr1 == null)
          _omschr1 =  "";
      if (_omschr1.equals(getOmschr1()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1953710383}
//{CODE-INSERT-END:-1953710383}
      omschr1 =  _omschr1;

      autoUpdate();

    }

    public void setOmschr2(String _omschr2 ) throws Exception {

      if (_omschr2 !=  null)
         _omschr2 = _omschr2.trim();
      if (_omschr2 == null)
          _omschr2 =  "";
      if (_omschr2.equals(getOmschr2()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1952786862}
//{CODE-INSERT-END:-1952786862}
      omschr2 =  _omschr2;

      autoUpdate();

    }

    public void setVrijeRub1(String _vrijeRub1 ) throws Exception {

      if (_vrijeRub1 == null)
          _vrijeRub1 =  "";
      if (_vrijeRub1.equals(getVrijeRub1()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1169425102}
//{CODE-INSERT-END:-1169425102}
      vrijeRub1 =  _vrijeRub1;

      autoUpdate();

    }

    public void setVrijeRub2(String _vrijeRub2 ) throws Exception {

      if (_vrijeRub2 == null)
          _vrijeRub2 =  "";
      if (_vrijeRub2.equals(getVrijeRub2()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1168501581}
//{CODE-INSERT-END:-1168501581}
      vrijeRub2 =  _vrijeRub2;

      autoUpdate();

    }

    public void setVrijeRub3(String _vrijeRub3 ) throws Exception {

      if (_vrijeRub3 == null)
          _vrijeRub3 =  "";
      if (_vrijeRub3.equals(getVrijeRub3()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1167578060}
//{CODE-INSERT-END:-1167578060}
      vrijeRub3 =  _vrijeRub3;

      autoUpdate();

    }

    public void setVrijeRub4(String _vrijeRub4 ) throws Exception {

      if (_vrijeRub4 == null)
          _vrijeRub4 =  "";
      if (_vrijeRub4.equals(getVrijeRub4()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1166654539}
//{CODE-INSERT-END:-1166654539}
      vrijeRub4 =  _vrijeRub4;

      autoUpdate();

    }

    public void setVrijeRub5(String _vrijeRub5 ) throws Exception {

      if (_vrijeRub5 == null)
          _vrijeRub5 =  "";
      if (_vrijeRub5.equals(getVrijeRub5()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1165731018}
//{CODE-INSERT-END:-1165731018}
      vrijeRub5 =  _vrijeRub5;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      dagboekId =  "";
      dagboekSoort =  "";
      boekstuk =  "";
      status = 10;
      favoriet =  false;
      boekdatum =  java.sql.Date.valueOf("2001-01-01");
      boekjaar = 0;
      boekperiode = 0;
      dcNummer =  "";
      factuurNummer =  "";
      referentieNummer =  "";
      factuurLayout =  "";
      aantalAanmaningen = 0;
      dC =  "";
      totaalExcl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      totaalBtw =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      totaalIncl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      totaalGeboekt =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      totaalBetaald =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      totaalOpenstaand =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      totaalDebet =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      totaalCredit =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      beginsaldo =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      eindsaldo =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      omschr1 =  "";
      omschr2 =  "";
      vrijeRub1 =  "";
      vrijeRub2 =  "";
      vrijeRub3 =  "";
      vrijeRub4 =  "";
      vrijeRub5 =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.dagboekId = dagboekId;
        currentDBImage.boekstuk = boekstuk;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("HeaderData is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         HeaderDataPK key = new HeaderDataPK();
         key.setBedrijf(getBedrijf());
         key.setDagboekId(getDagboekId());
         key.setBoekstuk(getBoekstuk());
         ((nl.eadmin.db.impl.HeaderDataManager_Impl)nl.eadmin.db.impl.HeaderDataManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((HeaderDataManager_Impl)HeaderDataManager_Impl.getInstance(dbd)).getResultSet((HeaderDataPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("-657057613_"+ dbd.getDBId() + bedrijf + dagboekId + boekstuk).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getHeaderDataDataBean()!
    */
    public HeaderDataDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public HeaderDataDataBean getHeaderDataDataBean() throws Exception {
      return getDataBean(null);
    }

    private HeaderDataDataBean getDataBean(HeaderDataDataBean bean) throws Exception {

      if (bean == null)
          bean = new HeaderDataDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setDagboekId(getDagboekId());
      bean.setDagboekSoort(getDagboekSoort());
      bean.setBoekstuk(getBoekstuk());
      bean.setStatus(getStatus());
      bean.setFavoriet(getFavoriet());
      bean.setBoekdatum(getBoekdatum());
      bean.setBoekjaar(getBoekjaar());
      bean.setBoekperiode(getBoekperiode());
      bean.setDcNummer(getDcNummer());
      bean.setFactuurNummer(getFactuurNummer());
      bean.setReferentieNummer(getReferentieNummer());
      bean.setFactuurdatum(getFactuurdatum());
      bean.setFactuurLayout(getFactuurLayout());
      bean.setVervaldatum(getVervaldatum());
      bean.setAantalAanmaningen(getAantalAanmaningen());
      bean.setDC(getDC());
      bean.setTotaalExcl(getTotaalExcl());
      bean.setTotaalBtw(getTotaalBtw());
      bean.setTotaalIncl(getTotaalIncl());
      bean.setTotaalGeboekt(getTotaalGeboekt());
      bean.setTotaalBetaald(getTotaalBetaald());
      bean.setTotaalOpenstaand(getTotaalOpenstaand());
      bean.setTotaalDebet(getTotaalDebet());
      bean.setTotaalCredit(getTotaalCredit());
      bean.setBeginsaldo(getBeginsaldo());
      bean.setEindsaldo(getEindsaldo());
      bean.setOmschr1(getOmschr1());
      bean.setOmschr2(getOmschr2());
      bean.setVrijeRub1(getVrijeRub1());
      bean.setVrijeRub2(getVrijeRub2());
      bean.setVrijeRub3(getVrijeRub3());
      bean.setVrijeRub4(getVrijeRub4());
      bean.setVrijeRub5(getVrijeRub5());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.HeaderData_Impl))
             return false;
          nl.eadmin.db.impl.HeaderData_Impl bo = (nl.eadmin.db.impl.HeaderData_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getDagboekId() == null){
              if (this.getDagboekId() != null)
                  return false;
          }else if (!bo.getDagboekId().equals(this.getDagboekId())){
              return false;
          }
          if (bo.getBoekstuk() == null){
              if (this.getBoekstuk() != null)
                  return false;
          }else if (!bo.getBoekstuk().equals(this.getBoekstuk())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(HeaderDataDataBean bean) throws Exception {
       set(bean);
    }


    public void set(HeaderDataDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setDagboekId(bean.getDagboekId());
           setDagboekSoort(bean.getDagboekSoort());
           setBoekstuk(bean.getBoekstuk());
           setStatus(bean.getStatus());
           setFavoriet(bean.getFavoriet());
           setBoekdatum(bean.getBoekdatum());
           setBoekjaar(bean.getBoekjaar());
           setBoekperiode(bean.getBoekperiode());
           setDcNummer(bean.getDcNummer());
           setFactuurNummer(bean.getFactuurNummer());
           setReferentieNummer(bean.getReferentieNummer());
           setFactuurdatum(bean.getFactuurdatum());
           setFactuurLayout(bean.getFactuurLayout());
           setVervaldatum(bean.getVervaldatum());
           setAantalAanmaningen(bean.getAantalAanmaningen());
           setDC(bean.getDC());
           setTotaalExcl(bean.getTotaalExcl());
           setTotaalBtw(bean.getTotaalBtw());
           setTotaalIncl(bean.getTotaalIncl());
           setTotaalGeboekt(bean.getTotaalGeboekt());
           setTotaalBetaald(bean.getTotaalBetaald());
           setTotaalOpenstaand(bean.getTotaalOpenstaand());
           setTotaalDebet(bean.getTotaalDebet());
           setTotaalCredit(bean.getTotaalCredit());
           setBeginsaldo(bean.getBeginsaldo());
           setEindsaldo(bean.getEindsaldo());
           setOmschr1(bean.getOmschr1());
           setOmschr2(bean.getOmschr2());
           setVrijeRub1(bean.getVrijeRub1());
           setVrijeRub2(bean.getVrijeRub2());
           setVrijeRub3(bean.getVrijeRub3());
           setVrijeRub4(bean.getVrijeRub4());
           setVrijeRub5(bean.getVrijeRub5());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"HeaderData is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.HeaderData.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("DagboekId",provider) +  " = ? " + ", " + pmd.getFieldName("DagboekSoort",provider) +  " = ? " + ", " + pmd.getFieldName("Boekstuk",provider) +  " = ? " + ", " + pmd.getFieldName("Status",provider) +  " = ? " + ", " + pmd.getFieldName("Favoriet",provider) +  " = ? " + ", " + pmd.getFieldName("Boekdatum",provider) +  " = ? " + ", " + pmd.getFieldName("Boekjaar",provider) +  " = ? " + ", " + pmd.getFieldName("Boekperiode",provider) +  " = ? " + ", " + pmd.getFieldName("DcNummer",provider) +  " = ? " + ", " + pmd.getFieldName("FactuurNummer",provider) +  " = ? " + ", " + pmd.getFieldName("ReferentieNummer",provider) +  " = ? " + ", " + pmd.getFieldName("Factuurdatum",provider) +  " = ? " + ", " + pmd.getFieldName("FactuurLayout",provider) +  " = ? " + ", " + pmd.getFieldName("Vervaldatum",provider) +  " = ? " + ", " + pmd.getFieldName("AantalAanmaningen",provider) +  " = ? " + ", " + pmd.getFieldName("DC",provider) +  " = ? " + ", " + pmd.getFieldName("TotaalExcl",provider) +  " = ? " + ", " + pmd.getFieldName("TotaalBtw",provider) +  " = ? " + ", " + pmd.getFieldName("TotaalIncl",provider) +  " = ? " + ", " + pmd.getFieldName("TotaalGeboekt",provider) +  " = ? " + ", " + pmd.getFieldName("TotaalBetaald",provider) +  " = ? " + ", " + pmd.getFieldName("TotaalOpenstaand",provider) +  " = ? " + ", " + pmd.getFieldName("TotaalDebet",provider) +  " = ? " + ", " + pmd.getFieldName("TotaalCredit",provider) +  " = ? " + ", " + pmd.getFieldName("Beginsaldo",provider) +  " = ? " + ", " + pmd.getFieldName("Eindsaldo",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr1",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr2",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub1",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub2",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub3",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub4",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub5",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("DagboekId",provider) + " = ? " + " AND  " +  pmd.getFieldName("Boekstuk",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dagboekId,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dagboekSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  boekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( status),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  new Boolean( favoriet),  mapping.getJDBCTypeFor("boolean"));
        prep.setObject(parmIndex++,  boekdatum,  mapping.getJDBCTypeFor("Date"));
        prep.setObject(parmIndex++,  new Integer( boekjaar),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  new Integer( boekperiode),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  dcNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  factuurNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  referentieNummer,  mapping.getJDBCTypeFor("String"));
        if ( factuurdatum == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("Date"));
       } else {
          prep.setObject(parmIndex++,  factuurdatum,  mapping.getJDBCTypeFor("Date"));
}
        prep.setObject(parmIndex++,  factuurLayout,  mapping.getJDBCTypeFor("String"));
        if ( vervaldatum == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("Date"));
       } else {
          prep.setObject(parmIndex++,  vervaldatum,  mapping.getJDBCTypeFor("Date"));
}
        prep.setObject(parmIndex++,  new Integer( aantalAanmaningen),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  dC,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  totaalExcl,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  totaalBtw,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  totaalIncl,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  totaalGeboekt,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  totaalBetaald,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  totaalOpenstaand,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  totaalDebet,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  totaalCredit,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  beginsaldo,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  eindsaldo,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  omschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub3,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub4,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  vrijeRub5,  mapping.getJDBCTypeFor("String"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.dagboekId,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.boekstuk,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("HeaderData modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.HeaderData.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.HeaderData.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("DagboekId",provider) + ", " + pmd.getFieldName("DagboekSoort",provider) + ", " + pmd.getFieldName("Boekstuk",provider) + ", " + pmd.getFieldName("Status",provider) + ", " + pmd.getFieldName("Favoriet",provider) + ", " + pmd.getFieldName("Boekdatum",provider) + ", " + pmd.getFieldName("Boekjaar",provider) + ", " + pmd.getFieldName("Boekperiode",provider) + ", " + pmd.getFieldName("DcNummer",provider) + ", " + pmd.getFieldName("FactuurNummer",provider) + ", " + pmd.getFieldName("ReferentieNummer",provider) + ", " + pmd.getFieldName("Factuurdatum",provider) + ", " + pmd.getFieldName("FactuurLayout",provider) + ", " + pmd.getFieldName("Vervaldatum",provider) + ", " + pmd.getFieldName("AantalAanmaningen",provider) + ", " + pmd.getFieldName("DC",provider) + ", " + pmd.getFieldName("TotaalExcl",provider) + ", " + pmd.getFieldName("TotaalBtw",provider) + ", " + pmd.getFieldName("TotaalIncl",provider) + ", " + pmd.getFieldName("TotaalGeboekt",provider) + ", " + pmd.getFieldName("TotaalBetaald",provider) + ", " + pmd.getFieldName("TotaalOpenstaand",provider) + ", " + pmd.getFieldName("TotaalDebet",provider) + ", " + pmd.getFieldName("TotaalCredit",provider) + ", " + pmd.getFieldName("Beginsaldo",provider) + ", " + pmd.getFieldName("Eindsaldo",provider) + ", " + pmd.getFieldName("Omschr1",provider) + ", " + pmd.getFieldName("Omschr2",provider) + ", " + pmd.getFieldName("VrijeRub1",provider) + ", " + pmd.getFieldName("VrijeRub2",provider) + ", " + pmd.getFieldName("VrijeRub3",provider) + ", " + pmd.getFieldName("VrijeRub4",provider) + ", " + pmd.getFieldName("VrijeRub5",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, dagboekId,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, dagboekSoort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5, boekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6,  new Integer(status),  mapping.getJDBCTypeFor("int"));
        prep.setObject(7,  new Boolean(favoriet),  mapping.getJDBCTypeFor("boolean"));
        prep.setObject(8, boekdatum,  mapping.getJDBCTypeFor("Date"));
        prep.setObject(9,  new Integer(boekjaar),  mapping.getJDBCTypeFor("int"));
        prep.setObject(10,  new Integer(boekperiode),  mapping.getJDBCTypeFor("int"));
        prep.setObject(11, dcNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(12, factuurNummer,  mapping.getJDBCTypeFor("String"));
        prep.setObject(13, referentieNummer,  mapping.getJDBCTypeFor("String"));
        if (factuurdatum == null)
          prep.setNull(14,  mapping.getJDBCTypeFor("Date"));
        else
          prep.setObject(14, factuurdatum,  mapping.getJDBCTypeFor("Date"));
        prep.setObject(15, factuurLayout,  mapping.getJDBCTypeFor("String"));
        if (vervaldatum == null)
          prep.setNull(16,  mapping.getJDBCTypeFor("Date"));
        else
          prep.setObject(16, vervaldatum,  mapping.getJDBCTypeFor("Date"));
        prep.setObject(17,  new Integer(aantalAanmaningen),  mapping.getJDBCTypeFor("int"));
        prep.setObject(18, dC,  mapping.getJDBCTypeFor("String"));
        prep.setObject(19, totaalExcl,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(20, totaalBtw,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(21, totaalIncl,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(22, totaalGeboekt,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(23, totaalBetaald,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(24, totaalOpenstaand,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(25, totaalDebet,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(26, totaalCredit,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(27, beginsaldo,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(28, eindsaldo,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(29, omschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(30, omschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(31, vrijeRub1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(32, vrijeRub2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(33, vrijeRub3,  mapping.getJDBCTypeFor("String"));
        prep.setObject(34, vrijeRub4,  mapping.getJDBCTypeFor("String"));
        prep.setObject(35, vrijeRub5,  mapping.getJDBCTypeFor("String"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.dagboekId = currentDBImage.dagboekId;
        transactionImage.dbImage.boekstuk = currentDBImage.boekstuk;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.dagboekId = transactionImage.dbImage.dagboekId;
      currentDBImage.boekstuk = transactionImage.dbImage.boekstuk;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        dagboekId = transactionImage.dbImage.dagboekId;
        boekstuk = transactionImage.dbImage.boekstuk;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String dagboekId;
      private String boekstuk;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
	public Dagboek getDagboekObject() throws Exception {
		if (dagboek == null) {
			DagboekPK key = new DagboekPK();
			key.setBedrijf(bedrijf);
			key.setId(dagboekId);
			try {
				dagboek = DagboekManagerFactory.getInstance(dbd).findByPrimaryKey(key);
			} catch (Exception e) {
			}
		}
		return dagboek;
	}

	public Collection<?> getDetails() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM DetailData WHERE ");
		sb.append(DetailData.BEDRIJF + "='" + bedrijf + "' AND ");
		sb.append(DetailData.DAGBOEK_ID + "='" + dagboekId + "' AND ");
		sb.append(DetailData.BOEKSTUK + "='" + boekstuk + "' ");
		sb.append(" ORDER BY " + DetailData.BOEKSTUK_REGEL + " ascending");
		FreeQuery qry = QueryFactory.createFreeQuery((DetailDataManager_Impl) DetailDataManagerFactory.getInstance(dbd), sb.toString());
		return qry.getCollection();
	}


	public Collection<?> getBijlagen() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM Bijlage WHERE ");
		sb.append(Bijlage.BEDRIJF + "='" + bedrijf + "' AND ");
		sb.append(Bijlage.DAGBOEK_ID + "='" + dagboekId + "' AND ");
		sb.append(Bijlage.BOEKSTUK + "='" + boekstuk + "' ");
		sb.append(" ORDER BY " + Bijlage.VOLG_NR + " ascending");
		FreeQuery qry = QueryFactory.createFreeQuery((BijlageManager_Impl) BijlageManagerFactory.getInstance(dbd), sb.toString());
		return qry.getCollection();
	}

	public boolean isManualBank() throws Exception {
		return getDagboekSoort().equals(DagboekSoortEnum.BANK) && getBeginsaldo().doubleValue() != 0d; 
	}

    public int getAantalDagenVervallen() throws Exception {
    	if (vervaldatum == null) {
    		return 0;
    	} else {
			long today = Calendar.getInstance().getTime().getTime();
			long aanmg = vervaldatum.getTime();
			return Math.max(0, new Long(TimeUnit.MILLISECONDS.toDays(today - aanmg)).intValue());
    	}
    }

	public void recalculate() throws Exception {
		BigDecimal ZERO = new BigDecimal(0.00);
		BigDecimal totlExcl = ZERO;
		BigDecimal totlBtw = ZERO;
		BigDecimal totlIncl = ZERO;
		BigDecimal totlDebt = ZERO;
		BigDecimal totlCred = ZERO;
		Dagboek dagboek = getDagboekObject();
		boolean isMemoriaal = dagboek.isMemoriaal();
		DetailData detail;
		Iterator<?> iter = getDetails().iterator();
		while (iter.hasNext()) {
			detail = (DetailData) iter.next();
			if (detail.getDC().equals("D")) {
				totlDebt = totlDebt.add(detail.getBedragIncl()); 
			} else {
				totlCred = totlCred.add(detail.getBedragIncl()); 
			}
			if (!isMemoriaal) {
				if (dC.equalsIgnoreCase(detail.getDC())) {
					totlExcl = totlExcl.subtract(detail.getBedragExcl());
					totlBtw = totlBtw.subtract(detail.getBedragBtw());
					totlIncl = totlIncl.subtract(detail.getBedragIncl());
				} else {
					totlExcl = totlExcl.add(detail.getBedragExcl());
					totlBtw = totlBtw.add(detail.getBedragBtw());
					totlIncl = totlIncl.add(detail.getBedragIncl());
				}
			}
		}

		setTotaalExcl(totlExcl);
		setTotaalBtw(totlBtw);
		setTotaalIncl(totlIncl);
		setTotaalOpenstaand(getTotaalGeboekt().subtract(getTotaalBetaald()));
		if (dagboekSoort.equals(DagboekSoortEnum.BANK)) {
			setTotaalDebet(dC.equals("C") ? getTotaalGeboekt() : ZERO);
			setTotaalCredit(dC.equals("C") ? ZERO : getTotaalGeboekt());
		} else {
			setTotaalDebet(totlDebt);
			setTotaalCredit(totlCred);
		}

		BigDecimal amount1, amount2;
		if (isMemoriaal) {
			setTotaalGeboekt(ZERO);
			amount1 = totlDebt;
			amount2 = totlCred;
		} else {
			amount1 = totlIncl;
			amount2 = totaalGeboekt;
		}
		if (amount1.doubleValue() == amount2.doubleValue()) {
			setStatus(GeneralHelper.getClosedStatus(dagboek));
			new JournaalHelper(dbd).createJournals(this, true);
		} else {
			setStatus(GeneralHelper.getOpenStatus(dagboek));
			new JournaalHelper(dbd).deleteJournals(this);
		}
	}   
	
	public void recalculateBedragBetaald() throws Exception {
		BigDecimal bedrBetaald = ApplicationConstants.ZERO;
		Dagboek dagboek = getDagboekObject();
		if (dagboek.isInkoopboek() || dagboek.isVerkoopboek()) {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT SUM(BEDRAGINCL) FROM DetailData WHERE ");
			sb.append(DetailData.BEDRIJF + "='" + bedrijf + "' AND ");
			sb.append(DetailData.DAGBOEK_ID + "<>'" + dagboekId + "' AND ");
			sb.append(DetailData.FACTUUR_NUMMER + "='" + factuurNummer + "' AND ");
			sb.append(DetailData.DC_NUMMER + "='" + dcNummer + "' AND ");
			sb.append(DetailData.DC + "='" + (dC.equals("C") ? "D" : "C") + "' ");
			FreeQuery qry = QueryFactory.createFreeQuery((DetailDataManager_Impl) DetailDataManagerFactory.getInstance(dbd), sb.toString());
			Object[][] maxRslt = qry.getResultArray();
			if (maxRslt.length > 0 && maxRslt[0].length > 0) {
				bedrBetaald = (maxRslt[0][0] == null) ? ApplicationConstants.ZERO : ((BigDecimal) maxRslt[0][0]);
			} else {
				bedrBetaald = ApplicationConstants.ZERO;
			}
			setTotaalBetaald(bedrBetaald);
			setTotaalOpenstaand(getTotaalGeboekt().subtract(getTotaalBetaald()));
		}
	}
//{CODE-INSERT-END:955534258}
}
