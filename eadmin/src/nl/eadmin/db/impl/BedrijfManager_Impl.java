package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import nl.eadmin.db.*;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


  public class BedrijfManager_Impl extends BusinessObjectManager implements BedrijfManager{


    

    private final static String APPLICATION ="eadmin";
    private static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    private static AttributeAccessor generalAccessor = new AttributeAccessor();
    private static boolean verbose = Log.debug();
    private static PersistenceMetaData pmd = PersistenceMetaDataBedrijf.getInstance();
    private static Hashtable instances = new Hashtable();

    private DBData  dbd;

    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    private BedrijfManager_Impl(DBData dbData){
       dbd = dbData;
    }


    public static BedrijfManager getInstance(){
		return getInstance(null);
    }


    public static BedrijfManager getInstance(DBData dbd){
		BedrijfManager_Impl inst = (BedrijfManager_Impl)instances.get(dbd==null?"":dbd);
		if (inst == null){
		  inst = new BedrijfManager_Impl(dbd);
		  instances.put(dbd==null?"":dbd,inst);
		}
		return inst;
    }


    public DBData getDBData(){
       return dbd==null?DBData.getDefaultDBData(APPLICATION):dbd;
    }


    public Bedrijf create(BedrijfDataBean bean)  throws Exception {
      Bedrijf_Impl impl = new Bedrijf_Impl(getDBData(),  bean);
        impl.assureStorage();
      return (Bedrijf)impl;
    }


    public Bedrijf create(String _bedrijfscode) throws Exception {
      Bedrijf_Impl obj = new Bedrijf_Impl(getDBData(), _bedrijfscode);
      obj.assureStorage();
      return obj;
    }


    private String getPK(ResultSet rs) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      String inst =  rs.getString( pmd.getFieldName("Bedrijfscode",provider));
      return inst; 
    }


    public Bedrijf findOrCreate (String _bedrijfscode) throws Exception {

      Bedrijf obj;
      try{
         obj = findByPrimaryKey( _bedrijfscode);
      }catch (FinderException e){
         obj = create( _bedrijfscode);
      }

      return obj;
    }


    public void add(BedrijfDataBean inst)  throws Exception {
        Bedrijf_Impl impl = new Bedrijf_Impl(getDBData(),  inst);
        impl.assureStorage();
    }


    public void removeByPrimaryKey (String _bedrijfscode) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      Bedrijf cachedObject = (Bedrijf)getCachedObjectByPrimaryKey( _bedrijfscode);
      if (provider instanceof AtomicDBTransaction)
        dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate((BusinessObject_Impl)cachedObject);
      else
        dbc = provider.getConnection();
      String  pskey = "nl.eadmin.db.Bedrijf.removeByPrimaryKey";
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider) + 
                                           " WHERE "  + pmd.getFieldName("Bedrijfscode",provider) + "=?" );
          prep.setString(1, _bedrijfscode);
          int x = prep.executeUpdate();

          if (x == 0) throw new RemoveException("Key not found");
          if (cachedObject != null){
          		DBPersistenceManager.removeFromCache(cachedObject);
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
    }


    public Bedrijf findByPrimaryKey (String _bedrijfscode) throws Exception {
      //Custom code
//{CODE-INSERT-BEGIN:-1586979883}
//{CODE-INSERT-END:-1586979883}

      Bedrijf bedrijf = getCachedObjectByPrimaryKey(_bedrijfscode);
       if (bedrijf != null)
      	  return bedrijf;
       ResultSet rs = null;
       try{
           rs = getResultSet(_bedrijfscode);
           return (Bedrijf)getBusinessObject(rs);
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }      //Custom code
//{CODE-INSERT-BEGIN:-933211338}
//{CODE-INSERT-END:-933211338}

    }

    public ResultSet getResultSet(String _bedrijfscode) throws Exception{
      ResultSet rs = null;
      String  pskey = "nl.eadmin.db.Bedrijf.findByPrimaryKey1";
      Object inst = null;
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " SELECT * " + 
                                           " FROM " + provider.getPrefix() + pmd.getTableName(provider)  + 
                                           " WHERE "  + pmd.getFieldName("Bedrijfscode",provider) + "=?" );
          prep.setString(1, _bedrijfscode);
          prep.executeQuery();

          rs = prep.getResultSet();

          if (!rs.next()){
                throw new FinderException("Key not found");
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
      return rs;
    }
    public Bedrijf getCachedObjectByPrimaryKey (String _bedrijfscode) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache == null)
          return null;
       Object o = cache.get(new String("1073670744_"+ getDBData().getDBId() + _bedrijfscode));
       return (Bedrijf)o;
    }



    public ConnectionProvider getConnectionProvider() throws Exception{
       return DBPersistenceManager.getConnectionProvider(getDBData());
    }


    public PersistenceMetaData getPersistenceMetaData(){
      return pmd;
    }


    public int generalUpdate(String setClause, String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String update = "UPDATE " + provider.getPrefix() + pmd.getTableName(provider)  + " SET " + QueryTranslator.translateObjectQuery(setClause + " WHERE " + whereClause,pmd,provider);
        return dbc.executeUpdate(update);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public int generalDelete(String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String delete = "DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider)  + " WHERE " + QueryTranslator.translateObjectQuery(whereClause,pmd,provider);
        return dbc.executeUpdate(delete);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public Bedrijf getFirstObject(Query query) throws Exception{
      if (query == null)
         query = QueryFactory.create();
      int prevMax = query.getMaxObjects();
      query.setMaxObjects(1);
      try{
         return (Bedrijf) ((QueryImplementation)query).execute(this, RETURN_FIRST_OBJECT);
      }catch(Exception e){
         throw e;
      }finally{
         query.setMaxObjects(prevMax);
      }
    }

    public Collection getCollection(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Collection) queryImpl.execute(this, RETURN_COLLECTION);
    }

    public ListIterator getListIterator(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ListIterator) queryImpl.execute(this, RETURN_LISTITERATOR);
    }

    public Document getDocument(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Document) queryImpl.execute(this, RETURN_DOCUMENT);
    }

    public DocumentFragment getDocumentFragment(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (DocumentFragment) queryImpl.execute(this, RETURN_DOCUMENT_FRAGMENT);
    }

    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ArrayList) queryImpl.execute(this,RETURN_DOCUMENT_FRAGMENT_ARRAYLIST );
    }



    protected Collection getCollection(ResultSet rs) throws Exception {
      ArrayList result = new ArrayListImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      return (Collection)result;
    }



    protected Document getDocument(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      Element root = document.getDocumentElement();
      collectResultElements(resultSet, root, getConnectionProvider());
      return document;
    };


    protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      DocumentFragment documentFragment = document.createDocumentFragment();
      collectResultElements(resultSet, documentFragment, getConnectionProvider());
      return documentFragment;
    };


    protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      Document document = domImplementation.createDocument("", "data", null);
      ArrayList arrayList = new ArrayListImpl();
      while (resultSet.next()){
        DocumentFragment documentFragment = document.createDocumentFragment();
        Node rec = documentFragment.appendChild(document.createElement("bedrijf"));
        addBOFieldsToElement(resultSet, rec, provider);
        arrayList.add(documentFragment);
      }
      return arrayList;
    }


    protected ListIterator getListIterator(ResultSet rs) throws Exception {
      ListIterator result = new ListIteratorImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      ((ListIteratorImpl)result).pointer = 0; 
      return result; 
    }


    protected Object getBusinessObject(ResultSet rs) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache != null){
          ConnectionProvider provider = getConnectionProvider();
          Object o = cache.get(new String("1073670744_"+ getDBData().getDBId()+rs.getObject(pmd.getFieldName("Bedrijfscode",provider))));
          if (o != null)
             return o;
       }
    	  return DBPersistenceManager.cache(createBusinessObject(rs));
    }

    private Object createBusinessObject(ResultSet rs) throws Exception {
      return new Bedrijf_Impl(getDBData(), rs);
    }


    private void collectResultElements(ResultSet rs, Node root, ConnectionProvider provider) throws Exception {
      Document doc = root.getOwnerDocument();
      while (rs.next()){
        Node rec = root.appendChild(doc.createElement("bedrijf"));
        addBOFieldsToElement(rs,rec,provider);
      }
    }


    public static void addBOFieldsToElement(ResultSet rs, Node rec, ConnectionProvider provider) throws Exception {
      DBMapping mapping = provider.getDBMapping();
      Document doc = rec.getOwnerDocument();
      Object o = null;
      String s = null;
      if ((o=rs.getObject(pmd.getFieldName("mutationTimeStamp",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("mutationtimestamp")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Bedrijfscode",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrijfscode")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Omschrijving",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschrijving")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Email",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("email")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("KvkNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("kvknr")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("BtwNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("btwnr")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("IbanNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("ibannr")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Bic",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bic")).appendChild(doc.createCDATASection(s));
    }

    public nl.ibs.vegas.ExecutableQuery getExecutableQuery() {
    		return QueryFactory.create(this);
    }

    final static String[] keys = new String[]{ "bedrijfscode"};
    public String[] getKeyNames() {
    	return keys;
    }
    final static nl.ibs.vegas.meta.AttributeMeta[] attributes = new nl.ibs.vegas.meta.AttributeMeta[]{
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrijfscode","bedrijfscode","bedrijfscode-short","bedrijfscode-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschrijving","omschrijving","omschrijving-short","omschrijving-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("email","email","email-short","email-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("kvkNr","kvk-nr","kvk-nr-short","kvk-nr-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("btwNr","btw-nr","btw-nr-short","btw-nr-description","text",String.class,18,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("ibanNr","iban-nr","iban-nr-short","iban-nr-description","text",String.class,18,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bic","bic","bic-short","bic-description","text",String.class,15,0,null,"false","false","false","upper", false, false,null,null)
    };
    public nl.ibs.vegas.meta.AttributeMeta[] getAttributes() {
    	return attributes;
    }
    public Class getClassType() {
    	return nl.eadmin.db.Bedrijf.class;
    }
    public String getNameField() {
    	return "bedrijfscode";
    }
    public String getDescriptionField() {
    	return "bedrijfscode";
    }
    public String getTextKey() {
    	return "bedrijf";
    }
    public String getTextKeyShort() {
    	return "bedrijf-short";
    }
    public String getTextKeyDescription() {
    	return "bedrijf-description";
    }
    public nl.ibs.vegas.meta.AttributeMeta getAttribute(String name) {
    	if (attributes == null || name == null)
    		return null;
    	for (int i = 0; i < attributes.length; i++)
    		if (name.equals(attributes[i].getName()))
    			return attributes[i];
    	return null;
    }
  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
