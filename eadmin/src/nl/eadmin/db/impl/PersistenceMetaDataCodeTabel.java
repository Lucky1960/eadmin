package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataCodeTabel implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataCodeTabel();

      persistables.add("nl.eadmin.db.impl.CodeTabel_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("tabelNaam");
      javaVariableNames.add("omschrijving");
      javaVariableNames.add("dataType");
      javaVariableNames.add("lengte");
      javaVariableNames.add("decimalen");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("tabelNaam");

      fieldNames.put("mutationTimeStamp","CODETBL_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("TabelNaam","TABELNAAM");
      fieldNames.put("Omschrijving","OMSCHR");
      fieldNames.put("DataType","TYPE");
      fieldNames.put("Lengte","LENGTE");
      fieldNames.put("Decimalen","DECIMALEN");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("TabelNaam","Tabelnaam");
      fieldDescriptions.put("Omschrijving","Omschrijving codetabel");
      fieldDescriptions.put("DataType","Datatype van de codes");
      fieldDescriptions.put("Lengte","Lengte van de codes");
      fieldDescriptions.put("Decimalen","Decimalen");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("tabelNaam","TabelNaam");
      javaToBONameMapping.put("omschrijving","Omschrijving");
      javaToBONameMapping.put("dataType","DataType");
      javaToBONameMapping.put("lengte","Lengte");
      javaToBONameMapping.put("decimalen","Decimalen");

    }

    private PersistenceMetaDataCodeTabel(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "CodeTabel";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("CodeTabel" , "CODETBL");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "CodeTabel" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Codetabel voor vrije rubrieken";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("TabelNaam", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschrijving", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DataType", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Lengte", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 3, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Decimalen", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 1, 0) + " DEFAULT 0" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[7][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("TabelNaam", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Tabelnaam"};
      columnDefinitions[3]=new String[]{getFieldName("Omschrijving", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving codetabel"};
      columnDefinitions[4]=new String[]{getFieldName("DataType", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "Datatype van de codes"};
      columnDefinitions[5]=new String[]{getFieldName("Lengte", provider), provider.getDBMapping().getSQLDefinition("int", 3, 0) , "0"  , " NOT NULL", "Lengte van de codes"};
      columnDefinitions[6]=new String[]{getFieldName("Decimalen", provider), provider.getDBMapping().getSQLDefinition("int", 1, 0) , "0"  , " NOT NULL", "Decimalen"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_CODETABEL_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_CODETABEL_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("TabelNaam", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_CODETABEL", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_CODETABEL PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("TabelNaam", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
