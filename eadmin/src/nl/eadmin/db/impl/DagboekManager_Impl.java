package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import nl.eadmin.db.*;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


  public class DagboekManager_Impl extends BusinessObjectManager implements DagboekManager{


    

    private final static String APPLICATION ="eadmin";
    private static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    private static AttributeAccessor generalAccessor = new AttributeAccessor();
    private static boolean verbose = Log.debug();
    private static PersistenceMetaData pmd = PersistenceMetaDataDagboek.getInstance();
    private static Hashtable instances = new Hashtable();

    private DBData  dbd;

    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    private DagboekManager_Impl(DBData dbData){
       dbd = dbData;
    }


    public static DagboekManager getInstance(){
		return getInstance(null);
    }


    public static DagboekManager getInstance(DBData dbd){
		DagboekManager_Impl inst = (DagboekManager_Impl)instances.get(dbd==null?"":dbd);
		if (inst == null){
		  inst = new DagboekManager_Impl(dbd);
		  instances.put(dbd==null?"":dbd,inst);
		}
		return inst;
    }


    public DBData getDBData(){
       return dbd==null?DBData.getDefaultDBData(APPLICATION):dbd;
    }


    public Dagboek create(DagboekDataBean bean)  throws Exception {
      Dagboek_Impl impl = new Dagboek_Impl(getDBData(),  bean);
        impl.assureStorage();
      return (Dagboek)impl;
    }


    public Dagboek create(String _bedrijf, String _id) throws Exception {
      Dagboek_Impl obj = new Dagboek_Impl(getDBData(), _bedrijf, _id);
      obj.assureStorage();
      return obj;
    }


    private DagboekPK getPK(ResultSet rs) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DagboekPK inst =  new DagboekPK();
      inst.setBedrijf(rs.getString(pmd.getFieldName("Bedrijf",provider)));
      inst.setId(rs.getString(pmd.getFieldName("Id",provider)));
      return inst; 
    }


    public Dagboek findOrCreate (DagboekPK key) throws Exception {

      Dagboek obj;
      try{
         obj = findByPrimaryKey( key);
      }catch (FinderException e){
         obj = create( key.getBedrijf (),  key.getId ());
      }

      return obj;
    }


    public void add(DagboekDataBean inst)  throws Exception {
        Dagboek_Impl impl = new Dagboek_Impl(getDBData(),  inst);
        impl.assureStorage();
    }


    public void removeByPrimaryKey (DagboekPK key) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      Dagboek cachedObject = (Dagboek)getCachedObjectByPrimaryKey(key);
      if (provider instanceof AtomicDBTransaction)
        dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate((BusinessObject_Impl)cachedObject);
      else
        dbc = provider.getConnection();
      String  pskey = "nl.eadmin.db.Dagboek.removeByPrimaryKey";
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider) + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("Id",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getId());
          int x = prep.executeUpdate();

          if (x == 0) throw new RemoveException("Key not found");
          if (cachedObject != null){
          		DBPersistenceManager.removeFromCache(cachedObject);
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
    }


    public Dagboek findByPrimaryKey (DagboekPK key) throws Exception {
      //Custom code
//{CODE-INSERT-BEGIN:-1586979883}
//{CODE-INSERT-END:-1586979883}

      Dagboek dagboek = getCachedObjectByPrimaryKey(key);
       if (dagboek != null)
      	  return dagboek;
       ResultSet rs = null;
       try{
           rs = getResultSet(key);
           return (Dagboek)getBusinessObject(rs);
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }      //Custom code
//{CODE-INSERT-BEGIN:-933211338}
//{CODE-INSERT-END:-933211338}

    }

    public ResultSet getResultSet(DagboekPK key) throws Exception{
      ResultSet rs = null;
      String  pskey = "nl.eadmin.db.Dagboek.findByPrimaryKey1";
      Object inst = null;
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " SELECT * " + 
                                           " FROM " + provider.getPrefix() + pmd.getTableName(provider)  + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("Id",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getId());
          prep.executeQuery();

          rs = prep.getResultSet();

          if (!rs.next()){
                throw new FinderException("Key not found");
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
      return rs;
    }
    public Dagboek getCachedObjectByPrimaryKey (DagboekPK key) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache == null)
          return null;
       Object o = cache.get(new String("-1558506271_"+ getDBData().getDBId() + key.getBedrijf() + key.getId()));
       return (Dagboek)o;
    }



    public ConnectionProvider getConnectionProvider() throws Exception{
       return DBPersistenceManager.getConnectionProvider(getDBData());
    }


    public PersistenceMetaData getPersistenceMetaData(){
      return pmd;
    }


    public int generalUpdate(String setClause, String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String update = "UPDATE " + provider.getPrefix() + pmd.getTableName(provider)  + " SET " + QueryTranslator.translateObjectQuery(setClause + " WHERE " + whereClause,pmd,provider);
        return dbc.executeUpdate(update);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public int generalDelete(String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String delete = "DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider)  + " WHERE " + QueryTranslator.translateObjectQuery(whereClause,pmd,provider);
        return dbc.executeUpdate(delete);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public Dagboek getFirstObject(Query query) throws Exception{
      if (query == null)
         query = QueryFactory.create();
      int prevMax = query.getMaxObjects();
      query.setMaxObjects(1);
      try{
         return (Dagboek) ((QueryImplementation)query).execute(this, RETURN_FIRST_OBJECT);
      }catch(Exception e){
         throw e;
      }finally{
         query.setMaxObjects(prevMax);
      }
    }

    public Collection getCollection(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Collection) queryImpl.execute(this, RETURN_COLLECTION);
    }

    public ListIterator getListIterator(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ListIterator) queryImpl.execute(this, RETURN_LISTITERATOR);
    }

    public Document getDocument(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Document) queryImpl.execute(this, RETURN_DOCUMENT);
    }

    public DocumentFragment getDocumentFragment(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (DocumentFragment) queryImpl.execute(this, RETURN_DOCUMENT_FRAGMENT);
    }

    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ArrayList) queryImpl.execute(this,RETURN_DOCUMENT_FRAGMENT_ARRAYLIST );
    }



    protected Collection getCollection(ResultSet rs) throws Exception {
      ArrayList result = new ArrayListImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      return (Collection)result;
    }



    protected Document getDocument(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      Element root = document.getDocumentElement();
      collectResultElements(resultSet, root, getConnectionProvider());
      return document;
    };


    protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      DocumentFragment documentFragment = document.createDocumentFragment();
      collectResultElements(resultSet, documentFragment, getConnectionProvider());
      return documentFragment;
    };


    protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      Document document = domImplementation.createDocument("", "data", null);
      ArrayList arrayList = new ArrayListImpl();
      while (resultSet.next()){
        DocumentFragment documentFragment = document.createDocumentFragment();
        Node rec = documentFragment.appendChild(document.createElement("dagboek"));
        addBOFieldsToElement(resultSet, rec, provider);
        arrayList.add(documentFragment);
      }
      return arrayList;
    }


    protected ListIterator getListIterator(ResultSet rs) throws Exception {
      ListIterator result = new ListIteratorImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      ((ListIteratorImpl)result).pointer = 0; 
      return result; 
    }


    protected Object getBusinessObject(ResultSet rs) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache != null){
          ConnectionProvider provider = getConnectionProvider();
          Object o = cache.get(new String("-1558506271_"+ getDBData().getDBId()+rs.getObject(pmd.getFieldName("Bedrijf",provider))+rs.getObject(pmd.getFieldName("Id",provider))));
          if (o != null)
             return o;
       }
    	  return DBPersistenceManager.cache(createBusinessObject(rs));
    }

    private Object createBusinessObject(ResultSet rs) throws Exception {
      return new Dagboek_Impl(getDBData(), rs);
    }


    private void collectResultElements(ResultSet rs, Node root, ConnectionProvider provider) throws Exception {
      Document doc = root.getOwnerDocument();
      while (rs.next()){
        Node rec = root.appendChild(doc.createElement("dagboek"));
        addBOFieldsToElement(rs,rec,provider);
      }
    }


    public static void addBOFieldsToElement(ResultSet rs, Node rec, ConnectionProvider provider) throws Exception {
      DBMapping mapping = provider.getDBMapping();
      Document doc = rec.getOwnerDocument();
      Object o = null;
      String s = null;
      if ((o=rs.getObject(pmd.getFieldName("mutationTimeStamp",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("mutationtimestamp")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Bedrijf",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrijf")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Id",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("id")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Soort",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("soort")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Omschrijving",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("omschrijving")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("BoekstukMask",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("boekstukmask")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("LaatsteBoekstuk",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("laatsteboekstuk")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Rekening",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("rekening")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("TegenRekening",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("tegenrekening")).appendChild(doc.createCDATASection(s));
      if ((o=rs.getObject(pmd.getFieldName("SystemValue",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("systemvalue")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBytes(pmd.getFieldName("VrijeRub1",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub1")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBytes(pmd.getFieldName("VrijeRub2",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub2")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBytes(pmd.getFieldName("VrijeRub3",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub3")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBytes(pmd.getFieldName("VrijeRub4",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub4")).appendChild(doc.createTextNode(s));
      if ((o=rs.getBytes(pmd.getFieldName("VrijeRub5",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("vrijerub5")).appendChild(doc.createTextNode(s));
    }

    public nl.ibs.vegas.ExecutableQuery getExecutableQuery() {
    		return QueryFactory.create(this);
    }

    final static String[] keys = new String[]{ "bedrijf","id"};
    public String[] getKeyNames() {
    	return keys;
    }
    final static nl.ibs.vegas.meta.AttributeMeta[] attributes = new nl.ibs.vegas.meta.AttributeMeta[]{
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrijf","bedrijf","bedrijf-short","bedrijf-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("id","id","id-short","id-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("soort","soort","soort-short","soort-description","text",String.class,1,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("omschrijving","omschrijving","omschrijving-short","omschrijving-description","text",String.class,50,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("boekstukMask","boekstuk-mask","boekstuk-mask-short","boekstuk-mask-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("laatsteBoekstuk","laatste-boekstuk","laatste-boekstuk-short","laatste-boekstuk-description","text",String.class,15,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("rekening","rekening","rekening-short","rekening-description","text",String.class,9,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("tegenRekening","tegen-rekening","tegen-rekening-short","tegen-rekening-description","text",String.class,9,0,null,"false","false","false","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("systemValue","system-value","system-value-short","system-value-description","boolean",boolean.class,0,0,"false","false","false","","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub1","vrije-rub1","vrije-rub1-short","vrije-rub1-description","object",String.class,0,0,null,"false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub2","vrije-rub2","vrije-rub2-short","vrije-rub2-description","object",String.class,0,0,null,"false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub3","vrije-rub3","vrije-rub3-short","vrije-rub3-description","object",String.class,0,0,null,"false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub4","vrije-rub4","vrije-rub4-short","vrije-rub4-description","object",String.class,0,0,null,"false","false","false","", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("vrijeRub5","vrije-rub5","vrije-rub5-short","vrije-rub5-description","object",String.class,0,0,null,"false","false","false","", false, false,null,null)
    };
    public nl.ibs.vegas.meta.AttributeMeta[] getAttributes() {
    	return attributes;
    }
    public Class getClassType() {
    	return nl.eadmin.db.Dagboek.class;
    }
    public String getNameField() {
    	return "id";
    }
    public String getDescriptionField() {
    	return "id";
    }
    public String getTextKey() {
    	return "dagboek";
    }
    public String getTextKeyShort() {
    	return "dagboek-short";
    }
    public String getTextKeyDescription() {
    	return "dagboek-description";
    }
    public nl.ibs.vegas.meta.AttributeMeta getAttribute(String name) {
    	if (attributes == null || name == null)
    		return null;
    	for (int i = 0; i < attributes.length; i++)
    		if (name.equals(attributes[i].getName()))
    			return attributes[i];
    	return null;
    }
  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
