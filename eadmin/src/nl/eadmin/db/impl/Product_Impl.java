package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Product_Impl  implements Product, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataProduct.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String product;
    protected String omschr1;
    protected String omschr2;
    protected String omschr3;
    protected String eenheid;
    protected BigDecimal prijs;
    protected BigDecimal prijsInkoop;
    protected String rekeningNr;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Product_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Product_Impl (DBData _dbd ,  String _bedrijf, String _product) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setProduct(_product);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Product_Impl (DBData _dbd, ProductDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.ProductDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Product with " + bean.getClass().getName() + "!! Use nl.eadmin.db.ProductDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setProduct(bean.getProduct());
        setOmschr1(bean.getOmschr1());
        setOmschr2(bean.getOmschr2());
        setOmschr3(bean.getOmschr3());
        setEenheid(bean.getEenheid());
        setPrijs(bean.getPrijs());
        setPrijsInkoop(bean.getPrijsInkoop());
        setRekeningNr(bean.getRekeningNr());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Product_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      product = rs.getString(pmd.getFieldName("Product",provider));
      omschr1 = rs.getString(pmd.getFieldName("Omschr1",provider));
      omschr2 = rs.getString(pmd.getFieldName("Omschr2",provider));
      omschr3 = rs.getString(pmd.getFieldName("Omschr3",provider));
      eenheid = rs.getString(pmd.getFieldName("Eenheid",provider));
      prijs = rs.getBigDecimal(pmd.getFieldName("Prijs",provider));
      prijsInkoop = rs.getBigDecimal(pmd.getFieldName("PrijsInkoop",provider));
      rekeningNr = rs.getString(pmd.getFieldName("RekeningNr",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       ProductPK key = new ProductPK();
       key.setBedrijf(getBedrijf());
       key.setProduct(getProduct());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getProduct()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:1613731003}
//{CODE-INSERT-END:1613731003}
      return trim(product);
      //Custom code
//{CODE-INSERT-BEGIN:-1513948792}
//{CODE-INSERT-END:-1513948792}
    }


    public String getOmschr1()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1430504389}
//{CODE-INSERT-END:1430504389}
      return trim(omschr1);
      //Custom code
//{CODE-INSERT-BEGIN:1395960766}
//{CODE-INSERT-END:1395960766}
    }


    public String getOmschr2()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1431427910}
//{CODE-INSERT-END:1431427910}
      return trim(omschr2);
      //Custom code
//{CODE-INSERT-BEGIN:1424589917}
//{CODE-INSERT-END:1424589917}
    }


    public String getOmschr3()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1432351431}
//{CODE-INSERT-END:1432351431}
      return trim(omschr3);
      //Custom code
//{CODE-INSERT-BEGIN:1453219068}
//{CODE-INSERT-END:1453219068}
    }


    public String getEenheid()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1490637810}
//{CODE-INSERT-END:1490637810}
      return eenheid;
      //Custom code
//{CODE-INSERT-BEGIN:-1034870479}
//{CODE-INSERT-END:-1034870479}
    }


    public BigDecimal getPrijs()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-358012036}
//{CODE-INSERT-END:-358012036}
      return prijs;
      //Custom code
//{CODE-INSERT-BEGIN:1786526439}
//{CODE-INSERT-END:1786526439}
    }


    public BigDecimal getPrijsInkoop()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1440729722}
//{CODE-INSERT-END:-1440729722}
      return prijsInkoop;
      //Custom code
//{CODE-INSERT-BEGIN:-1712950755}
//{CODE-INSERT-END:-1712950755}
    }


    public String getRekeningNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:666684321}
//{CODE-INSERT-END:666684321}
      return trim(rekeningNr);
      //Custom code
//{CODE-INSERT-BEGIN:-807624862}
//{CODE-INSERT-END:-807624862}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setProduct(String _product ) throws Exception {

      if (_product !=  null)
         _product = _product.trim();
      if (_product == null)
          _product =  "";
      if (_product.equals(getProduct()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1770483769}
//{CODE-INSERT-END:-1770483769}
      product =  _product;

      autoUpdate();

    }

    public void setOmschr1(String _omschr1 ) throws Exception {

      if (_omschr1 !=  null)
         _omschr1 = _omschr1.trim();
      if (_omschr1 == null)
          _omschr1 =  "";
      if (_omschr1.equals(getOmschr1()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1953710383}
//{CODE-INSERT-END:-1953710383}
      omschr1 =  _omschr1;

      autoUpdate();

    }

    public void setOmschr2(String _omschr2 ) throws Exception {

      if (_omschr2 !=  null)
         _omschr2 = _omschr2.trim();
      if (_omschr2 == null)
          _omschr2 =  "";
      if (_omschr2.equals(getOmschr2()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1952786862}
//{CODE-INSERT-END:-1952786862}
      omschr2 =  _omschr2;

      autoUpdate();

    }

    public void setOmschr3(String _omschr3 ) throws Exception {

      if (_omschr3 !=  null)
         _omschr3 = _omschr3.trim();
      if (_omschr3 == null)
          _omschr3 =  "";
      if (_omschr3.equals(getOmschr3()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1951863341}
//{CODE-INSERT-END:-1951863341}
      omschr3 =  _omschr3;

      autoUpdate();

    }

    public void setEenheid(String _eenheid ) throws Exception {

      if (_eenheid == null)
          _eenheid =  "";
      if (_eenheid.equals(getEenheid()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1893576962}
//{CODE-INSERT-END:-1893576962}
      eenheid =  _eenheid;

      autoUpdate();

    }

    public void setPrijs(BigDecimal _prijs ) throws Exception {

      if (_prijs == null)
          _prijs =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_prijs.equals(getPrijs()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:416119176}
//{CODE-INSERT-END:416119176}
      prijs =  _prijs;

      autoUpdate();

    }

    public void setPrijsInkoop(BigDecimal _prijsInkoop ) throws Exception {

      if (_prijsInkoop == null)
          _prijsInkoop =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      if (_prijsInkoop.equals(getPrijsInkoop()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1310509714}
//{CODE-INSERT-END:1310509714}
      prijsInkoop =  _prijsInkoop;

      autoUpdate();

    }

    public void setRekeningNr(String _rekeningNr ) throws Exception {

      if (_rekeningNr !=  null)
         _rekeningNr = _rekeningNr.trim();
      if (_rekeningNr !=  null)
         _rekeningNr = _rekeningNr.toUpperCase();
      if (_rekeningNr == null)
          _rekeningNr =  "";
      if (_rekeningNr.equals(getRekeningNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1586717973}
//{CODE-INSERT-END:1586717973}
      rekeningNr =  _rekeningNr;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      product =  "";
      omschr1 =  "";
      omschr2 =  "";
      omschr3 =  "";
      eenheid =  "";
      prijs =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      prijsInkoop =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
      rekeningNr =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.product = product;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Product is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         ProductPK key = new ProductPK();
         key.setBedrijf(getBedrijf());
         key.setProduct(getProduct());
         ((nl.eadmin.db.impl.ProductManager_Impl)nl.eadmin.db.impl.ProductManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((ProductManager_Impl)ProductManager_Impl.getInstance(dbd)).getResultSet((ProductPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("995752339_"+ dbd.getDBId() + bedrijf + product).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getProductDataBean()!
    */
    public ProductDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public ProductDataBean getProductDataBean() throws Exception {
      return getDataBean(null);
    }

    private ProductDataBean getDataBean(ProductDataBean bean) throws Exception {

      if (bean == null)
          bean = new ProductDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setProduct(getProduct());
      bean.setOmschr1(getOmschr1());
      bean.setOmschr2(getOmschr2());
      bean.setOmschr3(getOmschr3());
      bean.setEenheid(getEenheid());
      bean.setPrijs(getPrijs());
      bean.setPrijsInkoop(getPrijsInkoop());
      bean.setRekeningNr(getRekeningNr());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Product_Impl))
             return false;
          nl.eadmin.db.impl.Product_Impl bo = (nl.eadmin.db.impl.Product_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getProduct() == null){
              if (this.getProduct() != null)
                  return false;
          }else if (!bo.getProduct().equals(this.getProduct())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(ProductDataBean bean) throws Exception {
       set(bean);
    }


    public void set(ProductDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setProduct(bean.getProduct());
           setOmschr1(bean.getOmschr1());
           setOmschr2(bean.getOmschr2());
           setOmschr3(bean.getOmschr3());
           setEenheid(bean.getEenheid());
           setPrijs(bean.getPrijs());
           setPrijsInkoop(bean.getPrijsInkoop());
           setRekeningNr(bean.getRekeningNr());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Product is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Product.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("Product",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr1",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr2",provider) +  " = ? " + ", " + pmd.getFieldName("Omschr3",provider) +  " = ? " + ", " + pmd.getFieldName("Eenheid",provider) +  " = ? " + ", " + pmd.getFieldName("Prijs",provider) +  " = ? " + ", " + pmd.getFieldName("PrijsInkoop",provider) +  " = ? " + ", " + pmd.getFieldName("RekeningNr",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("Product",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  product,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschr3,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  eenheid,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  prijs,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  prijsInkoop,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(parmIndex++,  rekeningNr,  mapping.getJDBCTypeFor("String"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.product,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Product modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Product.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Product.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("Product",provider) + ", " + pmd.getFieldName("Omschr1",provider) + ", " + pmd.getFieldName("Omschr2",provider) + ", " + pmd.getFieldName("Omschr3",provider) + ", " + pmd.getFieldName("Eenheid",provider) + ", " + pmd.getFieldName("Prijs",provider) + ", " + pmd.getFieldName("PrijsInkoop",provider) + ", " + pmd.getFieldName("RekeningNr",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, product,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, omschr1,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5, omschr2,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6, omschr3,  mapping.getJDBCTypeFor("String"));
        prep.setObject(7, eenheid,  mapping.getJDBCTypeFor("String"));
        prep.setObject(8, prijs,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(9, prijsInkoop,  mapping.getJDBCTypeFor("BigDecimal"), 2);
        prep.setObject(10, rekeningNr,  mapping.getJDBCTypeFor("String"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.product = currentDBImage.product;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.product = transactionImage.dbImage.product;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        product = transactionImage.dbImage.product;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String product;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
