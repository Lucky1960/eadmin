package nl.eadmin.db.impl;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import nl.eadmin.db.Instellingen;
import nl.eadmin.db.InstellingenDataBean;
import nl.ibs.jeelog.Log;
import nl.ibs.jsql.AtomicDBTransaction;
import nl.ibs.jsql.BusinessObject_Impl;
import nl.ibs.jsql.DBConfig;
import nl.ibs.jsql.DBData;
import nl.ibs.jsql.DBPersistenceManager;
import nl.ibs.jsql.DBTransactionListener;
import nl.ibs.jsql.exception.CreateException;
import nl.ibs.jsql.exception.DeletedException;
import nl.ibs.jsql.exception.FinderException;
import nl.ibs.jsql.exception.InvalidStateException;
import nl.ibs.jsql.exception.JSQLException;
import nl.ibs.jsql.exception.UpdateException;
import nl.ibs.jsql.impl.ArrayListImpl;
import nl.ibs.jsql.sql.AttributeAccessor;
import nl.ibs.jsql.sql.ConnectionProvider;
import nl.ibs.jsql.sql.DBConnection;
import nl.ibs.jsql.sql.DBMapping;
import nl.ibs.jsql.sql.PersistenceMetaData;

import org.w3c.dom.DOMImplementation;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Instellingen_Impl  implements Instellingen, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataInstellingen.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected int stdBetaalTermijn;
    protected int lengteRekening;
    protected String maskCredNr;
    protected String maskDebNr;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Instellingen_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Instellingen_Impl (DBData _dbd ,  String _bedrijf) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Instellingen_Impl (DBData _dbd, InstellingenDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.InstellingenDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Instellingen with " + bean.getClass().getName() + "!! Use nl.eadmin.db.InstellingenDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setStdBetaalTermijn(bean.getStdBetaalTermijn());
        setLengteRekening(bean.getLengteRekening());
        setMaskCredNr(bean.getMaskCredNr());
        setMaskDebNr(bean.getMaskDebNr());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Instellingen_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      stdBetaalTermijn = rs.getInt(pmd.getFieldName("StdBetaalTermijn",provider));
      lengteRekening = rs.getInt(pmd.getFieldName("LengteRekening",provider));
      maskCredNr = rs.getString(pmd.getFieldName("MaskCredNr",provider));
      maskDebNr = rs.getString(pmd.getFieldName("MaskDebNr",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       return getBedrijf();
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public int getStdBetaalTermijn()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1971854465}
//{CODE-INSERT-END:-1971854465}
      return stdBetaalTermijn;
      //Custom code
//{CODE-INSERT-BEGIN:-997948604}
//{CODE-INSERT-END:-997948604}
    }


    public int getLengteRekening()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:778057344}
//{CODE-INSERT-END:778057344}
      return lengteRekening;
      //Custom code
//{CODE-INSERT-BEGIN:-1650028445}
//{CODE-INSERT-END:-1650028445}
    }


    public String getMaskCredNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1480999366}
//{CODE-INSERT-END:-1480999366}
      return maskCredNr;
      //Custom code
//{CODE-INSERT-BEGIN:1333657577}
//{CODE-INSERT-END:1333657577}
    }


    public String getMaskDebNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1600616165}
//{CODE-INSERT-END:1600616165}
      return maskDebNr;
      //Custom code
//{CODE-INSERT-BEGIN:-1920508770}
//{CODE-INSERT-END:-1920508770}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setStdBetaalTermijn(int _stdBetaalTermijn ) throws Exception {

      if (_stdBetaalTermijn == getStdBetaalTermijn())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1519031795}
//{CODE-INSERT-END:1519031795}
      stdBetaalTermijn =  _stdBetaalTermijn;

      autoUpdate();

    }

    public void setLengteRekening(int _lengteRekening ) throws Exception {

      if (_lengteRekening == getLengteRekening())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:2091185652}
//{CODE-INSERT-END:2091185652}
      lengteRekening =  _lengteRekening;

      autoUpdate();

    }

    public void setMaskCredNr(String _maskCredNr ) throws Exception {

      if (_maskCredNr == null)
          _maskCredNr =  "";
      if (_maskCredNr.equals(getMaskCredNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-560965714}
//{CODE-INSERT-END:-560965714}
      maskCredNr =  _maskCredNr;

      autoUpdate();

    }

    public void setMaskDebNr(String _maskDebNr ) throws Exception {

      if (_maskDebNr == null)
          _maskDebNr =  "";
      if (_maskDebNr.equals(getMaskDebNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:660463345}
//{CODE-INSERT-END:660463345}
      maskDebNr =  _maskDebNr;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      stdBetaalTermijn = 14;
      lengteRekening = 4;
      maskCredNr =  "";
      maskDebNr =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Instellingen is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         ((nl.eadmin.db.impl.InstellingenManager_Impl)nl.eadmin.db.impl.InstellingenManager_Impl.getInstance(dbd)).removeByPrimaryKey(getBedrijf());
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((InstellingenManager_Impl)InstellingenManager_Impl.getInstance(dbd)).getResultSet((String)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("-1205197656_"+ dbd.getDBId() + bedrijf).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getInstellingenDataBean()!
    */
    public InstellingenDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public InstellingenDataBean getInstellingenDataBean() throws Exception {
      return getDataBean(null);
    }

    private InstellingenDataBean getDataBean(InstellingenDataBean bean) throws Exception {

      if (bean == null)
          bean = new InstellingenDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setStdBetaalTermijn(getStdBetaalTermijn());
      bean.setLengteRekening(getLengteRekening());
      bean.setMaskCredNr(getMaskCredNr());
      bean.setMaskDebNr(getMaskDebNr());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Instellingen_Impl))
             return false;
          nl.eadmin.db.impl.Instellingen_Impl bo = (nl.eadmin.db.impl.Instellingen_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(InstellingenDataBean bean) throws Exception {
       set(bean);
    }


    public void set(InstellingenDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setStdBetaalTermijn(bean.getStdBetaalTermijn());
           setLengteRekening(bean.getLengteRekening());
           setMaskCredNr(bean.getMaskCredNr());
           setMaskDebNr(bean.getMaskDebNr());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Instellingen is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Instellingen.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("StdBetaalTermijn",provider) +  " = ? " + ", " + pmd.getFieldName("LengteRekening",provider) +  " = ? " + ", " + pmd.getFieldName("MaskCredNr",provider) +  " = ? " + ", " + pmd.getFieldName("MaskDebNr",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( stdBetaalTermijn),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  new Integer( lengteRekening),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  maskCredNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  maskDebNr,  mapping.getJDBCTypeFor("String"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Instellingen modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Instellingen.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Instellingen.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("StdBetaalTermijn",provider) + ", " + pmd.getFieldName("LengteRekening",provider) + ", " + pmd.getFieldName("MaskCredNr",provider) + ", " + pmd.getFieldName("MaskDebNr",provider)+ 
                                           ") VALUES (?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3,  new Integer(stdBetaalTermijn),  mapping.getJDBCTypeFor("int"));
        prep.setObject(4,  new Integer(lengteRekening),  mapping.getJDBCTypeFor("int"));
        prep.setObject(5, maskCredNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6, maskDebNr,  mapping.getJDBCTypeFor("String"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
