package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}


import nl.eadmin.ApplicationConstants;
import nl.eadmin.SessionKeys;
import nl.ibs.esp.servlet.ESPSessionContext;

//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Gebruiker_Impl  implements Gebruiker, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataGebruiker.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String id;
    protected String naam;
    protected String wachtwoord;
    protected String bedrijf;
    protected int boekjaar;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Gebruiker_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Gebruiker_Impl (DBData _dbd ,  String _id) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setId(_id);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Gebruiker_Impl (DBData _dbd, GebruikerDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.GebruikerDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Gebruiker with " + bean.getClass().getName() + "!! Use nl.eadmin.db.GebruikerDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setId(bean.getId());
        setNaam(bean.getNaam());
        setWachtwoord(bean.getWachtwoord());
        setBedrijf(bean.getBedrijf());
        setBoekjaar(bean.getBoekjaar());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Gebruiker_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      id = rs.getString(pmd.getFieldName("Id",provider));
      naam = rs.getString(pmd.getFieldName("Naam",provider));
      wachtwoord = rs.getString(pmd.getFieldName("Wachtwoord",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      boekjaar = rs.getInt(pmd.getFieldName("Boekjaar",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       return getId();
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getId()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:349057815}
//{CODE-INSERT-END:349057815}
      return trim(id);
      //Custom code
//{CODE-INSERT-BEGIN:-2064111956}
//{CODE-INSERT-END:-2064111956}
    }


    public String getNaam()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2047346917}
//{CODE-INSERT-END:-2047346917}
      return trim(naam);
      //Custom code
//{CODE-INSERT-BEGIN:956752680}
//{CODE-INSERT-END:956752680}
    }


    public String getWachtwoord()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1311375754}
//{CODE-INSERT-END:-1311375754}

        String obj = wachtwoord == null?null:nl.ibs.util.Scrambler.unscramble(wachtwoord);

      //Custom code
//{CODE-INSERT-BEGIN:2035907584}
//{CODE-INSERT-END:2035907584}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:-1311376669}
//{CODE-INSERT-END:-1311376669}
    }


    public String getBedrijf()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public int getBoekjaar()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-25361449}
//{CODE-INSERT-END:-25361449}
      return boekjaar;
      //Custom code
//{CODE-INSERT-BEGIN:-786207252}
//{CODE-INSERT-END:-786207252}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setId(String _id ) throws Exception {

      if (_id !=  null)
         _id = _id.trim();
      if (_id == null)
          _id =  "";
      if (_id.equals(getId()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-2009104245}
//{CODE-INSERT-END:-2009104245}
      id =  _id;

      autoUpdate();

    }

    public void setNaam(String _naam ) throws Exception {

      if (_naam !=  null)
         _naam = _naam.trim();
      if (_naam == null)
          _naam =  "";
      if (_naam.equals(getNaam()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-498354289}
//{CODE-INSERT-END:-498354289}
      naam =  _naam;

      autoUpdate();

    }

    public void setWachtwoord(String _wachtwoord ) throws Exception {

      if (_wachtwoord == null)
          _wachtwoord =  "24";
      if (_wachtwoord.equals(getWachtwoord()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1339026060}
//{CODE-INSERT-END:-1339026060}
      wachtwoord =  _wachtwoord == null?null:nl.ibs.util.Scrambler.scramble( _wachtwoord);

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setBoekjaar(int _boekjaar ) throws Exception {

      if (_boekjaar == getBoekjaar())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1856804277}
//{CODE-INSERT-END:-1856804277}
      boekjaar =  _boekjaar;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      id =  "";
      naam =  "";
      wachtwoord =  "07";
      bedrijf =  "";
      boekjaar = 0;
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.id = id;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Gebruiker is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         ((nl.eadmin.db.impl.GebruikerManager_Impl)nl.eadmin.db.impl.GebruikerManager_Impl.getInstance(dbd)).removeByPrimaryKey(getId());
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((GebruikerManager_Impl)GebruikerManager_Impl.getInstance(dbd)).getResultSet((String)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("-1205850630_"+ dbd.getDBId() + id).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getGebruikerDataBean()!
    */
    public GebruikerDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public GebruikerDataBean getGebruikerDataBean() throws Exception {
      return getDataBean(null);
    }

    private GebruikerDataBean getDataBean(GebruikerDataBean bean) throws Exception {

      if (bean == null)
          bean = new GebruikerDataBean();

      bean.setId(getId());
      bean.setNaam(getNaam());
      bean.setWachtwoord(getWachtwoord());
      bean.setBedrijf(getBedrijf());
      bean.setBoekjaar(getBoekjaar());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Gebruiker_Impl))
             return false;
          nl.eadmin.db.impl.Gebruiker_Impl bo = (nl.eadmin.db.impl.Gebruiker_Impl)object;
          if (bo.getId() == null){
              if (this.getId() != null)
                  return false;
          }else if (!bo.getId().equals(this.getId())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(GebruikerDataBean bean) throws Exception {
       set(bean);
    }


    public void set(GebruikerDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setId(bean.getId());
           setNaam(bean.getNaam());
           setWachtwoord(bean.getWachtwoord());
           setBedrijf(bean.getBedrijf());
           setBoekjaar(bean.getBoekjaar());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Gebruiker is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Gebruiker.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Id",provider) +  " = ? " + ", " + pmd.getFieldName("Naam",provider) +  " = ? " + ", " + pmd.getFieldName("Wachtwoord",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("Boekjaar",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Id",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  id,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  naam,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  wachtwoord,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( boekjaar),  mapping.getJDBCTypeFor("int"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.id,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Gebruiker modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Gebruiker.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Gebruiker.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Id",provider) + ", " + pmd.getFieldName("Naam",provider) + ", " + pmd.getFieldName("Wachtwoord",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("Boekjaar",provider)+ 
                                           ") VALUES (?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, id,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, naam,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, wachtwoord,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6,  new Integer(boekjaar),  mapping.getJDBCTypeFor("int"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.id = currentDBImage.id;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.id = transactionImage.dbImage.id;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        id = transactionImage.dbImage.id;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String id;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
	public boolean isAdmin() throws Exception {
		return getId().toUpperCase().equals(ApplicationConstants.DEFAULT_ADMIN_NAME ) ||
				ESPSessionContext.getSessionAttribute(SessionKeys.USER_IS_ADMIN) != null;
	}
//{CODE-INSERT-END:955534258}
}
