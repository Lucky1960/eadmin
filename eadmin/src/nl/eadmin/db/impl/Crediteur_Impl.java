package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
import nl.eadmin.enums.AdresTypeEnum;
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Crediteur_Impl  implements Crediteur, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataCrediteur.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String credNr;
    protected String naam;
    protected String contactPersoon;
    protected String email;
    protected String kvkNr;
    protected String btwNr;
    protected String ibanNr;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
    private Adres myAddress;
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Crediteur_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Crediteur_Impl (DBData _dbd ,  String _bedrijf, String _credNr) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setCredNr(_credNr);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Crediteur_Impl (DBData _dbd, CrediteurDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.CrediteurDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Crediteur with " + bean.getClass().getName() + "!! Use nl.eadmin.db.CrediteurDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setCredNr(bean.getCredNr());
        setNaam(bean.getNaam());
        setContactPersoon(bean.getContactPersoon());
        setEmail(bean.getEmail());
        setKvkNr(bean.getKvkNr());
        setBtwNr(bean.getBtwNr());
        setIbanNr(bean.getIbanNr());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Crediteur_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      credNr = rs.getString(pmd.getFieldName("CredNr",provider));
      naam = rs.getString(pmd.getFieldName("Naam",provider));
      contactPersoon = rs.getString(pmd.getFieldName("ContactPersoon",provider));
      email = rs.getString(pmd.getFieldName("Email",provider));
      kvkNr = rs.getString(pmd.getFieldName("KvkNr",provider));
      btwNr = rs.getString(pmd.getFieldName("BtwNr",provider));
      ibanNr = rs.getString(pmd.getFieldName("IbanNr",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       CrediteurPK key = new CrediteurPK();
       key.setBedrijf(getBedrijf());
       key.setCredNr(getCredNr());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getCredNr()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-407364050}
//{CODE-INSERT-END:-407364050}
      return trim(credNr);
      //Custom code
//{CODE-INSERT-BEGIN:256614005}
//{CODE-INSERT-END:256614005}
    }


    public String getNaam()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2047346917}
//{CODE-INSERT-END:-2047346917}
      return trim(naam);
      //Custom code
//{CODE-INSERT-BEGIN:956752680}
//{CODE-INSERT-END:956752680}
    }


    public String getContactPersoon()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1057184532}
//{CODE-INSERT-END:1057184532}
      return trim(contactPersoon);
      //Custom code
//{CODE-INSERT-BEGIN:-1587020209}
//{CODE-INSERT-END:-1587020209}
    }


    public String getEmail()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-620155160}
//{CODE-INSERT-END:-620155160}
      return trim(email);
      //Custom code
//{CODE-INSERT-BEGIN:-2044943109}
//{CODE-INSERT-END:-2044943109}
    }


    public String getKvkNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-556899952}
//{CODE-INSERT-END:-556899952}
      return trim(kvkNr);
      //Custom code
//{CODE-INSERT-BEGIN:-84031661}
//{CODE-INSERT-END:-84031661}
    }


    public String getBtwNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1399777237}
//{CODE-INSERT-END:1399777237}
      return trim(btwNr);
      //Custom code
//{CODE-INSERT-BEGIN:443419054}
//{CODE-INSERT-END:443419054}
    }


    public String getIbanNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-828149274}
//{CODE-INSERT-END:-828149274}
      return trim(ibanNr);
      //Custom code
//{CODE-INSERT-BEGIN:97173949}
//{CODE-INSERT-END:97173949}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setCredNr(String _credNr ) throws Exception {

      if (_credNr !=  null)
         _credNr = _credNr.trim();
      if (_credNr == null)
          _credNr =  "";
      if (_credNr.equals(getCredNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:2115867042}
//{CODE-INSERT-END:2115867042}
      credNr =  _credNr;

      autoUpdate();

    }

    public void setNaam(String _naam ) throws Exception {

      if (_naam !=  null)
         _naam = _naam.trim();
      if (_naam == null)
          _naam =  "";
      if (_naam.equals(getNaam()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-498354289}
//{CODE-INSERT-END:-498354289}
      naam =  _naam;

      autoUpdate();

    }

    public void setContactPersoon(String _contactPersoon ) throws Exception {

      if (_contactPersoon !=  null)
         _contactPersoon = _contactPersoon.trim();
      if (_contactPersoon == null)
          _contactPersoon =  "";
      if (_contactPersoon.equals(getContactPersoon()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1924654456}
//{CODE-INSERT-END:-1924654456}
      contactPersoon =  _contactPersoon;

      autoUpdate();

    }

    public void setEmail(String _email ) throws Exception {

      if (_email !=  null)
         _email = _email.trim();
      if (_email == null)
          _email =  "";
      if (_email.equals(getEmail()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:153976052}
//{CODE-INSERT-END:153976052}
      email =  _email;

      autoUpdate();

    }

    public void setKvkNr(String _kvkNr ) throws Exception {

      if (_kvkNr !=  null)
         _kvkNr = _kvkNr.trim();
      if (_kvkNr == null)
          _kvkNr =  "";
      if (_kvkNr.equals(getKvkNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:217231260}
//{CODE-INSERT-END:217231260}
      kvkNr =  _kvkNr;

      autoUpdate();

    }

    public void setBtwNr(String _btwNr ) throws Exception {

      if (_btwNr !=  null)
         _btwNr = _btwNr.trim();
      if (_btwNr == null)
          _btwNr =  "";
      if (_btwNr.equals(getBtwNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-2121058847}
//{CODE-INSERT-END:-2121058847}
      btwNr =  _btwNr;

      autoUpdate();

    }

    public void setIbanNr(String _ibanNr ) throws Exception {

      if (_ibanNr !=  null)
         _ibanNr = _ibanNr.trim();
      if (_ibanNr == null)
          _ibanNr =  "";
      if (_ibanNr.equals(getIbanNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1695081818}
//{CODE-INSERT-END:1695081818}
      ibanNr =  _ibanNr;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      credNr =  "";
      naam =  "";
      contactPersoon =  "";
      email =  "";
      kvkNr =  "";
      btwNr =  "";
      ibanNr =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.credNr = credNr;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Crediteur is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         CrediteurPK key = new CrediteurPK();
         key.setBedrijf(getBedrijf());
         key.setCredNr(getCredNr());
         ((nl.eadmin.db.impl.CrediteurManager_Impl)nl.eadmin.db.impl.CrediteurManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((CrediteurManager_Impl)CrediteurManager_Impl.getInstance(dbd)).getResultSet((CrediteurPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("866673613_"+ dbd.getDBId() + bedrijf + credNr).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getCrediteurDataBean()!
    */
    public CrediteurDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public CrediteurDataBean getCrediteurDataBean() throws Exception {
      return getDataBean(null);
    }

    private CrediteurDataBean getDataBean(CrediteurDataBean bean) throws Exception {

      if (bean == null)
          bean = new CrediteurDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setCredNr(getCredNr());
      bean.setNaam(getNaam());
      bean.setContactPersoon(getContactPersoon());
      bean.setEmail(getEmail());
      bean.setKvkNr(getKvkNr());
      bean.setBtwNr(getBtwNr());
      bean.setIbanNr(getIbanNr());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Crediteur_Impl))
             return false;
          nl.eadmin.db.impl.Crediteur_Impl bo = (nl.eadmin.db.impl.Crediteur_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getCredNr() == null){
              if (this.getCredNr() != null)
                  return false;
          }else if (!bo.getCredNr().equals(this.getCredNr())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(CrediteurDataBean bean) throws Exception {
       set(bean);
    }


    public void set(CrediteurDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setCredNr(bean.getCredNr());
           setNaam(bean.getNaam());
           setContactPersoon(bean.getContactPersoon());
           setEmail(bean.getEmail());
           setKvkNr(bean.getKvkNr());
           setBtwNr(bean.getBtwNr());
           setIbanNr(bean.getIbanNr());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Crediteur is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Crediteur.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("CredNr",provider) +  " = ? " + ", " + pmd.getFieldName("Naam",provider) +  " = ? " + ", " + pmd.getFieldName("ContactPersoon",provider) +  " = ? " + ", " + pmd.getFieldName("Email",provider) +  " = ? " + ", " + pmd.getFieldName("KvkNr",provider) +  " = ? " + ", " + pmd.getFieldName("BtwNr",provider) +  " = ? " + ", " + pmd.getFieldName("IbanNr",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("CredNr",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  credNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  naam,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  contactPersoon,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  email,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  kvkNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  btwNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  ibanNr,  mapping.getJDBCTypeFor("String"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.credNr,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Crediteur modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Crediteur.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Crediteur.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("CredNr",provider) + ", " + pmd.getFieldName("Naam",provider) + ", " + pmd.getFieldName("ContactPersoon",provider) + ", " + pmd.getFieldName("Email",provider) + ", " + pmd.getFieldName("KvkNr",provider) + ", " + pmd.getFieldName("BtwNr",provider) + ", " + pmd.getFieldName("IbanNr",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, credNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, naam,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5, contactPersoon,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6, email,  mapping.getJDBCTypeFor("String"));
        prep.setObject(7, kvkNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(8, btwNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(9, ibanNr,  mapping.getJDBCTypeFor("String"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.credNr = currentDBImage.credNr;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.credNr = transactionImage.dbImage.credNr;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        credNr = transactionImage.dbImage.credNr;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String credNr;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
    public Adres getAdres(boolean postAdres) throws Exception {
    	if (myAddress == null) {
    		AdresPK key = new AdresPK();
    		key.setBedrijf(getBedrijf());
    		key.setDc("C");
    		key.setDcNr(getCredNr());
    		key.setAdresType(postAdres ? AdresTypeEnum.ADRESTYPE_POST:AdresTypeEnum.ADRESTYPE_BEZOEK);
    		myAddress = AdresManagerFactory.getInstance(dbd).findOrCreate(key);
    		if (myAddress.isEmpty()) {
        		key.setAdresType(postAdres ? AdresTypeEnum.ADRESTYPE_BEZOEK:AdresTypeEnum.ADRESTYPE_POST);
        		myAddress = AdresManagerFactory.getInstance(dbd).findOrCreate(key);
    		}
    	}
    	return myAddress;
    }

    public String getAdresRegel1() throws Exception {
    	return getAdres(false).getAdresRegel1();
    }

    public String getAdresRegel2() throws Exception {
    	return getAdres(false).getAdresRegel2();
    }

    public String getAdresRegel3() throws Exception {
    	return getAdres(false).getAdresRegel3();
    }

    public String getPlaats() throws Exception {
    	return getAdres(false).getPlaats();
    }
//{CODE-INSERT-END:955534258}
}
