package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
import nl.eadmin.enums.DagboekSoortEnum;
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Dagboek_Impl  implements Dagboek, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataDagboek.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String id;
    protected String soort;
    protected String omschrijving;
    protected String boekstukMask;
    protected String laatsteBoekstuk;
    protected String rekening;
    protected String tegenRekening;
    protected boolean systemValue;
    protected byte[] vrijeRub1;
    protected byte[] vrijeRub2;
    protected byte[] vrijeRub3;
    protected byte[] vrijeRub4;
    protected byte[] vrijeRub5;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Dagboek_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Dagboek_Impl (DBData _dbd ,  String _bedrijf, String _id) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setId(_id);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Dagboek_Impl (DBData _dbd, DagboekDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.DagboekDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Dagboek with " + bean.getClass().getName() + "!! Use nl.eadmin.db.DagboekDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setId(bean.getId());
        setSoort(bean.getSoort());
        setOmschrijving(bean.getOmschrijving());
        setBoekstukMask(bean.getBoekstukMask());
        setLaatsteBoekstuk(bean.getLaatsteBoekstuk());
        setRekening(bean.getRekening());
        setTegenRekening(bean.getTegenRekening());
        setSystemValue(bean.getSystemValue());
        setVrijeRub1(bean.getVrijeRub1());
        setVrijeRub2(bean.getVrijeRub2());
        setVrijeRub3(bean.getVrijeRub3());
        setVrijeRub4(bean.getVrijeRub4());
        setVrijeRub5(bean.getVrijeRub5());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Dagboek_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      id = rs.getString(pmd.getFieldName("Id",provider));
      soort = rs.getString(pmd.getFieldName("Soort",provider));
      omschrijving = rs.getString(pmd.getFieldName("Omschrijving",provider));
      boekstukMask = rs.getString(pmd.getFieldName("BoekstukMask",provider));
      laatsteBoekstuk = rs.getString(pmd.getFieldName("LaatsteBoekstuk",provider));
      rekening = rs.getString(pmd.getFieldName("Rekening",provider));
      tegenRekening = rs.getString(pmd.getFieldName("TegenRekening",provider));
      systemValue = rs.getBoolean(pmd.getFieldName("SystemValue",provider));
      vrijeRub1 = rs.getBytes(pmd.getFieldName("VrijeRub1",provider));
      vrijeRub2 = rs.getBytes(pmd.getFieldName("VrijeRub2",provider));
      vrijeRub3 = rs.getBytes(pmd.getFieldName("VrijeRub3",provider));
      vrijeRub4 = rs.getBytes(pmd.getFieldName("VrijeRub4",provider));
      vrijeRub5 = rs.getBytes(pmd.getFieldName("VrijeRub5",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       DagboekPK key = new DagboekPK();
       key.setBedrijf(getBedrijf());
       key.setId(getId());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getId()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:349057815}
//{CODE-INSERT-END:349057815}
      return trim(id);
      //Custom code
//{CODE-INSERT-BEGIN:-2064111956}
//{CODE-INSERT-END:-2064111956}
    }


    public String getSoort()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1158860319}
//{CODE-INSERT-END:-1158860319}
      return trim(soort);
      //Custom code
//{CODE-INSERT-BEGIN:-1564933854}
//{CODE-INSERT-END:-1564933854}
    }


    public String getOmschrijving()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:86851457}
//{CODE-INSERT-END:86851457}
      return trim(omschrijving);
      //Custom code
//{CODE-INSERT-BEGIN:-1602574462}
//{CODE-INSERT-END:-1602574462}
    }


    public String getBoekstukMask()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1559049870}
//{CODE-INSERT-END:-1559049870}
      return trim(boekstukMask);
      //Custom code
//{CODE-INSERT-BEGIN:-1085908047}
//{CODE-INSERT-END:-1085908047}
    }


    public String getLaatsteBoekstuk()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1798711826}
//{CODE-INSERT-END:1798711826}
      return trim(laatsteBoekstuk);
      //Custom code
//{CODE-INSERT-BEGIN:-74510575}
//{CODE-INSERT-END:-74510575}
    }


    public String getRekening()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1158739453}
//{CODE-INSERT-END:1158739453}
      return trim(rekening);
      //Custom code
//{CODE-INSERT-BEGIN:1561182342}
//{CODE-INSERT-END:1561182342}
    }


    public String getTegenRekening()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:687477420}
//{CODE-INSERT-END:687477420}
      return trim(tegenRekening);
      //Custom code
//{CODE-INSERT-BEGIN:-163038793}
//{CODE-INSERT-END:-163038793}
    }


    public boolean getSystemValue()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:777848526}
//{CODE-INSERT-END:777848526}
      return systemValue;
      //Custom code
//{CODE-INSERT-BEGIN:-1656501803}
//{CODE-INSERT-END:-1656501803}
    }


    public String getVrijeRub1()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1482492432}
//{CODE-INSERT-END:1482492432}

      String obj = null;
	     if (vrijeRub1 != null && vrijeRub1.length > 0){
	        java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(vrijeRub1);
	        java.io.ObjectInputStream in = new java.io.ObjectInputStream(bis);
	        obj = (String)in.readObject();
	        in.close();
      }

      //Custom code
//{CODE-INSERT-BEGIN:-229272282}
//{CODE-INSERT-END:-229272282}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:1482491517}
//{CODE-INSERT-END:1482491517}
    }


    public String getVrijeRub2()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1511121583}
//{CODE-INSERT-END:1511121583}

      String obj = null;
	     if (vrijeRub2 != null && vrijeRub2.length > 0){
	        java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(vrijeRub2);
	        java.io.ObjectInputStream in = new java.io.ObjectInputStream(bis);
	        obj = (String)in.readObject();
	        in.close();
      }

      //Custom code
//{CODE-INSERT-BEGIN:-228348761}
//{CODE-INSERT-END:-228348761}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:1511120668}
//{CODE-INSERT-END:1511120668}
    }


    public String getVrijeRub3()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1539750734}
//{CODE-INSERT-END:1539750734}

      String obj = null;
	     if (vrijeRub3 != null && vrijeRub3.length > 0){
	        java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(vrijeRub3);
	        java.io.ObjectInputStream in = new java.io.ObjectInputStream(bis);
	        obj = (String)in.readObject();
	        in.close();
      }

      //Custom code
//{CODE-INSERT-BEGIN:-227425240}
//{CODE-INSERT-END:-227425240}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:1539749819}
//{CODE-INSERT-END:1539749819}
    }


    public String getVrijeRub4()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1568379885}
//{CODE-INSERT-END:1568379885}

      String obj = null;
	     if (vrijeRub4 != null && vrijeRub4.length > 0){
	        java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(vrijeRub4);
	        java.io.ObjectInputStream in = new java.io.ObjectInputStream(bis);
	        obj = (String)in.readObject();
	        in.close();
      }

      //Custom code
//{CODE-INSERT-BEGIN:-226501719}
//{CODE-INSERT-END:-226501719}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:1568378970}
//{CODE-INSERT-END:1568378970}
    }


    public String getVrijeRub5()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1597009036}
//{CODE-INSERT-END:1597009036}

      String obj = null;
	     if (vrijeRub5 != null && vrijeRub5.length > 0){
	        java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(vrijeRub5);
	        java.io.ObjectInputStream in = new java.io.ObjectInputStream(bis);
	        obj = (String)in.readObject();
	        in.close();
      }

      //Custom code
//{CODE-INSERT-BEGIN:-225578198}
//{CODE-INSERT-END:-225578198}
      return obj;
      //Custom code
//{CODE-INSERT-BEGIN:1597008121}
//{CODE-INSERT-END:1597008121}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setId(String _id ) throws Exception {

      if (_id !=  null)
         _id = _id.trim();
      if (_id !=  null)
         _id = _id.toUpperCase();
      if (_id == null)
          _id =  "";
      if (_id.equals(getId()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-2009104245}
//{CODE-INSERT-END:-2009104245}
      id =  _id;

      autoUpdate();

    }

    public void setSoort(String _soort ) throws Exception {

      if (_soort !=  null)
         _soort = _soort.trim();
      if (_soort !=  null)
         _soort = _soort.toUpperCase();
      if (_soort == null)
          _soort =  "";
      if (_soort.equals(getSoort()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-384729107}
//{CODE-INSERT-END:-384729107}
      soort =  _soort;

      autoUpdate();

    }

    public void setOmschrijving(String _omschrijving ) throws Exception {

      if (_omschrijving !=  null)
         _omschrijving = _omschrijving.trim();
      if (_omschrijving == null)
          _omschrijving =  "";
      if (_omschrijving.equals(getOmschrijving()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-524071947}
//{CODE-INSERT-END:-524071947}
      omschrijving =  _omschrijving;

      autoUpdate();

    }

    public void setBoekstukMask(String _boekstukMask ) throws Exception {

      if (_boekstukMask !=  null)
         _boekstukMask = _boekstukMask.trim();
      if (_boekstukMask == null)
          _boekstukMask =  "";
      if (_boekstukMask.equals(getBoekstukMask()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:2124994022}
//{CODE-INSERT-END:2124994022}
      boekstukMask =  _boekstukMask;

      autoUpdate();

    }

    public void setLaatsteBoekstuk(String _laatsteBoekstuk ) throws Exception {

      if (_laatsteBoekstuk !=  null)
         _laatsteBoekstuk = _laatsteBoekstuk.trim();
      if (_laatsteBoekstuk == null)
          _laatsteBoekstuk =  "";
      if (_laatsteBoekstuk.equals(getLaatsteBoekstuk()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-443983586}
//{CODE-INSERT-END:-443983586}
      laatsteBoekstuk =  _laatsteBoekstuk;

      autoUpdate();

    }

    public void setRekening(String _rekening ) throws Exception {

      if (_rekening !=  null)
         _rekening = _rekening.trim();
      if (_rekening !=  null)
         _rekening = _rekening.toUpperCase();
      if (_rekening == null)
          _rekening =  "";
      if (_rekening.equals(getRekening()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-672703375}
//{CODE-INSERT-END:-672703375}
      rekening =  _rekening;

      autoUpdate();

    }

    public void setTegenRekening(String _tegenRekening ) throws Exception {

      if (_tegenRekening !=  null)
         _tegenRekening = _tegenRekening.trim();
      if (_tegenRekening !=  null)
         _tegenRekening = _tegenRekening.toUpperCase();
      if (_tegenRekening == null)
          _tegenRekening =  "";
      if (_tegenRekening.equals(getTegenRekening()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1071278920}
//{CODE-INSERT-END:-1071278920}
      tegenRekening =  _tegenRekening;

      autoUpdate();

    }

    public void setSystemValue(boolean _systemValue ) throws Exception {

      if (_systemValue == getSystemValue())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-765879334}
//{CODE-INSERT-END:-765879334}
      systemValue =  _systemValue;

      autoUpdate();

    }

    public void setVrijeRub1(String _vrijeRub1 ) throws Exception {

      if ((_vrijeRub1 == null ? getVrijeRub1() == null : _vrijeRub1.equals(getVrijeRub1())))
         return;
      preSet();

         if ( _vrijeRub1 == null){
            vrijeRub1 = null;
         } else {
            java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
            java.io.ObjectOutputStream out = new java.io.ObjectOutputStream(bos);
            out.writeObject( _vrijeRub1);
            out.flush();
            out.close();
         vrijeRub1 = bos.toByteArray();
         }

      autoUpdate();

    }

    public void setVrijeRub2(String _vrijeRub2 ) throws Exception {

      if ((_vrijeRub2 == null ? getVrijeRub2() == null : _vrijeRub2.equals(getVrijeRub2())))
         return;
      preSet();

         if ( _vrijeRub2 == null){
            vrijeRub2 = null;
         } else {
            java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
            java.io.ObjectOutputStream out = new java.io.ObjectOutputStream(bos);
            out.writeObject( _vrijeRub2);
            out.flush();
            out.close();
         vrijeRub2 = bos.toByteArray();
         }

      autoUpdate();

    }

    public void setVrijeRub3(String _vrijeRub3 ) throws Exception {

      if ((_vrijeRub3 == null ? getVrijeRub3() == null : _vrijeRub3.equals(getVrijeRub3())))
         return;
      preSet();

         if ( _vrijeRub3 == null){
            vrijeRub3 = null;
         } else {
            java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
            java.io.ObjectOutputStream out = new java.io.ObjectOutputStream(bos);
            out.writeObject( _vrijeRub3);
            out.flush();
            out.close();
         vrijeRub3 = bos.toByteArray();
         }

      autoUpdate();

    }

    public void setVrijeRub4(String _vrijeRub4 ) throws Exception {

      if ((_vrijeRub4 == null ? getVrijeRub4() == null : _vrijeRub4.equals(getVrijeRub4())))
         return;
      preSet();

         if ( _vrijeRub4 == null){
            vrijeRub4 = null;
         } else {
            java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
            java.io.ObjectOutputStream out = new java.io.ObjectOutputStream(bos);
            out.writeObject( _vrijeRub4);
            out.flush();
            out.close();
         vrijeRub4 = bos.toByteArray();
         }

      autoUpdate();

    }

    public void setVrijeRub5(String _vrijeRub5 ) throws Exception {

      if ((_vrijeRub5 == null ? getVrijeRub5() == null : _vrijeRub5.equals(getVrijeRub5())))
         return;
      preSet();

         if ( _vrijeRub5 == null){
            vrijeRub5 = null;
         } else {
            java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
            java.io.ObjectOutputStream out = new java.io.ObjectOutputStream(bos);
            out.writeObject( _vrijeRub5);
            out.flush();
            out.close();
         vrijeRub5 = bos.toByteArray();
         }

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      id =  "";
      soort =  "";
      omschrijving =  "";
      boekstukMask =  "";
      laatsteBoekstuk =  "";
      rekening =  "";
      tegenRekening =  "";
      systemValue =  false;
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.id = id;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Dagboek is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         DagboekPK key = new DagboekPK();
         key.setBedrijf(getBedrijf());
         key.setId(getId());
         ((nl.eadmin.db.impl.DagboekManager_Impl)nl.eadmin.db.impl.DagboekManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((DagboekManager_Impl)DagboekManager_Impl.getInstance(dbd)).getResultSet((DagboekPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("-1558506271_"+ dbd.getDBId() + bedrijf + id).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getDagboekDataBean()!
    */
    public DagboekDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public DagboekDataBean getDagboekDataBean() throws Exception {
      return getDataBean(null);
    }

    private DagboekDataBean getDataBean(DagboekDataBean bean) throws Exception {

      if (bean == null)
          bean = new DagboekDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setId(getId());
      bean.setSoort(getSoort());
      bean.setOmschrijving(getOmschrijving());
      bean.setBoekstukMask(getBoekstukMask());
      bean.setLaatsteBoekstuk(getLaatsteBoekstuk());
      bean.setRekening(getRekening());
      bean.setTegenRekening(getTegenRekening());
      bean.setSystemValue(getSystemValue());
      bean.setVrijeRub1(getVrijeRub1());
      bean.setVrijeRub2(getVrijeRub2());
      bean.setVrijeRub3(getVrijeRub3());
      bean.setVrijeRub4(getVrijeRub4());
      bean.setVrijeRub5(getVrijeRub5());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Dagboek_Impl))
             return false;
          nl.eadmin.db.impl.Dagboek_Impl bo = (nl.eadmin.db.impl.Dagboek_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getId() == null){
              if (this.getId() != null)
                  return false;
          }else if (!bo.getId().equals(this.getId())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(DagboekDataBean bean) throws Exception {
       set(bean);
    }


    public void set(DagboekDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setId(bean.getId());
           setSoort(bean.getSoort());
           setOmschrijving(bean.getOmschrijving());
           setBoekstukMask(bean.getBoekstukMask());
           setLaatsteBoekstuk(bean.getLaatsteBoekstuk());
           setRekening(bean.getRekening());
           setTegenRekening(bean.getTegenRekening());
           setSystemValue(bean.getSystemValue());
           setVrijeRub1(bean.getVrijeRub1());
           setVrijeRub2(bean.getVrijeRub2());
           setVrijeRub3(bean.getVrijeRub3());
           setVrijeRub4(bean.getVrijeRub4());
           setVrijeRub5(bean.getVrijeRub5());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Dagboek is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Dagboek.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("Id",provider) +  " = ? " + ", " + pmd.getFieldName("Soort",provider) +  " = ? " + ", " + pmd.getFieldName("Omschrijving",provider) +  " = ? " + ", " + pmd.getFieldName("BoekstukMask",provider) +  " = ? " + ", " + pmd.getFieldName("LaatsteBoekstuk",provider) +  " = ? " + ", " + pmd.getFieldName("Rekening",provider) +  " = ? " + ", " + pmd.getFieldName("TegenRekening",provider) +  " = ? " + ", " + pmd.getFieldName("SystemValue",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub1",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub2",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub3",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub4",provider) +  " = ? " + ", " + pmd.getFieldName("VrijeRub5",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("Id",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  id,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  soort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  boekstukMask,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  laatsteBoekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  rekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  tegenRekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Boolean( systemValue),  mapping.getJDBCTypeFor("boolean"));
        if ( vrijeRub1 == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("byte[]"));
       } else {
          prep.setObject(parmIndex++,  vrijeRub1,  mapping.getJDBCTypeFor("byte[]"));
}
        if ( vrijeRub2 == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("byte[]"));
       } else {
          prep.setObject(parmIndex++,  vrijeRub2,  mapping.getJDBCTypeFor("byte[]"));
}
        if ( vrijeRub3 == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("byte[]"));
       } else {
          prep.setObject(parmIndex++,  vrijeRub3,  mapping.getJDBCTypeFor("byte[]"));
}
        if ( vrijeRub4 == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("byte[]"));
       } else {
          prep.setObject(parmIndex++,  vrijeRub4,  mapping.getJDBCTypeFor("byte[]"));
}
        if ( vrijeRub5 == null) {
          prep.setNull(parmIndex++,  mapping.getJDBCTypeFor("byte[]"));
       } else {
          prep.setObject(parmIndex++,  vrijeRub5,  mapping.getJDBCTypeFor("byte[]"));
}

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.id,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Dagboek modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Dagboek.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Dagboek.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("Id",provider) + ", " + pmd.getFieldName("Soort",provider) + ", " + pmd.getFieldName("Omschrijving",provider) + ", " + pmd.getFieldName("BoekstukMask",provider) + ", " + pmd.getFieldName("LaatsteBoekstuk",provider) + ", " + pmd.getFieldName("Rekening",provider) + ", " + pmd.getFieldName("TegenRekening",provider) + ", " + pmd.getFieldName("SystemValue",provider) + ", " + pmd.getFieldName("VrijeRub1",provider) + ", " + pmd.getFieldName("VrijeRub2",provider) + ", " + pmd.getFieldName("VrijeRub3",provider) + ", " + pmd.getFieldName("VrijeRub4",provider) + ", " + pmd.getFieldName("VrijeRub5",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, id,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, soort,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5, omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6, boekstukMask,  mapping.getJDBCTypeFor("String"));
        prep.setObject(7, laatsteBoekstuk,  mapping.getJDBCTypeFor("String"));
        prep.setObject(8, rekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(9, tegenRekening,  mapping.getJDBCTypeFor("String"));
        prep.setObject(10,  new Boolean(systemValue),  mapping.getJDBCTypeFor("boolean"));
        if (vrijeRub1 == null)
          prep.setNull(11,  mapping.getJDBCTypeFor("byte[]"));
        else
          prep.setObject(11, vrijeRub1,  mapping.getJDBCTypeFor("byte[]"));
        if (vrijeRub2 == null)
          prep.setNull(12,  mapping.getJDBCTypeFor("byte[]"));
        else
          prep.setObject(12, vrijeRub2,  mapping.getJDBCTypeFor("byte[]"));
        if (vrijeRub3 == null)
          prep.setNull(13,  mapping.getJDBCTypeFor("byte[]"));
        else
          prep.setObject(13, vrijeRub3,  mapping.getJDBCTypeFor("byte[]"));
        if (vrijeRub4 == null)
          prep.setNull(14,  mapping.getJDBCTypeFor("byte[]"));
        else
          prep.setObject(14, vrijeRub4,  mapping.getJDBCTypeFor("byte[]"));
        if (vrijeRub5 == null)
          prep.setNull(15,  mapping.getJDBCTypeFor("byte[]"));
        else
          prep.setObject(15, vrijeRub5,  mapping.getJDBCTypeFor("byte[]"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.id = currentDBImage.id;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.id = transactionImage.dbImage.id;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        id = transactionImage.dbImage.id;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String id;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
    public Rekening getRekeningObject() throws Exception {
    	RekeningPK key = new RekeningPK();
    	key.setBedrijf(bedrijf);
    	key.setRekeningNr(rekening);
    	try {
			return RekeningManagerFactory.getInstance(dbd).findByPrimaryKey(key);
		} catch (Exception e) {
			return null;
		}
    }
    public boolean isBankboek() {
    	return soort.equals(DagboekSoortEnum.BANK);
    }
    public boolean isKasboek() {
    	return soort.equals(DagboekSoortEnum.KAS);
    }
    public boolean isMemoriaal() {
    	return soort.equals(DagboekSoortEnum.MEMO);
    }
    public boolean isInkoopboek() {
    	return soort.equals(DagboekSoortEnum.INKOOP);
    }
    public boolean isVerkoopboek() {
    	return soort.equals(DagboekSoortEnum.VERKOOP);
    }
    public BankImport getBankImport() throws Exception {
		if (isBankboek()) {
	    	BankImportPK key = new BankImportPK();
			key.setBedrijf(bedrijf);
			key.setDagboekId(id);
			return BankImportManagerFactory.getInstance(dbd).findOrCreate(key);
		} else {
			return null;
		}
    }
//{CODE-INSERT-END:955534258}
}
