package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import nl.eadmin.db.*;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


  public class BankImportManager_Impl extends BusinessObjectManager implements BankImportManager{


    

    private final static String APPLICATION ="eadmin";
    private static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    private static AttributeAccessor generalAccessor = new AttributeAccessor();
    private static boolean verbose = Log.debug();
    private static PersistenceMetaData pmd = PersistenceMetaDataBankImport.getInstance();
    private static Hashtable instances = new Hashtable();

    private DBData  dbd;

    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    private BankImportManager_Impl(DBData dbData){
       dbd = dbData;
    }


    public static BankImportManager getInstance(){
		return getInstance(null);
    }


    public static BankImportManager getInstance(DBData dbd){
		BankImportManager_Impl inst = (BankImportManager_Impl)instances.get(dbd==null?"":dbd);
		if (inst == null){
		  inst = new BankImportManager_Impl(dbd);
		  instances.put(dbd==null?"":dbd,inst);
		}
		return inst;
    }


    public DBData getDBData(){
       return dbd==null?DBData.getDefaultDBData(APPLICATION):dbd;
    }


    public BankImport create(BankImportDataBean bean)  throws Exception {
      BankImport_Impl impl = new BankImport_Impl(getDBData(),  bean);
        impl.assureStorage();
      return (BankImport)impl;
    }


    public BankImport create(String _bedrijf, String _dagboekId) throws Exception {
      BankImport_Impl obj = new BankImport_Impl(getDBData(), _bedrijf, _dagboekId);
      obj.assureStorage();
      return obj;
    }


    private BankImportPK getPK(ResultSet rs) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      BankImportPK inst =  new BankImportPK();
      inst.setBedrijf(rs.getString(pmd.getFieldName("Bedrijf",provider)));
      inst.setDagboekId(rs.getString(pmd.getFieldName("DagboekId",provider)));
      return inst; 
    }


    public BankImport findOrCreate (BankImportPK key) throws Exception {

      BankImport obj;
      try{
         obj = findByPrimaryKey( key);
      }catch (FinderException e){
         obj = create( key.getBedrijf (),  key.getDagboekId ());
      }

      return obj;
    }


    public void add(BankImportDataBean inst)  throws Exception {
        BankImport_Impl impl = new BankImport_Impl(getDBData(),  inst);
        impl.assureStorage();
    }


    public void removeByPrimaryKey (BankImportPK key) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      BankImport cachedObject = (BankImport)getCachedObjectByPrimaryKey(key);
      if (provider instanceof AtomicDBTransaction)
        dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate((BusinessObject_Impl)cachedObject);
      else
        dbc = provider.getConnection();
      String  pskey = "nl.eadmin.db.BankImport.removeByPrimaryKey";
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider) + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("DagboekId",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDagboekId());
          int x = prep.executeUpdate();

          if (x == 0) throw new RemoveException("Key not found");
          if (cachedObject != null){
          		DBPersistenceManager.removeFromCache(cachedObject);
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
    }


    public BankImport findByPrimaryKey (BankImportPK key) throws Exception {
      //Custom code
//{CODE-INSERT-BEGIN:-1586979883}
//{CODE-INSERT-END:-1586979883}

      BankImport bankImport = getCachedObjectByPrimaryKey(key);
       if (bankImport != null)
      	  return bankImport;
       ResultSet rs = null;
       try{
           rs = getResultSet(key);
           return (BankImport)getBusinessObject(rs);
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }      //Custom code
//{CODE-INSERT-BEGIN:-933211338}
//{CODE-INSERT-END:-933211338}

    }

    public ResultSet getResultSet(BankImportPK key) throws Exception{
      ResultSet rs = null;
      String  pskey = "nl.eadmin.db.BankImport.findByPrimaryKey1";
      Object inst = null;
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{

        PreparedStatement prep = dbc.getPreparedStatement(pskey);
        if (prep == null)
          prep = dbc.getPreparedStatement(pskey, 
                                           " SELECT * " + 
                                           " FROM " + provider.getPrefix() + pmd.getTableName(provider)  + 
                                           " WHERE "  + pmd.getFieldName("Bedrijf",provider) + "=?" + " AND "  + pmd.getFieldName("DagboekId",provider) + "=?" );
          prep.setString(1, key.getBedrijf());
          prep.setString(2, key.getDagboekId());
          prep.executeQuery();

          rs = prep.getResultSet();

          if (!rs.next()){
                throw new FinderException("Key not found");
          }
      }catch(java.sql.SQLException e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
          provider.returnConnection(dbc);
      }
      return rs;
    }
    public BankImport getCachedObjectByPrimaryKey (BankImportPK key) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache == null)
          return null;
       Object o = cache.get(new String("1456253405_"+ getDBData().getDBId() + key.getBedrijf() + key.getDagboekId()));
       return (BankImport)o;
    }



    public ConnectionProvider getConnectionProvider() throws Exception{
       return DBPersistenceManager.getConnectionProvider(getDBData());
    }


    public PersistenceMetaData getPersistenceMetaData(){
      return pmd;
    }


    public int generalUpdate(String setClause, String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String update = "UPDATE " + provider.getPrefix() + pmd.getTableName(provider)  + " SET " + QueryTranslator.translateObjectQuery(setClause + " WHERE " + whereClause,pmd,provider);
        return dbc.executeUpdate(update);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public int generalDelete(String whereClause) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc = provider.getConnection();
      try{
        String delete = "DELETE FROM " + provider.getPrefix() + pmd.getTableName(provider)  + " WHERE " + QueryTranslator.translateObjectQuery(whereClause,pmd,provider);
        return dbc.executeUpdate(delete);
      }catch(java.sql.SQLException e){
        Log.error(e.getMessage());
        throw e;
      }finally {
        provider.returnConnection(dbc);
      }
    }


    public BankImport getFirstObject(Query query) throws Exception{
      if (query == null)
         query = QueryFactory.create();
      int prevMax = query.getMaxObjects();
      query.setMaxObjects(1);
      try{
         return (BankImport) ((QueryImplementation)query).execute(this, RETURN_FIRST_OBJECT);
      }catch(Exception e){
         throw e;
      }finally{
         query.setMaxObjects(prevMax);
      }
    }

    public Collection getCollection(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Collection) queryImpl.execute(this, RETURN_COLLECTION);
    }

    public ListIterator getListIterator(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ListIterator) queryImpl.execute(this, RETURN_LISTITERATOR);
    }

    public Document getDocument(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (Document) queryImpl.execute(this, RETURN_DOCUMENT);
    }

    public DocumentFragment getDocumentFragment(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (DocumentFragment) queryImpl.execute(this, RETURN_DOCUMENT_FRAGMENT);
    }

    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception{
       if (query == null)
         query = QueryFactory.create(this,null,null,1000);
       QueryImplementation queryImpl = (QueryImplementation)query;
       return (ArrayList) queryImpl.execute(this,RETURN_DOCUMENT_FRAGMENT_ARRAYLIST );
    }



    protected Collection getCollection(ResultSet rs) throws Exception {
      ArrayList result = new ArrayListImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      return (Collection)result;
    }



    protected Document getDocument(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      Element root = document.getDocumentElement();
      collectResultElements(resultSet, root, getConnectionProvider());
      return document;
    };


    protected DocumentFragment getDocumentFragment(ResultSet resultSet) throws Exception {
      Document document = domImplementation.createDocument("", "data", null);
      DocumentFragment documentFragment = document.createDocumentFragment();
      collectResultElements(resultSet, documentFragment, getConnectionProvider());
      return documentFragment;
    };


    protected ArrayList getDocumentFragmentArrayList(ResultSet resultSet) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      Document document = domImplementation.createDocument("", "data", null);
      ArrayList arrayList = new ArrayListImpl();
      while (resultSet.next()){
        DocumentFragment documentFragment = document.createDocumentFragment();
        Node rec = documentFragment.appendChild(document.createElement("bankimport"));
        addBOFieldsToElement(resultSet, rec, provider);
        arrayList.add(documentFragment);
      }
      return arrayList;
    }


    protected ListIterator getListIterator(ResultSet rs) throws Exception {
      ListIterator result = new ListIteratorImpl();
      while (rs.next()){
        result.add(getBusinessObject(rs));
      } 
      ((ListIteratorImpl)result).pointer = 0; 
      return result; 
    }


    protected Object getBusinessObject(ResultSet rs) throws Exception {
       Cache cache = DBPersistenceManager.getCache();
       if (cache != null){
          ConnectionProvider provider = getConnectionProvider();
          Object o = cache.get(new String("1456253405_"+ getDBData().getDBId()+rs.getObject(pmd.getFieldName("Bedrijf",provider))+rs.getObject(pmd.getFieldName("DagboekId",provider))));
          if (o != null)
             return o;
       }
    	  return DBPersistenceManager.cache(createBusinessObject(rs));
    }

    private Object createBusinessObject(ResultSet rs) throws Exception {
      return new BankImport_Impl(getDBData(), rs);
    }


    private void collectResultElements(ResultSet rs, Node root, ConnectionProvider provider) throws Exception {
      Document doc = root.getOwnerDocument();
      while (rs.next()){
        Node rec = root.appendChild(doc.createElement("bankimport"));
        addBOFieldsToElement(rs,rec,provider);
      }
    }


    public static void addBOFieldsToElement(ResultSet rs, Node rec, ConnectionProvider provider) throws Exception {
      DBMapping mapping = provider.getDBMapping();
      Document doc = rec.getOwnerDocument();
      Object o = null;
      String s = null;
      if ((o=rs.getObject(pmd.getFieldName("mutationTimeStamp",provider))) != null && (s = String.valueOf(o).trim()).length() > 0)
         rec.appendChild(doc.createElement("mutationtimestamp")).appendChild(doc.createTextNode(s));
      if ((s=rs.getString(pmd.getFieldName("Bedrijf",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("bedrijf")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("DagboekId",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("dagboekid")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("IbanNr",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("ibannr")).appendChild(doc.createCDATASection(s));
      if ((s=rs.getString(pmd.getFieldName("Format",provider))) != null && (s = s.trim()).length() > 0)
         rec.appendChild(doc.createElement("format")).appendChild(doc.createCDATASection(s));
    }

    public nl.ibs.vegas.ExecutableQuery getExecutableQuery() {
    		return QueryFactory.create(this);
    }

    final static String[] keys = new String[]{ "bedrijf","dagboekId"};
    public String[] getKeyNames() {
    	return keys;
    }
    final static nl.ibs.vegas.meta.AttributeMeta[] attributes = new nl.ibs.vegas.meta.AttributeMeta[]{
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("bedrijf","bedrijf","bedrijf-short","bedrijf-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("dagboekId","dagboek-id","dagboek-id-short","dagboek-id-description","text",String.class,10,0,null,"false","false","true","upper", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("ibanNr","iban-nr","iban-nr-short","iban-nr-description","text",String.class,18,0,null,"false","false","false","mixed", false, false,null,null),
       new nl.ibs.vegas.meta.impl.FieldMetaImpl("format","format","format-short","format-description","text",String.class,10,0,null,"false","false","false","mixed", false, false,null,null)
    };
    public nl.ibs.vegas.meta.AttributeMeta[] getAttributes() {
    	return attributes;
    }
    public Class getClassType() {
    	return nl.eadmin.db.BankImport.class;
    }
    public String getNameField() {
    	return "dagboekId";
    }
    public String getDescriptionField() {
    	return "dagboekId";
    }
    public String getTextKey() {
    	return "bank-import";
    }
    public String getTextKeyShort() {
    	return "bank-import-short";
    }
    public String getTextKeyDescription() {
    	return "bank-import-description";
    }
    public nl.ibs.vegas.meta.AttributeMeta getAttribute(String name) {
    	if (attributes == null || name == null)
    		return null;
    	for (int i = 0; i < attributes.length; i++)
    		if (name.equals(attributes[i].getName()))
    			return attributes[i];
    	return null;
    }
  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
