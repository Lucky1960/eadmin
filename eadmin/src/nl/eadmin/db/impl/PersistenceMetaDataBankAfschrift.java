package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataBankAfschrift implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataBankAfschrift();

      persistables.add("nl.eadmin.db.impl.BankAfschrift_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("runNr");
      javaVariableNames.add("seqNr");
      javaVariableNames.add("afschriftNr");
      javaVariableNames.add("bladNr");
      javaVariableNames.add("boekdatum");
      javaVariableNames.add("rekening");
      javaVariableNames.add("tegenrekening");
      javaVariableNames.add("naam");
      javaVariableNames.add("dC");
      javaVariableNames.add("bedrag");
      javaVariableNames.add("muntsoort");
      javaVariableNames.add("hdrOmschr1");
      javaVariableNames.add("hdrOmschr2");
      javaVariableNames.add("dtlOmschr1");
      javaVariableNames.add("dtlOmschr2");
      javaVariableNames.add("dtlOmschr3");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("runNr");
      javaPKVariableNames.add("seqNr");

      fieldNames.put("mutationTimeStamp","BANKAFSCHR_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("RunNr","RUNNR");
      fieldNames.put("SeqNr","SEQNR");
      fieldNames.put("AfschriftNr","AFSCHRIFT");
      fieldNames.put("BladNr","BLADNR");
      fieldNames.put("Boekdatum","BOEKDTM");
      fieldNames.put("Rekening","REKNR");
      fieldNames.put("Tegenrekening","TREKNR");
      fieldNames.put("Naam","NAAM");
      fieldNames.put("DC","DC");
      fieldNames.put("Bedrag","BEDRAG");
      fieldNames.put("Muntsoort","MUNTSRT");
      fieldNames.put("HdrOmschr1","HDROMS1");
      fieldNames.put("HdrOmschr2","HDROMS2");
      fieldNames.put("DtlOmschr1","DTLOMS1");
      fieldNames.put("DtlOmschr2","DTLOMS2");
      fieldNames.put("DtlOmschr3","DTLOMS3");
      fieldNames.put("Remark","REMARK");
      fieldNames.put("InputRecord","INPUTRCD");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("RunNr","Runnummer");
      fieldDescriptions.put("SeqNr","Volgnummer");
      fieldDescriptions.put("AfschriftNr","Afschriftnummer");
      fieldDescriptions.put("BladNr","Bladnummer afschrift");
      fieldDescriptions.put("Boekdatum","Boekdatum");
      fieldDescriptions.put("Rekening","Rekening");
      fieldDescriptions.put("Tegenrekening","Tegenrekening");
      fieldDescriptions.put("Naam","Naam");
      fieldDescriptions.put("DC","Af/Bij");
      fieldDescriptions.put("Bedrag","Bedrag");
      fieldDescriptions.put("Muntsoort","ISO code muntsoort");
      fieldDescriptions.put("HdrOmschr1","Header omschrijving 1");
      fieldDescriptions.put("HdrOmschr2","Header omschrijving 2");
      fieldDescriptions.put("DtlOmschr1","Detail omschrijving 1");
      fieldDescriptions.put("DtlOmschr2","Detail omschrijving 2");
      fieldDescriptions.put("DtlOmschr3","Detail omschrijving 3");
      fieldDescriptions.put("Remark","Remark for import into database");
      fieldDescriptions.put("InputRecord","Original input-record");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("runNr","RunNr");
      javaToBONameMapping.put("seqNr","SeqNr");
      javaToBONameMapping.put("afschriftNr","AfschriftNr");
      javaToBONameMapping.put("bladNr","BladNr");
      javaToBONameMapping.put("boekdatum","Boekdatum");
      javaToBONameMapping.put("rekening","Rekening");
      javaToBONameMapping.put("tegenrekening","Tegenrekening");
      javaToBONameMapping.put("naam","Naam");
      javaToBONameMapping.put("dC","DC");
      javaToBONameMapping.put("bedrag","Bedrag");
      javaToBONameMapping.put("muntsoort","Muntsoort");
      javaToBONameMapping.put("hdrOmschr1","HdrOmschr1");
      javaToBONameMapping.put("hdrOmschr2","HdrOmschr2");
      javaToBONameMapping.put("dtlOmschr1","DtlOmschr1");
      javaToBONameMapping.put("dtlOmschr2","DtlOmschr2");
      javaToBONameMapping.put("dtlOmschr3","DtlOmschr3");
      javaToBONameMapping.put("remark","Remark");
      javaToBONameMapping.put("inputRecord","InputRecord");

    }

    private PersistenceMetaDataBankAfschrift(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "BankAfschrift";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("BankAfschrift" , "BANKAFSCHR");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "BankAfschrift" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Bankafschriften";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("RunNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 5, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("SeqNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("int", 5, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("AfschriftNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 5, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("BladNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 5, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Boekdatum", provider)	 + " " + provider.getDBMapping().getSQLDefinition("Date", 0, 0) + " DEFAULT '2001-01-01'" + " NOT NULL	," + 
				getFieldName("Rekening", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 18, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Tegenrekening", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 30, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Naam", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DC", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 1, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Bedrag", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("Muntsoort", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 3, 0) + " DEFAULT 'EUR'" + " NOT NULL	," + 
				getFieldName("HdrOmschr1", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("HdrOmschr2", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DtlOmschr1", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DtlOmschr2", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("DtlOmschr3", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Remark", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	," + 
				getFieldName("InputRecord", provider)	 + " " + provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) + "	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[20][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("RunNr", provider), provider.getDBMapping().getSQLDefinition("int", 5, 0) , "0"  , " NOT NULL", "Runnummer"};
      columnDefinitions[3]=new String[]{getFieldName("SeqNr", provider), provider.getDBMapping().getSQLDefinition("int", 5, 0) , "0"  , " NOT NULL", "Volgnummer"};
      columnDefinitions[4]=new String[]{getFieldName("AfschriftNr", provider), provider.getDBMapping().getSQLDefinition("String", 5, 0) , "''"  , " NOT NULL", "Afschriftnummer"};
      columnDefinitions[5]=new String[]{getFieldName("BladNr", provider), provider.getDBMapping().getSQLDefinition("String", 5, 0) , "''"  , " NOT NULL", "Bladnummer afschrift"};
      columnDefinitions[6]=new String[]{getFieldName("Boekdatum", provider), provider.getDBMapping().getSQLDefinition("Date", 0, 0) , "'2001-01-01'"  , " NOT NULL", "Boekdatum"};
      columnDefinitions[7]=new String[]{getFieldName("Rekening", provider), provider.getDBMapping().getSQLDefinition("String", 18, 0) , "''"  , " NOT NULL", "Rekening"};
      columnDefinitions[8]=new String[]{getFieldName("Tegenrekening", provider), provider.getDBMapping().getSQLDefinition("String", 30, 0) , "''"  , " NOT NULL", "Tegenrekening"};
      columnDefinitions[9]=new String[]{getFieldName("Naam", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Naam"};
      columnDefinitions[10]=new String[]{getFieldName("DC", provider), provider.getDBMapping().getSQLDefinition("String", 1, 0) , "''"  , " NOT NULL", "Af/Bij"};
      columnDefinitions[11]=new String[]{getFieldName("Bedrag", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Bedrag"};
      columnDefinitions[12]=new String[]{getFieldName("Muntsoort", provider), provider.getDBMapping().getSQLDefinition("String", 3, 0) , "'EUR'"  , " NOT NULL", "ISO code muntsoort"};
      columnDefinitions[13]=new String[]{getFieldName("HdrOmschr1", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Header omschrijving 1"};
      columnDefinitions[14]=new String[]{getFieldName("HdrOmschr2", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Header omschrijving 2"};
      columnDefinitions[15]=new String[]{getFieldName("DtlOmschr1", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Detail omschrijving 1"};
      columnDefinitions[16]=new String[]{getFieldName("DtlOmschr2", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Detail omschrijving 2"};
      columnDefinitions[17]=new String[]{getFieldName("DtlOmschr3", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Detail omschrijving 3"};
      columnDefinitions[18]=new String[]{getFieldName("Remark", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Remark for import into database"};
      columnDefinitions[19]=new String[]{getFieldName("InputRecord", provider), provider.getDBMapping().getSQLDefinition("byte[]", 0, 0) ,  null  , "", "Original input-record"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_BN_UPDTSL_N733089384", " INDEX  " + provider.getPrefix() + "JSQL_INDX_BN_UPDTSL_N733089384" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("RunNr", provider) + " , "  +  getFieldName("SeqNr", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_BANKAFSCHRIFT", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_BANKAFSCHRIFT PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("RunNr", provider) + " , "  +  getFieldName("SeqNr", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
