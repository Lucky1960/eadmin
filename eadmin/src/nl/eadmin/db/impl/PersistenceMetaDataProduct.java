package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataProduct implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataProduct();

      persistables.add("nl.eadmin.db.impl.Product_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("product");
      javaVariableNames.add("omschr1");
      javaVariableNames.add("omschr2");
      javaVariableNames.add("omschr3");
      javaVariableNames.add("eenheid");
      javaVariableNames.add("prijs");
      javaVariableNames.add("prijsInkoop");
      javaVariableNames.add("rekeningNr");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("product");

      fieldNames.put("mutationTimeStamp","PRODUCT_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("Product","PRODUCT");
      fieldNames.put("Omschr1","OMSCHR1");
      fieldNames.put("Omschr2","OMSCHR2");
      fieldNames.put("Omschr3","OMSCHR3");
      fieldNames.put("Eenheid","EENHEID");
      fieldNames.put("Prijs","PRIJS");
      fieldNames.put("PrijsInkoop","INKPRIJS");
      fieldNames.put("RekeningNr","REKNR");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("Product","Productcode");
      fieldDescriptions.put("Omschr1","Omschrijving 1");
      fieldDescriptions.put("Omschr2","Omschrijving 2");
      fieldDescriptions.put("Omschr3","Omschrijving 3");
      fieldDescriptions.put("Eenheid","Eenheid");
      fieldDescriptions.put("Prijs","Prijs per eenheid");
      fieldDescriptions.put("PrijsInkoop","Inkoopprijs");
      fieldDescriptions.put("RekeningNr","Rekeningnummer");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("product","Product");
      javaToBONameMapping.put("omschr1","Omschr1");
      javaToBONameMapping.put("omschr2","Omschr2");
      javaToBONameMapping.put("omschr3","Omschr3");
      javaToBONameMapping.put("eenheid","Eenheid");
      javaToBONameMapping.put("prijs","Prijs");
      javaToBONameMapping.put("prijsInkoop","PrijsInkoop");
      javaToBONameMapping.put("rekeningNr","RekeningNr");

    }

    private PersistenceMetaDataProduct(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "Product";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("Product" , "PRODUCT");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "Product" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Product";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Product", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 30, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschr1", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschr2", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschr3", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Eenheid", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Prijs", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("PrijsInkoop", provider)	 + " " + provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) + " DEFAULT 0.00" + " NOT NULL	," + 
				getFieldName("RekeningNr", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 9, 0) + " DEFAULT ''" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[10][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("Product", provider), provider.getDBMapping().getSQLDefinition("String", 30, 0) , "''"  , " NOT NULL", "Productcode"};
      columnDefinitions[3]=new String[]{getFieldName("Omschr1", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 1"};
      columnDefinitions[4]=new String[]{getFieldName("Omschr2", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 2"};
      columnDefinitions[5]=new String[]{getFieldName("Omschr3", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving 3"};
      columnDefinitions[6]=new String[]{getFieldName("Eenheid", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Eenheid"};
      columnDefinitions[7]=new String[]{getFieldName("Prijs", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Prijs per eenheid"};
      columnDefinitions[8]=new String[]{getFieldName("PrijsInkoop", provider), provider.getDBMapping().getSQLDefinition("BigDecimal", 15, 2) , "0.00"  , " NOT NULL", "Inkoopprijs"};
      columnDefinitions[9]=new String[]{getFieldName("RekeningNr", provider), provider.getDBMapping().getSQLDefinition("String", 9, 0) , "''"  , " NOT NULL", "Rekeningnummer"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_PRODUCT_UPDTSLCT", " INDEX  " + provider.getPrefix() + "JSQL_INDX_PRODUCT_UPDTSLCT" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Product", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_PRODUCT", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_PRODUCT PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("Product", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
