package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class CodeTabel_Impl  implements CodeTabel, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataCodeTabel.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijf;
    protected String tabelNaam;
    protected String omschrijving;
    protected String dataType;
    protected int lengte;
    protected int decimalen;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected CodeTabel_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected CodeTabel_Impl (DBData _dbd ,  String _bedrijf, String _tabelNaam) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(_bedrijf);
        setTabelNaam(_tabelNaam);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected CodeTabel_Impl (DBData _dbd, CodeTabelDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.CodeTabelDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate CodeTabel with " + bean.getClass().getName() + "!! Use nl.eadmin.db.CodeTabelDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijf(bean.getBedrijf());
        setTabelNaam(bean.getTabelNaam());
        setOmschrijving(bean.getOmschrijving());
        setDataType(bean.getDataType());
        setLengte(bean.getLengte());
        setDecimalen(bean.getDecimalen());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public CodeTabel_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijf = rs.getString(pmd.getFieldName("Bedrijf",provider));
      tabelNaam = rs.getString(pmd.getFieldName("TabelNaam",provider));
      omschrijving = rs.getString(pmd.getFieldName("Omschrijving",provider));
      dataType = rs.getString(pmd.getFieldName("DataType",provider));
      lengte = rs.getInt(pmd.getFieldName("Lengte",provider));
      decimalen = rs.getInt(pmd.getFieldName("Decimalen",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       CodeTabelPK key = new CodeTabelPK();
       key.setBedrijf(getBedrijf());
       key.setTabelNaam(getTabelNaam());
       return key;
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijf()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1280009472}
//{CODE-INSERT-END:-1280009472}
      return trim(bedrijf);
      //Custom code
//{CODE-INSERT-BEGIN:-1025590301}
//{CODE-INSERT-END:-1025590301}
    }


    public String getTabelNaam()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:147444231}
//{CODE-INSERT-END:147444231}
      return trim(tabelNaam);
      //Custom code
//{CODE-INSERT-BEGIN:275801532}
//{CODE-INSERT-END:275801532}
    }


    public String getOmschrijving()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:86851457}
//{CODE-INSERT-END:86851457}
      return omschrijving;
      //Custom code
//{CODE-INSERT-BEGIN:-1602574462}
//{CODE-INSERT-END:-1602574462}
    }


    public String getDataType()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-1877146240}
//{CODE-INSERT-END:-1877146240}
      return dataType;
      //Custom code
//{CODE-INSERT-BEGIN:1938006371}
//{CODE-INSERT-END:1938006371}
    }


    public int getLengte()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:2089922783}
//{CODE-INSERT-END:2089922783}
      return lengte;
      //Custom code
//{CODE-INSERT-BEGIN:363094500}
//{CODE-INSERT-END:363094500}
    }


    public int getDecimalen()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1009467398}
//{CODE-INSERT-END:1009467398}
      return decimalen;
      //Custom code
//{CODE-INSERT-BEGIN:1228715933}
//{CODE-INSERT-END:1228715933}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijf(String _bedrijf ) throws Exception {

      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.trim();
      if (_bedrijf !=  null)
         _bedrijf = _bedrijf.toUpperCase();
      if (_bedrijf == null)
          _bedrijf =  "";
      if (_bedrijf.equals(getBedrijf()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-369256948}
//{CODE-INSERT-END:-369256948}
      bedrijf =  _bedrijf;

      autoUpdate();

    }

    public void setTabelNaam(String _tabelNaam ) throws Exception {

      if (_tabelNaam !=  null)
         _tabelNaam = _tabelNaam.trim();
      if (_tabelNaam !=  null)
         _tabelNaam = _tabelNaam.toUpperCase();
      if (_tabelNaam == null)
          _tabelNaam =  "";
      if (_tabelNaam.equals(getTabelNaam()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-792708589}
//{CODE-INSERT-END:-792708589}
      tabelNaam =  _tabelNaam;

      autoUpdate();

    }

    public void setOmschrijving(String _omschrijving ) throws Exception {

      if (_omschrijving == null)
          _omschrijving =  "";
      if (_omschrijving.equals(getOmschrijving()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-524071947}
//{CODE-INSERT-END:-524071947}
      omschrijving =  _omschrijving;

      autoUpdate();

    }

    public void setDataType(String _dataType ) throws Exception {

      if (_dataType == null)
          _dataType =  "";
      if (_dataType.equals(getDataType()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:586378228}
//{CODE-INSERT-END:586378228}
      dataType =  _dataType;

      autoUpdate();

    }

    public void setLengte(int _lengte ) throws Exception {

      if (_lengte == getLengte())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:318186579}
//{CODE-INSERT-END:318186579}
      lengte =  _lengte;

      autoUpdate();

    }

    public void setDecimalen(int _decimalen ) throws Exception {

      if (_decimalen == getDecimalen())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:69314578}
//{CODE-INSERT-END:69314578}
      decimalen =  _decimalen;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijf =  "";
      tabelNaam =  "";
      omschrijving =  "";
      dataType =  "";
      lengte = 0;
      decimalen = 0;
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijf = bedrijf;
        currentDBImage.tabelNaam = tabelNaam;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("CodeTabel is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         CodeTabelPK key = new CodeTabelPK();
         key.setBedrijf(getBedrijf());
         key.setTabelNaam(getTabelNaam());
         ((nl.eadmin.db.impl.CodeTabelManager_Impl)nl.eadmin.db.impl.CodeTabelManager_Impl.getInstance(dbd)).removeByPrimaryKey(key);
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((CodeTabelManager_Impl)CodeTabelManager_Impl.getInstance(dbd)).getResultSet((CodeTabelPK)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("-945627981_"+ dbd.getDBId() + bedrijf + tabelNaam).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getCodeTabelDataBean()!
    */
    public CodeTabelDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public CodeTabelDataBean getCodeTabelDataBean() throws Exception {
      return getDataBean(null);
    }

    private CodeTabelDataBean getDataBean(CodeTabelDataBean bean) throws Exception {

      if (bean == null)
          bean = new CodeTabelDataBean();

      bean.setBedrijf(getBedrijf());
      bean.setTabelNaam(getTabelNaam());
      bean.setOmschrijving(getOmschrijving());
      bean.setDataType(getDataType());
      bean.setLengte(getLengte());
      bean.setDecimalen(getDecimalen());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.CodeTabel_Impl))
             return false;
          nl.eadmin.db.impl.CodeTabel_Impl bo = (nl.eadmin.db.impl.CodeTabel_Impl)object;
          if (bo.getBedrijf() == null){
              if (this.getBedrijf() != null)
                  return false;
          }else if (!bo.getBedrijf().equals(this.getBedrijf())){
              return false;
          }
          if (bo.getTabelNaam() == null){
              if (this.getTabelNaam() != null)
                  return false;
          }else if (!bo.getTabelNaam().equals(this.getTabelNaam())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(CodeTabelDataBean bean) throws Exception {
       set(bean);
    }


    public void set(CodeTabelDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijf(bean.getBedrijf());
           setTabelNaam(bean.getTabelNaam());
           setOmschrijving(bean.getOmschrijving());
           setDataType(bean.getDataType());
           setLengte(bean.getLengte());
           setDecimalen(bean.getDecimalen());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"CodeTabel is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.CodeTabel.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijf",provider) +  " = ? " + ", " + pmd.getFieldName("TabelNaam",provider) +  " = ? " + ", " + pmd.getFieldName("Omschrijving",provider) +  " = ? " + ", " + pmd.getFieldName("DataType",provider) +  " = ? " + ", " + pmd.getFieldName("Lengte",provider) +  " = ? " + ", " + pmd.getFieldName("Decimalen",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijf",provider) + " = ? " + " AND  " +  pmd.getFieldName("TabelNaam",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  tabelNaam,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  dataType,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  new Integer( lengte),  mapping.getJDBCTypeFor("int"));
        prep.setObject(parmIndex++,  new Integer( decimalen),  mapping.getJDBCTypeFor("int"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++, image.tabelNaam,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("CodeTabel modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.CodeTabel.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.CodeTabel.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijf",provider) + ", " + pmd.getFieldName("TabelNaam",provider) + ", " + pmd.getFieldName("Omschrijving",provider) + ", " + pmd.getFieldName("DataType",provider) + ", " + pmd.getFieldName("Lengte",provider) + ", " + pmd.getFieldName("Decimalen",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijf,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, tabelNaam,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5, dataType,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6,  new Integer(lengte),  mapping.getJDBCTypeFor("int"));
        prep.setObject(7,  new Integer(decimalen),  mapping.getJDBCTypeFor("int"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijf = currentDBImage.bedrijf;
        transactionImage.dbImage.tabelNaam = currentDBImage.tabelNaam;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijf = transactionImage.dbImage.bedrijf;
      currentDBImage.tabelNaam = transactionImage.dbImage.tabelNaam;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijf = transactionImage.dbImage.bedrijf;
        tabelNaam = transactionImage.dbImage.tabelNaam;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijf;
      private String tabelNaam;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
