package nl.eadmin.db.impl;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.*;
import java.util.*;

public class PersistenceMetaDataCodeTabelElement implements ExtendedPersistenceMetaData{
    private static Set javaVariableNames = new HashSet();
    private static Set javaPKVariableNames = new HashSet();
    private static Set persistables = new HashSet();
    private static Set backwardReferenceVariableNames = new HashSet();
    private static HashMap relationType = new HashMap();
    private static HashMap relationMultiplicity = new HashMap();
    private static HashMap fieldNames = new HashMap();
    private static HashMap fieldDescriptions = new HashMap();
    private static HashMap javaToBONameMapping = new HashMap();
    private static HashMap relationPMDs = new HashMap();
    private static HashMap associatedTables= new HashMap();
    private static HashMap associatedTableDefinitions= new HashMap();
    private static PersistenceMetaData inst;
    static {
      inst = new PersistenceMetaDataCodeTabelElement();

      persistables.add("nl.eadmin.db.impl.CodeTabelElement_Impl");

      javaVariableNames.add("mutationTimeStamp");
      javaVariableNames.add("bedrijf");
      javaVariableNames.add("tabelNaam");
      javaVariableNames.add("id");
      javaVariableNames.add("omschrijving");
      javaPKVariableNames.add("bedrijf");
      javaPKVariableNames.add("tabelNaam");
      javaPKVariableNames.add("id");

      fieldNames.put("mutationTimeStamp","CODETBLELM_RCRDMTTM");
      fieldNames.put("Bedrijf","BDR");
      fieldNames.put("TabelNaam","TABELNAAM");
      fieldNames.put("Id","ID");
      fieldNames.put("Omschrijving","OMSCHR");

      fieldDescriptions.put("mutationTimeStamp","Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)");
      fieldDescriptions.put("Bedrijf","Bedrijfscode");
      fieldDescriptions.put("TabelNaam","Tabelnaam");
      fieldDescriptions.put("Id","Naam code-element");
      fieldDescriptions.put("Omschrijving","Omschrijving");

      javaToBONameMapping.put("mutationTimeStamp","mutationTimeStamp");
      javaToBONameMapping.put("bedrijf","Bedrijf");
      javaToBONameMapping.put("tabelNaam","TabelNaam");
      javaToBONameMapping.put("id","Id");
      javaToBONameMapping.put("omschrijving","Omschrijving");

    }

    private PersistenceMetaDataCodeTabelElement(){
    }


    public static PersistenceMetaData getInstance(){
       return inst;
    }


    public PersistenceMetaData getPersistingPMD(){
       return inst;
    }

    public String getBusinessObjectName(){
       return "CodeTabelElement";
    }

    public String getTableName(String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableName(objectName, objectName.toUpperCase());
    }

    public String getTableName(ConnectionProvider provider){
       return provider.getORMapping().getTableName("CodeTabelElement" , "CODETBLELM");
    }

    public String getFieldDescription(String name){
       return (String)fieldDescriptions.get(name);
    }

    public String getFieldName(String fieldName, String objectName, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(fieldName, objectName, fieldName.toUpperCase());
    }

    public String getFieldName(String name, ConnectionProvider provider){
       return provider.getORMapping().getTableFieldName(name, "CodeTabelElement" , (String)fieldNames.get(name));
    }

    public String getFieldNameForJavaField(String javaFieldName, ConnectionProvider provider){
       return getFieldName((String)javaToBONameMapping.get(javaFieldName),provider);
    }

    public String getTableDescription(){
      return "Codetabel element";
    }

    public String getTableDefinition(ConnectionProvider provider){
      return new String("CREATE TABLE " + provider.getPrefix() + getTableName(provider) + " (" + 
				getFieldName("mutationTimeStamp", provider)	 + " " + provider.getDBMapping().getSQLDefinition("long", 18, 0) + " DEFAULT 0" + " NOT NULL	," + 
				getFieldName("Bedrijf", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("TabelNaam", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Id", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 10, 0) + " DEFAULT ''" + " NOT NULL	," + 
				getFieldName("Omschrijving", provider)	 + " " + provider.getDBMapping().getSQLDefinition("String", 50, 0) + " DEFAULT ''" + " NOT NULL	" + 
      ")");
    }

    public String[][] getColumnDefinitions(ConnectionProvider provider){
      String[][] columnDefinitions = new String[5][4];
      columnDefinitions[0]=new String[]{getFieldName("mutationTimeStamp", provider), provider.getDBMapping().getSQLDefinition("long", 18, 0) , "0"  , " NOT NULL", "Time of the last mutation (in miliseconds from 1-1-1970 00:00:00)"};
      columnDefinitions[1]=new String[]{getFieldName("Bedrijf", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Bedrijfscode"};
      columnDefinitions[2]=new String[]{getFieldName("TabelNaam", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Tabelnaam"};
      columnDefinitions[3]=new String[]{getFieldName("Id", provider), provider.getDBMapping().getSQLDefinition("String", 10, 0) , "''"  , " NOT NULL", "Naam code-element"};
      columnDefinitions[4]=new String[]{getFieldName("Omschrijving", provider), provider.getDBMapping().getSQLDefinition("String", 50, 0) , "''"  , " NOT NULL", "Omschrijving"};
      return columnDefinitions;
    }

    public Map getIndexDefinitions(ConnectionProvider provider){
      HashMap map = new HashMap();
      if (provider.getDBMapping().addConstraintForeignKeyColumns()){
      }
      map.put("JSQL_INDX_CD_UPDTSL_1854264455", " INDEX  " + provider.getPrefix() + "JSQL_INDX_CD_UPDTSL_1854264455" +" ON " + provider.getPrefix() + getTableName(provider)+ "(" +  getFieldName("mutationTimeStamp", provider) + " , "  +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("TabelNaam", provider) + " , "  +  getFieldName("Id", provider)+")" );
      return map;
    }

    public Map getConstraintDefinitions(ConnectionProvider provider){
       HashMap map = new HashMap();
       map.put("JSQL_PK_CODETABELELEMENT", " CONSTRAINT " + provider.getPrefix() + "JSQL_PK_CODETABELELEMENT PRIMARY KEY(" +  getFieldName("Bedrijf", provider) + " , "  +  getFieldName("TabelNaam", provider) + " , "  +  getFieldName("Id", provider)+")");
       return map;
    }


    public String[][] getRelationColomnPairs(String javaFieldName, ConnectionProvider provider){
       return null;
    }


    public String[][] getManyToManyColomnPairs(String javaFieldName, ConnectionProvider provider, String type){
       return null;
    }

    public PersistenceMetaData getRelationPersistenceMetaData(String javaFieldName){
       return (PersistenceMetaData)relationPMDs.get(javaFieldName);
    }

    public String getRelationType(String javaVariableNameOfRelation){
       return (String)relationType.get(javaVariableNameOfRelation);
    }

    public String getRelationMultiplicity(String javaVariableNameOfRelation){
       return (String)relationMultiplicity.get(javaVariableNameOfRelation);
    }

    public String getAssociationTableName(String javaVariableNameOfRelation,ConnectionProvider provider){
       return getTableName((String)associatedTables.get(javaVariableNameOfRelation),provider);
    }

    public boolean isBackwardReference(String javaVariableNameOfRelation){
       return backwardReferenceVariableNames.contains(javaVariableNameOfRelation);
    }

    public Set getJavaVariableNames(){
    	return javaVariableNames;
    }
    public Set getJavaPKFieldNames(){
    	return javaPKVariableNames;
    }
    public boolean containsJavaVariable(String javaFieldName){
         return javaVariableNames.contains(javaFieldName);
    }


    public boolean definesTable(){
       return true;
    }


    public boolean isPersistable(Object object){
       return persistables.contains(object.getClass().getName());
    }


    public String getClassNameConstraint(ConnectionProvider provider){
	   return null;
    }

    public Map getAssociativeTableDefinitions(ConnectionProvider provider){
       return new HashMap();
    }
}
