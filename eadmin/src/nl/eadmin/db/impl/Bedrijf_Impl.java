package nl.eadmin.db.impl;

import nl.eadmin.db.*;
import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;

import java.io.Serializable;
import java.math.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;

import nl.ibs.jeelog.*;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.Node;



/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
import nl.eadmin.enums.AdresTypeEnum;
import nl.eadmin.SessionKeys;
import nl.ibs.esp.servlet.ESPSessionContext;
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class Bedrijf_Impl  implements Bedrijf, BusinessObject_Impl, DBTransactionListener{


    

    private final static PersistenceMetaData pmd = PersistenceMetaDataBedrijf.getInstance();
    private final static AttributeAccessor generalAccessor = new AttributeAccessor();
    protected final static boolean verbose = Log.debug();
    protected final static DOMImplementation domImplementation = DBConfig.getDOMImplementation();
    protected final static boolean autoUpdate = true;
    protected final static boolean cacheRelations = DBConfig.getCacheObjectRelations();
    protected final static boolean deferUpdates = DBConfig.getDeferUpdates();
    protected boolean updateDefered;
    protected boolean holdUpdate;
    protected boolean dirty;
    protected boolean stored;
    protected boolean deleted;
    protected boolean underConstruction;
    protected boolean participatingInTransaction;
    protected int hashCode;
    protected transient java.util.HashMap relationalCache;
    private TransactionImage transactionImage;
    private DBImage currentDBImage;
    protected DBData dbd;

 //  instance variable declarations

    protected long mutationTimeStamp;
    protected String bedrijfscode;
    protected String omschrijving;
    protected String email;
    protected String kvkNr;
    protected String btwNr;
    protected String ibanNr;
    protected String bic;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
    private Adres myBezoekAdres, myPostAdres;
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */

    protected Bedrijf_Impl (DBData _dbd) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      //Custom code
//{CODE-INSERT-BEGIN:-341861524}
//{CODE-INSERT-END:-341861524}

    }

    protected Bedrijf_Impl (DBData _dbd ,  String _bedrijfscode) throws Exception {
      dbd = _dbd;
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijfscode(_bedrijfscode);
      //Custom code
//{CODE-INSERT-BEGIN:-1755318330}
//{CODE-INSERT-END:-1755318330}

      }finally{
        underConstruction=false;
      }
    }

    protected Bedrijf_Impl (DBData _dbd, BedrijfDataBean bean) throws Exception{
      dbd = _dbd;
      if (bean.getClass().getName().equals("nl.eadmin.db.BedrijfDataBean") == false)
         throw new IllegalArgumentException ("JSQL: Tried to instantiate Bedrijf with " + bean.getClass().getName() + "!! Use nl.eadmin.db.BedrijfDataBean instead !");
      initializeTheImplementationClass();
      try{
        underConstruction=true;
        setBedrijfscode(bean.getBedrijfscode());
        setOmschrijving(bean.getOmschrijving());
        setEmail(bean.getEmail());
        setKvkNr(bean.getKvkNr());
        setBtwNr(bean.getBtwNr());
        setIbanNr(bean.getIbanNr());
        setBic(bean.getBic());
        //Custom code
//{CODE-INSERT-BEGIN:930252410}
//{CODE-INSERT-END:930252410}

      }finally{
        underConstruction=false;
      }
    }

    public Bedrijf_Impl (DBData _dbd, ResultSet rs) throws Exception {
      dbd = _dbd;
      loadData(rs);
      //Custom code
//{CODE-INSERT-BEGIN:1639069445}
//{CODE-INSERT-END:1639069445}

      stored=true;
    }

    public void loadData(ResultSet rs) throws Exception {
      initializeTheImplementationClass();
      ConnectionProvider provider = getConnectionProvider();
      mutationTimeStamp = rs.getLong(pmd.getFieldName("mutationTimeStamp",provider));
      bedrijfscode = rs.getString(pmd.getFieldName("Bedrijfscode",provider));
      omschrijving = rs.getString(pmd.getFieldName("Omschrijving",provider));
      email = rs.getString(pmd.getFieldName("Email",provider));
      kvkNr = rs.getString(pmd.getFieldName("KvkNr",provider));
      btwNr = rs.getString(pmd.getFieldName("BtwNr",provider));
      ibanNr = rs.getString(pmd.getFieldName("IbanNr",provider));
      bic = rs.getString(pmd.getFieldName("Bic",provider));
      currentDBImage=null;
      //Custom code
//{CODE-INSERT-BEGIN:-2031089231}
//{CODE-INSERT-END:-2031089231}

    }

    private String trim(String in){
        return in==null?in:in.trim();
    }
    protected ConnectionProvider getConnectionProvider() throws Exception{
        return DBPersistenceManager.getConnectionProvider(dbd);
    }

    public Object getPrimaryKey () throws Exception {
       return getBedrijfscode();
    }


    public long getMutationTimeStamp()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-2091058407}
//{CODE-INSERT-END:-2091058407}
      return mutationTimeStamp;
      //Custom code
//{CODE-INSERT-BEGIN:-398303510}
//{CODE-INSERT-END:-398303510}
    }


    public String getBedrijfscode()throws Exception { 
       assureStorage();

      //Custom code
//{CODE-INSERT-BEGIN:-1338882264}
//{CODE-INSERT-END:-1338882264}
      return trim(bedrijfscode);
      //Custom code
//{CODE-INSERT-BEGIN:1444320443}
//{CODE-INSERT-END:1444320443}
    }


    public String getOmschrijving()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:86851457}
//{CODE-INSERT-END:86851457}
      return trim(omschrijving);
      //Custom code
//{CODE-INSERT-BEGIN:-1602574462}
//{CODE-INSERT-END:-1602574462}
    }


    public String getEmail()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-620155160}
//{CODE-INSERT-END:-620155160}
      return trim(email);
      //Custom code
//{CODE-INSERT-BEGIN:-2044943109}
//{CODE-INSERT-END:-2044943109}
    }


    public String getKvkNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-556899952}
//{CODE-INSERT-END:-556899952}
      return trim(kvkNr);
      //Custom code
//{CODE-INSERT-BEGIN:-84031661}
//{CODE-INSERT-END:-84031661}
    }


    public String getBtwNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:1399777237}
//{CODE-INSERT-END:1399777237}
      return trim(btwNr);
      //Custom code
//{CODE-INSERT-BEGIN:443419054}
//{CODE-INSERT-END:443419054}
    }


    public String getIbanNr()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:-828149274}
//{CODE-INSERT-END:-828149274}
      return trim(ibanNr);
      //Custom code
//{CODE-INSERT-BEGIN:97173949}
//{CODE-INSERT-END:97173949}
    }


    public String getBic()throws Exception { 

      //Custom code
//{CODE-INSERT-BEGIN:459631176}
//{CODE-INSERT-END:459631176}
      return bic;
      //Custom code
//{CODE-INSERT-BEGIN:1363662235}
//{CODE-INSERT-END:1363662235}
    }


    public void setMutationTimeStamp(long _mutationTimeStamp ) throws Exception {

      if (_mutationTimeStamp == getMutationTimeStamp())
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1247766747}
//{CODE-INSERT-END:-1247766747}
      mutationTimeStamp =  _mutationTimeStamp;

      autoUpdate();

    }

    public void setBedrijfscode(String _bedrijfscode ) throws Exception {

      if (_bedrijfscode !=  null)
         _bedrijfscode = _bedrijfscode.trim();
      if (_bedrijfscode !=  null)
         _bedrijfscode = _bedrijfscode.toUpperCase();
      if (_bedrijfscode == null)
          _bedrijfscode =  "";
      if (_bedrijfscode.equals(getBedrijfscode()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-1949805668}
//{CODE-INSERT-END:-1949805668}
      bedrijfscode =  _bedrijfscode;

      autoUpdate();

    }

    public void setOmschrijving(String _omschrijving ) throws Exception {

      if (_omschrijving !=  null)
         _omschrijving = _omschrijving.trim();
      if (_omschrijving == null)
          _omschrijving =  "";
      if (_omschrijving.equals(getOmschrijving()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-524071947}
//{CODE-INSERT-END:-524071947}
      omschrijving =  _omschrijving;

      autoUpdate();

    }

    public void setEmail(String _email ) throws Exception {

      if (_email !=  null)
         _email = _email.trim();
      if (_email == null)
          _email =  "";
      if (_email.equals(getEmail()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:153976052}
//{CODE-INSERT-END:153976052}
      email =  _email;

      autoUpdate();

    }

    public void setKvkNr(String _kvkNr ) throws Exception {

      if (_kvkNr !=  null)
         _kvkNr = _kvkNr.trim();
      if (_kvkNr == null)
          _kvkNr =  "";
      if (_kvkNr.equals(getKvkNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:217231260}
//{CODE-INSERT-END:217231260}
      kvkNr =  _kvkNr;

      autoUpdate();

    }

    public void setBtwNr(String _btwNr ) throws Exception {

      if (_btwNr !=  null)
         _btwNr = _btwNr.trim();
      if (_btwNr == null)
          _btwNr =  "";
      if (_btwNr.equals(getBtwNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:-2121058847}
//{CODE-INSERT-END:-2121058847}
      btwNr =  _btwNr;

      autoUpdate();

    }

    public void setIbanNr(String _ibanNr ) throws Exception {

      if (_ibanNr !=  null)
         _ibanNr = _ibanNr.trim();
      if (_ibanNr == null)
          _ibanNr =  "";
      if (_ibanNr.equals(getIbanNr()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:1695081818}
//{CODE-INSERT-END:1695081818}
      ibanNr =  _ibanNr;

      autoUpdate();

    }

    public void setBic(String _bic ) throws Exception {

      if (_bic !=  null)
         _bic = _bic.toUpperCase();
      if (_bic == null)
          _bic =  "";
      if (_bic.equals(getBic()))
         return;
      preSet();

      //Custom code
//{CODE-INSERT-BEGIN:371051348}
//{CODE-INSERT-END:371051348}
      bic =  _bic;

      autoUpdate();

    }


    private void initializeTheImplementationClass() throws Exception {
      mutationTimeStamp = 0;
      bedrijfscode =  "";
      omschrijving =  "";
      email =  "";
      kvkNr =  "";
      btwNr =  "";
      ibanNr =  "";
      bic =  "";
    }


    protected void updateDBImage() throws Exception{
      if (currentDBImage == null)
          currentDBImage = new DBImage();
        currentDBImage.mutationTimeStamp = mutationTimeStamp;
        currentDBImage.bedrijfscode = bedrijfscode;
      updateTransactionImage();
    }



    private void autoUpdate() throws Exception {
      if(underConstruction)
        return;
      if(deleted)
        throw new InvalidStateException("Bedrijf is deleted!");
      dirty = true;
      if(!deferUpdate() && autoUpdate && !holdUpdate)
        save();
    }


    public void assureStorage()throws Exception{
      if (stored || updateDefered)
      	return;
      boolean pre = holdUpdate;
      holdUpdate = false;
      autoUpdate();
      holdUpdate = pre;
    }


    public void delete() throws Exception {
      if(deleted)
        return;
      if (stored){
         ConnectionProvider provider = getConnectionProvider();
         if (participatingInTransaction == false && provider instanceof AtomicDBTransaction){
         	((AtomicDBTransaction)provider).addListener(this);
         	participatingInTransaction = true;
         	updateDefered = false;
         }
         ((nl.eadmin.db.impl.BedrijfManager_Impl)nl.eadmin.db.impl.BedrijfManager_Impl.getInstance(dbd)).removeByPrimaryKey(getBedrijfscode());
      }
      deleted=true;
    }


    public void refreshData() throws Exception{
       save();
       relationalCache=null;
       ResultSet rs = null;
       try{
          rs = ((BedrijfManager_Impl)BedrijfManager_Impl.getInstance(dbd)).getResultSet((String)getPrimaryKey ());
          loadData(rs);
       }catch(FinderException e){
          throw new DeletedException("Deleted by other user");
       }finally{
          if (rs != null)
             try{rs.close();}catch(Exception e){
Log.error(e.getMessage());
}
       }
    }

    public int hashCode(){
       if (hashCode == 0)
          try {
            hashCode = new String("1073670744_"+ dbd.getDBId() + bedrijfscode).hashCode();
          } catch (Exception e){
            throw new RuntimeException(e.getMessage());
          }
       return hashCode;
    }


    /**
    * @deprecated  replaced by getBedrijfDataBean()!
    */
    public BedrijfDataBean getDataBean() throws Exception {
      return getDataBean(null);
    }

    public BedrijfDataBean getBedrijfDataBean() throws Exception {
      return getDataBean(null);
    }

    private BedrijfDataBean getDataBean(BedrijfDataBean bean) throws Exception {

      if (bean == null)
          bean = new BedrijfDataBean();

      bean.setBedrijfscode(getBedrijfscode());
      bean.setOmschrijving(getOmschrijving());
      bean.setEmail(getEmail());
      bean.setKvkNr(getKvkNr());
      bean.setBtwNr(getBtwNr());
      bean.setIbanNr(getIbanNr());
      bean.setBic(getBic());

      return bean;
    }

    public boolean equals(Object object){
       try{
       	 if (object == null || !(object instanceof nl.eadmin.db.impl.Bedrijf_Impl))
             return false;
          nl.eadmin.db.impl.Bedrijf_Impl bo = (nl.eadmin.db.impl.Bedrijf_Impl)object;
          if (bo.getBedrijfscode() == null){
              if (this.getBedrijfscode() != null)
                  return false;
          }else if (!bo.getBedrijfscode().equals(this.getBedrijfscode())){
              return false;
          }
          if (bo.getClass() != getClass())
             return false;
          if (bo.getDBId() != getDBId())
             return false;
          return true;
       }catch (Exception e){
          throw new RuntimeException(e.getMessage());
       }
    }


    public void update(BedrijfDataBean bean) throws Exception {
       set(bean);
    }


    public void set(BedrijfDataBean bean) throws Exception {

       preSet();
       boolean pre = holdUpdate;
       try{
          holdUpdate = true;
           setBedrijfscode(bean.getBedrijfscode());
           setOmschrijving(bean.getOmschrijving());
           setEmail(bean.getEmail());
           setKvkNr(bean.getKvkNr());
           setBtwNr(bean.getBtwNr());
           setIbanNr(bean.getIbanNr());
           setBic(bean.getBic());
       }catch(Exception e){
          throw e;
       }finally{
          holdUpdate = pre;
       }
       autoUpdate();

    }


    public void save() throws Exception {

      if(deleted)
          throw new InvalidStateException(InvalidStateException.DELETED,"Bedrijf is deleted!");

      if (!stored){
         insert();
      } else if (dirty){
         update();
      }

      updateDefered=false;
    }


    private void update() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();
      DBImage image = currentDBImage;
      StringBuffer key = new StringBuffer("nl.eadmin.db.Bedrijf.save");

      try{
        long currentTime = System.currentTimeMillis();
        mutationTimeStamp = currentTime <= mutationTimeStamp?++mutationTimeStamp:currentTime;

        PreparedStatement prep = dbc.getPreparedStatement(key.toString());
        if (prep == null)
          prep = dbc.getPreparedStatement(key.toString(), 
                                           " UPDATE " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " SET " + pmd.getFieldName("mutationTimeStamp",provider) +  " = ? " + ", " + pmd.getFieldName("Bedrijfscode",provider) +  " = ? " + ", " + pmd.getFieldName("Omschrijving",provider) +  " = ? " + ", " + pmd.getFieldName("Email",provider) +  " = ? " + ", " + pmd.getFieldName("KvkNr",provider) +  " = ? " + ", " + pmd.getFieldName("BtwNr",provider) +  " = ? " + ", " + pmd.getFieldName("IbanNr",provider) +  " = ? " + ", " + pmd.getFieldName("Bic",provider) +  " = ? " + 
                                           " WHERE " +  pmd.getFieldName("mutationTimeStamp",provider) + " = ? " + " AND  " +  pmd.getFieldName("Bedrijfscode",provider) + " = ? ");

        int parmIndex = 1;
        prep.setObject(parmIndex++,  new Long( mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++,  bedrijfscode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  email,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  kvkNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  btwNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  ibanNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(parmIndex++,  bic,  mapping.getJDBCTypeFor("String"));

        prep.setObject(parmIndex++,  new Long(image.mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(parmIndex++, image.bedrijfscode,  mapping.getJDBCTypeFor("String"));

        int n = prep.executeUpdate();
        if (n == 0){
          rollback(true);
          throw new UpdateException("Bedrijf modified by other user!", this);
        }

        updateDBImage();
        dirty  = false;
        stored = true;
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(Exception e){
          if (!(e instanceof JSQLException)){
          	Log.error(e.getMessage());
          }
          throw e;
      }finally {
        provider.returnConnection(dbc);
      }

    }


    private void insert() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      DBMapping mapping = provider.getDBMapping();
      DBConnection dbc;
      stored = true;
      if (provider instanceof AtomicDBTransaction)
         dbc = ((AtomicDBTransaction)provider).getConnectionForUpdate(this);
      else
         dbc = provider.getConnection();

      try{

        PreparedStatement prep = dbc.getPreparedStatement("nl.eadmin.db.Bedrijf.insert");
        if (prep == null)
          prep = dbc.getPreparedStatement("nl.eadmin.db.Bedrijf.insert",  
                                           "INSERT INTO " +  provider.getPrefix() + pmd.getTableName(provider) + 
                                           " ( " + pmd.getFieldName("mutationTimeStamp",provider) + ", " + pmd.getFieldName("Bedrijfscode",provider) + ", " + pmd.getFieldName("Omschrijving",provider) + ", " + pmd.getFieldName("Email",provider) + ", " + pmd.getFieldName("KvkNr",provider) + ", " + pmd.getFieldName("BtwNr",provider) + ", " + pmd.getFieldName("IbanNr",provider) + ", " + pmd.getFieldName("Bic",provider)+ 
                                           ") VALUES (?,?,?,?,?,?,?,?)");

        prep.setObject(1,  new Long(mutationTimeStamp),  mapping.getJDBCTypeFor("long"));
        prep.setObject(2, bedrijfscode,  mapping.getJDBCTypeFor("String"));
        prep.setObject(3, omschrijving,  mapping.getJDBCTypeFor("String"));
        prep.setObject(4, email,  mapping.getJDBCTypeFor("String"));
        prep.setObject(5, kvkNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(6, btwNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(7, ibanNr,  mapping.getJDBCTypeFor("String"));
        prep.setObject(8, bic,  mapping.getJDBCTypeFor("String"));

        prep.executeUpdate();

        updateDBImage();
        dirty  = false;
        DBPersistenceManager.cache(this);
        if (provider instanceof AtomicDBTransaction && participatingInTransaction == false){
           ((AtomicDBTransaction)provider).addListener(this);
           participatingInTransaction = true;
        }

      }catch(CreateException e){
          stored = false;
          throw e;

      }catch(Exception e){
          stored = false;
          Log.error(e.getMessage());
          throw e;
      }finally {
         provider.returnConnection(dbc);
      }

    }



    public void preSet() throws Exception {
      if (underConstruction)
          return;
      validateProvider();
      if (currentDBImage == null && stored)
          updateDBImage();
      dirty = true;
    }



    private void validateProvider() throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if (provider instanceof  AtomicDBTransaction && !((AtomicDBTransaction)provider).isActive())
          throw new Exception("Current transaction set but not active! Start transaction before modifying BusinessObjects!");
    }


    protected void updateTransactionImage()throws Exception{
       updateTransactionImage(false);
    }

    protected void updateTransactionImage(boolean forse) throws Exception {
      ConnectionProvider provider = getConnectionProvider();
      if ((transactionImage == null || forse) && provider instanceof AtomicDBTransaction){
        if (transactionImage == null)
           transactionImage = new TransactionImage();
        if (transactionImage.dbImage == null)
           transactionImage.dbImage = new DBImage();
        transactionImage.dbImage.mutationTimeStamp = currentDBImage.mutationTimeStamp;
        transactionImage.dbImage.bedrijfscode = currentDBImage.bedrijfscode;
        transactionImage.stored = stored;
        transactionImage.deleted = deleted;
      }
    }



    public void rollback(boolean includeBOFields) throws Exception {
      if(relationalCache != null)
          relationalCache.clear();
      updateDefered = false;
      if (transactionImage == null )
          return;
      currentDBImage.mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
      currentDBImage.bedrijfscode = transactionImage.dbImage.bedrijfscode;
      if (includeBOFields){
        mutationTimeStamp = transactionImage.dbImage.mutationTimeStamp;
        bedrijfscode = transactionImage.dbImage.bedrijfscode;
      }
      stored = transactionImage.stored;
      deleted = transactionImage.deleted;
      participatingInTransaction = false;
      if (stored == false)
         DBPersistenceManager.removeFromCache(this);
    }



    public void commit() throws Exception {
      if (deleted)
        DBPersistenceManager.removeFromCache(this);
      else
        updateTransactionImage(true);
      participatingInTransaction = false;
      updateDefered = false;
    }


    public PersistenceMetaData getPersistenceMetaData(){
       return pmd;
    }


    public boolean isDeleted(){
       return deleted;
    }


    public int getDBId(){
       return dbd.getDBId();
    }


    public DBData getDBData(){
       return dbd;
    }


    protected boolean deferUpdate() throws Exception {
      if (!deferUpdates){
      	return false;
      }else if (!updateDefered){
        ConnectionProvider provider = getConnectionProvider();
        if (provider instanceof AtomicDBTransaction){
          ((AtomicDBTransaction)provider).setDeferedForUpdate(this);
          updateDefered=true;
        }
      }
      return updateDefered;
    }



    protected boolean isCachedRelation(String relation){
      	return relationalCache==null?false:relationalCache.containsKey(relation);
    }

    public Object addCachedRelationObject(String relation, Object object){
      	if(!cacheRelations)
       		return object;
      	if(relationalCache==null)
       		relationalCache = new java.util.HashMap();
      	if(object instanceof ArrayListImpl)
       		((ArrayListImpl)object).fixate();
       	relationalCache.put(relation,object);
       	return object;
    }

    protected Object getCachedRelationObject(String relation){
      	if (relationalCache==null)
      	   return null;
      	Object o = relationalCache.get(relation);
      	if (o instanceof BusinessObject_Impl){
      	   return ((BusinessObject_Impl)o).isDeleted()?null:o;
      	}else if (o instanceof ArrayListImpl){
      	   ArrayList list = ((ArrayListImpl)o).getBaseCopy();
      	   for (int x = 0; x < list.size();x++){
      	      if (((BusinessObject_Impl)list.get(x)).isDeleted())
      	   		  list.remove(x--);
      	   }
      	   return list;
      	}
      	return o;
    }

    public void clearCachedRelation(String relation){
      	if(relationalCache != null)
       		relationalCache.remove(relation);
    }



    private static final class DBImage implements Serializable {
      private long mutationTimeStamp;
      private String bedrijfscode;
    }



    private static final class TransactionImage implements Serializable{
      private DBImage dbImage;
      boolean stored;
      boolean deleted;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
    public Dagboek getDagboek(String id) throws Exception {
    	DagboekPK key = new DagboekPK();
    	key.setBedrijf(bedrijfscode);
    	key.setId(id);
    	try {
			return DagboekManagerFactory.getInstance(dbd).findByPrimaryKey(key);
		} catch (FinderException e) {
			return null;
		}
    }

    public Instellingen getInstellingen() throws Exception {
    	return InstellingenManagerFactory.getInstance(dbd).findOrCreate(bedrijfscode);
    }

    public String getOmschrijvingLang() throws Exception {
    	return bedrijfscode + " - " + omschrijving;
    }
    
    public Adres getBezoekAdres() throws Exception {
    	if (myBezoekAdres == null) {
    		AdresPK key = new AdresPK();
    		key.setBedrijf(getBedrijfscode());
    		key.setDc("");
    		key.setDcNr("");
    		key.setAdresType(AdresTypeEnum.ADRESTYPE_BEZOEK);
    		myBezoekAdres = AdresManagerFactory.getInstance(dbd).findOrCreate(key);
    	}
    	return myBezoekAdres;
    }

    public Adres getPostAdres() throws Exception {
    	if (myPostAdres == null) {
    		AdresPK key = new AdresPK();
    		key.setBedrijf(getBedrijfscode());
    		key.setDc("");
    		key.setDcNr("");
    		key.setAdresType(AdresTypeEnum.ADRESTYPE_POST);
    		myPostAdres = AdresManagerFactory.getInstance(dbd).findOrCreate(key);
    	}
    	return myPostAdres;
    }

    public String getAdresRegel1() throws Exception {
    	return getBezoekAdres().getAdresRegel1();
    }

    public String getAdresRegel2() throws Exception {
    	return getBezoekAdres().getAdresRegel2();
    }

    public String getPlaats() throws Exception {
    	return getBezoekAdres().getPlaats();
    }

    public boolean getBedrijfActief() throws Exception {
    	Bedrijf bedrijf = (Bedrijf) ESPSessionContext.getSessionAttribute(SessionKeys.BEDRIJF);
    	return (bedrijf != null && bedrijfscode.equals(bedrijf.getBedrijfscode()));
    }
//{CODE-INSERT-END:955534258}
}
