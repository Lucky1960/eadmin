package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Gebruiker extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String BOEKJAAR = "boekjaar";
    public final String ID = "id";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String NAAM = "naam";
    public final String WACHTWOORD = "wachtwoord";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getId()throws Exception;
    public String getNaam()throws Exception;
    public String getWachtwoord()throws Exception;
    public String getBedrijf()throws Exception;
    public int getBoekjaar()throws Exception;

    public void setNaam(String _naam )throws Exception;
    public void setWachtwoord(String _wachtwoord )throws Exception;
    public void setBedrijf(String _bedrijf )throws Exception;
    public void setBoekjaar(int _boekjaar )throws Exception;

    /**
    * @deprecated  replaced by getGebruikerDataBean()!
    */
    public GebruikerDataBean getDataBean()throws Exception;
    public GebruikerDataBean getGebruikerDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(GebruikerDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
	public boolean isAdmin() throws Exception;
	public DBData determineDBData() throws Exception;
//{CODE-INSERT-END:955534258}
}
