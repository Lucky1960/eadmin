package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Hoofdverdichting extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String ID = "id";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";
    public final String REKENINGSOORT = "rekeningsoort";

//{CODE-INSERT-BEGIN:-1274708295}
    public final String REKENINGSOORT_FULL = "rekeningsoortFull";
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public int getId()throws Exception;
    public String getOmschrijving()throws Exception;
    public String getRekeningsoort()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setRekeningsoort(String _rekeningsoort )throws Exception;

    /**
    * @deprecated  replaced by getHoofdverdichtingDataBean()!
    */
    public HoofdverdichtingDataBean getDataBean()throws Exception;
    public HoofdverdichtingDataBean getHoofdverdichtingDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(HoofdverdichtingDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public String getRekeningsoortFull()throws Exception;
//{CODE-INSERT-END:955534258}
}
