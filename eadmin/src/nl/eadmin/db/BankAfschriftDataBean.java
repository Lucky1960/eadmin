package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
import nl.eadmin.ApplicationConstants;
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class BankAfschriftDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected int runNr = 0;
    protected int seqNr = 0;
    protected String afschriftNr =  "";
    protected String bladNr =  "";
    protected Date boekdatum =  java.sql.Date.valueOf("2001-01-01");
    protected String rekening =  "";
    protected String tegenrekening =  "";
    protected String naam =  "";
    protected String dC =  "";
    protected BigDecimal bedrag =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected String muntsoort =  "EUR";
    protected String hdrOmschr1 =  "";
    protected String hdrOmschr2 =  "";
    protected String dtlOmschr1 =  "";
    protected String dtlOmschr2 =  "";
    protected String dtlOmschr3 =  "";
    protected String remark;
    protected String inputRecord;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
	private static final long serialVersionUID = -5312860835366392255L;
    private boolean writeFlag;
    private BigDecimal ZERO = BigDecimal.ZERO.setScale(2);
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public BankAfschriftDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public int getRunNr (){
      return runNr;
    }

    public int getSeqNr (){
      return seqNr;
    }

    public String getAfschriftNr (){
      return afschriftNr;
    }

    public String getBladNr (){
      return bladNr;
    }

    public Date getBoekdatum (){
      return boekdatum;
    }

    public String getRekening (){
      return rekening;
    }

    public String getTegenrekening (){
      return tegenrekening;
    }

    public String getNaam (){
      return naam;
    }

    public String getDC (){
      return dC;
    }

    public BigDecimal getBedrag (){
      return bedrag;
    }

    public String getMuntsoort (){
      return muntsoort;
    }

    public String getHdrOmschr1 (){
      return hdrOmschr1;
    }

    public String getHdrOmschr2 (){
      return hdrOmschr2;
    }

    public String getDtlOmschr1 (){
      return dtlOmschr1;
    }

    public String getDtlOmschr2 (){
      return dtlOmschr2;
    }

    public String getDtlOmschr3 (){
      return dtlOmschr3;
    }

    public String getRemark (){
      return remark;
    }

    public String getInputRecord (){
      return inputRecord;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setRunNr (int _runNr ){
      runNr = _runNr;
    }

    public void setSeqNr (int _seqNr ){
      seqNr = _seqNr;
    }

    public void setAfschriftNr (String _afschriftNr ){
      afschriftNr = _afschriftNr;
    }

    public void setBladNr (String _bladNr ){
      bladNr = _bladNr;
    }

    public void setBoekdatum (Date _boekdatum ){
      boekdatum = _boekdatum;
    }

    public void setRekening (String _rekening ){
      rekening = _rekening;
    }

    public void setTegenrekening (String _tegenrekening ){
      tegenrekening = _tegenrekening;
    }

    public void setNaam (String _naam ){
      naam = _naam;
    }

    public void setDC (String _dC ){
      dC = _dC;
    }

    public void setBedrag (BigDecimal _bedrag ){
      bedrag = _bedrag;
    }

    public void setMuntsoort (String _muntsoort ){
      muntsoort = _muntsoort;
    }

    public void setHdrOmschr1 (String _hdrOmschr1 ){
      hdrOmschr1 = _hdrOmschr1;
    }

    public void setHdrOmschr2 (String _hdrOmschr2 ){
      hdrOmschr2 = _hdrOmschr2;
    }

    public void setDtlOmschr1 (String _dtlOmschr1 ){
      dtlOmschr1 = _dtlOmschr1;
    }

    public void setDtlOmschr2 (String _dtlOmschr2 ){
      dtlOmschr2 = _dtlOmschr2;
    }

    public void setDtlOmschr3 (String _dtlOmschr3 ){
      dtlOmschr3 = _dtlOmschr3;
    }

    public void setRemark (String _remark ){
      remark = _remark;
    }

    public void setInputRecord (String _inputRecord ){
      inputRecord = _inputRecord;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
	public void setHeaderOmschrijving(String s) throws Exception {
		int len = s.length();
		if (len <= 50) {
			setHdrOmschr1(s);
			setHdrOmschr2("");
		} else if (len <= 100) {
			setHdrOmschr1(s.substring(0, 50));
			setHdrOmschr2(s.substring(50));
		} else {
			setHdrOmschr1(s.substring(0, 50));
			setHdrOmschr2(s.substring(50, 100));
		}
	}

	public void setDetailOmschrijving(String s) throws Exception {
		int len = s.length();
		if (len <= 50) {
			setDtlOmschr1(s);
			setDtlOmschr2("");
			setDtlOmschr3("");
		} else if (len <= 100) {
			setDtlOmschr1(s.substring(0, 50));
			setDtlOmschr2(s.substring(50));
			setDtlOmschr3("");
		} else if (len <= 150) {
			setDtlOmschr1(s.substring(0, 50));
			setDtlOmschr2(s.substring(50, 100));
			setDtlOmschr3(s.substring(100));
		} else {
			setDtlOmschr1(s.substring(0, 50));
			setDtlOmschr2(s.substring(50, 100));
			setDtlOmschr3(s.substring(100, 150));
		}
	}

    public void reset() {
    	remark = null;
    	inputRecord = null;
    	writeFlag = false;
    	boekdatum = null;
    	bedrag = ZERO;
        hdrOmschr1 = "";
        hdrOmschr2 = "";
        dtlOmschr1 = "";
        dtlOmschr2 = "";
        dtlOmschr3 = "";
        dC = "";
        naam = "";
        tegenrekening = "";
    }

	public boolean inError() throws Exception {
		return remark != null && remark.length() > 0;
	}

	public void addRemark(String s) throws Exception {
		if (remark == null) {
			remark = "";
		}
		remark += (" - " + s + ApplicationConstants.EOL);
	}

    public boolean getWriteFlag() {
    	return writeFlag;
    }

    public void setWriteFlag(boolean b) {
    	writeFlag = b;
    }
//{CODE-INSERT-END:955534258}
}
