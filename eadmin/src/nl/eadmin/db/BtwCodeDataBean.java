package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class BtwCodeDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String code =  "";
    protected String omschrijving =  "";
    protected BigDecimal percentage =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected String inEx;
    protected String btwCatInkoop =  "";
    protected String btwCatVerkoop =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public BtwCodeDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getCode (){
      return code;
    }

    public String getOmschrijving (){
      return omschrijving;
    }

    public BigDecimal getPercentage (){
      return percentage;
    }

    public String getInEx (){
      return inEx;
    }

    public String getBtwCatInkoop (){
      return btwCatInkoop;
    }

    public String getBtwCatVerkoop (){
      return btwCatVerkoop;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setCode (String _code ){
      code = _code;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

    public void setPercentage (BigDecimal _percentage ){
      percentage = _percentage;
    }

    public void setInEx (String _inEx ){
      inEx = _inEx;
    }

    public void setBtwCatInkoop (String _btwCatInkoop ){
      btwCatInkoop = _btwCatInkoop;
    }

    public void setBtwCatVerkoop (String _btwCatVerkoop ){
      btwCatVerkoop = _btwCatVerkoop;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
