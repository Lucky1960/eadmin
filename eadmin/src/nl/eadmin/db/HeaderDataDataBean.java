package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class HeaderDataDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String dagboekId =  "";
    protected String dagboekSoort =  "";
    protected String boekstuk =  "";
    protected int status = 10;
    protected boolean favoriet =  false;
    protected Date boekdatum =  java.sql.Date.valueOf("2001-01-01");
    protected int boekjaar = 0;
    protected int boekperiode = 0;
    protected String dcNummer =  "";
    protected String factuurNummer =  "";
    protected String referentieNummer =  "";
    protected Date factuurdatum;
    protected String factuurLayout =  "";
    protected Date vervaldatum;
    protected int aantalAanmaningen = 0;
    protected String dC =  "";
    protected BigDecimal totaalExcl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal totaalBtw =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal totaalIncl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal totaalGeboekt =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal totaalBetaald =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal totaalOpenstaand =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal totaalDebet =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal totaalCredit =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal beginsaldo =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal eindsaldo =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected String omschr1 =  "";
    protected String omschr2 =  "";
    protected String vrijeRub1 =  "";
    protected String vrijeRub2 =  "";
    protected String vrijeRub3 =  "";
    protected String vrijeRub4 =  "";
    protected String vrijeRub5 =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public HeaderDataDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getDagboekId (){
      return dagboekId;
    }

    public String getDagboekSoort (){
      return dagboekSoort;
    }

    public String getBoekstuk (){
      return boekstuk;
    }

    public int getStatus (){
      return status;
    }

    public boolean getFavoriet (){
      return favoriet;
    }

    public Date getBoekdatum (){
      return boekdatum;
    }

    public int getBoekjaar (){
      return boekjaar;
    }

    public int getBoekperiode (){
      return boekperiode;
    }

    public String getDcNummer (){
      return dcNummer;
    }

    public String getFactuurNummer (){
      return factuurNummer;
    }

    public String getReferentieNummer (){
      return referentieNummer;
    }

    public Date getFactuurdatum (){
      return factuurdatum;
    }

    public String getFactuurLayout (){
      return factuurLayout;
    }

    public Date getVervaldatum (){
      return vervaldatum;
    }

    public int getAantalAanmaningen (){
      return aantalAanmaningen;
    }

    public String getDC (){
      return dC;
    }

    public BigDecimal getTotaalExcl (){
      return totaalExcl;
    }

    public BigDecimal getTotaalBtw (){
      return totaalBtw;
    }

    public BigDecimal getTotaalIncl (){
      return totaalIncl;
    }

    public BigDecimal getTotaalGeboekt (){
      return totaalGeboekt;
    }

    public BigDecimal getTotaalBetaald (){
      return totaalBetaald;
    }

    public BigDecimal getTotaalOpenstaand (){
      return totaalOpenstaand;
    }

    public BigDecimal getTotaalDebet (){
      return totaalDebet;
    }

    public BigDecimal getTotaalCredit (){
      return totaalCredit;
    }

    public BigDecimal getBeginsaldo (){
      return beginsaldo;
    }

    public BigDecimal getEindsaldo (){
      return eindsaldo;
    }

    public String getOmschr1 (){
      return omschr1;
    }

    public String getOmschr2 (){
      return omschr2;
    }

    public String getVrijeRub1 (){
      return vrijeRub1;
    }

    public String getVrijeRub2 (){
      return vrijeRub2;
    }

    public String getVrijeRub3 (){
      return vrijeRub3;
    }

    public String getVrijeRub4 (){
      return vrijeRub4;
    }

    public String getVrijeRub5 (){
      return vrijeRub5;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ){
      dagboekId = _dagboekId;
    }

    public void setDagboekSoort (String _dagboekSoort ){
      dagboekSoort = _dagboekSoort;
    }

    public void setBoekstuk (String _boekstuk ){
      boekstuk = _boekstuk;
    }

    public void setStatus (int _status ){
      status = _status;
    }

    public void setFavoriet (boolean _favoriet ){
      favoriet = _favoriet;
    }

    public void setBoekdatum (Date _boekdatum ){
      boekdatum = _boekdatum;
    }

    public void setBoekjaar (int _boekjaar ){
      boekjaar = _boekjaar;
    }

    public void setBoekperiode (int _boekperiode ){
      boekperiode = _boekperiode;
    }

    public void setDcNummer (String _dcNummer ){
      dcNummer = _dcNummer;
    }

    public void setFactuurNummer (String _factuurNummer ){
      factuurNummer = _factuurNummer;
    }

    public void setReferentieNummer (String _referentieNummer ){
      referentieNummer = _referentieNummer;
    }

    public void setFactuurdatum (Date _factuurdatum ){
      factuurdatum = _factuurdatum;
    }

    public void setFactuurLayout (String _factuurLayout ){
      factuurLayout = _factuurLayout;
    }

    public void setVervaldatum (Date _vervaldatum ){
      vervaldatum = _vervaldatum;
    }

    public void setAantalAanmaningen (int _aantalAanmaningen ){
      aantalAanmaningen = _aantalAanmaningen;
    }

    public void setDC (String _dC ){
      dC = _dC;
    }

    public void setTotaalExcl (BigDecimal _totaalExcl ){
      totaalExcl = _totaalExcl;
    }

    public void setTotaalBtw (BigDecimal _totaalBtw ){
      totaalBtw = _totaalBtw;
    }

    public void setTotaalIncl (BigDecimal _totaalIncl ){
      totaalIncl = _totaalIncl;
    }

    public void setTotaalGeboekt (BigDecimal _totaalGeboekt ){
      totaalGeboekt = _totaalGeboekt;
    }

    public void setTotaalBetaald (BigDecimal _totaalBetaald ){
      totaalBetaald = _totaalBetaald;
    }

    public void setTotaalOpenstaand (BigDecimal _totaalOpenstaand ){
      totaalOpenstaand = _totaalOpenstaand;
    }

    public void setTotaalDebet (BigDecimal _totaalDebet ){
      totaalDebet = _totaalDebet;
    }

    public void setTotaalCredit (BigDecimal _totaalCredit ){
      totaalCredit = _totaalCredit;
    }

    public void setBeginsaldo (BigDecimal _beginsaldo ){
      beginsaldo = _beginsaldo;
    }

    public void setEindsaldo (BigDecimal _eindsaldo ){
      eindsaldo = _eindsaldo;
    }

    public void setOmschr1 (String _omschr1 ){
      omschr1 = _omschr1;
    }

    public void setOmschr2 (String _omschr2 ){
      omschr2 = _omschr2;
    }

    public void setVrijeRub1 (String _vrijeRub1 ){
      vrijeRub1 = _vrijeRub1;
    }

    public void setVrijeRub2 (String _vrijeRub2 ){
      vrijeRub2 = _vrijeRub2;
    }

    public void setVrijeRub3 (String _vrijeRub3 ){
      vrijeRub3 = _vrijeRub3;
    }

    public void setVrijeRub4 (String _vrijeRub4 ){
      vrijeRub4 = _vrijeRub4;
    }

    public void setVrijeRub5 (String _vrijeRub5 ){
      vrijeRub5 = _vrijeRub5;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
