package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class PeriodeDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected int boekjaar = 0;
    protected int boekperiode = 0;
    protected Date startdatum;
    protected Date einddatum =  java.sql.Date.valueOf("0001-01-01");


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public PeriodeDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public int getBoekjaar (){
      return boekjaar;
    }

    public int getBoekperiode (){
      return boekperiode;
    }

    public Date getStartdatum (){
      return startdatum;
    }

    public Date getEinddatum (){
      return einddatum;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setBoekjaar (int _boekjaar ){
      boekjaar = _boekjaar;
    }

    public void setBoekperiode (int _boekperiode ){
      boekperiode = _boekperiode;
    }

    public void setStartdatum (Date _startdatum ){
      startdatum = _startdatum;
    }

    public void setEinddatum (Date _einddatum ){
      einddatum = _einddatum;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
