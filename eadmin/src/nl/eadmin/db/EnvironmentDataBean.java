package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class EnvironmentDataBean implements java.io.Serializable {


    protected String dtaLib =  "";
    protected String description =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public EnvironmentDataBean(){
    }



    public String getDtaLib (){
      return dtaLib;
    }

    public String getDescription (){
      return description;
    }



    public void setDtaLib (String _dtaLib ){
      dtaLib = _dtaLib;
    }

    public void setDescription (String _description ){
      description = _description;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
