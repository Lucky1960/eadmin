package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Verdichting extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String HOOFDVERDICHTING = "hoofdverdichting";
    public final String ID = "id";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";

//{CODE-INSERT-BEGIN:-1274708295}
    public final String HOOFDVERDICHTING_OMSCHR = "hoofdverdichtingOmschrijving";
    public final String REKENING_SOORT = "rekeningSoort";
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public int getId()throws Exception;
    public String getOmschrijving()throws Exception;
    public int getHoofdverdichting()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setHoofdverdichting(int _hoofdverdichting )throws Exception;

    /**
    * @deprecated  replaced by getVerdichtingDataBean()!
    */
    public VerdichtingDataBean getDataBean()throws Exception;
    public VerdichtingDataBean getVerdichtingDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(VerdichtingDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public String getHoofdverdichtingOmschrijving() throws Exception;
    public String getRekeningSoort() throws Exception;
//{CODE-INSERT-END:955534258}
}
