package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class AdresDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String dc =  "";
    protected String dcNr =  "";
    protected int adresType = 0;
    protected String straat =  "";
    protected int huisNr = 0;
    protected String huisNrToev =  "";
    protected String extraAdresRegel =  "";
    protected String postcode =  "";
    protected String plaats =  "";
    protected String land =  "NL";
    protected String telefoon =  "";
    protected String mobiel =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public AdresDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getDc (){
      return dc;
    }

    public String getDcNr (){
      return dcNr;
    }

    public int getAdresType (){
      return adresType;
    }

    public String getStraat (){
      return straat;
    }

    public int getHuisNr (){
      return huisNr;
    }

    public String getHuisNrToev (){
      return huisNrToev;
    }

    public String getExtraAdresRegel (){
      return extraAdresRegel;
    }

    public String getPostcode (){
      return postcode;
    }

    public String getPlaats (){
      return plaats;
    }

    public String getLand (){
      return land;
    }

    public String getTelefoon (){
      return telefoon;
    }

    public String getMobiel (){
      return mobiel;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setDc (String _dc ){
      dc = _dc;
    }

    public void setDcNr (String _dcNr ){
      dcNr = _dcNr;
    }

    public void setAdresType (int _adresType ){
      adresType = _adresType;
    }

    public void setStraat (String _straat ){
      straat = _straat;
    }

    public void setHuisNr (int _huisNr ){
      huisNr = _huisNr;
    }

    public void setHuisNrToev (String _huisNrToev ){
      huisNrToev = _huisNrToev;
    }

    public void setExtraAdresRegel (String _extraAdresRegel ){
      extraAdresRegel = _extraAdresRegel;
    }

    public void setPostcode (String _postcode ){
      postcode = _postcode;
    }

    public void setPlaats (String _plaats ){
      plaats = _plaats;
    }

    public void setLand (String _land ){
      land = _land;
    }

    public void setTelefoon (String _telefoon ){
      telefoon = _telefoon;
    }

    public void setMobiel (String _mobiel ){
      mobiel = _mobiel;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
