package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class BankImportPK implements java.io.Serializable {


    private String bedrijf;
    private String dagboekId;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ) {
      dagboekId = _dagboekId;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getDagboekId () {
       return dagboekId;
    }

}
