package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class ProductDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String product =  "";
    protected String omschr1 =  "";
    protected String omschr2 =  "";
    protected String omschr3 =  "";
    protected String eenheid =  "";
    protected BigDecimal prijs =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal prijsInkoop =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected String rekeningNr =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public ProductDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getProduct (){
      return product;
    }

    public String getOmschr1 (){
      return omschr1;
    }

    public String getOmschr2 (){
      return omschr2;
    }

    public String getOmschr3 (){
      return omschr3;
    }

    public String getEenheid (){
      return eenheid;
    }

    public BigDecimal getPrijs (){
      return prijs;
    }

    public BigDecimal getPrijsInkoop (){
      return prijsInkoop;
    }

    public String getRekeningNr (){
      return rekeningNr;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setProduct (String _product ){
      product = _product;
    }

    public void setOmschr1 (String _omschr1 ){
      omschr1 = _omschr1;
    }

    public void setOmschr2 (String _omschr2 ){
      omschr2 = _omschr2;
    }

    public void setOmschr3 (String _omschr3 ){
      omschr3 = _omschr3;
    }

    public void setEenheid (String _eenheid ){
      eenheid = _eenheid;
    }

    public void setPrijs (BigDecimal _prijs ){
      prijs = _prijs;
    }

    public void setPrijsInkoop (BigDecimal _prijsInkoop ){
      prijsInkoop = _prijsInkoop;
    }

    public void setRekeningNr (String _rekeningNr ){
      rekeningNr = _rekeningNr;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
