package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.eadmin.db.impl.*;

  public class EnvironmentManagerFactory{
    public static EnvironmentManager getInstance(){
  //Custom methods
//{CODE-INSERT-BEGIN:-200577399}
//{CODE-INSERT-END:-200577399}

        return EnvironmentManager_Impl.getInstance();
  //Custom methods
//{CODE-INSERT-BEGIN:-224072788}
//{CODE-INSERT-END:-224072788}

    }

    public static EnvironmentManager getInstance(DBData dbd){
  //Custom methods
//{CODE-INSERT-BEGIN:-1563039877}
//{CODE-INSERT-END:-1563039877}

        return EnvironmentManager_Impl.getInstance(dbd);
  //Custom methods
//{CODE-INSERT-BEGIN:-54815522}
//{CODE-INSERT-END:-54815522}

    }

}
