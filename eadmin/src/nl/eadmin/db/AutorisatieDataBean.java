package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class AutorisatieDataBean implements java.io.Serializable {


    protected String gebruikerId =  "";
    protected String bedrijf =  "";
    protected String autMenu;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public AutorisatieDataBean(){
    }



    public String getGebruikerId (){
      return gebruikerId;
    }

    public String getBedrijf (){
      return bedrijf;
    }

    public String getAutMenu (){
      return autMenu;
    }



    public void setGebruikerId (String _gebruikerId ){
      gebruikerId = _gebruikerId;
    }

    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setAutMenu (String _autMenu ){
      autMenu = _autMenu;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
