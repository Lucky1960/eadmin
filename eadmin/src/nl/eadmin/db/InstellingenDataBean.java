package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class InstellingenDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected int stdBetaalTermijn = 14;
    protected int lengteRekening = 4;
    protected String maskCredNr =  "";
    protected String maskDebNr =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public InstellingenDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public int getStdBetaalTermijn (){
      return stdBetaalTermijn;
    }

    public int getLengteRekening (){
      return lengteRekening;
    }

    public String getMaskCredNr (){
      return maskCredNr;
    }

    public String getMaskDebNr (){
      return maskDebNr;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setStdBetaalTermijn (int _stdBetaalTermijn ){
      stdBetaalTermijn = _stdBetaalTermijn;
    }

    public void setLengteRekening (int _lengteRekening ){
      lengteRekening = _lengteRekening;
    }

    public void setMaskCredNr (String _maskCredNr ){
      maskCredNr = _maskCredNr;
    }

    public void setMaskDebNr (String _maskDebNr ){
      maskDebNr = _maskDebNr;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
