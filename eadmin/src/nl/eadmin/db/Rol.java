package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Rol extends  BusinessObject{

    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String NAAM = "naam";
    public final String OMSCHRIJVING = "omschrijving";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getNaam()throws Exception;
    public String getOmschrijving()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;

    /**
    * @deprecated  replaced by getRolDataBean()!
    */
    public RolDataBean getDataBean()throws Exception;
    public RolDataBean getRolDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(RolDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
