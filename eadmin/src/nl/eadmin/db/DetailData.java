package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface DetailData extends  BusinessObject{

    public final String AANTAL = "aantal";
    public final String BEDRAG_BTW = "bedragBtw";
    public final String BEDRAG_CREDIT = "bedragCredit";
    public final String BEDRAG_DEBET = "bedragDebet";
    public final String BEDRAG_EXCL = "bedragExcl";
    public final String BEDRAG_INCL = "bedragIncl";
    public final String BEDRIJF = "bedrijf";
    public final String BOEKSTUK = "boekstuk";
    public final String BOEKSTUK_REGEL = "boekstukRegel";
    public final String BTW_CODE = "btwCode";
    public final String DC = "dC";
    public final String DAGBOEK_ID = "dagboekId";
    public final String DATUM_LEVERING = "datumLevering";
    public final String DC_NUMMER = "dcNummer";
    public final String EENHEID = "eenheid";
    public final String FACTUUR_NUMMER = "factuurNummer";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHR1 = "omschr1";
    public final String OMSCHR2 = "omschr2";
    public final String OMSCHR3 = "omschr3";
    public final String PRIJS = "prijs";
    public final String REKENING = "rekening";
    public final String VRIJE_RUB1 = "vrijeRub1";
    public final String VRIJE_RUB2 = "vrijeRub2";
    public final String VRIJE_RUB3 = "vrijeRub3";
    public final String VRIJE_RUB4 = "vrijeRub4";
    public final String VRIJE_RUB5 = "vrijeRub5";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getDagboekId()throws Exception;
    public String getBoekstuk()throws Exception;
    public int getBoekstukRegel()throws Exception;
    public Date getDatumLevering()throws Exception;
    public String getOmschr1()throws Exception;
    public String getOmschr2()throws Exception;
    public String getOmschr3()throws Exception;
    public BigDecimal getAantal()throws Exception;
    public String getEenheid()throws Exception;
    public BigDecimal getPrijs()throws Exception;
    public String getBtwCode()throws Exception;
    public BigDecimal getBedragExcl()throws Exception;
    public BigDecimal getBedragBtw()throws Exception;
    public BigDecimal getBedragIncl()throws Exception;
    public BigDecimal getBedragDebet()throws Exception;
    public BigDecimal getBedragCredit()throws Exception;
    public String getDC()throws Exception;
    public String getRekening()throws Exception;
    public String getDcNummer()throws Exception;
    public String getFactuurNummer()throws Exception;
    public String getVrijeRub1()throws Exception;
    public String getVrijeRub2()throws Exception;
    public String getVrijeRub3()throws Exception;
    public String getVrijeRub4()throws Exception;
    public String getVrijeRub5()throws Exception;

    public void setDatumLevering(Date _datumLevering )throws Exception;
    public void setOmschr1(String _omschr1 )throws Exception;
    public void setOmschr2(String _omschr2 )throws Exception;
    public void setOmschr3(String _omschr3 )throws Exception;
    public void setAantal(BigDecimal _aantal )throws Exception;
    public void setEenheid(String _eenheid )throws Exception;
    public void setPrijs(BigDecimal _prijs )throws Exception;
    public void setBtwCode(String _btwCode )throws Exception;
    public void setBedragExcl(BigDecimal _bedragExcl )throws Exception;
    public void setBedragBtw(BigDecimal _bedragBtw )throws Exception;
    public void setBedragIncl(BigDecimal _bedragIncl )throws Exception;
    public void setBedragDebet(BigDecimal _bedragDebet )throws Exception;
    public void setBedragCredit(BigDecimal _bedragCredit )throws Exception;
    public void setDC(String _dC )throws Exception;
    public void setRekening(String _rekening )throws Exception;
    public void setDcNummer(String _dcNummer )throws Exception;
    public void setFactuurNummer(String _factuurNummer )throws Exception;
    public void setVrijeRub1(String _vrijeRub1 )throws Exception;
    public void setVrijeRub2(String _vrijeRub2 )throws Exception;
    public void setVrijeRub3(String _vrijeRub3 )throws Exception;
    public void setVrijeRub4(String _vrijeRub4 )throws Exception;
    public void setVrijeRub5(String _vrijeRub5 )throws Exception;

    /**
    * @deprecated  replaced by getDetailDataDataBean()!
    */
    public DetailDataDataBean getDataBean()throws Exception;
    public DetailDataDataBean getDetailDataDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(DetailDataDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public HeaderData getHeaderData() throws Exception;
    public Rekening getRekeningObject() throws Exception;
    public BtwCode getBtwCodeObject() throws Exception;
//{CODE-INSERT-END:955534258}
}
