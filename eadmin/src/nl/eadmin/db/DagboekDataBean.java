package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class DagboekDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String id =  "";
    protected String soort =  "";
    protected String omschrijving =  "";
    protected String boekstukMask =  "";
    protected String laatsteBoekstuk =  "";
    protected String rekening =  "";
    protected String tegenRekening =  "";
    protected boolean systemValue =  false;
    protected String vrijeRub1;
    protected String vrijeRub2;
    protected String vrijeRub3;
    protected String vrijeRub4;
    protected String vrijeRub5;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public DagboekDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getId (){
      return id;
    }

    public String getSoort (){
      return soort;
    }

    public String getOmschrijving (){
      return omschrijving;
    }

    public String getBoekstukMask (){
      return boekstukMask;
    }

    public String getLaatsteBoekstuk (){
      return laatsteBoekstuk;
    }

    public String getRekening (){
      return rekening;
    }

    public String getTegenRekening (){
      return tegenRekening;
    }

    public boolean getSystemValue (){
      return systemValue;
    }

    public String getVrijeRub1 (){
      return vrijeRub1;
    }

    public String getVrijeRub2 (){
      return vrijeRub2;
    }

    public String getVrijeRub3 (){
      return vrijeRub3;
    }

    public String getVrijeRub4 (){
      return vrijeRub4;
    }

    public String getVrijeRub5 (){
      return vrijeRub5;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setId (String _id ){
      id = _id;
    }

    public void setSoort (String _soort ){
      soort = _soort;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

    public void setBoekstukMask (String _boekstukMask ){
      boekstukMask = _boekstukMask;
    }

    public void setLaatsteBoekstuk (String _laatsteBoekstuk ){
      laatsteBoekstuk = _laatsteBoekstuk;
    }

    public void setRekening (String _rekening ){
      rekening = _rekening;
    }

    public void setTegenRekening (String _tegenRekening ){
      tegenRekening = _tegenRekening;
    }

    public void setSystemValue (boolean _systemValue ){
      systemValue = _systemValue;
    }

    public void setVrijeRub1 (String _vrijeRub1 ){
      vrijeRub1 = _vrijeRub1;
    }

    public void setVrijeRub2 (String _vrijeRub2 ){
      vrijeRub2 = _vrijeRub2;
    }

    public void setVrijeRub3 (String _vrijeRub3 ){
      vrijeRub3 = _vrijeRub3;
    }

    public void setVrijeRub4 (String _vrijeRub4 ){
      vrijeRub4 = _vrijeRub4;
    }

    public void setVrijeRub5 (String _vrijeRub5 ){
      vrijeRub5 = _vrijeRub5;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
