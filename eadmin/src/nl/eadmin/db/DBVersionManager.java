package nl.eadmin.db;

import nl.eadmin.ApplicationConstants;
import nl.ibs.jsql.sql.DBConnectionPool;
import nl.ibs.jsql.sql.PersistenceMetaData;


/**
 * @author c_hc01
 */
public class DBVersionManager extends nl.ibs.jsql.sql.DBManager {
	
	
	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.DBManager#skipConversion(nl.ibs.jsql.sql.DBConnectionPool)
	 */
	protected boolean skipConversion(DBConnectionPool pool) throws Exception {//if(true)return true;
		String dbVersion = getCurrentDataBaseVersion();	
		int compare = getNewDataBaseVersion().compareTo(dbVersion==null||dbVersion.equals("null")?"0.000":dbVersion);
		if (compare<0)
			throw new Exception("msg-db-version-higher-then-application-version");	
		else 
			return compare==0;
		
	}
	
	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.DBManager#getNewDataBaseVersion()
	 */
	protected String getNewDataBaseVersion() {
		return ApplicationConstants.APPLICATION_VERSION;
	}
	
	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.DBManager#ostConvert(nl.ibs.jsql.sql.DBConnectionPool)
	 */
	protected void postConvert(DBConnectionPool pool) throws Exception {
		super.postConvert(pool);
	}

	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.DBManager#preConvert(nl.ibs.jsql.sql.DBConnectionPool)
	 */
	protected void preConvert(DBConnectionPool pool) throws Exception {
		super.preConvert(pool);
	}
	
	/* (non-Javadoc)
	 * @see nl.ibs.jsql.sql.DBManager#preConvert(nl.ibs.jsql.sql.DBConnectionPool)
	 */
	protected void initialise(DBConnectionPool pool) throws Exception{
		//removeForeignKeys();
		//removePrimaryKeys();
		//removeIndexes();
	}
	
	public PersistenceMetaData[] getPersistenceMetaDataArray() {
    	return PersistenceMetaDataSet.getArray();
    }
	

}
