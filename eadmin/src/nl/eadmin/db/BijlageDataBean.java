package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class BijlageDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String dagboekId =  "";
    protected String boekstuk =  "";
    protected int volgNr = 0;
    protected String bestandsnaam;
    protected String omschrijving =  "";
    protected byte[] data;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public BijlageDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getDagboekId (){
      return dagboekId;
    }

    public String getBoekstuk (){
      return boekstuk;
    }

    public int getVolgNr (){
      return volgNr;
    }

    public String getBestandsnaam (){
      return bestandsnaam;
    }

    public String getOmschrijving (){
      return omschrijving;
    }

    public byte[] getData (){
      return data;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ){
      dagboekId = _dagboekId;
    }

    public void setBoekstuk (String _boekstuk ){
      boekstuk = _boekstuk;
    }

    public void setVolgNr (int _volgNr ){
      volgNr = _volgNr;
    }

    public void setBestandsnaam (String _bestandsnaam ){
      bestandsnaam = _bestandsnaam;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

    public void setData (byte[] _data ){
      data = _data;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
