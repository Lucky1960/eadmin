package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class AdresPK implements java.io.Serializable {


    private String bedrijf;
    private String dc;
    private String dcNr;
    private int adresType;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setDc (String _dc ) {
      dc = _dc;
    }

    public void setDcNr (String _dcNr ) {
      dcNr = _dcNr;
    }

    public void setAdresType (int _adresType ) {
      adresType = _adresType;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getDc () {
       return dc;
    }

    public String getDcNr () {
       return dcNr;
    }

    public int getAdresType () {
       return adresType;
    }

}
