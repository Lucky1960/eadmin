package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Crediteur extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String BTW_NR = "btwNr";
    public final String CONTACT_PERSOON = "contactPersoon";
    public final String CRED_NR = "credNr";
    public final String EMAIL = "email";
    public final String IBAN_NR = "ibanNr";
    public final String KVK_NR = "kvkNr";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String NAAM = "naam";

//{CODE-INSERT-BEGIN:-1274708295}
    public final String ADRESREGEL1 = "adresRegel1";
    public final String ADRESREGEL2 = "adresRegel2";
    public final String PLAATS = "plaats";
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getCredNr()throws Exception;
    public String getNaam()throws Exception;
    public String getContactPersoon()throws Exception;
    public String getEmail()throws Exception;
    public String getKvkNr()throws Exception;
    public String getBtwNr()throws Exception;
    public String getIbanNr()throws Exception;

    public void setNaam(String _naam )throws Exception;
    public void setContactPersoon(String _contactPersoon )throws Exception;
    public void setEmail(String _email )throws Exception;
    public void setKvkNr(String _kvkNr )throws Exception;
    public void setBtwNr(String _btwNr )throws Exception;
    public void setIbanNr(String _ibanNr )throws Exception;

    /**
    * @deprecated  replaced by getCrediteurDataBean()!
    */
    public CrediteurDataBean getDataBean()throws Exception;
    public CrediteurDataBean getCrediteurDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(CrediteurDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public Adres getAdres(boolean postAdres) throws Exception;
    public String getAdresRegel1() throws Exception;
    public String getAdresRegel2() throws Exception;
    public String getAdresRegel3() throws Exception;
    public String getPlaats() throws Exception;
//{CODE-INSERT-END:955534258}
}
