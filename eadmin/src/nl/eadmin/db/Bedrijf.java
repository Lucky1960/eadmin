package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Bedrijf extends  BusinessObject{

    public final String BEDRIJFSCODE = "bedrijfscode";
    public final String BIC = "bic";
    public final String BTW_NR = "btwNr";
    public final String EMAIL = "email";
    public final String IBAN_NR = "ibanNr";
    public final String KVK_NR = "kvkNr";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";

//{CODE-INSERT-BEGIN:-1274708295}
    public final String ADRESREGEL1 = "adresRegel1";
    public final String ADRESREGEL2 = "adresRegel2";
    public final String PLAATS = "plaats";
    public final String ACTIVE = "bedrijfActief";
//{CODE-INSERT-END:-1274708295}


    public String getBedrijfscode()throws Exception;
    public String getOmschrijving()throws Exception;
    public String getEmail()throws Exception;
    public String getKvkNr()throws Exception;
    public String getBtwNr()throws Exception;
    public String getIbanNr()throws Exception;
    public String getBic()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setEmail(String _email )throws Exception;
    public void setKvkNr(String _kvkNr )throws Exception;
    public void setBtwNr(String _btwNr )throws Exception;
    public void setIbanNr(String _ibanNr )throws Exception;
    public void setBic(String _bic )throws Exception;

    /**
    * @deprecated  replaced by getBedrijfDataBean()!
    */
    public BedrijfDataBean getDataBean()throws Exception;
    public BedrijfDataBean getBedrijfDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(BedrijfDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public Dagboek getDagboek(String id) throws Exception;
    public Instellingen getInstellingen() throws Exception;
    public String getOmschrijvingLang() throws Exception;
    public Adres getBezoekAdres() throws Exception;
    public Adres getPostAdres() throws Exception;
    public String getAdresRegel1() throws Exception;
    public String getAdresRegel2() throws Exception;
    public String getPlaats() throws Exception;
    public boolean getBedrijfActief() throws Exception;
//{CODE-INSERT-END:955534258}
}
