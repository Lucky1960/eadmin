package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface HtmlTemplate extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String ID = "id";
    public final String IS_DEFAULT = "isDefault";
    public final String LOGO_HEIGHT = "logoHeight";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";

//{CODE-INSERT-BEGIN:-1274708295}
    public final String LOGO = "logo";
    public final String HTML_STRING = "htmlString";
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getId()throws Exception;
    public String getOmschrijving()throws Exception;
    public boolean getIsDefault()throws Exception;
    public byte[] getLogo()throws Exception;
    public int getLogoHeight()throws Exception;
    public String getHtmlString()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setIsDefault(boolean _isDefault )throws Exception;
    public void setLogo(byte[] _logo )throws Exception;
    public void setLogoHeight(int _logoHeight )throws Exception;
    public void setHtmlString(String _htmlString )throws Exception;

    /**
    * @deprecated  replaced by getHtmlTemplateDataBean()!
    */
    public HtmlTemplateDataBean getDataBean()throws Exception;
    public HtmlTemplateDataBean getHtmlTemplateDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(HtmlTemplateDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
