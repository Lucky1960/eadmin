package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class HeaderDataPK implements java.io.Serializable {


    private String bedrijf;
    private String dagboekId;
    private String boekstuk;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ) {
      dagboekId = _dagboekId;
    }

    public void setBoekstuk (String _boekstuk ) {
      boekstuk = _boekstuk;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getDagboekId () {
       return dagboekId;
    }

    public String getBoekstuk () {
       return boekstuk;
    }

}
