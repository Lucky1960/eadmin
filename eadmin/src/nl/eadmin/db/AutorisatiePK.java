package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class AutorisatiePK implements java.io.Serializable {


    private String gebruikerId;
    private String bedrijf;

    public void setGebruikerId (String _gebruikerId ) {
      gebruikerId = _gebruikerId;
    }

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public String getGebruikerId () {
       return gebruikerId;
    }

    public String getBedrijf () {
       return bedrijf;
    }

}
