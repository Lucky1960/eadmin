package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.eadmin.db.impl.*;

  public class CrediteurManagerFactory{
    public static CrediteurManager getInstance(){
  //Custom methods
//{CODE-INSERT-BEGIN:-200577399}
//{CODE-INSERT-END:-200577399}

        return CrediteurManager_Impl.getInstance();
  //Custom methods
//{CODE-INSERT-BEGIN:-224072788}
//{CODE-INSERT-END:-224072788}

    }

    public static CrediteurManager getInstance(DBData dbd){
  //Custom methods
//{CODE-INSERT-BEGIN:-1563039877}
//{CODE-INSERT-END:-1563039877}

        return CrediteurManager_Impl.getInstance(dbd);
  //Custom methods
//{CODE-INSERT-BEGIN:-54815522}
//{CODE-INSERT-END:-54815522}

    }

}
