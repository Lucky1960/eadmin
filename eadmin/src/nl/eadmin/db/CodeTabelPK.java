package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class CodeTabelPK implements java.io.Serializable {


    private String bedrijf;
    private String tabelNaam;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setTabelNaam (String _tabelNaam ) {
      tabelNaam = _tabelNaam;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getTabelNaam () {
       return tabelNaam;
    }

}
