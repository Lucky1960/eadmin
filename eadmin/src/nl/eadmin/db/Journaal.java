package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Journaal extends  BusinessObject{

    public final String BEDRAG_CREDIT = "bedragCredit";
    public final String BEDRAG_DEBET = "bedragDebet";
    public final String BEDRIJF = "bedrijf";
    public final String BOEKDATUM = "boekdatum";
    public final String BOEKJAAR = "boekjaar";
    public final String BOEKPERIODE = "boekperiode";
    public final String BOEKSTUK = "boekstuk";
    public final String BTW_AANGIFTE_KOLOM = "btwAangifteKolom";
    public final String BTW_CATEGORIE = "btwCategorie";
    public final String BTW_CODE = "btwCode";
    public final String DC = "dC";
    public final String DAGBOEK_ID = "dagboekId";
    public final String DAGBOEK_SOORT = "dagboekSoort";
    public final String DC_NUMMER = "dcNummer";
    public final String FACTUUR_NR = "factuurNr";
    public final String ICP_SOORT = "iCPSoort";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHR1 = "omschr1";
    public final String OMSCHR2 = "omschr2";
    public final String OMSCHR3 = "omschr3";
    public final String REKENING_NR = "rekeningNr";
    public final String REKENING_OMSCHR = "rekeningOmschr";
    public final String REKENINGSOORT = "rekeningsoort";
    public final String SEQ_NR = "seqNr";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getDagboekId()throws Exception;
    public String getBoekstuk()throws Exception;
    public int getSeqNr()throws Exception;
    public String getDagboekSoort()throws Exception;
    public String getRekeningNr()throws Exception;
    public String getRekeningsoort()throws Exception;
    public String getRekeningOmschr()throws Exception;
    public String getDcNummer()throws Exception;
    public String getFactuurNr()throws Exception;
    public Date getBoekdatum()throws Exception;
    public int getBoekjaar()throws Exception;
    public int getBoekperiode()throws Exception;
    public String getBtwCode()throws Exception;
    public String getICPSoort()throws Exception;
    public String getBtwCategorie()throws Exception;
    public String getBtwAangifteKolom()throws Exception;
    public String getDC()throws Exception;
    public BigDecimal getBedragDebet()throws Exception;
    public BigDecimal getBedragCredit()throws Exception;
    public String getOmschr1()throws Exception;
    public String getOmschr2()throws Exception;
    public String getOmschr3()throws Exception;

    public void setDagboekSoort(String _dagboekSoort )throws Exception;
    public void setRekeningNr(String _rekeningNr )throws Exception;
    public void setRekeningsoort(String _rekeningsoort )throws Exception;
    public void setRekeningOmschr(String _rekeningOmschr )throws Exception;
    public void setDcNummer(String _dcNummer )throws Exception;
    public void setFactuurNr(String _factuurNr )throws Exception;
    public void setBoekdatum(Date _boekdatum )throws Exception;
    public void setBoekjaar(int _boekjaar )throws Exception;
    public void setBoekperiode(int _boekperiode )throws Exception;
    public void setBtwCode(String _btwCode )throws Exception;
    public void setICPSoort(String _iCPSoort )throws Exception;
    public void setBtwCategorie(String _btwCategorie )throws Exception;
    public void setBtwAangifteKolom(String _btwAangifteKolom )throws Exception;
    public void setDC(String _dC )throws Exception;
    public void setBedragDebet(BigDecimal _bedragDebet )throws Exception;
    public void setBedragCredit(BigDecimal _bedragCredit )throws Exception;
    public void setOmschr1(String _omschr1 )throws Exception;
    public void setOmschr2(String _omschr2 )throws Exception;
    public void setOmschr3(String _omschr3 )throws Exception;

    /**
    * @deprecated  replaced by getJournaalDataBean()!
    */
    public JournaalDataBean getDataBean()throws Exception;
    public JournaalDataBean getJournaalDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(JournaalDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public Rekening getRekeningObject() throws Exception;
//{CODE-INSERT-END:955534258}
}
