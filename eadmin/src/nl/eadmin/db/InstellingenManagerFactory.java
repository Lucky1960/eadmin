package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.eadmin.db.impl.*;

  public class InstellingenManagerFactory{
    public static InstellingenManager getInstance(){
  //Custom methods
//{CODE-INSERT-BEGIN:-200577399}
//{CODE-INSERT-END:-200577399}

        return InstellingenManager_Impl.getInstance();
  //Custom methods
//{CODE-INSERT-BEGIN:-224072788}
//{CODE-INSERT-END:-224072788}

    }

    public static InstellingenManager getInstance(DBData dbd){
  //Custom methods
//{CODE-INSERT-BEGIN:-1563039877}
//{CODE-INSERT-END:-1563039877}

        return InstellingenManager_Impl.getInstance(dbd);
  //Custom methods
//{CODE-INSERT-BEGIN:-54815522}
//{CODE-INSERT-END:-54815522}

    }

}
