package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class BtwCategoriePK implements java.io.Serializable {


    private String bedrijf;
    private String code;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setCode (String _code ) {
      code = _code;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getCode () {
       return code;
    }

}
