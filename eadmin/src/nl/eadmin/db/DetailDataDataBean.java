package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class DetailDataDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String dagboekId =  "";
    protected String boekstuk =  "";
    protected int boekstukRegel = 0;
    protected Date datumLevering;
    protected String omschr1 =  "";
    protected String omschr2 =  "";
    protected String omschr3 =  "";
    protected BigDecimal aantal =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected String eenheid =  "";
    protected BigDecimal prijs =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected String btwCode =  "";
    protected BigDecimal bedragExcl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal bedragBtw =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal bedragIncl =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal bedragDebet =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected BigDecimal bedragCredit =  (new BigDecimal ("0.00")).setScale(2 , BigDecimal.ROUND_HALF_EVEN) ;
    protected String dC =  "";
    protected String rekening =  "";
    protected String dcNummer =  "";
    protected String factuurNummer =  "";
    protected String vrijeRub1 =  "";
    protected String vrijeRub2 =  "";
    protected String vrijeRub3 =  "";
    protected String vrijeRub4 =  "";
    protected String vrijeRub5 =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public DetailDataDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getDagboekId (){
      return dagboekId;
    }

    public String getBoekstuk (){
      return boekstuk;
    }

    public int getBoekstukRegel (){
      return boekstukRegel;
    }

    public Date getDatumLevering (){
      return datumLevering;
    }

    public String getOmschr1 (){
      return omschr1;
    }

    public String getOmschr2 (){
      return omschr2;
    }

    public String getOmschr3 (){
      return omschr3;
    }

    public BigDecimal getAantal (){
      return aantal;
    }

    public String getEenheid (){
      return eenheid;
    }

    public BigDecimal getPrijs (){
      return prijs;
    }

    public String getBtwCode (){
      return btwCode;
    }

    public BigDecimal getBedragExcl (){
      return bedragExcl;
    }

    public BigDecimal getBedragBtw (){
      return bedragBtw;
    }

    public BigDecimal getBedragIncl (){
      return bedragIncl;
    }

    public BigDecimal getBedragDebet (){
      return bedragDebet;
    }

    public BigDecimal getBedragCredit (){
      return bedragCredit;
    }

    public String getDC (){
      return dC;
    }

    public String getRekening (){
      return rekening;
    }

    public String getDcNummer (){
      return dcNummer;
    }

    public String getFactuurNummer (){
      return factuurNummer;
    }

    public String getVrijeRub1 (){
      return vrijeRub1;
    }

    public String getVrijeRub2 (){
      return vrijeRub2;
    }

    public String getVrijeRub3 (){
      return vrijeRub3;
    }

    public String getVrijeRub4 (){
      return vrijeRub4;
    }

    public String getVrijeRub5 (){
      return vrijeRub5;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ){
      dagboekId = _dagboekId;
    }

    public void setBoekstuk (String _boekstuk ){
      boekstuk = _boekstuk;
    }

    public void setBoekstukRegel (int _boekstukRegel ){
      boekstukRegel = _boekstukRegel;
    }

    public void setDatumLevering (Date _datumLevering ){
      datumLevering = _datumLevering;
    }

    public void setOmschr1 (String _omschr1 ){
      omschr1 = _omschr1;
    }

    public void setOmschr2 (String _omschr2 ){
      omschr2 = _omschr2;
    }

    public void setOmschr3 (String _omschr3 ){
      omschr3 = _omschr3;
    }

    public void setAantal (BigDecimal _aantal ){
      aantal = _aantal;
    }

    public void setEenheid (String _eenheid ){
      eenheid = _eenheid;
    }

    public void setPrijs (BigDecimal _prijs ){
      prijs = _prijs;
    }

    public void setBtwCode (String _btwCode ){
      btwCode = _btwCode;
    }

    public void setBedragExcl (BigDecimal _bedragExcl ){
      bedragExcl = _bedragExcl;
    }

    public void setBedragBtw (BigDecimal _bedragBtw ){
      bedragBtw = _bedragBtw;
    }

    public void setBedragIncl (BigDecimal _bedragIncl ){
      bedragIncl = _bedragIncl;
    }

    public void setBedragDebet (BigDecimal _bedragDebet ){
      bedragDebet = _bedragDebet;
    }

    public void setBedragCredit (BigDecimal _bedragCredit ){
      bedragCredit = _bedragCredit;
    }

    public void setDC (String _dC ){
      dC = _dC;
    }

    public void setRekening (String _rekening ){
      rekening = _rekening;
    }

    public void setDcNummer (String _dcNummer ){
      dcNummer = _dcNummer;
    }

    public void setFactuurNummer (String _factuurNummer ){
      factuurNummer = _factuurNummer;
    }

    public void setVrijeRub1 (String _vrijeRub1 ){
      vrijeRub1 = _vrijeRub1;
    }

    public void setVrijeRub2 (String _vrijeRub2 ){
      vrijeRub2 = _vrijeRub2;
    }

    public void setVrijeRub3 (String _vrijeRub3 ){
      vrijeRub3 = _vrijeRub3;
    }

    public void setVrijeRub4 (String _vrijeRub4 ){
      vrijeRub4 = _vrijeRub4;
    }

    public void setVrijeRub5 (String _vrijeRub5 ){
      vrijeRub5 = _vrijeRub5;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
