package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class DetailDataPK implements java.io.Serializable {


    private String bedrijf;
    private String dagboekId;
    private String boekstuk;
    private int boekstukRegel;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ) {
      dagboekId = _dagboekId;
    }

    public void setBoekstuk (String _boekstuk ) {
      boekstuk = _boekstuk;
    }

    public void setBoekstukRegel (int _boekstukRegel ) {
      boekstukRegel = _boekstukRegel;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getDagboekId () {
       return dagboekId;
    }

    public String getBoekstuk () {
       return boekstuk;
    }

    public int getBoekstukRegel () {
       return boekstukRegel;
    }

}
