package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Dagboek extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String BOEKSTUK_MASK = "boekstukMask";
    public final String ID = "id";
    public final String LAATSTE_BOEKSTUK = "laatsteBoekstuk";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";
    public final String REKENING = "rekening";
    public final String SOORT = "soort";
    public final String SYSTEM_VALUE = "systemValue";
    public final String TEGEN_REKENING = "tegenRekening";

//{CODE-INSERT-BEGIN:-1274708295}
    public final String VRIJE_RUB1 = "vrijeRub1";
    public final String VRIJE_RUB2 = "vrijeRub2";
    public final String VRIJE_RUB3 = "vrijeRub3";
    public final String VRIJE_RUB4 = "vrijeRub4";
    public final String VRIJE_RUB5 = "vrijeRub5";
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getId()throws Exception;
    public String getSoort()throws Exception;
    public String getOmschrijving()throws Exception;
    public String getBoekstukMask()throws Exception;
    public String getLaatsteBoekstuk()throws Exception;
    public String getRekening()throws Exception;
    public String getTegenRekening()throws Exception;
    public boolean getSystemValue()throws Exception;
    public String getVrijeRub1()throws Exception;
    public String getVrijeRub2()throws Exception;
    public String getVrijeRub3()throws Exception;
    public String getVrijeRub4()throws Exception;
    public String getVrijeRub5()throws Exception;

    public void setSoort(String _soort )throws Exception;
    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setBoekstukMask(String _boekstukMask )throws Exception;
    public void setLaatsteBoekstuk(String _laatsteBoekstuk )throws Exception;
    public void setRekening(String _rekening )throws Exception;
    public void setTegenRekening(String _tegenRekening )throws Exception;
    public void setSystemValue(boolean _systemValue )throws Exception;
    public void setVrijeRub1(String _vrijeRub1 )throws Exception;
    public void setVrijeRub2(String _vrijeRub2 )throws Exception;
    public void setVrijeRub3(String _vrijeRub3 )throws Exception;
    public void setVrijeRub4(String _vrijeRub4 )throws Exception;
    public void setVrijeRub5(String _vrijeRub5 )throws Exception;

    /**
    * @deprecated  replaced by getDagboekDataBean()!
    */
    public DagboekDataBean getDataBean()throws Exception;
    public DagboekDataBean getDagboekDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(DagboekDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public Rekening getRekeningObject() throws Exception;
    public boolean isBankboek();
    public boolean isKasboek();
    public boolean isMemoriaal();
    public boolean isInkoopboek();
    public boolean isVerkoopboek();
    public BankImport getBankImport() throws Exception;
//{CODE-INSERT-END:955534258}
}
