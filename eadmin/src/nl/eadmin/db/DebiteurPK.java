package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class DebiteurPK implements java.io.Serializable {


    private String bedrijf;
    private String debNr;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setDebNr (String _debNr ) {
      debNr = _debNr;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getDebNr () {
       return debNr;
    }

}
