package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface CodeTabel extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String DATA_TYPE = "dataType";
    public final String DECIMALEN = "decimalen";
    public final String LENGTE = "lengte";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";
    public final String TABEL_NAAM = "tabelNaam";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getTabelNaam()throws Exception;
    public String getOmschrijving()throws Exception;
    public String getDataType()throws Exception;
    public int getLengte()throws Exception;
    public int getDecimalen()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setDataType(String _dataType )throws Exception;
    public void setLengte(int _lengte )throws Exception;
    public void setDecimalen(int _decimalen )throws Exception;

    /**
    * @deprecated  replaced by getCodeTabelDataBean()!
    */
    public CodeTabelDataBean getDataBean()throws Exception;
    public CodeTabelDataBean getCodeTabelDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(CodeTabelDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
