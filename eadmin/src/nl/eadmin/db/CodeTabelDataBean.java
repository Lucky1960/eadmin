package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class CodeTabelDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String tabelNaam =  "";
    protected String omschrijving =  "";
    protected String dataType =  "";
    protected int lengte = 0;
    protected int decimalen = 0;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public CodeTabelDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getTabelNaam (){
      return tabelNaam;
    }

    public String getOmschrijving (){
      return omschrijving;
    }

    public String getDataType (){
      return dataType;
    }

    public int getLengte (){
      return lengte;
    }

    public int getDecimalen (){
      return decimalen;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setTabelNaam (String _tabelNaam ){
      tabelNaam = _tabelNaam;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

    public void setDataType (String _dataType ){
      dataType = _dataType;
    }

    public void setLengte (int _lengte ){
      lengte = _lengte;
    }

    public void setDecimalen (int _decimalen ){
      decimalen = _decimalen;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
