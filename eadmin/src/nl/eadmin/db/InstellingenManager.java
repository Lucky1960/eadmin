package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import org.w3c.dom.*;

//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}


  public interface InstellingenManager extends nl.ibs.vegas.persistence.Manager{

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}

    public Instellingen create(String _bedrijf) throws Exception ;
    public Instellingen create(InstellingenDataBean bean)  throws Exception ;
    public Instellingen findOrCreate (String _bedrijf) throws Exception ;
    public int generalUpdate(String setClause, String whereClause) throws Exception ;
    public int generalDelete(String whereClause) throws Exception ;
    public void add(InstellingenDataBean inst) throws Exception ;
    public Instellingen findByPrimaryKey (String _bedrijf) throws Exception ;
    public void removeByPrimaryKey (String _bedrijf) throws Exception ;
    public Instellingen getFirstObject(Query query) throws Exception;
    public Collection getCollection(Query query) throws Exception;
    public Document getDocument(Query query) throws Exception;
    public DocumentFragment getDocumentFragment(Query query) throws Exception;
    public ArrayList getDocumentFragmentArrayList(Query query) throws Exception;

//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}

}
