package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class HtmlTemplateDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String id =  "";
    protected String omschrijving =  "";
    protected boolean isDefault =  false;
    protected byte[] logo;
    protected int logoHeight = 60;
    protected String htmlString;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public HtmlTemplateDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getId (){
      return id;
    }

    public String getOmschrijving (){
      return omschrijving;
    }

    public boolean getIsDefault (){
      return isDefault;
    }

    public byte[] getLogo (){
      return logo;
    }

    public int getLogoHeight (){
      return logoHeight;
    }

    public String getHtmlString (){
      return htmlString;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setId (String _id ){
      id = _id;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

    public void setIsDefault (boolean _isDefault ){
      isDefault = _isDefault;
    }

    public void setLogo (byte[] _logo ){
      logo = _logo;
    }

    public void setLogoHeight (int _logoHeight ){
      logoHeight = _logoHeight;
    }

    public void setHtmlString (String _htmlString ){
      htmlString = _htmlString;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
