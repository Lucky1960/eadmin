package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class JournaalPK implements java.io.Serializable {


    private String bedrijf;
    private String dagboekId;
    private String boekstuk;
    private int seqNr;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setDagboekId (String _dagboekId ) {
      dagboekId = _dagboekId;
    }

    public void setBoekstuk (String _boekstuk ) {
      boekstuk = _boekstuk;
    }

    public void setSeqNr (int _seqNr ) {
      seqNr = _seqNr;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getDagboekId () {
       return dagboekId;
    }

    public String getBoekstuk () {
       return boekstuk;
    }

    public int getSeqNr () {
       return seqNr;
    }

}
