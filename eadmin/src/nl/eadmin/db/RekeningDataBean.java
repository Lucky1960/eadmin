package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class RekeningDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String rekeningNr =  "";
    protected String omschrijving =  "";
    protected String omschrijvingKort =  "";
    protected int verdichting = 0;
    protected String iCPSoort =  "";
    protected String btwCode =  "";
    protected String btwSoort =  "I";
    protected boolean btwRekening =  false;
    protected String subAdministratieType =  "";
    protected String toonKolom =  "S";
    protected boolean blocked =  false;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public RekeningDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getRekeningNr (){
      return rekeningNr;
    }

    public String getOmschrijving (){
      return omschrijving;
    }

    public String getOmschrijvingKort (){
      return omschrijvingKort;
    }

    public int getVerdichting (){
      return verdichting;
    }

    public String getICPSoort (){
      return iCPSoort;
    }

    public String getBtwCode (){
      return btwCode;
    }

    public String getBtwSoort (){
      return btwSoort;
    }

    public boolean getBtwRekening (){
      return btwRekening;
    }

    public String getSubAdministratieType (){
      return subAdministratieType;
    }

    public String getToonKolom (){
      return toonKolom;
    }

    public boolean getBlocked (){
      return blocked;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setRekeningNr (String _rekeningNr ){
      rekeningNr = _rekeningNr;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

    public void setOmschrijvingKort (String _omschrijvingKort ){
      omschrijvingKort = _omschrijvingKort;
    }

    public void setVerdichting (int _verdichting ){
      verdichting = _verdichting;
    }

    public void setICPSoort (String _iCPSoort ){
      iCPSoort = _iCPSoort;
    }

    public void setBtwCode (String _btwCode ){
      btwCode = _btwCode;
    }

    public void setBtwSoort (String _btwSoort ){
      btwSoort = _btwSoort;
    }

    public void setBtwRekening (boolean _btwRekening ){
      btwRekening = _btwRekening;
    }

    public void setSubAdministratieType (String _subAdministratieType ){
      subAdministratieType = _subAdministratieType;
    }

    public void setToonKolom (String _toonKolom ){
      toonKolom = _toonKolom;
    }

    public void setBlocked (boolean _blocked ){
      blocked = _blocked;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
