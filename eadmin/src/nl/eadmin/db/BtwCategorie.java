package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;

import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;

import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface BtwCategorie extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String CODE = "code";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";
    public final String REKENING_NR = "rekeningNr";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getCode()throws Exception;
    public String getOmschrijving()throws Exception;
    public String getRekeningNr()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setRekeningNr(String _rekeningNr )throws Exception;

    /**
    * @deprecated  replaced by getBtwCategorieDataBean()!
    */
    public BtwCategorieDataBean getDataBean()throws Exception;
    public BtwCategorieDataBean getBtwCategorieDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(BtwCategorieDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public Rekening getRekeningObject() throws Exception;
//{CODE-INSERT-END:955534258}
}
