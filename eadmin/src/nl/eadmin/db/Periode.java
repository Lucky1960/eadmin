package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Periode extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String BOEKJAAR = "boekjaar";
    public final String BOEKPERIODE = "boekperiode";
    public final String EINDDATUM = "einddatum";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String STARTDATUM = "startdatum";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public int getBoekjaar()throws Exception;
    public int getBoekperiode()throws Exception;
    public Date getStartdatum()throws Exception;
    public Date getEinddatum()throws Exception;

    public void setStartdatum(Date _startdatum )throws Exception;
    public void setEinddatum(Date _einddatum )throws Exception;

    /**
    * @deprecated  replaced by getPeriodeDataBean()!
    */
    public PeriodeDataBean getDataBean()throws Exception;
    public PeriodeDataBean getPeriodeDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(PeriodeDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
