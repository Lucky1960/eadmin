package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class BedrijfDataBean implements java.io.Serializable {


    protected String bedrijfscode =  "";
    protected String omschrijving =  "";
    protected String email =  "";
    protected String kvkNr =  "";
    protected String btwNr =  "";
    protected String ibanNr =  "";
    protected String bic =  "";


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public BedrijfDataBean(){
    }



    public String getBedrijfscode (){
      return bedrijfscode;
    }

    public String getOmschrijving (){
      return omschrijving;
    }

    public String getEmail (){
      return email;
    }

    public String getKvkNr (){
      return kvkNr;
    }

    public String getBtwNr (){
      return btwNr;
    }

    public String getIbanNr (){
      return ibanNr;
    }

    public String getBic (){
      return bic;
    }



    public void setBedrijfscode (String _bedrijfscode ){
      bedrijfscode = _bedrijfscode;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

    public void setEmail (String _email ){
      email = _email;
    }

    public void setKvkNr (String _kvkNr ){
      kvkNr = _kvkNr;
    }

    public void setBtwNr (String _btwNr ){
      btwNr = _btwNr;
    }

    public void setIbanNr (String _ibanNr ){
      ibanNr = _ibanNr;
    }

    public void setBic (String _bic ){
      bic = _bic;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
