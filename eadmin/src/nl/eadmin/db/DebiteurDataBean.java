package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class DebiteurDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected String debNr =  "";
    protected String naam =  "";
    protected String contactPersoon =  "";
    protected String email =  "";
    protected String kvkNr =  "";
    protected String btwNr =  "";
    protected String ibanNr =  "";
    protected int betaalTermijn = 0;
    protected Date dtmLaatsteAanmaning;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public DebiteurDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public String getDebNr (){
      return debNr;
    }

    public String getNaam (){
      return naam;
    }

    public String getContactPersoon (){
      return contactPersoon;
    }

    public String getEmail (){
      return email;
    }

    public String getKvkNr (){
      return kvkNr;
    }

    public String getBtwNr (){
      return btwNr;
    }

    public String getIbanNr (){
      return ibanNr;
    }

    public int getBetaalTermijn (){
      return betaalTermijn;
    }

    public Date getDtmLaatsteAanmaning (){
      return dtmLaatsteAanmaning;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setDebNr (String _debNr ){
      debNr = _debNr;
    }

    public void setNaam (String _naam ){
      naam = _naam;
    }

    public void setContactPersoon (String _contactPersoon ){
      contactPersoon = _contactPersoon;
    }

    public void setEmail (String _email ){
      email = _email;
    }

    public void setKvkNr (String _kvkNr ){
      kvkNr = _kvkNr;
    }

    public void setBtwNr (String _btwNr ){
      btwNr = _btwNr;
    }

    public void setIbanNr (String _ibanNr ){
      ibanNr = _ibanNr;
    }

    public void setBetaalTermijn (int _betaalTermijn ){
      betaalTermijn = _betaalTermijn;
    }

    public void setDtmLaatsteAanmaning (Date _dtmLaatsteAanmaning ){
      dtmLaatsteAanmaning = _dtmLaatsteAanmaning;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
