package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class VerdichtingDataBean implements java.io.Serializable {


    protected String bedrijf =  "";
    protected int id = 0;
    protected String omschrijving =  "";
    protected int hoofdverdichting = 0;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public VerdichtingDataBean(){
    }



    public String getBedrijf (){
      return bedrijf;
    }

    public int getId (){
      return id;
    }

    public String getOmschrijving (){
      return omschrijving;
    }

    public int getHoofdverdichting (){
      return hoofdverdichting;
    }



    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setId (int _id ){
      id = _id;
    }

    public void setOmschrijving (String _omschrijving ){
      omschrijving = _omschrijving;
    }

    public void setHoofdverdichting (int _hoofdverdichting ){
      hoofdverdichting = _hoofdverdichting;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
