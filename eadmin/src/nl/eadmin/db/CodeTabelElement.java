package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface CodeTabelElement extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String ID = "id";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";
    public final String TABEL_NAAM = "tabelNaam";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getTabelNaam()throws Exception;
    public String getId()throws Exception;
    public String getOmschrijving()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;

    /**
    * @deprecated  replaced by getCodeTabelElementDataBean()!
    */
    public CodeTabelElementDataBean getDataBean()throws Exception;
    public CodeTabelElementDataBean getCodeTabelElementDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(CodeTabelElementDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
