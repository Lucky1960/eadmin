package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class ProductPK implements java.io.Serializable {


    private String bedrijf;
    private String product;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setProduct (String _product ) {
      product = _product;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public String getProduct () {
       return product;
    }

}
