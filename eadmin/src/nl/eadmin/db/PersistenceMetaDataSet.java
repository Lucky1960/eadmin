package nl.eadmin.db;

import nl.ibs.jsql.sql.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class PersistenceMetaDataSet{


	private static PersistenceMetaData[] pmds = {
		DBObjectIDGenerator.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataAdres.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataAutorisatie.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataBankAfschrift.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataBankImport.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataBedrijf.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataBijlage.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataBtwCategorie.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataBtwCode.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataCodeTabel.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataCodeTabelElement.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataCrediteur.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataDagboek.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataDebiteur.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataDetailData.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataEnvironment.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataGebruiker.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataHeaderData.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataHoofdverdichting.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataHtmlTemplate.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataInstellingen.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataJournaal.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataPeriode.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataProduct.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataRekening.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataRol.getInstance(),
		nl.eadmin.db.impl.PersistenceMetaDataVerdichting.getInstance()
	};



	public static PersistenceMetaData[] getArray(){

		return pmds;

	};

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
