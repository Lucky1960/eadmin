package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Autorisatie extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String GEBRUIKER_ID = "gebruikerId";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getGebruikerId()throws Exception;
    public String getBedrijf()throws Exception;
    public String getAutMenu()throws Exception;

    public void setAutMenu(String _autMenu )throws Exception;

    /**
    * @deprecated  replaced by getAutorisatieDataBean()!
    */
    public AutorisatieDataBean getDataBean()throws Exception;
    public AutorisatieDataBean getAutorisatieDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(AutorisatieDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
