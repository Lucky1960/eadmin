package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface BankImport extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String DAGBOEK_ID = "dagboekId";
    public final String FORMAT = "format";
    public final String IBAN_NR = "ibanNr";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getDagboekId()throws Exception;
    public String getIbanNr()throws Exception;
    public String getFormat()throws Exception;

    public void setIbanNr(String _ibanNr )throws Exception;
    public void setFormat(String _format )throws Exception;

    /**
    * @deprecated  replaced by getBankImportDataBean()!
    */
    public BankImportDataBean getDataBean()throws Exception;
    public BankImportDataBean getBankImportDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(BankImportDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
