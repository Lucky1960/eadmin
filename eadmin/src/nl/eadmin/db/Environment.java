package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Environment extends  BusinessObject{

    public final String DESCRIPTION = "description";
    public final String DTA_LIB = "dtaLib";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getDtaLib()throws Exception;
    public String getDescription()throws Exception;

    public void setDescription(String _description )throws Exception;

    /**
    * @deprecated  replaced by getEnvironmentDataBean()!
    */
    public EnvironmentDataBean getDataBean()throws Exception;
    public EnvironmentDataBean getEnvironmentDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(EnvironmentDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
