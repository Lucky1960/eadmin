package nl.eadmin.db;

import java.math.*;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

  public class HoofdverdichtingPK implements java.io.Serializable {


    private String bedrijf;
    private int id;

    public void setBedrijf (String _bedrijf ) {
      bedrijf = _bedrijf;
    }

    public void setId (int _id ) {
      id = _id;
    }

    public String getBedrijf () {
       return bedrijf;
    }

    public int getId () {
       return id;
    }

}
