package nl.eadmin.db;

import nl.ibs.jsql.BusinessObject;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface Rekening extends  BusinessObject{

    public final String BEDRIJF = "bedrijf";
    public final String BLOCKED = "blocked";
    public final String BTW_CODE = "btwCode";
    public final String BTW_REKENING = "btwRekening";
    public final String BTW_SOORT = "btwSoort";
    public final String ICP_SOORT = "iCPSoort";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHRIJVING = "omschrijving";
    public final String OMSCHRIJVING_KORT = "omschrijvingKort";
    public final String REKENING_NR = "rekeningNr";
    public final String SUB_ADMINISTRATIE_TYPE = "subAdministratieType";
    public final String TOON_KOLOM = "toonKolom";
    public final String VERDICHTING = "verdichting";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getRekeningNr()throws Exception;
    public String getOmschrijving()throws Exception;
    public String getOmschrijvingKort()throws Exception;
    public int getVerdichting()throws Exception;
    public String getICPSoort()throws Exception;
    public String getBtwCode()throws Exception;
    public String getBtwSoort()throws Exception;
    public boolean getBtwRekening()throws Exception;
    public String getSubAdministratieType()throws Exception;
    public String getToonKolom()throws Exception;
    public boolean getBlocked()throws Exception;

    public void setOmschrijving(String _omschrijving )throws Exception;
    public void setOmschrijvingKort(String _omschrijvingKort )throws Exception;
    public void setVerdichting(int _verdichting )throws Exception;
    public void setICPSoort(String _iCPSoort )throws Exception;
    public void setBtwCode(String _btwCode )throws Exception;
    public void setBtwSoort(String _btwSoort )throws Exception;
    public void setBtwRekening(boolean _btwRekening )throws Exception;
    public void setSubAdministratieType(String _subAdministratieType )throws Exception;
    public void setToonKolom(String _toonKolom )throws Exception;
    public void setBlocked(boolean _blocked )throws Exception;

    /**
    * @deprecated  replaced by getRekeningDataBean()!
    */
    public RekeningDataBean getDataBean()throws Exception;
    public RekeningDataBean getRekeningDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(RekeningDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public String getRekeningSoort() throws Exception;
    public BtwCategorie getBtwCategorie() throws Exception;
//{CODE-INSERT-END:955534258}
}
