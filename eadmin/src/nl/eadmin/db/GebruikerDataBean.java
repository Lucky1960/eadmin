package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.sql.*;
import nl.ibs.jsql.exception.*;
import nl.ibs.jsql.impl.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.math.*;

/* start custom imports */
//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}
/* end custom imports */


public class GebruikerDataBean implements java.io.Serializable {


    protected String id =  "";
    protected String naam =  "";
    protected String wachtwoord =  "35";
    protected String bedrijf =  "";
    protected int boekjaar = 0;


    /* start custom declerations */
//{CODE-INSERT-BEGIN:1540831801}
//{CODE-INSERT-END:1540831801}
    /* end custom declerations */


    public GebruikerDataBean(){
    }



    public String getId (){
      return id;
    }

    public String getNaam (){
      return naam;
    }

    public String getWachtwoord (){
      return wachtwoord;
    }

    public String getBedrijf (){
      return bedrijf;
    }

    public int getBoekjaar (){
      return boekjaar;
    }



    public void setId (String _id ){
      id = _id;
    }

    public void setNaam (String _naam ){
      naam = _naam;
    }

    public void setWachtwoord (String _wachtwoord ){
      wachtwoord = _wachtwoord;
    }

    public void setBedrijf (String _bedrijf ){
      bedrijf = _bedrijf;
    }

    public void setBoekjaar (int _boekjaar ){
      boekjaar = _boekjaar;
    }

  //Custom methods
//{CODE-INSERT-BEGIN:955534258}
//{CODE-INSERT-END:955534258}
}
