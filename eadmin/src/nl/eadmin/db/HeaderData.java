package nl.eadmin.db;

import nl.ibs.jsql.*;
import nl.ibs.jsql.exception.*;
import java.math.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;
import org.w3c.dom.*;


//{CODE-INSERT-BEGIN:-1184795739}
//{CODE-INSERT-END:-1184795739}



 public interface HeaderData extends  BusinessObject{

    public final String AANTAL_AANMANINGEN = "aantalAanmaningen";
    public final String BEDRIJF = "bedrijf";
    public final String BEGINSALDO = "beginsaldo";
    public final String BOEKDATUM = "boekdatum";
    public final String BOEKJAAR = "boekjaar";
    public final String BOEKPERIODE = "boekperiode";
    public final String BOEKSTUK = "boekstuk";
    public final String DC = "dC";
    public final String DAGBOEK_ID = "dagboekId";
    public final String DAGBOEK_SOORT = "dagboekSoort";
    public final String DC_NUMMER = "dcNummer";
    public final String EINDSALDO = "eindsaldo";
    public final String FACTUUR_LAYOUT = "factuurLayout";
    public final String FACTUUR_NUMMER = "factuurNummer";
    public final String FACTUURDATUM = "factuurdatum";
    public final String FAVORIET = "favoriet";
    public final String MUTATION_TIME_STAMP = "mutationTimeStamp";
    public final String OMSCHR1 = "omschr1";
    public final String OMSCHR2 = "omschr2";
    public final String REFERENTIE_NUMMER = "referentieNummer";
    public final String STATUS = "status";
    public final String TOTAAL_BETAALD = "totaalBetaald";
    public final String TOTAAL_BTW = "totaalBtw";
    public final String TOTAAL_CREDIT = "totaalCredit";
    public final String TOTAAL_DEBET = "totaalDebet";
    public final String TOTAAL_EXCL = "totaalExcl";
    public final String TOTAAL_GEBOEKT = "totaalGeboekt";
    public final String TOTAAL_INCL = "totaalIncl";
    public final String TOTAAL_OPENSTAAND = "totaalOpenstaand";
    public final String VERVALDATUM = "vervaldatum";
    public final String VRIJE_RUB1 = "vrijeRub1";
    public final String VRIJE_RUB2 = "vrijeRub2";
    public final String VRIJE_RUB3 = "vrijeRub3";
    public final String VRIJE_RUB4 = "vrijeRub4";
    public final String VRIJE_RUB5 = "vrijeRub5";

//{CODE-INSERT-BEGIN:-1274708295}
//{CODE-INSERT-END:-1274708295}


    public String getBedrijf()throws Exception;
    public String getDagboekId()throws Exception;
    public String getDagboekSoort()throws Exception;
    public String getBoekstuk()throws Exception;
    public int getStatus()throws Exception;
    public boolean getFavoriet()throws Exception;
    public Date getBoekdatum()throws Exception;
    public int getBoekjaar()throws Exception;
    public int getBoekperiode()throws Exception;
    public String getDcNummer()throws Exception;
    public String getFactuurNummer()throws Exception;
    public String getReferentieNummer()throws Exception;
    public Date getFactuurdatum()throws Exception;
    public String getFactuurLayout()throws Exception;
    public Date getVervaldatum()throws Exception;
    public int getAantalAanmaningen()throws Exception;
    public String getDC()throws Exception;
    public BigDecimal getTotaalExcl()throws Exception;
    public BigDecimal getTotaalBtw()throws Exception;
    public BigDecimal getTotaalIncl()throws Exception;
    public BigDecimal getTotaalGeboekt()throws Exception;
    public BigDecimal getTotaalBetaald()throws Exception;
    public BigDecimal getTotaalOpenstaand()throws Exception;
    public BigDecimal getTotaalDebet()throws Exception;
    public BigDecimal getTotaalCredit()throws Exception;
    public BigDecimal getBeginsaldo()throws Exception;
    public BigDecimal getEindsaldo()throws Exception;
    public String getOmschr1()throws Exception;
    public String getOmschr2()throws Exception;
    public String getVrijeRub1()throws Exception;
    public String getVrijeRub2()throws Exception;
    public String getVrijeRub3()throws Exception;
    public String getVrijeRub4()throws Exception;
    public String getVrijeRub5()throws Exception;

    public void setDagboekSoort(String _dagboekSoort )throws Exception;
    public void setStatus(int _status )throws Exception;
    public void setFavoriet(boolean _favoriet )throws Exception;
    public void setBoekdatum(Date _boekdatum )throws Exception;
    public void setBoekjaar(int _boekjaar )throws Exception;
    public void setBoekperiode(int _boekperiode )throws Exception;
    public void setDcNummer(String _dcNummer )throws Exception;
    public void setFactuurNummer(String _factuurNummer )throws Exception;
    public void setReferentieNummer(String _referentieNummer )throws Exception;
    public void setFactuurdatum(Date _factuurdatum )throws Exception;
    public void setFactuurLayout(String _factuurLayout )throws Exception;
    public void setVervaldatum(Date _vervaldatum )throws Exception;
    public void setAantalAanmaningen(int _aantalAanmaningen )throws Exception;
    public void setDC(String _dC )throws Exception;
    public void setTotaalExcl(BigDecimal _totaalExcl )throws Exception;
    public void setTotaalBtw(BigDecimal _totaalBtw )throws Exception;
    public void setTotaalIncl(BigDecimal _totaalIncl )throws Exception;
    public void setTotaalGeboekt(BigDecimal _totaalGeboekt )throws Exception;
    public void setTotaalBetaald(BigDecimal _totaalBetaald )throws Exception;
    public void setTotaalOpenstaand(BigDecimal _totaalOpenstaand )throws Exception;
    public void setTotaalDebet(BigDecimal _totaalDebet )throws Exception;
    public void setTotaalCredit(BigDecimal _totaalCredit )throws Exception;
    public void setBeginsaldo(BigDecimal _beginsaldo )throws Exception;
    public void setEindsaldo(BigDecimal _eindsaldo )throws Exception;
    public void setOmschr1(String _omschr1 )throws Exception;
    public void setOmschr2(String _omschr2 )throws Exception;
    public void setVrijeRub1(String _vrijeRub1 )throws Exception;
    public void setVrijeRub2(String _vrijeRub2 )throws Exception;
    public void setVrijeRub3(String _vrijeRub3 )throws Exception;
    public void setVrijeRub4(String _vrijeRub4 )throws Exception;
    public void setVrijeRub5(String _vrijeRub5 )throws Exception;

    /**
    * @deprecated  replaced by getHeaderDataDataBean()!
    */
    public HeaderDataDataBean getDataBean()throws Exception;
    public HeaderDataDataBean getHeaderDataDataBean() throws Exception;
    public void delete() throws Exception;
    public void update(HeaderDataDataBean bean) throws Exception ;


//{CODE-INSERT-BEGIN:955534258}
    public Dagboek getDagboekObject() throws Exception;
    public Collection<?> getDetails() throws Exception;
	public Collection<?> getBijlagen() throws Exception;
    public boolean isManualBank() throws Exception;
    public int getAantalDagenVervallen() throws Exception;
    public void recalculate() throws Exception;
	public void recalculateBedragBetaald() throws Exception;
//{CODE-INSERT-END:955534258}
}
