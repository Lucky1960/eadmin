package nl.eadmin.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.eadmin.ui.uiobjects.UserContext;
import nl.ibs.util.NameValuePair;

public abstract class AdresTypeEnum extends EnumHelper {
	public static final int ADRESTYPE_BEZOEK = 1;
	public static final int ADRESTYPE_POST = 2;
	private static Map<String, List<NameValuePair>> map = new HashMap<String, List<NameValuePair>>(2);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static synchronized List getCollection() {
		String locale = UserContext.getLocale().toString();
		List list = (List) map.get(locale);
		if (list != null) {
			return list;
		}
		list = new ArrayList();
		list.add(new NameValuePair("" + ADRESTYPE_BEZOEK, "Adres"));
		list.add(new NameValuePair("" + ADRESTYPE_POST, "Postadres"));
		list = translate(list);
		map.put(locale, list);
		return list;
	}

	public static String getValue(int code) {
		NameValuePair pair;
		Iterator<?> iterator = getCollection().iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			if (pair.getName().equals("" + code)) {
				return pair.getValue();
			}
		}
		return null;
	}
}