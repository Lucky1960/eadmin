package nl.eadmin.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.eadmin.ui.uiobjects.UserContext;
import nl.ibs.util.NameValuePair;

public abstract class BtwTypeEnum extends EnumHelper {
	public static final String TYPE_1A = "1A";
	public static final String TYPE_1B = "1B";
	public static final String TYPE_1C = "1C";
	public static final String TYPE_1D = "1D";
	public static final String TYPE_1E = "1E";
	public static final String TYPE_2A = "2A";
	public static final String TYPE_3A = "3A";
	public static final String TYPE_3B = "3B";
	public static final String TYPE_3C = "3C";
	public static final String TYPE_4A = "4A";
	public static final String TYPE_4B = "4B";
	public static final String TYPE_5B = "5B";
	public static final String TYPE_5D = "5D";
	private static Map<String, List<NameValuePair>> map = new HashMap<String, List<NameValuePair>>(2);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static synchronized List getCollection() {
		String locale = UserContext.getLocale().toString();
		List list = (List) map.get(locale);
		if (list != null) {
			return list;
		}
		list = new ArrayList();
		list.add(new NameValuePair(TYPE_1A, "Btw-formulier cat. 1A"));
		list.add(new NameValuePair(TYPE_1B, "Btw-formulier cat. 1B"));
		list.add(new NameValuePair(TYPE_1C, "Btw-formulier cat. 1C"));
		list.add(new NameValuePair(TYPE_1D, "Btw-formulier cat. 1D"));
		list.add(new NameValuePair(TYPE_1E, "Btw-formulier cat. 1E"));
		list.add(new NameValuePair(TYPE_2A, "Btw-formulier cat. 2A"));
		list.add(new NameValuePair(TYPE_3A, "Btw-formulier cat. 3A"));
		list.add(new NameValuePair(TYPE_3B, "Btw-formulier cat. 3B"));
		list.add(new NameValuePair(TYPE_3C, "Btw-formulier cat. 3C"));
		list.add(new NameValuePair(TYPE_4A, "Btw-formulier cat. 4A"));
		list.add(new NameValuePair(TYPE_4B, "Btw-formulier cat. 4B"));
		list.add(new NameValuePair(TYPE_5B, "Btw-formulier cat. 5B"));
		list.add(new NameValuePair(TYPE_5D, "Btw-formulier cat. 5D"));
		list = translate(list);
		map.put(locale, list);
		return list;
	}

	public static String getValue(String code) {
		NameValuePair pair;
		Iterator<?> iterator = getCollection().iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			if (pair.getName().equals(code)) {
				return pair.getValue();
			}
		}
		return null;
	}
}