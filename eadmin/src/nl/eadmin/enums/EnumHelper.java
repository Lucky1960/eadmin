package nl.eadmin.enums;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import nl.ibs.esp.language.LanguageHelper;
import nl.ibs.esp.uiobjects.ComboBox;
import nl.ibs.esp.uiobjects.Option;
import nl.ibs.util.NameValuePair;

public abstract class EnumHelper {

	protected static List<NameValuePair> translate(List<NameValuePair> list) {
		NameValuePair pair;
		Iterator<?> iterator = list.iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			pair.setValue(LanguageHelper.getString(pair.getValue()));
		}
		return list;
	}

	public static ComboBox createComboBox(String label, Collection<?> collection, boolean addEmptyOption) throws Exception {
		Object[] values = collection.toArray();

		int j = 0;
		Option[] options;
		if (addEmptyOption) {
			options = new Option[values.length + 1];
			options[j++] = new Option("", "");
		} else {
			options = new Option[values.length];
		}

		NameValuePair pair;
		for (int i = 0; i < values.length; i++) {
			pair = (NameValuePair) values[i];
			options[j++] = new Option(pair.getValue(), pair.getName());
		}
		return new ComboBox(label, options);
	}
}
