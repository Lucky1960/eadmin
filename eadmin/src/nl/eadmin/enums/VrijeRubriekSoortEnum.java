package nl.eadmin.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.eadmin.ui.uiobjects.UserContext;
import nl.ibs.util.NameValuePair;

public abstract class VrijeRubriekSoortEnum extends EnumHelper {
	public static final String NOT_IN_USE = "X";
	public static final String TABELWAARDE = "T";
	public static final String ALFA = "A";
	public static final String NUM = "N";
	public static final String DEC = ".";
	public static final String DATUM = "D";
	public static final String BOOL = "B";
	private static Map<String, List<NameValuePair>> map = new HashMap<String, List<NameValuePair>>(2);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static synchronized List getCollection() {
		String locale = UserContext.getLocale().toString();
		List list = (List) map.get(locale);
		if (list != null) {
			return list;
		}
		list = new ArrayList();
		list.add(new NameValuePair(NOT_IN_USE, "Niet in gebruik"));
		list.add(new NameValuePair(TABELWAARDE, "Waarde uit een tabel"));
		list.add(new NameValuePair(ALFA, "Vrije tekst (Max. 10 tekens)"));
		list.add(new NameValuePair(NUM, "Numerieke waarde"));
		list.add(new NameValuePair(DEC, "Decimale waarde"));
		list.add(new NameValuePair(DATUM, "Datum"));
		list.add(new NameValuePair(BOOL, "Ja/Nee"));
		list = translate(list);
		map.put(locale, list);
		return list;
	}

	public static String getValue(String code) {
		NameValuePair pair;
		Iterator<?> iterator = getCollection().iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			if (pair.getName().equals(code)) {
				return pair.getValue();
			}
		}
		return null;
	}
}