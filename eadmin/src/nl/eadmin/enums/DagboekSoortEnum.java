package nl.eadmin.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.eadmin.ui.uiobjects.UserContext;
import nl.ibs.util.NameValuePair;

public abstract class DagboekSoortEnum extends EnumHelper {
	public static final String BANK = "B";
	public static final String KAS = "K";
	public static final String MEMO = "M";
	public static final String INKOOP = "I";
	public static final String VERKOOP = "V";
	private static Map<String, List<NameValuePair>> map = new HashMap<String, List<NameValuePair>>(2);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static synchronized List getCollection() {
		String locale = UserContext.getLocale().toString();
		List list = (List) map.get(locale);
		if (list != null) {
			return list;
		}
		list = new ArrayList();
		list.add(new NameValuePair(BANK, "Bank"));
		list.add(new NameValuePair(KAS, "Kas"));
		list.add(new NameValuePair(MEMO, "Memoriaal"));
		list.add(new NameValuePair(INKOOP, "Inkoop"));
		list.add(new NameValuePair(VERKOOP, "Verkoop"));
		list = translate(list);
		map.put(locale, list);
		return list;
	}

	public static String getValue(String code) {
		NameValuePair pair;
		Iterator<?> iterator = getCollection().iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			if (pair.getName().equals(code)) {
				return pair.getValue();
			}
		}
		return null;
	}
}