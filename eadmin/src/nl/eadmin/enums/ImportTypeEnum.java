package nl.eadmin.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.eadmin.ui.uiobjects.UserContext;
import nl.ibs.util.NameValuePair;


public abstract class ImportTypeEnum extends EnumHelper {
	public static final int ADRES = 0;
	public static final int BEDRIJF = 1;
	public static final int BIJLAGE = 2;
	public static final int BTWCATEGORIE = 18;
	public static final int BTWCODE = 3;
	public static final int CODETABEL = 4;
	public static final int CODETABELELM = 5;
	public static final int CREDITEUR = 6;
	public static final int DAGBOEK = 7;
	public static final int DEBITEUR = 8;
	public static final int DETAILDATA = 9;
	public static final int HEADERDATA = 10;
	public static final int HFDVERDICHTING = 11;
	public static final int HTMLTEMPLATE = 16;
	public static final int INSTELLING = 12;
	public static final int PERIODE = 13;
	public static final int PRODUCT = 17;
	public static final int REKENING = 14;
	public static final int VERDICHTING = 15;
	
	public static final String _ADRES = "ADR";
	public static final String _BEDRIJF = "BDR";
	public static final String _BIJLAGE = "BIJL";
	public static final String _BTWCATEGORIE = "BTWCAT";
	public static final String _BTWCODE = "BTW";
	public static final String _CODETABEL = "COD";
	public static final String _CODETBLELM = "CTE";
	public static final String _CREDITEUR = "CRED";
	public static final String _DAGBOEK = "DAGB";
	public static final String _DEBITEUR = "DEB";
	public static final String _DETAILDATA = "DTLDTA";
	public static final String _HEADERDATA = "HDRDTA";
	public static final String _HFDVERDICHT = "HFDV";
	public static final String _HTMLTEMPLATE = "HTMLTMP";
	public static final String _INSTELLING = "INST";
	public static final String _PERIODE = "PER";
	public static final String _PRODUCT = "PRD";
	public static final String _REKENING = "REK";
	public static final String _VERDICHTING = "VERD";

	private static Map<String, List<NameValuePair>> map = new HashMap<String, List<NameValuePair>>(2);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static synchronized List getCollection() {
		String locale = UserContext.getLocale().toString();
		List list = (List) map.get(locale);
		if (list != null) {
			return list;
		}
		list = new ArrayList();
			list.add(new NameValuePair("" + ADRES, "Adresgegevens"));
			list.add(new NameValuePair("" + BEDRIJF, "Bedrijfsgegevens"));
			list.add(new NameValuePair("" + BIJLAGE, "Bijlages"));
			list.add(new NameValuePair("" + BTWCATEGORIE, "BTW-categorieŽn"));
			list.add(new NameValuePair("" + BTWCODE, "BTW-codes"));
			list.add(new NameValuePair("" + CODETABEL, "Codetabellen"));
			list.add(new NameValuePair("" + CODETABELELM, "Codetabel-elementen"));
			list.add(new NameValuePair("" + CREDITEUR, "Crediteuren"));
			list.add(new NameValuePair("" + DAGBOEK, "Dagboeken"));
			list.add(new NameValuePair("" + DEBITEUR, "Debiteuren"));
			list.add(new NameValuePair("" + DETAILDATA, "Detaildata"));
			list.add(new NameValuePair("" + HEADERDATA, "Headerdata"));
			list.add(new NameValuePair("" + HTMLTEMPLATE, "Factuurlayouts"));
			list.add(new NameValuePair("" + HFDVERDICHTING, "Hoofdverdichtingen"));
			list.add(new NameValuePair("" + INSTELLING, "Instellingen"));
			list.add(new NameValuePair("" + PERIODE, "Periodetabel"));
			list.add(new NameValuePair("" + PRODUCT, "Producttabel"));
			list.add(new NameValuePair("" + REKENING, "Rekeningnummers"));
			list.add(new NameValuePair("" + VERDICHTING, "Verdichtingen"));
		list = translate(list);
		map.put(locale, list);
		return list;
	}

	public static String getValue(int code) {
		NameValuePair pair;
		Iterator<?> iterator = getCollection().iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			if (pair.getName().equals("" + code)) {
				return pair.getValue();
			}
		}
		return null;
	}
}
