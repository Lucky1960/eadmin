package nl.eadmin.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.eadmin.ui.uiobjects.UserContext;
import nl.ibs.util.NameValuePair;

public abstract class RekeningToonKolomEnum extends EnumHelper {
	public static final String DEBET = "D";
	public static final String CREDIT = "C";
	public static final String SALDO = "S";
	private static Map<String, List<NameValuePair>> map = new HashMap<String, List<NameValuePair>>(2);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static synchronized List getCollection() {
		String locale = UserContext.getLocale().toString();
		List list = (List) map.get(locale);
		if (list != null) {
			return list;
		}
		list = new ArrayList();
		list.add(new NameValuePair(DEBET, "Debet"));
		list.add(new NameValuePair(CREDIT, "Credit"));
		list.add(new NameValuePair(SALDO, "Saldoafhankelijk"));
		list = translate(list);
		map.put(locale, list);
		return list;
	}

	public static String getValue(String code) {
		NameValuePair pair;
		Iterator<?> iterator = getCollection().iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			if (pair.getName().equals(code)) {
				return pair.getValue();
			}
		}
		return null;
	}
}