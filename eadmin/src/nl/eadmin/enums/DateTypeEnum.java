package nl.eadmin.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.eadmin.ui.uiobjects.UserContext;
import nl.ibs.util.NameValuePair;

public abstract class DateTypeEnum extends EnumHelper {
	public static final int YYYYMMDD = 0;
	public static final int DDMMYYYY = 1;
	public static final int YYYY_MM_DD = 2;
	public static final int DD_MM_YYYY = 3;
	private static Map<String, List<NameValuePair>> map = new HashMap<String, List<NameValuePair>>(2);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static synchronized List getCollection() {
		String locale = UserContext.getLocale().toString();
		List list = (List) map.get(locale);
		if (list != null) {
			return list;
		}
		list = new ArrayList();
		list.add(new NameValuePair("" + YYYYMMDD, "jjjjmmdd"));
		list.add(new NameValuePair("" + YYYY_MM_DD, "jjjj-mm-dd"));
		list.add(new NameValuePair("" + DDMMYYYY, "ddmmjjjj"));
		list.add(new NameValuePair("" + DD_MM_YYYY, "dd-mm-jjjj"));
		list = translate(list);
		map.put(locale, list);
		return list;
	}

	public static String getValue(int code) {
		NameValuePair pair;
		Iterator<?> iterator = getCollection().iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			if (pair.getName().equals("" + code)) {
				return pair.getValue();
			}
		}
		return null;
	}
}