package nl.eadmin.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.eadmin.ui.uiobjects.UserContext;
import nl.ibs.util.NameValuePair;

public abstract class BankImportTypeEnum extends EnumHelper {
	public static final String TYPE_ABN_CSV = "ABN_CSV";
	public static final String TYPE_ABN_MT940 = "ABN_MT940";
	public static final String TYPE_DB_CSV = "DB_CSV";
	public static final String TYPE_DB_MT940 = "DB_MT940";
	public static final String TYPE_ING_CSV = "ING_CSV";
	public static final String TYPE_ING_MT940 = "ING_MT940";
	public static final String TYPE_KNAB_CSV = "KNAB_CSV";
	public static final String TYPE_KNAB_MT940 = "KNAB_MT940";
	public static final String TYPE_RABO_CSV = "RABO_CSV";
	public static final String TYPE_RABO_MT940 = "RABO_MT940";
	public static final String TYPE_TRIODOS_CSV = "TRIO";
	private static Map<String, List<NameValuePair>> map = new HashMap<String, List<NameValuePair>>(2);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static synchronized List getCollection() {
		String locale = UserContext.getLocale().toString();
		List list = (List) map.get(locale);
		if (list != null) {
			return list;
		}
		list = new ArrayList();
		list.add(new NameValuePair(TYPE_ABN_CSV, "ABN Amro - csv"));
		list.add(new NameValuePair(TYPE_ABN_MT940, "ABN Amro - MT940"));
		list.add(new NameValuePair(TYPE_DB_CSV, "Deutsche Bank - csv"));
//		list.add(new NameValuePair(TYPE_DB_MT940, "Deutsche Bank - MT940"));
		list.add(new NameValuePair(TYPE_ING_CSV, "ING Bank - csv"));
		list.add(new NameValuePair(TYPE_ING_MT940, "ING Bank - MT940"));
		list.add(new NameValuePair(TYPE_KNAB_CSV, "Knab - csv"));
//		list.add(new NameValuePair(TYPE_KNAB_MT940, "Knab - MT940"));
		list.add(new NameValuePair(TYPE_RABO_CSV, "Rabobank - csv"));
		list.add(new NameValuePair(TYPE_RABO_MT940, "Rabobank - MT940"));
		list.add(new NameValuePair(TYPE_TRIODOS_CSV, "Triodos Bank - csv"));
		list = translate(list);
		map.put(locale, list);
		return list;
	}

	public static String getValue(String code) {
		NameValuePair pair;
		Iterator<?> iterator = getCollection().iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			if (pair.getName().equals(code)) {
				return pair.getValue();
			}
		}
		return null;
	}
}