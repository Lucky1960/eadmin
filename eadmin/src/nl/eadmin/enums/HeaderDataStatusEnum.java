package nl.eadmin.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.eadmin.db.Dagboek;
import nl.eadmin.ui.uiobjects.UserContext;
import nl.ibs.util.NameValuePair;

public abstract class HeaderDataStatusEnum extends EnumHelper {
	public static final int BANK_OPEN = 10;
	public static final int BANK_CLOSED = 19;
	public static final int KAS_OPEN = 20;
	public static final int KAS_CLOSED = 29;
	public static final int MEMO_OPEN = 30;
	public static final int MEMO_CLOSED = 39;
	public static final int INKOOP_OPEN = 40;
	public static final int INKOOP_CLOSED = 49;
	public static final int VERKOOP_OPEN = 50;
	public static final int VERKOOP_CLOSED = 59;
	private static Map<String, List<NameValuePair>> map = new HashMap<String, List<NameValuePair>>(2);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static synchronized List getCollection(Dagboek dagboek) {
		String locale = UserContext.getLocale().toString();
		List list = (List) map.get(locale);
		if (list != null) {
			return list;
		}
		list = new ArrayList();
		if (dagboek == null || dagboek.isBankboek()) {
			list.add(new NameValuePair("" + BANK_OPEN, "Onvolledig"));
			list.add(new NameValuePair("" + BANK_CLOSED, "Volledig"));
		}
		if (dagboek == null || dagboek.isKasboek()) {
			list.add(new NameValuePair("" + KAS_OPEN, "Onvolledig"));
			list.add(new NameValuePair("" + KAS_CLOSED, "Volledig"));
		}
		if (dagboek == null || dagboek.isMemoriaal()) {
			list.add(new NameValuePair("" + MEMO_OPEN, "Onvolledig"));
			list.add(new NameValuePair("" + MEMO_CLOSED, "Volledig"));
		}
		if (dagboek == null || dagboek.isInkoopboek()) {
			list.add(new NameValuePair("" + INKOOP_OPEN, "Onvolledig"));
			list.add(new NameValuePair("" + INKOOP_CLOSED, "Volledig"));
		}
		if (dagboek == null || dagboek.isVerkoopboek()) {
			list.add(new NameValuePair("" + VERKOOP_OPEN, "Onvolledig"));
			list.add(new NameValuePair("" + VERKOOP_CLOSED, "Volledig"));
		}
		list = translate(list);
		map.put(locale, list);
		return list;
	}

	public static String getValue(int code) {
		NameValuePair pair;
		Iterator<?> iterator = getCollection(null).iterator();
		while (iterator.hasNext()) {
			pair = (NameValuePair) iterator.next();
			if (pair.getName().equals("" + code)) {
				return pair.getValue();
			}
		}
		return null;
	}
}