package nl.eadmin;

import java.util.Calendar;
import java.util.Date;

import nl.sf.api.ProductData;

// Testclass for licensecontrol

public class MyProductData implements ProductData {
	private static final long serialVersionUID = 5829206431049453052L;

	@Override
	public String getCustomerId() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProductId() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getApplication() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getModule01() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getModule02() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getModule03() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getModule04() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getModule05() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getModule06() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getModule07() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getModule08() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getModule09() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getModule10() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Date getStartDate() throws Exception {
		Calendar tmp = Calendar.getInstance();
		tmp.set(Calendar.YEAR, 2014);
		tmp.set(Calendar.MONTH, 11);
		tmp.set(Calendar.DATE, 1);
		return tmp.getTime();
	}

	@Override
	public Date getEndDate() throws Exception {
		Calendar tmp = Calendar.getInstance();
		tmp.set(Calendar.YEAR, 2015);
		tmp.set(Calendar.MONTH, 11);
		tmp.set(Calendar.DATE, 31);
		return tmp.getTime();
	}

	@Override
	public String getDatabase() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
