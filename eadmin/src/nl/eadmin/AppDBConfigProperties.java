package nl.eadmin;

import nl.ibs.jsql.DBConfigProperties;

/**
 * @author nlhuucle
 *
 */
public class AppDBConfigProperties {

	private static final String APPLICATION = ApplicationConstants.APPLICATION;

	public static boolean getAutoCreateTables() {
		return DBConfigProperties.getInstance().getAutoCreateTables(APPLICATION);
	}

	public static boolean getAutoAddColumns() {
		return DBConfigProperties.getInstance().getAutoAddColumns(APPLICATION);
	}

	public static boolean getAutoModifyColumns() {
		return DBConfigProperties.getInstance().getAutoModifyColumns(APPLICATION);
	}

	public static boolean getAutoDropColumns() {
		return DBConfigProperties.getInstance().getAutoDropColumns(APPLICATION);
	}

	public static String getDBManager() {
		return DBConfigProperties.getInstance().getDBManager(APPLICATION);
	}

	public static String getJDBCDriver() {
		return DBConfigProperties.getInstance().getJDBCDriver(APPLICATION);
	}

	public static String getURL() {
		return DBConfigProperties.getInstance().getURL(APPLICATION);
	}

	public static String getSchema() {
		return DBConfigProperties.getInstance().getSchema(APPLICATION);
	}

	public static String getUser() {
		return DBConfigProperties.getInstance().getUser(APPLICATION);
	}

	public static String getPassword() {
		return DBConfigProperties.getInstance().getPassword(APPLICATION);
	}

	public static String getSystem() {
		String url = getURL();
		int b = url.indexOf("as400:");
		if (b > -1) {
			int e = url.indexOf(";", b);
			if (e == -1) {
				e = url.length();
			}
			return url.substring(b + 6, e);
		}
		return null;
	}

	public static void setURL(String url) throws Exception {
		DBConfigProperties.getInstance().setProperty(APPLICATION, DBConfigProperties._URLDB, url);
	}

	public static void setSchema(String schema) throws Exception {
		DBConfigProperties.getInstance().setProperty(APPLICATION, DBConfigProperties._DB, schema);
	}

	public static void setUser(String user) throws Exception {
		DBConfigProperties.getInstance().setProperty(APPLICATION, DBConfigProperties._USERDB, user);
	}

	public static void setPassword(String password) throws Exception {
		DBConfigProperties.getInstance().setProperty(APPLICATION, DBConfigProperties._PASSWRDDB, password);
	}

}
