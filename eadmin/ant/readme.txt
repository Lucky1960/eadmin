Some notes:

1. ant/WEB-INF/classes/DocumentViewer.properties is an empty file:
This file is copied by build.xml ant script into the warfile.
userId, password, userName are empty and must be manually configured in the deployed webapp.

2. ant/WEB-INF/classes/jsql_config.properties
This file is copied by build.xml ant script into the warfile.
Settings in this file are configured by the jsql.html webpage.

3. ant/WEB-INF/classes/jeeLog.properties
This file is copied by build.xml ant script into the warfile.
The loglevel is ERROR and can be manually changed in the deployed webapp.

These files must manually be copied into src folder of this project.
Changes to these files (manually or by jsql.html) are not committed to CVS
as these files are in .cvsignore.
These files are excluded by build.xml ant.
So developer settings are never in the build.